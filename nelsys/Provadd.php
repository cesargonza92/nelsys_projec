<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "Provinfo.php" ?>
<?php include_once "Usuainfo.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$Prov_add = NULL; // Initialize page object first

class cProv_add extends cProv {

	// Page ID
	var $PageID = 'add';

	// Project ID
	var $ProjectID = "{04439FF7-B43F-460F-8514-F71C8FF9E679}";

	// Table name
	var $TableName = 'Prov';

	// Page object name
	var $PageObjName = 'Prov_add';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (Prov)
		if (!isset($GLOBALS["Prov"]) || get_class($GLOBALS["Prov"]) == "cProv") {
			$GLOBALS["Prov"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["Prov"];
		}

		// Table object (Usua)
		if (!isset($GLOBALS['Usua'])) $GLOBALS['Usua'] = new cUsua();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'add', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'Prov', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (Usua)
		if (!isset($UserTable)) {
			$UserTable = new cUsua();
			$UserTableConn = Conn($UserTable->DBID);
		}
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanAdd()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage($Language->Phrase("NoPermission")); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("Provlist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}

		// Create form object
		$objForm = new cFormObj();
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Process auto fill
		if (@$_POST["ajax"] == "autofill") {
			$results = $this->GetAutoFill(@$_POST["name"], @$_POST["q"]);
			if ($results) {

				// Clean output buffer
				if (!EW_DEBUG_ENABLED && ob_get_length())
					ob_end_clean();
				echo $results;
				$this->Page_Terminate();
				exit();
			}
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $Prov;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($Prov);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $FormClassName = "form-horizontal ewForm ewAddForm";
	var $DbMasterFilter = "";
	var $DbDetailFilter = "";
	var $StartRec;
	var $Priv = 0;
	var $OldRecordset;
	var $CopyRecord;

	// 
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError;

		// Process form if post back
		if (@$_POST["a_add"] <> "") {
			$this->CurrentAction = $_POST["a_add"]; // Get form action
			$this->CopyRecord = $this->LoadOldRecord(); // Load old recordset
			$this->LoadFormValues(); // Load form values
		} else { // Not post back

			// Load key values from QueryString
			$this->CopyRecord = TRUE;
			if (@$_GET["PrvCodi"] != "") {
				$this->PrvCodi->setQueryStringValue($_GET["PrvCodi"]);
				$this->setKey("PrvCodi", $this->PrvCodi->CurrentValue); // Set up key
			} else {
				$this->setKey("PrvCodi", ""); // Clear key
				$this->CopyRecord = FALSE;
			}
			if ($this->CopyRecord) {
				$this->CurrentAction = "C"; // Copy record
			} else {
				$this->CurrentAction = "I"; // Display blank record
				$this->LoadDefaultValues(); // Load default values
			}
		}

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Validate form if post back
		if (@$_POST["a_add"] <> "") {
			if (!$this->ValidateForm()) {
				$this->CurrentAction = "I"; // Form error, reset action
				$this->EventCancelled = TRUE; // Event cancelled
				$this->RestoreFormValues(); // Restore form values
				$this->setFailureMessage($gsFormError);
			}
		}

		// Perform action based on action code
		switch ($this->CurrentAction) {
			case "I": // Blank record, no action required
				break;
			case "C": // Copy an existing record
				if (!$this->LoadRow()) { // Load record based on key
					if ($this->getFailureMessage() == "") $this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
					$this->Page_Terminate("Provlist.php"); // No matching record, return to list
				}
				break;
			case "A": // Add new record
				$this->SendEmail = TRUE; // Send email on add success
				if ($this->AddRow($this->OldRecordset)) { // Add successful
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage($Language->Phrase("AddSuccess")); // Set up success message
					$sReturnUrl = $this->getReturnUrl();
					if (ew_GetPageName($sReturnUrl) == "Provview.php")
						$sReturnUrl = $this->GetViewUrl(); // View paging, return to view page with keyurl directly
					$this->Page_Terminate($sReturnUrl); // Clean up and return
				} else {
					$this->EventCancelled = TRUE; // Event cancelled
					$this->RestoreFormValues(); // Add failed, restore form values
				}
		}

		// Render row based on row type
		if ($this->CurrentAction == "F") { // Confirm page
			$this->RowType = EW_ROWTYPE_VIEW; // Render view type
		} else {
			$this->RowType = EW_ROWTYPE_ADD; // Render add type
		}

		// Render row
		$this->ResetAttrs();
		$this->RenderRow();
	}

	// Get upload files
	function GetUploadFiles() {
		global $objForm, $Language;

		// Get upload data
	}

	// Load default values
	function LoadDefaultValues() {
		$this->PrvFIni->CurrentValue = NULL;
		$this->PrvFIni->OldValue = $this->PrvFIni->CurrentValue;
		$this->PrvFFin->CurrentValue = NULL;
		$this->PrvFFin->OldValue = $this->PrvFFin->CurrentValue;
		$this->PrvPais->CurrentValue = NULL;
		$this->PrvPais->OldValue = $this->PrvPais->CurrentValue;
		$this->PrvTipo->CurrentValue = NULL;
		$this->PrvTipo->OldValue = $this->PrvTipo->CurrentValue;
		$this->PrvNomb->CurrentValue = NULL;
		$this->PrvNomb->OldValue = $this->PrvNomb->CurrentValue;
		$this->PrvApel->CurrentValue = NULL;
		$this->PrvApel->OldValue = $this->PrvApel->CurrentValue;
		$this->PrvCRuc->CurrentValue = NULL;
		$this->PrvCRuc->OldValue = $this->PrvCRuc->CurrentValue;
		$this->PrvDire->CurrentValue = NULL;
		$this->PrvDire->OldValue = $this->PrvDire->CurrentValue;
		$this->PrvMail->CurrentValue = NULL;
		$this->PrvMail->OldValue = $this->PrvMail->CurrentValue;
		$this->PrvTele->CurrentValue = NULL;
		$this->PrvTele->OldValue = $this->PrvTele->CurrentValue;
		$this->PrvRSoc->CurrentValue = NULL;
		$this->PrvRSoc->OldValue = $this->PrvRSoc->CurrentValue;
	}

	// Load form values
	function LoadFormValues() {

		// Load from form
		global $objForm;
		if (!$this->PrvFIni->FldIsDetailKey) {
			$this->PrvFIni->setFormValue($objForm->GetValue("x_PrvFIni"));
			$this->PrvFIni->CurrentValue = ew_UnFormatDateTime($this->PrvFIni->CurrentValue, 7);
		}
		if (!$this->PrvFFin->FldIsDetailKey) {
			$this->PrvFFin->setFormValue($objForm->GetValue("x_PrvFFin"));
			$this->PrvFFin->CurrentValue = ew_UnFormatDateTime($this->PrvFFin->CurrentValue, 7);
		}
		if (!$this->PrvPais->FldIsDetailKey) {
			$this->PrvPais->setFormValue($objForm->GetValue("x_PrvPais"));
		}
		if (!$this->PrvTipo->FldIsDetailKey) {
			$this->PrvTipo->setFormValue($objForm->GetValue("x_PrvTipo"));
		}
		if (!$this->PrvNomb->FldIsDetailKey) {
			$this->PrvNomb->setFormValue($objForm->GetValue("x_PrvNomb"));
		}
		if (!$this->PrvApel->FldIsDetailKey) {
			$this->PrvApel->setFormValue($objForm->GetValue("x_PrvApel"));
		}
		if (!$this->PrvCRuc->FldIsDetailKey) {
			$this->PrvCRuc->setFormValue($objForm->GetValue("x_PrvCRuc"));
		}
		if (!$this->PrvDire->FldIsDetailKey) {
			$this->PrvDire->setFormValue($objForm->GetValue("x_PrvDire"));
		}
		if (!$this->PrvMail->FldIsDetailKey) {
			$this->PrvMail->setFormValue($objForm->GetValue("x_PrvMail"));
		}
		if (!$this->PrvTele->FldIsDetailKey) {
			$this->PrvTele->setFormValue($objForm->GetValue("x_PrvTele"));
		}
		if (!$this->PrvRSoc->FldIsDetailKey) {
			$this->PrvRSoc->setFormValue($objForm->GetValue("x_PrvRSoc"));
		}
	}

	// Restore form values
	function RestoreFormValues() {
		global $objForm;
		$this->LoadOldRecord();
		$this->PrvFIni->CurrentValue = $this->PrvFIni->FormValue;
		$this->PrvFIni->CurrentValue = ew_UnFormatDateTime($this->PrvFIni->CurrentValue, 7);
		$this->PrvFFin->CurrentValue = $this->PrvFFin->FormValue;
		$this->PrvFFin->CurrentValue = ew_UnFormatDateTime($this->PrvFFin->CurrentValue, 7);
		$this->PrvPais->CurrentValue = $this->PrvPais->FormValue;
		$this->PrvTipo->CurrentValue = $this->PrvTipo->FormValue;
		$this->PrvNomb->CurrentValue = $this->PrvNomb->FormValue;
		$this->PrvApel->CurrentValue = $this->PrvApel->FormValue;
		$this->PrvCRuc->CurrentValue = $this->PrvCRuc->FormValue;
		$this->PrvDire->CurrentValue = $this->PrvDire->FormValue;
		$this->PrvMail->CurrentValue = $this->PrvMail->FormValue;
		$this->PrvTele->CurrentValue = $this->PrvTele->FormValue;
		$this->PrvRSoc->CurrentValue = $this->PrvRSoc->FormValue;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->PrvCodi->setDbValue($rs->fields('PrvCodi'));
		$this->PrvFIni->setDbValue($rs->fields('PrvFIni'));
		$this->PrvFFin->setDbValue($rs->fields('PrvFFin'));
		$this->PrvPais->setDbValue($rs->fields('PrvPais'));
		$this->PrvTipo->setDbValue($rs->fields('PrvTipo'));
		$this->PrvNomb->setDbValue($rs->fields('PrvNomb'));
		$this->PrvApel->setDbValue($rs->fields('PrvApel'));
		$this->PrvCRuc->setDbValue($rs->fields('PrvCRuc'));
		$this->PrvDire->setDbValue($rs->fields('PrvDire'));
		$this->PrvMail->setDbValue($rs->fields('PrvMail'));
		$this->PrvTele->setDbValue($rs->fields('PrvTele'));
		$this->PrvRSoc->setDbValue($rs->fields('PrvRSoc'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->PrvCodi->DbValue = $row['PrvCodi'];
		$this->PrvFIni->DbValue = $row['PrvFIni'];
		$this->PrvFFin->DbValue = $row['PrvFFin'];
		$this->PrvPais->DbValue = $row['PrvPais'];
		$this->PrvTipo->DbValue = $row['PrvTipo'];
		$this->PrvNomb->DbValue = $row['PrvNomb'];
		$this->PrvApel->DbValue = $row['PrvApel'];
		$this->PrvCRuc->DbValue = $row['PrvCRuc'];
		$this->PrvDire->DbValue = $row['PrvDire'];
		$this->PrvMail->DbValue = $row['PrvMail'];
		$this->PrvTele->DbValue = $row['PrvTele'];
		$this->PrvRSoc->DbValue = $row['PrvRSoc'];
	}

	// Load old record
	function LoadOldRecord() {

		// Load key values from Session
		$bValidKey = TRUE;
		if (strval($this->getKey("PrvCodi")) <> "")
			$this->PrvCodi->CurrentValue = $this->getKey("PrvCodi"); // PrvCodi
		else
			$bValidKey = FALSE;

		// Load old recordset
		if ($bValidKey) {
			$this->CurrentFilter = $this->KeyFilter();
			$sSql = $this->SQL();
			$conn = &$this->Connection();
			$this->OldRecordset = ew_LoadRecordset($sSql, $conn);
			$this->LoadRowValues($this->OldRecordset); // Load row values
		} else {
			$this->OldRecordset = NULL;
		}
		return $bValidKey;
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		// Call Row_Rendering event

		$this->Row_Rendering();

		// Common render codes for all row types
		// PrvCodi
		// PrvFIni
		// PrvFFin
		// PrvPais
		// PrvTipo
		// PrvNomb
		// PrvApel
		// PrvCRuc
		// PrvDire
		// PrvMail
		// PrvTele
		// PrvRSoc

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// PrvCodi
		$this->PrvCodi->ViewValue = $this->PrvCodi->CurrentValue;
		$this->PrvCodi->ViewCustomAttributes = "";

		// PrvFIni
		$this->PrvFIni->ViewValue = $this->PrvFIni->CurrentValue;
		$this->PrvFIni->ViewValue = ew_FormatDateTime($this->PrvFIni->ViewValue, 7);
		$this->PrvFIni->ViewCustomAttributes = "";

		// PrvFFin
		$this->PrvFFin->ViewValue = $this->PrvFFin->CurrentValue;
		$this->PrvFFin->ViewValue = ew_FormatDateTime($this->PrvFFin->ViewValue, 7);
		$this->PrvFFin->ViewCustomAttributes = "";

		// PrvPais
		$this->PrvPais->ViewValue = $this->PrvPais->CurrentValue;
		$this->PrvPais->ViewCustomAttributes = "";

		// PrvTipo
		$this->PrvTipo->ViewValue = $this->PrvTipo->CurrentValue;
		$this->PrvTipo->ViewCustomAttributes = "";

		// PrvNomb
		$this->PrvNomb->ViewValue = $this->PrvNomb->CurrentValue;
		$this->PrvNomb->ViewCustomAttributes = "";

		// PrvApel
		$this->PrvApel->ViewValue = $this->PrvApel->CurrentValue;
		$this->PrvApel->ViewCustomAttributes = "";

		// PrvCRuc
		$this->PrvCRuc->ViewValue = $this->PrvCRuc->CurrentValue;
		$this->PrvCRuc->ViewCustomAttributes = "";

		// PrvDire
		$this->PrvDire->ViewValue = $this->PrvDire->CurrentValue;
		$this->PrvDire->ViewCustomAttributes = "";

		// PrvMail
		$this->PrvMail->ViewValue = $this->PrvMail->CurrentValue;
		$this->PrvMail->ViewCustomAttributes = "";

		// PrvTele
		$this->PrvTele->ViewValue = $this->PrvTele->CurrentValue;
		$this->PrvTele->ViewCustomAttributes = "";

		// PrvRSoc
		$this->PrvRSoc->ViewValue = $this->PrvRSoc->CurrentValue;
		$this->PrvRSoc->ViewCustomAttributes = "";

			// PrvFIni
			$this->PrvFIni->LinkCustomAttributes = "";
			$this->PrvFIni->HrefValue = "";
			$this->PrvFIni->TooltipValue = "";

			// PrvFFin
			$this->PrvFFin->LinkCustomAttributes = "";
			$this->PrvFFin->HrefValue = "";
			$this->PrvFFin->TooltipValue = "";

			// PrvPais
			$this->PrvPais->LinkCustomAttributes = "";
			$this->PrvPais->HrefValue = "";
			$this->PrvPais->TooltipValue = "";

			// PrvTipo
			$this->PrvTipo->LinkCustomAttributes = "";
			$this->PrvTipo->HrefValue = "";
			$this->PrvTipo->TooltipValue = "";

			// PrvNomb
			$this->PrvNomb->LinkCustomAttributes = "";
			$this->PrvNomb->HrefValue = "";
			$this->PrvNomb->TooltipValue = "";

			// PrvApel
			$this->PrvApel->LinkCustomAttributes = "";
			$this->PrvApel->HrefValue = "";
			$this->PrvApel->TooltipValue = "";

			// PrvCRuc
			$this->PrvCRuc->LinkCustomAttributes = "";
			$this->PrvCRuc->HrefValue = "";
			$this->PrvCRuc->TooltipValue = "";

			// PrvDire
			$this->PrvDire->LinkCustomAttributes = "";
			$this->PrvDire->HrefValue = "";
			$this->PrvDire->TooltipValue = "";

			// PrvMail
			$this->PrvMail->LinkCustomAttributes = "";
			$this->PrvMail->HrefValue = "";
			$this->PrvMail->TooltipValue = "";

			// PrvTele
			$this->PrvTele->LinkCustomAttributes = "";
			$this->PrvTele->HrefValue = "";
			$this->PrvTele->TooltipValue = "";

			// PrvRSoc
			$this->PrvRSoc->LinkCustomAttributes = "";
			$this->PrvRSoc->HrefValue = "";
			$this->PrvRSoc->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_ADD) { // Add row

			// PrvFIni
			$this->PrvFIni->EditAttrs["class"] = "form-control";
			$this->PrvFIni->EditCustomAttributes = "";
			$this->PrvFIni->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->PrvFIni->CurrentValue, 7));
			$this->PrvFIni->PlaceHolder = ew_RemoveHtml($this->PrvFIni->FldCaption());

			// PrvFFin
			$this->PrvFFin->EditAttrs["class"] = "form-control";
			$this->PrvFFin->EditCustomAttributes = "";
			$this->PrvFFin->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->PrvFFin->CurrentValue, 7));
			$this->PrvFFin->PlaceHolder = ew_RemoveHtml($this->PrvFFin->FldCaption());

			// PrvPais
			$this->PrvPais->EditAttrs["class"] = "form-control";
			$this->PrvPais->EditCustomAttributes = "";
			$this->PrvPais->EditValue = ew_HtmlEncode($this->PrvPais->CurrentValue);
			$this->PrvPais->PlaceHolder = ew_RemoveHtml($this->PrvPais->FldCaption());

			// PrvTipo
			$this->PrvTipo->EditAttrs["class"] = "form-control";
			$this->PrvTipo->EditCustomAttributes = "";
			$this->PrvTipo->EditValue = ew_HtmlEncode($this->PrvTipo->CurrentValue);
			$this->PrvTipo->PlaceHolder = ew_RemoveHtml($this->PrvTipo->FldCaption());

			// PrvNomb
			$this->PrvNomb->EditAttrs["class"] = "form-control";
			$this->PrvNomb->EditCustomAttributes = "";
			$this->PrvNomb->EditValue = ew_HtmlEncode($this->PrvNomb->CurrentValue);
			$this->PrvNomb->PlaceHolder = ew_RemoveHtml($this->PrvNomb->FldCaption());

			// PrvApel
			$this->PrvApel->EditAttrs["class"] = "form-control";
			$this->PrvApel->EditCustomAttributes = "";
			$this->PrvApel->EditValue = ew_HtmlEncode($this->PrvApel->CurrentValue);
			$this->PrvApel->PlaceHolder = ew_RemoveHtml($this->PrvApel->FldCaption());

			// PrvCRuc
			$this->PrvCRuc->EditAttrs["class"] = "form-control";
			$this->PrvCRuc->EditCustomAttributes = "";
			$this->PrvCRuc->EditValue = ew_HtmlEncode($this->PrvCRuc->CurrentValue);
			$this->PrvCRuc->PlaceHolder = ew_RemoveHtml($this->PrvCRuc->FldCaption());

			// PrvDire
			$this->PrvDire->EditAttrs["class"] = "form-control";
			$this->PrvDire->EditCustomAttributes = "";
			$this->PrvDire->EditValue = ew_HtmlEncode($this->PrvDire->CurrentValue);
			$this->PrvDire->PlaceHolder = ew_RemoveHtml($this->PrvDire->FldCaption());

			// PrvMail
			$this->PrvMail->EditAttrs["class"] = "form-control";
			$this->PrvMail->EditCustomAttributes = "";
			$this->PrvMail->EditValue = ew_HtmlEncode($this->PrvMail->CurrentValue);
			$this->PrvMail->PlaceHolder = ew_RemoveHtml($this->PrvMail->FldCaption());

			// PrvTele
			$this->PrvTele->EditAttrs["class"] = "form-control";
			$this->PrvTele->EditCustomAttributes = "";
			$this->PrvTele->EditValue = ew_HtmlEncode($this->PrvTele->CurrentValue);
			$this->PrvTele->PlaceHolder = ew_RemoveHtml($this->PrvTele->FldCaption());

			// PrvRSoc
			$this->PrvRSoc->EditAttrs["class"] = "form-control";
			$this->PrvRSoc->EditCustomAttributes = "";
			$this->PrvRSoc->EditValue = ew_HtmlEncode($this->PrvRSoc->CurrentValue);
			$this->PrvRSoc->PlaceHolder = ew_RemoveHtml($this->PrvRSoc->FldCaption());

			// Edit refer script
			// PrvFIni

			$this->PrvFIni->HrefValue = "";

			// PrvFFin
			$this->PrvFFin->HrefValue = "";

			// PrvPais
			$this->PrvPais->HrefValue = "";

			// PrvTipo
			$this->PrvTipo->HrefValue = "";

			// PrvNomb
			$this->PrvNomb->HrefValue = "";

			// PrvApel
			$this->PrvApel->HrefValue = "";

			// PrvCRuc
			$this->PrvCRuc->HrefValue = "";

			// PrvDire
			$this->PrvDire->HrefValue = "";

			// PrvMail
			$this->PrvMail->HrefValue = "";

			// PrvTele
			$this->PrvTele->HrefValue = "";

			// PrvRSoc
			$this->PrvRSoc->HrefValue = "";
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate form
	function ValidateForm() {
		global $Language, $gsFormError;

		// Initialize form error message
		$gsFormError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return ($gsFormError == "");
		if (!$this->PrvFIni->FldIsDetailKey && !is_null($this->PrvFIni->FormValue) && $this->PrvFIni->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->PrvFIni->FldCaption(), $this->PrvFIni->ReqErrMsg));
		}
		if (!ew_CheckEuroDate($this->PrvFIni->FormValue)) {
			ew_AddMessage($gsFormError, $this->PrvFIni->FldErrMsg());
		}
		if (!ew_CheckEuroDate($this->PrvFFin->FormValue)) {
			ew_AddMessage($gsFormError, $this->PrvFFin->FldErrMsg());
		}
		if (!$this->PrvPais->FldIsDetailKey && !is_null($this->PrvPais->FormValue) && $this->PrvPais->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->PrvPais->FldCaption(), $this->PrvPais->ReqErrMsg));
		}
		if (!$this->PrvTipo->FldIsDetailKey && !is_null($this->PrvTipo->FormValue) && $this->PrvTipo->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->PrvTipo->FldCaption(), $this->PrvTipo->ReqErrMsg));
		}
		if (!$this->PrvCRuc->FldIsDetailKey && !is_null($this->PrvCRuc->FormValue) && $this->PrvCRuc->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->PrvCRuc->FldCaption(), $this->PrvCRuc->ReqErrMsg));
		}
		if (!$this->PrvMail->FldIsDetailKey && !is_null($this->PrvMail->FormValue) && $this->PrvMail->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->PrvMail->FldCaption(), $this->PrvMail->ReqErrMsg));
		}
		if (!ew_CheckEmail($this->PrvMail->FormValue)) {
			ew_AddMessage($gsFormError, $this->PrvMail->FldErrMsg());
		}
		if (!ew_CheckInteger($this->PrvTele->FormValue)) {
			ew_AddMessage($gsFormError, $this->PrvTele->FldErrMsg());
		}
		if (!$this->PrvRSoc->FldIsDetailKey && !is_null($this->PrvRSoc->FormValue) && $this->PrvRSoc->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->PrvRSoc->FldCaption(), $this->PrvRSoc->ReqErrMsg));
		}

		// Return validate result
		$ValidateForm = ($gsFormError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateForm = $ValidateForm && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsFormError, $sFormCustomError);
		}
		return $ValidateForm;
	}

	// Add record
	function AddRow($rsold = NULL) {
		global $Language, $Security;
		if ($this->PrvCRuc->CurrentValue <> "") { // Check field with unique index
			$sFilter = "(PrvCRuc = '" . ew_AdjustSql($this->PrvCRuc->CurrentValue, $this->DBID) . "')";
			$rsChk = $this->LoadRs($sFilter);
			if ($rsChk && !$rsChk->EOF) {
				$sIdxErrMsg = str_replace("%f", $this->PrvCRuc->FldCaption(), $Language->Phrase("DupIndex"));
				$sIdxErrMsg = str_replace("%v", $this->PrvCRuc->CurrentValue, $sIdxErrMsg);
				$this->setFailureMessage($sIdxErrMsg);
				$rsChk->Close();
				return FALSE;
			}
		}
		if ($this->PrvMail->CurrentValue <> "") { // Check field with unique index
			$sFilter = "(PrvMail = '" . ew_AdjustSql($this->PrvMail->CurrentValue, $this->DBID) . "')";
			$rsChk = $this->LoadRs($sFilter);
			if ($rsChk && !$rsChk->EOF) {
				$sIdxErrMsg = str_replace("%f", $this->PrvMail->FldCaption(), $Language->Phrase("DupIndex"));
				$sIdxErrMsg = str_replace("%v", $this->PrvMail->CurrentValue, $sIdxErrMsg);
				$this->setFailureMessage($sIdxErrMsg);
				$rsChk->Close();
				return FALSE;
			}
		}
		if ($this->PrvRSoc->CurrentValue <> "") { // Check field with unique index
			$sFilter = "(PrvRSoc = '" . ew_AdjustSql($this->PrvRSoc->CurrentValue, $this->DBID) . "')";
			$rsChk = $this->LoadRs($sFilter);
			if ($rsChk && !$rsChk->EOF) {
				$sIdxErrMsg = str_replace("%f", $this->PrvRSoc->FldCaption(), $Language->Phrase("DupIndex"));
				$sIdxErrMsg = str_replace("%v", $this->PrvRSoc->CurrentValue, $sIdxErrMsg);
				$this->setFailureMessage($sIdxErrMsg);
				$rsChk->Close();
				return FALSE;
			}
		}
		$conn = &$this->Connection();

		// Load db values from rsold
		if ($rsold) {
			$this->LoadDbValues($rsold);
		}
		$rsnew = array();

		// PrvFIni
		$this->PrvFIni->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->PrvFIni->CurrentValue, 7), ew_CurrentDate(), FALSE);

		// PrvFFin
		$this->PrvFFin->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->PrvFFin->CurrentValue, 7), NULL, FALSE);

		// PrvPais
		$this->PrvPais->SetDbValueDef($rsnew, $this->PrvPais->CurrentValue, "", FALSE);

		// PrvTipo
		$this->PrvTipo->SetDbValueDef($rsnew, $this->PrvTipo->CurrentValue, "", FALSE);

		// PrvNomb
		$this->PrvNomb->SetDbValueDef($rsnew, $this->PrvNomb->CurrentValue, NULL, FALSE);

		// PrvApel
		$this->PrvApel->SetDbValueDef($rsnew, $this->PrvApel->CurrentValue, NULL, FALSE);

		// PrvCRuc
		$this->PrvCRuc->SetDbValueDef($rsnew, $this->PrvCRuc->CurrentValue, NULL, FALSE);

		// PrvDire
		$this->PrvDire->SetDbValueDef($rsnew, $this->PrvDire->CurrentValue, NULL, FALSE);

		// PrvMail
		$this->PrvMail->SetDbValueDef($rsnew, $this->PrvMail->CurrentValue, NULL, FALSE);

		// PrvTele
		$this->PrvTele->SetDbValueDef($rsnew, $this->PrvTele->CurrentValue, NULL, FALSE);

		// PrvRSoc
		$this->PrvRSoc->SetDbValueDef($rsnew, $this->PrvRSoc->CurrentValue, NULL, FALSE);

		// Call Row Inserting event
		$rs = ($rsold == NULL) ? NULL : $rsold->fields;
		$bInsertRow = $this->Row_Inserting($rs, $rsnew);
		if ($bInsertRow) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			$AddRow = $this->Insert($rsnew);
			$conn->raiseErrorFn = '';
			if ($AddRow) {

				// Get insert id if necessary
				$this->PrvCodi->setDbValue($conn->GetOne("SELECT currval('\"Prov_PrvCodi_seq\"'::regclass)"));
				$rsnew['PrvCodi'] = $this->PrvCodi->DbValue;
			}
		} else {
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("InsertCancelled"));
			}
			$AddRow = FALSE;
		}
		if ($AddRow) {

			// Call Row Inserted event
			$rs = ($rsold == NULL) ? NULL : $rsold->fields;
			$this->Row_Inserted($rs, $rsnew);
		}
		return $AddRow;
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, "Provlist.php", "", $this->TableVar, TRUE);
		$PageId = ($this->CurrentAction == "C") ? "Copy" : "Add";
		$Breadcrumb->Add("add", $PageId, $url);
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($Prov_add)) $Prov_add = new cProv_add();

// Page init
$Prov_add->Page_Init();

// Page main
$Prov_add->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$Prov_add->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "add";
var CurrentForm = fProvadd = new ew_Form("fProvadd", "add");

// Validate form
fProvadd.Validate = function() {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.GetForm(), $fobj = $(fobj);
	if ($fobj.find("#a_confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.FormKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = $fobj.find("#a_list").val() == "gridinsert";
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
			elm = this.GetElements("x" + infix + "_PrvFIni");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $Prov->PrvFIni->FldCaption(), $Prov->PrvFIni->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_PrvFIni");
			if (elm && !ew_CheckEuroDate(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($Prov->PrvFIni->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_PrvFFin");
			if (elm && !ew_CheckEuroDate(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($Prov->PrvFFin->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_PrvPais");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $Prov->PrvPais->FldCaption(), $Prov->PrvPais->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_PrvTipo");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $Prov->PrvTipo->FldCaption(), $Prov->PrvTipo->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_PrvCRuc");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $Prov->PrvCRuc->FldCaption(), $Prov->PrvCRuc->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_PrvMail");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $Prov->PrvMail->FldCaption(), $Prov->PrvMail->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_PrvMail");
			if (elm && !ew_CheckEmail(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($Prov->PrvMail->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_PrvTele");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($Prov->PrvTele->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_PrvRSoc");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $Prov->PrvRSoc->FldCaption(), $Prov->PrvRSoc->ReqErrMsg)) ?>");

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
	}

	// Process detail forms
	var dfs = $fobj.find("input[name='detailpage']").get();
	for (var i = 0; i < dfs.length; i++) {
		var df = dfs[i], val = df.value;
		if (val && ewForms[val])
			if (!ewForms[val].Validate())
				return false;
	}
	return true;
}

// Form_CustomValidate event
fProvadd.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fProvadd.ValidateRequired = true;
<?php } else { ?>
fProvadd.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
// Form object for search

</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php $Prov_add->ShowPageHeader(); ?>
<?php
$Prov_add->ShowMessage();
?>
<form name="fProvadd" id="fProvadd" class="<?php echo $Prov_add->FormClassName ?>" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($Prov_add->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $Prov_add->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="Prov">
<input type="hidden" name="a_add" id="a_add" value="A">
<?php if ($Prov->CurrentAction == "F") { // Confirm page ?>
<input type="hidden" name="a_confirm" id="a_confirm" value="F">
<?php } ?>
<div>
<?php if ($Prov->PrvFIni->Visible) { // PrvFIni ?>
	<div id="r_PrvFIni" class="form-group">
		<label id="elh_Prov_PrvFIni" for="x_PrvFIni" class="col-sm-2 control-label ewLabel"><?php echo $Prov->PrvFIni->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $Prov->PrvFIni->CellAttributes() ?>>
<?php if ($Prov->CurrentAction <> "F") { ?>
<span id="el_Prov_PrvFIni">
<input type="text" data-table="Prov" data-field="x_PrvFIni" data-format="7" name="x_PrvFIni" id="x_PrvFIni" placeholder="<?php echo ew_HtmlEncode($Prov->PrvFIni->getPlaceHolder()) ?>" value="<?php echo $Prov->PrvFIni->EditValue ?>"<?php echo $Prov->PrvFIni->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_Prov_PrvFIni">
<span<?php echo $Prov->PrvFIni->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $Prov->PrvFIni->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="Prov" data-field="x_PrvFIni" name="x_PrvFIni" id="x_PrvFIni" value="<?php echo ew_HtmlEncode($Prov->PrvFIni->FormValue) ?>">
<?php } ?>
<?php echo $Prov->PrvFIni->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($Prov->PrvFFin->Visible) { // PrvFFin ?>
	<div id="r_PrvFFin" class="form-group">
		<label id="elh_Prov_PrvFFin" for="x_PrvFFin" class="col-sm-2 control-label ewLabel"><?php echo $Prov->PrvFFin->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $Prov->PrvFFin->CellAttributes() ?>>
<?php if ($Prov->CurrentAction <> "F") { ?>
<span id="el_Prov_PrvFFin">
<input type="text" data-table="Prov" data-field="x_PrvFFin" data-format="7" name="x_PrvFFin" id="x_PrvFFin" placeholder="<?php echo ew_HtmlEncode($Prov->PrvFFin->getPlaceHolder()) ?>" value="<?php echo $Prov->PrvFFin->EditValue ?>"<?php echo $Prov->PrvFFin->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_Prov_PrvFFin">
<span<?php echo $Prov->PrvFFin->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $Prov->PrvFFin->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="Prov" data-field="x_PrvFFin" name="x_PrvFFin" id="x_PrvFFin" value="<?php echo ew_HtmlEncode($Prov->PrvFFin->FormValue) ?>">
<?php } ?>
<?php echo $Prov->PrvFFin->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($Prov->PrvPais->Visible) { // PrvPais ?>
	<div id="r_PrvPais" class="form-group">
		<label id="elh_Prov_PrvPais" for="x_PrvPais" class="col-sm-2 control-label ewLabel"><?php echo $Prov->PrvPais->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $Prov->PrvPais->CellAttributes() ?>>
<?php if ($Prov->CurrentAction <> "F") { ?>
<span id="el_Prov_PrvPais">
<input type="text" data-table="Prov" data-field="x_PrvPais" name="x_PrvPais" id="x_PrvPais" size="30" placeholder="<?php echo ew_HtmlEncode($Prov->PrvPais->getPlaceHolder()) ?>" value="<?php echo $Prov->PrvPais->EditValue ?>"<?php echo $Prov->PrvPais->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_Prov_PrvPais">
<span<?php echo $Prov->PrvPais->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $Prov->PrvPais->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="Prov" data-field="x_PrvPais" name="x_PrvPais" id="x_PrvPais" value="<?php echo ew_HtmlEncode($Prov->PrvPais->FormValue) ?>">
<?php } ?>
<?php echo $Prov->PrvPais->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($Prov->PrvTipo->Visible) { // PrvTipo ?>
	<div id="r_PrvTipo" class="form-group">
		<label id="elh_Prov_PrvTipo" for="x_PrvTipo" class="col-sm-2 control-label ewLabel"><?php echo $Prov->PrvTipo->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $Prov->PrvTipo->CellAttributes() ?>>
<?php if ($Prov->CurrentAction <> "F") { ?>
<span id="el_Prov_PrvTipo">
<input type="text" data-table="Prov" data-field="x_PrvTipo" name="x_PrvTipo" id="x_PrvTipo" size="30" maxlength="1" placeholder="<?php echo ew_HtmlEncode($Prov->PrvTipo->getPlaceHolder()) ?>" value="<?php echo $Prov->PrvTipo->EditValue ?>"<?php echo $Prov->PrvTipo->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_Prov_PrvTipo">
<span<?php echo $Prov->PrvTipo->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $Prov->PrvTipo->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="Prov" data-field="x_PrvTipo" name="x_PrvTipo" id="x_PrvTipo" value="<?php echo ew_HtmlEncode($Prov->PrvTipo->FormValue) ?>">
<?php } ?>
<?php echo $Prov->PrvTipo->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($Prov->PrvNomb->Visible) { // PrvNomb ?>
	<div id="r_PrvNomb" class="form-group">
		<label id="elh_Prov_PrvNomb" for="x_PrvNomb" class="col-sm-2 control-label ewLabel"><?php echo $Prov->PrvNomb->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $Prov->PrvNomb->CellAttributes() ?>>
<?php if ($Prov->CurrentAction <> "F") { ?>
<span id="el_Prov_PrvNomb">
<input type="text" data-table="Prov" data-field="x_PrvNomb" name="x_PrvNomb" id="x_PrvNomb" size="30" placeholder="<?php echo ew_HtmlEncode($Prov->PrvNomb->getPlaceHolder()) ?>" value="<?php echo $Prov->PrvNomb->EditValue ?>"<?php echo $Prov->PrvNomb->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_Prov_PrvNomb">
<span<?php echo $Prov->PrvNomb->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $Prov->PrvNomb->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="Prov" data-field="x_PrvNomb" name="x_PrvNomb" id="x_PrvNomb" value="<?php echo ew_HtmlEncode($Prov->PrvNomb->FormValue) ?>">
<?php } ?>
<?php echo $Prov->PrvNomb->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($Prov->PrvApel->Visible) { // PrvApel ?>
	<div id="r_PrvApel" class="form-group">
		<label id="elh_Prov_PrvApel" for="x_PrvApel" class="col-sm-2 control-label ewLabel"><?php echo $Prov->PrvApel->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $Prov->PrvApel->CellAttributes() ?>>
<?php if ($Prov->CurrentAction <> "F") { ?>
<span id="el_Prov_PrvApel">
<input type="text" data-table="Prov" data-field="x_PrvApel" name="x_PrvApel" id="x_PrvApel" size="30" placeholder="<?php echo ew_HtmlEncode($Prov->PrvApel->getPlaceHolder()) ?>" value="<?php echo $Prov->PrvApel->EditValue ?>"<?php echo $Prov->PrvApel->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_Prov_PrvApel">
<span<?php echo $Prov->PrvApel->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $Prov->PrvApel->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="Prov" data-field="x_PrvApel" name="x_PrvApel" id="x_PrvApel" value="<?php echo ew_HtmlEncode($Prov->PrvApel->FormValue) ?>">
<?php } ?>
<?php echo $Prov->PrvApel->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($Prov->PrvCRuc->Visible) { // PrvCRuc ?>
	<div id="r_PrvCRuc" class="form-group">
		<label id="elh_Prov_PrvCRuc" for="x_PrvCRuc" class="col-sm-2 control-label ewLabel"><?php echo $Prov->PrvCRuc->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $Prov->PrvCRuc->CellAttributes() ?>>
<?php if ($Prov->CurrentAction <> "F") { ?>
<span id="el_Prov_PrvCRuc">
<input type="text" data-table="Prov" data-field="x_PrvCRuc" name="x_PrvCRuc" id="x_PrvCRuc" size="30" placeholder="<?php echo ew_HtmlEncode($Prov->PrvCRuc->getPlaceHolder()) ?>" value="<?php echo $Prov->PrvCRuc->EditValue ?>"<?php echo $Prov->PrvCRuc->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_Prov_PrvCRuc">
<span<?php echo $Prov->PrvCRuc->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $Prov->PrvCRuc->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="Prov" data-field="x_PrvCRuc" name="x_PrvCRuc" id="x_PrvCRuc" value="<?php echo ew_HtmlEncode($Prov->PrvCRuc->FormValue) ?>">
<?php } ?>
<?php echo $Prov->PrvCRuc->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($Prov->PrvDire->Visible) { // PrvDire ?>
	<div id="r_PrvDire" class="form-group">
		<label id="elh_Prov_PrvDire" for="x_PrvDire" class="col-sm-2 control-label ewLabel"><?php echo $Prov->PrvDire->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $Prov->PrvDire->CellAttributes() ?>>
<?php if ($Prov->CurrentAction <> "F") { ?>
<span id="el_Prov_PrvDire">
<input type="text" data-table="Prov" data-field="x_PrvDire" name="x_PrvDire" id="x_PrvDire" size="30" placeholder="<?php echo ew_HtmlEncode($Prov->PrvDire->getPlaceHolder()) ?>" value="<?php echo $Prov->PrvDire->EditValue ?>"<?php echo $Prov->PrvDire->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_Prov_PrvDire">
<span<?php echo $Prov->PrvDire->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $Prov->PrvDire->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="Prov" data-field="x_PrvDire" name="x_PrvDire" id="x_PrvDire" value="<?php echo ew_HtmlEncode($Prov->PrvDire->FormValue) ?>">
<?php } ?>
<?php echo $Prov->PrvDire->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($Prov->PrvMail->Visible) { // PrvMail ?>
	<div id="r_PrvMail" class="form-group">
		<label id="elh_Prov_PrvMail" for="x_PrvMail" class="col-sm-2 control-label ewLabel"><?php echo $Prov->PrvMail->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $Prov->PrvMail->CellAttributes() ?>>
<?php if ($Prov->CurrentAction <> "F") { ?>
<span id="el_Prov_PrvMail">
<input type="text" data-table="Prov" data-field="x_PrvMail" name="x_PrvMail" id="x_PrvMail" size="30" maxlength="50" placeholder="<?php echo ew_HtmlEncode($Prov->PrvMail->getPlaceHolder()) ?>" value="<?php echo $Prov->PrvMail->EditValue ?>"<?php echo $Prov->PrvMail->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_Prov_PrvMail">
<span<?php echo $Prov->PrvMail->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $Prov->PrvMail->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="Prov" data-field="x_PrvMail" name="x_PrvMail" id="x_PrvMail" value="<?php echo ew_HtmlEncode($Prov->PrvMail->FormValue) ?>">
<?php } ?>
<?php echo $Prov->PrvMail->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($Prov->PrvTele->Visible) { // PrvTele ?>
	<div id="r_PrvTele" class="form-group">
		<label id="elh_Prov_PrvTele" for="x_PrvTele" class="col-sm-2 control-label ewLabel"><?php echo $Prov->PrvTele->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $Prov->PrvTele->CellAttributes() ?>>
<?php if ($Prov->CurrentAction <> "F") { ?>
<span id="el_Prov_PrvTele">
<input type="text" data-table="Prov" data-field="x_PrvTele" name="x_PrvTele" id="x_PrvTele" size="30" placeholder="<?php echo ew_HtmlEncode($Prov->PrvTele->getPlaceHolder()) ?>" value="<?php echo $Prov->PrvTele->EditValue ?>"<?php echo $Prov->PrvTele->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_Prov_PrvTele">
<span<?php echo $Prov->PrvTele->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $Prov->PrvTele->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="Prov" data-field="x_PrvTele" name="x_PrvTele" id="x_PrvTele" value="<?php echo ew_HtmlEncode($Prov->PrvTele->FormValue) ?>">
<?php } ?>
<?php echo $Prov->PrvTele->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($Prov->PrvRSoc->Visible) { // PrvRSoc ?>
	<div id="r_PrvRSoc" class="form-group">
		<label id="elh_Prov_PrvRSoc" for="x_PrvRSoc" class="col-sm-2 control-label ewLabel"><?php echo $Prov->PrvRSoc->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $Prov->PrvRSoc->CellAttributes() ?>>
<?php if ($Prov->CurrentAction <> "F") { ?>
<span id="el_Prov_PrvRSoc">
<input type="text" data-table="Prov" data-field="x_PrvRSoc" name="x_PrvRSoc" id="x_PrvRSoc" size="30" placeholder="<?php echo ew_HtmlEncode($Prov->PrvRSoc->getPlaceHolder()) ?>" value="<?php echo $Prov->PrvRSoc->EditValue ?>"<?php echo $Prov->PrvRSoc->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_Prov_PrvRSoc">
<span<?php echo $Prov->PrvRSoc->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $Prov->PrvRSoc->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="Prov" data-field="x_PrvRSoc" name="x_PrvRSoc" id="x_PrvRSoc" value="<?php echo ew_HtmlEncode($Prov->PrvRSoc->FormValue) ?>">
<?php } ?>
<?php echo $Prov->PrvRSoc->CustomMsg ?></div></div>
	</div>
<?php } ?>
</div>
<div class="form-group">
	<div class="col-sm-offset-2 col-sm-10">
<?php if ($Prov->CurrentAction <> "F") { // Confirm page ?>
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit" onclick="this.form.a_add.value='F';"><?php echo $Language->Phrase("AddBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="button" data-href="<?php echo $Prov_add->getReturnUrl() ?>"><?php echo $Language->Phrase("CancelBtn") ?></button>
<?php } else { ?>
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("ConfirmBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="submit" onclick="this.form.a_add.value='X';"><?php echo $Language->Phrase("CancelBtn") ?></button>
<?php } ?>
	</div>
</div>
</form>
<script type="text/javascript">
fProvadd.Init();
</script>
<?php
$Prov_add->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$Prov_add->Page_Terminate();
?>
