<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "FDetinfo.php" ?>
<?php include_once "Usuainfo.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$FDet_view = NULL; // Initialize page object first

class cFDet_view extends cFDet {

	// Page ID
	var $PageID = 'view';

	// Project ID
	var $ProjectID = "{04439FF7-B43F-460F-8514-F71C8FF9E679}";

	// Table name
	var $TableName = 'FDet';

	// Page object name
	var $PageObjName = 'FDet_view';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Page URLs
	var $AddUrl;
	var $EditUrl;
	var $CopyUrl;
	var $DeleteUrl;
	var $ViewUrl;
	var $ListUrl;

	// Export URLs
	var $ExportPrintUrl;
	var $ExportHtmlUrl;
	var $ExportExcelUrl;
	var $ExportWordUrl;
	var $ExportXmlUrl;
	var $ExportCsvUrl;
	var $ExportPdfUrl;

	// Custom export
	var $ExportExcelCustom = FALSE;
	var $ExportWordCustom = FALSE;
	var $ExportPdfCustom = FALSE;
	var $ExportEmailCustom = FALSE;

	// Update URLs
	var $InlineAddUrl;
	var $InlineCopyUrl;
	var $InlineEditUrl;
	var $GridAddUrl;
	var $GridEditUrl;
	var $MultiDeleteUrl;
	var $MultiUpdateUrl;

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (FDet)
		if (!isset($GLOBALS["FDet"]) || get_class($GLOBALS["FDet"]) == "cFDet") {
			$GLOBALS["FDet"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["FDet"];
		}
		$KeyUrl = "";
		if (@$_GET["FdeCodi"] <> "") {
			$this->RecKey["FdeCodi"] = $_GET["FdeCodi"];
			$KeyUrl .= "&amp;FdeCodi=" . urlencode($this->RecKey["FdeCodi"]);
		}
		if (@$_GET["FcaCodi"] <> "") {
			$this->RecKey["FcaCodi"] = $_GET["FcaCodi"];
			$KeyUrl .= "&amp;FcaCodi=" . urlencode($this->RecKey["FcaCodi"]);
		}
		$this->ExportPrintUrl = $this->PageUrl() . "export=print" . $KeyUrl;
		$this->ExportHtmlUrl = $this->PageUrl() . "export=html" . $KeyUrl;
		$this->ExportExcelUrl = $this->PageUrl() . "export=excel" . $KeyUrl;
		$this->ExportWordUrl = $this->PageUrl() . "export=word" . $KeyUrl;
		$this->ExportXmlUrl = $this->PageUrl() . "export=xml" . $KeyUrl;
		$this->ExportCsvUrl = $this->PageUrl() . "export=csv" . $KeyUrl;
		$this->ExportPdfUrl = $this->PageUrl() . "export=pdf" . $KeyUrl;

		// Table object (Usua)
		if (!isset($GLOBALS['Usua'])) $GLOBALS['Usua'] = new cUsua();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'view', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'FDet', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (Usua)
		if (!isset($UserTable)) {
			$UserTable = new cUsua();
			$UserTableConn = Conn($UserTable->DBID);
		}

		// Export options
		$this->ExportOptions = new cListOptions();
		$this->ExportOptions->Tag = "div";
		$this->ExportOptions->TagClassName = "ewExportOption";

		// Other options
		$this->OtherOptions['action'] = new cListOptions();
		$this->OtherOptions['action']->Tag = "div";
		$this->OtherOptions['action']->TagClassName = "ewActionOption";
		$this->OtherOptions['detail'] = new cListOptions();
		$this->OtherOptions['detail']->Tag = "div";
		$this->OtherOptions['detail']->TagClassName = "ewDetailOption";
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanView()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage($Language->Phrase("NoPermission")); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("FDetlist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $FDet;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($FDet);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $ExportOptions; // Export options
	var $OtherOptions = array(); // Other options
	var $DisplayRecs = 1;
	var $DbMasterFilter;
	var $DbDetailFilter;
	var $StartRec;
	var $StopRec;
	var $TotalRecs = 0;
	var $RecRange = 10;
	var $RecCnt;
	var $RecKey = array();
	var $Recordset;

	//
	// Page main
	//
	function Page_Main() {
		global $Language;
		$sReturnUrl = "";
		$bMatchRecord = FALSE;

		// Set up Breadcrumb
		if ($this->Export == "")
			$this->SetupBreadcrumb();
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET["FdeCodi"] <> "") {
				$this->FdeCodi->setQueryStringValue($_GET["FdeCodi"]);
				$this->RecKey["FdeCodi"] = $this->FdeCodi->QueryStringValue;
			} elseif (@$_POST["FdeCodi"] <> "") {
				$this->FdeCodi->setFormValue($_POST["FdeCodi"]);
				$this->RecKey["FdeCodi"] = $this->FdeCodi->FormValue;
			} else {
				$sReturnUrl = "FDetlist.php"; // Return to list
			}
			if (@$_GET["FcaCodi"] <> "") {
				$this->FcaCodi->setQueryStringValue($_GET["FcaCodi"]);
				$this->RecKey["FcaCodi"] = $this->FcaCodi->QueryStringValue;
			} elseif (@$_POST["FcaCodi"] <> "") {
				$this->FcaCodi->setFormValue($_POST["FcaCodi"]);
				$this->RecKey["FcaCodi"] = $this->FcaCodi->FormValue;
			} else {
				$sReturnUrl = "FDetlist.php"; // Return to list
			}

			// Get action
			$this->CurrentAction = "I"; // Display form
			switch ($this->CurrentAction) {
				case "I": // Get a record to display
					if (!$this->LoadRow()) { // Load record based on key
						if ($this->getSuccessMessage() == "" && $this->getFailureMessage() == "")
							$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record message
						$sReturnUrl = "FDetlist.php"; // No matching record, return to list
					}
			}
		} else {
			$sReturnUrl = "FDetlist.php"; // Not page request, return to list
		}
		if ($sReturnUrl <> "")
			$this->Page_Terminate($sReturnUrl);

		// Render row
		$this->RowType = EW_ROWTYPE_VIEW;
		$this->ResetAttrs();
		$this->RenderRow();
	}

	// Set up other options
	function SetupOtherOptions() {
		global $Language, $Security;
		$options = &$this->OtherOptions;
		$option = &$options["action"];

		// Add
		$item = &$option->Add("add");
		$item->Body = "<a class=\"ewAction ewAdd\" title=\"" . ew_HtmlTitle($Language->Phrase("ViewPageAddLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("ViewPageAddLink")) . "\" href=\"" . ew_HtmlEncode($this->AddUrl) . "\">" . $Language->Phrase("ViewPageAddLink") . "</a>";
		$item->Visible = ($this->AddUrl <> "" && $Security->CanAdd());

		// Edit
		$item = &$option->Add("edit");
		$item->Body = "<a class=\"ewAction ewEdit\" title=\"" . ew_HtmlTitle($Language->Phrase("ViewPageEditLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("ViewPageEditLink")) . "\" href=\"" . ew_HtmlEncode($this->EditUrl) . "\">" . $Language->Phrase("ViewPageEditLink") . "</a>";
		$item->Visible = ($this->EditUrl <> "" && $Security->CanEdit());

		// Copy
		$item = &$option->Add("copy");
		$item->Body = "<a class=\"ewAction ewCopy\" title=\"" . ew_HtmlTitle($Language->Phrase("ViewPageCopyLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("ViewPageCopyLink")) . "\" href=\"" . ew_HtmlEncode($this->CopyUrl) . "\">" . $Language->Phrase("ViewPageCopyLink") . "</a>";
		$item->Visible = ($this->CopyUrl <> "" && $Security->CanAdd());

		// Delete
		$item = &$option->Add("delete");
		$item->Body = "<a class=\"ewAction ewDelete\" title=\"" . ew_HtmlTitle($Language->Phrase("ViewPageDeleteLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("ViewPageDeleteLink")) . "\" href=\"" . ew_HtmlEncode($this->DeleteUrl) . "\">" . $Language->Phrase("ViewPageDeleteLink") . "</a>";
		$item->Visible = ($this->DeleteUrl <> "" && $Security->CanDelete());

		// Set up action default
		$option = &$options["action"];
		$option->DropDownButtonPhrase = $Language->Phrase("ButtonActions");
		$option->UseImageAndText = TRUE;
		$option->UseDropDownButton = FALSE;
		$option->UseButtonGroup = TRUE;
		$item = &$option->Add($option->GroupOptionName);
		$item->Body = "";
		$item->Visible = FALSE;
	}

	// Set up starting record parameters
	function SetUpStartRec() {
		if ($this->DisplayRecs == 0)
			return;
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET[EW_TABLE_START_REC] <> "") { // Check for "start" parameter
				$this->StartRec = $_GET[EW_TABLE_START_REC];
				$this->setStartRecordNumber($this->StartRec);
			} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
				$PageNo = $_GET[EW_TABLE_PAGE_NO];
				if (is_numeric($PageNo)) {
					$this->StartRec = ($PageNo-1)*$this->DisplayRecs+1;
					if ($this->StartRec <= 0) {
						$this->StartRec = 1;
					} elseif ($this->StartRec >= intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1) {
						$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1;
					}
					$this->setStartRecordNumber($this->StartRec);
				}
			}
		}
		$this->StartRec = $this->getStartRecordNumber();

		// Check if correct start record counter
		if (!is_numeric($this->StartRec) || $this->StartRec == "") { // Avoid invalid start record counter
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} elseif (intval($this->StartRec) > intval($this->TotalRecs)) { // Avoid starting record > total records
			$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to last page first record
			$this->setStartRecordNumber($this->StartRec);
		} elseif (($this->StartRec-1) % $this->DisplayRecs <> 0) {
			$this->StartRec = intval(($this->StartRec-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to page boundary
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->FdeCodi->setDbValue($rs->fields('FdeCodi'));
		$this->FcaCodi->setDbValue($rs->fields('FcaCodi'));
		$this->FdeCant->setDbValue($rs->fields('FdeCant'));
		$this->FdePrec->setDbValue($rs->fields('FdePrec'));
		$this->FdeDesc->setDbValue($rs->fields('FdeDesc'));
		$this->FdeUsua->setDbValue($rs->fields('FdeUsua'));
		$this->FdeFCre->setDbValue($rs->fields('FdeFCre'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->FdeCodi->DbValue = $row['FdeCodi'];
		$this->FcaCodi->DbValue = $row['FcaCodi'];
		$this->FdeCant->DbValue = $row['FdeCant'];
		$this->FdePrec->DbValue = $row['FdePrec'];
		$this->FdeDesc->DbValue = $row['FdeDesc'];
		$this->FdeUsua->DbValue = $row['FdeUsua'];
		$this->FdeFCre->DbValue = $row['FdeFCre'];
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		$this->AddUrl = $this->GetAddUrl();
		$this->EditUrl = $this->GetEditUrl();
		$this->CopyUrl = $this->GetCopyUrl();
		$this->DeleteUrl = $this->GetDeleteUrl();
		$this->ListUrl = $this->GetListUrl();
		$this->SetupOtherOptions();

		// Convert decimal values if posted back
		if ($this->FdePrec->FormValue == $this->FdePrec->CurrentValue && is_numeric(ew_StrToFloat($this->FdePrec->CurrentValue)))
			$this->FdePrec->CurrentValue = ew_StrToFloat($this->FdePrec->CurrentValue);

		// Call Row_Rendering event
		$this->Row_Rendering();

		// Common render codes for all row types
		// FdeCodi
		// FcaCodi
		// FdeCant
		// FdePrec
		// FdeDesc
		// FdeUsua
		// FdeFCre

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// FdeCodi
		$this->FdeCodi->ViewValue = $this->FdeCodi->CurrentValue;
		$this->FdeCodi->ViewCustomAttributes = "";

		// FcaCodi
		if (strval($this->FcaCodi->CurrentValue) <> "") {
			$sFilterWrk = "\"FcaCodi\"" . ew_SearchString("=", $this->FcaCodi->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT \"FcaCodi\", \"FcaCodi\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"FCab\"";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->FcaCodi, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->FcaCodi->ViewValue = $this->FcaCodi->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->FcaCodi->ViewValue = $this->FcaCodi->CurrentValue;
			}
		} else {
			$this->FcaCodi->ViewValue = NULL;
		}
		$this->FcaCodi->ViewCustomAttributes = "";

		// FdeCant
		$this->FdeCant->ViewValue = $this->FdeCant->CurrentValue;
		$this->FdeCant->ViewCustomAttributes = "";

		// FdePrec
		$this->FdePrec->ViewValue = $this->FdePrec->CurrentValue;
		$this->FdePrec->ViewCustomAttributes = "";

		// FdeDesc
		$this->FdeDesc->ViewValue = $this->FdeDesc->CurrentValue;
		$this->FdeDesc->ViewCustomAttributes = "";

		// FdeUsua
		$this->FdeUsua->ViewValue = $this->FdeUsua->CurrentValue;
		$this->FdeUsua->ViewCustomAttributes = "";

		// FdeFCre
		$this->FdeFCre->ViewValue = $this->FdeFCre->CurrentValue;
		$this->FdeFCre->ViewValue = ew_FormatDateTime($this->FdeFCre->ViewValue, 7);
		$this->FdeFCre->ViewCustomAttributes = "";

			// FdeCodi
			$this->FdeCodi->LinkCustomAttributes = "";
			$this->FdeCodi->HrefValue = "";
			$this->FdeCodi->TooltipValue = "";

			// FcaCodi
			$this->FcaCodi->LinkCustomAttributes = "";
			$this->FcaCodi->HrefValue = "";
			$this->FcaCodi->TooltipValue = "";

			// FdeCant
			$this->FdeCant->LinkCustomAttributes = "";
			$this->FdeCant->HrefValue = "";
			$this->FdeCant->TooltipValue = "";

			// FdePrec
			$this->FdePrec->LinkCustomAttributes = "";
			$this->FdePrec->HrefValue = "";
			$this->FdePrec->TooltipValue = "";

			// FdeDesc
			$this->FdeDesc->LinkCustomAttributes = "";
			$this->FdeDesc->HrefValue = "";
			$this->FdeDesc->TooltipValue = "";

			// FdeUsua
			$this->FdeUsua->LinkCustomAttributes = "";
			$this->FdeUsua->HrefValue = "";
			$this->FdeUsua->TooltipValue = "";

			// FdeFCre
			$this->FdeFCre->LinkCustomAttributes = "";
			$this->FdeFCre->HrefValue = "";
			$this->FdeFCre->TooltipValue = "";
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, "FDetlist.php", "", $this->TableVar, TRUE);
		$PageId = "view";
		$Breadcrumb->Add("view", $PageId, $url);
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Page Exporting event
	// $this->ExportDoc = export document object
	function Page_Exporting() {

		//$this->ExportDoc->Text = "my header"; // Export header
		//return FALSE; // Return FALSE to skip default export and use Row_Export event

		return TRUE; // Return TRUE to use default export and skip Row_Export event
	}

	// Row Export event
	// $this->ExportDoc = export document object
	function Row_Export($rs) {

	    //$this->ExportDoc->Text .= "my content"; // Build HTML with field value: $rs["MyField"] or $this->MyField->ViewValue
	}

	// Page Exported event
	// $this->ExportDoc = export document object
	function Page_Exported() {

		//$this->ExportDoc->Text .= "my footer"; // Export footer
		//echo $this->ExportDoc->Text;

	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($FDet_view)) $FDet_view = new cFDet_view();

// Page init
$FDet_view->Page_Init();

// Page main
$FDet_view->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$FDet_view->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "view";
var CurrentForm = fFDetview = new ew_Form("fFDetview", "view");

// Form_CustomValidate event
fFDetview.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fFDetview.ValidateRequired = true;
<?php } else { ?>
fFDetview.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
fFDetview.Lists["x_FcaCodi"] = {"LinkField":"x_FcaCodi","Ajax":true,"AutoFill":false,"DisplayFields":["x_FcaCodi","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};

// Form object for search
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php $FDet_view->ExportOptions->Render("body") ?>
<?php
	foreach ($FDet_view->OtherOptions as &$option)
		$option->Render("body");
?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php $FDet_view->ShowPageHeader(); ?>
<?php
$FDet_view->ShowMessage();
?>
<form name="fFDetview" id="fFDetview" class="form-inline ewForm ewViewForm" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($FDet_view->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $FDet_view->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="FDet">
<table class="table table-bordered table-striped ewViewTable">
<?php if ($FDet->FdeCodi->Visible) { // FdeCodi ?>
	<tr id="r_FdeCodi">
		<td><span id="elh_FDet_FdeCodi"><?php echo $FDet->FdeCodi->FldCaption() ?></span></td>
		<td data-name="FdeCodi"<?php echo $FDet->FdeCodi->CellAttributes() ?>>
<span id="el_FDet_FdeCodi">
<span<?php echo $FDet->FdeCodi->ViewAttributes() ?>>
<?php echo $FDet->FdeCodi->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($FDet->FcaCodi->Visible) { // FcaCodi ?>
	<tr id="r_FcaCodi">
		<td><span id="elh_FDet_FcaCodi"><?php echo $FDet->FcaCodi->FldCaption() ?></span></td>
		<td data-name="FcaCodi"<?php echo $FDet->FcaCodi->CellAttributes() ?>>
<span id="el_FDet_FcaCodi">
<span<?php echo $FDet->FcaCodi->ViewAttributes() ?>>
<?php echo $FDet->FcaCodi->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($FDet->FdeCant->Visible) { // FdeCant ?>
	<tr id="r_FdeCant">
		<td><span id="elh_FDet_FdeCant"><?php echo $FDet->FdeCant->FldCaption() ?></span></td>
		<td data-name="FdeCant"<?php echo $FDet->FdeCant->CellAttributes() ?>>
<span id="el_FDet_FdeCant">
<span<?php echo $FDet->FdeCant->ViewAttributes() ?>>
<?php echo $FDet->FdeCant->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($FDet->FdePrec->Visible) { // FdePrec ?>
	<tr id="r_FdePrec">
		<td><span id="elh_FDet_FdePrec"><?php echo $FDet->FdePrec->FldCaption() ?></span></td>
		<td data-name="FdePrec"<?php echo $FDet->FdePrec->CellAttributes() ?>>
<span id="el_FDet_FdePrec">
<span<?php echo $FDet->FdePrec->ViewAttributes() ?>>
<?php echo $FDet->FdePrec->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($FDet->FdeDesc->Visible) { // FdeDesc ?>
	<tr id="r_FdeDesc">
		<td><span id="elh_FDet_FdeDesc"><?php echo $FDet->FdeDesc->FldCaption() ?></span></td>
		<td data-name="FdeDesc"<?php echo $FDet->FdeDesc->CellAttributes() ?>>
<span id="el_FDet_FdeDesc">
<span<?php echo $FDet->FdeDesc->ViewAttributes() ?>>
<?php echo $FDet->FdeDesc->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($FDet->FdeUsua->Visible) { // FdeUsua ?>
	<tr id="r_FdeUsua">
		<td><span id="elh_FDet_FdeUsua"><?php echo $FDet->FdeUsua->FldCaption() ?></span></td>
		<td data-name="FdeUsua"<?php echo $FDet->FdeUsua->CellAttributes() ?>>
<span id="el_FDet_FdeUsua">
<span<?php echo $FDet->FdeUsua->ViewAttributes() ?>>
<?php echo $FDet->FdeUsua->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($FDet->FdeFCre->Visible) { // FdeFCre ?>
	<tr id="r_FdeFCre">
		<td><span id="elh_FDet_FdeFCre"><?php echo $FDet->FdeFCre->FldCaption() ?></span></td>
		<td data-name="FdeFCre"<?php echo $FDet->FdeFCre->CellAttributes() ?>>
<span id="el_FDet_FdeFCre">
<span<?php echo $FDet->FdeFCre->ViewAttributes() ?>>
<?php echo $FDet->FdeFCre->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
</table>
</form>
<script type="text/javascript">
fFDetview.Init();
</script>
<?php
$FDet_view->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$FDet_view->Page_Terminate();
?>
