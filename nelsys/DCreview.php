<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "DCreinfo.php" ?>
<?php include_once "Usuainfo.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$DCre_view = NULL; // Initialize page object first

class cDCre_view extends cDCre {

	// Page ID
	var $PageID = 'view';

	// Project ID
	var $ProjectID = "{04439FF7-B43F-460F-8514-F71C8FF9E679}";

	// Table name
	var $TableName = 'DCre';

	// Page object name
	var $PageObjName = 'DCre_view';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Page URLs
	var $AddUrl;
	var $EditUrl;
	var $CopyUrl;
	var $DeleteUrl;
	var $ViewUrl;
	var $ListUrl;

	// Export URLs
	var $ExportPrintUrl;
	var $ExportHtmlUrl;
	var $ExportExcelUrl;
	var $ExportWordUrl;
	var $ExportXmlUrl;
	var $ExportCsvUrl;
	var $ExportPdfUrl;

	// Custom export
	var $ExportExcelCustom = FALSE;
	var $ExportWordCustom = FALSE;
	var $ExportPdfCustom = FALSE;
	var $ExportEmailCustom = FALSE;

	// Update URLs
	var $InlineAddUrl;
	var $InlineCopyUrl;
	var $InlineEditUrl;
	var $GridAddUrl;
	var $GridEditUrl;
	var $MultiDeleteUrl;
	var $MultiUpdateUrl;

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (DCre)
		if (!isset($GLOBALS["DCre"]) || get_class($GLOBALS["DCre"]) == "cDCre") {
			$GLOBALS["DCre"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["DCre"];
		}
		$KeyUrl = "";
		if (@$_GET["DcrCodi"] <> "") {
			$this->RecKey["DcrCodi"] = $_GET["DcrCodi"];
			$KeyUrl .= "&amp;DcrCodi=" . urlencode($this->RecKey["DcrCodi"]);
		}
		$this->ExportPrintUrl = $this->PageUrl() . "export=print" . $KeyUrl;
		$this->ExportHtmlUrl = $this->PageUrl() . "export=html" . $KeyUrl;
		$this->ExportExcelUrl = $this->PageUrl() . "export=excel" . $KeyUrl;
		$this->ExportWordUrl = $this->PageUrl() . "export=word" . $KeyUrl;
		$this->ExportXmlUrl = $this->PageUrl() . "export=xml" . $KeyUrl;
		$this->ExportCsvUrl = $this->PageUrl() . "export=csv" . $KeyUrl;
		$this->ExportPdfUrl = $this->PageUrl() . "export=pdf" . $KeyUrl;

		// Table object (Usua)
		if (!isset($GLOBALS['Usua'])) $GLOBALS['Usua'] = new cUsua();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'view', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'DCre', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (Usua)
		if (!isset($UserTable)) {
			$UserTable = new cUsua();
			$UserTableConn = Conn($UserTable->DBID);
		}

		// Export options
		$this->ExportOptions = new cListOptions();
		$this->ExportOptions->Tag = "div";
		$this->ExportOptions->TagClassName = "ewExportOption";

		// Other options
		$this->OtherOptions['action'] = new cListOptions();
		$this->OtherOptions['action']->Tag = "div";
		$this->OtherOptions['action']->TagClassName = "ewActionOption";
		$this->OtherOptions['detail'] = new cListOptions();
		$this->OtherOptions['detail']->Tag = "div";
		$this->OtherOptions['detail']->TagClassName = "ewDetailOption";
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanView()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage($Language->Phrase("NoPermission")); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("DCrelist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $DCre;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($DCre);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $ExportOptions; // Export options
	var $OtherOptions = array(); // Other options
	var $DisplayRecs = 1;
	var $DbMasterFilter;
	var $DbDetailFilter;
	var $StartRec;
	var $StopRec;
	var $TotalRecs = 0;
	var $RecRange = 10;
	var $RecCnt;
	var $RecKey = array();
	var $Recordset;

	//
	// Page main
	//
	function Page_Main() {
		global $Language;
		$sReturnUrl = "";
		$bMatchRecord = FALSE;

		// Set up Breadcrumb
		if ($this->Export == "")
			$this->SetupBreadcrumb();
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET["DcrCodi"] <> "") {
				$this->DcrCodi->setQueryStringValue($_GET["DcrCodi"]);
				$this->RecKey["DcrCodi"] = $this->DcrCodi->QueryStringValue;
			} elseif (@$_POST["DcrCodi"] <> "") {
				$this->DcrCodi->setFormValue($_POST["DcrCodi"]);
				$this->RecKey["DcrCodi"] = $this->DcrCodi->FormValue;
			} else {
				$sReturnUrl = "DCrelist.php"; // Return to list
			}

			// Get action
			$this->CurrentAction = "I"; // Display form
			switch ($this->CurrentAction) {
				case "I": // Get a record to display
					if (!$this->LoadRow()) { // Load record based on key
						if ($this->getSuccessMessage() == "" && $this->getFailureMessage() == "")
							$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record message
						$sReturnUrl = "DCrelist.php"; // No matching record, return to list
					}
			}
		} else {
			$sReturnUrl = "DCrelist.php"; // Not page request, return to list
		}
		if ($sReturnUrl <> "")
			$this->Page_Terminate($sReturnUrl);

		// Render row
		$this->RowType = EW_ROWTYPE_VIEW;
		$this->ResetAttrs();
		$this->RenderRow();
	}

	// Set up other options
	function SetupOtherOptions() {
		global $Language, $Security;
		$options = &$this->OtherOptions;
		$option = &$options["action"];

		// Add
		$item = &$option->Add("add");
		$item->Body = "<a class=\"ewAction ewAdd\" title=\"" . ew_HtmlTitle($Language->Phrase("ViewPageAddLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("ViewPageAddLink")) . "\" href=\"" . ew_HtmlEncode($this->AddUrl) . "\">" . $Language->Phrase("ViewPageAddLink") . "</a>";
		$item->Visible = ($this->AddUrl <> "" && $Security->CanAdd());

		// Edit
		$item = &$option->Add("edit");
		$item->Body = "<a class=\"ewAction ewEdit\" title=\"" . ew_HtmlTitle($Language->Phrase("ViewPageEditLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("ViewPageEditLink")) . "\" href=\"" . ew_HtmlEncode($this->EditUrl) . "\">" . $Language->Phrase("ViewPageEditLink") . "</a>";
		$item->Visible = ($this->EditUrl <> "" && $Security->CanEdit());

		// Copy
		$item = &$option->Add("copy");
		$item->Body = "<a class=\"ewAction ewCopy\" title=\"" . ew_HtmlTitle($Language->Phrase("ViewPageCopyLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("ViewPageCopyLink")) . "\" href=\"" . ew_HtmlEncode($this->CopyUrl) . "\">" . $Language->Phrase("ViewPageCopyLink") . "</a>";
		$item->Visible = ($this->CopyUrl <> "" && $Security->CanAdd());

		// Delete
		$item = &$option->Add("delete");
		$item->Body = "<a class=\"ewAction ewDelete\" title=\"" . ew_HtmlTitle($Language->Phrase("ViewPageDeleteLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("ViewPageDeleteLink")) . "\" href=\"" . ew_HtmlEncode($this->DeleteUrl) . "\">" . $Language->Phrase("ViewPageDeleteLink") . "</a>";
		$item->Visible = ($this->DeleteUrl <> "" && $Security->CanDelete());

		// Set up action default
		$option = &$options["action"];
		$option->DropDownButtonPhrase = $Language->Phrase("ButtonActions");
		$option->UseImageAndText = TRUE;
		$option->UseDropDownButton = FALSE;
		$option->UseButtonGroup = TRUE;
		$item = &$option->Add($option->GroupOptionName);
		$item->Body = "";
		$item->Visible = FALSE;
	}

	// Set up starting record parameters
	function SetUpStartRec() {
		if ($this->DisplayRecs == 0)
			return;
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET[EW_TABLE_START_REC] <> "") { // Check for "start" parameter
				$this->StartRec = $_GET[EW_TABLE_START_REC];
				$this->setStartRecordNumber($this->StartRec);
			} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
				$PageNo = $_GET[EW_TABLE_PAGE_NO];
				if (is_numeric($PageNo)) {
					$this->StartRec = ($PageNo-1)*$this->DisplayRecs+1;
					if ($this->StartRec <= 0) {
						$this->StartRec = 1;
					} elseif ($this->StartRec >= intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1) {
						$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1;
					}
					$this->setStartRecordNumber($this->StartRec);
				}
			}
		}
		$this->StartRec = $this->getStartRecordNumber();

		// Check if correct start record counter
		if (!is_numeric($this->StartRec) || $this->StartRec == "") { // Avoid invalid start record counter
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} elseif (intval($this->StartRec) > intval($this->TotalRecs)) { // Avoid starting record > total records
			$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to last page first record
			$this->setStartRecordNumber($this->StartRec);
		} elseif (($this->StartRec-1) % $this->DisplayRecs <> 0) {
			$this->StartRec = intval(($this->StartRec-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to page boundary
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->DcrCodi->setDbValue($rs->fields('DcrCodi'));
		$this->VcrCodi->setDbValue($rs->fields('VcrCodi'));
		$this->DcrFech->setDbValue($rs->fields('DcrFech'));
		$this->DcrMont->setDbValue($rs->fields('DcrMont'));
		$this->DcrSCuo->setDbValue($rs->fields('DcrSCuo'));
		$this->DcrNCuo->setDbValue($rs->fields('DcrNCuo'));
		$this->DcrEsta->setDbValue($rs->fields('DcrEsta'));
		$this->DcrUsu->setDbValue($rs->fields('DcrUsu'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->DcrCodi->DbValue = $row['DcrCodi'];
		$this->VcrCodi->DbValue = $row['VcrCodi'];
		$this->DcrFech->DbValue = $row['DcrFech'];
		$this->DcrMont->DbValue = $row['DcrMont'];
		$this->DcrSCuo->DbValue = $row['DcrSCuo'];
		$this->DcrNCuo->DbValue = $row['DcrNCuo'];
		$this->DcrEsta->DbValue = $row['DcrEsta'];
		$this->DcrUsu->DbValue = $row['DcrUsu'];
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		$this->AddUrl = $this->GetAddUrl();
		$this->EditUrl = $this->GetEditUrl();
		$this->CopyUrl = $this->GetCopyUrl();
		$this->DeleteUrl = $this->GetDeleteUrl();
		$this->ListUrl = $this->GetListUrl();
		$this->SetupOtherOptions();

		// Convert decimal values if posted back
		if ($this->DcrMont->FormValue == $this->DcrMont->CurrentValue && is_numeric(ew_StrToFloat($this->DcrMont->CurrentValue)))
			$this->DcrMont->CurrentValue = ew_StrToFloat($this->DcrMont->CurrentValue);

		// Convert decimal values if posted back
		if ($this->DcrSCuo->FormValue == $this->DcrSCuo->CurrentValue && is_numeric(ew_StrToFloat($this->DcrSCuo->CurrentValue)))
			$this->DcrSCuo->CurrentValue = ew_StrToFloat($this->DcrSCuo->CurrentValue);

		// Call Row_Rendering event
		$this->Row_Rendering();

		// Common render codes for all row types
		// DcrCodi
		// VcrCodi
		// DcrFech
		// DcrMont
		// DcrSCuo
		// DcrNCuo
		// DcrEsta
		// DcrUsu

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// DcrCodi
		$this->DcrCodi->ViewValue = $this->DcrCodi->CurrentValue;
		$this->DcrCodi->ViewCustomAttributes = "";

		// VcrCodi
		if (strval($this->VcrCodi->CurrentValue) <> "") {
			$sFilterWrk = "\"VcrCodi\"" . ew_SearchString("=", $this->VcrCodi->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT \"VcrCodi\", \"VcrCodi\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"VCre\"";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->VcrCodi, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->VcrCodi->ViewValue = $this->VcrCodi->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->VcrCodi->ViewValue = $this->VcrCodi->CurrentValue;
			}
		} else {
			$this->VcrCodi->ViewValue = NULL;
		}
		$this->VcrCodi->ViewCustomAttributes = "";

		// DcrFech
		$this->DcrFech->ViewValue = $this->DcrFech->CurrentValue;
		$this->DcrFech->ViewValue = ew_FormatDateTime($this->DcrFech->ViewValue, 7);
		$this->DcrFech->ViewCustomAttributes = "";

		// DcrMont
		$this->DcrMont->ViewValue = $this->DcrMont->CurrentValue;
		$this->DcrMont->ViewCustomAttributes = "";

		// DcrSCuo
		$this->DcrSCuo->ViewValue = $this->DcrSCuo->CurrentValue;
		$this->DcrSCuo->ViewCustomAttributes = "";

		// DcrNCuo
		$this->DcrNCuo->ViewValue = $this->DcrNCuo->CurrentValue;
		$this->DcrNCuo->ViewCustomAttributes = "";

		// DcrEsta
		$this->DcrEsta->ViewValue = $this->DcrEsta->CurrentValue;
		$this->DcrEsta->ViewCustomAttributes = "";

		// DcrUsu
		$this->DcrUsu->ViewValue = $this->DcrUsu->CurrentValue;
		$this->DcrUsu->ViewCustomAttributes = "";

			// DcrCodi
			$this->DcrCodi->LinkCustomAttributes = "";
			$this->DcrCodi->HrefValue = "";
			$this->DcrCodi->TooltipValue = "";

			// VcrCodi
			$this->VcrCodi->LinkCustomAttributes = "";
			$this->VcrCodi->HrefValue = "";
			$this->VcrCodi->TooltipValue = "";

			// DcrFech
			$this->DcrFech->LinkCustomAttributes = "";
			$this->DcrFech->HrefValue = "";
			$this->DcrFech->TooltipValue = "";

			// DcrMont
			$this->DcrMont->LinkCustomAttributes = "";
			$this->DcrMont->HrefValue = "";
			$this->DcrMont->TooltipValue = "";

			// DcrSCuo
			$this->DcrSCuo->LinkCustomAttributes = "";
			$this->DcrSCuo->HrefValue = "";
			$this->DcrSCuo->TooltipValue = "";

			// DcrNCuo
			$this->DcrNCuo->LinkCustomAttributes = "";
			$this->DcrNCuo->HrefValue = "";
			$this->DcrNCuo->TooltipValue = "";

			// DcrEsta
			$this->DcrEsta->LinkCustomAttributes = "";
			$this->DcrEsta->HrefValue = "";
			$this->DcrEsta->TooltipValue = "";

			// DcrUsu
			$this->DcrUsu->LinkCustomAttributes = "";
			$this->DcrUsu->HrefValue = "";
			$this->DcrUsu->TooltipValue = "";
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, "DCrelist.php", "", $this->TableVar, TRUE);
		$PageId = "view";
		$Breadcrumb->Add("view", $PageId, $url);
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Page Exporting event
	// $this->ExportDoc = export document object
	function Page_Exporting() {

		//$this->ExportDoc->Text = "my header"; // Export header
		//return FALSE; // Return FALSE to skip default export and use Row_Export event

		return TRUE; // Return TRUE to use default export and skip Row_Export event
	}

	// Row Export event
	// $this->ExportDoc = export document object
	function Row_Export($rs) {

	    //$this->ExportDoc->Text .= "my content"; // Build HTML with field value: $rs["MyField"] or $this->MyField->ViewValue
	}

	// Page Exported event
	// $this->ExportDoc = export document object
	function Page_Exported() {

		//$this->ExportDoc->Text .= "my footer"; // Export footer
		//echo $this->ExportDoc->Text;

	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($DCre_view)) $DCre_view = new cDCre_view();

// Page init
$DCre_view->Page_Init();

// Page main
$DCre_view->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$DCre_view->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "view";
var CurrentForm = fDCreview = new ew_Form("fDCreview", "view");

// Form_CustomValidate event
fDCreview.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fDCreview.ValidateRequired = true;
<?php } else { ?>
fDCreview.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
fDCreview.Lists["x_VcrCodi"] = {"LinkField":"x_VcrCodi","Ajax":true,"AutoFill":false,"DisplayFields":["x_VcrCodi","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};

// Form object for search
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php $DCre_view->ExportOptions->Render("body") ?>
<?php
	foreach ($DCre_view->OtherOptions as &$option)
		$option->Render("body");
?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php $DCre_view->ShowPageHeader(); ?>
<?php
$DCre_view->ShowMessage();
?>
<form name="fDCreview" id="fDCreview" class="form-inline ewForm ewViewForm" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($DCre_view->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $DCre_view->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="DCre">
<table class="table table-bordered table-striped ewViewTable">
<?php if ($DCre->DcrCodi->Visible) { // DcrCodi ?>
	<tr id="r_DcrCodi">
		<td><span id="elh_DCre_DcrCodi"><?php echo $DCre->DcrCodi->FldCaption() ?></span></td>
		<td data-name="DcrCodi"<?php echo $DCre->DcrCodi->CellAttributes() ?>>
<span id="el_DCre_DcrCodi">
<span<?php echo $DCre->DcrCodi->ViewAttributes() ?>>
<?php echo $DCre->DcrCodi->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($DCre->VcrCodi->Visible) { // VcrCodi ?>
	<tr id="r_VcrCodi">
		<td><span id="elh_DCre_VcrCodi"><?php echo $DCre->VcrCodi->FldCaption() ?></span></td>
		<td data-name="VcrCodi"<?php echo $DCre->VcrCodi->CellAttributes() ?>>
<span id="el_DCre_VcrCodi">
<span<?php echo $DCre->VcrCodi->ViewAttributes() ?>>
<?php echo $DCre->VcrCodi->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($DCre->DcrFech->Visible) { // DcrFech ?>
	<tr id="r_DcrFech">
		<td><span id="elh_DCre_DcrFech"><?php echo $DCre->DcrFech->FldCaption() ?></span></td>
		<td data-name="DcrFech"<?php echo $DCre->DcrFech->CellAttributes() ?>>
<span id="el_DCre_DcrFech">
<span<?php echo $DCre->DcrFech->ViewAttributes() ?>>
<?php echo $DCre->DcrFech->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($DCre->DcrMont->Visible) { // DcrMont ?>
	<tr id="r_DcrMont">
		<td><span id="elh_DCre_DcrMont"><?php echo $DCre->DcrMont->FldCaption() ?></span></td>
		<td data-name="DcrMont"<?php echo $DCre->DcrMont->CellAttributes() ?>>
<span id="el_DCre_DcrMont">
<span<?php echo $DCre->DcrMont->ViewAttributes() ?>>
<?php echo $DCre->DcrMont->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($DCre->DcrSCuo->Visible) { // DcrSCuo ?>
	<tr id="r_DcrSCuo">
		<td><span id="elh_DCre_DcrSCuo"><?php echo $DCre->DcrSCuo->FldCaption() ?></span></td>
		<td data-name="DcrSCuo"<?php echo $DCre->DcrSCuo->CellAttributes() ?>>
<span id="el_DCre_DcrSCuo">
<span<?php echo $DCre->DcrSCuo->ViewAttributes() ?>>
<?php echo $DCre->DcrSCuo->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($DCre->DcrNCuo->Visible) { // DcrNCuo ?>
	<tr id="r_DcrNCuo">
		<td><span id="elh_DCre_DcrNCuo"><?php echo $DCre->DcrNCuo->FldCaption() ?></span></td>
		<td data-name="DcrNCuo"<?php echo $DCre->DcrNCuo->CellAttributes() ?>>
<span id="el_DCre_DcrNCuo">
<span<?php echo $DCre->DcrNCuo->ViewAttributes() ?>>
<?php echo $DCre->DcrNCuo->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($DCre->DcrEsta->Visible) { // DcrEsta ?>
	<tr id="r_DcrEsta">
		<td><span id="elh_DCre_DcrEsta"><?php echo $DCre->DcrEsta->FldCaption() ?></span></td>
		<td data-name="DcrEsta"<?php echo $DCre->DcrEsta->CellAttributes() ?>>
<span id="el_DCre_DcrEsta">
<span<?php echo $DCre->DcrEsta->ViewAttributes() ?>>
<?php echo $DCre->DcrEsta->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($DCre->DcrUsu->Visible) { // DcrUsu ?>
	<tr id="r_DcrUsu">
		<td><span id="elh_DCre_DcrUsu"><?php echo $DCre->DcrUsu->FldCaption() ?></span></td>
		<td data-name="DcrUsu"<?php echo $DCre->DcrUsu->CellAttributes() ?>>
<span id="el_DCre_DcrUsu">
<span<?php echo $DCre->DcrUsu->ViewAttributes() ?>>
<?php echo $DCre->DcrUsu->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
</table>
</form>
<script type="text/javascript">
fDCreview.Init();
</script>
<?php
$DCre_view->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$DCre_view->Page_Terminate();
?>
