<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "CConinfo.php" ?>
<?php include_once "Usuainfo.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$CCon_delete = NULL; // Initialize page object first

class cCCon_delete extends cCCon {

	// Page ID
	var $PageID = 'delete';

	// Project ID
	var $ProjectID = "{04439FF7-B43F-460F-8514-F71C8FF9E679}";

	// Table name
	var $TableName = 'CCon';

	// Page object name
	var $PageObjName = 'CCon_delete';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (CCon)
		if (!isset($GLOBALS["CCon"]) || get_class($GLOBALS["CCon"]) == "cCCon") {
			$GLOBALS["CCon"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["CCon"];
		}

		// Table object (Usua)
		if (!isset($GLOBALS['Usua'])) $GLOBALS['Usua'] = new cUsua();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'delete', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'CCon', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (Usua)
		if (!isset($UserTable)) {
			$UserTable = new cUsua();
			$UserTableConn = Conn($UserTable->DBID);
		}
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanDelete()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage($Language->Phrase("NoPermission")); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("CConlist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action
		$this->CcoCodi->Visible = !$this->IsAdd() && !$this->IsCopy() && !$this->IsGridAdd();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $CCon;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($CCon);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $DbMasterFilter = "";
	var $DbDetailFilter = "";
	var $StartRec;
	var $TotalRecs = 0;
	var $RecCnt;
	var $RecKeys = array();
	var $Recordset;
	var $StartRowCnt = 1;
	var $RowCnt = 0;

	//
	// Page main
	//
	function Page_Main() {
		global $Language;

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Load key parameters
		$this->RecKeys = $this->GetRecordKeys(); // Load record keys
		$sFilter = $this->GetKeyFilter();
		if ($sFilter == "")
			$this->Page_Terminate("CConlist.php"); // Prevent SQL injection, return to list

		// Set up filter (SQL WHHERE clause) and get return SQL
		// SQL constructor in CCon class, CConinfo.php

		$this->CurrentFilter = $sFilter;

		// Get action
		if (@$_POST["a_delete"] <> "") {
			$this->CurrentAction = $_POST["a_delete"];
		} else {
			$this->CurrentAction = "I"; // Display record
		}
		switch ($this->CurrentAction) {
			case "D": // Delete
				$this->SendEmail = TRUE; // Send email on delete success
				if ($this->DeleteRows()) { // Delete rows
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage($Language->Phrase("DeleteSuccess")); // Set up success message
					$this->Page_Terminate($this->getReturnUrl()); // Return to caller
				}
		}
	}

	// Load recordset
	function LoadRecordset($offset = -1, $rowcnt = -1) {

		// Load List page SQL
		$sSql = $this->SelectSQL();
		$conn = &$this->Connection();

		// Load recordset
		$dbtype = ew_GetConnectionType($this->DBID);
		if ($this->UseSelectLimit) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			if ($dbtype == "MSSQL") {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset, array("_hasOrderBy" => trim($this->getOrderBy()) || trim($this->getSessionOrderBy())));
			} else {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset);
			}
			$conn->raiseErrorFn = '';
		} else {
			$rs = ew_LoadRecordset($sSql, $conn);
		}

		// Call Recordset Selected event
		$this->Recordset_Selected($rs);
		return $rs;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->CcoCodi->setDbValue($rs->fields('CcoCodi'));
		$this->VcoCodi->setDbValue($rs->fields('VcoCodi'));
		$this->CcoFPag->setDbValue($rs->fields('CcoFPag'));
		$this->CcoMont->setDbValue($rs->fields('CcoMont'));
		$this->CcoEsta->setDbValue($rs->fields('CcoEsta'));
		$this->CcoAnho->setDbValue($rs->fields('CcoAnho'));
		$this->CcoMes->setDbValue($rs->fields('CcoMes'));
		$this->CcoConc->setDbValue($rs->fields('CcoConc'));
		$this->CcoUsua->setDbValue($rs->fields('CcoUsua'));
		$this->CcoFCre->setDbValue($rs->fields('CcoFCre'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->CcoCodi->DbValue = $row['CcoCodi'];
		$this->VcoCodi->DbValue = $row['VcoCodi'];
		$this->CcoFPag->DbValue = $row['CcoFPag'];
		$this->CcoMont->DbValue = $row['CcoMont'];
		$this->CcoEsta->DbValue = $row['CcoEsta'];
		$this->CcoAnho->DbValue = $row['CcoAnho'];
		$this->CcoMes->DbValue = $row['CcoMes'];
		$this->CcoConc->DbValue = $row['CcoConc'];
		$this->CcoUsua->DbValue = $row['CcoUsua'];
		$this->CcoFCre->DbValue = $row['CcoFCre'];
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		// Convert decimal values if posted back

		if ($this->CcoMont->FormValue == $this->CcoMont->CurrentValue && is_numeric(ew_StrToFloat($this->CcoMont->CurrentValue)))
			$this->CcoMont->CurrentValue = ew_StrToFloat($this->CcoMont->CurrentValue);

		// Call Row_Rendering event
		$this->Row_Rendering();

		// Common render codes for all row types
		// CcoCodi
		// VcoCodi
		// CcoFPag
		// CcoMont
		// CcoEsta
		// CcoAnho
		// CcoMes
		// CcoConc
		// CcoUsua
		// CcoFCre

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// CcoCodi
		$this->CcoCodi->ViewValue = $this->CcoCodi->CurrentValue;
		$this->CcoCodi->ViewCustomAttributes = "";

		// VcoCodi
		if (strval($this->VcoCodi->CurrentValue) <> "") {
			$sFilterWrk = "\"VcoCodi\"" . ew_SearchString("=", $this->VcoCodi->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT \"VcoCodi\", \"VcoCodi\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"VCon\"";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->VcoCodi, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->VcoCodi->ViewValue = $this->VcoCodi->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->VcoCodi->ViewValue = $this->VcoCodi->CurrentValue;
			}
		} else {
			$this->VcoCodi->ViewValue = NULL;
		}
		$this->VcoCodi->ViewCustomAttributes = "";

		// CcoFPag
		$this->CcoFPag->ViewValue = $this->CcoFPag->CurrentValue;
		$this->CcoFPag->ViewValue = ew_FormatDateTime($this->CcoFPag->ViewValue, 7);
		$this->CcoFPag->ViewCustomAttributes = "";

		// CcoMont
		$this->CcoMont->ViewValue = $this->CcoMont->CurrentValue;
		$this->CcoMont->ViewCustomAttributes = "";

		// CcoEsta
		$this->CcoEsta->ViewValue = $this->CcoEsta->CurrentValue;
		$this->CcoEsta->ViewCustomAttributes = "";

		// CcoAnho
		$this->CcoAnho->ViewValue = $this->CcoAnho->CurrentValue;
		$this->CcoAnho->ViewCustomAttributes = "";

		// CcoMes
		$this->CcoMes->ViewValue = $this->CcoMes->CurrentValue;
		$this->CcoMes->ViewCustomAttributes = "";

		// CcoConc
		$this->CcoConc->ViewValue = $this->CcoConc->CurrentValue;
		$this->CcoConc->ViewCustomAttributes = "";

		// CcoUsua
		$this->CcoUsua->ViewValue = $this->CcoUsua->CurrentValue;
		$this->CcoUsua->ViewCustomAttributes = "";

		// CcoFCre
		$this->CcoFCre->ViewValue = $this->CcoFCre->CurrentValue;
		$this->CcoFCre->ViewValue = ew_FormatDateTime($this->CcoFCre->ViewValue, 7);
		$this->CcoFCre->ViewCustomAttributes = "";

			// CcoCodi
			$this->CcoCodi->LinkCustomAttributes = "";
			$this->CcoCodi->HrefValue = "";
			$this->CcoCodi->TooltipValue = "";

			// VcoCodi
			$this->VcoCodi->LinkCustomAttributes = "";
			$this->VcoCodi->HrefValue = "";
			$this->VcoCodi->TooltipValue = "";

			// CcoFPag
			$this->CcoFPag->LinkCustomAttributes = "";
			$this->CcoFPag->HrefValue = "";
			$this->CcoFPag->TooltipValue = "";

			// CcoMont
			$this->CcoMont->LinkCustomAttributes = "";
			$this->CcoMont->HrefValue = "";
			$this->CcoMont->TooltipValue = "";

			// CcoEsta
			$this->CcoEsta->LinkCustomAttributes = "";
			$this->CcoEsta->HrefValue = "";
			$this->CcoEsta->TooltipValue = "";

			// CcoAnho
			$this->CcoAnho->LinkCustomAttributes = "";
			$this->CcoAnho->HrefValue = "";
			$this->CcoAnho->TooltipValue = "";

			// CcoMes
			$this->CcoMes->LinkCustomAttributes = "";
			$this->CcoMes->HrefValue = "";
			$this->CcoMes->TooltipValue = "";

			// CcoConc
			$this->CcoConc->LinkCustomAttributes = "";
			$this->CcoConc->HrefValue = "";
			$this->CcoConc->TooltipValue = "";

			// CcoUsua
			$this->CcoUsua->LinkCustomAttributes = "";
			$this->CcoUsua->HrefValue = "";
			$this->CcoUsua->TooltipValue = "";

			// CcoFCre
			$this->CcoFCre->LinkCustomAttributes = "";
			$this->CcoFCre->HrefValue = "";
			$this->CcoFCre->TooltipValue = "";
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	//
	// Delete records based on current filter
	//
	function DeleteRows() {
		global $Language, $Security;
		if (!$Security->CanDelete()) {
			$this->setFailureMessage($Language->Phrase("NoDeletePermission")); // No delete permission
			return FALSE;
		}
		$DeleteRows = TRUE;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE) {
			return FALSE;
		} elseif ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
			$rs->Close();
			return FALSE;

		//} else {
		//	$this->LoadRowValues($rs); // Load row values

		}
		$rows = ($rs) ? $rs->GetRows() : array();
		$conn->BeginTrans();

		// Clone old rows
		$rsold = $rows;
		if ($rs)
			$rs->Close();

		// Call row deleting event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$DeleteRows = $this->Row_Deleting($row);
				if (!$DeleteRows) break;
			}
		}
		if ($DeleteRows) {
			$sKey = "";
			foreach ($rsold as $row) {
				$sThisKey = "";
				if ($sThisKey <> "") $sThisKey .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
				$sThisKey .= $row['CcoCodi'];
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				$DeleteRows = $this->Delete($row); // Delete
				$conn->raiseErrorFn = '';
				if ($DeleteRows === FALSE)
					break;
				if ($sKey <> "") $sKey .= ", ";
				$sKey .= $sThisKey;
			}
		} else {

			// Set up error message
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("DeleteCancelled"));
			}
		}
		if ($DeleteRows) {
			$conn->CommitTrans(); // Commit the changes
		} else {
			$conn->RollbackTrans(); // Rollback changes
		}

		// Call Row Deleted event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$this->Row_Deleted($row);
			}
		}
		return $DeleteRows;
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, "CConlist.php", "", $this->TableVar, TRUE);
		$PageId = "delete";
		$Breadcrumb->Add("delete", $PageId, $url);
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($CCon_delete)) $CCon_delete = new cCCon_delete();

// Page init
$CCon_delete->Page_Init();

// Page main
$CCon_delete->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$CCon_delete->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "delete";
var CurrentForm = fCCondelete = new ew_Form("fCCondelete", "delete");

// Form_CustomValidate event
fCCondelete.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fCCondelete.ValidateRequired = true;
<?php } else { ?>
fCCondelete.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
fCCondelete.Lists["x_VcoCodi"] = {"LinkField":"x_VcoCodi","Ajax":true,"AutoFill":false,"DisplayFields":["x_VcoCodi","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};

// Form object for search
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<?php

// Load records for display
if ($CCon_delete->Recordset = $CCon_delete->LoadRecordset())
	$CCon_deleteTotalRecs = $CCon_delete->Recordset->RecordCount(); // Get record count
if ($CCon_deleteTotalRecs <= 0) { // No record found, exit
	if ($CCon_delete->Recordset)
		$CCon_delete->Recordset->Close();
	$CCon_delete->Page_Terminate("CConlist.php"); // Return to list
}
?>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php $CCon_delete->ShowPageHeader(); ?>
<?php
$CCon_delete->ShowMessage();
?>
<form name="fCCondelete" id="fCCondelete" class="form-inline ewForm ewDeleteForm" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($CCon_delete->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $CCon_delete->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="CCon">
<input type="hidden" name="a_delete" id="a_delete" value="D">
<?php foreach ($CCon_delete->RecKeys as $key) { ?>
<?php $keyvalue = is_array($key) ? implode($EW_COMPOSITE_KEY_SEPARATOR, $key) : $key; ?>
<input type="hidden" name="key_m[]" value="<?php echo ew_HtmlEncode($keyvalue) ?>">
<?php } ?>
<div class="ewGrid">
<div class="<?php if (ew_IsResponsiveLayout()) { echo "table-responsive "; } ?>ewGridMiddlePanel">
<table class="table ewTable">
<?php echo $CCon->TableCustomInnerHtml ?>
	<thead>
	<tr class="ewTableHeader">
<?php if ($CCon->CcoCodi->Visible) { // CcoCodi ?>
		<th><span id="elh_CCon_CcoCodi" class="CCon_CcoCodi"><?php echo $CCon->CcoCodi->FldCaption() ?></span></th>
<?php } ?>
<?php if ($CCon->VcoCodi->Visible) { // VcoCodi ?>
		<th><span id="elh_CCon_VcoCodi" class="CCon_VcoCodi"><?php echo $CCon->VcoCodi->FldCaption() ?></span></th>
<?php } ?>
<?php if ($CCon->CcoFPag->Visible) { // CcoFPag ?>
		<th><span id="elh_CCon_CcoFPag" class="CCon_CcoFPag"><?php echo $CCon->CcoFPag->FldCaption() ?></span></th>
<?php } ?>
<?php if ($CCon->CcoMont->Visible) { // CcoMont ?>
		<th><span id="elh_CCon_CcoMont" class="CCon_CcoMont"><?php echo $CCon->CcoMont->FldCaption() ?></span></th>
<?php } ?>
<?php if ($CCon->CcoEsta->Visible) { // CcoEsta ?>
		<th><span id="elh_CCon_CcoEsta" class="CCon_CcoEsta"><?php echo $CCon->CcoEsta->FldCaption() ?></span></th>
<?php } ?>
<?php if ($CCon->CcoAnho->Visible) { // CcoAnho ?>
		<th><span id="elh_CCon_CcoAnho" class="CCon_CcoAnho"><?php echo $CCon->CcoAnho->FldCaption() ?></span></th>
<?php } ?>
<?php if ($CCon->CcoMes->Visible) { // CcoMes ?>
		<th><span id="elh_CCon_CcoMes" class="CCon_CcoMes"><?php echo $CCon->CcoMes->FldCaption() ?></span></th>
<?php } ?>
<?php if ($CCon->CcoConc->Visible) { // CcoConc ?>
		<th><span id="elh_CCon_CcoConc" class="CCon_CcoConc"><?php echo $CCon->CcoConc->FldCaption() ?></span></th>
<?php } ?>
<?php if ($CCon->CcoUsua->Visible) { // CcoUsua ?>
		<th><span id="elh_CCon_CcoUsua" class="CCon_CcoUsua"><?php echo $CCon->CcoUsua->FldCaption() ?></span></th>
<?php } ?>
<?php if ($CCon->CcoFCre->Visible) { // CcoFCre ?>
		<th><span id="elh_CCon_CcoFCre" class="CCon_CcoFCre"><?php echo $CCon->CcoFCre->FldCaption() ?></span></th>
<?php } ?>
	</tr>
	</thead>
	<tbody>
<?php
$CCon_delete->RecCnt = 0;
$i = 0;
while (!$CCon_delete->Recordset->EOF) {
	$CCon_delete->RecCnt++;
	$CCon_delete->RowCnt++;

	// Set row properties
	$CCon->ResetAttrs();
	$CCon->RowType = EW_ROWTYPE_VIEW; // View

	// Get the field contents
	$CCon_delete->LoadRowValues($CCon_delete->Recordset);

	// Render row
	$CCon_delete->RenderRow();
?>
	<tr<?php echo $CCon->RowAttributes() ?>>
<?php if ($CCon->CcoCodi->Visible) { // CcoCodi ?>
		<td<?php echo $CCon->CcoCodi->CellAttributes() ?>>
<span id="el<?php echo $CCon_delete->RowCnt ?>_CCon_CcoCodi" class="CCon_CcoCodi">
<span<?php echo $CCon->CcoCodi->ViewAttributes() ?>>
<?php echo $CCon->CcoCodi->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($CCon->VcoCodi->Visible) { // VcoCodi ?>
		<td<?php echo $CCon->VcoCodi->CellAttributes() ?>>
<span id="el<?php echo $CCon_delete->RowCnt ?>_CCon_VcoCodi" class="CCon_VcoCodi">
<span<?php echo $CCon->VcoCodi->ViewAttributes() ?>>
<?php echo $CCon->VcoCodi->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($CCon->CcoFPag->Visible) { // CcoFPag ?>
		<td<?php echo $CCon->CcoFPag->CellAttributes() ?>>
<span id="el<?php echo $CCon_delete->RowCnt ?>_CCon_CcoFPag" class="CCon_CcoFPag">
<span<?php echo $CCon->CcoFPag->ViewAttributes() ?>>
<?php echo $CCon->CcoFPag->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($CCon->CcoMont->Visible) { // CcoMont ?>
		<td<?php echo $CCon->CcoMont->CellAttributes() ?>>
<span id="el<?php echo $CCon_delete->RowCnt ?>_CCon_CcoMont" class="CCon_CcoMont">
<span<?php echo $CCon->CcoMont->ViewAttributes() ?>>
<?php echo $CCon->CcoMont->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($CCon->CcoEsta->Visible) { // CcoEsta ?>
		<td<?php echo $CCon->CcoEsta->CellAttributes() ?>>
<span id="el<?php echo $CCon_delete->RowCnt ?>_CCon_CcoEsta" class="CCon_CcoEsta">
<span<?php echo $CCon->CcoEsta->ViewAttributes() ?>>
<?php echo $CCon->CcoEsta->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($CCon->CcoAnho->Visible) { // CcoAnho ?>
		<td<?php echo $CCon->CcoAnho->CellAttributes() ?>>
<span id="el<?php echo $CCon_delete->RowCnt ?>_CCon_CcoAnho" class="CCon_CcoAnho">
<span<?php echo $CCon->CcoAnho->ViewAttributes() ?>>
<?php echo $CCon->CcoAnho->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($CCon->CcoMes->Visible) { // CcoMes ?>
		<td<?php echo $CCon->CcoMes->CellAttributes() ?>>
<span id="el<?php echo $CCon_delete->RowCnt ?>_CCon_CcoMes" class="CCon_CcoMes">
<span<?php echo $CCon->CcoMes->ViewAttributes() ?>>
<?php echo $CCon->CcoMes->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($CCon->CcoConc->Visible) { // CcoConc ?>
		<td<?php echo $CCon->CcoConc->CellAttributes() ?>>
<span id="el<?php echo $CCon_delete->RowCnt ?>_CCon_CcoConc" class="CCon_CcoConc">
<span<?php echo $CCon->CcoConc->ViewAttributes() ?>>
<?php echo $CCon->CcoConc->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($CCon->CcoUsua->Visible) { // CcoUsua ?>
		<td<?php echo $CCon->CcoUsua->CellAttributes() ?>>
<span id="el<?php echo $CCon_delete->RowCnt ?>_CCon_CcoUsua" class="CCon_CcoUsua">
<span<?php echo $CCon->CcoUsua->ViewAttributes() ?>>
<?php echo $CCon->CcoUsua->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($CCon->CcoFCre->Visible) { // CcoFCre ?>
		<td<?php echo $CCon->CcoFCre->CellAttributes() ?>>
<span id="el<?php echo $CCon_delete->RowCnt ?>_CCon_CcoFCre" class="CCon_CcoFCre">
<span<?php echo $CCon->CcoFCre->ViewAttributes() ?>>
<?php echo $CCon->CcoFCre->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
	</tr>
<?php
	$CCon_delete->Recordset->MoveNext();
}
$CCon_delete->Recordset->Close();
?>
</tbody>
</table>
</div>
</div>
<div>
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("DeleteBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="button" data-href="<?php echo $CCon_delete->getReturnUrl() ?>"><?php echo $Language->Phrase("CancelBtn") ?></button>
</div>
</form>
<script type="text/javascript">
fCCondelete.Init();
</script>
<?php
$CCon_delete->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$CCon_delete->Page_Terminate();
?>
