<?php

// Global variable for table object
$FCab = NULL;

//
// Table class for FCab
//
class cFCab extends cTable {
	var $FcaCodi;
	var $OveCodi;
	var $FcaVend;
	var $FcaCaje;
	var $FcaFech;
	var $FcaTimb;
	var $FcaTFac;
	var $FcaAnul;
	var $FcaMAnu;
	var $FcaFAnu;
	var $FcaFTip;
	var $FcaUsua;
	var $FcaFCre;

	//
	// Table class constructor
	//
	function __construct() {
		global $Language;

		// Language object
		if (!isset($Language)) $Language = new cLanguage();
		$this->TableVar = 'FCab';
		$this->TableName = 'FCab';
		$this->TableType = 'TABLE';

		// Update Table
		$this->UpdateTable = "\"public\".\"FCab\"";
		$this->DBID = 'DB';
		$this->ExportAll = TRUE;
		$this->ExportPageBreakCount = 0; // Page break per every n record (PDF only)
		$this->ExportPageOrientation = "portrait"; // Page orientation (PDF only)
		$this->ExportPageSize = "a4"; // Page size (PDF only)
		$this->ExportExcelPageOrientation = ""; // Page orientation (PHPExcel only)
		$this->ExportExcelPageSize = ""; // Page size (PHPExcel only)
		$this->DetailAdd = TRUE; // Allow detail add
		$this->DetailEdit = TRUE; // Allow detail edit
		$this->DetailView = FALSE; // Allow detail view
		$this->ShowMultipleDetails = FALSE; // Show multiple details
		$this->GridAddRowCount = 5;
		$this->AllowAddDeleteRow = ew_AllowAddDeleteRow(); // Allow add/delete row
		$this->UserIDAllowSecurity = 0; // User ID Allow
		$this->BasicSearch = new cBasicSearch($this->TableVar);

		// FcaCodi
		$this->FcaCodi = new cField('FCab', 'FCab', 'x_FcaCodi', 'FcaCodi', '"FcaCodi"', 'CAST("FcaCodi" AS varchar(255))', 3, -1, FALSE, '"FcaCodi"', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'NO');
		$this->FcaCodi->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['FcaCodi'] = &$this->FcaCodi;

		// OveCodi
		$this->OveCodi = new cField('FCab', 'FCab', 'x_OveCodi', 'OveCodi', '"OveCodi"', 'CAST("OveCodi" AS varchar(255))', 3, -1, FALSE, '"OveCodi"', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'SELECT');
		$this->OveCodi->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['OveCodi'] = &$this->OveCodi;

		// FcaVend
		$this->FcaVend = new cField('FCab', 'FCab', 'x_FcaVend', 'FcaVend', '"FcaVend"', 'CAST("FcaVend" AS varchar(255))', 3, -1, FALSE, '"FcaVend"', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->FcaVend->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['FcaVend'] = &$this->FcaVend;

		// FcaCaje
		$this->FcaCaje = new cField('FCab', 'FCab', 'x_FcaCaje', 'FcaCaje', '"FcaCaje"', 'CAST("FcaCaje" AS varchar(255))', 3, -1, FALSE, '"FcaCaje"', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->FcaCaje->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['FcaCaje'] = &$this->FcaCaje;

		// FcaFech
		$this->FcaFech = new cField('FCab', 'FCab', 'x_FcaFech', 'FcaFech', '"FcaFech"', 'CAST("FcaFech" AS varchar(255))', 133, 7, FALSE, '"FcaFech"', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->FcaFech->FldDefaultErrMsg = str_replace("%s", "/", $Language->Phrase("IncorrectDateDMY"));
		$this->fields['FcaFech'] = &$this->FcaFech;

		// FcaTimb
		$this->FcaTimb = new cField('FCab', 'FCab', 'x_FcaTimb', 'FcaTimb', '"FcaTimb"', '"FcaTimb"', 200, -1, FALSE, '"FcaTimb"', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->fields['FcaTimb'] = &$this->FcaTimb;

		// FcaTFac
		$this->FcaTFac = new cField('FCab', 'FCab', 'x_FcaTFac', 'FcaTFac', '"FcaTFac"', 'CAST("FcaTFac" AS varchar(255))', 5, -1, FALSE, '"FcaTFac"', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->FcaTFac->FldDefaultErrMsg = $Language->Phrase("IncorrectFloat");
		$this->fields['FcaTFac'] = &$this->FcaTFac;

		// FcaAnul
		$this->FcaAnul = new cField('FCab', 'FCab', 'x_FcaAnul', 'FcaAnul', '"FcaAnul"', '"FcaAnul"', 200, -1, FALSE, '"FcaAnul"', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->fields['FcaAnul'] = &$this->FcaAnul;

		// FcaMAnu
		$this->FcaMAnu = new cField('FCab', 'FCab', 'x_FcaMAnu', 'FcaMAnu', '"FcaMAnu"', '"FcaMAnu"', 200, -1, FALSE, '"FcaMAnu"', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->fields['FcaMAnu'] = &$this->FcaMAnu;

		// FcaFAnu
		$this->FcaFAnu = new cField('FCab', 'FCab', 'x_FcaFAnu', 'FcaFAnu', '"FcaFAnu"', 'CAST("FcaFAnu" AS varchar(255))', 133, 7, FALSE, '"FcaFAnu"', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->FcaFAnu->FldDefaultErrMsg = str_replace("%s", "/", $Language->Phrase("IncorrectDateDMY"));
		$this->fields['FcaFAnu'] = &$this->FcaFAnu;

		// FcaFTip
		$this->FcaFTip = new cField('FCab', 'FCab', 'x_FcaFTip', 'FcaFTip', '"FcaFTip"', '"FcaFTip"', 200, -1, FALSE, '"FcaFTip"', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->fields['FcaFTip'] = &$this->FcaFTip;

		// FcaUsua
		$this->FcaUsua = new cField('FCab', 'FCab', 'x_FcaUsua', 'FcaUsua', '"FcaUsua"', '"FcaUsua"', 200, -1, FALSE, '"FcaUsua"', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->fields['FcaUsua'] = &$this->FcaUsua;

		// FcaFCre
		$this->FcaFCre = new cField('FCab', 'FCab', 'x_FcaFCre', 'FcaFCre', '"FcaFCre"', 'CAST("FcaFCre" AS varchar(255))', 135, 7, FALSE, '"FcaFCre"', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->FcaFCre->FldDefaultErrMsg = str_replace("%s", "/", $Language->Phrase("IncorrectDateDMY"));
		$this->fields['FcaFCre'] = &$this->FcaFCre;
	}

	// Multiple column sort
	function UpdateSort(&$ofld, $ctrl) {
		if ($this->CurrentOrder == $ofld->FldName) {
			$sSortField = $ofld->FldExpression;
			$sLastSort = $ofld->getSort();
			if ($this->CurrentOrderType == "ASC" || $this->CurrentOrderType == "DESC") {
				$sThisSort = $this->CurrentOrderType;
			} else {
				$sThisSort = ($sLastSort == "ASC") ? "DESC" : "ASC";
			}
			$ofld->setSort($sThisSort);
			if ($ctrl) {
				$sOrderBy = $this->getSessionOrderBy();
				if (strpos($sOrderBy, $sSortField . " " . $sLastSort) !== FALSE) {
					$sOrderBy = str_replace($sSortField . " " . $sLastSort, $sSortField . " " . $sThisSort, $sOrderBy);
				} else {
					if ($sOrderBy <> "") $sOrderBy .= ", ";
					$sOrderBy .= $sSortField . " " . $sThisSort;
				}
				$this->setSessionOrderBy($sOrderBy); // Save to Session
			} else {
				$this->setSessionOrderBy($sSortField . " " . $sThisSort); // Save to Session
			}
		} else {
			if (!$ctrl) $ofld->setSort("");
		}
	}

	// Table level SQL
	var $_SqlFrom = "";

	function getSqlFrom() { // From
		return ($this->_SqlFrom <> "") ? $this->_SqlFrom : "\"public\".\"FCab\"";
	}

	function SqlFrom() { // For backward compatibility
    	return $this->getSqlFrom();
	}

	function setSqlFrom($v) {
    	$this->_SqlFrom = $v;
	}
	var $_SqlSelect = "";

	function getSqlSelect() { // Select
		return ($this->_SqlSelect <> "") ? $this->_SqlSelect : "SELECT * FROM " . $this->getSqlFrom();
	}

	function SqlSelect() { // For backward compatibility
    	return $this->getSqlSelect();
	}

	function setSqlSelect($v) {
    	$this->_SqlSelect = $v;
	}
	var $_SqlWhere = "";

	function getSqlWhere() { // Where
		$sWhere = ($this->_SqlWhere <> "") ? $this->_SqlWhere : "";
		$this->TableFilter = "";
		ew_AddFilter($sWhere, $this->TableFilter);
		return $sWhere;
	}

	function SqlWhere() { // For backward compatibility
    	return $this->getSqlWhere();
	}

	function setSqlWhere($v) {
    	$this->_SqlWhere = $v;
	}
	var $_SqlGroupBy = "";

	function getSqlGroupBy() { // Group By
		return ($this->_SqlGroupBy <> "") ? $this->_SqlGroupBy : "";
	}

	function SqlGroupBy() { // For backward compatibility
    	return $this->getSqlGroupBy();
	}

	function setSqlGroupBy($v) {
    	$this->_SqlGroupBy = $v;
	}
	var $_SqlHaving = "";

	function getSqlHaving() { // Having
		return ($this->_SqlHaving <> "") ? $this->_SqlHaving : "";
	}

	function SqlHaving() { // For backward compatibility
    	return $this->getSqlHaving();
	}

	function setSqlHaving($v) {
    	$this->_SqlHaving = $v;
	}
	var $_SqlOrderBy = "";

	function getSqlOrderBy() { // Order By
		return ($this->_SqlOrderBy <> "") ? $this->_SqlOrderBy : "";
	}

	function SqlOrderBy() { // For backward compatibility
    	return $this->getSqlOrderBy();
	}

	function setSqlOrderBy($v) {
    	$this->_SqlOrderBy = $v;
	}

	// Apply User ID filters
	function ApplyUserIDFilters($sFilter) {
		return $sFilter;
	}

	// Check if User ID security allows view all
	function UserIDAllow($id = "") {
		$allow = EW_USER_ID_ALLOW;
		switch ($id) {
			case "add":
			case "copy":
			case "gridadd":
			case "register":
			case "addopt":
				return (($allow & 1) == 1);
			case "edit":
			case "gridedit":
			case "update":
			case "changepwd":
			case "forgotpwd":
				return (($allow & 4) == 4);
			case "delete":
				return (($allow & 2) == 2);
			case "view":
				return (($allow & 32) == 32);
			case "search":
				return (($allow & 64) == 64);
			default:
				return (($allow & 8) == 8);
		}
	}

	// Get SQL
	function GetSQL($where, $orderby) {
		return ew_BuildSelectSql($this->getSqlSelect(), $this->getSqlWhere(),
			$this->getSqlGroupBy(), $this->getSqlHaving(), $this->getSqlOrderBy(),
			$where, $orderby);
	}

	// Table SQL
	function SQL() {
		$sFilter = $this->CurrentFilter;
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$sSort = $this->getSessionOrderBy();
		return ew_BuildSelectSql($this->getSqlSelect(), $this->getSqlWhere(),
			$this->getSqlGroupBy(), $this->getSqlHaving(), $this->getSqlOrderBy(),
			$sFilter, $sSort);
	}

	// Table SQL with List page filter
	function SelectSQL() {
		$sFilter = $this->getSessionWhere();
		ew_AddFilter($sFilter, $this->CurrentFilter);
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$this->Recordset_Selecting($sFilter);
		$sSort = $this->getSessionOrderBy();
		return ew_BuildSelectSql($this->getSqlSelect(), $this->getSqlWhere(), $this->getSqlGroupBy(),
			$this->getSqlHaving(), $this->getSqlOrderBy(), $sFilter, $sSort);
	}

	// Get ORDER BY clause
	function GetOrderBy() {
		$sSort = $this->getSessionOrderBy();
		return ew_BuildSelectSql("", "", "", "", $this->getSqlOrderBy(), "", $sSort);
	}

	// Try to get record count
	function TryGetRecordCount($sSql) {
		$cnt = -1;
		if (($this->TableType == 'TABLE' || $this->TableType == 'VIEW' || $this->TableType == 'LINKTABLE') && preg_match("/^SELECT \* FROM/i", $sSql)) {
			$sSql = "SELECT COUNT(*) FROM" . preg_replace('/^SELECT\s([\s\S]+)?\*\sFROM/i', "", $sSql);
			$sOrderBy = $this->GetOrderBy();
			if (substr($sSql, strlen($sOrderBy) * -1) == $sOrderBy)
				$sSql = substr($sSql, 0, strlen($sSql) - strlen($sOrderBy)); // Remove ORDER BY clause
		} else {
			$sSql = "SELECT COUNT(*) FROM (" . $sSql . ") EW_COUNT_TABLE";
		}
		$conn = &$this->Connection();
		if ($rs = $conn->Execute($sSql)) {
			if (!$rs->EOF && $rs->FieldCount() > 0) {
				$cnt = $rs->fields[0];
				$rs->Close();
			}
		}
		return intval($cnt);
	}

	// Get record count based on filter (for detail record count in master table pages)
	function LoadRecordCount($sFilter) {
		$origFilter = $this->CurrentFilter;
		$this->CurrentFilter = $sFilter;
		$this->Recordset_Selecting($this->CurrentFilter);

		//$sSql = $this->SQL();
		$sSql = $this->GetSQL($this->CurrentFilter, "");
		$cnt = $this->TryGetRecordCount($sSql);
		if ($cnt == -1) {
			if ($rs = $this->LoadRs($this->CurrentFilter)) {
				$cnt = $rs->RecordCount();
				$rs->Close();
			}
		}
		$this->CurrentFilter = $origFilter;
		return intval($cnt);
	}

	// Get record count (for current List page)
	function SelectRecordCount() {
		$sSql = $this->SelectSQL();
		$cnt = $this->TryGetRecordCount($sSql);
		if ($cnt == -1) {
			$conn = &$this->Connection();
			if ($rs = $conn->Execute($sSql)) {
				$cnt = $rs->RecordCount();
				$rs->Close();
			}
		}
		return intval($cnt);
	}

	// INSERT statement
	function InsertSQL(&$rs) {
		$names = "";
		$values = "";
		foreach ($rs as $name => $value) {
			if (!isset($this->fields[$name]) || $this->fields[$name]->FldIsCustom)
				continue;
			$names .= $this->fields[$name]->FldExpression . ",";
			$values .= ew_QuotedValue($value, $this->fields[$name]->FldDataType, $this->DBID) . ",";
		}
		while (substr($names, -1) == ",")
			$names = substr($names, 0, -1);
		while (substr($values, -1) == ",")
			$values = substr($values, 0, -1);
		return "INSERT INTO " . $this->UpdateTable . " ($names) VALUES ($values)";
	}

	// Insert
	function Insert(&$rs) {
		$conn = &$this->Connection();
		return $conn->Execute($this->InsertSQL($rs));
	}

	// UPDATE statement
	function UpdateSQL(&$rs, $where = "", $curfilter = TRUE) {
		$sql = "UPDATE " . $this->UpdateTable . " SET ";
		foreach ($rs as $name => $value) {
			if (!isset($this->fields[$name]) || $this->fields[$name]->FldIsCustom)
				continue;
			$sql .= $this->fields[$name]->FldExpression . "=";
			$sql .= ew_QuotedValue($value, $this->fields[$name]->FldDataType, $this->DBID) . ",";
		}
		while (substr($sql, -1) == ",")
			$sql = substr($sql, 0, -1);
		$filter = ($curfilter) ? $this->CurrentFilter : "";
		if (is_array($where))
			$where = $this->ArrayToFilter($where);
		ew_AddFilter($filter, $where);
		if ($filter <> "")	$sql .= " WHERE " . $filter;
		return $sql;
	}

	// Update
	function Update(&$rs, $where = "", $rsold = NULL, $curfilter = TRUE) {
		$conn = &$this->Connection();
		return $conn->Execute($this->UpdateSQL($rs, $where, $curfilter));
	}

	// DELETE statement
	function DeleteSQL(&$rs, $where = "", $curfilter = TRUE) {
		$sql = "DELETE FROM " . $this->UpdateTable . " WHERE ";
		if (is_array($where))
			$where = $this->ArrayToFilter($where);
		if ($rs) {
			if (array_key_exists('FcaCodi', $rs))
				ew_AddFilter($where, ew_QuotedName('FcaCodi', $this->DBID) . '=' . ew_QuotedValue($rs['FcaCodi'], $this->FcaCodi->FldDataType, $this->DBID));
		}
		$filter = ($curfilter) ? $this->CurrentFilter : "";
		ew_AddFilter($filter, $where);
		if ($filter <> "")
			$sql .= $filter;
		else
			$sql .= "0=1"; // Avoid delete
		return $sql;
	}

	// Delete
	function Delete(&$rs, $where = "", $curfilter = TRUE) {
		$conn = &$this->Connection();
		return $conn->Execute($this->DeleteSQL($rs, $where, $curfilter));
	}

	// Key filter WHERE clause
	function SqlKeyFilter() {
		return "\"FcaCodi\" = @FcaCodi@";
	}

	// Key filter
	function KeyFilter() {
		$sKeyFilter = $this->SqlKeyFilter();
		if (!is_numeric($this->FcaCodi->CurrentValue))
			$sKeyFilter = "0=1"; // Invalid key
		$sKeyFilter = str_replace("@FcaCodi@", ew_AdjustSql($this->FcaCodi->CurrentValue, $this->DBID), $sKeyFilter); // Replace key value
		return $sKeyFilter;
	}

	// Return page URL
	function getReturnUrl() {
		$name = EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_RETURN_URL;

		// Get referer URL automatically
		if (ew_ServerVar("HTTP_REFERER") <> "" && ew_ReferPage() <> ew_CurrentPage() && ew_ReferPage() <> "login.php") // Referer not same page or login page
			$_SESSION[$name] = ew_ServerVar("HTTP_REFERER"); // Save to Session
		if (@$_SESSION[$name] <> "") {
			return $_SESSION[$name];
		} else {
			return "FCablist.php";
		}
	}

	function setReturnUrl($v) {
		$_SESSION[EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_RETURN_URL] = $v;
	}

	// List URL
	function GetListUrl() {
		return "FCablist.php";
	}

	// View URL
	function GetViewUrl($parm = "") {
		if ($parm <> "")
			$url = $this->KeyUrl("FCabview.php", $this->UrlParm($parm));
		else
			$url = $this->KeyUrl("FCabview.php", $this->UrlParm(EW_TABLE_SHOW_DETAIL . "="));
		return $this->AddMasterUrl($url);
	}

	// Add URL
	function GetAddUrl($parm = "") {
		if ($parm <> "")
			$url = "FCabadd.php?" . $this->UrlParm($parm);
		else
			$url = "FCabadd.php";
		return $this->AddMasterUrl($url);
	}

	// Edit URL
	function GetEditUrl($parm = "") {
		$url = $this->KeyUrl("FCabedit.php", $this->UrlParm($parm));
		return $this->AddMasterUrl($url);
	}

	// Inline edit URL
	function GetInlineEditUrl() {
		$url = $this->KeyUrl(ew_CurrentPage(), $this->UrlParm("a=edit"));
		return $this->AddMasterUrl($url);
	}

	// Copy URL
	function GetCopyUrl($parm = "") {
		$url = $this->KeyUrl("FCabadd.php", $this->UrlParm($parm));
		return $this->AddMasterUrl($url);
	}

	// Inline copy URL
	function GetInlineCopyUrl() {
		$url = $this->KeyUrl(ew_CurrentPage(), $this->UrlParm("a=copy"));
		return $this->AddMasterUrl($url);
	}

	// Delete URL
	function GetDeleteUrl() {
		return $this->KeyUrl("FCabdelete.php", $this->UrlParm());
	}

	// Add master url
	function AddMasterUrl($url) {
		return $url;
	}

	function KeyToJson() {
		$json = "";
		$json .= "FcaCodi:" . ew_VarToJson($this->FcaCodi->CurrentValue, "number", "'");
		return "{" . $json . "}";
	}

	// Add key value to URL
	function KeyUrl($url, $parm = "") {
		$sUrl = $url . "?";
		if ($parm <> "") $sUrl .= $parm . "&";
		if (!is_null($this->FcaCodi->CurrentValue)) {
			$sUrl .= "FcaCodi=" . urlencode($this->FcaCodi->CurrentValue);
		} else {
			return "javascript:ew_Alert(ewLanguage.Phrase('InvalidRecord'));";
		}
		return $sUrl;
	}

	// Sort URL
	function SortUrl(&$fld) {
		if ($this->CurrentAction <> "" || $this->Export <> "" ||
			in_array($fld->FldType, array(128, 204, 205))) { // Unsortable data type
				return "";
		} elseif ($fld->Sortable) {
			$sUrlParm = $this->UrlParm("order=" . urlencode($fld->FldName) . "&amp;ordertype=" . $fld->ReverseSort());
			return ew_CurrentPage() . "?" . $sUrlParm;
		} else {
			return "";
		}
	}

	// Get record keys from $_POST/$_GET/$_SESSION
	function GetRecordKeys() {
		global $EW_COMPOSITE_KEY_SEPARATOR;
		$arKeys = array();
		$arKey = array();
		if (isset($_POST["key_m"])) {
			$arKeys = ew_StripSlashes($_POST["key_m"]);
			$cnt = count($arKeys);
		} elseif (isset($_GET["key_m"])) {
			$arKeys = ew_StripSlashes($_GET["key_m"]);
			$cnt = count($arKeys);
		} elseif (!empty($_GET) || !empty($_POST)) {
			$isPost = ew_IsHttpPost();
			$arKeys[] = $isPost ? ew_StripSlashes(@$_POST["FcaCodi"]) : ew_StripSlashes(@$_GET["FcaCodi"]); // FcaCodi

			//return $arKeys; // Do not return yet, so the values will also be checked by the following code
		}

		// Check keys
		$ar = array();
		foreach ($arKeys as $key) {
			if (!is_numeric($key))
				continue;
			$ar[] = $key;
		}
		return $ar;
	}

	// Get key filter
	function GetKeyFilter() {
		$arKeys = $this->GetRecordKeys();
		$sKeyFilter = "";
		foreach ($arKeys as $key) {
			if ($sKeyFilter <> "") $sKeyFilter .= " OR ";
			$this->FcaCodi->CurrentValue = $key;
			$sKeyFilter .= "(" . $this->KeyFilter() . ")";
		}
		return $sKeyFilter;
	}

	// Load rows based on filter
	function &LoadRs($sFilter) {

		// Set up filter (SQL WHERE clause) and get return SQL
		//$this->CurrentFilter = $sFilter;
		//$sSql = $this->SQL();

		$sSql = $this->GetSQL($sFilter, "");
		$conn = &$this->Connection();
		$rs = $conn->Execute($sSql);
		return $rs;
	}

	// Load row values from recordset
	function LoadListRowValues(&$rs) {
		$this->FcaCodi->setDbValue($rs->fields('FcaCodi'));
		$this->OveCodi->setDbValue($rs->fields('OveCodi'));
		$this->FcaVend->setDbValue($rs->fields('FcaVend'));
		$this->FcaCaje->setDbValue($rs->fields('FcaCaje'));
		$this->FcaFech->setDbValue($rs->fields('FcaFech'));
		$this->FcaTimb->setDbValue($rs->fields('FcaTimb'));
		$this->FcaTFac->setDbValue($rs->fields('FcaTFac'));
		$this->FcaAnul->setDbValue($rs->fields('FcaAnul'));
		$this->FcaMAnu->setDbValue($rs->fields('FcaMAnu'));
		$this->FcaFAnu->setDbValue($rs->fields('FcaFAnu'));
		$this->FcaFTip->setDbValue($rs->fields('FcaFTip'));
		$this->FcaUsua->setDbValue($rs->fields('FcaUsua'));
		$this->FcaFCre->setDbValue($rs->fields('FcaFCre'));
	}

	// Render list row values
	function RenderListRow() {
		global $Security, $gsLanguage, $Language;

		// Call Row Rendering event
		$this->Row_Rendering();

   // Common render codes
		// FcaCodi
		// OveCodi
		// FcaVend
		// FcaCaje
		// FcaFech
		// FcaTimb
		// FcaTFac
		// FcaAnul
		// FcaMAnu
		// FcaFAnu
		// FcaFTip
		// FcaUsua
		// FcaFCre
		// FcaCodi

		$this->FcaCodi->ViewValue = $this->FcaCodi->CurrentValue;
		$this->FcaCodi->ViewCustomAttributes = "";

		// OveCodi
		if (strval($this->OveCodi->CurrentValue) <> "") {
			$sFilterWrk = "\"OveCodi\"" . ew_SearchString("=", $this->OveCodi->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT \"OveCodi\", \"OveCodi\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"OVen\"";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->OveCodi, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->OveCodi->ViewValue = $this->OveCodi->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->OveCodi->ViewValue = $this->OveCodi->CurrentValue;
			}
		} else {
			$this->OveCodi->ViewValue = NULL;
		}
		$this->OveCodi->ViewCustomAttributes = "";

		// FcaVend
		$this->FcaVend->ViewValue = $this->FcaVend->CurrentValue;
		$this->FcaVend->ViewCustomAttributes = "";

		// FcaCaje
		$this->FcaCaje->ViewValue = $this->FcaCaje->CurrentValue;
		$this->FcaCaje->ViewCustomAttributes = "";

		// FcaFech
		$this->FcaFech->ViewValue = $this->FcaFech->CurrentValue;
		$this->FcaFech->ViewValue = ew_FormatDateTime($this->FcaFech->ViewValue, 7);
		$this->FcaFech->ViewCustomAttributes = "";

		// FcaTimb
		$this->FcaTimb->ViewValue = $this->FcaTimb->CurrentValue;
		$this->FcaTimb->ViewCustomAttributes = "";

		// FcaTFac
		$this->FcaTFac->ViewValue = $this->FcaTFac->CurrentValue;
		$this->FcaTFac->ViewCustomAttributes = "";

		// FcaAnul
		$this->FcaAnul->ViewValue = $this->FcaAnul->CurrentValue;
		$this->FcaAnul->ViewCustomAttributes = "";

		// FcaMAnu
		$this->FcaMAnu->ViewValue = $this->FcaMAnu->CurrentValue;
		$this->FcaMAnu->ViewCustomAttributes = "";

		// FcaFAnu
		$this->FcaFAnu->ViewValue = $this->FcaFAnu->CurrentValue;
		$this->FcaFAnu->ViewValue = ew_FormatDateTime($this->FcaFAnu->ViewValue, 7);
		$this->FcaFAnu->ViewCustomAttributes = "";

		// FcaFTip
		$this->FcaFTip->ViewValue = $this->FcaFTip->CurrentValue;
		$this->FcaFTip->ViewCustomAttributes = "";

		// FcaUsua
		$this->FcaUsua->ViewValue = $this->FcaUsua->CurrentValue;
		$this->FcaUsua->ViewCustomAttributes = "";

		// FcaFCre
		$this->FcaFCre->ViewValue = $this->FcaFCre->CurrentValue;
		$this->FcaFCre->ViewValue = ew_FormatDateTime($this->FcaFCre->ViewValue, 7);
		$this->FcaFCre->ViewCustomAttributes = "";

		// FcaCodi
		$this->FcaCodi->LinkCustomAttributes = "";
		$this->FcaCodi->HrefValue = "";
		$this->FcaCodi->TooltipValue = "";

		// OveCodi
		$this->OveCodi->LinkCustomAttributes = "";
		$this->OveCodi->HrefValue = "";
		$this->OveCodi->TooltipValue = "";

		// FcaVend
		$this->FcaVend->LinkCustomAttributes = "";
		$this->FcaVend->HrefValue = "";
		$this->FcaVend->TooltipValue = "";

		// FcaCaje
		$this->FcaCaje->LinkCustomAttributes = "";
		$this->FcaCaje->HrefValue = "";
		$this->FcaCaje->TooltipValue = "";

		// FcaFech
		$this->FcaFech->LinkCustomAttributes = "";
		$this->FcaFech->HrefValue = "";
		$this->FcaFech->TooltipValue = "";

		// FcaTimb
		$this->FcaTimb->LinkCustomAttributes = "";
		$this->FcaTimb->HrefValue = "";
		$this->FcaTimb->TooltipValue = "";

		// FcaTFac
		$this->FcaTFac->LinkCustomAttributes = "";
		$this->FcaTFac->HrefValue = "";
		$this->FcaTFac->TooltipValue = "";

		// FcaAnul
		$this->FcaAnul->LinkCustomAttributes = "";
		$this->FcaAnul->HrefValue = "";
		$this->FcaAnul->TooltipValue = "";

		// FcaMAnu
		$this->FcaMAnu->LinkCustomAttributes = "";
		$this->FcaMAnu->HrefValue = "";
		$this->FcaMAnu->TooltipValue = "";

		// FcaFAnu
		$this->FcaFAnu->LinkCustomAttributes = "";
		$this->FcaFAnu->HrefValue = "";
		$this->FcaFAnu->TooltipValue = "";

		// FcaFTip
		$this->FcaFTip->LinkCustomAttributes = "";
		$this->FcaFTip->HrefValue = "";
		$this->FcaFTip->TooltipValue = "";

		// FcaUsua
		$this->FcaUsua->LinkCustomAttributes = "";
		$this->FcaUsua->HrefValue = "";
		$this->FcaUsua->TooltipValue = "";

		// FcaFCre
		$this->FcaFCre->LinkCustomAttributes = "";
		$this->FcaFCre->HrefValue = "";
		$this->FcaFCre->TooltipValue = "";

		// Call Row Rendered event
		$this->Row_Rendered();
	}

	// Render edit row values
	function RenderEditRow() {
		global $Security, $gsLanguage, $Language;

		// Call Row Rendering event
		$this->Row_Rendering();

		// FcaCodi
		$this->FcaCodi->EditAttrs["class"] = "form-control";
		$this->FcaCodi->EditCustomAttributes = "";
		$this->FcaCodi->EditValue = $this->FcaCodi->CurrentValue;
		$this->FcaCodi->ViewCustomAttributes = "";

		// OveCodi
		$this->OveCodi->EditAttrs["class"] = "form-control";
		$this->OveCodi->EditCustomAttributes = "";

		// FcaVend
		$this->FcaVend->EditAttrs["class"] = "form-control";
		$this->FcaVend->EditCustomAttributes = "";
		$this->FcaVend->EditValue = $this->FcaVend->CurrentValue;
		$this->FcaVend->PlaceHolder = ew_RemoveHtml($this->FcaVend->FldCaption());

		// FcaCaje
		$this->FcaCaje->EditAttrs["class"] = "form-control";
		$this->FcaCaje->EditCustomAttributes = "";
		$this->FcaCaje->EditValue = $this->FcaCaje->CurrentValue;
		$this->FcaCaje->PlaceHolder = ew_RemoveHtml($this->FcaCaje->FldCaption());

		// FcaFech
		$this->FcaFech->EditAttrs["class"] = "form-control";
		$this->FcaFech->EditCustomAttributes = "";
		$this->FcaFech->EditValue = ew_FormatDateTime($this->FcaFech->CurrentValue, 7);
		$this->FcaFech->PlaceHolder = ew_RemoveHtml($this->FcaFech->FldCaption());

		// FcaTimb
		$this->FcaTimb->EditAttrs["class"] = "form-control";
		$this->FcaTimb->EditCustomAttributes = "";
		$this->FcaTimb->EditValue = $this->FcaTimb->CurrentValue;
		$this->FcaTimb->PlaceHolder = ew_RemoveHtml($this->FcaTimb->FldCaption());

		// FcaTFac
		$this->FcaTFac->EditAttrs["class"] = "form-control";
		$this->FcaTFac->EditCustomAttributes = "";
		$this->FcaTFac->EditValue = $this->FcaTFac->CurrentValue;
		$this->FcaTFac->PlaceHolder = ew_RemoveHtml($this->FcaTFac->FldCaption());
		if (strval($this->FcaTFac->EditValue) <> "" && is_numeric($this->FcaTFac->EditValue)) $this->FcaTFac->EditValue = ew_FormatNumber($this->FcaTFac->EditValue, -2, -1, -2, 0);

		// FcaAnul
		$this->FcaAnul->EditAttrs["class"] = "form-control";
		$this->FcaAnul->EditCustomAttributes = "";
		$this->FcaAnul->EditValue = $this->FcaAnul->CurrentValue;
		$this->FcaAnul->PlaceHolder = ew_RemoveHtml($this->FcaAnul->FldCaption());

		// FcaMAnu
		$this->FcaMAnu->EditAttrs["class"] = "form-control";
		$this->FcaMAnu->EditCustomAttributes = "";
		$this->FcaMAnu->EditValue = $this->FcaMAnu->CurrentValue;
		$this->FcaMAnu->PlaceHolder = ew_RemoveHtml($this->FcaMAnu->FldCaption());

		// FcaFAnu
		$this->FcaFAnu->EditAttrs["class"] = "form-control";
		$this->FcaFAnu->EditCustomAttributes = "";
		$this->FcaFAnu->EditValue = ew_FormatDateTime($this->FcaFAnu->CurrentValue, 7);
		$this->FcaFAnu->PlaceHolder = ew_RemoveHtml($this->FcaFAnu->FldCaption());

		// FcaFTip
		$this->FcaFTip->EditAttrs["class"] = "form-control";
		$this->FcaFTip->EditCustomAttributes = "";
		$this->FcaFTip->EditValue = $this->FcaFTip->CurrentValue;
		$this->FcaFTip->PlaceHolder = ew_RemoveHtml($this->FcaFTip->FldCaption());

		// FcaUsua
		$this->FcaUsua->EditAttrs["class"] = "form-control";
		$this->FcaUsua->EditCustomAttributes = "";
		$this->FcaUsua->EditValue = $this->FcaUsua->CurrentValue;
		$this->FcaUsua->PlaceHolder = ew_RemoveHtml($this->FcaUsua->FldCaption());

		// FcaFCre
		$this->FcaFCre->EditAttrs["class"] = "form-control";
		$this->FcaFCre->EditCustomAttributes = "";
		$this->FcaFCre->EditValue = ew_FormatDateTime($this->FcaFCre->CurrentValue, 7);
		$this->FcaFCre->PlaceHolder = ew_RemoveHtml($this->FcaFCre->FldCaption());

		// Call Row Rendered event
		$this->Row_Rendered();
	}

	// Aggregate list row values
	function AggregateListRowValues() {
	}

	// Aggregate list row (for rendering)
	function AggregateListRow() {

		// Call Row Rendered event
		$this->Row_Rendered();
	}
	var $ExportDoc;

	// Export data in HTML/CSV/Word/Excel/Email/PDF format
	function ExportDocument(&$Doc, &$Recordset, $StartRec, $StopRec, $ExportPageType = "") {
		if (!$Recordset || !$Doc)
			return;
		if (!$Doc->ExportCustom) {

			// Write header
			$Doc->ExportTableHeader();
			if ($Doc->Horizontal) { // Horizontal format, write header
				$Doc->BeginExportRow();
				if ($ExportPageType == "view") {
					if ($this->FcaCodi->Exportable) $Doc->ExportCaption($this->FcaCodi);
					if ($this->OveCodi->Exportable) $Doc->ExportCaption($this->OveCodi);
					if ($this->FcaVend->Exportable) $Doc->ExportCaption($this->FcaVend);
					if ($this->FcaCaje->Exportable) $Doc->ExportCaption($this->FcaCaje);
					if ($this->FcaFech->Exportable) $Doc->ExportCaption($this->FcaFech);
					if ($this->FcaTimb->Exportable) $Doc->ExportCaption($this->FcaTimb);
					if ($this->FcaTFac->Exportable) $Doc->ExportCaption($this->FcaTFac);
					if ($this->FcaAnul->Exportable) $Doc->ExportCaption($this->FcaAnul);
					if ($this->FcaMAnu->Exportable) $Doc->ExportCaption($this->FcaMAnu);
					if ($this->FcaFAnu->Exportable) $Doc->ExportCaption($this->FcaFAnu);
					if ($this->FcaFTip->Exportable) $Doc->ExportCaption($this->FcaFTip);
					if ($this->FcaUsua->Exportable) $Doc->ExportCaption($this->FcaUsua);
					if ($this->FcaFCre->Exportable) $Doc->ExportCaption($this->FcaFCre);
				} else {
					if ($this->FcaCodi->Exportable) $Doc->ExportCaption($this->FcaCodi);
					if ($this->OveCodi->Exportable) $Doc->ExportCaption($this->OveCodi);
					if ($this->FcaVend->Exportable) $Doc->ExportCaption($this->FcaVend);
					if ($this->FcaCaje->Exportable) $Doc->ExportCaption($this->FcaCaje);
					if ($this->FcaFech->Exportable) $Doc->ExportCaption($this->FcaFech);
					if ($this->FcaTimb->Exportable) $Doc->ExportCaption($this->FcaTimb);
					if ($this->FcaTFac->Exportable) $Doc->ExportCaption($this->FcaTFac);
					if ($this->FcaAnul->Exportable) $Doc->ExportCaption($this->FcaAnul);
					if ($this->FcaMAnu->Exportable) $Doc->ExportCaption($this->FcaMAnu);
					if ($this->FcaFAnu->Exportable) $Doc->ExportCaption($this->FcaFAnu);
					if ($this->FcaFTip->Exportable) $Doc->ExportCaption($this->FcaFTip);
					if ($this->FcaUsua->Exportable) $Doc->ExportCaption($this->FcaUsua);
					if ($this->FcaFCre->Exportable) $Doc->ExportCaption($this->FcaFCre);
				}
				$Doc->EndExportRow();
			}
		}

		// Move to first record
		$RecCnt = $StartRec - 1;
		if (!$Recordset->EOF) {
			$Recordset->MoveFirst();
			if ($StartRec > 1)
				$Recordset->Move($StartRec - 1);
		}
		while (!$Recordset->EOF && $RecCnt < $StopRec) {
			$RecCnt++;
			if (intval($RecCnt) >= intval($StartRec)) {
				$RowCnt = intval($RecCnt) - intval($StartRec) + 1;

				// Page break
				if ($this->ExportPageBreakCount > 0) {
					if ($RowCnt > 1 && ($RowCnt - 1) % $this->ExportPageBreakCount == 0)
						$Doc->ExportPageBreak();
				}
				$this->LoadListRowValues($Recordset);

				// Render row
				$this->RowType = EW_ROWTYPE_VIEW; // Render view
				$this->ResetAttrs();
				$this->RenderListRow();
				if (!$Doc->ExportCustom) {
					$Doc->BeginExportRow($RowCnt); // Allow CSS styles if enabled
					if ($ExportPageType == "view") {
						if ($this->FcaCodi->Exportable) $Doc->ExportField($this->FcaCodi);
						if ($this->OveCodi->Exportable) $Doc->ExportField($this->OveCodi);
						if ($this->FcaVend->Exportable) $Doc->ExportField($this->FcaVend);
						if ($this->FcaCaje->Exportable) $Doc->ExportField($this->FcaCaje);
						if ($this->FcaFech->Exportable) $Doc->ExportField($this->FcaFech);
						if ($this->FcaTimb->Exportable) $Doc->ExportField($this->FcaTimb);
						if ($this->FcaTFac->Exportable) $Doc->ExportField($this->FcaTFac);
						if ($this->FcaAnul->Exportable) $Doc->ExportField($this->FcaAnul);
						if ($this->FcaMAnu->Exportable) $Doc->ExportField($this->FcaMAnu);
						if ($this->FcaFAnu->Exportable) $Doc->ExportField($this->FcaFAnu);
						if ($this->FcaFTip->Exportable) $Doc->ExportField($this->FcaFTip);
						if ($this->FcaUsua->Exportable) $Doc->ExportField($this->FcaUsua);
						if ($this->FcaFCre->Exportable) $Doc->ExportField($this->FcaFCre);
					} else {
						if ($this->FcaCodi->Exportable) $Doc->ExportField($this->FcaCodi);
						if ($this->OveCodi->Exportable) $Doc->ExportField($this->OveCodi);
						if ($this->FcaVend->Exportable) $Doc->ExportField($this->FcaVend);
						if ($this->FcaCaje->Exportable) $Doc->ExportField($this->FcaCaje);
						if ($this->FcaFech->Exportable) $Doc->ExportField($this->FcaFech);
						if ($this->FcaTimb->Exportable) $Doc->ExportField($this->FcaTimb);
						if ($this->FcaTFac->Exportable) $Doc->ExportField($this->FcaTFac);
						if ($this->FcaAnul->Exportable) $Doc->ExportField($this->FcaAnul);
						if ($this->FcaMAnu->Exportable) $Doc->ExportField($this->FcaMAnu);
						if ($this->FcaFAnu->Exportable) $Doc->ExportField($this->FcaFAnu);
						if ($this->FcaFTip->Exportable) $Doc->ExportField($this->FcaFTip);
						if ($this->FcaUsua->Exportable) $Doc->ExportField($this->FcaUsua);
						if ($this->FcaFCre->Exportable) $Doc->ExportField($this->FcaFCre);
					}
					$Doc->EndExportRow();
				}
			}

			// Call Row Export server event
			if ($Doc->ExportCustom)
				$this->Row_Export($Recordset->fields);
			$Recordset->MoveNext();
		}
		if (!$Doc->ExportCustom) {
			$Doc->ExportTableFooter();
		}
	}

	// Get auto fill value
	function GetAutoFill($id, $val) {
		$rsarr = array();
		$rowcnt = 0;

		// Output
		if (is_array($rsarr) && $rowcnt > 0) {
			$fldcnt = count($rsarr[0]);
			for ($i = 0; $i < $rowcnt; $i++) {
				for ($j = 0; $j < $fldcnt; $j++) {
					$str = strval($rsarr[$i][$j]);
					$str = ew_ConvertToUtf8($str);
					if (isset($post["keepCRLF"])) {
						$str = str_replace(array("\r", "\n"), array("\\r", "\\n"), $str);
					} else {
						$str = str_replace(array("\r", "\n"), array(" ", " "), $str);
					}
					$rsarr[$i][$j] = $str;
				}
			}
			return ew_ArrayToJson($rsarr);
		} else {
			return FALSE;
		}
	}

	// Table level events
	// Recordset Selecting event
	function Recordset_Selecting(&$filter) {

		// Enter your code here	
	}

	// Recordset Selected event
	function Recordset_Selected(&$rs) {

		//echo "Recordset Selected";
	}

	// Recordset Search Validated event
	function Recordset_SearchValidated() {

		// Example:
		//$this->MyField1->AdvancedSearch->SearchValue = "your search criteria"; // Search value

	}

	// Recordset Searching event
	function Recordset_Searching(&$filter) {

		// Enter your code here	
	}

	// Row_Selecting event
	function Row_Selecting(&$filter) {

		// Enter your code here	
	}

	// Row Selected event
	function Row_Selected(&$rs) {

		//echo "Row Selected";
	}

	// Row Inserting event
	function Row_Inserting($rsold, &$rsnew) {

		// Enter your code here
		// To cancel, set return value to FALSE

		return TRUE;
	}

	// Row Inserted event
	function Row_Inserted($rsold, &$rsnew) {

		//echo "Row Inserted"
	}

	// Row Updating event
	function Row_Updating($rsold, &$rsnew) {

		// Enter your code here
		// To cancel, set return value to FALSE

		return TRUE;
	}

	// Row Updated event
	function Row_Updated($rsold, &$rsnew) {

		//echo "Row Updated";
	}

	// Row Update Conflict event
	function Row_UpdateConflict($rsold, &$rsnew) {

		// Enter your code here
		// To ignore conflict, set return value to FALSE

		return TRUE;
	}

	// Grid Inserting event
	function Grid_Inserting() {

		// Enter your code here
		// To reject grid insert, set return value to FALSE

		return TRUE;
	}

	// Grid Inserted event
	function Grid_Inserted($rsnew) {

		//echo "Grid Inserted";
	}

	// Grid Updating event
	function Grid_Updating($rsold) {

		// Enter your code here
		// To reject grid update, set return value to FALSE

		return TRUE;
	}

	// Grid Updated event
	function Grid_Updated($rsold, $rsnew) {

		//echo "Grid Updated";
	}

	// Row Deleting event
	function Row_Deleting(&$rs) {

		// Enter your code here
		// To cancel, set return value to False

		return TRUE;
	}

	// Row Deleted event
	function Row_Deleted(&$rs) {

		//echo "Row Deleted";
	}

	// Email Sending event
	function Email_Sending(&$Email, &$Args) {

		//var_dump($Email); var_dump($Args); exit();
		return TRUE;
	}

	// Lookup Selecting event
	function Lookup_Selecting($fld, &$filter) {

		//var_dump($fld->FldName, $fld->LookupFilters, $filter); // Uncomment to view the filter
		// Enter your code here

	}

	// Row Rendering event
	function Row_Rendering() {

		// Enter your code here	
	}

	// Row Rendered event
	function Row_Rendered() {

		// To view properties of field class, use:
		//var_dump($this-><FieldName>); 

	}

	// User ID Filtering event
	function UserID_Filtering(&$filter) {

		// Enter your code here
	}
}
?>
