<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "VCreinfo.php" ?>
<?php include_once "Usuainfo.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$VCre_edit = NULL; // Initialize page object first

class cVCre_edit extends cVCre {

	// Page ID
	var $PageID = 'edit';

	// Project ID
	var $ProjectID = "{04439FF7-B43F-460F-8514-F71C8FF9E679}";

	// Table name
	var $TableName = 'VCre';

	// Page object name
	var $PageObjName = 'VCre_edit';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (VCre)
		if (!isset($GLOBALS["VCre"]) || get_class($GLOBALS["VCre"]) == "cVCre") {
			$GLOBALS["VCre"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["VCre"];
		}

		// Table object (Usua)
		if (!isset($GLOBALS['Usua'])) $GLOBALS['Usua'] = new cUsua();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'edit', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'VCre', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (Usua)
		if (!isset($UserTable)) {
			$UserTable = new cUsua();
			$UserTableConn = Conn($UserTable->DBID);
		}
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanEdit()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage($Language->Phrase("NoPermission")); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("VCrelist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}

		// Create form object
		$objForm = new cFormObj();
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action
		$this->VcrCodi->Visible = !$this->IsAdd() && !$this->IsCopy() && !$this->IsGridAdd();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Process auto fill
		if (@$_POST["ajax"] == "autofill") {
			$results = $this->GetAutoFill(@$_POST["name"], @$_POST["q"]);
			if ($results) {

				// Clean output buffer
				if (!EW_DEBUG_ENABLED && ob_get_length())
					ob_end_clean();
				echo $results;
				$this->Page_Terminate();
				exit();
			}
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $VCre;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($VCre);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $FormClassName = "form-horizontal ewForm ewEditForm";
	var $DbMasterFilter;
	var $DbDetailFilter;

	// 
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError;

		// Load key from QueryString
		if (@$_GET["VcrCodi"] <> "") {
			$this->VcrCodi->setQueryStringValue($_GET["VcrCodi"]);
		}

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Process form if post back
		if (@$_POST["a_edit"] <> "") {
			$this->CurrentAction = $_POST["a_edit"]; // Get action code
			$this->LoadFormValues(); // Get form values
		} else {
			$this->CurrentAction = "I"; // Default action is display
		}

		// Check if valid key
		if ($this->VcrCodi->CurrentValue == "")
			$this->Page_Terminate("VCrelist.php"); // Invalid key, return to list

		// Validate form if post back
		if (@$_POST["a_edit"] <> "") {
			if (!$this->ValidateForm()) {
				$this->CurrentAction = ""; // Form error, reset action
				$this->setFailureMessage($gsFormError);
				$this->EventCancelled = TRUE; // Event cancelled
				$this->RestoreFormValues();
			}
		}
		switch ($this->CurrentAction) {
			case "I": // Get a record to display
				if (!$this->LoadRow()) { // Load record based on key
					if ($this->getFailureMessage() == "") $this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
					$this->Page_Terminate("VCrelist.php"); // No matching record, return to list
				}
				break;
			Case "U": // Update
				$sReturnUrl = $this->getReturnUrl();
				$this->SendEmail = TRUE; // Send email on update success
				if ($this->EditRow()) { // Update record based on key
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage($Language->Phrase("UpdateSuccess")); // Update success
					$this->Page_Terminate($sReturnUrl); // Return to caller
				} elseif ($this->getFailureMessage() == $Language->Phrase("NoRecord")) {
					$this->Page_Terminate($sReturnUrl); // Return to caller
				} else {
					$this->EventCancelled = TRUE; // Event cancelled
					$this->RestoreFormValues(); // Restore form values if update failed
				}
		}

		// Render the record
		if ($this->CurrentAction == "F") { // Confirm page
			$this->RowType = EW_ROWTYPE_VIEW; // Render as View
		} else {
			$this->RowType = EW_ROWTYPE_EDIT; // Render as Edit
		}
		$this->ResetAttrs();
		$this->RenderRow();
	}

	// Set up starting record parameters
	function SetUpStartRec() {
		if ($this->DisplayRecs == 0)
			return;
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET[EW_TABLE_START_REC] <> "") { // Check for "start" parameter
				$this->StartRec = $_GET[EW_TABLE_START_REC];
				$this->setStartRecordNumber($this->StartRec);
			} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
				$PageNo = $_GET[EW_TABLE_PAGE_NO];
				if (is_numeric($PageNo)) {
					$this->StartRec = ($PageNo-1)*$this->DisplayRecs+1;
					if ($this->StartRec <= 0) {
						$this->StartRec = 1;
					} elseif ($this->StartRec >= intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1) {
						$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1;
					}
					$this->setStartRecordNumber($this->StartRec);
				}
			}
		}
		$this->StartRec = $this->getStartRecordNumber();

		// Check if correct start record counter
		if (!is_numeric($this->StartRec) || $this->StartRec == "") { // Avoid invalid start record counter
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} elseif (intval($this->StartRec) > intval($this->TotalRecs)) { // Avoid starting record > total records
			$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to last page first record
			$this->setStartRecordNumber($this->StartRec);
		} elseif (($this->StartRec-1) % $this->DisplayRecs <> 0) {
			$this->StartRec = intval(($this->StartRec-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to page boundary
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Get upload files
	function GetUploadFiles() {
		global $objForm, $Language;

		// Get upload data
	}

	// Load form values
	function LoadFormValues() {

		// Load from form
		global $objForm;
		if (!$this->VcrCodi->FldIsDetailKey)
			$this->VcrCodi->setFormValue($objForm->GetValue("x_VcrCodi"));
		if (!$this->VcrCCuo->FldIsDetailKey) {
			$this->VcrCCuo->setFormValue($objForm->GetValue("x_VcrCCuo"));
		}
		if (!$this->VcrMCuo->FldIsDetailKey) {
			$this->VcrMCuo->setFormValue($objForm->GetValue("x_VcrMCuo"));
		}
		if (!$this->VcrEIni->FldIsDetailKey) {
			$this->VcrEIni->setFormValue($objForm->GetValue("x_VcrEIni"));
		}
		if (!$this->VcrTInt->FldIsDetailKey) {
			$this->VcrTInt->setFormValue($objForm->GetValue("x_VcrTInt"));
		}
		if (!$this->VcrMTot->FldIsDetailKey) {
			$this->VcrMTot->setFormValue($objForm->GetValue("x_VcrMTot"));
		}
		if (!$this->VcrEsta->FldIsDetailKey) {
			$this->VcrEsta->setFormValue($objForm->GetValue("x_VcrEsta"));
		}
		if (!$this->VcrDes->FldIsDetailKey) {
			$this->VcrDes->setFormValue($objForm->GetValue("x_VcrDes"));
			$this->VcrDes->CurrentValue = ew_UnFormatDateTime($this->VcrDes->CurrentValue, 7);
		}
		if (!$this->VcrHas->FldIsDetailKey) {
			$this->VcrHas->setFormValue($objForm->GetValue("x_VcrHas"));
			$this->VcrHas->CurrentValue = ew_UnFormatDateTime($this->VcrHas->CurrentValue, 7);
		}
	}

	// Restore form values
	function RestoreFormValues() {
		global $objForm;
		$this->LoadRow();
		$this->VcrCodi->CurrentValue = $this->VcrCodi->FormValue;
		$this->VcrCCuo->CurrentValue = $this->VcrCCuo->FormValue;
		$this->VcrMCuo->CurrentValue = $this->VcrMCuo->FormValue;
		$this->VcrEIni->CurrentValue = $this->VcrEIni->FormValue;
		$this->VcrTInt->CurrentValue = $this->VcrTInt->FormValue;
		$this->VcrMTot->CurrentValue = $this->VcrMTot->FormValue;
		$this->VcrEsta->CurrentValue = $this->VcrEsta->FormValue;
		$this->VcrDes->CurrentValue = $this->VcrDes->FormValue;
		$this->VcrDes->CurrentValue = ew_UnFormatDateTime($this->VcrDes->CurrentValue, 7);
		$this->VcrHas->CurrentValue = $this->VcrHas->FormValue;
		$this->VcrHas->CurrentValue = ew_UnFormatDateTime($this->VcrHas->CurrentValue, 7);
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->VcrCodi->setDbValue($rs->fields('VcrCodi'));
		$this->VcrCCuo->setDbValue($rs->fields('VcrCCuo'));
		$this->VcrMCuo->setDbValue($rs->fields('VcrMCuo'));
		$this->VcrEIni->setDbValue($rs->fields('VcrEIni'));
		$this->VcrTInt->setDbValue($rs->fields('VcrTInt'));
		$this->VcrMTot->setDbValue($rs->fields('VcrMTot'));
		$this->VcrEsta->setDbValue($rs->fields('VcrEsta'));
		$this->VcrDes->setDbValue($rs->fields('VcrDes'));
		$this->VcrHas->setDbValue($rs->fields('VcrHas'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->VcrCodi->DbValue = $row['VcrCodi'];
		$this->VcrCCuo->DbValue = $row['VcrCCuo'];
		$this->VcrMCuo->DbValue = $row['VcrMCuo'];
		$this->VcrEIni->DbValue = $row['VcrEIni'];
		$this->VcrTInt->DbValue = $row['VcrTInt'];
		$this->VcrMTot->DbValue = $row['VcrMTot'];
		$this->VcrEsta->DbValue = $row['VcrEsta'];
		$this->VcrDes->DbValue = $row['VcrDes'];
		$this->VcrHas->DbValue = $row['VcrHas'];
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		// Convert decimal values if posted back

		if ($this->VcrMCuo->FormValue == $this->VcrMCuo->CurrentValue && is_numeric(ew_StrToFloat($this->VcrMCuo->CurrentValue)))
			$this->VcrMCuo->CurrentValue = ew_StrToFloat($this->VcrMCuo->CurrentValue);

		// Convert decimal values if posted back
		if ($this->VcrEIni->FormValue == $this->VcrEIni->CurrentValue && is_numeric(ew_StrToFloat($this->VcrEIni->CurrentValue)))
			$this->VcrEIni->CurrentValue = ew_StrToFloat($this->VcrEIni->CurrentValue);

		// Convert decimal values if posted back
		if ($this->VcrTInt->FormValue == $this->VcrTInt->CurrentValue && is_numeric(ew_StrToFloat($this->VcrTInt->CurrentValue)))
			$this->VcrTInt->CurrentValue = ew_StrToFloat($this->VcrTInt->CurrentValue);

		// Convert decimal values if posted back
		if ($this->VcrMTot->FormValue == $this->VcrMTot->CurrentValue && is_numeric(ew_StrToFloat($this->VcrMTot->CurrentValue)))
			$this->VcrMTot->CurrentValue = ew_StrToFloat($this->VcrMTot->CurrentValue);

		// Call Row_Rendering event
		$this->Row_Rendering();

		// Common render codes for all row types
		// VcrCodi
		// VcrCCuo
		// VcrMCuo
		// VcrEIni
		// VcrTInt
		// VcrMTot
		// VcrEsta
		// VcrDes
		// VcrHas

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// VcrCodi
		$this->VcrCodi->ViewValue = $this->VcrCodi->CurrentValue;
		$this->VcrCodi->ViewCustomAttributes = "";

		// VcrCCuo
		$this->VcrCCuo->ViewValue = $this->VcrCCuo->CurrentValue;
		$this->VcrCCuo->ViewCustomAttributes = "";

		// VcrMCuo
		$this->VcrMCuo->ViewValue = $this->VcrMCuo->CurrentValue;
		$this->VcrMCuo->ViewCustomAttributes = "";

		// VcrEIni
		$this->VcrEIni->ViewValue = $this->VcrEIni->CurrentValue;
		$this->VcrEIni->ViewCustomAttributes = "";

		// VcrTInt
		$this->VcrTInt->ViewValue = $this->VcrTInt->CurrentValue;
		$this->VcrTInt->ViewCustomAttributes = "";

		// VcrMTot
		$this->VcrMTot->ViewValue = $this->VcrMTot->CurrentValue;
		$this->VcrMTot->ViewCustomAttributes = "";

		// VcrEsta
		$this->VcrEsta->ViewValue = $this->VcrEsta->CurrentValue;
		$this->VcrEsta->ViewCustomAttributes = "";

		// VcrDes
		$this->VcrDes->ViewValue = $this->VcrDes->CurrentValue;
		$this->VcrDes->ViewValue = ew_FormatDateTime($this->VcrDes->ViewValue, 7);
		$this->VcrDes->ViewCustomAttributes = "";

		// VcrHas
		$this->VcrHas->ViewValue = $this->VcrHas->CurrentValue;
		$this->VcrHas->ViewValue = ew_FormatDateTime($this->VcrHas->ViewValue, 7);
		$this->VcrHas->ViewCustomAttributes = "";

			// VcrCodi
			$this->VcrCodi->LinkCustomAttributes = "";
			$this->VcrCodi->HrefValue = "";
			$this->VcrCodi->TooltipValue = "";

			// VcrCCuo
			$this->VcrCCuo->LinkCustomAttributes = "";
			$this->VcrCCuo->HrefValue = "";
			$this->VcrCCuo->TooltipValue = "";

			// VcrMCuo
			$this->VcrMCuo->LinkCustomAttributes = "";
			$this->VcrMCuo->HrefValue = "";
			$this->VcrMCuo->TooltipValue = "";

			// VcrEIni
			$this->VcrEIni->LinkCustomAttributes = "";
			$this->VcrEIni->HrefValue = "";
			$this->VcrEIni->TooltipValue = "";

			// VcrTInt
			$this->VcrTInt->LinkCustomAttributes = "";
			$this->VcrTInt->HrefValue = "";
			$this->VcrTInt->TooltipValue = "";

			// VcrMTot
			$this->VcrMTot->LinkCustomAttributes = "";
			$this->VcrMTot->HrefValue = "";
			$this->VcrMTot->TooltipValue = "";

			// VcrEsta
			$this->VcrEsta->LinkCustomAttributes = "";
			$this->VcrEsta->HrefValue = "";
			$this->VcrEsta->TooltipValue = "";

			// VcrDes
			$this->VcrDes->LinkCustomAttributes = "";
			$this->VcrDes->HrefValue = "";
			$this->VcrDes->TooltipValue = "";

			// VcrHas
			$this->VcrHas->LinkCustomAttributes = "";
			$this->VcrHas->HrefValue = "";
			$this->VcrHas->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_EDIT) { // Edit row

			// VcrCodi
			$this->VcrCodi->EditAttrs["class"] = "form-control";
			$this->VcrCodi->EditCustomAttributes = "";
			$this->VcrCodi->EditValue = $this->VcrCodi->CurrentValue;
			$this->VcrCodi->ViewCustomAttributes = "";

			// VcrCCuo
			$this->VcrCCuo->EditAttrs["class"] = "form-control";
			$this->VcrCCuo->EditCustomAttributes = "";
			$this->VcrCCuo->EditValue = ew_HtmlEncode($this->VcrCCuo->CurrentValue);
			$this->VcrCCuo->PlaceHolder = ew_RemoveHtml($this->VcrCCuo->FldCaption());

			// VcrMCuo
			$this->VcrMCuo->EditAttrs["class"] = "form-control";
			$this->VcrMCuo->EditCustomAttributes = "";
			$this->VcrMCuo->EditValue = ew_HtmlEncode($this->VcrMCuo->CurrentValue);
			$this->VcrMCuo->PlaceHolder = ew_RemoveHtml($this->VcrMCuo->FldCaption());
			if (strval($this->VcrMCuo->EditValue) <> "" && is_numeric($this->VcrMCuo->EditValue)) $this->VcrMCuo->EditValue = ew_FormatNumber($this->VcrMCuo->EditValue, -2, -1, -2, 0);

			// VcrEIni
			$this->VcrEIni->EditAttrs["class"] = "form-control";
			$this->VcrEIni->EditCustomAttributes = "";
			$this->VcrEIni->EditValue = ew_HtmlEncode($this->VcrEIni->CurrentValue);
			$this->VcrEIni->PlaceHolder = ew_RemoveHtml($this->VcrEIni->FldCaption());
			if (strval($this->VcrEIni->EditValue) <> "" && is_numeric($this->VcrEIni->EditValue)) $this->VcrEIni->EditValue = ew_FormatNumber($this->VcrEIni->EditValue, -2, -1, -2, 0);

			// VcrTInt
			$this->VcrTInt->EditAttrs["class"] = "form-control";
			$this->VcrTInt->EditCustomAttributes = "";
			$this->VcrTInt->EditValue = ew_HtmlEncode($this->VcrTInt->CurrentValue);
			$this->VcrTInt->PlaceHolder = ew_RemoveHtml($this->VcrTInt->FldCaption());
			if (strval($this->VcrTInt->EditValue) <> "" && is_numeric($this->VcrTInt->EditValue)) $this->VcrTInt->EditValue = ew_FormatNumber($this->VcrTInt->EditValue, -2, -1, -2, 0);

			// VcrMTot
			$this->VcrMTot->EditAttrs["class"] = "form-control";
			$this->VcrMTot->EditCustomAttributes = "";
			$this->VcrMTot->EditValue = ew_HtmlEncode($this->VcrMTot->CurrentValue);
			$this->VcrMTot->PlaceHolder = ew_RemoveHtml($this->VcrMTot->FldCaption());
			if (strval($this->VcrMTot->EditValue) <> "" && is_numeric($this->VcrMTot->EditValue)) $this->VcrMTot->EditValue = ew_FormatNumber($this->VcrMTot->EditValue, -2, -1, -2, 0);

			// VcrEsta
			$this->VcrEsta->EditAttrs["class"] = "form-control";
			$this->VcrEsta->EditCustomAttributes = "";
			$this->VcrEsta->EditValue = ew_HtmlEncode($this->VcrEsta->CurrentValue);
			$this->VcrEsta->PlaceHolder = ew_RemoveHtml($this->VcrEsta->FldCaption());

			// VcrDes
			$this->VcrDes->EditAttrs["class"] = "form-control";
			$this->VcrDes->EditCustomAttributes = "";
			$this->VcrDes->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->VcrDes->CurrentValue, 7));
			$this->VcrDes->PlaceHolder = ew_RemoveHtml($this->VcrDes->FldCaption());

			// VcrHas
			$this->VcrHas->EditAttrs["class"] = "form-control";
			$this->VcrHas->EditCustomAttributes = "";
			$this->VcrHas->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->VcrHas->CurrentValue, 7));
			$this->VcrHas->PlaceHolder = ew_RemoveHtml($this->VcrHas->FldCaption());

			// Edit refer script
			// VcrCodi

			$this->VcrCodi->HrefValue = "";

			// VcrCCuo
			$this->VcrCCuo->HrefValue = "";

			// VcrMCuo
			$this->VcrMCuo->HrefValue = "";

			// VcrEIni
			$this->VcrEIni->HrefValue = "";

			// VcrTInt
			$this->VcrTInt->HrefValue = "";

			// VcrMTot
			$this->VcrMTot->HrefValue = "";

			// VcrEsta
			$this->VcrEsta->HrefValue = "";

			// VcrDes
			$this->VcrDes->HrefValue = "";

			// VcrHas
			$this->VcrHas->HrefValue = "";
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate form
	function ValidateForm() {
		global $Language, $gsFormError;

		// Initialize form error message
		$gsFormError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return ($gsFormError == "");
		if (!$this->VcrCCuo->FldIsDetailKey && !is_null($this->VcrCCuo->FormValue) && $this->VcrCCuo->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->VcrCCuo->FldCaption(), $this->VcrCCuo->ReqErrMsg));
		}
		if (!ew_CheckInteger($this->VcrCCuo->FormValue)) {
			ew_AddMessage($gsFormError, $this->VcrCCuo->FldErrMsg());
		}
		if (!$this->VcrMCuo->FldIsDetailKey && !is_null($this->VcrMCuo->FormValue) && $this->VcrMCuo->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->VcrMCuo->FldCaption(), $this->VcrMCuo->ReqErrMsg));
		}
		if (!ew_CheckNumber($this->VcrMCuo->FormValue)) {
			ew_AddMessage($gsFormError, $this->VcrMCuo->FldErrMsg());
		}
		if (!$this->VcrEIni->FldIsDetailKey && !is_null($this->VcrEIni->FormValue) && $this->VcrEIni->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->VcrEIni->FldCaption(), $this->VcrEIni->ReqErrMsg));
		}
		if (!ew_CheckNumber($this->VcrEIni->FormValue)) {
			ew_AddMessage($gsFormError, $this->VcrEIni->FldErrMsg());
		}
		if (!$this->VcrTInt->FldIsDetailKey && !is_null($this->VcrTInt->FormValue) && $this->VcrTInt->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->VcrTInt->FldCaption(), $this->VcrTInt->ReqErrMsg));
		}
		if (!ew_CheckNumber($this->VcrTInt->FormValue)) {
			ew_AddMessage($gsFormError, $this->VcrTInt->FldErrMsg());
		}
		if (!$this->VcrMTot->FldIsDetailKey && !is_null($this->VcrMTot->FormValue) && $this->VcrMTot->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->VcrMTot->FldCaption(), $this->VcrMTot->ReqErrMsg));
		}
		if (!ew_CheckNumber($this->VcrMTot->FormValue)) {
			ew_AddMessage($gsFormError, $this->VcrMTot->FldErrMsg());
		}
		if (!$this->VcrEsta->FldIsDetailKey && !is_null($this->VcrEsta->FormValue) && $this->VcrEsta->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->VcrEsta->FldCaption(), $this->VcrEsta->ReqErrMsg));
		}
		if (!$this->VcrDes->FldIsDetailKey && !is_null($this->VcrDes->FormValue) && $this->VcrDes->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->VcrDes->FldCaption(), $this->VcrDes->ReqErrMsg));
		}
		if (!ew_CheckEuroDate($this->VcrDes->FormValue)) {
			ew_AddMessage($gsFormError, $this->VcrDes->FldErrMsg());
		}
		if (!$this->VcrHas->FldIsDetailKey && !is_null($this->VcrHas->FormValue) && $this->VcrHas->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->VcrHas->FldCaption(), $this->VcrHas->ReqErrMsg));
		}
		if (!ew_CheckEuroDate($this->VcrHas->FormValue)) {
			ew_AddMessage($gsFormError, $this->VcrHas->FldErrMsg());
		}

		// Return validate result
		$ValidateForm = ($gsFormError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateForm = $ValidateForm && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsFormError, $sFormCustomError);
		}
		return $ValidateForm;
	}

	// Update record based on key values
	function EditRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$conn = &$this->Connection();
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE)
			return FALSE;
		if ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record message
			$EditRow = FALSE; // Update Failed
		} else {

			// Save old values
			$rsold = &$rs->fields;
			$this->LoadDbValues($rsold);
			$rsnew = array();

			// VcrCCuo
			$this->VcrCCuo->SetDbValueDef($rsnew, $this->VcrCCuo->CurrentValue, 0, $this->VcrCCuo->ReadOnly);

			// VcrMCuo
			$this->VcrMCuo->SetDbValueDef($rsnew, $this->VcrMCuo->CurrentValue, 0, $this->VcrMCuo->ReadOnly);

			// VcrEIni
			$this->VcrEIni->SetDbValueDef($rsnew, $this->VcrEIni->CurrentValue, 0, $this->VcrEIni->ReadOnly);

			// VcrTInt
			$this->VcrTInt->SetDbValueDef($rsnew, $this->VcrTInt->CurrentValue, 0, $this->VcrTInt->ReadOnly);

			// VcrMTot
			$this->VcrMTot->SetDbValueDef($rsnew, $this->VcrMTot->CurrentValue, 0, $this->VcrMTot->ReadOnly);

			// VcrEsta
			$this->VcrEsta->SetDbValueDef($rsnew, $this->VcrEsta->CurrentValue, "", $this->VcrEsta->ReadOnly);

			// VcrDes
			$this->VcrDes->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->VcrDes->CurrentValue, 7), ew_CurrentDate(), $this->VcrDes->ReadOnly);

			// VcrHas
			$this->VcrHas->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->VcrHas->CurrentValue, 7), ew_CurrentDate(), $this->VcrHas->ReadOnly);

			// Call Row Updating event
			$bUpdateRow = $this->Row_Updating($rsold, $rsnew);
			if ($bUpdateRow) {
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				if (count($rsnew) > 0)
					$EditRow = $this->Update($rsnew, "", $rsold);
				else
					$EditRow = TRUE; // No field to update
				$conn->raiseErrorFn = '';
				if ($EditRow) {
				}
			} else {
				if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

					// Use the message, do nothing
				} elseif ($this->CancelMessage <> "") {
					$this->setFailureMessage($this->CancelMessage);
					$this->CancelMessage = "";
				} else {
					$this->setFailureMessage($Language->Phrase("UpdateCancelled"));
				}
				$EditRow = FALSE;
			}
		}

		// Call Row_Updated event
		if ($EditRow)
			$this->Row_Updated($rsold, $rsnew);
		$rs->Close();
		return $EditRow;
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, "VCrelist.php", "", $this->TableVar, TRUE);
		$PageId = "edit";
		$Breadcrumb->Add("edit", $PageId, $url);
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($VCre_edit)) $VCre_edit = new cVCre_edit();

// Page init
$VCre_edit->Page_Init();

// Page main
$VCre_edit->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$VCre_edit->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "edit";
var CurrentForm = fVCreedit = new ew_Form("fVCreedit", "edit");

// Validate form
fVCreedit.Validate = function() {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.GetForm(), $fobj = $(fobj);
	if ($fobj.find("#a_confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.FormKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = $fobj.find("#a_list").val() == "gridinsert";
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
			elm = this.GetElements("x" + infix + "_VcrCCuo");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $VCre->VcrCCuo->FldCaption(), $VCre->VcrCCuo->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_VcrCCuo");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($VCre->VcrCCuo->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_VcrMCuo");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $VCre->VcrMCuo->FldCaption(), $VCre->VcrMCuo->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_VcrMCuo");
			if (elm && !ew_CheckNumber(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($VCre->VcrMCuo->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_VcrEIni");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $VCre->VcrEIni->FldCaption(), $VCre->VcrEIni->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_VcrEIni");
			if (elm && !ew_CheckNumber(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($VCre->VcrEIni->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_VcrTInt");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $VCre->VcrTInt->FldCaption(), $VCre->VcrTInt->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_VcrTInt");
			if (elm && !ew_CheckNumber(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($VCre->VcrTInt->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_VcrMTot");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $VCre->VcrMTot->FldCaption(), $VCre->VcrMTot->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_VcrMTot");
			if (elm && !ew_CheckNumber(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($VCre->VcrMTot->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_VcrEsta");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $VCre->VcrEsta->FldCaption(), $VCre->VcrEsta->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_VcrDes");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $VCre->VcrDes->FldCaption(), $VCre->VcrDes->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_VcrDes");
			if (elm && !ew_CheckEuroDate(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($VCre->VcrDes->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_VcrHas");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $VCre->VcrHas->FldCaption(), $VCre->VcrHas->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_VcrHas");
			if (elm && !ew_CheckEuroDate(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($VCre->VcrHas->FldErrMsg()) ?>");

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
	}

	// Process detail forms
	var dfs = $fobj.find("input[name='detailpage']").get();
	for (var i = 0; i < dfs.length; i++) {
		var df = dfs[i], val = df.value;
		if (val && ewForms[val])
			if (!ewForms[val].Validate())
				return false;
	}
	return true;
}

// Form_CustomValidate event
fVCreedit.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fVCreedit.ValidateRequired = true;
<?php } else { ?>
fVCreedit.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
// Form object for search

</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php $VCre_edit->ShowPageHeader(); ?>
<?php
$VCre_edit->ShowMessage();
?>
<form name="fVCreedit" id="fVCreedit" class="<?php echo $VCre_edit->FormClassName ?>" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($VCre_edit->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $VCre_edit->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="VCre">
<input type="hidden" name="a_edit" id="a_edit" value="U">
<?php if ($VCre->CurrentAction == "F") { // Confirm page ?>
<input type="hidden" name="a_confirm" id="a_confirm" value="F">
<?php } ?>
<div>
<?php if ($VCre->VcrCodi->Visible) { // VcrCodi ?>
	<div id="r_VcrCodi" class="form-group">
		<label id="elh_VCre_VcrCodi" class="col-sm-2 control-label ewLabel"><?php echo $VCre->VcrCodi->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $VCre->VcrCodi->CellAttributes() ?>>
<?php if ($VCre->CurrentAction <> "F") { ?>
<span id="el_VCre_VcrCodi">
<span<?php echo $VCre->VcrCodi->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $VCre->VcrCodi->EditValue ?></p></span>
</span>
<input type="hidden" data-table="VCre" data-field="x_VcrCodi" name="x_VcrCodi" id="x_VcrCodi" value="<?php echo ew_HtmlEncode($VCre->VcrCodi->CurrentValue) ?>">
<?php } else { ?>
<span id="el_VCre_VcrCodi">
<span<?php echo $VCre->VcrCodi->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $VCre->VcrCodi->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="VCre" data-field="x_VcrCodi" name="x_VcrCodi" id="x_VcrCodi" value="<?php echo ew_HtmlEncode($VCre->VcrCodi->FormValue) ?>">
<?php } ?>
<?php echo $VCre->VcrCodi->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($VCre->VcrCCuo->Visible) { // VcrCCuo ?>
	<div id="r_VcrCCuo" class="form-group">
		<label id="elh_VCre_VcrCCuo" for="x_VcrCCuo" class="col-sm-2 control-label ewLabel"><?php echo $VCre->VcrCCuo->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $VCre->VcrCCuo->CellAttributes() ?>>
<?php if ($VCre->CurrentAction <> "F") { ?>
<span id="el_VCre_VcrCCuo">
<input type="text" data-table="VCre" data-field="x_VcrCCuo" name="x_VcrCCuo" id="x_VcrCCuo" size="30" placeholder="<?php echo ew_HtmlEncode($VCre->VcrCCuo->getPlaceHolder()) ?>" value="<?php echo $VCre->VcrCCuo->EditValue ?>"<?php echo $VCre->VcrCCuo->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_VCre_VcrCCuo">
<span<?php echo $VCre->VcrCCuo->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $VCre->VcrCCuo->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="VCre" data-field="x_VcrCCuo" name="x_VcrCCuo" id="x_VcrCCuo" value="<?php echo ew_HtmlEncode($VCre->VcrCCuo->FormValue) ?>">
<?php } ?>
<?php echo $VCre->VcrCCuo->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($VCre->VcrMCuo->Visible) { // VcrMCuo ?>
	<div id="r_VcrMCuo" class="form-group">
		<label id="elh_VCre_VcrMCuo" for="x_VcrMCuo" class="col-sm-2 control-label ewLabel"><?php echo $VCre->VcrMCuo->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $VCre->VcrMCuo->CellAttributes() ?>>
<?php if ($VCre->CurrentAction <> "F") { ?>
<span id="el_VCre_VcrMCuo">
<input type="text" data-table="VCre" data-field="x_VcrMCuo" name="x_VcrMCuo" id="x_VcrMCuo" size="30" placeholder="<?php echo ew_HtmlEncode($VCre->VcrMCuo->getPlaceHolder()) ?>" value="<?php echo $VCre->VcrMCuo->EditValue ?>"<?php echo $VCre->VcrMCuo->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_VCre_VcrMCuo">
<span<?php echo $VCre->VcrMCuo->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $VCre->VcrMCuo->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="VCre" data-field="x_VcrMCuo" name="x_VcrMCuo" id="x_VcrMCuo" value="<?php echo ew_HtmlEncode($VCre->VcrMCuo->FormValue) ?>">
<?php } ?>
<?php echo $VCre->VcrMCuo->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($VCre->VcrEIni->Visible) { // VcrEIni ?>
	<div id="r_VcrEIni" class="form-group">
		<label id="elh_VCre_VcrEIni" for="x_VcrEIni" class="col-sm-2 control-label ewLabel"><?php echo $VCre->VcrEIni->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $VCre->VcrEIni->CellAttributes() ?>>
<?php if ($VCre->CurrentAction <> "F") { ?>
<span id="el_VCre_VcrEIni">
<input type="text" data-table="VCre" data-field="x_VcrEIni" name="x_VcrEIni" id="x_VcrEIni" size="30" placeholder="<?php echo ew_HtmlEncode($VCre->VcrEIni->getPlaceHolder()) ?>" value="<?php echo $VCre->VcrEIni->EditValue ?>"<?php echo $VCre->VcrEIni->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_VCre_VcrEIni">
<span<?php echo $VCre->VcrEIni->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $VCre->VcrEIni->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="VCre" data-field="x_VcrEIni" name="x_VcrEIni" id="x_VcrEIni" value="<?php echo ew_HtmlEncode($VCre->VcrEIni->FormValue) ?>">
<?php } ?>
<?php echo $VCre->VcrEIni->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($VCre->VcrTInt->Visible) { // VcrTInt ?>
	<div id="r_VcrTInt" class="form-group">
		<label id="elh_VCre_VcrTInt" for="x_VcrTInt" class="col-sm-2 control-label ewLabel"><?php echo $VCre->VcrTInt->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $VCre->VcrTInt->CellAttributes() ?>>
<?php if ($VCre->CurrentAction <> "F") { ?>
<span id="el_VCre_VcrTInt">
<input type="text" data-table="VCre" data-field="x_VcrTInt" name="x_VcrTInt" id="x_VcrTInt" size="30" placeholder="<?php echo ew_HtmlEncode($VCre->VcrTInt->getPlaceHolder()) ?>" value="<?php echo $VCre->VcrTInt->EditValue ?>"<?php echo $VCre->VcrTInt->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_VCre_VcrTInt">
<span<?php echo $VCre->VcrTInt->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $VCre->VcrTInt->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="VCre" data-field="x_VcrTInt" name="x_VcrTInt" id="x_VcrTInt" value="<?php echo ew_HtmlEncode($VCre->VcrTInt->FormValue) ?>">
<?php } ?>
<?php echo $VCre->VcrTInt->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($VCre->VcrMTot->Visible) { // VcrMTot ?>
	<div id="r_VcrMTot" class="form-group">
		<label id="elh_VCre_VcrMTot" for="x_VcrMTot" class="col-sm-2 control-label ewLabel"><?php echo $VCre->VcrMTot->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $VCre->VcrMTot->CellAttributes() ?>>
<?php if ($VCre->CurrentAction <> "F") { ?>
<span id="el_VCre_VcrMTot">
<input type="text" data-table="VCre" data-field="x_VcrMTot" name="x_VcrMTot" id="x_VcrMTot" size="30" placeholder="<?php echo ew_HtmlEncode($VCre->VcrMTot->getPlaceHolder()) ?>" value="<?php echo $VCre->VcrMTot->EditValue ?>"<?php echo $VCre->VcrMTot->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_VCre_VcrMTot">
<span<?php echo $VCre->VcrMTot->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $VCre->VcrMTot->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="VCre" data-field="x_VcrMTot" name="x_VcrMTot" id="x_VcrMTot" value="<?php echo ew_HtmlEncode($VCre->VcrMTot->FormValue) ?>">
<?php } ?>
<?php echo $VCre->VcrMTot->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($VCre->VcrEsta->Visible) { // VcrEsta ?>
	<div id="r_VcrEsta" class="form-group">
		<label id="elh_VCre_VcrEsta" for="x_VcrEsta" class="col-sm-2 control-label ewLabel"><?php echo $VCre->VcrEsta->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $VCre->VcrEsta->CellAttributes() ?>>
<?php if ($VCre->CurrentAction <> "F") { ?>
<span id="el_VCre_VcrEsta">
<input type="text" data-table="VCre" data-field="x_VcrEsta" name="x_VcrEsta" id="x_VcrEsta" size="30" maxlength="1" placeholder="<?php echo ew_HtmlEncode($VCre->VcrEsta->getPlaceHolder()) ?>" value="<?php echo $VCre->VcrEsta->EditValue ?>"<?php echo $VCre->VcrEsta->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_VCre_VcrEsta">
<span<?php echo $VCre->VcrEsta->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $VCre->VcrEsta->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="VCre" data-field="x_VcrEsta" name="x_VcrEsta" id="x_VcrEsta" value="<?php echo ew_HtmlEncode($VCre->VcrEsta->FormValue) ?>">
<?php } ?>
<?php echo $VCre->VcrEsta->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($VCre->VcrDes->Visible) { // VcrDes ?>
	<div id="r_VcrDes" class="form-group">
		<label id="elh_VCre_VcrDes" for="x_VcrDes" class="col-sm-2 control-label ewLabel"><?php echo $VCre->VcrDes->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $VCre->VcrDes->CellAttributes() ?>>
<?php if ($VCre->CurrentAction <> "F") { ?>
<span id="el_VCre_VcrDes">
<input type="text" data-table="VCre" data-field="x_VcrDes" data-format="7" name="x_VcrDes" id="x_VcrDes" placeholder="<?php echo ew_HtmlEncode($VCre->VcrDes->getPlaceHolder()) ?>" value="<?php echo $VCre->VcrDes->EditValue ?>"<?php echo $VCre->VcrDes->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_VCre_VcrDes">
<span<?php echo $VCre->VcrDes->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $VCre->VcrDes->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="VCre" data-field="x_VcrDes" name="x_VcrDes" id="x_VcrDes" value="<?php echo ew_HtmlEncode($VCre->VcrDes->FormValue) ?>">
<?php } ?>
<?php echo $VCre->VcrDes->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($VCre->VcrHas->Visible) { // VcrHas ?>
	<div id="r_VcrHas" class="form-group">
		<label id="elh_VCre_VcrHas" for="x_VcrHas" class="col-sm-2 control-label ewLabel"><?php echo $VCre->VcrHas->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $VCre->VcrHas->CellAttributes() ?>>
<?php if ($VCre->CurrentAction <> "F") { ?>
<span id="el_VCre_VcrHas">
<input type="text" data-table="VCre" data-field="x_VcrHas" data-format="7" name="x_VcrHas" id="x_VcrHas" placeholder="<?php echo ew_HtmlEncode($VCre->VcrHas->getPlaceHolder()) ?>" value="<?php echo $VCre->VcrHas->EditValue ?>"<?php echo $VCre->VcrHas->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_VCre_VcrHas">
<span<?php echo $VCre->VcrHas->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $VCre->VcrHas->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="VCre" data-field="x_VcrHas" name="x_VcrHas" id="x_VcrHas" value="<?php echo ew_HtmlEncode($VCre->VcrHas->FormValue) ?>">
<?php } ?>
<?php echo $VCre->VcrHas->CustomMsg ?></div></div>
	</div>
<?php } ?>
</div>
<div class="form-group">
	<div class="col-sm-offset-2 col-sm-10">
<?php if ($VCre->CurrentAction <> "F") { // Confirm page ?>
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit" onclick="this.form.a_edit.value='F';"><?php echo $Language->Phrase("SaveBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="button" data-href="<?php echo $VCre_edit->getReturnUrl() ?>"><?php echo $Language->Phrase("CancelBtn") ?></button>
<?php } else { ?>
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("ConfirmBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="submit" onclick="this.form.a_edit.value='X';"><?php echo $Language->Phrase("CancelBtn") ?></button>
<?php } ?>
	</div>
</div>
</form>
<script type="text/javascript">
fVCreedit.Init();
</script>
<?php
$VCre_edit->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$VCre_edit->Page_Terminate();
?>
