<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "Provinfo.php" ?>
<?php include_once "Usuainfo.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$Prov_list = NULL; // Initialize page object first

class cProv_list extends cProv {

	// Page ID
	var $PageID = 'list';

	// Project ID
	var $ProjectID = "{04439FF7-B43F-460F-8514-F71C8FF9E679}";

	// Table name
	var $TableName = 'Prov';

	// Page object name
	var $PageObjName = 'Prov_list';

	// Grid form hidden field names
	var $FormName = 'fProvlist';
	var $FormActionName = 'k_action';
	var $FormKeyName = 'k_key';
	var $FormOldKeyName = 'k_oldkey';
	var $FormBlankRowName = 'k_blankrow';
	var $FormKeyCountName = 'key_count';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Page URLs
	var $AddUrl;
	var $EditUrl;
	var $CopyUrl;
	var $DeleteUrl;
	var $ViewUrl;
	var $ListUrl;

	// Export URLs
	var $ExportPrintUrl;
	var $ExportHtmlUrl;
	var $ExportExcelUrl;
	var $ExportWordUrl;
	var $ExportXmlUrl;
	var $ExportCsvUrl;
	var $ExportPdfUrl;

	// Custom export
	var $ExportExcelCustom = FALSE;
	var $ExportWordCustom = FALSE;
	var $ExportPdfCustom = FALSE;
	var $ExportEmailCustom = FALSE;

	// Update URLs
	var $InlineAddUrl;
	var $InlineCopyUrl;
	var $InlineEditUrl;
	var $GridAddUrl;
	var $GridEditUrl;
	var $MultiDeleteUrl;
	var $MultiUpdateUrl;

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (Prov)
		if (!isset($GLOBALS["Prov"]) || get_class($GLOBALS["Prov"]) == "cProv") {
			$GLOBALS["Prov"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["Prov"];
		}

		// Initialize URLs
		$this->ExportPrintUrl = $this->PageUrl() . "export=print";
		$this->ExportExcelUrl = $this->PageUrl() . "export=excel";
		$this->ExportWordUrl = $this->PageUrl() . "export=word";
		$this->ExportHtmlUrl = $this->PageUrl() . "export=html";
		$this->ExportXmlUrl = $this->PageUrl() . "export=xml";
		$this->ExportCsvUrl = $this->PageUrl() . "export=csv";
		$this->ExportPdfUrl = $this->PageUrl() . "export=pdf";
		$this->AddUrl = "Provadd.php";
		$this->InlineAddUrl = $this->PageUrl() . "a=add";
		$this->GridAddUrl = $this->PageUrl() . "a=gridadd";
		$this->GridEditUrl = $this->PageUrl() . "a=gridedit";
		$this->MultiDeleteUrl = "Provdelete.php";
		$this->MultiUpdateUrl = "Provupdate.php";

		// Table object (Usua)
		if (!isset($GLOBALS['Usua'])) $GLOBALS['Usua'] = new cUsua();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'list', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'Prov', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (Usua)
		if (!isset($UserTable)) {
			$UserTable = new cUsua();
			$UserTableConn = Conn($UserTable->DBID);
		}

		// List options
		$this->ListOptions = new cListOptions();
		$this->ListOptions->TableVar = $this->TableVar;

		// Export options
		$this->ExportOptions = new cListOptions();
		$this->ExportOptions->Tag = "div";
		$this->ExportOptions->TagClassName = "ewExportOption";

		// Other options
		$this->OtherOptions['addedit'] = new cListOptions();
		$this->OtherOptions['addedit']->Tag = "div";
		$this->OtherOptions['addedit']->TagClassName = "ewAddEditOption";
		$this->OtherOptions['detail'] = new cListOptions();
		$this->OtherOptions['detail']->Tag = "div";
		$this->OtherOptions['detail']->TagClassName = "ewDetailOption";
		$this->OtherOptions['action'] = new cListOptions();
		$this->OtherOptions['action']->Tag = "div";
		$this->OtherOptions['action']->TagClassName = "ewActionOption";

		// Filter options
		$this->FilterOptions = new cListOptions();
		$this->FilterOptions->Tag = "div";
		$this->FilterOptions->TagClassName = "ewFilterOption fProvlistsrch";

		// List actions
		$this->ListActions = new cListActions();
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanList()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage($Language->Phrase("NoPermission")); // Set no permission
			$this->Page_Terminate(ew_GetUrl("index.php"));
		}

		// Create form object
		$objForm = new cFormObj();
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action

		// Get grid add count
		$gridaddcnt = @$_GET[EW_TABLE_GRID_ADD_ROW_COUNT];
		if (is_numeric($gridaddcnt) && $gridaddcnt > 0)
			$this->GridAddRowCount = $gridaddcnt;

		// Set up list options
		$this->SetupListOptions();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Process auto fill
		if (@$_POST["ajax"] == "autofill") {
			$results = $this->GetAutoFill(@$_POST["name"], @$_POST["q"]);
			if ($results) {

				// Clean output buffer
				if (!EW_DEBUG_ENABLED && ob_get_length())
					ob_end_clean();
				echo $results;
				$this->Page_Terminate();
				exit();
			}
		}

		// Create Token
		$this->CreateToken();

		// Setup other options
		$this->SetupOtherOptions();

		// Set up custom action (compatible with old version)
		foreach ($this->CustomActions as $name => $action)
			$this->ListActions->Add($name, $action);

		// Show checkbox column if multiple action
		foreach ($this->ListActions->Items as $listaction) {
			if ($listaction->Select == EW_ACTION_MULTIPLE && $listaction->Allow) {
				$this->ListOptions->Items["checkbox"]->Visible = TRUE;
				break;
			}
		}
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $Prov;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($Prov);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}

	// Class variables
	var $ListOptions; // List options
	var $ExportOptions; // Export options
	var $SearchOptions; // Search options
	var $OtherOptions = array(); // Other options
	var $FilterOptions; // Filter options
	var $ListActions; // List actions
	var $SelectedCount = 0;
	var $SelectedIndex = 0;
	var $DisplayRecs = 20;
	var $StartRec;
	var $StopRec;
	var $TotalRecs = 0;
	var $RecRange = 10;
	var $Pager;
	var $DefaultSearchWhere = ""; // Default search WHERE clause
	var $SearchWhere = ""; // Search WHERE clause
	var $RecCnt = 0; // Record count
	var $EditRowCnt;
	var $StartRowCnt = 1;
	var $RowCnt = 0;
	var $Attrs = array(); // Row attributes and cell attributes
	var $RowIndex = 0; // Row index
	var $KeyCount = 0; // Key count
	var $RowAction = ""; // Row action
	var $RowOldKey = ""; // Row old key (for copy)
	var $RecPerRow = 0;
	var $MultiColumnClass;
	var $MultiColumnEditClass = "col-sm-12";
	var $MultiColumnCnt = 12;
	var $MultiColumnEditCnt = 12;
	var $GridCnt = 0;
	var $ColCnt = 0;
	var $DbMasterFilter = ""; // Master filter
	var $DbDetailFilter = ""; // Detail filter
	var $MasterRecordExists;	
	var $MultiSelectKey;
	var $Command;
	var $RestoreSearch = FALSE;
	var $DetailPages;
	var $Recordset;
	var $OldRecordset;

	//
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError, $gsSearchError, $Security;

		// Search filters
		$sSrchAdvanced = ""; // Advanced search filter
		$sSrchBasic = ""; // Basic search filter
		$sFilter = "";

		// Get command
		$this->Command = strtolower(@$_GET["cmd"]);
		if ($this->IsPageRequest()) { // Validate request

			// Process list action first
			if ($this->ProcessListAction()) // Ajax request
				$this->Page_Terminate();

			// Handle reset command
			$this->ResetCmd();

			// Set up Breadcrumb
			if ($this->Export == "")
				$this->SetupBreadcrumb();

			// Check QueryString parameters
			if (@$_GET["a"] <> "") {
				$this->CurrentAction = $_GET["a"];

				// Clear inline mode
				if ($this->CurrentAction == "cancel")
					$this->ClearInlineMode();

				// Switch to grid edit mode
				if ($this->CurrentAction == "gridedit")
					$this->GridEditMode();

				// Switch to grid add mode
				if ($this->CurrentAction == "gridadd")
					$this->GridAddMode();
			} else {
				if (@$_POST["a_list"] <> "") {
					$this->CurrentAction = $_POST["a_list"]; // Get action

					// Grid Update
					if (($this->CurrentAction == "gridupdate" || $this->CurrentAction == "gridoverwrite") && @$_SESSION[EW_SESSION_INLINE_MODE] == "gridedit") {
						if ($this->ValidateGridForm()) {
							$bGridUpdate = $this->GridUpdate();
						} else {
							$bGridUpdate = FALSE;
							$this->setFailureMessage($gsFormError);
						}
						if (!$bGridUpdate) {
							$this->EventCancelled = TRUE;
							$this->CurrentAction = "gridedit"; // Stay in Grid Edit mode
						}
					}

					// Grid Insert
					if ($this->CurrentAction == "gridinsert" && @$_SESSION[EW_SESSION_INLINE_MODE] == "gridadd") {
						if ($this->ValidateGridForm()) {
							$bGridInsert = $this->GridInsert();
						} else {
							$bGridInsert = FALSE;
							$this->setFailureMessage($gsFormError);
						}
						if (!$bGridInsert) {
							$this->EventCancelled = TRUE;
							$this->CurrentAction = "gridadd"; // Stay in Grid Add mode
						}
					}
				}
			}

			// Hide list options
			if ($this->Export <> "") {
				$this->ListOptions->HideAllOptions(array("sequence"));
				$this->ListOptions->UseDropDownButton = FALSE; // Disable drop down button
				$this->ListOptions->UseButtonGroup = FALSE; // Disable button group
			} elseif ($this->CurrentAction == "gridadd" || $this->CurrentAction == "gridedit") {
				$this->ListOptions->HideAllOptions();
				$this->ListOptions->UseDropDownButton = FALSE; // Disable drop down button
				$this->ListOptions->UseButtonGroup = FALSE; // Disable button group
			}

			// Hide options
			if ($this->Export <> "" || $this->CurrentAction <> "") {
				$this->ExportOptions->HideAllOptions();
				$this->FilterOptions->HideAllOptions();
			}

			// Hide other options
			if ($this->Export <> "") {
				foreach ($this->OtherOptions as &$option)
					$option->HideAllOptions();
			}

			// Show grid delete link for grid add / grid edit
			if ($this->AllowAddDeleteRow) {
				if ($this->CurrentAction == "gridadd" || $this->CurrentAction == "gridedit") {
					$item = $this->ListOptions->GetItem("griddelete");
					if ($item) $item->Visible = TRUE;
				}
			}

			// Get default search criteria
			ew_AddFilter($this->DefaultSearchWhere, $this->BasicSearchWhere(TRUE));

			// Get basic search values
			$this->LoadBasicSearchValues();

			// Restore filter list
			$this->RestoreFilterList();

			// Restore search parms from Session if not searching / reset / export
			if (($this->Export <> "" || $this->Command <> "search" && $this->Command <> "reset" && $this->Command <> "resetall") && $this->CheckSearchParms())
				$this->RestoreSearchParms();

			// Call Recordset SearchValidated event
			$this->Recordset_SearchValidated();

			// Set up sorting order
			$this->SetUpSortOrder();

			// Get basic search criteria
			if ($gsSearchError == "")
				$sSrchBasic = $this->BasicSearchWhere();
		}

		// Restore display records
		if ($this->getRecordsPerPage() <> "") {
			$this->DisplayRecs = $this->getRecordsPerPage(); // Restore from Session
		} else {
			$this->DisplayRecs = 20; // Load default
		}

		// Load Sorting Order
		$this->LoadSortOrder();

		// Load search default if no existing search criteria
		if (!$this->CheckSearchParms()) {

			// Load basic search from default
			$this->BasicSearch->LoadDefault();
			if ($this->BasicSearch->Keyword != "")
				$sSrchBasic = $this->BasicSearchWhere();
		}

		// Build search criteria
		ew_AddFilter($this->SearchWhere, $sSrchAdvanced);
		ew_AddFilter($this->SearchWhere, $sSrchBasic);

		// Call Recordset_Searching event
		$this->Recordset_Searching($this->SearchWhere);

		// Save search criteria
		if ($this->Command == "search" && !$this->RestoreSearch) {
			$this->setSearchWhere($this->SearchWhere); // Save to Session
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} else {
			$this->SearchWhere = $this->getSearchWhere();
		}

		// Build filter
		$sFilter = "";
		if (!$Security->CanList())
			$sFilter = "(0=1)"; // Filter all records
		ew_AddFilter($sFilter, $this->DbDetailFilter);
		ew_AddFilter($sFilter, $this->SearchWhere);

		// Set up filter in session
		$this->setSessionWhere($sFilter);
		$this->CurrentFilter = "";

		// Load record count first
		if (!$this->IsAddOrEdit()) {
			$bSelectLimit = $this->UseSelectLimit;
			if ($bSelectLimit) {
				$this->TotalRecs = $this->SelectRecordCount();
			} else {
				if ($this->Recordset = $this->LoadRecordset())
					$this->TotalRecs = $this->Recordset->RecordCount();
			}
		}

		// Search options
		$this->SetupSearchOptions();
	}

	//  Exit inline mode
	function ClearInlineMode() {
		$this->LastAction = $this->CurrentAction; // Save last action
		$this->CurrentAction = ""; // Clear action
		$_SESSION[EW_SESSION_INLINE_MODE] = ""; // Clear inline mode
	}

	// Switch to Grid Add mode
	function GridAddMode() {
		$_SESSION[EW_SESSION_INLINE_MODE] = "gridadd"; // Enabled grid add
	}

	// Switch to Grid Edit mode
	function GridEditMode() {
		$_SESSION[EW_SESSION_INLINE_MODE] = "gridedit"; // Enable grid edit
	}

	// Perform update to grid
	function GridUpdate() {
		global $Language, $objForm, $gsFormError;
		$bGridUpdate = TRUE;

		// Get old recordset
		$this->CurrentFilter = $this->BuildKeyFilter();
		if ($this->CurrentFilter == "")
			$this->CurrentFilter = "0=1";
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		if ($rs = $conn->Execute($sSql)) {
			$rsold = $rs->GetRows();
			$rs->Close();
		}

		// Call Grid Updating event
		if (!$this->Grid_Updating($rsold)) {
			if ($this->getFailureMessage() == "")
				$this->setFailureMessage($Language->Phrase("GridEditCancelled")); // Set grid edit cancelled message
			return FALSE;
		}

		// Begin transaction
		$conn->BeginTrans();
		$sKey = "";

		// Update row index and get row key
		$objForm->Index = -1;
		$rowcnt = strval($objForm->GetValue($this->FormKeyCountName));
		if ($rowcnt == "" || !is_numeric($rowcnt))
			$rowcnt = 0;

		// Update all rows based on key
		for ($rowindex = 1; $rowindex <= $rowcnt; $rowindex++) {
			$objForm->Index = $rowindex;
			$rowkey = strval($objForm->GetValue($this->FormKeyName));
			$rowaction = strval($objForm->GetValue($this->FormActionName));

			// Load all values and keys
			if ($rowaction <> "insertdelete") { // Skip insert then deleted rows
				$this->LoadFormValues(); // Get form values
				if ($rowaction == "" || $rowaction == "edit" || $rowaction == "delete") {
					$bGridUpdate = $this->SetupKeyValues($rowkey); // Set up key values
				} else {
					$bGridUpdate = TRUE;
				}

				// Skip empty row
				if ($rowaction == "insert" && $this->EmptyRow()) {

					// No action required
				// Validate form and insert/update/delete record

				} elseif ($bGridUpdate) {
					if ($rowaction == "delete") {
						$this->CurrentFilter = $this->KeyFilter();
						$bGridUpdate = $this->DeleteRows(); // Delete this row
					} else if (!$this->ValidateForm()) {
						$bGridUpdate = FALSE; // Form error, reset action
						$this->setFailureMessage($gsFormError);
					} else {
						if ($rowaction == "insert") {
							$bGridUpdate = $this->AddRow(); // Insert this row
						} else {
							if ($rowkey <> "") {
								$this->SendEmail = FALSE; // Do not send email on update success
								$bGridUpdate = $this->EditRow(); // Update this row
							}
						} // End update
					}
				}
				if ($bGridUpdate) {
					if ($sKey <> "") $sKey .= ", ";
					$sKey .= $rowkey;
				} else {
					break;
				}
			}
		}
		if ($bGridUpdate) {
			$conn->CommitTrans(); // Commit transaction

			// Get new recordset
			if ($rs = $conn->Execute($sSql)) {
				$rsnew = $rs->GetRows();
				$rs->Close();
			}

			// Call Grid_Updated event
			$this->Grid_Updated($rsold, $rsnew);
			if ($this->getSuccessMessage() == "")
				$this->setSuccessMessage($Language->Phrase("UpdateSuccess")); // Set up update success message
			$this->ClearInlineMode(); // Clear inline edit mode
		} else {
			$conn->RollbackTrans(); // Rollback transaction
			if ($this->getFailureMessage() == "")
				$this->setFailureMessage($Language->Phrase("UpdateFailed")); // Set update failed message
		}
		return $bGridUpdate;
	}

	// Build filter for all keys
	function BuildKeyFilter() {
		global $objForm;
		$sWrkFilter = "";

		// Update row index and get row key
		$rowindex = 1;
		$objForm->Index = $rowindex;
		$sThisKey = strval($objForm->GetValue($this->FormKeyName));
		while ($sThisKey <> "") {
			if ($this->SetupKeyValues($sThisKey)) {
				$sFilter = $this->KeyFilter();
				if ($sWrkFilter <> "") $sWrkFilter .= " OR ";
				$sWrkFilter .= $sFilter;
			} else {
				$sWrkFilter = "0=1";
				break;
			}

			// Update row index and get row key
			$rowindex++; // Next row
			$objForm->Index = $rowindex;
			$sThisKey = strval($objForm->GetValue($this->FormKeyName));
		}
		return $sWrkFilter;
	}

	// Set up key values
	function SetupKeyValues($key) {
		$arrKeyFlds = explode($GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"], $key);
		if (count($arrKeyFlds) >= 1) {
			$this->PrvCodi->setFormValue($arrKeyFlds[0]);
			if (!is_numeric($this->PrvCodi->FormValue))
				return FALSE;
		}
		return TRUE;
	}

	// Perform Grid Add
	function GridInsert() {
		global $Language, $objForm, $gsFormError;
		$rowindex = 1;
		$bGridInsert = FALSE;
		$conn = &$this->Connection();

		// Call Grid Inserting event
		if (!$this->Grid_Inserting()) {
			if ($this->getFailureMessage() == "") {
				$this->setFailureMessage($Language->Phrase("GridAddCancelled")); // Set grid add cancelled message
			}
			return FALSE;
		}

		// Begin transaction
		$conn->BeginTrans();

		// Init key filter
		$sWrkFilter = "";
		$addcnt = 0;
		$sKey = "";

		// Get row count
		$objForm->Index = -1;
		$rowcnt = strval($objForm->GetValue($this->FormKeyCountName));
		if ($rowcnt == "" || !is_numeric($rowcnt))
			$rowcnt = 0;

		// Insert all rows
		for ($rowindex = 1; $rowindex <= $rowcnt; $rowindex++) {

			// Load current row values
			$objForm->Index = $rowindex;
			$rowaction = strval($objForm->GetValue($this->FormActionName));
			if ($rowaction <> "" && $rowaction <> "insert")
				continue; // Skip
			$this->LoadFormValues(); // Get form values
			if (!$this->EmptyRow()) {
				$addcnt++;
				$this->SendEmail = FALSE; // Do not send email on insert success

				// Validate form
				if (!$this->ValidateForm()) {
					$bGridInsert = FALSE; // Form error, reset action
					$this->setFailureMessage($gsFormError);
				} else {
					$bGridInsert = $this->AddRow($this->OldRecordset); // Insert this row
				}
				if ($bGridInsert) {
					if ($sKey <> "") $sKey .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
					$sKey .= $this->PrvCodi->CurrentValue;

					// Add filter for this record
					$sFilter = $this->KeyFilter();
					if ($sWrkFilter <> "") $sWrkFilter .= " OR ";
					$sWrkFilter .= $sFilter;
				} else {
					break;
				}
			}
		}
		if ($addcnt == 0) { // No record inserted
			$this->setFailureMessage($Language->Phrase("NoAddRecord"));
			$bGridInsert = FALSE;
		}
		if ($bGridInsert) {
			$conn->CommitTrans(); // Commit transaction

			// Get new recordset
			$this->CurrentFilter = $sWrkFilter;
			$sSql = $this->SQL();
			if ($rs = $conn->Execute($sSql)) {
				$rsnew = $rs->GetRows();
				$rs->Close();
			}

			// Call Grid_Inserted event
			$this->Grid_Inserted($rsnew);
			if ($this->getSuccessMessage() == "")
				$this->setSuccessMessage($Language->Phrase("InsertSuccess")); // Set up insert success message
			$this->ClearInlineMode(); // Clear grid add mode
		} else {
			$conn->RollbackTrans(); // Rollback transaction
			if ($this->getFailureMessage() == "") {
				$this->setFailureMessage($Language->Phrase("InsertFailed")); // Set insert failed message
			}
		}
		return $bGridInsert;
	}

	// Check if empty row
	function EmptyRow() {
		global $objForm;
		if ($objForm->HasValue("x_PrvFIni") && $objForm->HasValue("o_PrvFIni") && $this->PrvFIni->CurrentValue <> $this->PrvFIni->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_PrvFFin") && $objForm->HasValue("o_PrvFFin") && $this->PrvFFin->CurrentValue <> $this->PrvFFin->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_PrvPais") && $objForm->HasValue("o_PrvPais") && $this->PrvPais->CurrentValue <> $this->PrvPais->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_PrvTipo") && $objForm->HasValue("o_PrvTipo") && $this->PrvTipo->CurrentValue <> $this->PrvTipo->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_PrvNomb") && $objForm->HasValue("o_PrvNomb") && $this->PrvNomb->CurrentValue <> $this->PrvNomb->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_PrvApel") && $objForm->HasValue("o_PrvApel") && $this->PrvApel->CurrentValue <> $this->PrvApel->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_PrvCRuc") && $objForm->HasValue("o_PrvCRuc") && $this->PrvCRuc->CurrentValue <> $this->PrvCRuc->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_PrvDire") && $objForm->HasValue("o_PrvDire") && $this->PrvDire->CurrentValue <> $this->PrvDire->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_PrvMail") && $objForm->HasValue("o_PrvMail") && $this->PrvMail->CurrentValue <> $this->PrvMail->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_PrvTele") && $objForm->HasValue("o_PrvTele") && $this->PrvTele->CurrentValue <> $this->PrvTele->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_PrvRSoc") && $objForm->HasValue("o_PrvRSoc") && $this->PrvRSoc->CurrentValue <> $this->PrvRSoc->OldValue)
			return FALSE;
		return TRUE;
	}

	// Validate grid form
	function ValidateGridForm() {
		global $objForm;

		// Get row count
		$objForm->Index = -1;
		$rowcnt = strval($objForm->GetValue($this->FormKeyCountName));
		if ($rowcnt == "" || !is_numeric($rowcnt))
			$rowcnt = 0;

		// Validate all records
		for ($rowindex = 1; $rowindex <= $rowcnt; $rowindex++) {

			// Load current row values
			$objForm->Index = $rowindex;
			$rowaction = strval($objForm->GetValue($this->FormActionName));
			if ($rowaction <> "delete" && $rowaction <> "insertdelete") {
				$this->LoadFormValues(); // Get form values
				if ($rowaction == "insert" && $this->EmptyRow()) {

					// Ignore
				} else if (!$this->ValidateForm()) {
					return FALSE;
				}
			}
		}
		return TRUE;
	}

	// Get all form values of the grid
	function GetGridFormValues() {
		global $objForm;

		// Get row count
		$objForm->Index = -1;
		$rowcnt = strval($objForm->GetValue($this->FormKeyCountName));
		if ($rowcnt == "" || !is_numeric($rowcnt))
			$rowcnt = 0;
		$rows = array();

		// Loop through all records
		for ($rowindex = 1; $rowindex <= $rowcnt; $rowindex++) {

			// Load current row values
			$objForm->Index = $rowindex;
			$rowaction = strval($objForm->GetValue($this->FormActionName));
			if ($rowaction <> "delete" && $rowaction <> "insertdelete") {
				$this->LoadFormValues(); // Get form values
				if ($rowaction == "insert" && $this->EmptyRow()) {

					// Ignore
				} else {
					$rows[] = $this->GetFieldValues("FormValue"); // Return row as array
				}
			}
		}
		return $rows; // Return as array of array
	}

	// Restore form values for current row
	function RestoreCurrentRowFormValues($idx) {
		global $objForm;

		// Get row based on current index
		$objForm->Index = $idx;
		$this->LoadFormValues(); // Load form values
	}

	// Get list of filters
	function GetFilterList() {

		// Initialize
		$sFilterList = "";
		$sFilterList = ew_Concat($sFilterList, $this->PrvCodi->AdvancedSearch->ToJSON(), ","); // Field PrvCodi
		$sFilterList = ew_Concat($sFilterList, $this->PrvFIni->AdvancedSearch->ToJSON(), ","); // Field PrvFIni
		$sFilterList = ew_Concat($sFilterList, $this->PrvFFin->AdvancedSearch->ToJSON(), ","); // Field PrvFFin
		$sFilterList = ew_Concat($sFilterList, $this->PrvPais->AdvancedSearch->ToJSON(), ","); // Field PrvPais
		$sFilterList = ew_Concat($sFilterList, $this->PrvTipo->AdvancedSearch->ToJSON(), ","); // Field PrvTipo
		$sFilterList = ew_Concat($sFilterList, $this->PrvNomb->AdvancedSearch->ToJSON(), ","); // Field PrvNomb
		$sFilterList = ew_Concat($sFilterList, $this->PrvApel->AdvancedSearch->ToJSON(), ","); // Field PrvApel
		$sFilterList = ew_Concat($sFilterList, $this->PrvCRuc->AdvancedSearch->ToJSON(), ","); // Field PrvCRuc
		$sFilterList = ew_Concat($sFilterList, $this->PrvDire->AdvancedSearch->ToJSON(), ","); // Field PrvDire
		$sFilterList = ew_Concat($sFilterList, $this->PrvMail->AdvancedSearch->ToJSON(), ","); // Field PrvMail
		$sFilterList = ew_Concat($sFilterList, $this->PrvTele->AdvancedSearch->ToJSON(), ","); // Field PrvTele
		$sFilterList = ew_Concat($sFilterList, $this->PrvRSoc->AdvancedSearch->ToJSON(), ","); // Field PrvRSoc
		if ($this->BasicSearch->Keyword <> "") {
			$sWrk = "\"" . EW_TABLE_BASIC_SEARCH . "\":\"" . ew_JsEncode2($this->BasicSearch->Keyword) . "\",\"" . EW_TABLE_BASIC_SEARCH_TYPE . "\":\"" . ew_JsEncode2($this->BasicSearch->Type) . "\"";
			$sFilterList = ew_Concat($sFilterList, $sWrk, ",");
		}

		// Return filter list in json
		return ($sFilterList <> "") ? "{" . $sFilterList . "}" : "null";
	}

	// Restore list of filters
	function RestoreFilterList() {

		// Return if not reset filter
		if (@$_POST["cmd"] <> "resetfilter")
			return FALSE;
		$filter = json_decode(ew_StripSlashes(@$_POST["filter"]), TRUE);
		$this->Command = "search";

		// Field PrvCodi
		$this->PrvCodi->AdvancedSearch->SearchValue = @$filter["x_PrvCodi"];
		$this->PrvCodi->AdvancedSearch->SearchOperator = @$filter["z_PrvCodi"];
		$this->PrvCodi->AdvancedSearch->SearchCondition = @$filter["v_PrvCodi"];
		$this->PrvCodi->AdvancedSearch->SearchValue2 = @$filter["y_PrvCodi"];
		$this->PrvCodi->AdvancedSearch->SearchOperator2 = @$filter["w_PrvCodi"];
		$this->PrvCodi->AdvancedSearch->Save();

		// Field PrvFIni
		$this->PrvFIni->AdvancedSearch->SearchValue = @$filter["x_PrvFIni"];
		$this->PrvFIni->AdvancedSearch->SearchOperator = @$filter["z_PrvFIni"];
		$this->PrvFIni->AdvancedSearch->SearchCondition = @$filter["v_PrvFIni"];
		$this->PrvFIni->AdvancedSearch->SearchValue2 = @$filter["y_PrvFIni"];
		$this->PrvFIni->AdvancedSearch->SearchOperator2 = @$filter["w_PrvFIni"];
		$this->PrvFIni->AdvancedSearch->Save();

		// Field PrvFFin
		$this->PrvFFin->AdvancedSearch->SearchValue = @$filter["x_PrvFFin"];
		$this->PrvFFin->AdvancedSearch->SearchOperator = @$filter["z_PrvFFin"];
		$this->PrvFFin->AdvancedSearch->SearchCondition = @$filter["v_PrvFFin"];
		$this->PrvFFin->AdvancedSearch->SearchValue2 = @$filter["y_PrvFFin"];
		$this->PrvFFin->AdvancedSearch->SearchOperator2 = @$filter["w_PrvFFin"];
		$this->PrvFFin->AdvancedSearch->Save();

		// Field PrvPais
		$this->PrvPais->AdvancedSearch->SearchValue = @$filter["x_PrvPais"];
		$this->PrvPais->AdvancedSearch->SearchOperator = @$filter["z_PrvPais"];
		$this->PrvPais->AdvancedSearch->SearchCondition = @$filter["v_PrvPais"];
		$this->PrvPais->AdvancedSearch->SearchValue2 = @$filter["y_PrvPais"];
		$this->PrvPais->AdvancedSearch->SearchOperator2 = @$filter["w_PrvPais"];
		$this->PrvPais->AdvancedSearch->Save();

		// Field PrvTipo
		$this->PrvTipo->AdvancedSearch->SearchValue = @$filter["x_PrvTipo"];
		$this->PrvTipo->AdvancedSearch->SearchOperator = @$filter["z_PrvTipo"];
		$this->PrvTipo->AdvancedSearch->SearchCondition = @$filter["v_PrvTipo"];
		$this->PrvTipo->AdvancedSearch->SearchValue2 = @$filter["y_PrvTipo"];
		$this->PrvTipo->AdvancedSearch->SearchOperator2 = @$filter["w_PrvTipo"];
		$this->PrvTipo->AdvancedSearch->Save();

		// Field PrvNomb
		$this->PrvNomb->AdvancedSearch->SearchValue = @$filter["x_PrvNomb"];
		$this->PrvNomb->AdvancedSearch->SearchOperator = @$filter["z_PrvNomb"];
		$this->PrvNomb->AdvancedSearch->SearchCondition = @$filter["v_PrvNomb"];
		$this->PrvNomb->AdvancedSearch->SearchValue2 = @$filter["y_PrvNomb"];
		$this->PrvNomb->AdvancedSearch->SearchOperator2 = @$filter["w_PrvNomb"];
		$this->PrvNomb->AdvancedSearch->Save();

		// Field PrvApel
		$this->PrvApel->AdvancedSearch->SearchValue = @$filter["x_PrvApel"];
		$this->PrvApel->AdvancedSearch->SearchOperator = @$filter["z_PrvApel"];
		$this->PrvApel->AdvancedSearch->SearchCondition = @$filter["v_PrvApel"];
		$this->PrvApel->AdvancedSearch->SearchValue2 = @$filter["y_PrvApel"];
		$this->PrvApel->AdvancedSearch->SearchOperator2 = @$filter["w_PrvApel"];
		$this->PrvApel->AdvancedSearch->Save();

		// Field PrvCRuc
		$this->PrvCRuc->AdvancedSearch->SearchValue = @$filter["x_PrvCRuc"];
		$this->PrvCRuc->AdvancedSearch->SearchOperator = @$filter["z_PrvCRuc"];
		$this->PrvCRuc->AdvancedSearch->SearchCondition = @$filter["v_PrvCRuc"];
		$this->PrvCRuc->AdvancedSearch->SearchValue2 = @$filter["y_PrvCRuc"];
		$this->PrvCRuc->AdvancedSearch->SearchOperator2 = @$filter["w_PrvCRuc"];
		$this->PrvCRuc->AdvancedSearch->Save();

		// Field PrvDire
		$this->PrvDire->AdvancedSearch->SearchValue = @$filter["x_PrvDire"];
		$this->PrvDire->AdvancedSearch->SearchOperator = @$filter["z_PrvDire"];
		$this->PrvDire->AdvancedSearch->SearchCondition = @$filter["v_PrvDire"];
		$this->PrvDire->AdvancedSearch->SearchValue2 = @$filter["y_PrvDire"];
		$this->PrvDire->AdvancedSearch->SearchOperator2 = @$filter["w_PrvDire"];
		$this->PrvDire->AdvancedSearch->Save();

		// Field PrvMail
		$this->PrvMail->AdvancedSearch->SearchValue = @$filter["x_PrvMail"];
		$this->PrvMail->AdvancedSearch->SearchOperator = @$filter["z_PrvMail"];
		$this->PrvMail->AdvancedSearch->SearchCondition = @$filter["v_PrvMail"];
		$this->PrvMail->AdvancedSearch->SearchValue2 = @$filter["y_PrvMail"];
		$this->PrvMail->AdvancedSearch->SearchOperator2 = @$filter["w_PrvMail"];
		$this->PrvMail->AdvancedSearch->Save();

		// Field PrvTele
		$this->PrvTele->AdvancedSearch->SearchValue = @$filter["x_PrvTele"];
		$this->PrvTele->AdvancedSearch->SearchOperator = @$filter["z_PrvTele"];
		$this->PrvTele->AdvancedSearch->SearchCondition = @$filter["v_PrvTele"];
		$this->PrvTele->AdvancedSearch->SearchValue2 = @$filter["y_PrvTele"];
		$this->PrvTele->AdvancedSearch->SearchOperator2 = @$filter["w_PrvTele"];
		$this->PrvTele->AdvancedSearch->Save();

		// Field PrvRSoc
		$this->PrvRSoc->AdvancedSearch->SearchValue = @$filter["x_PrvRSoc"];
		$this->PrvRSoc->AdvancedSearch->SearchOperator = @$filter["z_PrvRSoc"];
		$this->PrvRSoc->AdvancedSearch->SearchCondition = @$filter["v_PrvRSoc"];
		$this->PrvRSoc->AdvancedSearch->SearchValue2 = @$filter["y_PrvRSoc"];
		$this->PrvRSoc->AdvancedSearch->SearchOperator2 = @$filter["w_PrvRSoc"];
		$this->PrvRSoc->AdvancedSearch->Save();
		$this->BasicSearch->setKeyword(@$filter[EW_TABLE_BASIC_SEARCH]);
		$this->BasicSearch->setType(@$filter[EW_TABLE_BASIC_SEARCH_TYPE]);
	}

	// Return basic search SQL
	function BasicSearchSQL($arKeywords, $type) {
		$sWhere = "";
		$this->BuildBasicSearchSQL($sWhere, $this->PrvPais, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->PrvTipo, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->PrvNomb, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->PrvApel, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->PrvCRuc, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->PrvDire, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->PrvMail, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->PrvRSoc, $arKeywords, $type);
		return $sWhere;
	}

	// Build basic search SQL
	function BuildBasicSearchSql(&$Where, &$Fld, $arKeywords, $type) {
		$sDefCond = ($type == "OR") ? "OR" : "AND";
		$arSQL = array(); // Array for SQL parts
		$arCond = array(); // Array for search conditions
		$cnt = count($arKeywords);
		$j = 0; // Number of SQL parts
		for ($i = 0; $i < $cnt; $i++) {
			$Keyword = $arKeywords[$i];
			$Keyword = trim($Keyword);
			if (EW_BASIC_SEARCH_IGNORE_PATTERN <> "") {
				$Keyword = preg_replace(EW_BASIC_SEARCH_IGNORE_PATTERN, "\\", $Keyword);
				$ar = explode("\\", $Keyword);
			} else {
				$ar = array($Keyword);
			}
			foreach ($ar as $Keyword) {
				if ($Keyword <> "") {
					$sWrk = "";
					if ($Keyword == "OR" && $type == "") {
						if ($j > 0)
							$arCond[$j-1] = "OR";
					} elseif ($Keyword == EW_NULL_VALUE) {
						$sWrk = $Fld->FldExpression . " IS NULL";
					} elseif ($Keyword == EW_NOT_NULL_VALUE) {
						$sWrk = $Fld->FldExpression . " IS NOT NULL";
					} elseif ($Fld->FldIsVirtual && $Fld->FldVirtualSearch) {
						$sWrk = $Fld->FldVirtualExpression . ew_Like(ew_QuotedValue("%" . $Keyword . "%", EW_DATATYPE_STRING, $this->DBID), $this->DBID);
					} elseif ($Fld->FldDataType != EW_DATATYPE_NUMBER || is_numeric($Keyword)) {
						$sWrk = $Fld->FldBasicSearchExpression . ew_Like(ew_QuotedValue("%" . $Keyword . "%", EW_DATATYPE_STRING, $this->DBID), $this->DBID);
					}
					if ($sWrk <> "") {
						$arSQL[$j] = $sWrk;
						$arCond[$j] = $sDefCond;
						$j += 1;
					}
				}
			}
		}
		$cnt = count($arSQL);
		$bQuoted = FALSE;
		$sSql = "";
		if ($cnt > 0) {
			for ($i = 0; $i < $cnt-1; $i++) {
				if ($arCond[$i] == "OR") {
					if (!$bQuoted) $sSql .= "(";
					$bQuoted = TRUE;
				}
				$sSql .= $arSQL[$i];
				if ($bQuoted && $arCond[$i] <> "OR") {
					$sSql .= ")";
					$bQuoted = FALSE;
				}
				$sSql .= " " . $arCond[$i] . " ";
			}
			$sSql .= $arSQL[$cnt-1];
			if ($bQuoted)
				$sSql .= ")";
		}
		if ($sSql <> "") {
			if ($Where <> "") $Where .= " OR ";
			$Where .=  "(" . $sSql . ")";
		}
	}

	// Return basic search WHERE clause based on search keyword and type
	function BasicSearchWhere($Default = FALSE) {
		global $Security;
		$sSearchStr = "";
		if (!$Security->CanSearch()) return "";
		$sSearchKeyword = ($Default) ? $this->BasicSearch->KeywordDefault : $this->BasicSearch->Keyword;
		$sSearchType = ($Default) ? $this->BasicSearch->TypeDefault : $this->BasicSearch->Type;
		if ($sSearchKeyword <> "") {
			$sSearch = trim($sSearchKeyword);
			if ($sSearchType <> "=") {
				$ar = array();

				// Match quoted keywords (i.e.: "...")
				if (preg_match_all('/"([^"]*)"/i', $sSearch, $matches, PREG_SET_ORDER)) {
					foreach ($matches as $match) {
						$p = strpos($sSearch, $match[0]);
						$str = substr($sSearch, 0, $p);
						$sSearch = substr($sSearch, $p + strlen($match[0]));
						if (strlen(trim($str)) > 0)
							$ar = array_merge($ar, explode(" ", trim($str)));
						$ar[] = $match[1]; // Save quoted keyword
					}
				}

				// Match individual keywords
				if (strlen(trim($sSearch)) > 0)
					$ar = array_merge($ar, explode(" ", trim($sSearch)));

				// Search keyword in any fields
				if (($sSearchType == "OR" || $sSearchType == "AND") && $this->BasicSearch->BasicSearchAnyFields) {
					foreach ($ar as $sKeyword) {
						if ($sKeyword <> "") {
							if ($sSearchStr <> "") $sSearchStr .= " " . $sSearchType . " ";
							$sSearchStr .= "(" . $this->BasicSearchSQL(array($sKeyword), $sSearchType) . ")";
						}
					}
				} else {
					$sSearchStr = $this->BasicSearchSQL($ar, $sSearchType);
				}
			} else {
				$sSearchStr = $this->BasicSearchSQL(array($sSearch), $sSearchType);
			}
			if (!$Default) $this->Command = "search";
		}
		if (!$Default && $this->Command == "search") {
			$this->BasicSearch->setKeyword($sSearchKeyword);
			$this->BasicSearch->setType($sSearchType);
		}
		return $sSearchStr;
	}

	// Check if search parm exists
	function CheckSearchParms() {

		// Check basic search
		if ($this->BasicSearch->IssetSession())
			return TRUE;
		return FALSE;
	}

	// Clear all search parameters
	function ResetSearchParms() {

		// Clear search WHERE clause
		$this->SearchWhere = "";
		$this->setSearchWhere($this->SearchWhere);

		// Clear basic search parameters
		$this->ResetBasicSearchParms();
	}

	// Load advanced search default values
	function LoadAdvancedSearchDefault() {
		return FALSE;
	}

	// Clear all basic search parameters
	function ResetBasicSearchParms() {
		$this->BasicSearch->UnsetSession();
	}

	// Restore all search parameters
	function RestoreSearchParms() {
		$this->RestoreSearch = TRUE;

		// Restore basic search values
		$this->BasicSearch->Load();
	}

	// Set up sort parameters
	function SetUpSortOrder() {

		// Check for Ctrl pressed
		$bCtrl = (@$_GET["ctrl"] <> "");

		// Check for "order" parameter
		if (@$_GET["order"] <> "") {
			$this->CurrentOrder = ew_StripSlashes(@$_GET["order"]);
			$this->CurrentOrderType = @$_GET["ordertype"];
			$this->UpdateSort($this->PrvFIni, $bCtrl); // PrvFIni
			$this->UpdateSort($this->PrvFFin, $bCtrl); // PrvFFin
			$this->UpdateSort($this->PrvPais, $bCtrl); // PrvPais
			$this->UpdateSort($this->PrvTipo, $bCtrl); // PrvTipo
			$this->UpdateSort($this->PrvNomb, $bCtrl); // PrvNomb
			$this->UpdateSort($this->PrvApel, $bCtrl); // PrvApel
			$this->UpdateSort($this->PrvCRuc, $bCtrl); // PrvCRuc
			$this->UpdateSort($this->PrvDire, $bCtrl); // PrvDire
			$this->UpdateSort($this->PrvMail, $bCtrl); // PrvMail
			$this->UpdateSort($this->PrvTele, $bCtrl); // PrvTele
			$this->UpdateSort($this->PrvRSoc, $bCtrl); // PrvRSoc
			$this->setStartRecordNumber(1); // Reset start position
		}
	}

	// Load sort order parameters
	function LoadSortOrder() {
		$sOrderBy = $this->getSessionOrderBy(); // Get ORDER BY from Session
		if ($sOrderBy == "") {
			if ($this->getSqlOrderBy() <> "") {
				$sOrderBy = $this->getSqlOrderBy();
				$this->setSessionOrderBy($sOrderBy);
			}
		}
	}

	// Reset command
	// - cmd=reset (Reset search parameters)
	// - cmd=resetall (Reset search and master/detail parameters)
	// - cmd=resetsort (Reset sort parameters)
	function ResetCmd() {

		// Check if reset command
		if (substr($this->Command,0,5) == "reset") {

			// Reset search criteria
			if ($this->Command == "reset" || $this->Command == "resetall")
				$this->ResetSearchParms();

			// Reset sorting order
			if ($this->Command == "resetsort") {
				$sOrderBy = "";
				$this->setSessionOrderBy($sOrderBy);
				$this->PrvFIni->setSort("");
				$this->PrvFFin->setSort("");
				$this->PrvPais->setSort("");
				$this->PrvTipo->setSort("");
				$this->PrvNomb->setSort("");
				$this->PrvApel->setSort("");
				$this->PrvCRuc->setSort("");
				$this->PrvDire->setSort("");
				$this->PrvMail->setSort("");
				$this->PrvTele->setSort("");
				$this->PrvRSoc->setSort("");
			}

			// Reset start position
			$this->StartRec = 1;
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Set up list options
	function SetupListOptions() {
		global $Security, $Language;

		// "griddelete"
		if ($this->AllowAddDeleteRow) {
			$item = &$this->ListOptions->Add("griddelete");
			$item->CssStyle = "white-space: nowrap;";
			$item->OnLeft = FALSE;
			$item->Visible = FALSE; // Default hidden
		}

		// Add group option item
		$item = &$this->ListOptions->Add($this->ListOptions->GroupOptionName);
		$item->Body = "";
		$item->OnLeft = FALSE;
		$item->Visible = FALSE;

		// "view"
		$item = &$this->ListOptions->Add("view");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->CanView();
		$item->OnLeft = FALSE;

		// "edit"
		$item = &$this->ListOptions->Add("edit");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->CanEdit();
		$item->OnLeft = FALSE;

		// "copy"
		$item = &$this->ListOptions->Add("copy");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->CanAdd();
		$item->OnLeft = FALSE;

		// "delete"
		$item = &$this->ListOptions->Add("delete");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->CanDelete();
		$item->OnLeft = FALSE;

		// List actions
		$item = &$this->ListOptions->Add("listactions");
		$item->CssStyle = "white-space: nowrap;";
		$item->OnLeft = FALSE;
		$item->Visible = FALSE;
		$item->ShowInButtonGroup = FALSE;
		$item->ShowInDropDown = FALSE;

		// "checkbox"
		$item = &$this->ListOptions->Add("checkbox");
		$item->Visible = FALSE;
		$item->OnLeft = FALSE;
		$item->Header = "<input type=\"checkbox\" name=\"key\" id=\"key\" onclick=\"ew_SelectAllKey(this);\">";
		$item->ShowInDropDown = FALSE;
		$item->ShowInButtonGroup = FALSE;

		// Drop down button for ListOptions
		$this->ListOptions->UseImageAndText = TRUE;
		$this->ListOptions->UseDropDownButton = FALSE;
		$this->ListOptions->DropDownButtonPhrase = $Language->Phrase("ButtonListOptions");
		$this->ListOptions->UseButtonGroup = FALSE;
		if ($this->ListOptions->UseButtonGroup && ew_IsMobile())
			$this->ListOptions->UseDropDownButton = TRUE;
		$this->ListOptions->ButtonClass = "btn-sm"; // Class for button group

		// Call ListOptions_Load event
		$this->ListOptions_Load();
		$this->SetupListOptionsExt();
		$item = &$this->ListOptions->GetItem($this->ListOptions->GroupOptionName);
		$item->Visible = $this->ListOptions->GroupOptionVisible();
	}

	// Render list options
	function RenderListOptions() {
		global $Security, $Language, $objForm;
		$this->ListOptions->LoadDefault();

		// Set up row action and key
		if (is_numeric($this->RowIndex) && $this->CurrentMode <> "view") {
			$objForm->Index = $this->RowIndex;
			$ActionName = str_replace("k_", "k" . $this->RowIndex . "_", $this->FormActionName);
			$OldKeyName = str_replace("k_", "k" . $this->RowIndex . "_", $this->FormOldKeyName);
			$KeyName = str_replace("k_", "k" . $this->RowIndex . "_", $this->FormKeyName);
			$BlankRowName = str_replace("k_", "k" . $this->RowIndex . "_", $this->FormBlankRowName);
			if ($this->RowAction <> "")
				$this->MultiSelectKey .= "<input type=\"hidden\" name=\"" . $ActionName . "\" id=\"" . $ActionName . "\" value=\"" . $this->RowAction . "\">";
			if ($this->RowAction == "delete") {
				$rowkey = $objForm->GetValue($this->FormKeyName);
				$this->SetupKeyValues($rowkey);
			}
			if ($this->RowAction == "insert" && $this->CurrentAction == "F" && $this->EmptyRow())
				$this->MultiSelectKey .= "<input type=\"hidden\" name=\"" . $BlankRowName . "\" id=\"" . $BlankRowName . "\" value=\"1\">";
		}

		// "delete"
		if ($this->AllowAddDeleteRow) {
			if ($this->CurrentAction == "gridadd" || $this->CurrentAction == "gridedit") {
				$option = &$this->ListOptions;
				$option->UseButtonGroup = TRUE; // Use button group for grid delete button
				$option->UseImageAndText = TRUE; // Use image and text for grid delete button
				$oListOpt = &$option->Items["griddelete"];
				if (!$Security->CanDelete() && is_numeric($this->RowIndex) && ($this->RowAction == "" || $this->RowAction == "edit")) { // Do not allow delete existing record
					$oListOpt->Body = "&nbsp;";
				} else {
					$oListOpt->Body = "<a class=\"ewGridLink ewGridDelete\" title=\"" . ew_HtmlTitle($Language->Phrase("DeleteLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("DeleteLink")) . "\" onclick=\"return ew_DeleteGridRow(this, " . $this->RowIndex . ");\">" . $Language->Phrase("DeleteLink") . "</a>";
				}
			}
		}

		// "view"
		$oListOpt = &$this->ListOptions->Items["view"];
		if ($Security->CanView())
			$oListOpt->Body = "<a class=\"ewRowLink ewView\" title=\"" . ew_HtmlTitle($Language->Phrase("ViewLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("ViewLink")) . "\" href=\"" . ew_HtmlEncode($this->ViewUrl) . "\">" . $Language->Phrase("ViewLink") . "</a>";
		else
			$oListOpt->Body = "";

		// "edit"
		$oListOpt = &$this->ListOptions->Items["edit"];
		if ($Security->CanEdit()) {
			$oListOpt->Body = "<a class=\"ewRowLink ewEdit\" title=\"" . ew_HtmlTitle($Language->Phrase("EditLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("EditLink")) . "\" href=\"" . ew_HtmlEncode($this->EditUrl) . "\">" . $Language->Phrase("EditLink") . "</a>";
		} else {
			$oListOpt->Body = "";
		}

		// "copy"
		$oListOpt = &$this->ListOptions->Items["copy"];
		if ($Security->CanAdd()) {
			$oListOpt->Body = "<a class=\"ewRowLink ewCopy\" title=\"" . ew_HtmlTitle($Language->Phrase("CopyLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("CopyLink")) . "\" href=\"" . ew_HtmlEncode($this->CopyUrl) . "\">" . $Language->Phrase("CopyLink") . "</a>";
		} else {
			$oListOpt->Body = "";
		}

		// "delete"
		$oListOpt = &$this->ListOptions->Items["delete"];
		if ($Security->CanDelete())
			$oListOpt->Body = "<a class=\"ewRowLink ewDelete\"" . "" . " title=\"" . ew_HtmlTitle($Language->Phrase("DeleteLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("DeleteLink")) . "\" href=\"" . ew_HtmlEncode($this->DeleteUrl) . "\">" . $Language->Phrase("DeleteLink") . "</a>";
		else
			$oListOpt->Body = "";

		// Set up list action buttons
		$oListOpt = &$this->ListOptions->GetItem("listactions");
		if ($oListOpt) {
			$body = "";
			$links = array();
			foreach ($this->ListActions->Items as $listaction) {
				if ($listaction->Select == EW_ACTION_SINGLE && $listaction->Allow) {
					$action = $listaction->Action;
					$caption = $listaction->Caption;
					$icon = ($listaction->Icon <> "") ? "<span class=\"" . ew_HtmlEncode(str_replace(" ewIcon", "", $listaction->Icon)) . "\" data-caption=\"" . ew_HtmlTitle($caption) . "\"></span> " : "";
					$links[] = "<li><a class=\"ewAction ewListAction\" data-action=\"" . ew_HtmlEncode($action) . "\" data-caption=\"" . ew_HtmlTitle($caption) . "\" href=\"\" onclick=\"ew_SubmitAction(event,jQuery.extend({key:" . $this->KeyToJson() . "}," . $listaction->ToJson(TRUE) . "));return false;\">" . $icon . $listaction->Caption . "</a></li>";
					if (count($links) == 1) // Single button
						$body = "<a class=\"ewAction ewListAction\" data-action=\"" . ew_HtmlEncode($action) . "\" title=\"" . ew_HtmlTitle($caption) . "\" data-caption=\"" . ew_HtmlTitle($caption) . "\" href=\"\" onclick=\"ew_SubmitAction(event,jQuery.extend({key:" . $this->KeyToJson() . "}," . $listaction->ToJson(TRUE) . "));return false;\">" . $Language->Phrase("ListActionButton") . "</a>";
				}
			}
			if (count($links) > 1) { // More than one buttons, use dropdown
				$body = "<button class=\"dropdown-toggle btn btn-default btn-sm ewActions\" title=\"" . ew_HtmlTitle($Language->Phrase("ListActionButton")) . "\" data-toggle=\"dropdown\">" . $Language->Phrase("ListActionButton") . "<b class=\"caret\"></b></button>";
				$content = "";
				foreach ($links as $link)
					$content .= "<li>" . $link . "</li>";
				$body .= "<ul class=\"dropdown-menu" . ($oListOpt->OnLeft ? "" : " dropdown-menu-right") . "\">". $content . "</ul>";
				$body = "<div class=\"btn-group\">" . $body . "</div>";
			}
			if (count($links) > 0) {
				$oListOpt->Body = $body;
				$oListOpt->Visible = TRUE;
			}
		}

		// "checkbox"
		$oListOpt = &$this->ListOptions->Items["checkbox"];
		$oListOpt->Body = "<input type=\"checkbox\" name=\"key_m[]\" value=\"" . ew_HtmlEncode($this->PrvCodi->CurrentValue) . "\" onclick='ew_ClickMultiCheckbox(event);'>";
		if ($this->CurrentAction == "gridedit" && is_numeric($this->RowIndex)) {
			$this->MultiSelectKey .= "<input type=\"hidden\" name=\"" . $KeyName . "\" id=\"" . $KeyName . "\" value=\"" . $this->PrvCodi->CurrentValue . "\">";
		}
		$this->RenderListOptionsExt();

		// Call ListOptions_Rendered event
		$this->ListOptions_Rendered();
	}

	// Set up other options
	function SetupOtherOptions() {
		global $Language, $Security;
		$options = &$this->OtherOptions;
		$option = $options["addedit"];

		// Add
		$item = &$option->Add("add");
		$item->Body = "<a class=\"ewAddEdit ewAdd\" title=\"" . ew_HtmlTitle($Language->Phrase("AddLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("AddLink")) . "\" href=\"" . ew_HtmlEncode($this->AddUrl) . "\">" . $Language->Phrase("AddLink") . "</a>";
		$item->Visible = ($this->AddUrl <> "" && $Security->CanAdd());
		$item = &$option->Add("gridadd");
		$item->Body = "<a class=\"ewAddEdit ewGridAdd\" title=\"" . ew_HtmlTitle($Language->Phrase("GridAddLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("GridAddLink")) . "\" href=\"" . ew_HtmlEncode($this->GridAddUrl) . "\">" . $Language->Phrase("GridAddLink") . "</a>";
		$item->Visible = ($this->GridAddUrl <> "" && $Security->CanAdd());

		// Add grid edit
		$option = $options["addedit"];
		$item = &$option->Add("gridedit");
		$item->Body = "<a class=\"ewAddEdit ewGridEdit\" title=\"" . ew_HtmlTitle($Language->Phrase("GridEditLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("GridEditLink")) . "\" href=\"" . ew_HtmlEncode($this->GridEditUrl) . "\">" . $Language->Phrase("GridEditLink") . "</a>";
		$item->Visible = ($this->GridEditUrl <> "" && $Security->CanEdit());
		$option = $options["action"];

		// Set up options default
		foreach ($options as &$option) {
			$option->UseImageAndText = TRUE;
			$option->UseDropDownButton = FALSE;
			$option->UseButtonGroup = TRUE;
			$option->ButtonClass = "btn-sm"; // Class for button group
			$item = &$option->Add($option->GroupOptionName);
			$item->Body = "";
			$item->Visible = FALSE;
		}
		$options["addedit"]->DropDownButtonPhrase = $Language->Phrase("ButtonAddEdit");
		$options["detail"]->DropDownButtonPhrase = $Language->Phrase("ButtonDetails");
		$options["action"]->DropDownButtonPhrase = $Language->Phrase("ButtonActions");

		// Filter button
		$item = &$this->FilterOptions->Add("savecurrentfilter");
		$item->Body = "<a class=\"ewSaveFilter\" data-form=\"fProvlistsrch\" href=\"#\">" . $Language->Phrase("SaveCurrentFilter") . "</a>";
		$item->Visible = TRUE;
		$item = &$this->FilterOptions->Add("deletefilter");
		$item->Body = "<a class=\"ewDeleteFilter\" data-form=\"fProvlistsrch\" href=\"#\">" . $Language->Phrase("DeleteFilter") . "</a>";
		$item->Visible = TRUE;
		$this->FilterOptions->UseDropDownButton = TRUE;
		$this->FilterOptions->UseButtonGroup = !$this->FilterOptions->UseDropDownButton;
		$this->FilterOptions->DropDownButtonPhrase = $Language->Phrase("Filters");

		// Add group option item
		$item = &$this->FilterOptions->Add($this->FilterOptions->GroupOptionName);
		$item->Body = "";
		$item->Visible = FALSE;
	}

	// Render other options
	function RenderOtherOptions() {
		global $Language, $Security;
		$options = &$this->OtherOptions;
		if ($this->CurrentAction <> "gridadd" && $this->CurrentAction <> "gridedit") { // Not grid add/edit mode
			$option = &$options["action"];

			// Set up list action buttons
			foreach ($this->ListActions->Items as $listaction) {
				if ($listaction->Select == EW_ACTION_MULTIPLE) {
					$item = &$option->Add("custom_" . $listaction->Action);
					$caption = $listaction->Caption;
					$icon = ($listaction->Icon <> "") ? "<span class=\"" . ew_HtmlEncode($listaction->Icon) . "\" data-caption=\"" . ew_HtmlEncode($caption) . "\"></span> " : $caption;
					$item->Body = "<a class=\"ewAction ewListAction\" title=\"" . ew_HtmlEncode($caption) . "\" data-caption=\"" . ew_HtmlEncode($caption) . "\" href=\"\" onclick=\"ew_SubmitAction(event,jQuery.extend({f:document.fProvlist}," . $listaction->ToJson(TRUE) . "));return false;\">" . $icon . "</a>";
					$item->Visible = $listaction->Allow;
				}
			}

			// Hide grid edit and other options
			if ($this->TotalRecs <= 0) {
				$option = &$options["addedit"];
				$item = &$option->GetItem("gridedit");
				if ($item) $item->Visible = FALSE;
				$option = &$options["action"];
				$option->HideAllOptions();
			}
		} else { // Grid add/edit mode

			// Hide all options first
			foreach ($options as &$option)
				$option->HideAllOptions();
			if ($this->CurrentAction == "gridadd") {
				if ($this->AllowAddDeleteRow) {

					// Add add blank row
					$option = &$options["addedit"];
					$option->UseDropDownButton = FALSE;
					$option->UseImageAndText = TRUE;
					$item = &$option->Add("addblankrow");
					$item->Body = "<a class=\"ewAddEdit ewAddBlankRow\" title=\"" . ew_HtmlTitle($Language->Phrase("AddBlankRow")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("AddBlankRow")) . "\" href=\"javascript:void(0);\" onclick=\"ew_AddGridRow(this);\">" . $Language->Phrase("AddBlankRow") . "</a>";
					$item->Visible = $Security->CanAdd();
				}
				$option = &$options["action"];
				$option->UseDropDownButton = FALSE;
				$option->UseImageAndText = TRUE;

				// Add grid insert
				$item = &$option->Add("gridinsert");
				$item->Body = "<a class=\"ewAction ewGridInsert\" title=\"" . ew_HtmlTitle($Language->Phrase("GridInsertLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("GridInsertLink")) . "\" href=\"\" onclick=\"return ewForms(this).Submit('" . $this->AddMasterUrl($this->PageName()) . "');\">" . $Language->Phrase("GridInsertLink") . "</a>";

				// Add grid cancel
				$item = &$option->Add("gridcancel");
				$cancelurl = $this->AddMasterUrl($this->PageUrl() . "a=cancel");
				$item->Body = "<a class=\"ewAction ewGridCancel\" title=\"" . ew_HtmlTitle($Language->Phrase("GridCancelLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("GridCancelLink")) . "\" href=\"" . $cancelurl . "\">" . $Language->Phrase("GridCancelLink") . "</a>";
			}
			if ($this->CurrentAction == "gridedit") {
				if ($this->AllowAddDeleteRow) {

					// Add add blank row
					$option = &$options["addedit"];
					$option->UseDropDownButton = FALSE;
					$option->UseImageAndText = TRUE;
					$item = &$option->Add("addblankrow");
					$item->Body = "<a class=\"ewAddEdit ewAddBlankRow\" title=\"" . ew_HtmlTitle($Language->Phrase("AddBlankRow")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("AddBlankRow")) . "\" href=\"javascript:void(0);\" onclick=\"ew_AddGridRow(this);\">" . $Language->Phrase("AddBlankRow") . "</a>";
					$item->Visible = $Security->CanAdd();
				}
				$option = &$options["action"];
				$option->UseDropDownButton = FALSE;
				$option->UseImageAndText = TRUE;
					$item = &$option->Add("gridsave");
					$item->Body = "<a class=\"ewAction ewGridSave\" title=\"" . ew_HtmlTitle($Language->Phrase("GridSaveLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("GridSaveLink")) . "\" href=\"\" onclick=\"return ewForms(this).Submit('" . $this->AddMasterUrl($this->PageName()) . "');\">" . $Language->Phrase("GridSaveLink") . "</a>";
					$item = &$option->Add("gridcancel");
					$cancelurl = $this->AddMasterUrl($this->PageUrl() . "a=cancel");
					$item->Body = "<a class=\"ewAction ewGridCancel\" title=\"" . ew_HtmlTitle($Language->Phrase("GridCancelLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("GridCancelLink")) . "\" href=\"" . $cancelurl . "\">" . $Language->Phrase("GridCancelLink") . "</a>";
			}
		}
	}

	// Process list action
	function ProcessListAction() {
		global $Language, $Security;
		$userlist = "";
		$user = "";
		$sFilter = $this->GetKeyFilter();
		$UserAction = @$_POST["useraction"];
		if ($sFilter <> "" && $UserAction <> "") {

			// Check permission first
			$ActionCaption = $UserAction;
			if (array_key_exists($UserAction, $this->ListActions->Items)) {
				$ActionCaption = $this->ListActions->Items[$UserAction]->Caption;
				if (!$this->ListActions->Items[$UserAction]->Allow) {
					$errmsg = str_replace('%s', $ActionCaption, $Language->Phrase("CustomActionNotAllowed"));
					if (@$_POST["ajax"] == $UserAction) // Ajax
						echo "<p class=\"text-danger\">" . $errmsg . "</p>";
					else
						$this->setFailureMessage($errmsg);
					return FALSE;
				}
			}
			$this->CurrentFilter = $sFilter;
			$sSql = $this->SQL();
			$conn = &$this->Connection();
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			$rs = $conn->Execute($sSql);
			$conn->raiseErrorFn = '';
			$this->CurrentAction = $UserAction;

			// Call row action event
			if ($rs && !$rs->EOF) {
				$conn->BeginTrans();
				$this->SelectedCount = $rs->RecordCount();
				$this->SelectedIndex = 0;
				while (!$rs->EOF) {
					$this->SelectedIndex++;
					$row = $rs->fields;
					$Processed = $this->Row_CustomAction($UserAction, $row);
					if (!$Processed) break;
					$rs->MoveNext();
				}
				if ($Processed) {
					$conn->CommitTrans(); // Commit the changes
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage(str_replace('%s', $ActionCaption, $Language->Phrase("CustomActionCompleted"))); // Set up success message
				} else {
					$conn->RollbackTrans(); // Rollback changes

					// Set up error message
					if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

						// Use the message, do nothing
					} elseif ($this->CancelMessage <> "") {
						$this->setFailureMessage($this->CancelMessage);
						$this->CancelMessage = "";
					} else {
						$this->setFailureMessage(str_replace('%s', $ActionCaption, $Language->Phrase("CustomActionFailed")));
					}
				}
			}
			if ($rs)
				$rs->Close();
			if (@$_POST["ajax"] == $UserAction) { // Ajax
				if ($this->getSuccessMessage() <> "") {
					echo "<p class=\"text-success\">" . $this->getSuccessMessage() . "</p>";
					$this->ClearSuccessMessage(); // Clear message
				}
				if ($this->getFailureMessage() <> "") {
					echo "<p class=\"text-danger\">" . $this->getFailureMessage() . "</p>";
					$this->ClearFailureMessage(); // Clear message
				}
				return TRUE;
			}
		}
		return FALSE; // Not ajax request
	}

	// Set up search options
	function SetupSearchOptions() {
		global $Language;
		$this->SearchOptions = new cListOptions();
		$this->SearchOptions->Tag = "div";
		$this->SearchOptions->TagClassName = "ewSearchOption";

		// Search button
		$item = &$this->SearchOptions->Add("searchtoggle");
		$SearchToggleClass = ($this->SearchWhere <> "") ? " active" : " active";
		$item->Body = "<button type=\"button\" class=\"btn btn-default ewSearchToggle" . $SearchToggleClass . "\" title=\"" . $Language->Phrase("SearchPanel") . "\" data-caption=\"" . $Language->Phrase("SearchPanel") . "\" data-toggle=\"button\" data-form=\"fProvlistsrch\">" . $Language->Phrase("SearchBtn") . "</button>";
		$item->Visible = TRUE;

		// Show all button
		$item = &$this->SearchOptions->Add("showall");
		$item->Body = "<a class=\"btn btn-default ewShowAll\" title=\"" . $Language->Phrase("ShowAll") . "\" data-caption=\"" . $Language->Phrase("ShowAll") . "\" href=\"" . $this->PageUrl() . "cmd=reset\">" . $Language->Phrase("ShowAllBtn") . "</a>";
		$item->Visible = ($this->SearchWhere <> $this->DefaultSearchWhere && $this->SearchWhere <> "0=101");

		// Button group for search
		$this->SearchOptions->UseDropDownButton = FALSE;
		$this->SearchOptions->UseImageAndText = TRUE;
		$this->SearchOptions->UseButtonGroup = TRUE;
		$this->SearchOptions->DropDownButtonPhrase = $Language->Phrase("ButtonSearch");

		// Add group option item
		$item = &$this->SearchOptions->Add($this->SearchOptions->GroupOptionName);
		$item->Body = "";
		$item->Visible = FALSE;

		// Hide search options
		if ($this->Export <> "" || $this->CurrentAction <> "")
			$this->SearchOptions->HideAllOptions();
		global $Security;
		if (!$Security->CanSearch()) {
			$this->SearchOptions->HideAllOptions();
			$this->FilterOptions->HideAllOptions();
		}
	}

	function SetupListOptionsExt() {
		global $Security, $Language;
	}

	function RenderListOptionsExt() {
		global $Security, $Language;
	}

	// Set up starting record parameters
	function SetUpStartRec() {
		if ($this->DisplayRecs == 0)
			return;
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET[EW_TABLE_START_REC] <> "") { // Check for "start" parameter
				$this->StartRec = $_GET[EW_TABLE_START_REC];
				$this->setStartRecordNumber($this->StartRec);
			} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
				$PageNo = $_GET[EW_TABLE_PAGE_NO];
				if (is_numeric($PageNo)) {
					$this->StartRec = ($PageNo-1)*$this->DisplayRecs+1;
					if ($this->StartRec <= 0) {
						$this->StartRec = 1;
					} elseif ($this->StartRec >= intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1) {
						$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1;
					}
					$this->setStartRecordNumber($this->StartRec);
				}
			}
		}
		$this->StartRec = $this->getStartRecordNumber();

		// Check if correct start record counter
		if (!is_numeric($this->StartRec) || $this->StartRec == "") { // Avoid invalid start record counter
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} elseif (intval($this->StartRec) > intval($this->TotalRecs)) { // Avoid starting record > total records
			$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to last page first record
			$this->setStartRecordNumber($this->StartRec);
		} elseif (($this->StartRec-1) % $this->DisplayRecs <> 0) {
			$this->StartRec = intval(($this->StartRec-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to page boundary
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Load default values
	function LoadDefaultValues() {
		$this->PrvFIni->CurrentValue = NULL;
		$this->PrvFIni->OldValue = $this->PrvFIni->CurrentValue;
		$this->PrvFFin->CurrentValue = NULL;
		$this->PrvFFin->OldValue = $this->PrvFFin->CurrentValue;
		$this->PrvPais->CurrentValue = NULL;
		$this->PrvPais->OldValue = $this->PrvPais->CurrentValue;
		$this->PrvTipo->CurrentValue = NULL;
		$this->PrvTipo->OldValue = $this->PrvTipo->CurrentValue;
		$this->PrvNomb->CurrentValue = NULL;
		$this->PrvNomb->OldValue = $this->PrvNomb->CurrentValue;
		$this->PrvApel->CurrentValue = NULL;
		$this->PrvApel->OldValue = $this->PrvApel->CurrentValue;
		$this->PrvCRuc->CurrentValue = NULL;
		$this->PrvCRuc->OldValue = $this->PrvCRuc->CurrentValue;
		$this->PrvDire->CurrentValue = NULL;
		$this->PrvDire->OldValue = $this->PrvDire->CurrentValue;
		$this->PrvMail->CurrentValue = NULL;
		$this->PrvMail->OldValue = $this->PrvMail->CurrentValue;
		$this->PrvTele->CurrentValue = NULL;
		$this->PrvTele->OldValue = $this->PrvTele->CurrentValue;
		$this->PrvRSoc->CurrentValue = NULL;
		$this->PrvRSoc->OldValue = $this->PrvRSoc->CurrentValue;
	}

	// Load basic search values
	function LoadBasicSearchValues() {
		$this->BasicSearch->Keyword = @$_GET[EW_TABLE_BASIC_SEARCH];
		if ($this->BasicSearch->Keyword <> "") $this->Command = "search";
		$this->BasicSearch->Type = @$_GET[EW_TABLE_BASIC_SEARCH_TYPE];
	}

	// Load form values
	function LoadFormValues() {

		// Load from form
		global $objForm;
		if (!$this->PrvFIni->FldIsDetailKey) {
			$this->PrvFIni->setFormValue($objForm->GetValue("x_PrvFIni"));
			$this->PrvFIni->CurrentValue = ew_UnFormatDateTime($this->PrvFIni->CurrentValue, 7);
		}
		$this->PrvFIni->setOldValue($objForm->GetValue("o_PrvFIni"));
		if (!$this->PrvFFin->FldIsDetailKey) {
			$this->PrvFFin->setFormValue($objForm->GetValue("x_PrvFFin"));
			$this->PrvFFin->CurrentValue = ew_UnFormatDateTime($this->PrvFFin->CurrentValue, 7);
		}
		$this->PrvFFin->setOldValue($objForm->GetValue("o_PrvFFin"));
		if (!$this->PrvPais->FldIsDetailKey) {
			$this->PrvPais->setFormValue($objForm->GetValue("x_PrvPais"));
		}
		$this->PrvPais->setOldValue($objForm->GetValue("o_PrvPais"));
		if (!$this->PrvTipo->FldIsDetailKey) {
			$this->PrvTipo->setFormValue($objForm->GetValue("x_PrvTipo"));
		}
		$this->PrvTipo->setOldValue($objForm->GetValue("o_PrvTipo"));
		if (!$this->PrvNomb->FldIsDetailKey) {
			$this->PrvNomb->setFormValue($objForm->GetValue("x_PrvNomb"));
		}
		$this->PrvNomb->setOldValue($objForm->GetValue("o_PrvNomb"));
		if (!$this->PrvApel->FldIsDetailKey) {
			$this->PrvApel->setFormValue($objForm->GetValue("x_PrvApel"));
		}
		$this->PrvApel->setOldValue($objForm->GetValue("o_PrvApel"));
		if (!$this->PrvCRuc->FldIsDetailKey) {
			$this->PrvCRuc->setFormValue($objForm->GetValue("x_PrvCRuc"));
		}
		$this->PrvCRuc->setOldValue($objForm->GetValue("o_PrvCRuc"));
		if (!$this->PrvDire->FldIsDetailKey) {
			$this->PrvDire->setFormValue($objForm->GetValue("x_PrvDire"));
		}
		$this->PrvDire->setOldValue($objForm->GetValue("o_PrvDire"));
		if (!$this->PrvMail->FldIsDetailKey) {
			$this->PrvMail->setFormValue($objForm->GetValue("x_PrvMail"));
		}
		$this->PrvMail->setOldValue($objForm->GetValue("o_PrvMail"));
		if (!$this->PrvTele->FldIsDetailKey) {
			$this->PrvTele->setFormValue($objForm->GetValue("x_PrvTele"));
		}
		$this->PrvTele->setOldValue($objForm->GetValue("o_PrvTele"));
		if (!$this->PrvRSoc->FldIsDetailKey) {
			$this->PrvRSoc->setFormValue($objForm->GetValue("x_PrvRSoc"));
		}
		$this->PrvRSoc->setOldValue($objForm->GetValue("o_PrvRSoc"));
		if (!$this->PrvCodi->FldIsDetailKey && $this->CurrentAction <> "gridadd" && $this->CurrentAction <> "add")
			$this->PrvCodi->setFormValue($objForm->GetValue("x_PrvCodi"));
	}

	// Restore form values
	function RestoreFormValues() {
		global $objForm;
		if ($this->CurrentAction <> "gridadd" && $this->CurrentAction <> "add")
			$this->PrvCodi->CurrentValue = $this->PrvCodi->FormValue;
		$this->PrvFIni->CurrentValue = $this->PrvFIni->FormValue;
		$this->PrvFIni->CurrentValue = ew_UnFormatDateTime($this->PrvFIni->CurrentValue, 7);
		$this->PrvFFin->CurrentValue = $this->PrvFFin->FormValue;
		$this->PrvFFin->CurrentValue = ew_UnFormatDateTime($this->PrvFFin->CurrentValue, 7);
		$this->PrvPais->CurrentValue = $this->PrvPais->FormValue;
		$this->PrvTipo->CurrentValue = $this->PrvTipo->FormValue;
		$this->PrvNomb->CurrentValue = $this->PrvNomb->FormValue;
		$this->PrvApel->CurrentValue = $this->PrvApel->FormValue;
		$this->PrvCRuc->CurrentValue = $this->PrvCRuc->FormValue;
		$this->PrvDire->CurrentValue = $this->PrvDire->FormValue;
		$this->PrvMail->CurrentValue = $this->PrvMail->FormValue;
		$this->PrvTele->CurrentValue = $this->PrvTele->FormValue;
		$this->PrvRSoc->CurrentValue = $this->PrvRSoc->FormValue;
	}

	// Load recordset
	function LoadRecordset($offset = -1, $rowcnt = -1) {

		// Load List page SQL
		$sSql = $this->SelectSQL();
		$conn = &$this->Connection();

		// Load recordset
		$dbtype = ew_GetConnectionType($this->DBID);
		if ($this->UseSelectLimit) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			if ($dbtype == "MSSQL") {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset, array("_hasOrderBy" => trim($this->getOrderBy()) || trim($this->getSessionOrderBy())));
			} else {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset);
			}
			$conn->raiseErrorFn = '';
		} else {
			$rs = ew_LoadRecordset($sSql, $conn);
		}

		// Call Recordset Selected event
		$this->Recordset_Selected($rs);
		return $rs;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->PrvCodi->setDbValue($rs->fields('PrvCodi'));
		$this->PrvFIni->setDbValue($rs->fields('PrvFIni'));
		$this->PrvFFin->setDbValue($rs->fields('PrvFFin'));
		$this->PrvPais->setDbValue($rs->fields('PrvPais'));
		$this->PrvTipo->setDbValue($rs->fields('PrvTipo'));
		$this->PrvNomb->setDbValue($rs->fields('PrvNomb'));
		$this->PrvApel->setDbValue($rs->fields('PrvApel'));
		$this->PrvCRuc->setDbValue($rs->fields('PrvCRuc'));
		$this->PrvDire->setDbValue($rs->fields('PrvDire'));
		$this->PrvMail->setDbValue($rs->fields('PrvMail'));
		$this->PrvTele->setDbValue($rs->fields('PrvTele'));
		$this->PrvRSoc->setDbValue($rs->fields('PrvRSoc'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->PrvCodi->DbValue = $row['PrvCodi'];
		$this->PrvFIni->DbValue = $row['PrvFIni'];
		$this->PrvFFin->DbValue = $row['PrvFFin'];
		$this->PrvPais->DbValue = $row['PrvPais'];
		$this->PrvTipo->DbValue = $row['PrvTipo'];
		$this->PrvNomb->DbValue = $row['PrvNomb'];
		$this->PrvApel->DbValue = $row['PrvApel'];
		$this->PrvCRuc->DbValue = $row['PrvCRuc'];
		$this->PrvDire->DbValue = $row['PrvDire'];
		$this->PrvMail->DbValue = $row['PrvMail'];
		$this->PrvTele->DbValue = $row['PrvTele'];
		$this->PrvRSoc->DbValue = $row['PrvRSoc'];
	}

	// Load old record
	function LoadOldRecord() {

		// Load key values from Session
		$bValidKey = TRUE;
		if (strval($this->getKey("PrvCodi")) <> "")
			$this->PrvCodi->CurrentValue = $this->getKey("PrvCodi"); // PrvCodi
		else
			$bValidKey = FALSE;

		// Load old recordset
		if ($bValidKey) {
			$this->CurrentFilter = $this->KeyFilter();
			$sSql = $this->SQL();
			$conn = &$this->Connection();
			$this->OldRecordset = ew_LoadRecordset($sSql, $conn);
			$this->LoadRowValues($this->OldRecordset); // Load row values
		} else {
			$this->OldRecordset = NULL;
		}
		return $bValidKey;
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		$this->ViewUrl = $this->GetViewUrl();
		$this->EditUrl = $this->GetEditUrl();
		$this->InlineEditUrl = $this->GetInlineEditUrl();
		$this->CopyUrl = $this->GetCopyUrl();
		$this->InlineCopyUrl = $this->GetInlineCopyUrl();
		$this->DeleteUrl = $this->GetDeleteUrl();

		// Call Row_Rendering event
		$this->Row_Rendering();

		// Common render codes for all row types
		// PrvCodi
		// PrvFIni
		// PrvFFin
		// PrvPais
		// PrvTipo
		// PrvNomb
		// PrvApel
		// PrvCRuc
		// PrvDire
		// PrvMail
		// PrvTele
		// PrvRSoc

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// PrvCodi
		$this->PrvCodi->ViewValue = $this->PrvCodi->CurrentValue;
		$this->PrvCodi->ViewCustomAttributes = "";

		// PrvFIni
		$this->PrvFIni->ViewValue = $this->PrvFIni->CurrentValue;
		$this->PrvFIni->ViewValue = ew_FormatDateTime($this->PrvFIni->ViewValue, 7);
		$this->PrvFIni->ViewCustomAttributes = "";

		// PrvFFin
		$this->PrvFFin->ViewValue = $this->PrvFFin->CurrentValue;
		$this->PrvFFin->ViewValue = ew_FormatDateTime($this->PrvFFin->ViewValue, 7);
		$this->PrvFFin->ViewCustomAttributes = "";

		// PrvPais
		$this->PrvPais->ViewValue = $this->PrvPais->CurrentValue;
		$this->PrvPais->ViewCustomAttributes = "";

		// PrvTipo
		$this->PrvTipo->ViewValue = $this->PrvTipo->CurrentValue;
		$this->PrvTipo->ViewCustomAttributes = "";

		// PrvNomb
		$this->PrvNomb->ViewValue = $this->PrvNomb->CurrentValue;
		$this->PrvNomb->ViewCustomAttributes = "";

		// PrvApel
		$this->PrvApel->ViewValue = $this->PrvApel->CurrentValue;
		$this->PrvApel->ViewCustomAttributes = "";

		// PrvCRuc
		$this->PrvCRuc->ViewValue = $this->PrvCRuc->CurrentValue;
		$this->PrvCRuc->ViewCustomAttributes = "";

		// PrvDire
		$this->PrvDire->ViewValue = $this->PrvDire->CurrentValue;
		$this->PrvDire->ViewCustomAttributes = "";

		// PrvMail
		$this->PrvMail->ViewValue = $this->PrvMail->CurrentValue;
		$this->PrvMail->ViewCustomAttributes = "";

		// PrvTele
		$this->PrvTele->ViewValue = $this->PrvTele->CurrentValue;
		$this->PrvTele->ViewCustomAttributes = "";

		// PrvRSoc
		$this->PrvRSoc->ViewValue = $this->PrvRSoc->CurrentValue;
		$this->PrvRSoc->ViewCustomAttributes = "";

			// PrvFIni
			$this->PrvFIni->LinkCustomAttributes = "";
			$this->PrvFIni->HrefValue = "";
			$this->PrvFIni->TooltipValue = "";

			// PrvFFin
			$this->PrvFFin->LinkCustomAttributes = "";
			$this->PrvFFin->HrefValue = "";
			$this->PrvFFin->TooltipValue = "";

			// PrvPais
			$this->PrvPais->LinkCustomAttributes = "";
			$this->PrvPais->HrefValue = "";
			$this->PrvPais->TooltipValue = "";

			// PrvTipo
			$this->PrvTipo->LinkCustomAttributes = "";
			$this->PrvTipo->HrefValue = "";
			$this->PrvTipo->TooltipValue = "";

			// PrvNomb
			$this->PrvNomb->LinkCustomAttributes = "";
			$this->PrvNomb->HrefValue = "";
			$this->PrvNomb->TooltipValue = "";

			// PrvApel
			$this->PrvApel->LinkCustomAttributes = "";
			$this->PrvApel->HrefValue = "";
			$this->PrvApel->TooltipValue = "";

			// PrvCRuc
			$this->PrvCRuc->LinkCustomAttributes = "";
			$this->PrvCRuc->HrefValue = "";
			$this->PrvCRuc->TooltipValue = "";

			// PrvDire
			$this->PrvDire->LinkCustomAttributes = "";
			$this->PrvDire->HrefValue = "";
			$this->PrvDire->TooltipValue = "";

			// PrvMail
			$this->PrvMail->LinkCustomAttributes = "";
			$this->PrvMail->HrefValue = "";
			$this->PrvMail->TooltipValue = "";

			// PrvTele
			$this->PrvTele->LinkCustomAttributes = "";
			$this->PrvTele->HrefValue = "";
			$this->PrvTele->TooltipValue = "";

			// PrvRSoc
			$this->PrvRSoc->LinkCustomAttributes = "";
			$this->PrvRSoc->HrefValue = "";
			$this->PrvRSoc->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_ADD) { // Add row

			// PrvFIni
			$this->PrvFIni->EditAttrs["class"] = "form-control";
			$this->PrvFIni->EditCustomAttributes = "";
			$this->PrvFIni->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->PrvFIni->CurrentValue, 7));
			$this->PrvFIni->PlaceHolder = ew_RemoveHtml($this->PrvFIni->FldCaption());

			// PrvFFin
			$this->PrvFFin->EditAttrs["class"] = "form-control";
			$this->PrvFFin->EditCustomAttributes = "";
			$this->PrvFFin->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->PrvFFin->CurrentValue, 7));
			$this->PrvFFin->PlaceHolder = ew_RemoveHtml($this->PrvFFin->FldCaption());

			// PrvPais
			$this->PrvPais->EditAttrs["class"] = "form-control";
			$this->PrvPais->EditCustomAttributes = "";
			$this->PrvPais->EditValue = ew_HtmlEncode($this->PrvPais->CurrentValue);
			$this->PrvPais->PlaceHolder = ew_RemoveHtml($this->PrvPais->FldCaption());

			// PrvTipo
			$this->PrvTipo->EditAttrs["class"] = "form-control";
			$this->PrvTipo->EditCustomAttributes = "";
			$this->PrvTipo->EditValue = ew_HtmlEncode($this->PrvTipo->CurrentValue);
			$this->PrvTipo->PlaceHolder = ew_RemoveHtml($this->PrvTipo->FldCaption());

			// PrvNomb
			$this->PrvNomb->EditAttrs["class"] = "form-control";
			$this->PrvNomb->EditCustomAttributes = "";
			$this->PrvNomb->EditValue = ew_HtmlEncode($this->PrvNomb->CurrentValue);
			$this->PrvNomb->PlaceHolder = ew_RemoveHtml($this->PrvNomb->FldCaption());

			// PrvApel
			$this->PrvApel->EditAttrs["class"] = "form-control";
			$this->PrvApel->EditCustomAttributes = "";
			$this->PrvApel->EditValue = ew_HtmlEncode($this->PrvApel->CurrentValue);
			$this->PrvApel->PlaceHolder = ew_RemoveHtml($this->PrvApel->FldCaption());

			// PrvCRuc
			$this->PrvCRuc->EditAttrs["class"] = "form-control";
			$this->PrvCRuc->EditCustomAttributes = "";
			$this->PrvCRuc->EditValue = ew_HtmlEncode($this->PrvCRuc->CurrentValue);
			$this->PrvCRuc->PlaceHolder = ew_RemoveHtml($this->PrvCRuc->FldCaption());

			// PrvDire
			$this->PrvDire->EditAttrs["class"] = "form-control";
			$this->PrvDire->EditCustomAttributes = "";
			$this->PrvDire->EditValue = ew_HtmlEncode($this->PrvDire->CurrentValue);
			$this->PrvDire->PlaceHolder = ew_RemoveHtml($this->PrvDire->FldCaption());

			// PrvMail
			$this->PrvMail->EditAttrs["class"] = "form-control";
			$this->PrvMail->EditCustomAttributes = "";
			$this->PrvMail->EditValue = ew_HtmlEncode($this->PrvMail->CurrentValue);
			$this->PrvMail->PlaceHolder = ew_RemoveHtml($this->PrvMail->FldCaption());

			// PrvTele
			$this->PrvTele->EditAttrs["class"] = "form-control";
			$this->PrvTele->EditCustomAttributes = "";
			$this->PrvTele->EditValue = ew_HtmlEncode($this->PrvTele->CurrentValue);
			$this->PrvTele->PlaceHolder = ew_RemoveHtml($this->PrvTele->FldCaption());

			// PrvRSoc
			$this->PrvRSoc->EditAttrs["class"] = "form-control";
			$this->PrvRSoc->EditCustomAttributes = "";
			$this->PrvRSoc->EditValue = ew_HtmlEncode($this->PrvRSoc->CurrentValue);
			$this->PrvRSoc->PlaceHolder = ew_RemoveHtml($this->PrvRSoc->FldCaption());

			// Edit refer script
			// PrvFIni

			$this->PrvFIni->HrefValue = "";

			// PrvFFin
			$this->PrvFFin->HrefValue = "";

			// PrvPais
			$this->PrvPais->HrefValue = "";

			// PrvTipo
			$this->PrvTipo->HrefValue = "";

			// PrvNomb
			$this->PrvNomb->HrefValue = "";

			// PrvApel
			$this->PrvApel->HrefValue = "";

			// PrvCRuc
			$this->PrvCRuc->HrefValue = "";

			// PrvDire
			$this->PrvDire->HrefValue = "";

			// PrvMail
			$this->PrvMail->HrefValue = "";

			// PrvTele
			$this->PrvTele->HrefValue = "";

			// PrvRSoc
			$this->PrvRSoc->HrefValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_EDIT) { // Edit row

			// PrvFIni
			$this->PrvFIni->EditAttrs["class"] = "form-control";
			$this->PrvFIni->EditCustomAttributes = "";
			$this->PrvFIni->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->PrvFIni->CurrentValue, 7));
			$this->PrvFIni->PlaceHolder = ew_RemoveHtml($this->PrvFIni->FldCaption());

			// PrvFFin
			$this->PrvFFin->EditAttrs["class"] = "form-control";
			$this->PrvFFin->EditCustomAttributes = "";
			$this->PrvFFin->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->PrvFFin->CurrentValue, 7));
			$this->PrvFFin->PlaceHolder = ew_RemoveHtml($this->PrvFFin->FldCaption());

			// PrvPais
			$this->PrvPais->EditAttrs["class"] = "form-control";
			$this->PrvPais->EditCustomAttributes = "";
			$this->PrvPais->EditValue = ew_HtmlEncode($this->PrvPais->CurrentValue);
			$this->PrvPais->PlaceHolder = ew_RemoveHtml($this->PrvPais->FldCaption());

			// PrvTipo
			$this->PrvTipo->EditAttrs["class"] = "form-control";
			$this->PrvTipo->EditCustomAttributes = "";
			$this->PrvTipo->EditValue = ew_HtmlEncode($this->PrvTipo->CurrentValue);
			$this->PrvTipo->PlaceHolder = ew_RemoveHtml($this->PrvTipo->FldCaption());

			// PrvNomb
			$this->PrvNomb->EditAttrs["class"] = "form-control";
			$this->PrvNomb->EditCustomAttributes = "";
			$this->PrvNomb->EditValue = ew_HtmlEncode($this->PrvNomb->CurrentValue);
			$this->PrvNomb->PlaceHolder = ew_RemoveHtml($this->PrvNomb->FldCaption());

			// PrvApel
			$this->PrvApel->EditAttrs["class"] = "form-control";
			$this->PrvApel->EditCustomAttributes = "";
			$this->PrvApel->EditValue = ew_HtmlEncode($this->PrvApel->CurrentValue);
			$this->PrvApel->PlaceHolder = ew_RemoveHtml($this->PrvApel->FldCaption());

			// PrvCRuc
			$this->PrvCRuc->EditAttrs["class"] = "form-control";
			$this->PrvCRuc->EditCustomAttributes = "";
			$this->PrvCRuc->EditValue = ew_HtmlEncode($this->PrvCRuc->CurrentValue);
			$this->PrvCRuc->PlaceHolder = ew_RemoveHtml($this->PrvCRuc->FldCaption());

			// PrvDire
			$this->PrvDire->EditAttrs["class"] = "form-control";
			$this->PrvDire->EditCustomAttributes = "";
			$this->PrvDire->EditValue = ew_HtmlEncode($this->PrvDire->CurrentValue);
			$this->PrvDire->PlaceHolder = ew_RemoveHtml($this->PrvDire->FldCaption());

			// PrvMail
			$this->PrvMail->EditAttrs["class"] = "form-control";
			$this->PrvMail->EditCustomAttributes = "";
			$this->PrvMail->EditValue = ew_HtmlEncode($this->PrvMail->CurrentValue);
			$this->PrvMail->PlaceHolder = ew_RemoveHtml($this->PrvMail->FldCaption());

			// PrvTele
			$this->PrvTele->EditAttrs["class"] = "form-control";
			$this->PrvTele->EditCustomAttributes = "";
			$this->PrvTele->EditValue = ew_HtmlEncode($this->PrvTele->CurrentValue);
			$this->PrvTele->PlaceHolder = ew_RemoveHtml($this->PrvTele->FldCaption());

			// PrvRSoc
			$this->PrvRSoc->EditAttrs["class"] = "form-control";
			$this->PrvRSoc->EditCustomAttributes = "";
			$this->PrvRSoc->EditValue = ew_HtmlEncode($this->PrvRSoc->CurrentValue);
			$this->PrvRSoc->PlaceHolder = ew_RemoveHtml($this->PrvRSoc->FldCaption());

			// Edit refer script
			// PrvFIni

			$this->PrvFIni->HrefValue = "";

			// PrvFFin
			$this->PrvFFin->HrefValue = "";

			// PrvPais
			$this->PrvPais->HrefValue = "";

			// PrvTipo
			$this->PrvTipo->HrefValue = "";

			// PrvNomb
			$this->PrvNomb->HrefValue = "";

			// PrvApel
			$this->PrvApel->HrefValue = "";

			// PrvCRuc
			$this->PrvCRuc->HrefValue = "";

			// PrvDire
			$this->PrvDire->HrefValue = "";

			// PrvMail
			$this->PrvMail->HrefValue = "";

			// PrvTele
			$this->PrvTele->HrefValue = "";

			// PrvRSoc
			$this->PrvRSoc->HrefValue = "";
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate form
	function ValidateForm() {
		global $Language, $gsFormError;

		// Initialize form error message
		$gsFormError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return ($gsFormError == "");
		if (!$this->PrvFIni->FldIsDetailKey && !is_null($this->PrvFIni->FormValue) && $this->PrvFIni->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->PrvFIni->FldCaption(), $this->PrvFIni->ReqErrMsg));
		}
		if (!ew_CheckEuroDate($this->PrvFIni->FormValue)) {
			ew_AddMessage($gsFormError, $this->PrvFIni->FldErrMsg());
		}
		if (!ew_CheckEuroDate($this->PrvFFin->FormValue)) {
			ew_AddMessage($gsFormError, $this->PrvFFin->FldErrMsg());
		}
		if (!$this->PrvPais->FldIsDetailKey && !is_null($this->PrvPais->FormValue) && $this->PrvPais->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->PrvPais->FldCaption(), $this->PrvPais->ReqErrMsg));
		}
		if (!$this->PrvTipo->FldIsDetailKey && !is_null($this->PrvTipo->FormValue) && $this->PrvTipo->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->PrvTipo->FldCaption(), $this->PrvTipo->ReqErrMsg));
		}
		if (!$this->PrvCRuc->FldIsDetailKey && !is_null($this->PrvCRuc->FormValue) && $this->PrvCRuc->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->PrvCRuc->FldCaption(), $this->PrvCRuc->ReqErrMsg));
		}
		if (!$this->PrvMail->FldIsDetailKey && !is_null($this->PrvMail->FormValue) && $this->PrvMail->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->PrvMail->FldCaption(), $this->PrvMail->ReqErrMsg));
		}
		if (!ew_CheckEmail($this->PrvMail->FormValue)) {
			ew_AddMessage($gsFormError, $this->PrvMail->FldErrMsg());
		}
		if (!ew_CheckInteger($this->PrvTele->FormValue)) {
			ew_AddMessage($gsFormError, $this->PrvTele->FldErrMsg());
		}
		if (!$this->PrvRSoc->FldIsDetailKey && !is_null($this->PrvRSoc->FormValue) && $this->PrvRSoc->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->PrvRSoc->FldCaption(), $this->PrvRSoc->ReqErrMsg));
		}

		// Return validate result
		$ValidateForm = ($gsFormError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateForm = $ValidateForm && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsFormError, $sFormCustomError);
		}
		return $ValidateForm;
	}

	//
	// Delete records based on current filter
	//
	function DeleteRows() {
		global $Language, $Security;
		if (!$Security->CanDelete()) {
			$this->setFailureMessage($Language->Phrase("NoDeletePermission")); // No delete permission
			return FALSE;
		}
		$DeleteRows = TRUE;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE) {
			return FALSE;
		} elseif ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
			$rs->Close();
			return FALSE;

		//} else {
		//	$this->LoadRowValues($rs); // Load row values

		}
		$rows = ($rs) ? $rs->GetRows() : array();

		// Clone old rows
		$rsold = $rows;
		if ($rs)
			$rs->Close();

		// Call row deleting event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$DeleteRows = $this->Row_Deleting($row);
				if (!$DeleteRows) break;
			}
		}
		if ($DeleteRows) {
			$sKey = "";
			foreach ($rsold as $row) {
				$sThisKey = "";
				if ($sThisKey <> "") $sThisKey .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
				$sThisKey .= $row['PrvCodi'];
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				$DeleteRows = $this->Delete($row); // Delete
				$conn->raiseErrorFn = '';
				if ($DeleteRows === FALSE)
					break;
				if ($sKey <> "") $sKey .= ", ";
				$sKey .= $sThisKey;
			}
		} else {

			// Set up error message
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("DeleteCancelled"));
			}
		}
		if ($DeleteRows) {
		} else {
		}

		// Call Row Deleted event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$this->Row_Deleted($row);
			}
		}
		return $DeleteRows;
	}

	// Update record based on key values
	function EditRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$conn = &$this->Connection();
		if ($this->PrvCRuc->CurrentValue <> "") { // Check field with unique index
			$sFilterChk = "(\"PrvCRuc\" = '" . ew_AdjustSql($this->PrvCRuc->CurrentValue, $this->DBID) . "')";
			$sFilterChk .= " AND NOT (" . $sFilter . ")";
			$this->CurrentFilter = $sFilterChk;
			$sSqlChk = $this->SQL();
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			$rsChk = $conn->Execute($sSqlChk);
			$conn->raiseErrorFn = '';
			if ($rsChk === FALSE) {
				return FALSE;
			} elseif (!$rsChk->EOF) {
				$sIdxErrMsg = str_replace("%f", $this->PrvCRuc->FldCaption(), $Language->Phrase("DupIndex"));
				$sIdxErrMsg = str_replace("%v", $this->PrvCRuc->CurrentValue, $sIdxErrMsg);
				$this->setFailureMessage($sIdxErrMsg);
				$rsChk->Close();
				return FALSE;
			}
			$rsChk->Close();
		}
		if ($this->PrvMail->CurrentValue <> "") { // Check field with unique index
			$sFilterChk = "(\"PrvMail\" = '" . ew_AdjustSql($this->PrvMail->CurrentValue, $this->DBID) . "')";
			$sFilterChk .= " AND NOT (" . $sFilter . ")";
			$this->CurrentFilter = $sFilterChk;
			$sSqlChk = $this->SQL();
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			$rsChk = $conn->Execute($sSqlChk);
			$conn->raiseErrorFn = '';
			if ($rsChk === FALSE) {
				return FALSE;
			} elseif (!$rsChk->EOF) {
				$sIdxErrMsg = str_replace("%f", $this->PrvMail->FldCaption(), $Language->Phrase("DupIndex"));
				$sIdxErrMsg = str_replace("%v", $this->PrvMail->CurrentValue, $sIdxErrMsg);
				$this->setFailureMessage($sIdxErrMsg);
				$rsChk->Close();
				return FALSE;
			}
			$rsChk->Close();
		}
		if ($this->PrvRSoc->CurrentValue <> "") { // Check field with unique index
			$sFilterChk = "(\"PrvRSoc\" = '" . ew_AdjustSql($this->PrvRSoc->CurrentValue, $this->DBID) . "')";
			$sFilterChk .= " AND NOT (" . $sFilter . ")";
			$this->CurrentFilter = $sFilterChk;
			$sSqlChk = $this->SQL();
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			$rsChk = $conn->Execute($sSqlChk);
			$conn->raiseErrorFn = '';
			if ($rsChk === FALSE) {
				return FALSE;
			} elseif (!$rsChk->EOF) {
				$sIdxErrMsg = str_replace("%f", $this->PrvRSoc->FldCaption(), $Language->Phrase("DupIndex"));
				$sIdxErrMsg = str_replace("%v", $this->PrvRSoc->CurrentValue, $sIdxErrMsg);
				$this->setFailureMessage($sIdxErrMsg);
				$rsChk->Close();
				return FALSE;
			}
			$rsChk->Close();
		}
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE)
			return FALSE;
		if ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record message
			$EditRow = FALSE; // Update Failed
		} else {

			// Save old values
			$rsold = &$rs->fields;
			$this->LoadDbValues($rsold);
			$rsnew = array();

			// PrvFIni
			$this->PrvFIni->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->PrvFIni->CurrentValue, 7), ew_CurrentDate(), $this->PrvFIni->ReadOnly);

			// PrvFFin
			$this->PrvFFin->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->PrvFFin->CurrentValue, 7), NULL, $this->PrvFFin->ReadOnly);

			// PrvPais
			$this->PrvPais->SetDbValueDef($rsnew, $this->PrvPais->CurrentValue, "", $this->PrvPais->ReadOnly);

			// PrvTipo
			$this->PrvTipo->SetDbValueDef($rsnew, $this->PrvTipo->CurrentValue, "", $this->PrvTipo->ReadOnly);

			// PrvNomb
			$this->PrvNomb->SetDbValueDef($rsnew, $this->PrvNomb->CurrentValue, NULL, $this->PrvNomb->ReadOnly);

			// PrvApel
			$this->PrvApel->SetDbValueDef($rsnew, $this->PrvApel->CurrentValue, NULL, $this->PrvApel->ReadOnly);

			// PrvCRuc
			$this->PrvCRuc->SetDbValueDef($rsnew, $this->PrvCRuc->CurrentValue, NULL, $this->PrvCRuc->ReadOnly);

			// PrvDire
			$this->PrvDire->SetDbValueDef($rsnew, $this->PrvDire->CurrentValue, NULL, $this->PrvDire->ReadOnly);

			// PrvMail
			$this->PrvMail->SetDbValueDef($rsnew, $this->PrvMail->CurrentValue, NULL, $this->PrvMail->ReadOnly);

			// PrvTele
			$this->PrvTele->SetDbValueDef($rsnew, $this->PrvTele->CurrentValue, NULL, $this->PrvTele->ReadOnly);

			// PrvRSoc
			$this->PrvRSoc->SetDbValueDef($rsnew, $this->PrvRSoc->CurrentValue, NULL, $this->PrvRSoc->ReadOnly);

			// Call Row Updating event
			$bUpdateRow = $this->Row_Updating($rsold, $rsnew);
			if ($bUpdateRow) {
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				if (count($rsnew) > 0)
					$EditRow = $this->Update($rsnew, "", $rsold);
				else
					$EditRow = TRUE; // No field to update
				$conn->raiseErrorFn = '';
				if ($EditRow) {
				}
			} else {
				if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

					// Use the message, do nothing
				} elseif ($this->CancelMessage <> "") {
					$this->setFailureMessage($this->CancelMessage);
					$this->CancelMessage = "";
				} else {
					$this->setFailureMessage($Language->Phrase("UpdateCancelled"));
				}
				$EditRow = FALSE;
			}
		}

		// Call Row_Updated event
		if ($EditRow)
			$this->Row_Updated($rsold, $rsnew);
		$rs->Close();
		return $EditRow;
	}

	// Add record
	function AddRow($rsold = NULL) {
		global $Language, $Security;
		if ($this->PrvCRuc->CurrentValue <> "") { // Check field with unique index
			$sFilter = "(PrvCRuc = '" . ew_AdjustSql($this->PrvCRuc->CurrentValue, $this->DBID) . "')";
			$rsChk = $this->LoadRs($sFilter);
			if ($rsChk && !$rsChk->EOF) {
				$sIdxErrMsg = str_replace("%f", $this->PrvCRuc->FldCaption(), $Language->Phrase("DupIndex"));
				$sIdxErrMsg = str_replace("%v", $this->PrvCRuc->CurrentValue, $sIdxErrMsg);
				$this->setFailureMessage($sIdxErrMsg);
				$rsChk->Close();
				return FALSE;
			}
		}
		if ($this->PrvMail->CurrentValue <> "") { // Check field with unique index
			$sFilter = "(PrvMail = '" . ew_AdjustSql($this->PrvMail->CurrentValue, $this->DBID) . "')";
			$rsChk = $this->LoadRs($sFilter);
			if ($rsChk && !$rsChk->EOF) {
				$sIdxErrMsg = str_replace("%f", $this->PrvMail->FldCaption(), $Language->Phrase("DupIndex"));
				$sIdxErrMsg = str_replace("%v", $this->PrvMail->CurrentValue, $sIdxErrMsg);
				$this->setFailureMessage($sIdxErrMsg);
				$rsChk->Close();
				return FALSE;
			}
		}
		if ($this->PrvRSoc->CurrentValue <> "") { // Check field with unique index
			$sFilter = "(PrvRSoc = '" . ew_AdjustSql($this->PrvRSoc->CurrentValue, $this->DBID) . "')";
			$rsChk = $this->LoadRs($sFilter);
			if ($rsChk && !$rsChk->EOF) {
				$sIdxErrMsg = str_replace("%f", $this->PrvRSoc->FldCaption(), $Language->Phrase("DupIndex"));
				$sIdxErrMsg = str_replace("%v", $this->PrvRSoc->CurrentValue, $sIdxErrMsg);
				$this->setFailureMessage($sIdxErrMsg);
				$rsChk->Close();
				return FALSE;
			}
		}
		$conn = &$this->Connection();

		// Load db values from rsold
		if ($rsold) {
			$this->LoadDbValues($rsold);
		}
		$rsnew = array();

		// PrvFIni
		$this->PrvFIni->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->PrvFIni->CurrentValue, 7), ew_CurrentDate(), FALSE);

		// PrvFFin
		$this->PrvFFin->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->PrvFFin->CurrentValue, 7), NULL, FALSE);

		// PrvPais
		$this->PrvPais->SetDbValueDef($rsnew, $this->PrvPais->CurrentValue, "", FALSE);

		// PrvTipo
		$this->PrvTipo->SetDbValueDef($rsnew, $this->PrvTipo->CurrentValue, "", FALSE);

		// PrvNomb
		$this->PrvNomb->SetDbValueDef($rsnew, $this->PrvNomb->CurrentValue, NULL, FALSE);

		// PrvApel
		$this->PrvApel->SetDbValueDef($rsnew, $this->PrvApel->CurrentValue, NULL, FALSE);

		// PrvCRuc
		$this->PrvCRuc->SetDbValueDef($rsnew, $this->PrvCRuc->CurrentValue, NULL, FALSE);

		// PrvDire
		$this->PrvDire->SetDbValueDef($rsnew, $this->PrvDire->CurrentValue, NULL, FALSE);

		// PrvMail
		$this->PrvMail->SetDbValueDef($rsnew, $this->PrvMail->CurrentValue, NULL, FALSE);

		// PrvTele
		$this->PrvTele->SetDbValueDef($rsnew, $this->PrvTele->CurrentValue, NULL, FALSE);

		// PrvRSoc
		$this->PrvRSoc->SetDbValueDef($rsnew, $this->PrvRSoc->CurrentValue, NULL, FALSE);

		// Call Row Inserting event
		$rs = ($rsold == NULL) ? NULL : $rsold->fields;
		$bInsertRow = $this->Row_Inserting($rs, $rsnew);
		if ($bInsertRow) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			$AddRow = $this->Insert($rsnew);
			$conn->raiseErrorFn = '';
			if ($AddRow) {

				// Get insert id if necessary
				$this->PrvCodi->setDbValue($conn->GetOne("SELECT currval('\"Prov_PrvCodi_seq\"'::regclass)"));
				$rsnew['PrvCodi'] = $this->PrvCodi->DbValue;
			}
		} else {
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("InsertCancelled"));
			}
			$AddRow = FALSE;
		}
		if ($AddRow) {

			// Call Row Inserted event
			$rs = ($rsold == NULL) ? NULL : $rsold->fields;
			$this->Row_Inserted($rs, $rsnew);
		}
		return $AddRow;
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$url = preg_replace('/\?cmd=reset(all){0,1}$/i', '', $url); // Remove cmd=reset / cmd=resetall
		$Breadcrumb->Add("list", $this->TableVar, $url, "", $this->TableVar, TRUE);
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}

	// ListOptions Load event
	function ListOptions_Load() {

		// Example:
		//$opt = &$this->ListOptions->Add("new");
		//$opt->Header = "xxx";
		//$opt->OnLeft = TRUE; // Link on left
		//$opt->MoveTo(0); // Move to first column

	}

	// ListOptions Rendered event
	function ListOptions_Rendered() {

		// Example: 
		//$this->ListOptions->Items["new"]->Body = "xxx";

	}

	// Row Custom Action event
	function Row_CustomAction($action, $row) {

		// Return FALSE to abort
		return TRUE;
	}

	// Page Exporting event
	// $this->ExportDoc = export document object
	function Page_Exporting() {

		//$this->ExportDoc->Text = "my header"; // Export header
		//return FALSE; // Return FALSE to skip default export and use Row_Export event

		return TRUE; // Return TRUE to use default export and skip Row_Export event
	}

	// Row Export event
	// $this->ExportDoc = export document object
	function Row_Export($rs) {

	    //$this->ExportDoc->Text .= "my content"; // Build HTML with field value: $rs["MyField"] or $this->MyField->ViewValue
	}

	// Page Exported event
	// $this->ExportDoc = export document object
	function Page_Exported() {

		//$this->ExportDoc->Text .= "my footer"; // Export footer
		//echo $this->ExportDoc->Text;

	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($Prov_list)) $Prov_list = new cProv_list();

// Page init
$Prov_list->Page_Init();

// Page main
$Prov_list->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$Prov_list->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "list";
var CurrentForm = fProvlist = new ew_Form("fProvlist", "list");
fProvlist.FormKeyCountName = '<?php echo $Prov_list->FormKeyCountName ?>';

// Validate form
fProvlist.Validate = function() {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.GetForm(), $fobj = $(fobj);
	if ($fobj.find("#a_confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.FormKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = $fobj.find("#a_list").val() == "gridinsert";
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
		var checkrow = (gridinsert) ? !this.EmptyRow(infix) : true;
		if (checkrow) {
			addcnt++;
			elm = this.GetElements("x" + infix + "_PrvFIni");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $Prov->PrvFIni->FldCaption(), $Prov->PrvFIni->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_PrvFIni");
			if (elm && !ew_CheckEuroDate(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($Prov->PrvFIni->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_PrvFFin");
			if (elm && !ew_CheckEuroDate(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($Prov->PrvFFin->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_PrvPais");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $Prov->PrvPais->FldCaption(), $Prov->PrvPais->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_PrvTipo");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $Prov->PrvTipo->FldCaption(), $Prov->PrvTipo->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_PrvCRuc");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $Prov->PrvCRuc->FldCaption(), $Prov->PrvCRuc->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_PrvMail");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $Prov->PrvMail->FldCaption(), $Prov->PrvMail->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_PrvMail");
			if (elm && !ew_CheckEmail(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($Prov->PrvMail->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_PrvTele");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($Prov->PrvTele->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_PrvRSoc");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $Prov->PrvRSoc->FldCaption(), $Prov->PrvRSoc->ReqErrMsg)) ?>");

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
		} // End Grid Add checking
	}
	if (gridinsert && addcnt == 0) { // No row added
		ew_Alert(ewLanguage.Phrase("NoAddRecord"));
		return false;
	}
	return true;
}

// Check empty row
fProvlist.EmptyRow = function(infix) {
	var fobj = this.Form;
	if (ew_ValueChanged(fobj, infix, "PrvFIni", false)) return false;
	if (ew_ValueChanged(fobj, infix, "PrvFFin", false)) return false;
	if (ew_ValueChanged(fobj, infix, "PrvPais", false)) return false;
	if (ew_ValueChanged(fobj, infix, "PrvTipo", false)) return false;
	if (ew_ValueChanged(fobj, infix, "PrvNomb", false)) return false;
	if (ew_ValueChanged(fobj, infix, "PrvApel", false)) return false;
	if (ew_ValueChanged(fobj, infix, "PrvCRuc", false)) return false;
	if (ew_ValueChanged(fobj, infix, "PrvDire", false)) return false;
	if (ew_ValueChanged(fobj, infix, "PrvMail", false)) return false;
	if (ew_ValueChanged(fobj, infix, "PrvTele", false)) return false;
	if (ew_ValueChanged(fobj, infix, "PrvRSoc", false)) return false;
	return true;
}

// Form_CustomValidate event
fProvlist.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fProvlist.ValidateRequired = true;
<?php } else { ?>
fProvlist.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
// Form object for search

var CurrentSearchForm = fProvlistsrch = new ew_Form("fProvlistsrch");
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php if ($Prov_list->TotalRecs > 0 && $Prov_list->ExportOptions->Visible()) { ?>
<?php $Prov_list->ExportOptions->Render("body") ?>
<?php } ?>
<?php if ($Prov_list->SearchOptions->Visible()) { ?>
<?php $Prov_list->SearchOptions->Render("body") ?>
<?php } ?>
<?php if ($Prov_list->FilterOptions->Visible()) { ?>
<?php $Prov_list->FilterOptions->Render("body") ?>
<?php } ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php
if ($Prov->CurrentAction == "gridadd") {
	$Prov->CurrentFilter = "0=1";
	$Prov_list->StartRec = 1;
	$Prov_list->DisplayRecs = $Prov->GridAddRowCount;
	$Prov_list->TotalRecs = $Prov_list->DisplayRecs;
	$Prov_list->StopRec = $Prov_list->DisplayRecs;
} else {
	$bSelectLimit = $Prov_list->UseSelectLimit;
	if ($bSelectLimit) {
		if ($Prov_list->TotalRecs <= 0)
			$Prov_list->TotalRecs = $Prov->SelectRecordCount();
	} else {
		if (!$Prov_list->Recordset && ($Prov_list->Recordset = $Prov_list->LoadRecordset()))
			$Prov_list->TotalRecs = $Prov_list->Recordset->RecordCount();
	}
	$Prov_list->StartRec = 1;
	if ($Prov_list->DisplayRecs <= 0 || ($Prov->Export <> "" && $Prov->ExportAll)) // Display all records
		$Prov_list->DisplayRecs = $Prov_list->TotalRecs;
	if (!($Prov->Export <> "" && $Prov->ExportAll))
		$Prov_list->SetUpStartRec(); // Set up start record position
	if ($bSelectLimit)
		$Prov_list->Recordset = $Prov_list->LoadRecordset($Prov_list->StartRec-1, $Prov_list->DisplayRecs);

	// Set no record found message
	if ($Prov->CurrentAction == "" && $Prov_list->TotalRecs == 0) {
		if (!$Security->CanList())
			$Prov_list->setWarningMessage($Language->Phrase("NoPermission"));
		if ($Prov_list->SearchWhere == "0=101")
			$Prov_list->setWarningMessage($Language->Phrase("EnterSearchCriteria"));
		else
			$Prov_list->setWarningMessage($Language->Phrase("NoRecord"));
	}
}
$Prov_list->RenderOtherOptions();
?>
<?php if ($Security->CanSearch()) { ?>
<?php if ($Prov->Export == "" && $Prov->CurrentAction == "") { ?>
<form name="fProvlistsrch" id="fProvlistsrch" class="form-inline ewForm" action="<?php echo ew_CurrentPage() ?>">
<?php $SearchPanelClass = ($Prov_list->SearchWhere <> "") ? " in" : " in"; ?>
<div id="fProvlistsrch_SearchPanel" class="ewSearchPanel collapse<?php echo $SearchPanelClass ?>">
<input type="hidden" name="cmd" value="search">
<input type="hidden" name="t" value="Prov">
	<div class="ewBasicSearch">
<div id="xsr_1" class="ewRow">
	<div class="ewQuickSearch input-group">
	<input type="text" name="<?php echo EW_TABLE_BASIC_SEARCH ?>" id="<?php echo EW_TABLE_BASIC_SEARCH ?>" class="form-control" value="<?php echo ew_HtmlEncode($Prov_list->BasicSearch->getKeyword()) ?>" placeholder="<?php echo ew_HtmlEncode($Language->Phrase("Search")) ?>">
	<input type="hidden" name="<?php echo EW_TABLE_BASIC_SEARCH_TYPE ?>" id="<?php echo EW_TABLE_BASIC_SEARCH_TYPE ?>" value="<?php echo ew_HtmlEncode($Prov_list->BasicSearch->getType()) ?>">
	<div class="input-group-btn">
		<button type="button" data-toggle="dropdown" class="btn btn-default"><span id="searchtype"><?php echo $Prov_list->BasicSearch->getTypeNameShort() ?></span><span class="caret"></span></button>
		<ul class="dropdown-menu pull-right" role="menu">
			<li<?php if ($Prov_list->BasicSearch->getType() == "") echo " class=\"active\""; ?>><a href="javascript:void(0);" onclick="ew_SetSearchType(this)"><?php echo $Language->Phrase("QuickSearchAuto") ?></a></li>
			<li<?php if ($Prov_list->BasicSearch->getType() == "=") echo " class=\"active\""; ?>><a href="javascript:void(0);" onclick="ew_SetSearchType(this,'=')"><?php echo $Language->Phrase("QuickSearchExact") ?></a></li>
			<li<?php if ($Prov_list->BasicSearch->getType() == "AND") echo " class=\"active\""; ?>><a href="javascript:void(0);" onclick="ew_SetSearchType(this,'AND')"><?php echo $Language->Phrase("QuickSearchAll") ?></a></li>
			<li<?php if ($Prov_list->BasicSearch->getType() == "OR") echo " class=\"active\""; ?>><a href="javascript:void(0);" onclick="ew_SetSearchType(this,'OR')"><?php echo $Language->Phrase("QuickSearchAny") ?></a></li>
		</ul>
	<button class="btn btn-primary ewButton" name="btnsubmit" id="btnsubmit" type="submit"><?php echo $Language->Phrase("QuickSearchBtn") ?></button>
	</div>
	</div>
</div>
	</div>
</div>
</form>
<?php } ?>
<?php } ?>
<?php $Prov_list->ShowPageHeader(); ?>
<?php
$Prov_list->ShowMessage();
?>
<?php if ($Prov_list->TotalRecs > 0 || $Prov->CurrentAction <> "") { ?>
<div class="panel panel-default ewGrid">
<form name="fProvlist" id="fProvlist" class="form-inline ewForm ewListForm" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($Prov_list->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $Prov_list->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="Prov">
<div id="gmp_Prov" class="<?php if (ew_IsResponsiveLayout()) { echo "table-responsive "; } ?>ewGridMiddlePanel">
<?php if ($Prov_list->TotalRecs > 0) { ?>
<table id="tbl_Provlist" class="table ewTable">
<?php echo $Prov->TableCustomInnerHtml ?>
<thead><!-- Table header -->
	<tr class="ewTableHeader">
<?php

// Header row
$Prov_list->RowType = EW_ROWTYPE_HEADER;

// Render list options
$Prov_list->RenderListOptions();

// Render list options (header, left)
$Prov_list->ListOptions->Render("header", "left");
?>
<?php if ($Prov->PrvFIni->Visible) { // PrvFIni ?>
	<?php if ($Prov->SortUrl($Prov->PrvFIni) == "") { ?>
		<th data-name="PrvFIni"><div id="elh_Prov_PrvFIni" class="Prov_PrvFIni"><div class="ewTableHeaderCaption"><?php echo $Prov->PrvFIni->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="PrvFIni"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $Prov->SortUrl($Prov->PrvFIni) ?>',2);"><div id="elh_Prov_PrvFIni" class="Prov_PrvFIni">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $Prov->PrvFIni->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($Prov->PrvFIni->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($Prov->PrvFIni->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($Prov->PrvFFin->Visible) { // PrvFFin ?>
	<?php if ($Prov->SortUrl($Prov->PrvFFin) == "") { ?>
		<th data-name="PrvFFin"><div id="elh_Prov_PrvFFin" class="Prov_PrvFFin"><div class="ewTableHeaderCaption"><?php echo $Prov->PrvFFin->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="PrvFFin"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $Prov->SortUrl($Prov->PrvFFin) ?>',2);"><div id="elh_Prov_PrvFFin" class="Prov_PrvFFin">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $Prov->PrvFFin->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($Prov->PrvFFin->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($Prov->PrvFFin->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($Prov->PrvPais->Visible) { // PrvPais ?>
	<?php if ($Prov->SortUrl($Prov->PrvPais) == "") { ?>
		<th data-name="PrvPais"><div id="elh_Prov_PrvPais" class="Prov_PrvPais"><div class="ewTableHeaderCaption"><?php echo $Prov->PrvPais->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="PrvPais"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $Prov->SortUrl($Prov->PrvPais) ?>',2);"><div id="elh_Prov_PrvPais" class="Prov_PrvPais">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $Prov->PrvPais->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($Prov->PrvPais->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($Prov->PrvPais->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($Prov->PrvTipo->Visible) { // PrvTipo ?>
	<?php if ($Prov->SortUrl($Prov->PrvTipo) == "") { ?>
		<th data-name="PrvTipo"><div id="elh_Prov_PrvTipo" class="Prov_PrvTipo"><div class="ewTableHeaderCaption"><?php echo $Prov->PrvTipo->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="PrvTipo"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $Prov->SortUrl($Prov->PrvTipo) ?>',2);"><div id="elh_Prov_PrvTipo" class="Prov_PrvTipo">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $Prov->PrvTipo->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($Prov->PrvTipo->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($Prov->PrvTipo->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($Prov->PrvNomb->Visible) { // PrvNomb ?>
	<?php if ($Prov->SortUrl($Prov->PrvNomb) == "") { ?>
		<th data-name="PrvNomb"><div id="elh_Prov_PrvNomb" class="Prov_PrvNomb"><div class="ewTableHeaderCaption"><?php echo $Prov->PrvNomb->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="PrvNomb"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $Prov->SortUrl($Prov->PrvNomb) ?>',2);"><div id="elh_Prov_PrvNomb" class="Prov_PrvNomb">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $Prov->PrvNomb->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($Prov->PrvNomb->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($Prov->PrvNomb->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($Prov->PrvApel->Visible) { // PrvApel ?>
	<?php if ($Prov->SortUrl($Prov->PrvApel) == "") { ?>
		<th data-name="PrvApel"><div id="elh_Prov_PrvApel" class="Prov_PrvApel"><div class="ewTableHeaderCaption"><?php echo $Prov->PrvApel->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="PrvApel"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $Prov->SortUrl($Prov->PrvApel) ?>',2);"><div id="elh_Prov_PrvApel" class="Prov_PrvApel">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $Prov->PrvApel->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($Prov->PrvApel->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($Prov->PrvApel->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($Prov->PrvCRuc->Visible) { // PrvCRuc ?>
	<?php if ($Prov->SortUrl($Prov->PrvCRuc) == "") { ?>
		<th data-name="PrvCRuc"><div id="elh_Prov_PrvCRuc" class="Prov_PrvCRuc"><div class="ewTableHeaderCaption"><?php echo $Prov->PrvCRuc->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="PrvCRuc"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $Prov->SortUrl($Prov->PrvCRuc) ?>',2);"><div id="elh_Prov_PrvCRuc" class="Prov_PrvCRuc">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $Prov->PrvCRuc->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($Prov->PrvCRuc->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($Prov->PrvCRuc->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($Prov->PrvDire->Visible) { // PrvDire ?>
	<?php if ($Prov->SortUrl($Prov->PrvDire) == "") { ?>
		<th data-name="PrvDire"><div id="elh_Prov_PrvDire" class="Prov_PrvDire"><div class="ewTableHeaderCaption"><?php echo $Prov->PrvDire->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="PrvDire"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $Prov->SortUrl($Prov->PrvDire) ?>',2);"><div id="elh_Prov_PrvDire" class="Prov_PrvDire">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $Prov->PrvDire->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($Prov->PrvDire->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($Prov->PrvDire->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($Prov->PrvMail->Visible) { // PrvMail ?>
	<?php if ($Prov->SortUrl($Prov->PrvMail) == "") { ?>
		<th data-name="PrvMail"><div id="elh_Prov_PrvMail" class="Prov_PrvMail"><div class="ewTableHeaderCaption"><?php echo $Prov->PrvMail->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="PrvMail"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $Prov->SortUrl($Prov->PrvMail) ?>',2);"><div id="elh_Prov_PrvMail" class="Prov_PrvMail">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $Prov->PrvMail->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($Prov->PrvMail->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($Prov->PrvMail->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($Prov->PrvTele->Visible) { // PrvTele ?>
	<?php if ($Prov->SortUrl($Prov->PrvTele) == "") { ?>
		<th data-name="PrvTele"><div id="elh_Prov_PrvTele" class="Prov_PrvTele"><div class="ewTableHeaderCaption"><?php echo $Prov->PrvTele->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="PrvTele"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $Prov->SortUrl($Prov->PrvTele) ?>',2);"><div id="elh_Prov_PrvTele" class="Prov_PrvTele">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $Prov->PrvTele->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($Prov->PrvTele->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($Prov->PrvTele->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($Prov->PrvRSoc->Visible) { // PrvRSoc ?>
	<?php if ($Prov->SortUrl($Prov->PrvRSoc) == "") { ?>
		<th data-name="PrvRSoc"><div id="elh_Prov_PrvRSoc" class="Prov_PrvRSoc"><div class="ewTableHeaderCaption"><?php echo $Prov->PrvRSoc->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="PrvRSoc"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $Prov->SortUrl($Prov->PrvRSoc) ?>',2);"><div id="elh_Prov_PrvRSoc" class="Prov_PrvRSoc">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $Prov->PrvRSoc->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($Prov->PrvRSoc->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($Prov->PrvRSoc->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php

// Render list options (header, right)
$Prov_list->ListOptions->Render("header", "right");
?>
	</tr>
</thead>
<tbody>
<?php
if ($Prov->ExportAll && $Prov->Export <> "") {
	$Prov_list->StopRec = $Prov_list->TotalRecs;
} else {

	// Set the last record to display
	if ($Prov_list->TotalRecs > $Prov_list->StartRec + $Prov_list->DisplayRecs - 1)
		$Prov_list->StopRec = $Prov_list->StartRec + $Prov_list->DisplayRecs - 1;
	else
		$Prov_list->StopRec = $Prov_list->TotalRecs;
}

// Restore number of post back records
if ($objForm) {
	$objForm->Index = -1;
	if ($objForm->HasValue($Prov_list->FormKeyCountName) && ($Prov->CurrentAction == "gridadd" || $Prov->CurrentAction == "gridedit" || $Prov->CurrentAction == "F")) {
		$Prov_list->KeyCount = $objForm->GetValue($Prov_list->FormKeyCountName);
		$Prov_list->StopRec = $Prov_list->StartRec + $Prov_list->KeyCount - 1;
	}
}
$Prov_list->RecCnt = $Prov_list->StartRec - 1;
if ($Prov_list->Recordset && !$Prov_list->Recordset->EOF) {
	$Prov_list->Recordset->MoveFirst();
	$bSelectLimit = $Prov_list->UseSelectLimit;
	if (!$bSelectLimit && $Prov_list->StartRec > 1)
		$Prov_list->Recordset->Move($Prov_list->StartRec - 1);
} elseif (!$Prov->AllowAddDeleteRow && $Prov_list->StopRec == 0) {
	$Prov_list->StopRec = $Prov->GridAddRowCount;
}

// Initialize aggregate
$Prov->RowType = EW_ROWTYPE_AGGREGATEINIT;
$Prov->ResetAttrs();
$Prov_list->RenderRow();
if ($Prov->CurrentAction == "gridadd")
	$Prov_list->RowIndex = 0;
if ($Prov->CurrentAction == "gridedit")
	$Prov_list->RowIndex = 0;
while ($Prov_list->RecCnt < $Prov_list->StopRec) {
	$Prov_list->RecCnt++;
	if (intval($Prov_list->RecCnt) >= intval($Prov_list->StartRec)) {
		$Prov_list->RowCnt++;
		if ($Prov->CurrentAction == "gridadd" || $Prov->CurrentAction == "gridedit" || $Prov->CurrentAction == "F") {
			$Prov_list->RowIndex++;
			$objForm->Index = $Prov_list->RowIndex;
			if ($objForm->HasValue($Prov_list->FormActionName))
				$Prov_list->RowAction = strval($objForm->GetValue($Prov_list->FormActionName));
			elseif ($Prov->CurrentAction == "gridadd")
				$Prov_list->RowAction = "insert";
			else
				$Prov_list->RowAction = "";
		}

		// Set up key count
		$Prov_list->KeyCount = $Prov_list->RowIndex;

		// Init row class and style
		$Prov->ResetAttrs();
		$Prov->CssClass = "";
		if ($Prov->CurrentAction == "gridadd") {
			$Prov_list->LoadDefaultValues(); // Load default values
		} else {
			$Prov_list->LoadRowValues($Prov_list->Recordset); // Load row values
		}
		$Prov->RowType = EW_ROWTYPE_VIEW; // Render view
		if ($Prov->CurrentAction == "gridadd") // Grid add
			$Prov->RowType = EW_ROWTYPE_ADD; // Render add
		if ($Prov->CurrentAction == "gridadd" && $Prov->EventCancelled && !$objForm->HasValue("k_blankrow")) // Insert failed
			$Prov_list->RestoreCurrentRowFormValues($Prov_list->RowIndex); // Restore form values
		if ($Prov->CurrentAction == "gridedit") { // Grid edit
			if ($Prov->EventCancelled) {
				$Prov_list->RestoreCurrentRowFormValues($Prov_list->RowIndex); // Restore form values
			}
			if ($Prov_list->RowAction == "insert")
				$Prov->RowType = EW_ROWTYPE_ADD; // Render add
			else
				$Prov->RowType = EW_ROWTYPE_EDIT; // Render edit
		}
		if ($Prov->CurrentAction == "gridedit" && ($Prov->RowType == EW_ROWTYPE_EDIT || $Prov->RowType == EW_ROWTYPE_ADD) && $Prov->EventCancelled) // Update failed
			$Prov_list->RestoreCurrentRowFormValues($Prov_list->RowIndex); // Restore form values
		if ($Prov->RowType == EW_ROWTYPE_EDIT) // Edit row
			$Prov_list->EditRowCnt++;

		// Set up row id / data-rowindex
		$Prov->RowAttrs = array_merge($Prov->RowAttrs, array('data-rowindex'=>$Prov_list->RowCnt, 'id'=>'r' . $Prov_list->RowCnt . '_Prov', 'data-rowtype'=>$Prov->RowType));

		// Render row
		$Prov_list->RenderRow();

		// Render list options
		$Prov_list->RenderListOptions();

		// Skip delete row / empty row for confirm page
		if ($Prov_list->RowAction <> "delete" && $Prov_list->RowAction <> "insertdelete" && !($Prov_list->RowAction == "insert" && $Prov->CurrentAction == "F" && $Prov_list->EmptyRow())) {
?>
	<tr<?php echo $Prov->RowAttributes() ?>>
<?php

// Render list options (body, left)
$Prov_list->ListOptions->Render("body", "left", $Prov_list->RowCnt);
?>
	<?php if ($Prov->PrvFIni->Visible) { // PrvFIni ?>
		<td data-name="PrvFIni"<?php echo $Prov->PrvFIni->CellAttributes() ?>>
<?php if ($Prov->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $Prov_list->RowCnt ?>_Prov_PrvFIni" class="form-group Prov_PrvFIni">
<input type="text" data-table="Prov" data-field="x_PrvFIni" data-format="7" name="x<?php echo $Prov_list->RowIndex ?>_PrvFIni" id="x<?php echo $Prov_list->RowIndex ?>_PrvFIni" placeholder="<?php echo ew_HtmlEncode($Prov->PrvFIni->getPlaceHolder()) ?>" value="<?php echo $Prov->PrvFIni->EditValue ?>"<?php echo $Prov->PrvFIni->EditAttributes() ?>>
</span>
<input type="hidden" data-table="Prov" data-field="x_PrvFIni" name="o<?php echo $Prov_list->RowIndex ?>_PrvFIni" id="o<?php echo $Prov_list->RowIndex ?>_PrvFIni" value="<?php echo ew_HtmlEncode($Prov->PrvFIni->OldValue) ?>">
<?php } ?>
<?php if ($Prov->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $Prov_list->RowCnt ?>_Prov_PrvFIni" class="form-group Prov_PrvFIni">
<input type="text" data-table="Prov" data-field="x_PrvFIni" data-format="7" name="x<?php echo $Prov_list->RowIndex ?>_PrvFIni" id="x<?php echo $Prov_list->RowIndex ?>_PrvFIni" placeholder="<?php echo ew_HtmlEncode($Prov->PrvFIni->getPlaceHolder()) ?>" value="<?php echo $Prov->PrvFIni->EditValue ?>"<?php echo $Prov->PrvFIni->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($Prov->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $Prov_list->RowCnt ?>_Prov_PrvFIni" class="Prov_PrvFIni">
<span<?php echo $Prov->PrvFIni->ViewAttributes() ?>>
<?php echo $Prov->PrvFIni->ListViewValue() ?></span>
</span>
<?php } ?>
<a id="<?php echo $Prov_list->PageObjName . "_row_" . $Prov_list->RowCnt ?>"></a></td>
	<?php } ?>
<?php if ($Prov->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<input type="hidden" data-table="Prov" data-field="x_PrvCodi" name="x<?php echo $Prov_list->RowIndex ?>_PrvCodi" id="x<?php echo $Prov_list->RowIndex ?>_PrvCodi" value="<?php echo ew_HtmlEncode($Prov->PrvCodi->CurrentValue) ?>">
<input type="hidden" data-table="Prov" data-field="x_PrvCodi" name="o<?php echo $Prov_list->RowIndex ?>_PrvCodi" id="o<?php echo $Prov_list->RowIndex ?>_PrvCodi" value="<?php echo ew_HtmlEncode($Prov->PrvCodi->OldValue) ?>">
<?php } ?>
<?php if ($Prov->RowType == EW_ROWTYPE_EDIT || $Prov->CurrentMode == "edit") { ?>
<input type="hidden" data-table="Prov" data-field="x_PrvCodi" name="x<?php echo $Prov_list->RowIndex ?>_PrvCodi" id="x<?php echo $Prov_list->RowIndex ?>_PrvCodi" value="<?php echo ew_HtmlEncode($Prov->PrvCodi->CurrentValue) ?>">
<?php } ?>
	<?php if ($Prov->PrvFFin->Visible) { // PrvFFin ?>
		<td data-name="PrvFFin"<?php echo $Prov->PrvFFin->CellAttributes() ?>>
<?php if ($Prov->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $Prov_list->RowCnt ?>_Prov_PrvFFin" class="form-group Prov_PrvFFin">
<input type="text" data-table="Prov" data-field="x_PrvFFin" data-format="7" name="x<?php echo $Prov_list->RowIndex ?>_PrvFFin" id="x<?php echo $Prov_list->RowIndex ?>_PrvFFin" placeholder="<?php echo ew_HtmlEncode($Prov->PrvFFin->getPlaceHolder()) ?>" value="<?php echo $Prov->PrvFFin->EditValue ?>"<?php echo $Prov->PrvFFin->EditAttributes() ?>>
</span>
<input type="hidden" data-table="Prov" data-field="x_PrvFFin" name="o<?php echo $Prov_list->RowIndex ?>_PrvFFin" id="o<?php echo $Prov_list->RowIndex ?>_PrvFFin" value="<?php echo ew_HtmlEncode($Prov->PrvFFin->OldValue) ?>">
<?php } ?>
<?php if ($Prov->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $Prov_list->RowCnt ?>_Prov_PrvFFin" class="form-group Prov_PrvFFin">
<input type="text" data-table="Prov" data-field="x_PrvFFin" data-format="7" name="x<?php echo $Prov_list->RowIndex ?>_PrvFFin" id="x<?php echo $Prov_list->RowIndex ?>_PrvFFin" placeholder="<?php echo ew_HtmlEncode($Prov->PrvFFin->getPlaceHolder()) ?>" value="<?php echo $Prov->PrvFFin->EditValue ?>"<?php echo $Prov->PrvFFin->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($Prov->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $Prov_list->RowCnt ?>_Prov_PrvFFin" class="Prov_PrvFFin">
<span<?php echo $Prov->PrvFFin->ViewAttributes() ?>>
<?php echo $Prov->PrvFFin->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($Prov->PrvPais->Visible) { // PrvPais ?>
		<td data-name="PrvPais"<?php echo $Prov->PrvPais->CellAttributes() ?>>
<?php if ($Prov->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $Prov_list->RowCnt ?>_Prov_PrvPais" class="form-group Prov_PrvPais">
<input type="text" data-table="Prov" data-field="x_PrvPais" name="x<?php echo $Prov_list->RowIndex ?>_PrvPais" id="x<?php echo $Prov_list->RowIndex ?>_PrvPais" size="30" placeholder="<?php echo ew_HtmlEncode($Prov->PrvPais->getPlaceHolder()) ?>" value="<?php echo $Prov->PrvPais->EditValue ?>"<?php echo $Prov->PrvPais->EditAttributes() ?>>
</span>
<input type="hidden" data-table="Prov" data-field="x_PrvPais" name="o<?php echo $Prov_list->RowIndex ?>_PrvPais" id="o<?php echo $Prov_list->RowIndex ?>_PrvPais" value="<?php echo ew_HtmlEncode($Prov->PrvPais->OldValue) ?>">
<?php } ?>
<?php if ($Prov->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $Prov_list->RowCnt ?>_Prov_PrvPais" class="form-group Prov_PrvPais">
<input type="text" data-table="Prov" data-field="x_PrvPais" name="x<?php echo $Prov_list->RowIndex ?>_PrvPais" id="x<?php echo $Prov_list->RowIndex ?>_PrvPais" size="30" placeholder="<?php echo ew_HtmlEncode($Prov->PrvPais->getPlaceHolder()) ?>" value="<?php echo $Prov->PrvPais->EditValue ?>"<?php echo $Prov->PrvPais->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($Prov->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $Prov_list->RowCnt ?>_Prov_PrvPais" class="Prov_PrvPais">
<span<?php echo $Prov->PrvPais->ViewAttributes() ?>>
<?php echo $Prov->PrvPais->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($Prov->PrvTipo->Visible) { // PrvTipo ?>
		<td data-name="PrvTipo"<?php echo $Prov->PrvTipo->CellAttributes() ?>>
<?php if ($Prov->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $Prov_list->RowCnt ?>_Prov_PrvTipo" class="form-group Prov_PrvTipo">
<input type="text" data-table="Prov" data-field="x_PrvTipo" name="x<?php echo $Prov_list->RowIndex ?>_PrvTipo" id="x<?php echo $Prov_list->RowIndex ?>_PrvTipo" size="30" maxlength="1" placeholder="<?php echo ew_HtmlEncode($Prov->PrvTipo->getPlaceHolder()) ?>" value="<?php echo $Prov->PrvTipo->EditValue ?>"<?php echo $Prov->PrvTipo->EditAttributes() ?>>
</span>
<input type="hidden" data-table="Prov" data-field="x_PrvTipo" name="o<?php echo $Prov_list->RowIndex ?>_PrvTipo" id="o<?php echo $Prov_list->RowIndex ?>_PrvTipo" value="<?php echo ew_HtmlEncode($Prov->PrvTipo->OldValue) ?>">
<?php } ?>
<?php if ($Prov->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $Prov_list->RowCnt ?>_Prov_PrvTipo" class="form-group Prov_PrvTipo">
<input type="text" data-table="Prov" data-field="x_PrvTipo" name="x<?php echo $Prov_list->RowIndex ?>_PrvTipo" id="x<?php echo $Prov_list->RowIndex ?>_PrvTipo" size="30" maxlength="1" placeholder="<?php echo ew_HtmlEncode($Prov->PrvTipo->getPlaceHolder()) ?>" value="<?php echo $Prov->PrvTipo->EditValue ?>"<?php echo $Prov->PrvTipo->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($Prov->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $Prov_list->RowCnt ?>_Prov_PrvTipo" class="Prov_PrvTipo">
<span<?php echo $Prov->PrvTipo->ViewAttributes() ?>>
<?php echo $Prov->PrvTipo->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($Prov->PrvNomb->Visible) { // PrvNomb ?>
		<td data-name="PrvNomb"<?php echo $Prov->PrvNomb->CellAttributes() ?>>
<?php if ($Prov->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $Prov_list->RowCnt ?>_Prov_PrvNomb" class="form-group Prov_PrvNomb">
<input type="text" data-table="Prov" data-field="x_PrvNomb" name="x<?php echo $Prov_list->RowIndex ?>_PrvNomb" id="x<?php echo $Prov_list->RowIndex ?>_PrvNomb" size="30" placeholder="<?php echo ew_HtmlEncode($Prov->PrvNomb->getPlaceHolder()) ?>" value="<?php echo $Prov->PrvNomb->EditValue ?>"<?php echo $Prov->PrvNomb->EditAttributes() ?>>
</span>
<input type="hidden" data-table="Prov" data-field="x_PrvNomb" name="o<?php echo $Prov_list->RowIndex ?>_PrvNomb" id="o<?php echo $Prov_list->RowIndex ?>_PrvNomb" value="<?php echo ew_HtmlEncode($Prov->PrvNomb->OldValue) ?>">
<?php } ?>
<?php if ($Prov->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $Prov_list->RowCnt ?>_Prov_PrvNomb" class="form-group Prov_PrvNomb">
<input type="text" data-table="Prov" data-field="x_PrvNomb" name="x<?php echo $Prov_list->RowIndex ?>_PrvNomb" id="x<?php echo $Prov_list->RowIndex ?>_PrvNomb" size="30" placeholder="<?php echo ew_HtmlEncode($Prov->PrvNomb->getPlaceHolder()) ?>" value="<?php echo $Prov->PrvNomb->EditValue ?>"<?php echo $Prov->PrvNomb->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($Prov->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $Prov_list->RowCnt ?>_Prov_PrvNomb" class="Prov_PrvNomb">
<span<?php echo $Prov->PrvNomb->ViewAttributes() ?>>
<?php echo $Prov->PrvNomb->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($Prov->PrvApel->Visible) { // PrvApel ?>
		<td data-name="PrvApel"<?php echo $Prov->PrvApel->CellAttributes() ?>>
<?php if ($Prov->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $Prov_list->RowCnt ?>_Prov_PrvApel" class="form-group Prov_PrvApel">
<input type="text" data-table="Prov" data-field="x_PrvApel" name="x<?php echo $Prov_list->RowIndex ?>_PrvApel" id="x<?php echo $Prov_list->RowIndex ?>_PrvApel" size="30" placeholder="<?php echo ew_HtmlEncode($Prov->PrvApel->getPlaceHolder()) ?>" value="<?php echo $Prov->PrvApel->EditValue ?>"<?php echo $Prov->PrvApel->EditAttributes() ?>>
</span>
<input type="hidden" data-table="Prov" data-field="x_PrvApel" name="o<?php echo $Prov_list->RowIndex ?>_PrvApel" id="o<?php echo $Prov_list->RowIndex ?>_PrvApel" value="<?php echo ew_HtmlEncode($Prov->PrvApel->OldValue) ?>">
<?php } ?>
<?php if ($Prov->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $Prov_list->RowCnt ?>_Prov_PrvApel" class="form-group Prov_PrvApel">
<input type="text" data-table="Prov" data-field="x_PrvApel" name="x<?php echo $Prov_list->RowIndex ?>_PrvApel" id="x<?php echo $Prov_list->RowIndex ?>_PrvApel" size="30" placeholder="<?php echo ew_HtmlEncode($Prov->PrvApel->getPlaceHolder()) ?>" value="<?php echo $Prov->PrvApel->EditValue ?>"<?php echo $Prov->PrvApel->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($Prov->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $Prov_list->RowCnt ?>_Prov_PrvApel" class="Prov_PrvApel">
<span<?php echo $Prov->PrvApel->ViewAttributes() ?>>
<?php echo $Prov->PrvApel->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($Prov->PrvCRuc->Visible) { // PrvCRuc ?>
		<td data-name="PrvCRuc"<?php echo $Prov->PrvCRuc->CellAttributes() ?>>
<?php if ($Prov->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $Prov_list->RowCnt ?>_Prov_PrvCRuc" class="form-group Prov_PrvCRuc">
<input type="text" data-table="Prov" data-field="x_PrvCRuc" name="x<?php echo $Prov_list->RowIndex ?>_PrvCRuc" id="x<?php echo $Prov_list->RowIndex ?>_PrvCRuc" size="30" placeholder="<?php echo ew_HtmlEncode($Prov->PrvCRuc->getPlaceHolder()) ?>" value="<?php echo $Prov->PrvCRuc->EditValue ?>"<?php echo $Prov->PrvCRuc->EditAttributes() ?>>
</span>
<input type="hidden" data-table="Prov" data-field="x_PrvCRuc" name="o<?php echo $Prov_list->RowIndex ?>_PrvCRuc" id="o<?php echo $Prov_list->RowIndex ?>_PrvCRuc" value="<?php echo ew_HtmlEncode($Prov->PrvCRuc->OldValue) ?>">
<?php } ?>
<?php if ($Prov->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $Prov_list->RowCnt ?>_Prov_PrvCRuc" class="form-group Prov_PrvCRuc">
<input type="text" data-table="Prov" data-field="x_PrvCRuc" name="x<?php echo $Prov_list->RowIndex ?>_PrvCRuc" id="x<?php echo $Prov_list->RowIndex ?>_PrvCRuc" size="30" placeholder="<?php echo ew_HtmlEncode($Prov->PrvCRuc->getPlaceHolder()) ?>" value="<?php echo $Prov->PrvCRuc->EditValue ?>"<?php echo $Prov->PrvCRuc->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($Prov->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $Prov_list->RowCnt ?>_Prov_PrvCRuc" class="Prov_PrvCRuc">
<span<?php echo $Prov->PrvCRuc->ViewAttributes() ?>>
<?php echo $Prov->PrvCRuc->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($Prov->PrvDire->Visible) { // PrvDire ?>
		<td data-name="PrvDire"<?php echo $Prov->PrvDire->CellAttributes() ?>>
<?php if ($Prov->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $Prov_list->RowCnt ?>_Prov_PrvDire" class="form-group Prov_PrvDire">
<input type="text" data-table="Prov" data-field="x_PrvDire" name="x<?php echo $Prov_list->RowIndex ?>_PrvDire" id="x<?php echo $Prov_list->RowIndex ?>_PrvDire" size="30" placeholder="<?php echo ew_HtmlEncode($Prov->PrvDire->getPlaceHolder()) ?>" value="<?php echo $Prov->PrvDire->EditValue ?>"<?php echo $Prov->PrvDire->EditAttributes() ?>>
</span>
<input type="hidden" data-table="Prov" data-field="x_PrvDire" name="o<?php echo $Prov_list->RowIndex ?>_PrvDire" id="o<?php echo $Prov_list->RowIndex ?>_PrvDire" value="<?php echo ew_HtmlEncode($Prov->PrvDire->OldValue) ?>">
<?php } ?>
<?php if ($Prov->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $Prov_list->RowCnt ?>_Prov_PrvDire" class="form-group Prov_PrvDire">
<input type="text" data-table="Prov" data-field="x_PrvDire" name="x<?php echo $Prov_list->RowIndex ?>_PrvDire" id="x<?php echo $Prov_list->RowIndex ?>_PrvDire" size="30" placeholder="<?php echo ew_HtmlEncode($Prov->PrvDire->getPlaceHolder()) ?>" value="<?php echo $Prov->PrvDire->EditValue ?>"<?php echo $Prov->PrvDire->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($Prov->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $Prov_list->RowCnt ?>_Prov_PrvDire" class="Prov_PrvDire">
<span<?php echo $Prov->PrvDire->ViewAttributes() ?>>
<?php echo $Prov->PrvDire->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($Prov->PrvMail->Visible) { // PrvMail ?>
		<td data-name="PrvMail"<?php echo $Prov->PrvMail->CellAttributes() ?>>
<?php if ($Prov->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $Prov_list->RowCnt ?>_Prov_PrvMail" class="form-group Prov_PrvMail">
<input type="text" data-table="Prov" data-field="x_PrvMail" name="x<?php echo $Prov_list->RowIndex ?>_PrvMail" id="x<?php echo $Prov_list->RowIndex ?>_PrvMail" size="30" maxlength="50" placeholder="<?php echo ew_HtmlEncode($Prov->PrvMail->getPlaceHolder()) ?>" value="<?php echo $Prov->PrvMail->EditValue ?>"<?php echo $Prov->PrvMail->EditAttributes() ?>>
</span>
<input type="hidden" data-table="Prov" data-field="x_PrvMail" name="o<?php echo $Prov_list->RowIndex ?>_PrvMail" id="o<?php echo $Prov_list->RowIndex ?>_PrvMail" value="<?php echo ew_HtmlEncode($Prov->PrvMail->OldValue) ?>">
<?php } ?>
<?php if ($Prov->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $Prov_list->RowCnt ?>_Prov_PrvMail" class="form-group Prov_PrvMail">
<input type="text" data-table="Prov" data-field="x_PrvMail" name="x<?php echo $Prov_list->RowIndex ?>_PrvMail" id="x<?php echo $Prov_list->RowIndex ?>_PrvMail" size="30" maxlength="50" placeholder="<?php echo ew_HtmlEncode($Prov->PrvMail->getPlaceHolder()) ?>" value="<?php echo $Prov->PrvMail->EditValue ?>"<?php echo $Prov->PrvMail->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($Prov->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $Prov_list->RowCnt ?>_Prov_PrvMail" class="Prov_PrvMail">
<span<?php echo $Prov->PrvMail->ViewAttributes() ?>>
<?php echo $Prov->PrvMail->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($Prov->PrvTele->Visible) { // PrvTele ?>
		<td data-name="PrvTele"<?php echo $Prov->PrvTele->CellAttributes() ?>>
<?php if ($Prov->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $Prov_list->RowCnt ?>_Prov_PrvTele" class="form-group Prov_PrvTele">
<input type="text" data-table="Prov" data-field="x_PrvTele" name="x<?php echo $Prov_list->RowIndex ?>_PrvTele" id="x<?php echo $Prov_list->RowIndex ?>_PrvTele" size="30" placeholder="<?php echo ew_HtmlEncode($Prov->PrvTele->getPlaceHolder()) ?>" value="<?php echo $Prov->PrvTele->EditValue ?>"<?php echo $Prov->PrvTele->EditAttributes() ?>>
</span>
<input type="hidden" data-table="Prov" data-field="x_PrvTele" name="o<?php echo $Prov_list->RowIndex ?>_PrvTele" id="o<?php echo $Prov_list->RowIndex ?>_PrvTele" value="<?php echo ew_HtmlEncode($Prov->PrvTele->OldValue) ?>">
<?php } ?>
<?php if ($Prov->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $Prov_list->RowCnt ?>_Prov_PrvTele" class="form-group Prov_PrvTele">
<input type="text" data-table="Prov" data-field="x_PrvTele" name="x<?php echo $Prov_list->RowIndex ?>_PrvTele" id="x<?php echo $Prov_list->RowIndex ?>_PrvTele" size="30" placeholder="<?php echo ew_HtmlEncode($Prov->PrvTele->getPlaceHolder()) ?>" value="<?php echo $Prov->PrvTele->EditValue ?>"<?php echo $Prov->PrvTele->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($Prov->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $Prov_list->RowCnt ?>_Prov_PrvTele" class="Prov_PrvTele">
<span<?php echo $Prov->PrvTele->ViewAttributes() ?>>
<?php echo $Prov->PrvTele->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($Prov->PrvRSoc->Visible) { // PrvRSoc ?>
		<td data-name="PrvRSoc"<?php echo $Prov->PrvRSoc->CellAttributes() ?>>
<?php if ($Prov->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $Prov_list->RowCnt ?>_Prov_PrvRSoc" class="form-group Prov_PrvRSoc">
<input type="text" data-table="Prov" data-field="x_PrvRSoc" name="x<?php echo $Prov_list->RowIndex ?>_PrvRSoc" id="x<?php echo $Prov_list->RowIndex ?>_PrvRSoc" size="30" placeholder="<?php echo ew_HtmlEncode($Prov->PrvRSoc->getPlaceHolder()) ?>" value="<?php echo $Prov->PrvRSoc->EditValue ?>"<?php echo $Prov->PrvRSoc->EditAttributes() ?>>
</span>
<input type="hidden" data-table="Prov" data-field="x_PrvRSoc" name="o<?php echo $Prov_list->RowIndex ?>_PrvRSoc" id="o<?php echo $Prov_list->RowIndex ?>_PrvRSoc" value="<?php echo ew_HtmlEncode($Prov->PrvRSoc->OldValue) ?>">
<?php } ?>
<?php if ($Prov->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $Prov_list->RowCnt ?>_Prov_PrvRSoc" class="form-group Prov_PrvRSoc">
<input type="text" data-table="Prov" data-field="x_PrvRSoc" name="x<?php echo $Prov_list->RowIndex ?>_PrvRSoc" id="x<?php echo $Prov_list->RowIndex ?>_PrvRSoc" size="30" placeholder="<?php echo ew_HtmlEncode($Prov->PrvRSoc->getPlaceHolder()) ?>" value="<?php echo $Prov->PrvRSoc->EditValue ?>"<?php echo $Prov->PrvRSoc->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($Prov->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $Prov_list->RowCnt ?>_Prov_PrvRSoc" class="Prov_PrvRSoc">
<span<?php echo $Prov->PrvRSoc->ViewAttributes() ?>>
<?php echo $Prov->PrvRSoc->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
<?php

// Render list options (body, right)
$Prov_list->ListOptions->Render("body", "right", $Prov_list->RowCnt);
?>
	</tr>
<?php if ($Prov->RowType == EW_ROWTYPE_ADD || $Prov->RowType == EW_ROWTYPE_EDIT) { ?>
<script type="text/javascript">
fProvlist.UpdateOpts(<?php echo $Prov_list->RowIndex ?>);
</script>
<?php } ?>
<?php
	}
	} // End delete row checking
	if ($Prov->CurrentAction <> "gridadd")
		if (!$Prov_list->Recordset->EOF) $Prov_list->Recordset->MoveNext();
}
?>
<?php
	if ($Prov->CurrentAction == "gridadd" || $Prov->CurrentAction == "gridedit") {
		$Prov_list->RowIndex = '$rowindex$';
		$Prov_list->LoadDefaultValues();

		// Set row properties
		$Prov->ResetAttrs();
		$Prov->RowAttrs = array_merge($Prov->RowAttrs, array('data-rowindex'=>$Prov_list->RowIndex, 'id'=>'r0_Prov', 'data-rowtype'=>EW_ROWTYPE_ADD));
		ew_AppendClass($Prov->RowAttrs["class"], "ewTemplate");
		$Prov->RowType = EW_ROWTYPE_ADD;

		// Render row
		$Prov_list->RenderRow();

		// Render list options
		$Prov_list->RenderListOptions();
		$Prov_list->StartRowCnt = 0;
?>
	<tr<?php echo $Prov->RowAttributes() ?>>
<?php

// Render list options (body, left)
$Prov_list->ListOptions->Render("body", "left", $Prov_list->RowIndex);
?>
	<?php if ($Prov->PrvFIni->Visible) { // PrvFIni ?>
		<td data-name="PrvFIni">
<span id="el$rowindex$_Prov_PrvFIni" class="form-group Prov_PrvFIni">
<input type="text" data-table="Prov" data-field="x_PrvFIni" data-format="7" name="x<?php echo $Prov_list->RowIndex ?>_PrvFIni" id="x<?php echo $Prov_list->RowIndex ?>_PrvFIni" placeholder="<?php echo ew_HtmlEncode($Prov->PrvFIni->getPlaceHolder()) ?>" value="<?php echo $Prov->PrvFIni->EditValue ?>"<?php echo $Prov->PrvFIni->EditAttributes() ?>>
</span>
<input type="hidden" data-table="Prov" data-field="x_PrvFIni" name="o<?php echo $Prov_list->RowIndex ?>_PrvFIni" id="o<?php echo $Prov_list->RowIndex ?>_PrvFIni" value="<?php echo ew_HtmlEncode($Prov->PrvFIni->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($Prov->PrvFFin->Visible) { // PrvFFin ?>
		<td data-name="PrvFFin">
<span id="el$rowindex$_Prov_PrvFFin" class="form-group Prov_PrvFFin">
<input type="text" data-table="Prov" data-field="x_PrvFFin" data-format="7" name="x<?php echo $Prov_list->RowIndex ?>_PrvFFin" id="x<?php echo $Prov_list->RowIndex ?>_PrvFFin" placeholder="<?php echo ew_HtmlEncode($Prov->PrvFFin->getPlaceHolder()) ?>" value="<?php echo $Prov->PrvFFin->EditValue ?>"<?php echo $Prov->PrvFFin->EditAttributes() ?>>
</span>
<input type="hidden" data-table="Prov" data-field="x_PrvFFin" name="o<?php echo $Prov_list->RowIndex ?>_PrvFFin" id="o<?php echo $Prov_list->RowIndex ?>_PrvFFin" value="<?php echo ew_HtmlEncode($Prov->PrvFFin->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($Prov->PrvPais->Visible) { // PrvPais ?>
		<td data-name="PrvPais">
<span id="el$rowindex$_Prov_PrvPais" class="form-group Prov_PrvPais">
<input type="text" data-table="Prov" data-field="x_PrvPais" name="x<?php echo $Prov_list->RowIndex ?>_PrvPais" id="x<?php echo $Prov_list->RowIndex ?>_PrvPais" size="30" placeholder="<?php echo ew_HtmlEncode($Prov->PrvPais->getPlaceHolder()) ?>" value="<?php echo $Prov->PrvPais->EditValue ?>"<?php echo $Prov->PrvPais->EditAttributes() ?>>
</span>
<input type="hidden" data-table="Prov" data-field="x_PrvPais" name="o<?php echo $Prov_list->RowIndex ?>_PrvPais" id="o<?php echo $Prov_list->RowIndex ?>_PrvPais" value="<?php echo ew_HtmlEncode($Prov->PrvPais->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($Prov->PrvTipo->Visible) { // PrvTipo ?>
		<td data-name="PrvTipo">
<span id="el$rowindex$_Prov_PrvTipo" class="form-group Prov_PrvTipo">
<input type="text" data-table="Prov" data-field="x_PrvTipo" name="x<?php echo $Prov_list->RowIndex ?>_PrvTipo" id="x<?php echo $Prov_list->RowIndex ?>_PrvTipo" size="30" maxlength="1" placeholder="<?php echo ew_HtmlEncode($Prov->PrvTipo->getPlaceHolder()) ?>" value="<?php echo $Prov->PrvTipo->EditValue ?>"<?php echo $Prov->PrvTipo->EditAttributes() ?>>
</span>
<input type="hidden" data-table="Prov" data-field="x_PrvTipo" name="o<?php echo $Prov_list->RowIndex ?>_PrvTipo" id="o<?php echo $Prov_list->RowIndex ?>_PrvTipo" value="<?php echo ew_HtmlEncode($Prov->PrvTipo->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($Prov->PrvNomb->Visible) { // PrvNomb ?>
		<td data-name="PrvNomb">
<span id="el$rowindex$_Prov_PrvNomb" class="form-group Prov_PrvNomb">
<input type="text" data-table="Prov" data-field="x_PrvNomb" name="x<?php echo $Prov_list->RowIndex ?>_PrvNomb" id="x<?php echo $Prov_list->RowIndex ?>_PrvNomb" size="30" placeholder="<?php echo ew_HtmlEncode($Prov->PrvNomb->getPlaceHolder()) ?>" value="<?php echo $Prov->PrvNomb->EditValue ?>"<?php echo $Prov->PrvNomb->EditAttributes() ?>>
</span>
<input type="hidden" data-table="Prov" data-field="x_PrvNomb" name="o<?php echo $Prov_list->RowIndex ?>_PrvNomb" id="o<?php echo $Prov_list->RowIndex ?>_PrvNomb" value="<?php echo ew_HtmlEncode($Prov->PrvNomb->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($Prov->PrvApel->Visible) { // PrvApel ?>
		<td data-name="PrvApel">
<span id="el$rowindex$_Prov_PrvApel" class="form-group Prov_PrvApel">
<input type="text" data-table="Prov" data-field="x_PrvApel" name="x<?php echo $Prov_list->RowIndex ?>_PrvApel" id="x<?php echo $Prov_list->RowIndex ?>_PrvApel" size="30" placeholder="<?php echo ew_HtmlEncode($Prov->PrvApel->getPlaceHolder()) ?>" value="<?php echo $Prov->PrvApel->EditValue ?>"<?php echo $Prov->PrvApel->EditAttributes() ?>>
</span>
<input type="hidden" data-table="Prov" data-field="x_PrvApel" name="o<?php echo $Prov_list->RowIndex ?>_PrvApel" id="o<?php echo $Prov_list->RowIndex ?>_PrvApel" value="<?php echo ew_HtmlEncode($Prov->PrvApel->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($Prov->PrvCRuc->Visible) { // PrvCRuc ?>
		<td data-name="PrvCRuc">
<span id="el$rowindex$_Prov_PrvCRuc" class="form-group Prov_PrvCRuc">
<input type="text" data-table="Prov" data-field="x_PrvCRuc" name="x<?php echo $Prov_list->RowIndex ?>_PrvCRuc" id="x<?php echo $Prov_list->RowIndex ?>_PrvCRuc" size="30" placeholder="<?php echo ew_HtmlEncode($Prov->PrvCRuc->getPlaceHolder()) ?>" value="<?php echo $Prov->PrvCRuc->EditValue ?>"<?php echo $Prov->PrvCRuc->EditAttributes() ?>>
</span>
<input type="hidden" data-table="Prov" data-field="x_PrvCRuc" name="o<?php echo $Prov_list->RowIndex ?>_PrvCRuc" id="o<?php echo $Prov_list->RowIndex ?>_PrvCRuc" value="<?php echo ew_HtmlEncode($Prov->PrvCRuc->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($Prov->PrvDire->Visible) { // PrvDire ?>
		<td data-name="PrvDire">
<span id="el$rowindex$_Prov_PrvDire" class="form-group Prov_PrvDire">
<input type="text" data-table="Prov" data-field="x_PrvDire" name="x<?php echo $Prov_list->RowIndex ?>_PrvDire" id="x<?php echo $Prov_list->RowIndex ?>_PrvDire" size="30" placeholder="<?php echo ew_HtmlEncode($Prov->PrvDire->getPlaceHolder()) ?>" value="<?php echo $Prov->PrvDire->EditValue ?>"<?php echo $Prov->PrvDire->EditAttributes() ?>>
</span>
<input type="hidden" data-table="Prov" data-field="x_PrvDire" name="o<?php echo $Prov_list->RowIndex ?>_PrvDire" id="o<?php echo $Prov_list->RowIndex ?>_PrvDire" value="<?php echo ew_HtmlEncode($Prov->PrvDire->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($Prov->PrvMail->Visible) { // PrvMail ?>
		<td data-name="PrvMail">
<span id="el$rowindex$_Prov_PrvMail" class="form-group Prov_PrvMail">
<input type="text" data-table="Prov" data-field="x_PrvMail" name="x<?php echo $Prov_list->RowIndex ?>_PrvMail" id="x<?php echo $Prov_list->RowIndex ?>_PrvMail" size="30" maxlength="50" placeholder="<?php echo ew_HtmlEncode($Prov->PrvMail->getPlaceHolder()) ?>" value="<?php echo $Prov->PrvMail->EditValue ?>"<?php echo $Prov->PrvMail->EditAttributes() ?>>
</span>
<input type="hidden" data-table="Prov" data-field="x_PrvMail" name="o<?php echo $Prov_list->RowIndex ?>_PrvMail" id="o<?php echo $Prov_list->RowIndex ?>_PrvMail" value="<?php echo ew_HtmlEncode($Prov->PrvMail->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($Prov->PrvTele->Visible) { // PrvTele ?>
		<td data-name="PrvTele">
<span id="el$rowindex$_Prov_PrvTele" class="form-group Prov_PrvTele">
<input type="text" data-table="Prov" data-field="x_PrvTele" name="x<?php echo $Prov_list->RowIndex ?>_PrvTele" id="x<?php echo $Prov_list->RowIndex ?>_PrvTele" size="30" placeholder="<?php echo ew_HtmlEncode($Prov->PrvTele->getPlaceHolder()) ?>" value="<?php echo $Prov->PrvTele->EditValue ?>"<?php echo $Prov->PrvTele->EditAttributes() ?>>
</span>
<input type="hidden" data-table="Prov" data-field="x_PrvTele" name="o<?php echo $Prov_list->RowIndex ?>_PrvTele" id="o<?php echo $Prov_list->RowIndex ?>_PrvTele" value="<?php echo ew_HtmlEncode($Prov->PrvTele->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($Prov->PrvRSoc->Visible) { // PrvRSoc ?>
		<td data-name="PrvRSoc">
<span id="el$rowindex$_Prov_PrvRSoc" class="form-group Prov_PrvRSoc">
<input type="text" data-table="Prov" data-field="x_PrvRSoc" name="x<?php echo $Prov_list->RowIndex ?>_PrvRSoc" id="x<?php echo $Prov_list->RowIndex ?>_PrvRSoc" size="30" placeholder="<?php echo ew_HtmlEncode($Prov->PrvRSoc->getPlaceHolder()) ?>" value="<?php echo $Prov->PrvRSoc->EditValue ?>"<?php echo $Prov->PrvRSoc->EditAttributes() ?>>
</span>
<input type="hidden" data-table="Prov" data-field="x_PrvRSoc" name="o<?php echo $Prov_list->RowIndex ?>_PrvRSoc" id="o<?php echo $Prov_list->RowIndex ?>_PrvRSoc" value="<?php echo ew_HtmlEncode($Prov->PrvRSoc->OldValue) ?>">
</td>
	<?php } ?>
<?php

// Render list options (body, right)
$Prov_list->ListOptions->Render("body", "right", $Prov_list->RowCnt);
?>
<script type="text/javascript">
fProvlist.UpdateOpts(<?php echo $Prov_list->RowIndex ?>);
</script>
	</tr>
<?php
}
?>
</tbody>
</table>
<?php } ?>
<?php if ($Prov->CurrentAction == "gridadd") { ?>
<input type="hidden" name="a_list" id="a_list" value="gridinsert">
<input type="hidden" name="<?php echo $Prov_list->FormKeyCountName ?>" id="<?php echo $Prov_list->FormKeyCountName ?>" value="<?php echo $Prov_list->KeyCount ?>">
<?php echo $Prov_list->MultiSelectKey ?>
<?php } ?>
<?php if ($Prov->CurrentAction == "gridedit") { ?>
<input type="hidden" name="a_list" id="a_list" value="gridupdate">
<input type="hidden" name="<?php echo $Prov_list->FormKeyCountName ?>" id="<?php echo $Prov_list->FormKeyCountName ?>" value="<?php echo $Prov_list->KeyCount ?>">
<?php echo $Prov_list->MultiSelectKey ?>
<?php } ?>
<?php if ($Prov->CurrentAction == "") { ?>
<input type="hidden" name="a_list" id="a_list" value="">
<?php } ?>
</div>
</form>
<?php

// Close recordset
if ($Prov_list->Recordset)
	$Prov_list->Recordset->Close();
?>
<div class="panel-footer ewGridLowerPanel">
<?php if ($Prov->CurrentAction <> "gridadd" && $Prov->CurrentAction <> "gridedit") { ?>
<form name="ewPagerForm" class="ewForm form-inline ewPagerForm" action="<?php echo ew_CurrentPage() ?>">
<?php if (!isset($Prov_list->Pager)) $Prov_list->Pager = new cPrevNextPager($Prov_list->StartRec, $Prov_list->DisplayRecs, $Prov_list->TotalRecs) ?>
<?php if ($Prov_list->Pager->RecordCount > 0) { ?>
<div class="ewPager">
<span><?php echo $Language->Phrase("Page") ?>&nbsp;</span>
<div class="ewPrevNext"><div class="input-group">
<div class="input-group-btn">
<!--first page button-->
	<?php if ($Prov_list->Pager->FirstButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerFirst") ?>" href="<?php echo $Prov_list->PageUrl() ?>start=<?php echo $Prov_list->Pager->FirstButton->Start ?>"><span class="icon-first ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerFirst") ?>"><span class="icon-first ewIcon"></span></a>
	<?php } ?>
<!--previous page button-->
	<?php if ($Prov_list->Pager->PrevButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerPrevious") ?>" href="<?php echo $Prov_list->PageUrl() ?>start=<?php echo $Prov_list->Pager->PrevButton->Start ?>"><span class="icon-prev ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerPrevious") ?>"><span class="icon-prev ewIcon"></span></a>
	<?php } ?>
</div>
<!--current page number-->
	<input class="form-control input-sm" type="text" name="<?php echo EW_TABLE_PAGE_NO ?>" value="<?php echo $Prov_list->Pager->CurrentPage ?>">
<div class="input-group-btn">
<!--next page button-->
	<?php if ($Prov_list->Pager->NextButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerNext") ?>" href="<?php echo $Prov_list->PageUrl() ?>start=<?php echo $Prov_list->Pager->NextButton->Start ?>"><span class="icon-next ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerNext") ?>"><span class="icon-next ewIcon"></span></a>
	<?php } ?>
<!--last page button-->
	<?php if ($Prov_list->Pager->LastButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerLast") ?>" href="<?php echo $Prov_list->PageUrl() ?>start=<?php echo $Prov_list->Pager->LastButton->Start ?>"><span class="icon-last ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerLast") ?>"><span class="icon-last ewIcon"></span></a>
	<?php } ?>
</div>
</div>
</div>
<span>&nbsp;<?php echo $Language->Phrase("of") ?>&nbsp;<?php echo $Prov_list->Pager->PageCount ?></span>
</div>
<div class="ewPager ewRec">
	<span><?php echo $Language->Phrase("Record") ?>&nbsp;<?php echo $Prov_list->Pager->FromIndex ?>&nbsp;<?php echo $Language->Phrase("To") ?>&nbsp;<?php echo $Prov_list->Pager->ToIndex ?>&nbsp;<?php echo $Language->Phrase("Of") ?>&nbsp;<?php echo $Prov_list->Pager->RecordCount ?></span>
</div>
<?php } ?>
</form>
<?php } ?>
<div class="ewListOtherOptions">
<?php
	foreach ($Prov_list->OtherOptions as &$option)
		$option->Render("body", "bottom");
?>
</div>
<div class="clearfix"></div>
</div>
</div>
<?php } ?>
<?php if ($Prov_list->TotalRecs == 0 && $Prov->CurrentAction == "") { // Show other options ?>
<div class="ewListOtherOptions">
<?php
	foreach ($Prov_list->OtherOptions as &$option) {
		$option->ButtonClass = "";
		$option->Render("body", "");
	}
?>
</div>
<div class="clearfix"></div>
<?php } ?>
<script type="text/javascript">
fProvlistsrch.Init();
fProvlistsrch.FilterList = <?php echo $Prov_list->GetFilterList() ?>;
fProvlist.Init();
</script>
<?php
$Prov_list->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$Prov_list->Page_Terminate();
?>
