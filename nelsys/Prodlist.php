<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "Prodinfo.php" ?>
<?php include_once "Usuainfo.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$Prod_list = NULL; // Initialize page object first

class cProd_list extends cProd {

	// Page ID
	var $PageID = 'list';

	// Project ID
	var $ProjectID = "{04439FF7-B43F-460F-8514-F71C8FF9E679}";

	// Table name
	var $TableName = 'Prod';

	// Page object name
	var $PageObjName = 'Prod_list';

	// Grid form hidden field names
	var $FormName = 'fProdlist';
	var $FormActionName = 'k_action';
	var $FormKeyName = 'k_key';
	var $FormOldKeyName = 'k_oldkey';
	var $FormBlankRowName = 'k_blankrow';
	var $FormKeyCountName = 'key_count';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Page URLs
	var $AddUrl;
	var $EditUrl;
	var $CopyUrl;
	var $DeleteUrl;
	var $ViewUrl;
	var $ListUrl;

	// Export URLs
	var $ExportPrintUrl;
	var $ExportHtmlUrl;
	var $ExportExcelUrl;
	var $ExportWordUrl;
	var $ExportXmlUrl;
	var $ExportCsvUrl;
	var $ExportPdfUrl;

	// Custom export
	var $ExportExcelCustom = FALSE;
	var $ExportWordCustom = FALSE;
	var $ExportPdfCustom = FALSE;
	var $ExportEmailCustom = FALSE;

	// Update URLs
	var $InlineAddUrl;
	var $InlineCopyUrl;
	var $InlineEditUrl;
	var $GridAddUrl;
	var $GridEditUrl;
	var $MultiDeleteUrl;
	var $MultiUpdateUrl;

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (Prod)
		if (!isset($GLOBALS["Prod"]) || get_class($GLOBALS["Prod"]) == "cProd") {
			$GLOBALS["Prod"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["Prod"];
		}

		// Initialize URLs
		$this->ExportPrintUrl = $this->PageUrl() . "export=print";
		$this->ExportExcelUrl = $this->PageUrl() . "export=excel";
		$this->ExportWordUrl = $this->PageUrl() . "export=word";
		$this->ExportHtmlUrl = $this->PageUrl() . "export=html";
		$this->ExportXmlUrl = $this->PageUrl() . "export=xml";
		$this->ExportCsvUrl = $this->PageUrl() . "export=csv";
		$this->ExportPdfUrl = $this->PageUrl() . "export=pdf";
		$this->AddUrl = "Prodadd.php";
		$this->InlineAddUrl = $this->PageUrl() . "a=add";
		$this->GridAddUrl = $this->PageUrl() . "a=gridadd";
		$this->GridEditUrl = $this->PageUrl() . "a=gridedit";
		$this->MultiDeleteUrl = "Proddelete.php";
		$this->MultiUpdateUrl = "Produpdate.php";

		// Table object (Usua)
		if (!isset($GLOBALS['Usua'])) $GLOBALS['Usua'] = new cUsua();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'list', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'Prod', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (Usua)
		if (!isset($UserTable)) {
			$UserTable = new cUsua();
			$UserTableConn = Conn($UserTable->DBID);
		}

		// List options
		$this->ListOptions = new cListOptions();
		$this->ListOptions->TableVar = $this->TableVar;

		// Export options
		$this->ExportOptions = new cListOptions();
		$this->ExportOptions->Tag = "div";
		$this->ExportOptions->TagClassName = "ewExportOption";

		// Other options
		$this->OtherOptions['addedit'] = new cListOptions();
		$this->OtherOptions['addedit']->Tag = "div";
		$this->OtherOptions['addedit']->TagClassName = "ewAddEditOption";
		$this->OtherOptions['detail'] = new cListOptions();
		$this->OtherOptions['detail']->Tag = "div";
		$this->OtherOptions['detail']->TagClassName = "ewDetailOption";
		$this->OtherOptions['action'] = new cListOptions();
		$this->OtherOptions['action']->Tag = "div";
		$this->OtherOptions['action']->TagClassName = "ewActionOption";

		// Filter options
		$this->FilterOptions = new cListOptions();
		$this->FilterOptions->Tag = "div";
		$this->FilterOptions->TagClassName = "ewFilterOption fProdlistsrch";

		// List actions
		$this->ListActions = new cListActions();
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanList()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage($Language->Phrase("NoPermission")); // Set no permission
			$this->Page_Terminate(ew_GetUrl("index.php"));
		}

		// Create form object
		$objForm = new cFormObj();
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action

		// Get grid add count
		$gridaddcnt = @$_GET[EW_TABLE_GRID_ADD_ROW_COUNT];
		if (is_numeric($gridaddcnt) && $gridaddcnt > 0)
			$this->GridAddRowCount = $gridaddcnt;

		// Set up list options
		$this->SetupListOptions();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Process auto fill
		if (@$_POST["ajax"] == "autofill") {
			$results = $this->GetAutoFill(@$_POST["name"], @$_POST["q"]);
			if ($results) {

				// Clean output buffer
				if (!EW_DEBUG_ENABLED && ob_get_length())
					ob_end_clean();
				echo $results;
				$this->Page_Terminate();
				exit();
			}
		}

		// Create Token
		$this->CreateToken();

		// Setup other options
		$this->SetupOtherOptions();

		// Set up custom action (compatible with old version)
		foreach ($this->CustomActions as $name => $action)
			$this->ListActions->Add($name, $action);

		// Show checkbox column if multiple action
		foreach ($this->ListActions->Items as $listaction) {
			if ($listaction->Select == EW_ACTION_MULTIPLE && $listaction->Allow) {
				$this->ListOptions->Items["checkbox"]->Visible = TRUE;
				break;
			}
		}
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $Prod;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($Prod);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}

	// Class variables
	var $ListOptions; // List options
	var $ExportOptions; // Export options
	var $SearchOptions; // Search options
	var $OtherOptions = array(); // Other options
	var $FilterOptions; // Filter options
	var $ListActions; // List actions
	var $SelectedCount = 0;
	var $SelectedIndex = 0;
	var $DisplayRecs = 20;
	var $StartRec;
	var $StopRec;
	var $TotalRecs = 0;
	var $RecRange = 10;
	var $Pager;
	var $DefaultSearchWhere = ""; // Default search WHERE clause
	var $SearchWhere = ""; // Search WHERE clause
	var $RecCnt = 0; // Record count
	var $EditRowCnt;
	var $StartRowCnt = 1;
	var $RowCnt = 0;
	var $Attrs = array(); // Row attributes and cell attributes
	var $RowIndex = 0; // Row index
	var $KeyCount = 0; // Key count
	var $RowAction = ""; // Row action
	var $RowOldKey = ""; // Row old key (for copy)
	var $RecPerRow = 0;
	var $MultiColumnClass;
	var $MultiColumnEditClass = "col-sm-12";
	var $MultiColumnCnt = 12;
	var $MultiColumnEditCnt = 12;
	var $GridCnt = 0;
	var $ColCnt = 0;
	var $DbMasterFilter = ""; // Master filter
	var $DbDetailFilter = ""; // Detail filter
	var $MasterRecordExists;	
	var $MultiSelectKey;
	var $Command;
	var $RestoreSearch = FALSE;
	var $DetailPages;
	var $Recordset;
	var $OldRecordset;

	//
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError, $gsSearchError, $Security;

		// Search filters
		$sSrchAdvanced = ""; // Advanced search filter
		$sSrchBasic = ""; // Basic search filter
		$sFilter = "";

		// Get command
		$this->Command = strtolower(@$_GET["cmd"]);
		if ($this->IsPageRequest()) { // Validate request

			// Process list action first
			if ($this->ProcessListAction()) // Ajax request
				$this->Page_Terminate();

			// Handle reset command
			$this->ResetCmd();

			// Set up Breadcrumb
			if ($this->Export == "")
				$this->SetupBreadcrumb();

			// Check QueryString parameters
			if (@$_GET["a"] <> "") {
				$this->CurrentAction = $_GET["a"];

				// Clear inline mode
				if ($this->CurrentAction == "cancel")
					$this->ClearInlineMode();

				// Switch to grid edit mode
				if ($this->CurrentAction == "gridedit")
					$this->GridEditMode();

				// Switch to grid add mode
				if ($this->CurrentAction == "gridadd")
					$this->GridAddMode();
			} else {
				if (@$_POST["a_list"] <> "") {
					$this->CurrentAction = $_POST["a_list"]; // Get action

					// Grid Update
					if (($this->CurrentAction == "gridupdate" || $this->CurrentAction == "gridoverwrite") && @$_SESSION[EW_SESSION_INLINE_MODE] == "gridedit") {
						if ($this->ValidateGridForm()) {
							$bGridUpdate = $this->GridUpdate();
						} else {
							$bGridUpdate = FALSE;
							$this->setFailureMessage($gsFormError);
						}
						if (!$bGridUpdate) {
							$this->EventCancelled = TRUE;
							$this->CurrentAction = "gridedit"; // Stay in Grid Edit mode
						}
					}

					// Grid Insert
					if ($this->CurrentAction == "gridinsert" && @$_SESSION[EW_SESSION_INLINE_MODE] == "gridadd") {
						if ($this->ValidateGridForm()) {
							$bGridInsert = $this->GridInsert();
						} else {
							$bGridInsert = FALSE;
							$this->setFailureMessage($gsFormError);
						}
						if (!$bGridInsert) {
							$this->EventCancelled = TRUE;
							$this->CurrentAction = "gridadd"; // Stay in Grid Add mode
						}
					}
				}
			}

			// Hide list options
			if ($this->Export <> "") {
				$this->ListOptions->HideAllOptions(array("sequence"));
				$this->ListOptions->UseDropDownButton = FALSE; // Disable drop down button
				$this->ListOptions->UseButtonGroup = FALSE; // Disable button group
			} elseif ($this->CurrentAction == "gridadd" || $this->CurrentAction == "gridedit") {
				$this->ListOptions->HideAllOptions();
				$this->ListOptions->UseDropDownButton = FALSE; // Disable drop down button
				$this->ListOptions->UseButtonGroup = FALSE; // Disable button group
			}

			// Hide options
			if ($this->Export <> "" || $this->CurrentAction <> "") {
				$this->ExportOptions->HideAllOptions();
				$this->FilterOptions->HideAllOptions();
			}

			// Hide other options
			if ($this->Export <> "") {
				foreach ($this->OtherOptions as &$option)
					$option->HideAllOptions();
			}

			// Show grid delete link for grid add / grid edit
			if ($this->AllowAddDeleteRow) {
				if ($this->CurrentAction == "gridadd" || $this->CurrentAction == "gridedit") {
					$item = $this->ListOptions->GetItem("griddelete");
					if ($item) $item->Visible = TRUE;
				}
			}

			// Get default search criteria
			ew_AddFilter($this->DefaultSearchWhere, $this->BasicSearchWhere(TRUE));

			// Get basic search values
			$this->LoadBasicSearchValues();

			// Restore filter list
			$this->RestoreFilterList();

			// Restore search parms from Session if not searching / reset / export
			if (($this->Export <> "" || $this->Command <> "search" && $this->Command <> "reset" && $this->Command <> "resetall") && $this->CheckSearchParms())
				$this->RestoreSearchParms();

			// Call Recordset SearchValidated event
			$this->Recordset_SearchValidated();

			// Set up sorting order
			$this->SetUpSortOrder();

			// Get basic search criteria
			if ($gsSearchError == "")
				$sSrchBasic = $this->BasicSearchWhere();
		}

		// Restore display records
		if ($this->getRecordsPerPage() <> "") {
			$this->DisplayRecs = $this->getRecordsPerPage(); // Restore from Session
		} else {
			$this->DisplayRecs = 20; // Load default
		}

		// Load Sorting Order
		$this->LoadSortOrder();

		// Load search default if no existing search criteria
		if (!$this->CheckSearchParms()) {

			// Load basic search from default
			$this->BasicSearch->LoadDefault();
			if ($this->BasicSearch->Keyword != "")
				$sSrchBasic = $this->BasicSearchWhere();
		}

		// Build search criteria
		ew_AddFilter($this->SearchWhere, $sSrchAdvanced);
		ew_AddFilter($this->SearchWhere, $sSrchBasic);

		// Call Recordset_Searching event
		$this->Recordset_Searching($this->SearchWhere);

		// Save search criteria
		if ($this->Command == "search" && !$this->RestoreSearch) {
			$this->setSearchWhere($this->SearchWhere); // Save to Session
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} else {
			$this->SearchWhere = $this->getSearchWhere();
		}

		// Build filter
		$sFilter = "";
		if (!$Security->CanList())
			$sFilter = "(0=1)"; // Filter all records
		ew_AddFilter($sFilter, $this->DbDetailFilter);
		ew_AddFilter($sFilter, $this->SearchWhere);

		// Set up filter in session
		$this->setSessionWhere($sFilter);
		$this->CurrentFilter = "";

		// Load record count first
		if (!$this->IsAddOrEdit()) {
			$bSelectLimit = $this->UseSelectLimit;
			if ($bSelectLimit) {
				$this->TotalRecs = $this->SelectRecordCount();
			} else {
				if ($this->Recordset = $this->LoadRecordset())
					$this->TotalRecs = $this->Recordset->RecordCount();
			}
		}

		// Search options
		$this->SetupSearchOptions();
	}

	//  Exit inline mode
	function ClearInlineMode() {
		$this->ProPrec->FormValue = ""; // Clear form value
		$this->LastAction = $this->CurrentAction; // Save last action
		$this->CurrentAction = ""; // Clear action
		$_SESSION[EW_SESSION_INLINE_MODE] = ""; // Clear inline mode
	}

	// Switch to Grid Add mode
	function GridAddMode() {
		$_SESSION[EW_SESSION_INLINE_MODE] = "gridadd"; // Enabled grid add
	}

	// Switch to Grid Edit mode
	function GridEditMode() {
		$_SESSION[EW_SESSION_INLINE_MODE] = "gridedit"; // Enable grid edit
	}

	// Perform update to grid
	function GridUpdate() {
		global $Language, $objForm, $gsFormError;
		$bGridUpdate = TRUE;

		// Get old recordset
		$this->CurrentFilter = $this->BuildKeyFilter();
		if ($this->CurrentFilter == "")
			$this->CurrentFilter = "0=1";
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		if ($rs = $conn->Execute($sSql)) {
			$rsold = $rs->GetRows();
			$rs->Close();
		}

		// Call Grid Updating event
		if (!$this->Grid_Updating($rsold)) {
			if ($this->getFailureMessage() == "")
				$this->setFailureMessage($Language->Phrase("GridEditCancelled")); // Set grid edit cancelled message
			return FALSE;
		}

		// Begin transaction
		$conn->BeginTrans();
		$sKey = "";

		// Update row index and get row key
		$objForm->Index = -1;
		$rowcnt = strval($objForm->GetValue($this->FormKeyCountName));
		if ($rowcnt == "" || !is_numeric($rowcnt))
			$rowcnt = 0;

		// Update all rows based on key
		for ($rowindex = 1; $rowindex <= $rowcnt; $rowindex++) {
			$objForm->Index = $rowindex;
			$rowkey = strval($objForm->GetValue($this->FormKeyName));
			$rowaction = strval($objForm->GetValue($this->FormActionName));

			// Load all values and keys
			if ($rowaction <> "insertdelete") { // Skip insert then deleted rows
				$this->LoadFormValues(); // Get form values
				if ($rowaction == "" || $rowaction == "edit" || $rowaction == "delete") {
					$bGridUpdate = $this->SetupKeyValues($rowkey); // Set up key values
				} else {
					$bGridUpdate = TRUE;
				}

				// Skip empty row
				if ($rowaction == "insert" && $this->EmptyRow()) {

					// No action required
				// Validate form and insert/update/delete record

				} elseif ($bGridUpdate) {
					if ($rowaction == "delete") {
						$this->CurrentFilter = $this->KeyFilter();
						$bGridUpdate = $this->DeleteRows(); // Delete this row
					} else if (!$this->ValidateForm()) {
						$bGridUpdate = FALSE; // Form error, reset action
						$this->setFailureMessage($gsFormError);
					} else {
						if ($rowaction == "insert") {
							$bGridUpdate = $this->AddRow(); // Insert this row
						} else {
							if ($rowkey <> "") {
								$this->SendEmail = FALSE; // Do not send email on update success
								$bGridUpdate = $this->EditRow(); // Update this row
							}
						} // End update
					}
				}
				if ($bGridUpdate) {
					if ($sKey <> "") $sKey .= ", ";
					$sKey .= $rowkey;
				} else {
					break;
				}
			}
		}
		if ($bGridUpdate) {
			$conn->CommitTrans(); // Commit transaction

			// Get new recordset
			if ($rs = $conn->Execute($sSql)) {
				$rsnew = $rs->GetRows();
				$rs->Close();
			}

			// Call Grid_Updated event
			$this->Grid_Updated($rsold, $rsnew);
			if ($this->getSuccessMessage() == "")
				$this->setSuccessMessage($Language->Phrase("UpdateSuccess")); // Set up update success message
			$this->ClearInlineMode(); // Clear inline edit mode
		} else {
			$conn->RollbackTrans(); // Rollback transaction
			if ($this->getFailureMessage() == "")
				$this->setFailureMessage($Language->Phrase("UpdateFailed")); // Set update failed message
		}
		return $bGridUpdate;
	}

	// Build filter for all keys
	function BuildKeyFilter() {
		global $objForm;
		$sWrkFilter = "";

		// Update row index and get row key
		$rowindex = 1;
		$objForm->Index = $rowindex;
		$sThisKey = strval($objForm->GetValue($this->FormKeyName));
		while ($sThisKey <> "") {
			if ($this->SetupKeyValues($sThisKey)) {
				$sFilter = $this->KeyFilter();
				if ($sWrkFilter <> "") $sWrkFilter .= " OR ";
				$sWrkFilter .= $sFilter;
			} else {
				$sWrkFilter = "0=1";
				break;
			}

			// Update row index and get row key
			$rowindex++; // Next row
			$objForm->Index = $rowindex;
			$sThisKey = strval($objForm->GetValue($this->FormKeyName));
		}
		return $sWrkFilter;
	}

	// Set up key values
	function SetupKeyValues($key) {
		$arrKeyFlds = explode($GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"], $key);
		if (count($arrKeyFlds) >= 1) {
			$this->ProdCodi->setFormValue($arrKeyFlds[0]);
			if (!is_numeric($this->ProdCodi->FormValue))
				return FALSE;
		}
		return TRUE;
	}

	// Perform Grid Add
	function GridInsert() {
		global $Language, $objForm, $gsFormError;
		$rowindex = 1;
		$bGridInsert = FALSE;
		$conn = &$this->Connection();

		// Call Grid Inserting event
		if (!$this->Grid_Inserting()) {
			if ($this->getFailureMessage() == "") {
				$this->setFailureMessage($Language->Phrase("GridAddCancelled")); // Set grid add cancelled message
			}
			return FALSE;
		}

		// Begin transaction
		$conn->BeginTrans();

		// Init key filter
		$sWrkFilter = "";
		$addcnt = 0;
		$sKey = "";

		// Get row count
		$objForm->Index = -1;
		$rowcnt = strval($objForm->GetValue($this->FormKeyCountName));
		if ($rowcnt == "" || !is_numeric($rowcnt))
			$rowcnt = 0;

		// Insert all rows
		for ($rowindex = 1; $rowindex <= $rowcnt; $rowindex++) {

			// Load current row values
			$objForm->Index = $rowindex;
			$rowaction = strval($objForm->GetValue($this->FormActionName));
			if ($rowaction <> "" && $rowaction <> "insert")
				continue; // Skip
			$this->LoadFormValues(); // Get form values
			if (!$this->EmptyRow()) {
				$addcnt++;
				$this->SendEmail = FALSE; // Do not send email on insert success

				// Validate form
				if (!$this->ValidateForm()) {
					$bGridInsert = FALSE; // Form error, reset action
					$this->setFailureMessage($gsFormError);
				} else {
					$bGridInsert = $this->AddRow($this->OldRecordset); // Insert this row
				}
				if ($bGridInsert) {
					if ($sKey <> "") $sKey .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
					$sKey .= $this->ProdCodi->CurrentValue;

					// Add filter for this record
					$sFilter = $this->KeyFilter();
					if ($sWrkFilter <> "") $sWrkFilter .= " OR ";
					$sWrkFilter .= $sFilter;
				} else {
					break;
				}
			}
		}
		if ($addcnt == 0) { // No record inserted
			$this->setFailureMessage($Language->Phrase("NoAddRecord"));
			$bGridInsert = FALSE;
		}
		if ($bGridInsert) {
			$conn->CommitTrans(); // Commit transaction

			// Get new recordset
			$this->CurrentFilter = $sWrkFilter;
			$sSql = $this->SQL();
			if ($rs = $conn->Execute($sSql)) {
				$rsnew = $rs->GetRows();
				$rs->Close();
			}

			// Call Grid_Inserted event
			$this->Grid_Inserted($rsnew);
			if ($this->getSuccessMessage() == "")
				$this->setSuccessMessage($Language->Phrase("InsertSuccess")); // Set up insert success message
			$this->ClearInlineMode(); // Clear grid add mode
		} else {
			$conn->RollbackTrans(); // Rollback transaction
			if ($this->getFailureMessage() == "") {
				$this->setFailureMessage($Language->Phrase("InsertFailed")); // Set insert failed message
			}
		}
		return $bGridInsert;
	}

	// Check if empty row
	function EmptyRow() {
		global $objForm;
		if ($objForm->HasValue("x_CprCodi") && $objForm->HasValue("o_CprCodi") && $this->CprCodi->CurrentValue <> $this->CprCodi->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_MprCodi") && $objForm->HasValue("o_MprCodi") && $this->MprCodi->CurrentValue <> $this->MprCodi->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_TprCodi") && $objForm->HasValue("o_TprCodi") && $this->TprCodi->CurrentValue <> $this->TprCodi->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_ProCant") && $objForm->HasValue("o_ProCant") && $this->ProCant->CurrentValue <> $this->ProCant->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_ProNMin") && $objForm->HasValue("o_ProNMin") && $this->ProNMin->CurrentValue <> $this->ProNMin->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_ProPrec") && $objForm->HasValue("o_ProPrec") && $this->ProPrec->CurrentValue <> $this->ProPrec->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_ProDesc") && $objForm->HasValue("o_ProDesc") && $this->ProDesc->CurrentValue <> $this->ProDesc->OldValue)
			return FALSE;
		return TRUE;
	}

	// Validate grid form
	function ValidateGridForm() {
		global $objForm;

		// Get row count
		$objForm->Index = -1;
		$rowcnt = strval($objForm->GetValue($this->FormKeyCountName));
		if ($rowcnt == "" || !is_numeric($rowcnt))
			$rowcnt = 0;

		// Validate all records
		for ($rowindex = 1; $rowindex <= $rowcnt; $rowindex++) {

			// Load current row values
			$objForm->Index = $rowindex;
			$rowaction = strval($objForm->GetValue($this->FormActionName));
			if ($rowaction <> "delete" && $rowaction <> "insertdelete") {
				$this->LoadFormValues(); // Get form values
				if ($rowaction == "insert" && $this->EmptyRow()) {

					// Ignore
				} else if (!$this->ValidateForm()) {
					return FALSE;
				}
			}
		}
		return TRUE;
	}

	// Get all form values of the grid
	function GetGridFormValues() {
		global $objForm;

		// Get row count
		$objForm->Index = -1;
		$rowcnt = strval($objForm->GetValue($this->FormKeyCountName));
		if ($rowcnt == "" || !is_numeric($rowcnt))
			$rowcnt = 0;
		$rows = array();

		// Loop through all records
		for ($rowindex = 1; $rowindex <= $rowcnt; $rowindex++) {

			// Load current row values
			$objForm->Index = $rowindex;
			$rowaction = strval($objForm->GetValue($this->FormActionName));
			if ($rowaction <> "delete" && $rowaction <> "insertdelete") {
				$this->LoadFormValues(); // Get form values
				if ($rowaction == "insert" && $this->EmptyRow()) {

					// Ignore
				} else {
					$rows[] = $this->GetFieldValues("FormValue"); // Return row as array
				}
			}
		}
		return $rows; // Return as array of array
	}

	// Restore form values for current row
	function RestoreCurrentRowFormValues($idx) {
		global $objForm;

		// Get row based on current index
		$objForm->Index = $idx;
		$this->LoadFormValues(); // Load form values
	}

	// Get list of filters
	function GetFilterList() {

		// Initialize
		$sFilterList = "";
		$sFilterList = ew_Concat($sFilterList, $this->ProdCodi->AdvancedSearch->ToJSON(), ","); // Field ProdCodi
		$sFilterList = ew_Concat($sFilterList, $this->CprCodi->AdvancedSearch->ToJSON(), ","); // Field CprCodi
		$sFilterList = ew_Concat($sFilterList, $this->MprCodi->AdvancedSearch->ToJSON(), ","); // Field MprCodi
		$sFilterList = ew_Concat($sFilterList, $this->TprCodi->AdvancedSearch->ToJSON(), ","); // Field TprCodi
		$sFilterList = ew_Concat($sFilterList, $this->ProCant->AdvancedSearch->ToJSON(), ","); // Field ProCant
		$sFilterList = ew_Concat($sFilterList, $this->ProNMin->AdvancedSearch->ToJSON(), ","); // Field ProNMin
		$sFilterList = ew_Concat($sFilterList, $this->ProPrec->AdvancedSearch->ToJSON(), ","); // Field ProPrec
		$sFilterList = ew_Concat($sFilterList, $this->ProDesc->AdvancedSearch->ToJSON(), ","); // Field ProDesc
		if ($this->BasicSearch->Keyword <> "") {
			$sWrk = "\"" . EW_TABLE_BASIC_SEARCH . "\":\"" . ew_JsEncode2($this->BasicSearch->Keyword) . "\",\"" . EW_TABLE_BASIC_SEARCH_TYPE . "\":\"" . ew_JsEncode2($this->BasicSearch->Type) . "\"";
			$sFilterList = ew_Concat($sFilterList, $sWrk, ",");
		}

		// Return filter list in json
		return ($sFilterList <> "") ? "{" . $sFilterList . "}" : "null";
	}

	// Restore list of filters
	function RestoreFilterList() {

		// Return if not reset filter
		if (@$_POST["cmd"] <> "resetfilter")
			return FALSE;
		$filter = json_decode(ew_StripSlashes(@$_POST["filter"]), TRUE);
		$this->Command = "search";

		// Field ProdCodi
		$this->ProdCodi->AdvancedSearch->SearchValue = @$filter["x_ProdCodi"];
		$this->ProdCodi->AdvancedSearch->SearchOperator = @$filter["z_ProdCodi"];
		$this->ProdCodi->AdvancedSearch->SearchCondition = @$filter["v_ProdCodi"];
		$this->ProdCodi->AdvancedSearch->SearchValue2 = @$filter["y_ProdCodi"];
		$this->ProdCodi->AdvancedSearch->SearchOperator2 = @$filter["w_ProdCodi"];
		$this->ProdCodi->AdvancedSearch->Save();

		// Field CprCodi
		$this->CprCodi->AdvancedSearch->SearchValue = @$filter["x_CprCodi"];
		$this->CprCodi->AdvancedSearch->SearchOperator = @$filter["z_CprCodi"];
		$this->CprCodi->AdvancedSearch->SearchCondition = @$filter["v_CprCodi"];
		$this->CprCodi->AdvancedSearch->SearchValue2 = @$filter["y_CprCodi"];
		$this->CprCodi->AdvancedSearch->SearchOperator2 = @$filter["w_CprCodi"];
		$this->CprCodi->AdvancedSearch->Save();

		// Field MprCodi
		$this->MprCodi->AdvancedSearch->SearchValue = @$filter["x_MprCodi"];
		$this->MprCodi->AdvancedSearch->SearchOperator = @$filter["z_MprCodi"];
		$this->MprCodi->AdvancedSearch->SearchCondition = @$filter["v_MprCodi"];
		$this->MprCodi->AdvancedSearch->SearchValue2 = @$filter["y_MprCodi"];
		$this->MprCodi->AdvancedSearch->SearchOperator2 = @$filter["w_MprCodi"];
		$this->MprCodi->AdvancedSearch->Save();

		// Field TprCodi
		$this->TprCodi->AdvancedSearch->SearchValue = @$filter["x_TprCodi"];
		$this->TprCodi->AdvancedSearch->SearchOperator = @$filter["z_TprCodi"];
		$this->TprCodi->AdvancedSearch->SearchCondition = @$filter["v_TprCodi"];
		$this->TprCodi->AdvancedSearch->SearchValue2 = @$filter["y_TprCodi"];
		$this->TprCodi->AdvancedSearch->SearchOperator2 = @$filter["w_TprCodi"];
		$this->TprCodi->AdvancedSearch->Save();

		// Field ProCant
		$this->ProCant->AdvancedSearch->SearchValue = @$filter["x_ProCant"];
		$this->ProCant->AdvancedSearch->SearchOperator = @$filter["z_ProCant"];
		$this->ProCant->AdvancedSearch->SearchCondition = @$filter["v_ProCant"];
		$this->ProCant->AdvancedSearch->SearchValue2 = @$filter["y_ProCant"];
		$this->ProCant->AdvancedSearch->SearchOperator2 = @$filter["w_ProCant"];
		$this->ProCant->AdvancedSearch->Save();

		// Field ProNMin
		$this->ProNMin->AdvancedSearch->SearchValue = @$filter["x_ProNMin"];
		$this->ProNMin->AdvancedSearch->SearchOperator = @$filter["z_ProNMin"];
		$this->ProNMin->AdvancedSearch->SearchCondition = @$filter["v_ProNMin"];
		$this->ProNMin->AdvancedSearch->SearchValue2 = @$filter["y_ProNMin"];
		$this->ProNMin->AdvancedSearch->SearchOperator2 = @$filter["w_ProNMin"];
		$this->ProNMin->AdvancedSearch->Save();

		// Field ProPrec
		$this->ProPrec->AdvancedSearch->SearchValue = @$filter["x_ProPrec"];
		$this->ProPrec->AdvancedSearch->SearchOperator = @$filter["z_ProPrec"];
		$this->ProPrec->AdvancedSearch->SearchCondition = @$filter["v_ProPrec"];
		$this->ProPrec->AdvancedSearch->SearchValue2 = @$filter["y_ProPrec"];
		$this->ProPrec->AdvancedSearch->SearchOperator2 = @$filter["w_ProPrec"];
		$this->ProPrec->AdvancedSearch->Save();

		// Field ProDesc
		$this->ProDesc->AdvancedSearch->SearchValue = @$filter["x_ProDesc"];
		$this->ProDesc->AdvancedSearch->SearchOperator = @$filter["z_ProDesc"];
		$this->ProDesc->AdvancedSearch->SearchCondition = @$filter["v_ProDesc"];
		$this->ProDesc->AdvancedSearch->SearchValue2 = @$filter["y_ProDesc"];
		$this->ProDesc->AdvancedSearch->SearchOperator2 = @$filter["w_ProDesc"];
		$this->ProDesc->AdvancedSearch->Save();
		$this->BasicSearch->setKeyword(@$filter[EW_TABLE_BASIC_SEARCH]);
		$this->BasicSearch->setType(@$filter[EW_TABLE_BASIC_SEARCH_TYPE]);
	}

	// Return basic search SQL
	function BasicSearchSQL($arKeywords, $type) {
		$sWhere = "";
		$this->BuildBasicSearchSQL($sWhere, $this->ProDesc, $arKeywords, $type);
		return $sWhere;
	}

	// Build basic search SQL
	function BuildBasicSearchSql(&$Where, &$Fld, $arKeywords, $type) {
		$sDefCond = ($type == "OR") ? "OR" : "AND";
		$arSQL = array(); // Array for SQL parts
		$arCond = array(); // Array for search conditions
		$cnt = count($arKeywords);
		$j = 0; // Number of SQL parts
		for ($i = 0; $i < $cnt; $i++) {
			$Keyword = $arKeywords[$i];
			$Keyword = trim($Keyword);
			if (EW_BASIC_SEARCH_IGNORE_PATTERN <> "") {
				$Keyword = preg_replace(EW_BASIC_SEARCH_IGNORE_PATTERN, "\\", $Keyword);
				$ar = explode("\\", $Keyword);
			} else {
				$ar = array($Keyword);
			}
			foreach ($ar as $Keyword) {
				if ($Keyword <> "") {
					$sWrk = "";
					if ($Keyword == "OR" && $type == "") {
						if ($j > 0)
							$arCond[$j-1] = "OR";
					} elseif ($Keyword == EW_NULL_VALUE) {
						$sWrk = $Fld->FldExpression . " IS NULL";
					} elseif ($Keyword == EW_NOT_NULL_VALUE) {
						$sWrk = $Fld->FldExpression . " IS NOT NULL";
					} elseif ($Fld->FldIsVirtual && $Fld->FldVirtualSearch) {
						$sWrk = $Fld->FldVirtualExpression . ew_Like(ew_QuotedValue("%" . $Keyword . "%", EW_DATATYPE_STRING, $this->DBID), $this->DBID);
					} elseif ($Fld->FldDataType != EW_DATATYPE_NUMBER || is_numeric($Keyword)) {
						$sWrk = $Fld->FldBasicSearchExpression . ew_Like(ew_QuotedValue("%" . $Keyword . "%", EW_DATATYPE_STRING, $this->DBID), $this->DBID);
					}
					if ($sWrk <> "") {
						$arSQL[$j] = $sWrk;
						$arCond[$j] = $sDefCond;
						$j += 1;
					}
				}
			}
		}
		$cnt = count($arSQL);
		$bQuoted = FALSE;
		$sSql = "";
		if ($cnt > 0) {
			for ($i = 0; $i < $cnt-1; $i++) {
				if ($arCond[$i] == "OR") {
					if (!$bQuoted) $sSql .= "(";
					$bQuoted = TRUE;
				}
				$sSql .= $arSQL[$i];
				if ($bQuoted && $arCond[$i] <> "OR") {
					$sSql .= ")";
					$bQuoted = FALSE;
				}
				$sSql .= " " . $arCond[$i] . " ";
			}
			$sSql .= $arSQL[$cnt-1];
			if ($bQuoted)
				$sSql .= ")";
		}
		if ($sSql <> "") {
			if ($Where <> "") $Where .= " OR ";
			$Where .=  "(" . $sSql . ")";
		}
	}

	// Return basic search WHERE clause based on search keyword and type
	function BasicSearchWhere($Default = FALSE) {
		global $Security;
		$sSearchStr = "";
		if (!$Security->CanSearch()) return "";
		$sSearchKeyword = ($Default) ? $this->BasicSearch->KeywordDefault : $this->BasicSearch->Keyword;
		$sSearchType = ($Default) ? $this->BasicSearch->TypeDefault : $this->BasicSearch->Type;
		if ($sSearchKeyword <> "") {
			$sSearch = trim($sSearchKeyword);
			if ($sSearchType <> "=") {
				$ar = array();

				// Match quoted keywords (i.e.: "...")
				if (preg_match_all('/"([^"]*)"/i', $sSearch, $matches, PREG_SET_ORDER)) {
					foreach ($matches as $match) {
						$p = strpos($sSearch, $match[0]);
						$str = substr($sSearch, 0, $p);
						$sSearch = substr($sSearch, $p + strlen($match[0]));
						if (strlen(trim($str)) > 0)
							$ar = array_merge($ar, explode(" ", trim($str)));
						$ar[] = $match[1]; // Save quoted keyword
					}
				}

				// Match individual keywords
				if (strlen(trim($sSearch)) > 0)
					$ar = array_merge($ar, explode(" ", trim($sSearch)));

				// Search keyword in any fields
				if (($sSearchType == "OR" || $sSearchType == "AND") && $this->BasicSearch->BasicSearchAnyFields) {
					foreach ($ar as $sKeyword) {
						if ($sKeyword <> "") {
							if ($sSearchStr <> "") $sSearchStr .= " " . $sSearchType . " ";
							$sSearchStr .= "(" . $this->BasicSearchSQL(array($sKeyword), $sSearchType) . ")";
						}
					}
				} else {
					$sSearchStr = $this->BasicSearchSQL($ar, $sSearchType);
				}
			} else {
				$sSearchStr = $this->BasicSearchSQL(array($sSearch), $sSearchType);
			}
			if (!$Default) $this->Command = "search";
		}
		if (!$Default && $this->Command == "search") {
			$this->BasicSearch->setKeyword($sSearchKeyword);
			$this->BasicSearch->setType($sSearchType);
		}
		return $sSearchStr;
	}

	// Check if search parm exists
	function CheckSearchParms() {

		// Check basic search
		if ($this->BasicSearch->IssetSession())
			return TRUE;
		return FALSE;
	}

	// Clear all search parameters
	function ResetSearchParms() {

		// Clear search WHERE clause
		$this->SearchWhere = "";
		$this->setSearchWhere($this->SearchWhere);

		// Clear basic search parameters
		$this->ResetBasicSearchParms();
	}

	// Load advanced search default values
	function LoadAdvancedSearchDefault() {
		return FALSE;
	}

	// Clear all basic search parameters
	function ResetBasicSearchParms() {
		$this->BasicSearch->UnsetSession();
	}

	// Restore all search parameters
	function RestoreSearchParms() {
		$this->RestoreSearch = TRUE;

		// Restore basic search values
		$this->BasicSearch->Load();
	}

	// Set up sort parameters
	function SetUpSortOrder() {

		// Check for Ctrl pressed
		$bCtrl = (@$_GET["ctrl"] <> "");

		// Check for "order" parameter
		if (@$_GET["order"] <> "") {
			$this->CurrentOrder = ew_StripSlashes(@$_GET["order"]);
			$this->CurrentOrderType = @$_GET["ordertype"];
			$this->UpdateSort($this->CprCodi, $bCtrl); // CprCodi
			$this->UpdateSort($this->MprCodi, $bCtrl); // MprCodi
			$this->UpdateSort($this->TprCodi, $bCtrl); // TprCodi
			$this->UpdateSort($this->ProCant, $bCtrl); // ProCant
			$this->UpdateSort($this->ProNMin, $bCtrl); // ProNMin
			$this->UpdateSort($this->ProPrec, $bCtrl); // ProPrec
			$this->UpdateSort($this->ProDesc, $bCtrl); // ProDesc
			$this->setStartRecordNumber(1); // Reset start position
		}
	}

	// Load sort order parameters
	function LoadSortOrder() {
		$sOrderBy = $this->getSessionOrderBy(); // Get ORDER BY from Session
		if ($sOrderBy == "") {
			if ($this->getSqlOrderBy() <> "") {
				$sOrderBy = $this->getSqlOrderBy();
				$this->setSessionOrderBy($sOrderBy);
			}
		}
	}

	// Reset command
	// - cmd=reset (Reset search parameters)
	// - cmd=resetall (Reset search and master/detail parameters)
	// - cmd=resetsort (Reset sort parameters)
	function ResetCmd() {

		// Check if reset command
		if (substr($this->Command,0,5) == "reset") {

			// Reset search criteria
			if ($this->Command == "reset" || $this->Command == "resetall")
				$this->ResetSearchParms();

			// Reset sorting order
			if ($this->Command == "resetsort") {
				$sOrderBy = "";
				$this->setSessionOrderBy($sOrderBy);
				$this->CprCodi->setSort("");
				$this->MprCodi->setSort("");
				$this->TprCodi->setSort("");
				$this->ProCant->setSort("");
				$this->ProNMin->setSort("");
				$this->ProPrec->setSort("");
				$this->ProDesc->setSort("");
			}

			// Reset start position
			$this->StartRec = 1;
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Set up list options
	function SetupListOptions() {
		global $Security, $Language;

		// "griddelete"
		if ($this->AllowAddDeleteRow) {
			$item = &$this->ListOptions->Add("griddelete");
			$item->CssStyle = "white-space: nowrap;";
			$item->OnLeft = FALSE;
			$item->Visible = FALSE; // Default hidden
		}

		// Add group option item
		$item = &$this->ListOptions->Add($this->ListOptions->GroupOptionName);
		$item->Body = "";
		$item->OnLeft = FALSE;
		$item->Visible = FALSE;

		// "view"
		$item = &$this->ListOptions->Add("view");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->CanView();
		$item->OnLeft = FALSE;

		// "edit"
		$item = &$this->ListOptions->Add("edit");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->CanEdit();
		$item->OnLeft = FALSE;

		// "copy"
		$item = &$this->ListOptions->Add("copy");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->CanAdd();
		$item->OnLeft = FALSE;

		// "delete"
		$item = &$this->ListOptions->Add("delete");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->CanDelete();
		$item->OnLeft = FALSE;

		// List actions
		$item = &$this->ListOptions->Add("listactions");
		$item->CssStyle = "white-space: nowrap;";
		$item->OnLeft = FALSE;
		$item->Visible = FALSE;
		$item->ShowInButtonGroup = FALSE;
		$item->ShowInDropDown = FALSE;

		// "checkbox"
		$item = &$this->ListOptions->Add("checkbox");
		$item->Visible = FALSE;
		$item->OnLeft = FALSE;
		$item->Header = "<input type=\"checkbox\" name=\"key\" id=\"key\" onclick=\"ew_SelectAllKey(this);\">";
		$item->ShowInDropDown = FALSE;
		$item->ShowInButtonGroup = FALSE;

		// Drop down button for ListOptions
		$this->ListOptions->UseImageAndText = TRUE;
		$this->ListOptions->UseDropDownButton = FALSE;
		$this->ListOptions->DropDownButtonPhrase = $Language->Phrase("ButtonListOptions");
		$this->ListOptions->UseButtonGroup = FALSE;
		if ($this->ListOptions->UseButtonGroup && ew_IsMobile())
			$this->ListOptions->UseDropDownButton = TRUE;
		$this->ListOptions->ButtonClass = "btn-sm"; // Class for button group

		// Call ListOptions_Load event
		$this->ListOptions_Load();
		$this->SetupListOptionsExt();
		$item = &$this->ListOptions->GetItem($this->ListOptions->GroupOptionName);
		$item->Visible = $this->ListOptions->GroupOptionVisible();
	}

	// Render list options
	function RenderListOptions() {
		global $Security, $Language, $objForm;
		$this->ListOptions->LoadDefault();

		// Set up row action and key
		if (is_numeric($this->RowIndex) && $this->CurrentMode <> "view") {
			$objForm->Index = $this->RowIndex;
			$ActionName = str_replace("k_", "k" . $this->RowIndex . "_", $this->FormActionName);
			$OldKeyName = str_replace("k_", "k" . $this->RowIndex . "_", $this->FormOldKeyName);
			$KeyName = str_replace("k_", "k" . $this->RowIndex . "_", $this->FormKeyName);
			$BlankRowName = str_replace("k_", "k" . $this->RowIndex . "_", $this->FormBlankRowName);
			if ($this->RowAction <> "")
				$this->MultiSelectKey .= "<input type=\"hidden\" name=\"" . $ActionName . "\" id=\"" . $ActionName . "\" value=\"" . $this->RowAction . "\">";
			if ($this->RowAction == "delete") {
				$rowkey = $objForm->GetValue($this->FormKeyName);
				$this->SetupKeyValues($rowkey);
			}
			if ($this->RowAction == "insert" && $this->CurrentAction == "F" && $this->EmptyRow())
				$this->MultiSelectKey .= "<input type=\"hidden\" name=\"" . $BlankRowName . "\" id=\"" . $BlankRowName . "\" value=\"1\">";
		}

		// "delete"
		if ($this->AllowAddDeleteRow) {
			if ($this->CurrentAction == "gridadd" || $this->CurrentAction == "gridedit") {
				$option = &$this->ListOptions;
				$option->UseButtonGroup = TRUE; // Use button group for grid delete button
				$option->UseImageAndText = TRUE; // Use image and text for grid delete button
				$oListOpt = &$option->Items["griddelete"];
				if (!$Security->CanDelete() && is_numeric($this->RowIndex) && ($this->RowAction == "" || $this->RowAction == "edit")) { // Do not allow delete existing record
					$oListOpt->Body = "&nbsp;";
				} else {
					$oListOpt->Body = "<a class=\"ewGridLink ewGridDelete\" title=\"" . ew_HtmlTitle($Language->Phrase("DeleteLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("DeleteLink")) . "\" onclick=\"return ew_DeleteGridRow(this, " . $this->RowIndex . ");\">" . $Language->Phrase("DeleteLink") . "</a>";
				}
			}
		}

		// "view"
		$oListOpt = &$this->ListOptions->Items["view"];
		if ($Security->CanView())
			$oListOpt->Body = "<a class=\"ewRowLink ewView\" title=\"" . ew_HtmlTitle($Language->Phrase("ViewLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("ViewLink")) . "\" href=\"" . ew_HtmlEncode($this->ViewUrl) . "\">" . $Language->Phrase("ViewLink") . "</a>";
		else
			$oListOpt->Body = "";

		// "edit"
		$oListOpt = &$this->ListOptions->Items["edit"];
		if ($Security->CanEdit()) {
			$oListOpt->Body = "<a class=\"ewRowLink ewEdit\" title=\"" . ew_HtmlTitle($Language->Phrase("EditLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("EditLink")) . "\" href=\"" . ew_HtmlEncode($this->EditUrl) . "\">" . $Language->Phrase("EditLink") . "</a>";
		} else {
			$oListOpt->Body = "";
		}

		// "copy"
		$oListOpt = &$this->ListOptions->Items["copy"];
		if ($Security->CanAdd()) {
			$oListOpt->Body = "<a class=\"ewRowLink ewCopy\" title=\"" . ew_HtmlTitle($Language->Phrase("CopyLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("CopyLink")) . "\" href=\"" . ew_HtmlEncode($this->CopyUrl) . "\">" . $Language->Phrase("CopyLink") . "</a>";
		} else {
			$oListOpt->Body = "";
		}

		// "delete"
		$oListOpt = &$this->ListOptions->Items["delete"];
		if ($Security->CanDelete())
			$oListOpt->Body = "<a class=\"ewRowLink ewDelete\"" . "" . " title=\"" . ew_HtmlTitle($Language->Phrase("DeleteLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("DeleteLink")) . "\" href=\"" . ew_HtmlEncode($this->DeleteUrl) . "\">" . $Language->Phrase("DeleteLink") . "</a>";
		else
			$oListOpt->Body = "";

		// Set up list action buttons
		$oListOpt = &$this->ListOptions->GetItem("listactions");
		if ($oListOpt) {
			$body = "";
			$links = array();
			foreach ($this->ListActions->Items as $listaction) {
				if ($listaction->Select == EW_ACTION_SINGLE && $listaction->Allow) {
					$action = $listaction->Action;
					$caption = $listaction->Caption;
					$icon = ($listaction->Icon <> "") ? "<span class=\"" . ew_HtmlEncode(str_replace(" ewIcon", "", $listaction->Icon)) . "\" data-caption=\"" . ew_HtmlTitle($caption) . "\"></span> " : "";
					$links[] = "<li><a class=\"ewAction ewListAction\" data-action=\"" . ew_HtmlEncode($action) . "\" data-caption=\"" . ew_HtmlTitle($caption) . "\" href=\"\" onclick=\"ew_SubmitAction(event,jQuery.extend({key:" . $this->KeyToJson() . "}," . $listaction->ToJson(TRUE) . "));return false;\">" . $icon . $listaction->Caption . "</a></li>";
					if (count($links) == 1) // Single button
						$body = "<a class=\"ewAction ewListAction\" data-action=\"" . ew_HtmlEncode($action) . "\" title=\"" . ew_HtmlTitle($caption) . "\" data-caption=\"" . ew_HtmlTitle($caption) . "\" href=\"\" onclick=\"ew_SubmitAction(event,jQuery.extend({key:" . $this->KeyToJson() . "}," . $listaction->ToJson(TRUE) . "));return false;\">" . $Language->Phrase("ListActionButton") . "</a>";
				}
			}
			if (count($links) > 1) { // More than one buttons, use dropdown
				$body = "<button class=\"dropdown-toggle btn btn-default btn-sm ewActions\" title=\"" . ew_HtmlTitle($Language->Phrase("ListActionButton")) . "\" data-toggle=\"dropdown\">" . $Language->Phrase("ListActionButton") . "<b class=\"caret\"></b></button>";
				$content = "";
				foreach ($links as $link)
					$content .= "<li>" . $link . "</li>";
				$body .= "<ul class=\"dropdown-menu" . ($oListOpt->OnLeft ? "" : " dropdown-menu-right") . "\">". $content . "</ul>";
				$body = "<div class=\"btn-group\">" . $body . "</div>";
			}
			if (count($links) > 0) {
				$oListOpt->Body = $body;
				$oListOpt->Visible = TRUE;
			}
		}

		// "checkbox"
		$oListOpt = &$this->ListOptions->Items["checkbox"];
		$oListOpt->Body = "<input type=\"checkbox\" name=\"key_m[]\" value=\"" . ew_HtmlEncode($this->ProdCodi->CurrentValue) . "\" onclick='ew_ClickMultiCheckbox(event);'>";
		if ($this->CurrentAction == "gridedit" && is_numeric($this->RowIndex)) {
			$this->MultiSelectKey .= "<input type=\"hidden\" name=\"" . $KeyName . "\" id=\"" . $KeyName . "\" value=\"" . $this->ProdCodi->CurrentValue . "\">";
		}
		$this->RenderListOptionsExt();

		// Call ListOptions_Rendered event
		$this->ListOptions_Rendered();
	}

	// Set up other options
	function SetupOtherOptions() {
		global $Language, $Security;
		$options = &$this->OtherOptions;
		$option = $options["addedit"];

		// Add
		$item = &$option->Add("add");
		$item->Body = "<a class=\"ewAddEdit ewAdd\" title=\"" . ew_HtmlTitle($Language->Phrase("AddLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("AddLink")) . "\" href=\"" . ew_HtmlEncode($this->AddUrl) . "\">" . $Language->Phrase("AddLink") . "</a>";
		$item->Visible = ($this->AddUrl <> "" && $Security->CanAdd());
		$item = &$option->Add("gridadd");
		$item->Body = "<a class=\"ewAddEdit ewGridAdd\" title=\"" . ew_HtmlTitle($Language->Phrase("GridAddLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("GridAddLink")) . "\" href=\"" . ew_HtmlEncode($this->GridAddUrl) . "\">" . $Language->Phrase("GridAddLink") . "</a>";
		$item->Visible = ($this->GridAddUrl <> "" && $Security->CanAdd());

		// Add grid edit
		$option = $options["addedit"];
		$item = &$option->Add("gridedit");
		$item->Body = "<a class=\"ewAddEdit ewGridEdit\" title=\"" . ew_HtmlTitle($Language->Phrase("GridEditLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("GridEditLink")) . "\" href=\"" . ew_HtmlEncode($this->GridEditUrl) . "\">" . $Language->Phrase("GridEditLink") . "</a>";
		$item->Visible = ($this->GridEditUrl <> "" && $Security->CanEdit());
		$option = $options["action"];

		// Set up options default
		foreach ($options as &$option) {
			$option->UseImageAndText = TRUE;
			$option->UseDropDownButton = FALSE;
			$option->UseButtonGroup = TRUE;
			$option->ButtonClass = "btn-sm"; // Class for button group
			$item = &$option->Add($option->GroupOptionName);
			$item->Body = "";
			$item->Visible = FALSE;
		}
		$options["addedit"]->DropDownButtonPhrase = $Language->Phrase("ButtonAddEdit");
		$options["detail"]->DropDownButtonPhrase = $Language->Phrase("ButtonDetails");
		$options["action"]->DropDownButtonPhrase = $Language->Phrase("ButtonActions");

		// Filter button
		$item = &$this->FilterOptions->Add("savecurrentfilter");
		$item->Body = "<a class=\"ewSaveFilter\" data-form=\"fProdlistsrch\" href=\"#\">" . $Language->Phrase("SaveCurrentFilter") . "</a>";
		$item->Visible = TRUE;
		$item = &$this->FilterOptions->Add("deletefilter");
		$item->Body = "<a class=\"ewDeleteFilter\" data-form=\"fProdlistsrch\" href=\"#\">" . $Language->Phrase("DeleteFilter") . "</a>";
		$item->Visible = TRUE;
		$this->FilterOptions->UseDropDownButton = TRUE;
		$this->FilterOptions->UseButtonGroup = !$this->FilterOptions->UseDropDownButton;
		$this->FilterOptions->DropDownButtonPhrase = $Language->Phrase("Filters");

		// Add group option item
		$item = &$this->FilterOptions->Add($this->FilterOptions->GroupOptionName);
		$item->Body = "";
		$item->Visible = FALSE;
	}

	// Render other options
	function RenderOtherOptions() {
		global $Language, $Security;
		$options = &$this->OtherOptions;
		if ($this->CurrentAction <> "gridadd" && $this->CurrentAction <> "gridedit") { // Not grid add/edit mode
			$option = &$options["action"];

			// Set up list action buttons
			foreach ($this->ListActions->Items as $listaction) {
				if ($listaction->Select == EW_ACTION_MULTIPLE) {
					$item = &$option->Add("custom_" . $listaction->Action);
					$caption = $listaction->Caption;
					$icon = ($listaction->Icon <> "") ? "<span class=\"" . ew_HtmlEncode($listaction->Icon) . "\" data-caption=\"" . ew_HtmlEncode($caption) . "\"></span> " : $caption;
					$item->Body = "<a class=\"ewAction ewListAction\" title=\"" . ew_HtmlEncode($caption) . "\" data-caption=\"" . ew_HtmlEncode($caption) . "\" href=\"\" onclick=\"ew_SubmitAction(event,jQuery.extend({f:document.fProdlist}," . $listaction->ToJson(TRUE) . "));return false;\">" . $icon . "</a>";
					$item->Visible = $listaction->Allow;
				}
			}

			// Hide grid edit and other options
			if ($this->TotalRecs <= 0) {
				$option = &$options["addedit"];
				$item = &$option->GetItem("gridedit");
				if ($item) $item->Visible = FALSE;
				$option = &$options["action"];
				$option->HideAllOptions();
			}
		} else { // Grid add/edit mode

			// Hide all options first
			foreach ($options as &$option)
				$option->HideAllOptions();
			if ($this->CurrentAction == "gridadd") {
				if ($this->AllowAddDeleteRow) {

					// Add add blank row
					$option = &$options["addedit"];
					$option->UseDropDownButton = FALSE;
					$option->UseImageAndText = TRUE;
					$item = &$option->Add("addblankrow");
					$item->Body = "<a class=\"ewAddEdit ewAddBlankRow\" title=\"" . ew_HtmlTitle($Language->Phrase("AddBlankRow")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("AddBlankRow")) . "\" href=\"javascript:void(0);\" onclick=\"ew_AddGridRow(this);\">" . $Language->Phrase("AddBlankRow") . "</a>";
					$item->Visible = $Security->CanAdd();
				}
				$option = &$options["action"];
				$option->UseDropDownButton = FALSE;
				$option->UseImageAndText = TRUE;

				// Add grid insert
				$item = &$option->Add("gridinsert");
				$item->Body = "<a class=\"ewAction ewGridInsert\" title=\"" . ew_HtmlTitle($Language->Phrase("GridInsertLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("GridInsertLink")) . "\" href=\"\" onclick=\"return ewForms(this).Submit('" . $this->AddMasterUrl($this->PageName()) . "');\">" . $Language->Phrase("GridInsertLink") . "</a>";

				// Add grid cancel
				$item = &$option->Add("gridcancel");
				$cancelurl = $this->AddMasterUrl($this->PageUrl() . "a=cancel");
				$item->Body = "<a class=\"ewAction ewGridCancel\" title=\"" . ew_HtmlTitle($Language->Phrase("GridCancelLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("GridCancelLink")) . "\" href=\"" . $cancelurl . "\">" . $Language->Phrase("GridCancelLink") . "</a>";
			}
			if ($this->CurrentAction == "gridedit") {
				if ($this->AllowAddDeleteRow) {

					// Add add blank row
					$option = &$options["addedit"];
					$option->UseDropDownButton = FALSE;
					$option->UseImageAndText = TRUE;
					$item = &$option->Add("addblankrow");
					$item->Body = "<a class=\"ewAddEdit ewAddBlankRow\" title=\"" . ew_HtmlTitle($Language->Phrase("AddBlankRow")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("AddBlankRow")) . "\" href=\"javascript:void(0);\" onclick=\"ew_AddGridRow(this);\">" . $Language->Phrase("AddBlankRow") . "</a>";
					$item->Visible = $Security->CanAdd();
				}
				$option = &$options["action"];
				$option->UseDropDownButton = FALSE;
				$option->UseImageAndText = TRUE;
					$item = &$option->Add("gridsave");
					$item->Body = "<a class=\"ewAction ewGridSave\" title=\"" . ew_HtmlTitle($Language->Phrase("GridSaveLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("GridSaveLink")) . "\" href=\"\" onclick=\"return ewForms(this).Submit('" . $this->AddMasterUrl($this->PageName()) . "');\">" . $Language->Phrase("GridSaveLink") . "</a>";
					$item = &$option->Add("gridcancel");
					$cancelurl = $this->AddMasterUrl($this->PageUrl() . "a=cancel");
					$item->Body = "<a class=\"ewAction ewGridCancel\" title=\"" . ew_HtmlTitle($Language->Phrase("GridCancelLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("GridCancelLink")) . "\" href=\"" . $cancelurl . "\">" . $Language->Phrase("GridCancelLink") . "</a>";
			}
		}
	}

	// Process list action
	function ProcessListAction() {
		global $Language, $Security;
		$userlist = "";
		$user = "";
		$sFilter = $this->GetKeyFilter();
		$UserAction = @$_POST["useraction"];
		if ($sFilter <> "" && $UserAction <> "") {

			// Check permission first
			$ActionCaption = $UserAction;
			if (array_key_exists($UserAction, $this->ListActions->Items)) {
				$ActionCaption = $this->ListActions->Items[$UserAction]->Caption;
				if (!$this->ListActions->Items[$UserAction]->Allow) {
					$errmsg = str_replace('%s', $ActionCaption, $Language->Phrase("CustomActionNotAllowed"));
					if (@$_POST["ajax"] == $UserAction) // Ajax
						echo "<p class=\"text-danger\">" . $errmsg . "</p>";
					else
						$this->setFailureMessage($errmsg);
					return FALSE;
				}
			}
			$this->CurrentFilter = $sFilter;
			$sSql = $this->SQL();
			$conn = &$this->Connection();
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			$rs = $conn->Execute($sSql);
			$conn->raiseErrorFn = '';
			$this->CurrentAction = $UserAction;

			// Call row action event
			if ($rs && !$rs->EOF) {
				$conn->BeginTrans();
				$this->SelectedCount = $rs->RecordCount();
				$this->SelectedIndex = 0;
				while (!$rs->EOF) {
					$this->SelectedIndex++;
					$row = $rs->fields;
					$Processed = $this->Row_CustomAction($UserAction, $row);
					if (!$Processed) break;
					$rs->MoveNext();
				}
				if ($Processed) {
					$conn->CommitTrans(); // Commit the changes
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage(str_replace('%s', $ActionCaption, $Language->Phrase("CustomActionCompleted"))); // Set up success message
				} else {
					$conn->RollbackTrans(); // Rollback changes

					// Set up error message
					if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

						// Use the message, do nothing
					} elseif ($this->CancelMessage <> "") {
						$this->setFailureMessage($this->CancelMessage);
						$this->CancelMessage = "";
					} else {
						$this->setFailureMessage(str_replace('%s', $ActionCaption, $Language->Phrase("CustomActionFailed")));
					}
				}
			}
			if ($rs)
				$rs->Close();
			if (@$_POST["ajax"] == $UserAction) { // Ajax
				if ($this->getSuccessMessage() <> "") {
					echo "<p class=\"text-success\">" . $this->getSuccessMessage() . "</p>";
					$this->ClearSuccessMessage(); // Clear message
				}
				if ($this->getFailureMessage() <> "") {
					echo "<p class=\"text-danger\">" . $this->getFailureMessage() . "</p>";
					$this->ClearFailureMessage(); // Clear message
				}
				return TRUE;
			}
		}
		return FALSE; // Not ajax request
	}

	// Set up search options
	function SetupSearchOptions() {
		global $Language;
		$this->SearchOptions = new cListOptions();
		$this->SearchOptions->Tag = "div";
		$this->SearchOptions->TagClassName = "ewSearchOption";

		// Search button
		$item = &$this->SearchOptions->Add("searchtoggle");
		$SearchToggleClass = ($this->SearchWhere <> "") ? " active" : " active";
		$item->Body = "<button type=\"button\" class=\"btn btn-default ewSearchToggle" . $SearchToggleClass . "\" title=\"" . $Language->Phrase("SearchPanel") . "\" data-caption=\"" . $Language->Phrase("SearchPanel") . "\" data-toggle=\"button\" data-form=\"fProdlistsrch\">" . $Language->Phrase("SearchBtn") . "</button>";
		$item->Visible = TRUE;

		// Show all button
		$item = &$this->SearchOptions->Add("showall");
		$item->Body = "<a class=\"btn btn-default ewShowAll\" title=\"" . $Language->Phrase("ShowAll") . "\" data-caption=\"" . $Language->Phrase("ShowAll") . "\" href=\"" . $this->PageUrl() . "cmd=reset\">" . $Language->Phrase("ShowAllBtn") . "</a>";
		$item->Visible = ($this->SearchWhere <> $this->DefaultSearchWhere && $this->SearchWhere <> "0=101");

		// Button group for search
		$this->SearchOptions->UseDropDownButton = FALSE;
		$this->SearchOptions->UseImageAndText = TRUE;
		$this->SearchOptions->UseButtonGroup = TRUE;
		$this->SearchOptions->DropDownButtonPhrase = $Language->Phrase("ButtonSearch");

		// Add group option item
		$item = &$this->SearchOptions->Add($this->SearchOptions->GroupOptionName);
		$item->Body = "";
		$item->Visible = FALSE;

		// Hide search options
		if ($this->Export <> "" || $this->CurrentAction <> "")
			$this->SearchOptions->HideAllOptions();
		global $Security;
		if (!$Security->CanSearch()) {
			$this->SearchOptions->HideAllOptions();
			$this->FilterOptions->HideAllOptions();
		}
	}

	function SetupListOptionsExt() {
		global $Security, $Language;
	}

	function RenderListOptionsExt() {
		global $Security, $Language;
	}

	// Set up starting record parameters
	function SetUpStartRec() {
		if ($this->DisplayRecs == 0)
			return;
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET[EW_TABLE_START_REC] <> "") { // Check for "start" parameter
				$this->StartRec = $_GET[EW_TABLE_START_REC];
				$this->setStartRecordNumber($this->StartRec);
			} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
				$PageNo = $_GET[EW_TABLE_PAGE_NO];
				if (is_numeric($PageNo)) {
					$this->StartRec = ($PageNo-1)*$this->DisplayRecs+1;
					if ($this->StartRec <= 0) {
						$this->StartRec = 1;
					} elseif ($this->StartRec >= intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1) {
						$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1;
					}
					$this->setStartRecordNumber($this->StartRec);
				}
			}
		}
		$this->StartRec = $this->getStartRecordNumber();

		// Check if correct start record counter
		if (!is_numeric($this->StartRec) || $this->StartRec == "") { // Avoid invalid start record counter
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} elseif (intval($this->StartRec) > intval($this->TotalRecs)) { // Avoid starting record > total records
			$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to last page first record
			$this->setStartRecordNumber($this->StartRec);
		} elseif (($this->StartRec-1) % $this->DisplayRecs <> 0) {
			$this->StartRec = intval(($this->StartRec-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to page boundary
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Load default values
	function LoadDefaultValues() {
		$this->CprCodi->CurrentValue = NULL;
		$this->CprCodi->OldValue = $this->CprCodi->CurrentValue;
		$this->MprCodi->CurrentValue = NULL;
		$this->MprCodi->OldValue = $this->MprCodi->CurrentValue;
		$this->TprCodi->CurrentValue = NULL;
		$this->TprCodi->OldValue = $this->TprCodi->CurrentValue;
		$this->ProCant->CurrentValue = NULL;
		$this->ProCant->OldValue = $this->ProCant->CurrentValue;
		$this->ProNMin->CurrentValue = NULL;
		$this->ProNMin->OldValue = $this->ProNMin->CurrentValue;
		$this->ProPrec->CurrentValue = NULL;
		$this->ProPrec->OldValue = $this->ProPrec->CurrentValue;
		$this->ProDesc->CurrentValue = NULL;
		$this->ProDesc->OldValue = $this->ProDesc->CurrentValue;
	}

	// Load basic search values
	function LoadBasicSearchValues() {
		$this->BasicSearch->Keyword = @$_GET[EW_TABLE_BASIC_SEARCH];
		if ($this->BasicSearch->Keyword <> "") $this->Command = "search";
		$this->BasicSearch->Type = @$_GET[EW_TABLE_BASIC_SEARCH_TYPE];
	}

	// Load form values
	function LoadFormValues() {

		// Load from form
		global $objForm;
		if (!$this->CprCodi->FldIsDetailKey) {
			$this->CprCodi->setFormValue($objForm->GetValue("x_CprCodi"));
		}
		$this->CprCodi->setOldValue($objForm->GetValue("o_CprCodi"));
		if (!$this->MprCodi->FldIsDetailKey) {
			$this->MprCodi->setFormValue($objForm->GetValue("x_MprCodi"));
		}
		$this->MprCodi->setOldValue($objForm->GetValue("o_MprCodi"));
		if (!$this->TprCodi->FldIsDetailKey) {
			$this->TprCodi->setFormValue($objForm->GetValue("x_TprCodi"));
		}
		$this->TprCodi->setOldValue($objForm->GetValue("o_TprCodi"));
		if (!$this->ProCant->FldIsDetailKey) {
			$this->ProCant->setFormValue($objForm->GetValue("x_ProCant"));
		}
		$this->ProCant->setOldValue($objForm->GetValue("o_ProCant"));
		if (!$this->ProNMin->FldIsDetailKey) {
			$this->ProNMin->setFormValue($objForm->GetValue("x_ProNMin"));
		}
		$this->ProNMin->setOldValue($objForm->GetValue("o_ProNMin"));
		if (!$this->ProPrec->FldIsDetailKey) {
			$this->ProPrec->setFormValue($objForm->GetValue("x_ProPrec"));
		}
		$this->ProPrec->setOldValue($objForm->GetValue("o_ProPrec"));
		if (!$this->ProDesc->FldIsDetailKey) {
			$this->ProDesc->setFormValue($objForm->GetValue("x_ProDesc"));
		}
		$this->ProDesc->setOldValue($objForm->GetValue("o_ProDesc"));
		if (!$this->ProdCodi->FldIsDetailKey && $this->CurrentAction <> "gridadd" && $this->CurrentAction <> "add")
			$this->ProdCodi->setFormValue($objForm->GetValue("x_ProdCodi"));
	}

	// Restore form values
	function RestoreFormValues() {
		global $objForm;
		if ($this->CurrentAction <> "gridadd" && $this->CurrentAction <> "add")
			$this->ProdCodi->CurrentValue = $this->ProdCodi->FormValue;
		$this->CprCodi->CurrentValue = $this->CprCodi->FormValue;
		$this->MprCodi->CurrentValue = $this->MprCodi->FormValue;
		$this->TprCodi->CurrentValue = $this->TprCodi->FormValue;
		$this->ProCant->CurrentValue = $this->ProCant->FormValue;
		$this->ProNMin->CurrentValue = $this->ProNMin->FormValue;
		$this->ProPrec->CurrentValue = $this->ProPrec->FormValue;
		$this->ProDesc->CurrentValue = $this->ProDesc->FormValue;
	}

	// Load recordset
	function LoadRecordset($offset = -1, $rowcnt = -1) {

		// Load List page SQL
		$sSql = $this->SelectSQL();
		$conn = &$this->Connection();

		// Load recordset
		$dbtype = ew_GetConnectionType($this->DBID);
		if ($this->UseSelectLimit) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			if ($dbtype == "MSSQL") {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset, array("_hasOrderBy" => trim($this->getOrderBy()) || trim($this->getSessionOrderBy())));
			} else {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset);
			}
			$conn->raiseErrorFn = '';
		} else {
			$rs = ew_LoadRecordset($sSql, $conn);
		}

		// Call Recordset Selected event
		$this->Recordset_Selected($rs);
		return $rs;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->ProdCodi->setDbValue($rs->fields('ProdCodi'));
		$this->CprCodi->setDbValue($rs->fields('CprCodi'));
		$this->MprCodi->setDbValue($rs->fields('MprCodi'));
		$this->TprCodi->setDbValue($rs->fields('TprCodi'));
		$this->ProCant->setDbValue($rs->fields('ProCant'));
		$this->ProNMin->setDbValue($rs->fields('ProNMin'));
		$this->ProPrec->setDbValue($rs->fields('ProPrec'));
		$this->ProDesc->setDbValue($rs->fields('ProDesc'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->ProdCodi->DbValue = $row['ProdCodi'];
		$this->CprCodi->DbValue = $row['CprCodi'];
		$this->MprCodi->DbValue = $row['MprCodi'];
		$this->TprCodi->DbValue = $row['TprCodi'];
		$this->ProCant->DbValue = $row['ProCant'];
		$this->ProNMin->DbValue = $row['ProNMin'];
		$this->ProPrec->DbValue = $row['ProPrec'];
		$this->ProDesc->DbValue = $row['ProDesc'];
	}

	// Load old record
	function LoadOldRecord() {

		// Load key values from Session
		$bValidKey = TRUE;
		if (strval($this->getKey("ProdCodi")) <> "")
			$this->ProdCodi->CurrentValue = $this->getKey("ProdCodi"); // ProdCodi
		else
			$bValidKey = FALSE;

		// Load old recordset
		if ($bValidKey) {
			$this->CurrentFilter = $this->KeyFilter();
			$sSql = $this->SQL();
			$conn = &$this->Connection();
			$this->OldRecordset = ew_LoadRecordset($sSql, $conn);
			$this->LoadRowValues($this->OldRecordset); // Load row values
		} else {
			$this->OldRecordset = NULL;
		}
		return $bValidKey;
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		$this->ViewUrl = $this->GetViewUrl();
		$this->EditUrl = $this->GetEditUrl();
		$this->InlineEditUrl = $this->GetInlineEditUrl();
		$this->CopyUrl = $this->GetCopyUrl();
		$this->InlineCopyUrl = $this->GetInlineCopyUrl();
		$this->DeleteUrl = $this->GetDeleteUrl();

		// Convert decimal values if posted back
		if ($this->ProPrec->FormValue == $this->ProPrec->CurrentValue && is_numeric(ew_StrToFloat($this->ProPrec->CurrentValue)))
			$this->ProPrec->CurrentValue = ew_StrToFloat($this->ProPrec->CurrentValue);

		// Call Row_Rendering event
		$this->Row_Rendering();

		// Common render codes for all row types
		// ProdCodi
		// CprCodi
		// MprCodi
		// TprCodi
		// ProCant
		// ProNMin
		// ProPrec
		// ProDesc

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// ProdCodi
		$this->ProdCodi->ViewValue = $this->ProdCodi->CurrentValue;
		$this->ProdCodi->ViewCustomAttributes = "";

		// CprCodi
		if (strval($this->CprCodi->CurrentValue) <> "") {
			$sFilterWrk = "\"CprCodi\"" . ew_SearchString("=", $this->CprCodi->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT \"CprCodi\", \"CprNomb\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"CPro\"";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->CprCodi, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->CprCodi->ViewValue = $this->CprCodi->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->CprCodi->ViewValue = $this->CprCodi->CurrentValue;
			}
		} else {
			$this->CprCodi->ViewValue = NULL;
		}
		$this->CprCodi->ViewCustomAttributes = "";

		// MprCodi
		if (strval($this->MprCodi->CurrentValue) <> "") {
			$sFilterWrk = "\"MprCodi\"" . ew_SearchString("=", $this->MprCodi->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT \"MprCodi\", \"MprNomb\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"Mpro\"";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->MprCodi, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->MprCodi->ViewValue = $this->MprCodi->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->MprCodi->ViewValue = $this->MprCodi->CurrentValue;
			}
		} else {
			$this->MprCodi->ViewValue = NULL;
		}
		$this->MprCodi->ViewCustomAttributes = "";

		// TprCodi
		if (strval($this->TprCodi->CurrentValue) <> "") {
			$sFilterWrk = "\"TprCodi\"" . ew_SearchString("=", $this->TprCodi->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT \"TprCodi\", \"TprNomb\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"TProd\"";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->TprCodi, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->TprCodi->ViewValue = $this->TprCodi->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->TprCodi->ViewValue = $this->TprCodi->CurrentValue;
			}
		} else {
			$this->TprCodi->ViewValue = NULL;
		}
		$this->TprCodi->ViewCustomAttributes = "";

		// ProCant
		$this->ProCant->ViewValue = $this->ProCant->CurrentValue;
		$this->ProCant->ViewCustomAttributes = "";

		// ProNMin
		$this->ProNMin->ViewValue = $this->ProNMin->CurrentValue;
		$this->ProNMin->ViewCustomAttributes = "";

		// ProPrec
		$this->ProPrec->ViewValue = $this->ProPrec->CurrentValue;
		$this->ProPrec->ViewCustomAttributes = "";

		// ProDesc
		$this->ProDesc->ViewValue = $this->ProDesc->CurrentValue;
		$this->ProDesc->ViewCustomAttributes = "";

			// CprCodi
			$this->CprCodi->LinkCustomAttributes = "";
			$this->CprCodi->HrefValue = "";
			$this->CprCodi->TooltipValue = "";

			// MprCodi
			$this->MprCodi->LinkCustomAttributes = "";
			$this->MprCodi->HrefValue = "";
			$this->MprCodi->TooltipValue = "";

			// TprCodi
			$this->TprCodi->LinkCustomAttributes = "";
			$this->TprCodi->HrefValue = "";
			$this->TprCodi->TooltipValue = "";

			// ProCant
			$this->ProCant->LinkCustomAttributes = "";
			$this->ProCant->HrefValue = "";
			$this->ProCant->TooltipValue = "";

			// ProNMin
			$this->ProNMin->LinkCustomAttributes = "";
			$this->ProNMin->HrefValue = "";
			$this->ProNMin->TooltipValue = "";

			// ProPrec
			$this->ProPrec->LinkCustomAttributes = "";
			$this->ProPrec->HrefValue = "";
			$this->ProPrec->TooltipValue = "";

			// ProDesc
			$this->ProDesc->LinkCustomAttributes = "";
			$this->ProDesc->HrefValue = "";
			$this->ProDesc->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_ADD) { // Add row

			// CprCodi
			$this->CprCodi->EditAttrs["class"] = "form-control";
			$this->CprCodi->EditCustomAttributes = "";
			if (trim(strval($this->CprCodi->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "\"CprCodi\"" . ew_SearchString("=", $this->CprCodi->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT \"CprCodi\", \"CprNomb\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\", '' AS \"SelectFilterFld\", '' AS \"SelectFilterFld2\", '' AS \"SelectFilterFld3\", '' AS \"SelectFilterFld4\" FROM \"public\".\"CPro\"";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->CprCodi, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->CprCodi->EditValue = $arwrk;

			// MprCodi
			$this->MprCodi->EditAttrs["class"] = "form-control";
			$this->MprCodi->EditCustomAttributes = "";
			if (trim(strval($this->MprCodi->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "\"MprCodi\"" . ew_SearchString("=", $this->MprCodi->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT \"MprCodi\", \"MprNomb\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\", '' AS \"SelectFilterFld\", '' AS \"SelectFilterFld2\", '' AS \"SelectFilterFld3\", '' AS \"SelectFilterFld4\" FROM \"public\".\"Mpro\"";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->MprCodi, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->MprCodi->EditValue = $arwrk;

			// TprCodi
			$this->TprCodi->EditAttrs["class"] = "form-control";
			$this->TprCodi->EditCustomAttributes = "";
			if (trim(strval($this->TprCodi->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "\"TprCodi\"" . ew_SearchString("=", $this->TprCodi->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT \"TprCodi\", \"TprNomb\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\", '' AS \"SelectFilterFld\", '' AS \"SelectFilterFld2\", '' AS \"SelectFilterFld3\", '' AS \"SelectFilterFld4\" FROM \"public\".\"TProd\"";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->TprCodi, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->TprCodi->EditValue = $arwrk;

			// ProCant
			$this->ProCant->EditAttrs["class"] = "form-control";
			$this->ProCant->EditCustomAttributes = "";
			$this->ProCant->EditValue = ew_HtmlEncode($this->ProCant->CurrentValue);
			$this->ProCant->PlaceHolder = ew_RemoveHtml($this->ProCant->FldCaption());

			// ProNMin
			$this->ProNMin->EditAttrs["class"] = "form-control";
			$this->ProNMin->EditCustomAttributes = "";
			$this->ProNMin->EditValue = ew_HtmlEncode($this->ProNMin->CurrentValue);
			$this->ProNMin->PlaceHolder = ew_RemoveHtml($this->ProNMin->FldCaption());

			// ProPrec
			$this->ProPrec->EditAttrs["class"] = "form-control";
			$this->ProPrec->EditCustomAttributes = "";
			$this->ProPrec->EditValue = ew_HtmlEncode($this->ProPrec->CurrentValue);
			$this->ProPrec->PlaceHolder = ew_RemoveHtml($this->ProPrec->FldCaption());
			if (strval($this->ProPrec->EditValue) <> "" && is_numeric($this->ProPrec->EditValue)) {
			$this->ProPrec->EditValue = ew_FormatNumber($this->ProPrec->EditValue, -2, -1, -2, 0);
			$this->ProPrec->OldValue = $this->ProPrec->EditValue;
			}

			// ProDesc
			$this->ProDesc->EditAttrs["class"] = "form-control";
			$this->ProDesc->EditCustomAttributes = "";
			$this->ProDesc->EditValue = ew_HtmlEncode($this->ProDesc->CurrentValue);
			$this->ProDesc->PlaceHolder = ew_RemoveHtml($this->ProDesc->FldCaption());

			// Edit refer script
			// CprCodi

			$this->CprCodi->HrefValue = "";

			// MprCodi
			$this->MprCodi->HrefValue = "";

			// TprCodi
			$this->TprCodi->HrefValue = "";

			// ProCant
			$this->ProCant->HrefValue = "";

			// ProNMin
			$this->ProNMin->HrefValue = "";

			// ProPrec
			$this->ProPrec->HrefValue = "";

			// ProDesc
			$this->ProDesc->HrefValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_EDIT) { // Edit row

			// CprCodi
			$this->CprCodi->EditAttrs["class"] = "form-control";
			$this->CprCodi->EditCustomAttributes = "";
			if (trim(strval($this->CprCodi->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "\"CprCodi\"" . ew_SearchString("=", $this->CprCodi->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT \"CprCodi\", \"CprNomb\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\", '' AS \"SelectFilterFld\", '' AS \"SelectFilterFld2\", '' AS \"SelectFilterFld3\", '' AS \"SelectFilterFld4\" FROM \"public\".\"CPro\"";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->CprCodi, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->CprCodi->EditValue = $arwrk;

			// MprCodi
			$this->MprCodi->EditAttrs["class"] = "form-control";
			$this->MprCodi->EditCustomAttributes = "";
			if (trim(strval($this->MprCodi->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "\"MprCodi\"" . ew_SearchString("=", $this->MprCodi->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT \"MprCodi\", \"MprNomb\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\", '' AS \"SelectFilterFld\", '' AS \"SelectFilterFld2\", '' AS \"SelectFilterFld3\", '' AS \"SelectFilterFld4\" FROM \"public\".\"Mpro\"";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->MprCodi, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->MprCodi->EditValue = $arwrk;

			// TprCodi
			$this->TprCodi->EditAttrs["class"] = "form-control";
			$this->TprCodi->EditCustomAttributes = "";
			if (trim(strval($this->TprCodi->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "\"TprCodi\"" . ew_SearchString("=", $this->TprCodi->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT \"TprCodi\", \"TprNomb\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\", '' AS \"SelectFilterFld\", '' AS \"SelectFilterFld2\", '' AS \"SelectFilterFld3\", '' AS \"SelectFilterFld4\" FROM \"public\".\"TProd\"";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->TprCodi, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->TprCodi->EditValue = $arwrk;

			// ProCant
			$this->ProCant->EditAttrs["class"] = "form-control";
			$this->ProCant->EditCustomAttributes = "";
			$this->ProCant->EditValue = ew_HtmlEncode($this->ProCant->CurrentValue);
			$this->ProCant->PlaceHolder = ew_RemoveHtml($this->ProCant->FldCaption());

			// ProNMin
			$this->ProNMin->EditAttrs["class"] = "form-control";
			$this->ProNMin->EditCustomAttributes = "";
			$this->ProNMin->EditValue = ew_HtmlEncode($this->ProNMin->CurrentValue);
			$this->ProNMin->PlaceHolder = ew_RemoveHtml($this->ProNMin->FldCaption());

			// ProPrec
			$this->ProPrec->EditAttrs["class"] = "form-control";
			$this->ProPrec->EditCustomAttributes = "";
			$this->ProPrec->EditValue = ew_HtmlEncode($this->ProPrec->CurrentValue);
			$this->ProPrec->PlaceHolder = ew_RemoveHtml($this->ProPrec->FldCaption());
			if (strval($this->ProPrec->EditValue) <> "" && is_numeric($this->ProPrec->EditValue)) {
			$this->ProPrec->EditValue = ew_FormatNumber($this->ProPrec->EditValue, -2, -1, -2, 0);
			$this->ProPrec->OldValue = $this->ProPrec->EditValue;
			}

			// ProDesc
			$this->ProDesc->EditAttrs["class"] = "form-control";
			$this->ProDesc->EditCustomAttributes = "";
			$this->ProDesc->EditValue = ew_HtmlEncode($this->ProDesc->CurrentValue);
			$this->ProDesc->PlaceHolder = ew_RemoveHtml($this->ProDesc->FldCaption());

			// Edit refer script
			// CprCodi

			$this->CprCodi->HrefValue = "";

			// MprCodi
			$this->MprCodi->HrefValue = "";

			// TprCodi
			$this->TprCodi->HrefValue = "";

			// ProCant
			$this->ProCant->HrefValue = "";

			// ProNMin
			$this->ProNMin->HrefValue = "";

			// ProPrec
			$this->ProPrec->HrefValue = "";

			// ProDesc
			$this->ProDesc->HrefValue = "";
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate form
	function ValidateForm() {
		global $Language, $gsFormError;

		// Initialize form error message
		$gsFormError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return ($gsFormError == "");
		if (!$this->CprCodi->FldIsDetailKey && !is_null($this->CprCodi->FormValue) && $this->CprCodi->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->CprCodi->FldCaption(), $this->CprCodi->ReqErrMsg));
		}
		if (!$this->MprCodi->FldIsDetailKey && !is_null($this->MprCodi->FormValue) && $this->MprCodi->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->MprCodi->FldCaption(), $this->MprCodi->ReqErrMsg));
		}
		if (!$this->TprCodi->FldIsDetailKey && !is_null($this->TprCodi->FormValue) && $this->TprCodi->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->TprCodi->FldCaption(), $this->TprCodi->ReqErrMsg));
		}
		if (!$this->ProCant->FldIsDetailKey && !is_null($this->ProCant->FormValue) && $this->ProCant->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->ProCant->FldCaption(), $this->ProCant->ReqErrMsg));
		}
		if (!ew_CheckInteger($this->ProCant->FormValue)) {
			ew_AddMessage($gsFormError, $this->ProCant->FldErrMsg());
		}
		if (!$this->ProNMin->FldIsDetailKey && !is_null($this->ProNMin->FormValue) && $this->ProNMin->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->ProNMin->FldCaption(), $this->ProNMin->ReqErrMsg));
		}
		if (!ew_CheckInteger($this->ProNMin->FormValue)) {
			ew_AddMessage($gsFormError, $this->ProNMin->FldErrMsg());
		}
		if (!$this->ProPrec->FldIsDetailKey && !is_null($this->ProPrec->FormValue) && $this->ProPrec->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->ProPrec->FldCaption(), $this->ProPrec->ReqErrMsg));
		}
		if (!ew_CheckNumber($this->ProPrec->FormValue)) {
			ew_AddMessage($gsFormError, $this->ProPrec->FldErrMsg());
		}

		// Return validate result
		$ValidateForm = ($gsFormError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateForm = $ValidateForm && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsFormError, $sFormCustomError);
		}
		return $ValidateForm;
	}

	//
	// Delete records based on current filter
	//
	function DeleteRows() {
		global $Language, $Security;
		if (!$Security->CanDelete()) {
			$this->setFailureMessage($Language->Phrase("NoDeletePermission")); // No delete permission
			return FALSE;
		}
		$DeleteRows = TRUE;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE) {
			return FALSE;
		} elseif ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
			$rs->Close();
			return FALSE;

		//} else {
		//	$this->LoadRowValues($rs); // Load row values

		}
		$rows = ($rs) ? $rs->GetRows() : array();

		// Clone old rows
		$rsold = $rows;
		if ($rs)
			$rs->Close();

		// Call row deleting event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$DeleteRows = $this->Row_Deleting($row);
				if (!$DeleteRows) break;
			}
		}
		if ($DeleteRows) {
			$sKey = "";
			foreach ($rsold as $row) {
				$sThisKey = "";
				if ($sThisKey <> "") $sThisKey .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
				$sThisKey .= $row['ProdCodi'];
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				$DeleteRows = $this->Delete($row); // Delete
				$conn->raiseErrorFn = '';
				if ($DeleteRows === FALSE)
					break;
				if ($sKey <> "") $sKey .= ", ";
				$sKey .= $sThisKey;
			}
		} else {

			// Set up error message
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("DeleteCancelled"));
			}
		}
		if ($DeleteRows) {
		} else {
		}

		// Call Row Deleted event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$this->Row_Deleted($row);
			}
		}
		return $DeleteRows;
	}

	// Update record based on key values
	function EditRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$conn = &$this->Connection();
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE)
			return FALSE;
		if ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record message
			$EditRow = FALSE; // Update Failed
		} else {

			// Save old values
			$rsold = &$rs->fields;
			$this->LoadDbValues($rsold);
			$rsnew = array();

			// CprCodi
			$this->CprCodi->SetDbValueDef($rsnew, $this->CprCodi->CurrentValue, 0, $this->CprCodi->ReadOnly);

			// MprCodi
			$this->MprCodi->SetDbValueDef($rsnew, $this->MprCodi->CurrentValue, 0, $this->MprCodi->ReadOnly);

			// TprCodi
			$this->TprCodi->SetDbValueDef($rsnew, $this->TprCodi->CurrentValue, 0, $this->TprCodi->ReadOnly);

			// ProCant
			$this->ProCant->SetDbValueDef($rsnew, $this->ProCant->CurrentValue, 0, $this->ProCant->ReadOnly);

			// ProNMin
			$this->ProNMin->SetDbValueDef($rsnew, $this->ProNMin->CurrentValue, 0, $this->ProNMin->ReadOnly);

			// ProPrec
			$this->ProPrec->SetDbValueDef($rsnew, $this->ProPrec->CurrentValue, 0, $this->ProPrec->ReadOnly);

			// ProDesc
			$this->ProDesc->SetDbValueDef($rsnew, $this->ProDesc->CurrentValue, NULL, $this->ProDesc->ReadOnly);

			// Call Row Updating event
			$bUpdateRow = $this->Row_Updating($rsold, $rsnew);
			if ($bUpdateRow) {
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				if (count($rsnew) > 0)
					$EditRow = $this->Update($rsnew, "", $rsold);
				else
					$EditRow = TRUE; // No field to update
				$conn->raiseErrorFn = '';
				if ($EditRow) {
				}
			} else {
				if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

					// Use the message, do nothing
				} elseif ($this->CancelMessage <> "") {
					$this->setFailureMessage($this->CancelMessage);
					$this->CancelMessage = "";
				} else {
					$this->setFailureMessage($Language->Phrase("UpdateCancelled"));
				}
				$EditRow = FALSE;
			}
		}

		// Call Row_Updated event
		if ($EditRow)
			$this->Row_Updated($rsold, $rsnew);
		$rs->Close();
		return $EditRow;
	}

	// Add record
	function AddRow($rsold = NULL) {
		global $Language, $Security;
		$conn = &$this->Connection();

		// Load db values from rsold
		if ($rsold) {
			$this->LoadDbValues($rsold);
		}
		$rsnew = array();

		// CprCodi
		$this->CprCodi->SetDbValueDef($rsnew, $this->CprCodi->CurrentValue, 0, FALSE);

		// MprCodi
		$this->MprCodi->SetDbValueDef($rsnew, $this->MprCodi->CurrentValue, 0, FALSE);

		// TprCodi
		$this->TprCodi->SetDbValueDef($rsnew, $this->TprCodi->CurrentValue, 0, FALSE);

		// ProCant
		$this->ProCant->SetDbValueDef($rsnew, $this->ProCant->CurrentValue, 0, FALSE);

		// ProNMin
		$this->ProNMin->SetDbValueDef($rsnew, $this->ProNMin->CurrentValue, 0, FALSE);

		// ProPrec
		$this->ProPrec->SetDbValueDef($rsnew, $this->ProPrec->CurrentValue, 0, FALSE);

		// ProDesc
		$this->ProDesc->SetDbValueDef($rsnew, $this->ProDesc->CurrentValue, NULL, FALSE);

		// Call Row Inserting event
		$rs = ($rsold == NULL) ? NULL : $rsold->fields;
		$bInsertRow = $this->Row_Inserting($rs, $rsnew);
		if ($bInsertRow) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			$AddRow = $this->Insert($rsnew);
			$conn->raiseErrorFn = '';
			if ($AddRow) {

				// Get insert id if necessary
				$this->ProdCodi->setDbValue($conn->GetOne("SELECT currval('\"Prod_ProdCodi_seq\"'::regclass)"));
				$rsnew['ProdCodi'] = $this->ProdCodi->DbValue;
			}
		} else {
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("InsertCancelled"));
			}
			$AddRow = FALSE;
		}
		if ($AddRow) {

			// Call Row Inserted event
			$rs = ($rsold == NULL) ? NULL : $rsold->fields;
			$this->Row_Inserted($rs, $rsnew);
		}
		return $AddRow;
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$url = preg_replace('/\?cmd=reset(all){0,1}$/i', '', $url); // Remove cmd=reset / cmd=resetall
		$Breadcrumb->Add("list", $this->TableVar, $url, "", $this->TableVar, TRUE);
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}

	// ListOptions Load event
	function ListOptions_Load() {

		// Example:
		//$opt = &$this->ListOptions->Add("new");
		//$opt->Header = "xxx";
		//$opt->OnLeft = TRUE; // Link on left
		//$opt->MoveTo(0); // Move to first column

	}

	// ListOptions Rendered event
	function ListOptions_Rendered() {

		// Example: 
		//$this->ListOptions->Items["new"]->Body = "xxx";

	}

	// Row Custom Action event
	function Row_CustomAction($action, $row) {

		// Return FALSE to abort
		return TRUE;
	}

	// Page Exporting event
	// $this->ExportDoc = export document object
	function Page_Exporting() {

		//$this->ExportDoc->Text = "my header"; // Export header
		//return FALSE; // Return FALSE to skip default export and use Row_Export event

		return TRUE; // Return TRUE to use default export and skip Row_Export event
	}

	// Row Export event
	// $this->ExportDoc = export document object
	function Row_Export($rs) {

	    //$this->ExportDoc->Text .= "my content"; // Build HTML with field value: $rs["MyField"] or $this->MyField->ViewValue
	}

	// Page Exported event
	// $this->ExportDoc = export document object
	function Page_Exported() {

		//$this->ExportDoc->Text .= "my footer"; // Export footer
		//echo $this->ExportDoc->Text;

	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($Prod_list)) $Prod_list = new cProd_list();

// Page init
$Prod_list->Page_Init();

// Page main
$Prod_list->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$Prod_list->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "list";
var CurrentForm = fProdlist = new ew_Form("fProdlist", "list");
fProdlist.FormKeyCountName = '<?php echo $Prod_list->FormKeyCountName ?>';

// Validate form
fProdlist.Validate = function() {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.GetForm(), $fobj = $(fobj);
	if ($fobj.find("#a_confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.FormKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = $fobj.find("#a_list").val() == "gridinsert";
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
		var checkrow = (gridinsert) ? !this.EmptyRow(infix) : true;
		if (checkrow) {
			addcnt++;
			elm = this.GetElements("x" + infix + "_CprCodi");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $Prod->CprCodi->FldCaption(), $Prod->CprCodi->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_MprCodi");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $Prod->MprCodi->FldCaption(), $Prod->MprCodi->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_TprCodi");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $Prod->TprCodi->FldCaption(), $Prod->TprCodi->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_ProCant");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $Prod->ProCant->FldCaption(), $Prod->ProCant->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_ProCant");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($Prod->ProCant->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_ProNMin");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $Prod->ProNMin->FldCaption(), $Prod->ProNMin->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_ProNMin");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($Prod->ProNMin->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_ProPrec");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $Prod->ProPrec->FldCaption(), $Prod->ProPrec->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_ProPrec");
			if (elm && !ew_CheckNumber(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($Prod->ProPrec->FldErrMsg()) ?>");

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
		} // End Grid Add checking
	}
	if (gridinsert && addcnt == 0) { // No row added
		ew_Alert(ewLanguage.Phrase("NoAddRecord"));
		return false;
	}
	return true;
}

// Check empty row
fProdlist.EmptyRow = function(infix) {
	var fobj = this.Form;
	if (ew_ValueChanged(fobj, infix, "CprCodi", false)) return false;
	if (ew_ValueChanged(fobj, infix, "MprCodi", false)) return false;
	if (ew_ValueChanged(fobj, infix, "TprCodi", false)) return false;
	if (ew_ValueChanged(fobj, infix, "ProCant", false)) return false;
	if (ew_ValueChanged(fobj, infix, "ProNMin", false)) return false;
	if (ew_ValueChanged(fobj, infix, "ProPrec", false)) return false;
	if (ew_ValueChanged(fobj, infix, "ProDesc", false)) return false;
	return true;
}

// Form_CustomValidate event
fProdlist.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fProdlist.ValidateRequired = true;
<?php } else { ?>
fProdlist.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
fProdlist.Lists["x_CprCodi"] = {"LinkField":"x_CprCodi","Ajax":true,"AutoFill":false,"DisplayFields":["x_CprNomb","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fProdlist.Lists["x_MprCodi"] = {"LinkField":"x_MprCodi","Ajax":true,"AutoFill":false,"DisplayFields":["x_MprNomb","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fProdlist.Lists["x_TprCodi"] = {"LinkField":"x_TprCodi","Ajax":true,"AutoFill":false,"DisplayFields":["x_TprNomb","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};

// Form object for search
var CurrentSearchForm = fProdlistsrch = new ew_Form("fProdlistsrch");
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php if ($Prod_list->TotalRecs > 0 && $Prod_list->ExportOptions->Visible()) { ?>
<?php $Prod_list->ExportOptions->Render("body") ?>
<?php } ?>
<?php if ($Prod_list->SearchOptions->Visible()) { ?>
<?php $Prod_list->SearchOptions->Render("body") ?>
<?php } ?>
<?php if ($Prod_list->FilterOptions->Visible()) { ?>
<?php $Prod_list->FilterOptions->Render("body") ?>
<?php } ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php
if ($Prod->CurrentAction == "gridadd") {
	$Prod->CurrentFilter = "0=1";
	$Prod_list->StartRec = 1;
	$Prod_list->DisplayRecs = $Prod->GridAddRowCount;
	$Prod_list->TotalRecs = $Prod_list->DisplayRecs;
	$Prod_list->StopRec = $Prod_list->DisplayRecs;
} else {
	$bSelectLimit = $Prod_list->UseSelectLimit;
	if ($bSelectLimit) {
		if ($Prod_list->TotalRecs <= 0)
			$Prod_list->TotalRecs = $Prod->SelectRecordCount();
	} else {
		if (!$Prod_list->Recordset && ($Prod_list->Recordset = $Prod_list->LoadRecordset()))
			$Prod_list->TotalRecs = $Prod_list->Recordset->RecordCount();
	}
	$Prod_list->StartRec = 1;
	if ($Prod_list->DisplayRecs <= 0 || ($Prod->Export <> "" && $Prod->ExportAll)) // Display all records
		$Prod_list->DisplayRecs = $Prod_list->TotalRecs;
	if (!($Prod->Export <> "" && $Prod->ExportAll))
		$Prod_list->SetUpStartRec(); // Set up start record position
	if ($bSelectLimit)
		$Prod_list->Recordset = $Prod_list->LoadRecordset($Prod_list->StartRec-1, $Prod_list->DisplayRecs);

	// Set no record found message
	if ($Prod->CurrentAction == "" && $Prod_list->TotalRecs == 0) {
		if (!$Security->CanList())
			$Prod_list->setWarningMessage($Language->Phrase("NoPermission"));
		if ($Prod_list->SearchWhere == "0=101")
			$Prod_list->setWarningMessage($Language->Phrase("EnterSearchCriteria"));
		else
			$Prod_list->setWarningMessage($Language->Phrase("NoRecord"));
	}
}
$Prod_list->RenderOtherOptions();
?>
<?php if ($Security->CanSearch()) { ?>
<?php if ($Prod->Export == "" && $Prod->CurrentAction == "") { ?>
<form name="fProdlistsrch" id="fProdlistsrch" class="form-inline ewForm" action="<?php echo ew_CurrentPage() ?>">
<?php $SearchPanelClass = ($Prod_list->SearchWhere <> "") ? " in" : " in"; ?>
<div id="fProdlistsrch_SearchPanel" class="ewSearchPanel collapse<?php echo $SearchPanelClass ?>">
<input type="hidden" name="cmd" value="search">
<input type="hidden" name="t" value="Prod">
	<div class="ewBasicSearch">
<div id="xsr_1" class="ewRow">
	<div class="ewQuickSearch input-group">
	<input type="text" name="<?php echo EW_TABLE_BASIC_SEARCH ?>" id="<?php echo EW_TABLE_BASIC_SEARCH ?>" class="form-control" value="<?php echo ew_HtmlEncode($Prod_list->BasicSearch->getKeyword()) ?>" placeholder="<?php echo ew_HtmlEncode($Language->Phrase("Search")) ?>">
	<input type="hidden" name="<?php echo EW_TABLE_BASIC_SEARCH_TYPE ?>" id="<?php echo EW_TABLE_BASIC_SEARCH_TYPE ?>" value="<?php echo ew_HtmlEncode($Prod_list->BasicSearch->getType()) ?>">
	<div class="input-group-btn">
		<button type="button" data-toggle="dropdown" class="btn btn-default"><span id="searchtype"><?php echo $Prod_list->BasicSearch->getTypeNameShort() ?></span><span class="caret"></span></button>
		<ul class="dropdown-menu pull-right" role="menu">
			<li<?php if ($Prod_list->BasicSearch->getType() == "") echo " class=\"active\""; ?>><a href="javascript:void(0);" onclick="ew_SetSearchType(this)"><?php echo $Language->Phrase("QuickSearchAuto") ?></a></li>
			<li<?php if ($Prod_list->BasicSearch->getType() == "=") echo " class=\"active\""; ?>><a href="javascript:void(0);" onclick="ew_SetSearchType(this,'=')"><?php echo $Language->Phrase("QuickSearchExact") ?></a></li>
			<li<?php if ($Prod_list->BasicSearch->getType() == "AND") echo " class=\"active\""; ?>><a href="javascript:void(0);" onclick="ew_SetSearchType(this,'AND')"><?php echo $Language->Phrase("QuickSearchAll") ?></a></li>
			<li<?php if ($Prod_list->BasicSearch->getType() == "OR") echo " class=\"active\""; ?>><a href="javascript:void(0);" onclick="ew_SetSearchType(this,'OR')"><?php echo $Language->Phrase("QuickSearchAny") ?></a></li>
		</ul>
	<button class="btn btn-primary ewButton" name="btnsubmit" id="btnsubmit" type="submit"><?php echo $Language->Phrase("QuickSearchBtn") ?></button>
	</div>
	</div>
</div>
	</div>
</div>
</form>
<?php } ?>
<?php } ?>
<?php $Prod_list->ShowPageHeader(); ?>
<?php
$Prod_list->ShowMessage();
?>
<?php if ($Prod_list->TotalRecs > 0 || $Prod->CurrentAction <> "") { ?>
<div class="panel panel-default ewGrid">
<form name="fProdlist" id="fProdlist" class="form-inline ewForm ewListForm" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($Prod_list->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $Prod_list->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="Prod">
<div id="gmp_Prod" class="<?php if (ew_IsResponsiveLayout()) { echo "table-responsive "; } ?>ewGridMiddlePanel">
<?php if ($Prod_list->TotalRecs > 0) { ?>
<table id="tbl_Prodlist" class="table ewTable">
<?php echo $Prod->TableCustomInnerHtml ?>
<thead><!-- Table header -->
	<tr class="ewTableHeader">
<?php

// Header row
$Prod_list->RowType = EW_ROWTYPE_HEADER;

// Render list options
$Prod_list->RenderListOptions();

// Render list options (header, left)
$Prod_list->ListOptions->Render("header", "left");
?>
<?php if ($Prod->CprCodi->Visible) { // CprCodi ?>
	<?php if ($Prod->SortUrl($Prod->CprCodi) == "") { ?>
		<th data-name="CprCodi"><div id="elh_Prod_CprCodi" class="Prod_CprCodi"><div class="ewTableHeaderCaption"><?php echo $Prod->CprCodi->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="CprCodi"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $Prod->SortUrl($Prod->CprCodi) ?>',2);"><div id="elh_Prod_CprCodi" class="Prod_CprCodi">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $Prod->CprCodi->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($Prod->CprCodi->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($Prod->CprCodi->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($Prod->MprCodi->Visible) { // MprCodi ?>
	<?php if ($Prod->SortUrl($Prod->MprCodi) == "") { ?>
		<th data-name="MprCodi"><div id="elh_Prod_MprCodi" class="Prod_MprCodi"><div class="ewTableHeaderCaption"><?php echo $Prod->MprCodi->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="MprCodi"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $Prod->SortUrl($Prod->MprCodi) ?>',2);"><div id="elh_Prod_MprCodi" class="Prod_MprCodi">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $Prod->MprCodi->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($Prod->MprCodi->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($Prod->MprCodi->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($Prod->TprCodi->Visible) { // TprCodi ?>
	<?php if ($Prod->SortUrl($Prod->TprCodi) == "") { ?>
		<th data-name="TprCodi"><div id="elh_Prod_TprCodi" class="Prod_TprCodi"><div class="ewTableHeaderCaption"><?php echo $Prod->TprCodi->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="TprCodi"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $Prod->SortUrl($Prod->TprCodi) ?>',2);"><div id="elh_Prod_TprCodi" class="Prod_TprCodi">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $Prod->TprCodi->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($Prod->TprCodi->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($Prod->TprCodi->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($Prod->ProCant->Visible) { // ProCant ?>
	<?php if ($Prod->SortUrl($Prod->ProCant) == "") { ?>
		<th data-name="ProCant"><div id="elh_Prod_ProCant" class="Prod_ProCant"><div class="ewTableHeaderCaption"><?php echo $Prod->ProCant->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="ProCant"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $Prod->SortUrl($Prod->ProCant) ?>',2);"><div id="elh_Prod_ProCant" class="Prod_ProCant">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $Prod->ProCant->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($Prod->ProCant->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($Prod->ProCant->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($Prod->ProNMin->Visible) { // ProNMin ?>
	<?php if ($Prod->SortUrl($Prod->ProNMin) == "") { ?>
		<th data-name="ProNMin"><div id="elh_Prod_ProNMin" class="Prod_ProNMin"><div class="ewTableHeaderCaption"><?php echo $Prod->ProNMin->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="ProNMin"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $Prod->SortUrl($Prod->ProNMin) ?>',2);"><div id="elh_Prod_ProNMin" class="Prod_ProNMin">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $Prod->ProNMin->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($Prod->ProNMin->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($Prod->ProNMin->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($Prod->ProPrec->Visible) { // ProPrec ?>
	<?php if ($Prod->SortUrl($Prod->ProPrec) == "") { ?>
		<th data-name="ProPrec"><div id="elh_Prod_ProPrec" class="Prod_ProPrec"><div class="ewTableHeaderCaption"><?php echo $Prod->ProPrec->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="ProPrec"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $Prod->SortUrl($Prod->ProPrec) ?>',2);"><div id="elh_Prod_ProPrec" class="Prod_ProPrec">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $Prod->ProPrec->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($Prod->ProPrec->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($Prod->ProPrec->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($Prod->ProDesc->Visible) { // ProDesc ?>
	<?php if ($Prod->SortUrl($Prod->ProDesc) == "") { ?>
		<th data-name="ProDesc"><div id="elh_Prod_ProDesc" class="Prod_ProDesc"><div class="ewTableHeaderCaption"><?php echo $Prod->ProDesc->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="ProDesc"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $Prod->SortUrl($Prod->ProDesc) ?>',2);"><div id="elh_Prod_ProDesc" class="Prod_ProDesc">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $Prod->ProDesc->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($Prod->ProDesc->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($Prod->ProDesc->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php

// Render list options (header, right)
$Prod_list->ListOptions->Render("header", "right");
?>
	</tr>
</thead>
<tbody>
<?php
if ($Prod->ExportAll && $Prod->Export <> "") {
	$Prod_list->StopRec = $Prod_list->TotalRecs;
} else {

	// Set the last record to display
	if ($Prod_list->TotalRecs > $Prod_list->StartRec + $Prod_list->DisplayRecs - 1)
		$Prod_list->StopRec = $Prod_list->StartRec + $Prod_list->DisplayRecs - 1;
	else
		$Prod_list->StopRec = $Prod_list->TotalRecs;
}

// Restore number of post back records
if ($objForm) {
	$objForm->Index = -1;
	if ($objForm->HasValue($Prod_list->FormKeyCountName) && ($Prod->CurrentAction == "gridadd" || $Prod->CurrentAction == "gridedit" || $Prod->CurrentAction == "F")) {
		$Prod_list->KeyCount = $objForm->GetValue($Prod_list->FormKeyCountName);
		$Prod_list->StopRec = $Prod_list->StartRec + $Prod_list->KeyCount - 1;
	}
}
$Prod_list->RecCnt = $Prod_list->StartRec - 1;
if ($Prod_list->Recordset && !$Prod_list->Recordset->EOF) {
	$Prod_list->Recordset->MoveFirst();
	$bSelectLimit = $Prod_list->UseSelectLimit;
	if (!$bSelectLimit && $Prod_list->StartRec > 1)
		$Prod_list->Recordset->Move($Prod_list->StartRec - 1);
} elseif (!$Prod->AllowAddDeleteRow && $Prod_list->StopRec == 0) {
	$Prod_list->StopRec = $Prod->GridAddRowCount;
}

// Initialize aggregate
$Prod->RowType = EW_ROWTYPE_AGGREGATEINIT;
$Prod->ResetAttrs();
$Prod_list->RenderRow();
if ($Prod->CurrentAction == "gridadd")
	$Prod_list->RowIndex = 0;
if ($Prod->CurrentAction == "gridedit")
	$Prod_list->RowIndex = 0;
while ($Prod_list->RecCnt < $Prod_list->StopRec) {
	$Prod_list->RecCnt++;
	if (intval($Prod_list->RecCnt) >= intval($Prod_list->StartRec)) {
		$Prod_list->RowCnt++;
		if ($Prod->CurrentAction == "gridadd" || $Prod->CurrentAction == "gridedit" || $Prod->CurrentAction == "F") {
			$Prod_list->RowIndex++;
			$objForm->Index = $Prod_list->RowIndex;
			if ($objForm->HasValue($Prod_list->FormActionName))
				$Prod_list->RowAction = strval($objForm->GetValue($Prod_list->FormActionName));
			elseif ($Prod->CurrentAction == "gridadd")
				$Prod_list->RowAction = "insert";
			else
				$Prod_list->RowAction = "";
		}

		// Set up key count
		$Prod_list->KeyCount = $Prod_list->RowIndex;

		// Init row class and style
		$Prod->ResetAttrs();
		$Prod->CssClass = "";
		if ($Prod->CurrentAction == "gridadd") {
			$Prod_list->LoadDefaultValues(); // Load default values
		} else {
			$Prod_list->LoadRowValues($Prod_list->Recordset); // Load row values
		}
		$Prod->RowType = EW_ROWTYPE_VIEW; // Render view
		if ($Prod->CurrentAction == "gridadd") // Grid add
			$Prod->RowType = EW_ROWTYPE_ADD; // Render add
		if ($Prod->CurrentAction == "gridadd" && $Prod->EventCancelled && !$objForm->HasValue("k_blankrow")) // Insert failed
			$Prod_list->RestoreCurrentRowFormValues($Prod_list->RowIndex); // Restore form values
		if ($Prod->CurrentAction == "gridedit") { // Grid edit
			if ($Prod->EventCancelled) {
				$Prod_list->RestoreCurrentRowFormValues($Prod_list->RowIndex); // Restore form values
			}
			if ($Prod_list->RowAction == "insert")
				$Prod->RowType = EW_ROWTYPE_ADD; // Render add
			else
				$Prod->RowType = EW_ROWTYPE_EDIT; // Render edit
		}
		if ($Prod->CurrentAction == "gridedit" && ($Prod->RowType == EW_ROWTYPE_EDIT || $Prod->RowType == EW_ROWTYPE_ADD) && $Prod->EventCancelled) // Update failed
			$Prod_list->RestoreCurrentRowFormValues($Prod_list->RowIndex); // Restore form values
		if ($Prod->RowType == EW_ROWTYPE_EDIT) // Edit row
			$Prod_list->EditRowCnt++;

		// Set up row id / data-rowindex
		$Prod->RowAttrs = array_merge($Prod->RowAttrs, array('data-rowindex'=>$Prod_list->RowCnt, 'id'=>'r' . $Prod_list->RowCnt . '_Prod', 'data-rowtype'=>$Prod->RowType));

		// Render row
		$Prod_list->RenderRow();

		// Render list options
		$Prod_list->RenderListOptions();

		// Skip delete row / empty row for confirm page
		if ($Prod_list->RowAction <> "delete" && $Prod_list->RowAction <> "insertdelete" && !($Prod_list->RowAction == "insert" && $Prod->CurrentAction == "F" && $Prod_list->EmptyRow())) {
?>
	<tr<?php echo $Prod->RowAttributes() ?>>
<?php

// Render list options (body, left)
$Prod_list->ListOptions->Render("body", "left", $Prod_list->RowCnt);
?>
	<?php if ($Prod->CprCodi->Visible) { // CprCodi ?>
		<td data-name="CprCodi"<?php echo $Prod->CprCodi->CellAttributes() ?>>
<?php if ($Prod->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $Prod_list->RowCnt ?>_Prod_CprCodi" class="form-group Prod_CprCodi">
<select data-table="Prod" data-field="x_CprCodi" data-value-separator="<?php echo ew_HtmlEncode(is_array($Prod->CprCodi->DisplayValueSeparator) ? json_encode($Prod->CprCodi->DisplayValueSeparator) : $Prod->CprCodi->DisplayValueSeparator) ?>" id="x<?php echo $Prod_list->RowIndex ?>_CprCodi" name="x<?php echo $Prod_list->RowIndex ?>_CprCodi"<?php echo $Prod->CprCodi->EditAttributes() ?>>
<?php
if (is_array($Prod->CprCodi->EditValue)) {
	$arwrk = $Prod->CprCodi->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($Prod->CprCodi->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $Prod->CprCodi->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($Prod->CprCodi->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($Prod->CprCodi->CurrentValue) ?>" selected><?php echo $Prod->CprCodi->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $Prod->CprCodi->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT \"CprCodi\", \"CprNomb\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"CPro\"";
$sWhereWrk = "";
$Prod->CprCodi->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$Prod->CprCodi->LookupFilters += array("f0" => "\"CprCodi\" = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$Prod->Lookup_Selecting($Prod->CprCodi, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $Prod->CprCodi->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $Prod_list->RowIndex ?>_CprCodi" id="s_x<?php echo $Prod_list->RowIndex ?>_CprCodi" value="<?php echo $Prod->CprCodi->LookupFilterQuery() ?>">
</span>
<input type="hidden" data-table="Prod" data-field="x_CprCodi" name="o<?php echo $Prod_list->RowIndex ?>_CprCodi" id="o<?php echo $Prod_list->RowIndex ?>_CprCodi" value="<?php echo ew_HtmlEncode($Prod->CprCodi->OldValue) ?>">
<?php } ?>
<?php if ($Prod->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $Prod_list->RowCnt ?>_Prod_CprCodi" class="form-group Prod_CprCodi">
<select data-table="Prod" data-field="x_CprCodi" data-value-separator="<?php echo ew_HtmlEncode(is_array($Prod->CprCodi->DisplayValueSeparator) ? json_encode($Prod->CprCodi->DisplayValueSeparator) : $Prod->CprCodi->DisplayValueSeparator) ?>" id="x<?php echo $Prod_list->RowIndex ?>_CprCodi" name="x<?php echo $Prod_list->RowIndex ?>_CprCodi"<?php echo $Prod->CprCodi->EditAttributes() ?>>
<?php
if (is_array($Prod->CprCodi->EditValue)) {
	$arwrk = $Prod->CprCodi->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($Prod->CprCodi->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $Prod->CprCodi->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($Prod->CprCodi->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($Prod->CprCodi->CurrentValue) ?>" selected><?php echo $Prod->CprCodi->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $Prod->CprCodi->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT \"CprCodi\", \"CprNomb\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"CPro\"";
$sWhereWrk = "";
$Prod->CprCodi->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$Prod->CprCodi->LookupFilters += array("f0" => "\"CprCodi\" = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$Prod->Lookup_Selecting($Prod->CprCodi, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $Prod->CprCodi->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $Prod_list->RowIndex ?>_CprCodi" id="s_x<?php echo $Prod_list->RowIndex ?>_CprCodi" value="<?php echo $Prod->CprCodi->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php if ($Prod->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $Prod_list->RowCnt ?>_Prod_CprCodi" class="Prod_CprCodi">
<span<?php echo $Prod->CprCodi->ViewAttributes() ?>>
<?php echo $Prod->CprCodi->ListViewValue() ?></span>
</span>
<?php } ?>
<a id="<?php echo $Prod_list->PageObjName . "_row_" . $Prod_list->RowCnt ?>"></a></td>
	<?php } ?>
<?php if ($Prod->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<input type="hidden" data-table="Prod" data-field="x_ProdCodi" name="x<?php echo $Prod_list->RowIndex ?>_ProdCodi" id="x<?php echo $Prod_list->RowIndex ?>_ProdCodi" value="<?php echo ew_HtmlEncode($Prod->ProdCodi->CurrentValue) ?>">
<input type="hidden" data-table="Prod" data-field="x_ProdCodi" name="o<?php echo $Prod_list->RowIndex ?>_ProdCodi" id="o<?php echo $Prod_list->RowIndex ?>_ProdCodi" value="<?php echo ew_HtmlEncode($Prod->ProdCodi->OldValue) ?>">
<?php } ?>
<?php if ($Prod->RowType == EW_ROWTYPE_EDIT || $Prod->CurrentMode == "edit") { ?>
<input type="hidden" data-table="Prod" data-field="x_ProdCodi" name="x<?php echo $Prod_list->RowIndex ?>_ProdCodi" id="x<?php echo $Prod_list->RowIndex ?>_ProdCodi" value="<?php echo ew_HtmlEncode($Prod->ProdCodi->CurrentValue) ?>">
<?php } ?>
	<?php if ($Prod->MprCodi->Visible) { // MprCodi ?>
		<td data-name="MprCodi"<?php echo $Prod->MprCodi->CellAttributes() ?>>
<?php if ($Prod->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $Prod_list->RowCnt ?>_Prod_MprCodi" class="form-group Prod_MprCodi">
<select data-table="Prod" data-field="x_MprCodi" data-value-separator="<?php echo ew_HtmlEncode(is_array($Prod->MprCodi->DisplayValueSeparator) ? json_encode($Prod->MprCodi->DisplayValueSeparator) : $Prod->MprCodi->DisplayValueSeparator) ?>" id="x<?php echo $Prod_list->RowIndex ?>_MprCodi" name="x<?php echo $Prod_list->RowIndex ?>_MprCodi"<?php echo $Prod->MprCodi->EditAttributes() ?>>
<?php
if (is_array($Prod->MprCodi->EditValue)) {
	$arwrk = $Prod->MprCodi->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($Prod->MprCodi->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $Prod->MprCodi->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($Prod->MprCodi->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($Prod->MprCodi->CurrentValue) ?>" selected><?php echo $Prod->MprCodi->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $Prod->MprCodi->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT \"MprCodi\", \"MprNomb\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"Mpro\"";
$sWhereWrk = "";
$Prod->MprCodi->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$Prod->MprCodi->LookupFilters += array("f0" => "\"MprCodi\" = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$Prod->Lookup_Selecting($Prod->MprCodi, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $Prod->MprCodi->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $Prod_list->RowIndex ?>_MprCodi" id="s_x<?php echo $Prod_list->RowIndex ?>_MprCodi" value="<?php echo $Prod->MprCodi->LookupFilterQuery() ?>">
</span>
<input type="hidden" data-table="Prod" data-field="x_MprCodi" name="o<?php echo $Prod_list->RowIndex ?>_MprCodi" id="o<?php echo $Prod_list->RowIndex ?>_MprCodi" value="<?php echo ew_HtmlEncode($Prod->MprCodi->OldValue) ?>">
<?php } ?>
<?php if ($Prod->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $Prod_list->RowCnt ?>_Prod_MprCodi" class="form-group Prod_MprCodi">
<select data-table="Prod" data-field="x_MprCodi" data-value-separator="<?php echo ew_HtmlEncode(is_array($Prod->MprCodi->DisplayValueSeparator) ? json_encode($Prod->MprCodi->DisplayValueSeparator) : $Prod->MprCodi->DisplayValueSeparator) ?>" id="x<?php echo $Prod_list->RowIndex ?>_MprCodi" name="x<?php echo $Prod_list->RowIndex ?>_MprCodi"<?php echo $Prod->MprCodi->EditAttributes() ?>>
<?php
if (is_array($Prod->MprCodi->EditValue)) {
	$arwrk = $Prod->MprCodi->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($Prod->MprCodi->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $Prod->MprCodi->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($Prod->MprCodi->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($Prod->MprCodi->CurrentValue) ?>" selected><?php echo $Prod->MprCodi->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $Prod->MprCodi->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT \"MprCodi\", \"MprNomb\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"Mpro\"";
$sWhereWrk = "";
$Prod->MprCodi->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$Prod->MprCodi->LookupFilters += array("f0" => "\"MprCodi\" = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$Prod->Lookup_Selecting($Prod->MprCodi, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $Prod->MprCodi->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $Prod_list->RowIndex ?>_MprCodi" id="s_x<?php echo $Prod_list->RowIndex ?>_MprCodi" value="<?php echo $Prod->MprCodi->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php if ($Prod->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $Prod_list->RowCnt ?>_Prod_MprCodi" class="Prod_MprCodi">
<span<?php echo $Prod->MprCodi->ViewAttributes() ?>>
<?php echo $Prod->MprCodi->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($Prod->TprCodi->Visible) { // TprCodi ?>
		<td data-name="TprCodi"<?php echo $Prod->TprCodi->CellAttributes() ?>>
<?php if ($Prod->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $Prod_list->RowCnt ?>_Prod_TprCodi" class="form-group Prod_TprCodi">
<select data-table="Prod" data-field="x_TprCodi" data-value-separator="<?php echo ew_HtmlEncode(is_array($Prod->TprCodi->DisplayValueSeparator) ? json_encode($Prod->TprCodi->DisplayValueSeparator) : $Prod->TprCodi->DisplayValueSeparator) ?>" id="x<?php echo $Prod_list->RowIndex ?>_TprCodi" name="x<?php echo $Prod_list->RowIndex ?>_TprCodi"<?php echo $Prod->TprCodi->EditAttributes() ?>>
<?php
if (is_array($Prod->TprCodi->EditValue)) {
	$arwrk = $Prod->TprCodi->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($Prod->TprCodi->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $Prod->TprCodi->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($Prod->TprCodi->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($Prod->TprCodi->CurrentValue) ?>" selected><?php echo $Prod->TprCodi->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $Prod->TprCodi->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT \"TprCodi\", \"TprNomb\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"TProd\"";
$sWhereWrk = "";
$Prod->TprCodi->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$Prod->TprCodi->LookupFilters += array("f0" => "\"TprCodi\" = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$Prod->Lookup_Selecting($Prod->TprCodi, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $Prod->TprCodi->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $Prod_list->RowIndex ?>_TprCodi" id="s_x<?php echo $Prod_list->RowIndex ?>_TprCodi" value="<?php echo $Prod->TprCodi->LookupFilterQuery() ?>">
</span>
<input type="hidden" data-table="Prod" data-field="x_TprCodi" name="o<?php echo $Prod_list->RowIndex ?>_TprCodi" id="o<?php echo $Prod_list->RowIndex ?>_TprCodi" value="<?php echo ew_HtmlEncode($Prod->TprCodi->OldValue) ?>">
<?php } ?>
<?php if ($Prod->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $Prod_list->RowCnt ?>_Prod_TprCodi" class="form-group Prod_TprCodi">
<select data-table="Prod" data-field="x_TprCodi" data-value-separator="<?php echo ew_HtmlEncode(is_array($Prod->TprCodi->DisplayValueSeparator) ? json_encode($Prod->TprCodi->DisplayValueSeparator) : $Prod->TprCodi->DisplayValueSeparator) ?>" id="x<?php echo $Prod_list->RowIndex ?>_TprCodi" name="x<?php echo $Prod_list->RowIndex ?>_TprCodi"<?php echo $Prod->TprCodi->EditAttributes() ?>>
<?php
if (is_array($Prod->TprCodi->EditValue)) {
	$arwrk = $Prod->TprCodi->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($Prod->TprCodi->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $Prod->TprCodi->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($Prod->TprCodi->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($Prod->TprCodi->CurrentValue) ?>" selected><?php echo $Prod->TprCodi->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $Prod->TprCodi->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT \"TprCodi\", \"TprNomb\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"TProd\"";
$sWhereWrk = "";
$Prod->TprCodi->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$Prod->TprCodi->LookupFilters += array("f0" => "\"TprCodi\" = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$Prod->Lookup_Selecting($Prod->TprCodi, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $Prod->TprCodi->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $Prod_list->RowIndex ?>_TprCodi" id="s_x<?php echo $Prod_list->RowIndex ?>_TprCodi" value="<?php echo $Prod->TprCodi->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php if ($Prod->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $Prod_list->RowCnt ?>_Prod_TprCodi" class="Prod_TprCodi">
<span<?php echo $Prod->TprCodi->ViewAttributes() ?>>
<?php echo $Prod->TprCodi->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($Prod->ProCant->Visible) { // ProCant ?>
		<td data-name="ProCant"<?php echo $Prod->ProCant->CellAttributes() ?>>
<?php if ($Prod->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $Prod_list->RowCnt ?>_Prod_ProCant" class="form-group Prod_ProCant">
<input type="text" data-table="Prod" data-field="x_ProCant" name="x<?php echo $Prod_list->RowIndex ?>_ProCant" id="x<?php echo $Prod_list->RowIndex ?>_ProCant" size="30" placeholder="<?php echo ew_HtmlEncode($Prod->ProCant->getPlaceHolder()) ?>" value="<?php echo $Prod->ProCant->EditValue ?>"<?php echo $Prod->ProCant->EditAttributes() ?>>
</span>
<input type="hidden" data-table="Prod" data-field="x_ProCant" name="o<?php echo $Prod_list->RowIndex ?>_ProCant" id="o<?php echo $Prod_list->RowIndex ?>_ProCant" value="<?php echo ew_HtmlEncode($Prod->ProCant->OldValue) ?>">
<?php } ?>
<?php if ($Prod->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $Prod_list->RowCnt ?>_Prod_ProCant" class="form-group Prod_ProCant">
<input type="text" data-table="Prod" data-field="x_ProCant" name="x<?php echo $Prod_list->RowIndex ?>_ProCant" id="x<?php echo $Prod_list->RowIndex ?>_ProCant" size="30" placeholder="<?php echo ew_HtmlEncode($Prod->ProCant->getPlaceHolder()) ?>" value="<?php echo $Prod->ProCant->EditValue ?>"<?php echo $Prod->ProCant->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($Prod->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $Prod_list->RowCnt ?>_Prod_ProCant" class="Prod_ProCant">
<span<?php echo $Prod->ProCant->ViewAttributes() ?>>
<?php echo $Prod->ProCant->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($Prod->ProNMin->Visible) { // ProNMin ?>
		<td data-name="ProNMin"<?php echo $Prod->ProNMin->CellAttributes() ?>>
<?php if ($Prod->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $Prod_list->RowCnt ?>_Prod_ProNMin" class="form-group Prod_ProNMin">
<input type="text" data-table="Prod" data-field="x_ProNMin" name="x<?php echo $Prod_list->RowIndex ?>_ProNMin" id="x<?php echo $Prod_list->RowIndex ?>_ProNMin" size="30" placeholder="<?php echo ew_HtmlEncode($Prod->ProNMin->getPlaceHolder()) ?>" value="<?php echo $Prod->ProNMin->EditValue ?>"<?php echo $Prod->ProNMin->EditAttributes() ?>>
</span>
<input type="hidden" data-table="Prod" data-field="x_ProNMin" name="o<?php echo $Prod_list->RowIndex ?>_ProNMin" id="o<?php echo $Prod_list->RowIndex ?>_ProNMin" value="<?php echo ew_HtmlEncode($Prod->ProNMin->OldValue) ?>">
<?php } ?>
<?php if ($Prod->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $Prod_list->RowCnt ?>_Prod_ProNMin" class="form-group Prod_ProNMin">
<input type="text" data-table="Prod" data-field="x_ProNMin" name="x<?php echo $Prod_list->RowIndex ?>_ProNMin" id="x<?php echo $Prod_list->RowIndex ?>_ProNMin" size="30" placeholder="<?php echo ew_HtmlEncode($Prod->ProNMin->getPlaceHolder()) ?>" value="<?php echo $Prod->ProNMin->EditValue ?>"<?php echo $Prod->ProNMin->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($Prod->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $Prod_list->RowCnt ?>_Prod_ProNMin" class="Prod_ProNMin">
<span<?php echo $Prod->ProNMin->ViewAttributes() ?>>
<?php echo $Prod->ProNMin->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($Prod->ProPrec->Visible) { // ProPrec ?>
		<td data-name="ProPrec"<?php echo $Prod->ProPrec->CellAttributes() ?>>
<?php if ($Prod->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $Prod_list->RowCnt ?>_Prod_ProPrec" class="form-group Prod_ProPrec">
<input type="text" data-table="Prod" data-field="x_ProPrec" name="x<?php echo $Prod_list->RowIndex ?>_ProPrec" id="x<?php echo $Prod_list->RowIndex ?>_ProPrec" size="30" placeholder="<?php echo ew_HtmlEncode($Prod->ProPrec->getPlaceHolder()) ?>" value="<?php echo $Prod->ProPrec->EditValue ?>"<?php echo $Prod->ProPrec->EditAttributes() ?>>
</span>
<input type="hidden" data-table="Prod" data-field="x_ProPrec" name="o<?php echo $Prod_list->RowIndex ?>_ProPrec" id="o<?php echo $Prod_list->RowIndex ?>_ProPrec" value="<?php echo ew_HtmlEncode($Prod->ProPrec->OldValue) ?>">
<?php } ?>
<?php if ($Prod->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $Prod_list->RowCnt ?>_Prod_ProPrec" class="form-group Prod_ProPrec">
<input type="text" data-table="Prod" data-field="x_ProPrec" name="x<?php echo $Prod_list->RowIndex ?>_ProPrec" id="x<?php echo $Prod_list->RowIndex ?>_ProPrec" size="30" placeholder="<?php echo ew_HtmlEncode($Prod->ProPrec->getPlaceHolder()) ?>" value="<?php echo $Prod->ProPrec->EditValue ?>"<?php echo $Prod->ProPrec->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($Prod->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $Prod_list->RowCnt ?>_Prod_ProPrec" class="Prod_ProPrec">
<span<?php echo $Prod->ProPrec->ViewAttributes() ?>>
<?php echo $Prod->ProPrec->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($Prod->ProDesc->Visible) { // ProDesc ?>
		<td data-name="ProDesc"<?php echo $Prod->ProDesc->CellAttributes() ?>>
<?php if ($Prod->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $Prod_list->RowCnt ?>_Prod_ProDesc" class="form-group Prod_ProDesc">
<input type="text" data-table="Prod" data-field="x_ProDesc" name="x<?php echo $Prod_list->RowIndex ?>_ProDesc" id="x<?php echo $Prod_list->RowIndex ?>_ProDesc" size="30" placeholder="<?php echo ew_HtmlEncode($Prod->ProDesc->getPlaceHolder()) ?>" value="<?php echo $Prod->ProDesc->EditValue ?>"<?php echo $Prod->ProDesc->EditAttributes() ?>>
</span>
<input type="hidden" data-table="Prod" data-field="x_ProDesc" name="o<?php echo $Prod_list->RowIndex ?>_ProDesc" id="o<?php echo $Prod_list->RowIndex ?>_ProDesc" value="<?php echo ew_HtmlEncode($Prod->ProDesc->OldValue) ?>">
<?php } ?>
<?php if ($Prod->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $Prod_list->RowCnt ?>_Prod_ProDesc" class="form-group Prod_ProDesc">
<input type="text" data-table="Prod" data-field="x_ProDesc" name="x<?php echo $Prod_list->RowIndex ?>_ProDesc" id="x<?php echo $Prod_list->RowIndex ?>_ProDesc" size="30" placeholder="<?php echo ew_HtmlEncode($Prod->ProDesc->getPlaceHolder()) ?>" value="<?php echo $Prod->ProDesc->EditValue ?>"<?php echo $Prod->ProDesc->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($Prod->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $Prod_list->RowCnt ?>_Prod_ProDesc" class="Prod_ProDesc">
<span<?php echo $Prod->ProDesc->ViewAttributes() ?>>
<?php echo $Prod->ProDesc->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
<?php

// Render list options (body, right)
$Prod_list->ListOptions->Render("body", "right", $Prod_list->RowCnt);
?>
	</tr>
<?php if ($Prod->RowType == EW_ROWTYPE_ADD || $Prod->RowType == EW_ROWTYPE_EDIT) { ?>
<script type="text/javascript">
fProdlist.UpdateOpts(<?php echo $Prod_list->RowIndex ?>);
</script>
<?php } ?>
<?php
	}
	} // End delete row checking
	if ($Prod->CurrentAction <> "gridadd")
		if (!$Prod_list->Recordset->EOF) $Prod_list->Recordset->MoveNext();
}
?>
<?php
	if ($Prod->CurrentAction == "gridadd" || $Prod->CurrentAction == "gridedit") {
		$Prod_list->RowIndex = '$rowindex$';
		$Prod_list->LoadDefaultValues();

		// Set row properties
		$Prod->ResetAttrs();
		$Prod->RowAttrs = array_merge($Prod->RowAttrs, array('data-rowindex'=>$Prod_list->RowIndex, 'id'=>'r0_Prod', 'data-rowtype'=>EW_ROWTYPE_ADD));
		ew_AppendClass($Prod->RowAttrs["class"], "ewTemplate");
		$Prod->RowType = EW_ROWTYPE_ADD;

		// Render row
		$Prod_list->RenderRow();

		// Render list options
		$Prod_list->RenderListOptions();
		$Prod_list->StartRowCnt = 0;
?>
	<tr<?php echo $Prod->RowAttributes() ?>>
<?php

// Render list options (body, left)
$Prod_list->ListOptions->Render("body", "left", $Prod_list->RowIndex);
?>
	<?php if ($Prod->CprCodi->Visible) { // CprCodi ?>
		<td data-name="CprCodi">
<span id="el$rowindex$_Prod_CprCodi" class="form-group Prod_CprCodi">
<select data-table="Prod" data-field="x_CprCodi" data-value-separator="<?php echo ew_HtmlEncode(is_array($Prod->CprCodi->DisplayValueSeparator) ? json_encode($Prod->CprCodi->DisplayValueSeparator) : $Prod->CprCodi->DisplayValueSeparator) ?>" id="x<?php echo $Prod_list->RowIndex ?>_CprCodi" name="x<?php echo $Prod_list->RowIndex ?>_CprCodi"<?php echo $Prod->CprCodi->EditAttributes() ?>>
<?php
if (is_array($Prod->CprCodi->EditValue)) {
	$arwrk = $Prod->CprCodi->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($Prod->CprCodi->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $Prod->CprCodi->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($Prod->CprCodi->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($Prod->CprCodi->CurrentValue) ?>" selected><?php echo $Prod->CprCodi->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $Prod->CprCodi->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT \"CprCodi\", \"CprNomb\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"CPro\"";
$sWhereWrk = "";
$Prod->CprCodi->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$Prod->CprCodi->LookupFilters += array("f0" => "\"CprCodi\" = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$Prod->Lookup_Selecting($Prod->CprCodi, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $Prod->CprCodi->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $Prod_list->RowIndex ?>_CprCodi" id="s_x<?php echo $Prod_list->RowIndex ?>_CprCodi" value="<?php echo $Prod->CprCodi->LookupFilterQuery() ?>">
</span>
<input type="hidden" data-table="Prod" data-field="x_CprCodi" name="o<?php echo $Prod_list->RowIndex ?>_CprCodi" id="o<?php echo $Prod_list->RowIndex ?>_CprCodi" value="<?php echo ew_HtmlEncode($Prod->CprCodi->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($Prod->MprCodi->Visible) { // MprCodi ?>
		<td data-name="MprCodi">
<span id="el$rowindex$_Prod_MprCodi" class="form-group Prod_MprCodi">
<select data-table="Prod" data-field="x_MprCodi" data-value-separator="<?php echo ew_HtmlEncode(is_array($Prod->MprCodi->DisplayValueSeparator) ? json_encode($Prod->MprCodi->DisplayValueSeparator) : $Prod->MprCodi->DisplayValueSeparator) ?>" id="x<?php echo $Prod_list->RowIndex ?>_MprCodi" name="x<?php echo $Prod_list->RowIndex ?>_MprCodi"<?php echo $Prod->MprCodi->EditAttributes() ?>>
<?php
if (is_array($Prod->MprCodi->EditValue)) {
	$arwrk = $Prod->MprCodi->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($Prod->MprCodi->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $Prod->MprCodi->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($Prod->MprCodi->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($Prod->MprCodi->CurrentValue) ?>" selected><?php echo $Prod->MprCodi->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $Prod->MprCodi->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT \"MprCodi\", \"MprNomb\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"Mpro\"";
$sWhereWrk = "";
$Prod->MprCodi->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$Prod->MprCodi->LookupFilters += array("f0" => "\"MprCodi\" = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$Prod->Lookup_Selecting($Prod->MprCodi, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $Prod->MprCodi->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $Prod_list->RowIndex ?>_MprCodi" id="s_x<?php echo $Prod_list->RowIndex ?>_MprCodi" value="<?php echo $Prod->MprCodi->LookupFilterQuery() ?>">
</span>
<input type="hidden" data-table="Prod" data-field="x_MprCodi" name="o<?php echo $Prod_list->RowIndex ?>_MprCodi" id="o<?php echo $Prod_list->RowIndex ?>_MprCodi" value="<?php echo ew_HtmlEncode($Prod->MprCodi->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($Prod->TprCodi->Visible) { // TprCodi ?>
		<td data-name="TprCodi">
<span id="el$rowindex$_Prod_TprCodi" class="form-group Prod_TprCodi">
<select data-table="Prod" data-field="x_TprCodi" data-value-separator="<?php echo ew_HtmlEncode(is_array($Prod->TprCodi->DisplayValueSeparator) ? json_encode($Prod->TprCodi->DisplayValueSeparator) : $Prod->TprCodi->DisplayValueSeparator) ?>" id="x<?php echo $Prod_list->RowIndex ?>_TprCodi" name="x<?php echo $Prod_list->RowIndex ?>_TprCodi"<?php echo $Prod->TprCodi->EditAttributes() ?>>
<?php
if (is_array($Prod->TprCodi->EditValue)) {
	$arwrk = $Prod->TprCodi->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($Prod->TprCodi->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $Prod->TprCodi->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($Prod->TprCodi->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($Prod->TprCodi->CurrentValue) ?>" selected><?php echo $Prod->TprCodi->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $Prod->TprCodi->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT \"TprCodi\", \"TprNomb\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"TProd\"";
$sWhereWrk = "";
$Prod->TprCodi->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$Prod->TprCodi->LookupFilters += array("f0" => "\"TprCodi\" = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$Prod->Lookup_Selecting($Prod->TprCodi, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $Prod->TprCodi->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $Prod_list->RowIndex ?>_TprCodi" id="s_x<?php echo $Prod_list->RowIndex ?>_TprCodi" value="<?php echo $Prod->TprCodi->LookupFilterQuery() ?>">
</span>
<input type="hidden" data-table="Prod" data-field="x_TprCodi" name="o<?php echo $Prod_list->RowIndex ?>_TprCodi" id="o<?php echo $Prod_list->RowIndex ?>_TprCodi" value="<?php echo ew_HtmlEncode($Prod->TprCodi->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($Prod->ProCant->Visible) { // ProCant ?>
		<td data-name="ProCant">
<span id="el$rowindex$_Prod_ProCant" class="form-group Prod_ProCant">
<input type="text" data-table="Prod" data-field="x_ProCant" name="x<?php echo $Prod_list->RowIndex ?>_ProCant" id="x<?php echo $Prod_list->RowIndex ?>_ProCant" size="30" placeholder="<?php echo ew_HtmlEncode($Prod->ProCant->getPlaceHolder()) ?>" value="<?php echo $Prod->ProCant->EditValue ?>"<?php echo $Prod->ProCant->EditAttributes() ?>>
</span>
<input type="hidden" data-table="Prod" data-field="x_ProCant" name="o<?php echo $Prod_list->RowIndex ?>_ProCant" id="o<?php echo $Prod_list->RowIndex ?>_ProCant" value="<?php echo ew_HtmlEncode($Prod->ProCant->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($Prod->ProNMin->Visible) { // ProNMin ?>
		<td data-name="ProNMin">
<span id="el$rowindex$_Prod_ProNMin" class="form-group Prod_ProNMin">
<input type="text" data-table="Prod" data-field="x_ProNMin" name="x<?php echo $Prod_list->RowIndex ?>_ProNMin" id="x<?php echo $Prod_list->RowIndex ?>_ProNMin" size="30" placeholder="<?php echo ew_HtmlEncode($Prod->ProNMin->getPlaceHolder()) ?>" value="<?php echo $Prod->ProNMin->EditValue ?>"<?php echo $Prod->ProNMin->EditAttributes() ?>>
</span>
<input type="hidden" data-table="Prod" data-field="x_ProNMin" name="o<?php echo $Prod_list->RowIndex ?>_ProNMin" id="o<?php echo $Prod_list->RowIndex ?>_ProNMin" value="<?php echo ew_HtmlEncode($Prod->ProNMin->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($Prod->ProPrec->Visible) { // ProPrec ?>
		<td data-name="ProPrec">
<span id="el$rowindex$_Prod_ProPrec" class="form-group Prod_ProPrec">
<input type="text" data-table="Prod" data-field="x_ProPrec" name="x<?php echo $Prod_list->RowIndex ?>_ProPrec" id="x<?php echo $Prod_list->RowIndex ?>_ProPrec" size="30" placeholder="<?php echo ew_HtmlEncode($Prod->ProPrec->getPlaceHolder()) ?>" value="<?php echo $Prod->ProPrec->EditValue ?>"<?php echo $Prod->ProPrec->EditAttributes() ?>>
</span>
<input type="hidden" data-table="Prod" data-field="x_ProPrec" name="o<?php echo $Prod_list->RowIndex ?>_ProPrec" id="o<?php echo $Prod_list->RowIndex ?>_ProPrec" value="<?php echo ew_HtmlEncode($Prod->ProPrec->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($Prod->ProDesc->Visible) { // ProDesc ?>
		<td data-name="ProDesc">
<span id="el$rowindex$_Prod_ProDesc" class="form-group Prod_ProDesc">
<input type="text" data-table="Prod" data-field="x_ProDesc" name="x<?php echo $Prod_list->RowIndex ?>_ProDesc" id="x<?php echo $Prod_list->RowIndex ?>_ProDesc" size="30" placeholder="<?php echo ew_HtmlEncode($Prod->ProDesc->getPlaceHolder()) ?>" value="<?php echo $Prod->ProDesc->EditValue ?>"<?php echo $Prod->ProDesc->EditAttributes() ?>>
</span>
<input type="hidden" data-table="Prod" data-field="x_ProDesc" name="o<?php echo $Prod_list->RowIndex ?>_ProDesc" id="o<?php echo $Prod_list->RowIndex ?>_ProDesc" value="<?php echo ew_HtmlEncode($Prod->ProDesc->OldValue) ?>">
</td>
	<?php } ?>
<?php

// Render list options (body, right)
$Prod_list->ListOptions->Render("body", "right", $Prod_list->RowCnt);
?>
<script type="text/javascript">
fProdlist.UpdateOpts(<?php echo $Prod_list->RowIndex ?>);
</script>
	</tr>
<?php
}
?>
</tbody>
</table>
<?php } ?>
<?php if ($Prod->CurrentAction == "gridadd") { ?>
<input type="hidden" name="a_list" id="a_list" value="gridinsert">
<input type="hidden" name="<?php echo $Prod_list->FormKeyCountName ?>" id="<?php echo $Prod_list->FormKeyCountName ?>" value="<?php echo $Prod_list->KeyCount ?>">
<?php echo $Prod_list->MultiSelectKey ?>
<?php } ?>
<?php if ($Prod->CurrentAction == "gridedit") { ?>
<input type="hidden" name="a_list" id="a_list" value="gridupdate">
<input type="hidden" name="<?php echo $Prod_list->FormKeyCountName ?>" id="<?php echo $Prod_list->FormKeyCountName ?>" value="<?php echo $Prod_list->KeyCount ?>">
<?php echo $Prod_list->MultiSelectKey ?>
<?php } ?>
<?php if ($Prod->CurrentAction == "") { ?>
<input type="hidden" name="a_list" id="a_list" value="">
<?php } ?>
</div>
</form>
<?php

// Close recordset
if ($Prod_list->Recordset)
	$Prod_list->Recordset->Close();
?>
<div class="panel-footer ewGridLowerPanel">
<?php if ($Prod->CurrentAction <> "gridadd" && $Prod->CurrentAction <> "gridedit") { ?>
<form name="ewPagerForm" class="ewForm form-inline ewPagerForm" action="<?php echo ew_CurrentPage() ?>">
<?php if (!isset($Prod_list->Pager)) $Prod_list->Pager = new cPrevNextPager($Prod_list->StartRec, $Prod_list->DisplayRecs, $Prod_list->TotalRecs) ?>
<?php if ($Prod_list->Pager->RecordCount > 0) { ?>
<div class="ewPager">
<span><?php echo $Language->Phrase("Page") ?>&nbsp;</span>
<div class="ewPrevNext"><div class="input-group">
<div class="input-group-btn">
<!--first page button-->
	<?php if ($Prod_list->Pager->FirstButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerFirst") ?>" href="<?php echo $Prod_list->PageUrl() ?>start=<?php echo $Prod_list->Pager->FirstButton->Start ?>"><span class="icon-first ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerFirst") ?>"><span class="icon-first ewIcon"></span></a>
	<?php } ?>
<!--previous page button-->
	<?php if ($Prod_list->Pager->PrevButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerPrevious") ?>" href="<?php echo $Prod_list->PageUrl() ?>start=<?php echo $Prod_list->Pager->PrevButton->Start ?>"><span class="icon-prev ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerPrevious") ?>"><span class="icon-prev ewIcon"></span></a>
	<?php } ?>
</div>
<!--current page number-->
	<input class="form-control input-sm" type="text" name="<?php echo EW_TABLE_PAGE_NO ?>" value="<?php echo $Prod_list->Pager->CurrentPage ?>">
<div class="input-group-btn">
<!--next page button-->
	<?php if ($Prod_list->Pager->NextButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerNext") ?>" href="<?php echo $Prod_list->PageUrl() ?>start=<?php echo $Prod_list->Pager->NextButton->Start ?>"><span class="icon-next ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerNext") ?>"><span class="icon-next ewIcon"></span></a>
	<?php } ?>
<!--last page button-->
	<?php if ($Prod_list->Pager->LastButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerLast") ?>" href="<?php echo $Prod_list->PageUrl() ?>start=<?php echo $Prod_list->Pager->LastButton->Start ?>"><span class="icon-last ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerLast") ?>"><span class="icon-last ewIcon"></span></a>
	<?php } ?>
</div>
</div>
</div>
<span>&nbsp;<?php echo $Language->Phrase("of") ?>&nbsp;<?php echo $Prod_list->Pager->PageCount ?></span>
</div>
<div class="ewPager ewRec">
	<span><?php echo $Language->Phrase("Record") ?>&nbsp;<?php echo $Prod_list->Pager->FromIndex ?>&nbsp;<?php echo $Language->Phrase("To") ?>&nbsp;<?php echo $Prod_list->Pager->ToIndex ?>&nbsp;<?php echo $Language->Phrase("Of") ?>&nbsp;<?php echo $Prod_list->Pager->RecordCount ?></span>
</div>
<?php } ?>
</form>
<?php } ?>
<div class="ewListOtherOptions">
<?php
	foreach ($Prod_list->OtherOptions as &$option)
		$option->Render("body", "bottom");
?>
</div>
<div class="clearfix"></div>
</div>
</div>
<?php } ?>
<?php if ($Prod_list->TotalRecs == 0 && $Prod->CurrentAction == "") { // Show other options ?>
<div class="ewListOtherOptions">
<?php
	foreach ($Prod_list->OtherOptions as &$option) {
		$option->ButtonClass = "";
		$option->Render("body", "");
	}
?>
</div>
<div class="clearfix"></div>
<?php } ?>
<script type="text/javascript">
fProdlistsrch.Init();
fProdlistsrch.FilterList = <?php echo $Prod_list->GetFilterList() ?>;
fProdlist.Init();
</script>
<?php
$Prod_list->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$Prod_list->Page_Terminate();
?>
