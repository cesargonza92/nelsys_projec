<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "OCominfo.php" ?>
<?php include_once "Usuainfo.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$OCom_add = NULL; // Initialize page object first

class cOCom_add extends cOCom {

	// Page ID
	var $PageID = 'add';

	// Project ID
	var $ProjectID = "{04439FF7-B43F-460F-8514-F71C8FF9E679}";

	// Table name
	var $TableName = 'OCom';

	// Page object name
	var $PageObjName = 'OCom_add';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (OCom)
		if (!isset($GLOBALS["OCom"]) || get_class($GLOBALS["OCom"]) == "cOCom") {
			$GLOBALS["OCom"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["OCom"];
		}

		// Table object (Usua)
		if (!isset($GLOBALS['Usua'])) $GLOBALS['Usua'] = new cUsua();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'add', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'OCom', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (Usua)
		if (!isset($UserTable)) {
			$UserTable = new cUsua();
			$UserTableConn = Conn($UserTable->DBID);
		}
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanAdd()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage($Language->Phrase("NoPermission")); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("OComlist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}

		// Create form object
		$objForm = new cFormObj();
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Process auto fill
		if (@$_POST["ajax"] == "autofill") {
			$results = $this->GetAutoFill(@$_POST["name"], @$_POST["q"]);
			if ($results) {

				// Clean output buffer
				if (!EW_DEBUG_ENABLED && ob_get_length())
					ob_end_clean();
				echo $results;
				$this->Page_Terminate();
				exit();
			}
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $OCom;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($OCom);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $FormClassName = "form-horizontal ewForm ewAddForm";
	var $DbMasterFilter = "";
	var $DbDetailFilter = "";
	var $StartRec;
	var $Priv = 0;
	var $OldRecordset;
	var $CopyRecord;

	// 
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError;

		// Process form if post back
		if (@$_POST["a_add"] <> "") {
			$this->CurrentAction = $_POST["a_add"]; // Get form action
			$this->CopyRecord = $this->LoadOldRecord(); // Load old recordset
			$this->LoadFormValues(); // Load form values
		} else { // Not post back

			// Load key values from QueryString
			$this->CopyRecord = TRUE;
			if (@$_GET["OcoCodi"] != "") {
				$this->OcoCodi->setQueryStringValue($_GET["OcoCodi"]);
				$this->setKey("OcoCodi", $this->OcoCodi->CurrentValue); // Set up key
			} else {
				$this->setKey("OcoCodi", ""); // Clear key
				$this->CopyRecord = FALSE;
			}
			if ($this->CopyRecord) {
				$this->CurrentAction = "C"; // Copy record
			} else {
				$this->CurrentAction = "I"; // Display blank record
				$this->LoadDefaultValues(); // Load default values
			}
		}

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Validate form if post back
		if (@$_POST["a_add"] <> "") {
			if (!$this->ValidateForm()) {
				$this->CurrentAction = "I"; // Form error, reset action
				$this->EventCancelled = TRUE; // Event cancelled
				$this->RestoreFormValues(); // Restore form values
				$this->setFailureMessage($gsFormError);
			}
		}

		// Perform action based on action code
		switch ($this->CurrentAction) {
			case "I": // Blank record, no action required
				break;
			case "C": // Copy an existing record
				if (!$this->LoadRow()) { // Load record based on key
					if ($this->getFailureMessage() == "") $this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
					$this->Page_Terminate("OComlist.php"); // No matching record, return to list
				}
				break;
			case "A": // Add new record
				$this->SendEmail = TRUE; // Send email on add success
				if ($this->AddRow($this->OldRecordset)) { // Add successful
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage($Language->Phrase("AddSuccess")); // Set up success message
					$sReturnUrl = $this->getReturnUrl();
					if (ew_GetPageName($sReturnUrl) == "OComview.php")
						$sReturnUrl = $this->GetViewUrl(); // View paging, return to view page with keyurl directly
					$this->Page_Terminate($sReturnUrl); // Clean up and return
				} else {
					$this->EventCancelled = TRUE; // Event cancelled
					$this->RestoreFormValues(); // Add failed, restore form values
				}
		}

		// Render row based on row type
		if ($this->CurrentAction == "F") { // Confirm page
			$this->RowType = EW_ROWTYPE_VIEW; // Render view type
		} else {
			$this->RowType = EW_ROWTYPE_ADD; // Render add type
		}

		// Render row
		$this->ResetAttrs();
		$this->RenderRow();
	}

	// Get upload files
	function GetUploadFiles() {
		global $objForm, $Language;

		// Get upload data
		$this->OcoLPed->Upload->Index = $objForm->Index;
		$this->OcoLPed->Upload->UploadFile();
		$this->OcoProf->Upload->Index = $objForm->Index;
		$this->OcoProf->Upload->UploadFile();
	}

	// Load default values
	function LoadDefaultValues() {
		$this->PrvCodi->CurrentValue = NULL;
		$this->PrvCodi->OldValue = $this->PrvCodi->CurrentValue;
		$this->OcoLPed->Upload->DbValue = NULL;
		$this->OcoLPed->OldValue = $this->OcoLPed->Upload->DbValue;
		$this->OcoFCom->CurrentValue = NULL;
		$this->OcoFCom->OldValue = $this->OcoFCom->CurrentValue;
		$this->OcoProf->Upload->DbValue = NULL;
		$this->OcoProf->OldValue = $this->OcoProf->Upload->DbValue;
		$this->OcoEsta->CurrentValue = NULL;
		$this->OcoEsta->OldValue = $this->OcoEsta->CurrentValue;
	}

	// Load form values
	function LoadFormValues() {

		// Load from form
		global $objForm;
		$this->GetUploadFiles(); // Get upload files
		if (!$this->PrvCodi->FldIsDetailKey) {
			$this->PrvCodi->setFormValue($objForm->GetValue("x_PrvCodi"));
		}
		if (!$this->OcoFCom->FldIsDetailKey) {
			$this->OcoFCom->setFormValue($objForm->GetValue("x_OcoFCom"));
			$this->OcoFCom->CurrentValue = ew_UnFormatDateTime($this->OcoFCom->CurrentValue, 7);
		}
		if (!$this->OcoEsta->FldIsDetailKey) {
			$this->OcoEsta->setFormValue($objForm->GetValue("x_OcoEsta"));
		}
	}

	// Restore form values
	function RestoreFormValues() {
		global $objForm;
		$this->LoadOldRecord();
		$this->PrvCodi->CurrentValue = $this->PrvCodi->FormValue;
		$this->OcoFCom->CurrentValue = $this->OcoFCom->FormValue;
		$this->OcoFCom->CurrentValue = ew_UnFormatDateTime($this->OcoFCom->CurrentValue, 7);
		$this->OcoEsta->CurrentValue = $this->OcoEsta->FormValue;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->OcoCodi->setDbValue($rs->fields('OcoCodi'));
		$this->PrvCodi->setDbValue($rs->fields('PrvCodi'));
		$this->OcoLPed->Upload->DbValue = $rs->fields('OcoLPed');
		if (is_array($this->OcoLPed->Upload->DbValue) || is_object($this->OcoLPed->Upload->DbValue)) // Byte array
			$this->OcoLPed->Upload->DbValue = ew_BytesToStr($this->OcoLPed->Upload->DbValue);
		$this->OcoFCom->setDbValue($rs->fields('OcoFCom'));
		$this->OcoProf->Upload->DbValue = $rs->fields('OcoProf');
		if (is_array($this->OcoProf->Upload->DbValue) || is_object($this->OcoProf->Upload->DbValue)) // Byte array
			$this->OcoProf->Upload->DbValue = ew_BytesToStr($this->OcoProf->Upload->DbValue);
		$this->OcoEsta->setDbValue($rs->fields('OcoEsta'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->OcoCodi->DbValue = $row['OcoCodi'];
		$this->PrvCodi->DbValue = $row['PrvCodi'];
		$this->OcoLPed->Upload->DbValue = $row['OcoLPed'];
		$this->OcoFCom->DbValue = $row['OcoFCom'];
		$this->OcoProf->Upload->DbValue = $row['OcoProf'];
		$this->OcoEsta->DbValue = $row['OcoEsta'];
	}

	// Load old record
	function LoadOldRecord() {

		// Load key values from Session
		$bValidKey = TRUE;
		if (strval($this->getKey("OcoCodi")) <> "")
			$this->OcoCodi->CurrentValue = $this->getKey("OcoCodi"); // OcoCodi
		else
			$bValidKey = FALSE;

		// Load old recordset
		if ($bValidKey) {
			$this->CurrentFilter = $this->KeyFilter();
			$sSql = $this->SQL();
			$conn = &$this->Connection();
			$this->OldRecordset = ew_LoadRecordset($sSql, $conn);
			$this->LoadRowValues($this->OldRecordset); // Load row values
		} else {
			$this->OldRecordset = NULL;
		}
		return $bValidKey;
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		// Call Row_Rendering event

		$this->Row_Rendering();

		// Common render codes for all row types
		// OcoCodi
		// PrvCodi
		// OcoLPed
		// OcoFCom
		// OcoProf
		// OcoEsta

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// OcoCodi
		$this->OcoCodi->ViewValue = $this->OcoCodi->CurrentValue;
		$this->OcoCodi->ViewCustomAttributes = "";

		// PrvCodi
		if (strval($this->PrvCodi->CurrentValue) <> "") {
			$sFilterWrk = "\"PrvCodi\"" . ew_SearchString("=", $this->PrvCodi->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT \"PrvCodi\", \"PrvRSoc\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"Prov\"";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->PrvCodi, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->PrvCodi->ViewValue = $this->PrvCodi->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->PrvCodi->ViewValue = $this->PrvCodi->CurrentValue;
			}
		} else {
			$this->PrvCodi->ViewValue = NULL;
		}
		$this->PrvCodi->ViewCustomAttributes = "";

		// OcoLPed
		if (!ew_Empty($this->OcoLPed->Upload->DbValue)) {
			$this->OcoLPed->ViewValue = "OCom_OcoLPed_bv.php?" . "OcoCodi=" . $this->OcoCodi->CurrentValue;
			$this->OcoLPed->IsBlobImage = ew_IsImageFile("image" . ew_ContentExt(substr($this->OcoLPed->Upload->DbValue, 0, 11)));
		} else {
			$this->OcoLPed->ViewValue = "";
		}
		$this->OcoLPed->ViewCustomAttributes = "";

		// OcoFCom
		$this->OcoFCom->ViewValue = $this->OcoFCom->CurrentValue;
		$this->OcoFCom->ViewValue = ew_FormatDateTime($this->OcoFCom->ViewValue, 7);
		$this->OcoFCom->ViewCustomAttributes = "";

		// OcoProf
		if (!ew_Empty($this->OcoProf->Upload->DbValue)) {
			$this->OcoProf->ViewValue = "OCom_OcoProf_bv.php?" . "OcoCodi=" . $this->OcoCodi->CurrentValue;
			$this->OcoProf->IsBlobImage = ew_IsImageFile("image" . ew_ContentExt(substr($this->OcoProf->Upload->DbValue, 0, 11)));
		} else {
			$this->OcoProf->ViewValue = "";
		}
		$this->OcoProf->ViewCustomAttributes = "";

		// OcoEsta
		$this->OcoEsta->ViewValue = $this->OcoEsta->CurrentValue;
		$this->OcoEsta->ViewCustomAttributes = "";

			// PrvCodi
			$this->PrvCodi->LinkCustomAttributes = "";
			$this->PrvCodi->HrefValue = "";
			$this->PrvCodi->TooltipValue = "";

			// OcoLPed
			$this->OcoLPed->LinkCustomAttributes = "";
			if (!empty($this->OcoLPed->Upload->DbValue)) {
				$this->OcoLPed->HrefValue = "OCom_OcoLPed_bv.php?OcoCodi=" . $this->OcoCodi->CurrentValue;
				$this->OcoLPed->LinkAttrs["target"] = "_blank";
				if ($this->Export <> "") $this->OcoLPed->HrefValue = ew_ConvertFullUrl($this->OcoLPed->HrefValue);
			} else {
				$this->OcoLPed->HrefValue = "";
			}
			$this->OcoLPed->HrefValue2 = "OCom_OcoLPed_bv.php?OcoCodi=" . $this->OcoCodi->CurrentValue;
			$this->OcoLPed->TooltipValue = "";

			// OcoFCom
			$this->OcoFCom->LinkCustomAttributes = "";
			$this->OcoFCom->HrefValue = "";
			$this->OcoFCom->TooltipValue = "";

			// OcoProf
			$this->OcoProf->LinkCustomAttributes = "";
			if (!empty($this->OcoProf->Upload->DbValue)) {
				$this->OcoProf->HrefValue = "OCom_OcoProf_bv.php?OcoCodi=" . $this->OcoCodi->CurrentValue;
				$this->OcoProf->LinkAttrs["target"] = "_blank";
				if ($this->Export <> "") $this->OcoProf->HrefValue = ew_ConvertFullUrl($this->OcoProf->HrefValue);
			} else {
				$this->OcoProf->HrefValue = "";
			}
			$this->OcoProf->HrefValue2 = "OCom_OcoProf_bv.php?OcoCodi=" . $this->OcoCodi->CurrentValue;
			$this->OcoProf->TooltipValue = "";

			// OcoEsta
			$this->OcoEsta->LinkCustomAttributes = "";
			$this->OcoEsta->HrefValue = "";
			$this->OcoEsta->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_ADD) { // Add row

			// PrvCodi
			$this->PrvCodi->EditAttrs["class"] = "form-control";
			$this->PrvCodi->EditCustomAttributes = "";
			if (trim(strval($this->PrvCodi->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "\"PrvCodi\"" . ew_SearchString("=", $this->PrvCodi->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT \"PrvCodi\", \"PrvRSoc\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\", '' AS \"SelectFilterFld\", '' AS \"SelectFilterFld2\", '' AS \"SelectFilterFld3\", '' AS \"SelectFilterFld4\" FROM \"public\".\"Prov\"";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->PrvCodi, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->PrvCodi->EditValue = $arwrk;

			// OcoLPed
			$this->OcoLPed->EditAttrs["class"] = "form-control";
			$this->OcoLPed->EditCustomAttributes = "";
			if (!ew_Empty($this->OcoLPed->Upload->DbValue)) {
				$this->OcoLPed->EditValue = "OCom_OcoLPed_bv.php?" . "OcoCodi=" . $this->OcoCodi->CurrentValue;
				$this->OcoLPed->IsBlobImage = ew_IsImageFile("image" . ew_ContentExt(substr($this->OcoLPed->Upload->DbValue, 0, 11)));
			} else {
				$this->OcoLPed->EditValue = "";
			}
			if (($this->CurrentAction == "I" || $this->CurrentAction == "C") && !$this->EventCancelled) ew_RenderUploadField($this->OcoLPed);

			// OcoFCom
			$this->OcoFCom->EditAttrs["class"] = "form-control";
			$this->OcoFCom->EditCustomAttributes = "";
			$this->OcoFCom->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->OcoFCom->CurrentValue, 7));
			$this->OcoFCom->PlaceHolder = ew_RemoveHtml($this->OcoFCom->FldCaption());

			// OcoProf
			$this->OcoProf->EditAttrs["class"] = "form-control";
			$this->OcoProf->EditCustomAttributes = "";
			if (!ew_Empty($this->OcoProf->Upload->DbValue)) {
				$this->OcoProf->EditValue = "OCom_OcoProf_bv.php?" . "OcoCodi=" . $this->OcoCodi->CurrentValue;
				$this->OcoProf->IsBlobImage = ew_IsImageFile("image" . ew_ContentExt(substr($this->OcoProf->Upload->DbValue, 0, 11)));
			} else {
				$this->OcoProf->EditValue = "";
			}
			if (($this->CurrentAction == "I" || $this->CurrentAction == "C") && !$this->EventCancelled) ew_RenderUploadField($this->OcoProf);

			// OcoEsta
			$this->OcoEsta->EditAttrs["class"] = "form-control";
			$this->OcoEsta->EditCustomAttributes = "";
			$this->OcoEsta->EditValue = ew_HtmlEncode($this->OcoEsta->CurrentValue);
			$this->OcoEsta->PlaceHolder = ew_RemoveHtml($this->OcoEsta->FldCaption());

			// Edit refer script
			// PrvCodi

			$this->PrvCodi->HrefValue = "";

			// OcoLPed
			if (!empty($this->OcoLPed->Upload->DbValue)) {
				$this->OcoLPed->HrefValue = "OCom_OcoLPed_bv.php?OcoCodi=" . $this->OcoCodi->CurrentValue;
				$this->OcoLPed->LinkAttrs["target"] = "_blank";
				if ($this->Export <> "") $this->OcoLPed->HrefValue = ew_ConvertFullUrl($this->OcoLPed->HrefValue);
			} else {
				$this->OcoLPed->HrefValue = "";
			}
			$this->OcoLPed->HrefValue2 = "OCom_OcoLPed_bv.php?OcoCodi=" . $this->OcoCodi->CurrentValue;

			// OcoFCom
			$this->OcoFCom->HrefValue = "";

			// OcoProf
			if (!empty($this->OcoProf->Upload->DbValue)) {
				$this->OcoProf->HrefValue = "OCom_OcoProf_bv.php?OcoCodi=" . $this->OcoCodi->CurrentValue;
				$this->OcoProf->LinkAttrs["target"] = "_blank";
				if ($this->Export <> "") $this->OcoProf->HrefValue = ew_ConvertFullUrl($this->OcoProf->HrefValue);
			} else {
				$this->OcoProf->HrefValue = "";
			}
			$this->OcoProf->HrefValue2 = "OCom_OcoProf_bv.php?OcoCodi=" . $this->OcoCodi->CurrentValue;

			// OcoEsta
			$this->OcoEsta->HrefValue = "";
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate form
	function ValidateForm() {
		global $Language, $gsFormError;

		// Initialize form error message
		$gsFormError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return ($gsFormError == "");
		if (!$this->PrvCodi->FldIsDetailKey && !is_null($this->PrvCodi->FormValue) && $this->PrvCodi->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->PrvCodi->FldCaption(), $this->PrvCodi->ReqErrMsg));
		}
		if ($this->OcoLPed->Upload->FileName == "" && !$this->OcoLPed->Upload->KeepFile) {
			ew_AddMessage($gsFormError, str_replace("%s", $this->OcoLPed->FldCaption(), $this->OcoLPed->ReqErrMsg));
		}
		if (!$this->OcoFCom->FldIsDetailKey && !is_null($this->OcoFCom->FormValue) && $this->OcoFCom->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->OcoFCom->FldCaption(), $this->OcoFCom->ReqErrMsg));
		}
		if (!ew_CheckEuroDate($this->OcoFCom->FormValue)) {
			ew_AddMessage($gsFormError, $this->OcoFCom->FldErrMsg());
		}
		if ($this->OcoProf->Upload->FileName == "" && !$this->OcoProf->Upload->KeepFile) {
			ew_AddMessage($gsFormError, str_replace("%s", $this->OcoProf->FldCaption(), $this->OcoProf->ReqErrMsg));
		}
		if (!$this->OcoEsta->FldIsDetailKey && !is_null($this->OcoEsta->FormValue) && $this->OcoEsta->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->OcoEsta->FldCaption(), $this->OcoEsta->ReqErrMsg));
		}

		// Return validate result
		$ValidateForm = ($gsFormError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateForm = $ValidateForm && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsFormError, $sFormCustomError);
		}
		return $ValidateForm;
	}

	// Add record
	function AddRow($rsold = NULL) {
		global $Language, $Security;
		$conn = &$this->Connection();

		// Load db values from rsold
		if ($rsold) {
			$this->LoadDbValues($rsold);
		}
		$rsnew = array();

		// PrvCodi
		$this->PrvCodi->SetDbValueDef($rsnew, $this->PrvCodi->CurrentValue, 0, FALSE);

		// OcoLPed
		if (!$this->OcoLPed->Upload->KeepFile) {
			if (is_null($this->OcoLPed->Upload->Value)) {
				$rsnew['OcoLPed'] = NULL;
			} else {
				$rsnew['OcoLPed'] = $this->OcoLPed->Upload->Value;
			}
		}

		// OcoFCom
		$this->OcoFCom->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->OcoFCom->CurrentValue, 7), ew_CurrentDate(), FALSE);

		// OcoProf
		if (!$this->OcoProf->Upload->KeepFile) {
			if (is_null($this->OcoProf->Upload->Value)) {
				$rsnew['OcoProf'] = NULL;
			} else {
				$rsnew['OcoProf'] = $this->OcoProf->Upload->Value;
			}
		}

		// OcoEsta
		$this->OcoEsta->SetDbValueDef($rsnew, $this->OcoEsta->CurrentValue, "", FALSE);

		// Call Row Inserting event
		$rs = ($rsold == NULL) ? NULL : $rsold->fields;
		$bInsertRow = $this->Row_Inserting($rs, $rsnew);
		if ($bInsertRow) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			$AddRow = $this->Insert($rsnew);
			$conn->raiseErrorFn = '';
			if ($AddRow) {

				// Get insert id if necessary
				$this->OcoCodi->setDbValue($conn->GetOne("SELECT currval('\"OCom_OcoCodi_seq\"'::regclass)"));
				$rsnew['OcoCodi'] = $this->OcoCodi->DbValue;
				if (!$this->OcoLPed->Upload->KeepFile) {
				}
				if (!$this->OcoProf->Upload->KeepFile) {
				}
			}
		} else {
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("InsertCancelled"));
			}
			$AddRow = FALSE;
		}
		if ($AddRow) {

			// Call Row Inserted event
			$rs = ($rsold == NULL) ? NULL : $rsold->fields;
			$this->Row_Inserted($rs, $rsnew);
		}

		// OcoLPed
		ew_CleanUploadTempPath($this->OcoLPed, $this->OcoLPed->Upload->Index);

		// OcoProf
		ew_CleanUploadTempPath($this->OcoProf, $this->OcoProf->Upload->Index);
		return $AddRow;
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, "OComlist.php", "", $this->TableVar, TRUE);
		$PageId = ($this->CurrentAction == "C") ? "Copy" : "Add";
		$Breadcrumb->Add("add", $PageId, $url);
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($OCom_add)) $OCom_add = new cOCom_add();

// Page init
$OCom_add->Page_Init();

// Page main
$OCom_add->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$OCom_add->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "add";
var CurrentForm = fOComadd = new ew_Form("fOComadd", "add");

// Validate form
fOComadd.Validate = function() {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.GetForm(), $fobj = $(fobj);
	if ($fobj.find("#a_confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.FormKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = $fobj.find("#a_list").val() == "gridinsert";
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
			elm = this.GetElements("x" + infix + "_PrvCodi");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $OCom->PrvCodi->FldCaption(), $OCom->PrvCodi->ReqErrMsg)) ?>");
			felm = this.GetElements("x" + infix + "_OcoLPed");
			elm = this.GetElements("fn_x" + infix + "_OcoLPed");
			if (felm && elm && !ew_HasValue(elm))
				return this.OnError(felm, "<?php echo ew_JsEncode2(str_replace("%s", $OCom->OcoLPed->FldCaption(), $OCom->OcoLPed->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_OcoFCom");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $OCom->OcoFCom->FldCaption(), $OCom->OcoFCom->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_OcoFCom");
			if (elm && !ew_CheckEuroDate(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($OCom->OcoFCom->FldErrMsg()) ?>");
			felm = this.GetElements("x" + infix + "_OcoProf");
			elm = this.GetElements("fn_x" + infix + "_OcoProf");
			if (felm && elm && !ew_HasValue(elm))
				return this.OnError(felm, "<?php echo ew_JsEncode2(str_replace("%s", $OCom->OcoProf->FldCaption(), $OCom->OcoProf->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_OcoEsta");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $OCom->OcoEsta->FldCaption(), $OCom->OcoEsta->ReqErrMsg)) ?>");

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
	}

	// Process detail forms
	var dfs = $fobj.find("input[name='detailpage']").get();
	for (var i = 0; i < dfs.length; i++) {
		var df = dfs[i], val = df.value;
		if (val && ewForms[val])
			if (!ewForms[val].Validate())
				return false;
	}
	return true;
}

// Form_CustomValidate event
fOComadd.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fOComadd.ValidateRequired = true;
<?php } else { ?>
fOComadd.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
fOComadd.Lists["x_PrvCodi"] = {"LinkField":"x_PrvCodi","Ajax":true,"AutoFill":false,"DisplayFields":["x_PrvRSoc","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};

// Form object for search
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php $OCom_add->ShowPageHeader(); ?>
<?php
$OCom_add->ShowMessage();
?>
<form name="fOComadd" id="fOComadd" class="<?php echo $OCom_add->FormClassName ?>" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($OCom_add->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $OCom_add->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="OCom">
<input type="hidden" name="a_add" id="a_add" value="A">
<?php if ($OCom->CurrentAction == "F") { // Confirm page ?>
<input type="hidden" name="a_confirm" id="a_confirm" value="F">
<?php } ?>
<div>
<?php if ($OCom->PrvCodi->Visible) { // PrvCodi ?>
	<div id="r_PrvCodi" class="form-group">
		<label id="elh_OCom_PrvCodi" for="x_PrvCodi" class="col-sm-2 control-label ewLabel"><?php echo $OCom->PrvCodi->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $OCom->PrvCodi->CellAttributes() ?>>
<?php if ($OCom->CurrentAction <> "F") { ?>
<span id="el_OCom_PrvCodi">
<select data-table="OCom" data-field="x_PrvCodi" data-value-separator="<?php echo ew_HtmlEncode(is_array($OCom->PrvCodi->DisplayValueSeparator) ? json_encode($OCom->PrvCodi->DisplayValueSeparator) : $OCom->PrvCodi->DisplayValueSeparator) ?>" id="x_PrvCodi" name="x_PrvCodi"<?php echo $OCom->PrvCodi->EditAttributes() ?>>
<?php
if (is_array($OCom->PrvCodi->EditValue)) {
	$arwrk = $OCom->PrvCodi->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($OCom->PrvCodi->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $OCom->PrvCodi->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($OCom->PrvCodi->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($OCom->PrvCodi->CurrentValue) ?>" selected><?php echo $OCom->PrvCodi->CurrentValue ?></option>
<?php
    }
}
?>
</select>
<?php
$sSqlWrk = "SELECT \"PrvCodi\", \"PrvRSoc\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"Prov\"";
$sWhereWrk = "";
$OCom->PrvCodi->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$OCom->PrvCodi->LookupFilters += array("f0" => "\"PrvCodi\" = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$OCom->Lookup_Selecting($OCom->PrvCodi, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $OCom->PrvCodi->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x_PrvCodi" id="s_x_PrvCodi" value="<?php echo $OCom->PrvCodi->LookupFilterQuery() ?>">
</span>
<?php } else { ?>
<span id="el_OCom_PrvCodi">
<span<?php echo $OCom->PrvCodi->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $OCom->PrvCodi->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="OCom" data-field="x_PrvCodi" name="x_PrvCodi" id="x_PrvCodi" value="<?php echo ew_HtmlEncode($OCom->PrvCodi->FormValue) ?>">
<?php } ?>
<?php echo $OCom->PrvCodi->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($OCom->OcoLPed->Visible) { // OcoLPed ?>
	<div id="r_OcoLPed" class="form-group">
		<label id="elh_OCom_OcoLPed" class="col-sm-2 control-label ewLabel"><?php echo $OCom->OcoLPed->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $OCom->OcoLPed->CellAttributes() ?>>
<span id="el_OCom_OcoLPed">
<div id="fd_x_OcoLPed">
<span title="<?php echo $OCom->OcoLPed->FldTitle() ? $OCom->OcoLPed->FldTitle() : $Language->Phrase("ChooseFile") ?>" class="btn btn-default btn-sm fileinput-button ewTooltip<?php if ($OCom->OcoLPed->ReadOnly || $OCom->OcoLPed->Disabled) echo " hide"; ?>">
	<span><?php echo $Language->Phrase("ChooseFileBtn") ?></span>
	<input type="file" title=" " data-table="OCom" data-field="x_OcoLPed" name="x_OcoLPed" id="x_OcoLPed"<?php echo $OCom->OcoLPed->EditAttributes() ?>>
</span>
<input type="hidden" name="fn_x_OcoLPed" id= "fn_x_OcoLPed" value="<?php echo $OCom->OcoLPed->Upload->FileName ?>">
<input type="hidden" name="fa_x_OcoLPed" id= "fa_x_OcoLPed" value="0">
<input type="hidden" name="fs_x_OcoLPed" id= "fs_x_OcoLPed" value="0">
<input type="hidden" name="fx_x_OcoLPed" id= "fx_x_OcoLPed" value="<?php echo $OCom->OcoLPed->UploadAllowedFileExt ?>">
<input type="hidden" name="fm_x_OcoLPed" id= "fm_x_OcoLPed" value="<?php echo $OCom->OcoLPed->UploadMaxFileSize ?>">
</div>
<table id="ft_x_OcoLPed" class="table table-condensed pull-left ewUploadTable"><tbody class="files"></tbody></table>
</span>
<?php echo $OCom->OcoLPed->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($OCom->OcoFCom->Visible) { // OcoFCom ?>
	<div id="r_OcoFCom" class="form-group">
		<label id="elh_OCom_OcoFCom" for="x_OcoFCom" class="col-sm-2 control-label ewLabel"><?php echo $OCom->OcoFCom->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $OCom->OcoFCom->CellAttributes() ?>>
<?php if ($OCom->CurrentAction <> "F") { ?>
<span id="el_OCom_OcoFCom">
<input type="text" data-table="OCom" data-field="x_OcoFCom" data-format="7" name="x_OcoFCom" id="x_OcoFCom" placeholder="<?php echo ew_HtmlEncode($OCom->OcoFCom->getPlaceHolder()) ?>" value="<?php echo $OCom->OcoFCom->EditValue ?>"<?php echo $OCom->OcoFCom->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_OCom_OcoFCom">
<span<?php echo $OCom->OcoFCom->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $OCom->OcoFCom->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="OCom" data-field="x_OcoFCom" name="x_OcoFCom" id="x_OcoFCom" value="<?php echo ew_HtmlEncode($OCom->OcoFCom->FormValue) ?>">
<?php } ?>
<?php echo $OCom->OcoFCom->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($OCom->OcoProf->Visible) { // OcoProf ?>
	<div id="r_OcoProf" class="form-group">
		<label id="elh_OCom_OcoProf" class="col-sm-2 control-label ewLabel"><?php echo $OCom->OcoProf->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $OCom->OcoProf->CellAttributes() ?>>
<span id="el_OCom_OcoProf">
<div id="fd_x_OcoProf">
<span title="<?php echo $OCom->OcoProf->FldTitle() ? $OCom->OcoProf->FldTitle() : $Language->Phrase("ChooseFile") ?>" class="btn btn-default btn-sm fileinput-button ewTooltip<?php if ($OCom->OcoProf->ReadOnly || $OCom->OcoProf->Disabled) echo " hide"; ?>">
	<span><?php echo $Language->Phrase("ChooseFileBtn") ?></span>
	<input type="file" title=" " data-table="OCom" data-field="x_OcoProf" name="x_OcoProf" id="x_OcoProf"<?php echo $OCom->OcoProf->EditAttributes() ?>>
</span>
<input type="hidden" name="fn_x_OcoProf" id= "fn_x_OcoProf" value="<?php echo $OCom->OcoProf->Upload->FileName ?>">
<input type="hidden" name="fa_x_OcoProf" id= "fa_x_OcoProf" value="0">
<input type="hidden" name="fs_x_OcoProf" id= "fs_x_OcoProf" value="0">
<input type="hidden" name="fx_x_OcoProf" id= "fx_x_OcoProf" value="<?php echo $OCom->OcoProf->UploadAllowedFileExt ?>">
<input type="hidden" name="fm_x_OcoProf" id= "fm_x_OcoProf" value="<?php echo $OCom->OcoProf->UploadMaxFileSize ?>">
</div>
<table id="ft_x_OcoProf" class="table table-condensed pull-left ewUploadTable"><tbody class="files"></tbody></table>
</span>
<?php echo $OCom->OcoProf->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($OCom->OcoEsta->Visible) { // OcoEsta ?>
	<div id="r_OcoEsta" class="form-group">
		<label id="elh_OCom_OcoEsta" for="x_OcoEsta" class="col-sm-2 control-label ewLabel"><?php echo $OCom->OcoEsta->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $OCom->OcoEsta->CellAttributes() ?>>
<?php if ($OCom->CurrentAction <> "F") { ?>
<span id="el_OCom_OcoEsta">
<input type="text" data-table="OCom" data-field="x_OcoEsta" name="x_OcoEsta" id="x_OcoEsta" size="30" maxlength="1" placeholder="<?php echo ew_HtmlEncode($OCom->OcoEsta->getPlaceHolder()) ?>" value="<?php echo $OCom->OcoEsta->EditValue ?>"<?php echo $OCom->OcoEsta->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_OCom_OcoEsta">
<span<?php echo $OCom->OcoEsta->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $OCom->OcoEsta->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="OCom" data-field="x_OcoEsta" name="x_OcoEsta" id="x_OcoEsta" value="<?php echo ew_HtmlEncode($OCom->OcoEsta->FormValue) ?>">
<?php } ?>
<?php echo $OCom->OcoEsta->CustomMsg ?></div></div>
	</div>
<?php } ?>
</div>
<div class="form-group">
	<div class="col-sm-offset-2 col-sm-10">
<?php if ($OCom->CurrentAction <> "F") { // Confirm page ?>
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit" onclick="this.form.a_add.value='F';"><?php echo $Language->Phrase("AddBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="button" data-href="<?php echo $OCom_add->getReturnUrl() ?>"><?php echo $Language->Phrase("CancelBtn") ?></button>
<?php } else { ?>
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("ConfirmBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="submit" onclick="this.form.a_add.value='X';"><?php echo $Language->Phrase("CancelBtn") ?></button>
<?php } ?>
	</div>
</div>
</form>
<script type="text/javascript">
fOComadd.Init();
</script>
<?php
$OCom_add->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$OCom_add->Page_Terminate();
?>
