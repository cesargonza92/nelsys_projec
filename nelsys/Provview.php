<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "Provinfo.php" ?>
<?php include_once "Usuainfo.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$Prov_view = NULL; // Initialize page object first

class cProv_view extends cProv {

	// Page ID
	var $PageID = 'view';

	// Project ID
	var $ProjectID = "{04439FF7-B43F-460F-8514-F71C8FF9E679}";

	// Table name
	var $TableName = 'Prov';

	// Page object name
	var $PageObjName = 'Prov_view';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Page URLs
	var $AddUrl;
	var $EditUrl;
	var $CopyUrl;
	var $DeleteUrl;
	var $ViewUrl;
	var $ListUrl;

	// Export URLs
	var $ExportPrintUrl;
	var $ExportHtmlUrl;
	var $ExportExcelUrl;
	var $ExportWordUrl;
	var $ExportXmlUrl;
	var $ExportCsvUrl;
	var $ExportPdfUrl;

	// Custom export
	var $ExportExcelCustom = FALSE;
	var $ExportWordCustom = FALSE;
	var $ExportPdfCustom = FALSE;
	var $ExportEmailCustom = FALSE;

	// Update URLs
	var $InlineAddUrl;
	var $InlineCopyUrl;
	var $InlineEditUrl;
	var $GridAddUrl;
	var $GridEditUrl;
	var $MultiDeleteUrl;
	var $MultiUpdateUrl;

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (Prov)
		if (!isset($GLOBALS["Prov"]) || get_class($GLOBALS["Prov"]) == "cProv") {
			$GLOBALS["Prov"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["Prov"];
		}
		$KeyUrl = "";
		if (@$_GET["PrvCodi"] <> "") {
			$this->RecKey["PrvCodi"] = $_GET["PrvCodi"];
			$KeyUrl .= "&amp;PrvCodi=" . urlencode($this->RecKey["PrvCodi"]);
		}
		$this->ExportPrintUrl = $this->PageUrl() . "export=print" . $KeyUrl;
		$this->ExportHtmlUrl = $this->PageUrl() . "export=html" . $KeyUrl;
		$this->ExportExcelUrl = $this->PageUrl() . "export=excel" . $KeyUrl;
		$this->ExportWordUrl = $this->PageUrl() . "export=word" . $KeyUrl;
		$this->ExportXmlUrl = $this->PageUrl() . "export=xml" . $KeyUrl;
		$this->ExportCsvUrl = $this->PageUrl() . "export=csv" . $KeyUrl;
		$this->ExportPdfUrl = $this->PageUrl() . "export=pdf" . $KeyUrl;

		// Table object (Usua)
		if (!isset($GLOBALS['Usua'])) $GLOBALS['Usua'] = new cUsua();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'view', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'Prov', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (Usua)
		if (!isset($UserTable)) {
			$UserTable = new cUsua();
			$UserTableConn = Conn($UserTable->DBID);
		}

		// Export options
		$this->ExportOptions = new cListOptions();
		$this->ExportOptions->Tag = "div";
		$this->ExportOptions->TagClassName = "ewExportOption";

		// Other options
		$this->OtherOptions['action'] = new cListOptions();
		$this->OtherOptions['action']->Tag = "div";
		$this->OtherOptions['action']->TagClassName = "ewActionOption";
		$this->OtherOptions['detail'] = new cListOptions();
		$this->OtherOptions['detail']->Tag = "div";
		$this->OtherOptions['detail']->TagClassName = "ewDetailOption";
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanView()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage($Language->Phrase("NoPermission")); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("Provlist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $Prov;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($Prov);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $ExportOptions; // Export options
	var $OtherOptions = array(); // Other options
	var $DisplayRecs = 1;
	var $DbMasterFilter;
	var $DbDetailFilter;
	var $StartRec;
	var $StopRec;
	var $TotalRecs = 0;
	var $RecRange = 10;
	var $RecCnt;
	var $RecKey = array();
	var $Recordset;

	//
	// Page main
	//
	function Page_Main() {
		global $Language;
		$sReturnUrl = "";
		$bMatchRecord = FALSE;

		// Set up Breadcrumb
		if ($this->Export == "")
			$this->SetupBreadcrumb();
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET["PrvCodi"] <> "") {
				$this->PrvCodi->setQueryStringValue($_GET["PrvCodi"]);
				$this->RecKey["PrvCodi"] = $this->PrvCodi->QueryStringValue;
			} elseif (@$_POST["PrvCodi"] <> "") {
				$this->PrvCodi->setFormValue($_POST["PrvCodi"]);
				$this->RecKey["PrvCodi"] = $this->PrvCodi->FormValue;
			} else {
				$sReturnUrl = "Provlist.php"; // Return to list
			}

			// Get action
			$this->CurrentAction = "I"; // Display form
			switch ($this->CurrentAction) {
				case "I": // Get a record to display
					if (!$this->LoadRow()) { // Load record based on key
						if ($this->getSuccessMessage() == "" && $this->getFailureMessage() == "")
							$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record message
						$sReturnUrl = "Provlist.php"; // No matching record, return to list
					}
			}
		} else {
			$sReturnUrl = "Provlist.php"; // Not page request, return to list
		}
		if ($sReturnUrl <> "")
			$this->Page_Terminate($sReturnUrl);

		// Render row
		$this->RowType = EW_ROWTYPE_VIEW;
		$this->ResetAttrs();
		$this->RenderRow();
	}

	// Set up other options
	function SetupOtherOptions() {
		global $Language, $Security;
		$options = &$this->OtherOptions;
		$option = &$options["action"];

		// Add
		$item = &$option->Add("add");
		$item->Body = "<a class=\"ewAction ewAdd\" title=\"" . ew_HtmlTitle($Language->Phrase("ViewPageAddLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("ViewPageAddLink")) . "\" href=\"" . ew_HtmlEncode($this->AddUrl) . "\">" . $Language->Phrase("ViewPageAddLink") . "</a>";
		$item->Visible = ($this->AddUrl <> "" && $Security->CanAdd());

		// Edit
		$item = &$option->Add("edit");
		$item->Body = "<a class=\"ewAction ewEdit\" title=\"" . ew_HtmlTitle($Language->Phrase("ViewPageEditLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("ViewPageEditLink")) . "\" href=\"" . ew_HtmlEncode($this->EditUrl) . "\">" . $Language->Phrase("ViewPageEditLink") . "</a>";
		$item->Visible = ($this->EditUrl <> "" && $Security->CanEdit());

		// Copy
		$item = &$option->Add("copy");
		$item->Body = "<a class=\"ewAction ewCopy\" title=\"" . ew_HtmlTitle($Language->Phrase("ViewPageCopyLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("ViewPageCopyLink")) . "\" href=\"" . ew_HtmlEncode($this->CopyUrl) . "\">" . $Language->Phrase("ViewPageCopyLink") . "</a>";
		$item->Visible = ($this->CopyUrl <> "" && $Security->CanAdd());

		// Delete
		$item = &$option->Add("delete");
		$item->Body = "<a class=\"ewAction ewDelete\" title=\"" . ew_HtmlTitle($Language->Phrase("ViewPageDeleteLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("ViewPageDeleteLink")) . "\" href=\"" . ew_HtmlEncode($this->DeleteUrl) . "\">" . $Language->Phrase("ViewPageDeleteLink") . "</a>";
		$item->Visible = ($this->DeleteUrl <> "" && $Security->CanDelete());

		// Set up action default
		$option = &$options["action"];
		$option->DropDownButtonPhrase = $Language->Phrase("ButtonActions");
		$option->UseImageAndText = TRUE;
		$option->UseDropDownButton = FALSE;
		$option->UseButtonGroup = TRUE;
		$item = &$option->Add($option->GroupOptionName);
		$item->Body = "";
		$item->Visible = FALSE;
	}

	// Set up starting record parameters
	function SetUpStartRec() {
		if ($this->DisplayRecs == 0)
			return;
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET[EW_TABLE_START_REC] <> "") { // Check for "start" parameter
				$this->StartRec = $_GET[EW_TABLE_START_REC];
				$this->setStartRecordNumber($this->StartRec);
			} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
				$PageNo = $_GET[EW_TABLE_PAGE_NO];
				if (is_numeric($PageNo)) {
					$this->StartRec = ($PageNo-1)*$this->DisplayRecs+1;
					if ($this->StartRec <= 0) {
						$this->StartRec = 1;
					} elseif ($this->StartRec >= intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1) {
						$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1;
					}
					$this->setStartRecordNumber($this->StartRec);
				}
			}
		}
		$this->StartRec = $this->getStartRecordNumber();

		// Check if correct start record counter
		if (!is_numeric($this->StartRec) || $this->StartRec == "") { // Avoid invalid start record counter
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} elseif (intval($this->StartRec) > intval($this->TotalRecs)) { // Avoid starting record > total records
			$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to last page first record
			$this->setStartRecordNumber($this->StartRec);
		} elseif (($this->StartRec-1) % $this->DisplayRecs <> 0) {
			$this->StartRec = intval(($this->StartRec-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to page boundary
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->PrvCodi->setDbValue($rs->fields('PrvCodi'));
		$this->PrvFIni->setDbValue($rs->fields('PrvFIni'));
		$this->PrvFFin->setDbValue($rs->fields('PrvFFin'));
		$this->PrvPais->setDbValue($rs->fields('PrvPais'));
		$this->PrvTipo->setDbValue($rs->fields('PrvTipo'));
		$this->PrvNomb->setDbValue($rs->fields('PrvNomb'));
		$this->PrvApel->setDbValue($rs->fields('PrvApel'));
		$this->PrvCRuc->setDbValue($rs->fields('PrvCRuc'));
		$this->PrvDire->setDbValue($rs->fields('PrvDire'));
		$this->PrvMail->setDbValue($rs->fields('PrvMail'));
		$this->PrvTele->setDbValue($rs->fields('PrvTele'));
		$this->PrvRSoc->setDbValue($rs->fields('PrvRSoc'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->PrvCodi->DbValue = $row['PrvCodi'];
		$this->PrvFIni->DbValue = $row['PrvFIni'];
		$this->PrvFFin->DbValue = $row['PrvFFin'];
		$this->PrvPais->DbValue = $row['PrvPais'];
		$this->PrvTipo->DbValue = $row['PrvTipo'];
		$this->PrvNomb->DbValue = $row['PrvNomb'];
		$this->PrvApel->DbValue = $row['PrvApel'];
		$this->PrvCRuc->DbValue = $row['PrvCRuc'];
		$this->PrvDire->DbValue = $row['PrvDire'];
		$this->PrvMail->DbValue = $row['PrvMail'];
		$this->PrvTele->DbValue = $row['PrvTele'];
		$this->PrvRSoc->DbValue = $row['PrvRSoc'];
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		$this->AddUrl = $this->GetAddUrl();
		$this->EditUrl = $this->GetEditUrl();
		$this->CopyUrl = $this->GetCopyUrl();
		$this->DeleteUrl = $this->GetDeleteUrl();
		$this->ListUrl = $this->GetListUrl();
		$this->SetupOtherOptions();

		// Call Row_Rendering event
		$this->Row_Rendering();

		// Common render codes for all row types
		// PrvCodi
		// PrvFIni
		// PrvFFin
		// PrvPais
		// PrvTipo
		// PrvNomb
		// PrvApel
		// PrvCRuc
		// PrvDire
		// PrvMail
		// PrvTele
		// PrvRSoc

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// PrvCodi
		$this->PrvCodi->ViewValue = $this->PrvCodi->CurrentValue;
		$this->PrvCodi->ViewCustomAttributes = "";

		// PrvFIni
		$this->PrvFIni->ViewValue = $this->PrvFIni->CurrentValue;
		$this->PrvFIni->ViewValue = ew_FormatDateTime($this->PrvFIni->ViewValue, 7);
		$this->PrvFIni->ViewCustomAttributes = "";

		// PrvFFin
		$this->PrvFFin->ViewValue = $this->PrvFFin->CurrentValue;
		$this->PrvFFin->ViewValue = ew_FormatDateTime($this->PrvFFin->ViewValue, 7);
		$this->PrvFFin->ViewCustomAttributes = "";

		// PrvPais
		$this->PrvPais->ViewValue = $this->PrvPais->CurrentValue;
		$this->PrvPais->ViewCustomAttributes = "";

		// PrvTipo
		$this->PrvTipo->ViewValue = $this->PrvTipo->CurrentValue;
		$this->PrvTipo->ViewCustomAttributes = "";

		// PrvNomb
		$this->PrvNomb->ViewValue = $this->PrvNomb->CurrentValue;
		$this->PrvNomb->ViewCustomAttributes = "";

		// PrvApel
		$this->PrvApel->ViewValue = $this->PrvApel->CurrentValue;
		$this->PrvApel->ViewCustomAttributes = "";

		// PrvCRuc
		$this->PrvCRuc->ViewValue = $this->PrvCRuc->CurrentValue;
		$this->PrvCRuc->ViewCustomAttributes = "";

		// PrvDire
		$this->PrvDire->ViewValue = $this->PrvDire->CurrentValue;
		$this->PrvDire->ViewCustomAttributes = "";

		// PrvMail
		$this->PrvMail->ViewValue = $this->PrvMail->CurrentValue;
		$this->PrvMail->ViewCustomAttributes = "";

		// PrvTele
		$this->PrvTele->ViewValue = $this->PrvTele->CurrentValue;
		$this->PrvTele->ViewCustomAttributes = "";

		// PrvRSoc
		$this->PrvRSoc->ViewValue = $this->PrvRSoc->CurrentValue;
		$this->PrvRSoc->ViewCustomAttributes = "";

			// PrvFIni
			$this->PrvFIni->LinkCustomAttributes = "";
			$this->PrvFIni->HrefValue = "";
			$this->PrvFIni->TooltipValue = "";

			// PrvFFin
			$this->PrvFFin->LinkCustomAttributes = "";
			$this->PrvFFin->HrefValue = "";
			$this->PrvFFin->TooltipValue = "";

			// PrvPais
			$this->PrvPais->LinkCustomAttributes = "";
			$this->PrvPais->HrefValue = "";
			$this->PrvPais->TooltipValue = "";

			// PrvTipo
			$this->PrvTipo->LinkCustomAttributes = "";
			$this->PrvTipo->HrefValue = "";
			$this->PrvTipo->TooltipValue = "";

			// PrvNomb
			$this->PrvNomb->LinkCustomAttributes = "";
			$this->PrvNomb->HrefValue = "";
			$this->PrvNomb->TooltipValue = "";

			// PrvApel
			$this->PrvApel->LinkCustomAttributes = "";
			$this->PrvApel->HrefValue = "";
			$this->PrvApel->TooltipValue = "";

			// PrvCRuc
			$this->PrvCRuc->LinkCustomAttributes = "";
			$this->PrvCRuc->HrefValue = "";
			$this->PrvCRuc->TooltipValue = "";

			// PrvDire
			$this->PrvDire->LinkCustomAttributes = "";
			$this->PrvDire->HrefValue = "";
			$this->PrvDire->TooltipValue = "";

			// PrvMail
			$this->PrvMail->LinkCustomAttributes = "";
			$this->PrvMail->HrefValue = "";
			$this->PrvMail->TooltipValue = "";

			// PrvTele
			$this->PrvTele->LinkCustomAttributes = "";
			$this->PrvTele->HrefValue = "";
			$this->PrvTele->TooltipValue = "";

			// PrvRSoc
			$this->PrvRSoc->LinkCustomAttributes = "";
			$this->PrvRSoc->HrefValue = "";
			$this->PrvRSoc->TooltipValue = "";
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, "Provlist.php", "", $this->TableVar, TRUE);
		$PageId = "view";
		$Breadcrumb->Add("view", $PageId, $url);
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Page Exporting event
	// $this->ExportDoc = export document object
	function Page_Exporting() {

		//$this->ExportDoc->Text = "my header"; // Export header
		//return FALSE; // Return FALSE to skip default export and use Row_Export event

		return TRUE; // Return TRUE to use default export and skip Row_Export event
	}

	// Row Export event
	// $this->ExportDoc = export document object
	function Row_Export($rs) {

	    //$this->ExportDoc->Text .= "my content"; // Build HTML with field value: $rs["MyField"] or $this->MyField->ViewValue
	}

	// Page Exported event
	// $this->ExportDoc = export document object
	function Page_Exported() {

		//$this->ExportDoc->Text .= "my footer"; // Export footer
		//echo $this->ExportDoc->Text;

	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($Prov_view)) $Prov_view = new cProv_view();

// Page init
$Prov_view->Page_Init();

// Page main
$Prov_view->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$Prov_view->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "view";
var CurrentForm = fProvview = new ew_Form("fProvview", "view");

// Form_CustomValidate event
fProvview.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fProvview.ValidateRequired = true;
<?php } else { ?>
fProvview.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
// Form object for search

</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php $Prov_view->ExportOptions->Render("body") ?>
<?php
	foreach ($Prov_view->OtherOptions as &$option)
		$option->Render("body");
?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php $Prov_view->ShowPageHeader(); ?>
<?php
$Prov_view->ShowMessage();
?>
<form name="fProvview" id="fProvview" class="form-inline ewForm ewViewForm" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($Prov_view->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $Prov_view->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="Prov">
<table class="table table-bordered table-striped ewViewTable">
<?php if ($Prov->PrvFIni->Visible) { // PrvFIni ?>
	<tr id="r_PrvFIni">
		<td><span id="elh_Prov_PrvFIni"><?php echo $Prov->PrvFIni->FldCaption() ?></span></td>
		<td data-name="PrvFIni"<?php echo $Prov->PrvFIni->CellAttributes() ?>>
<span id="el_Prov_PrvFIni">
<span<?php echo $Prov->PrvFIni->ViewAttributes() ?>>
<?php echo $Prov->PrvFIni->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($Prov->PrvFFin->Visible) { // PrvFFin ?>
	<tr id="r_PrvFFin">
		<td><span id="elh_Prov_PrvFFin"><?php echo $Prov->PrvFFin->FldCaption() ?></span></td>
		<td data-name="PrvFFin"<?php echo $Prov->PrvFFin->CellAttributes() ?>>
<span id="el_Prov_PrvFFin">
<span<?php echo $Prov->PrvFFin->ViewAttributes() ?>>
<?php echo $Prov->PrvFFin->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($Prov->PrvPais->Visible) { // PrvPais ?>
	<tr id="r_PrvPais">
		<td><span id="elh_Prov_PrvPais"><?php echo $Prov->PrvPais->FldCaption() ?></span></td>
		<td data-name="PrvPais"<?php echo $Prov->PrvPais->CellAttributes() ?>>
<span id="el_Prov_PrvPais">
<span<?php echo $Prov->PrvPais->ViewAttributes() ?>>
<?php echo $Prov->PrvPais->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($Prov->PrvTipo->Visible) { // PrvTipo ?>
	<tr id="r_PrvTipo">
		<td><span id="elh_Prov_PrvTipo"><?php echo $Prov->PrvTipo->FldCaption() ?></span></td>
		<td data-name="PrvTipo"<?php echo $Prov->PrvTipo->CellAttributes() ?>>
<span id="el_Prov_PrvTipo">
<span<?php echo $Prov->PrvTipo->ViewAttributes() ?>>
<?php echo $Prov->PrvTipo->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($Prov->PrvNomb->Visible) { // PrvNomb ?>
	<tr id="r_PrvNomb">
		<td><span id="elh_Prov_PrvNomb"><?php echo $Prov->PrvNomb->FldCaption() ?></span></td>
		<td data-name="PrvNomb"<?php echo $Prov->PrvNomb->CellAttributes() ?>>
<span id="el_Prov_PrvNomb">
<span<?php echo $Prov->PrvNomb->ViewAttributes() ?>>
<?php echo $Prov->PrvNomb->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($Prov->PrvApel->Visible) { // PrvApel ?>
	<tr id="r_PrvApel">
		<td><span id="elh_Prov_PrvApel"><?php echo $Prov->PrvApel->FldCaption() ?></span></td>
		<td data-name="PrvApel"<?php echo $Prov->PrvApel->CellAttributes() ?>>
<span id="el_Prov_PrvApel">
<span<?php echo $Prov->PrvApel->ViewAttributes() ?>>
<?php echo $Prov->PrvApel->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($Prov->PrvCRuc->Visible) { // PrvCRuc ?>
	<tr id="r_PrvCRuc">
		<td><span id="elh_Prov_PrvCRuc"><?php echo $Prov->PrvCRuc->FldCaption() ?></span></td>
		<td data-name="PrvCRuc"<?php echo $Prov->PrvCRuc->CellAttributes() ?>>
<span id="el_Prov_PrvCRuc">
<span<?php echo $Prov->PrvCRuc->ViewAttributes() ?>>
<?php echo $Prov->PrvCRuc->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($Prov->PrvDire->Visible) { // PrvDire ?>
	<tr id="r_PrvDire">
		<td><span id="elh_Prov_PrvDire"><?php echo $Prov->PrvDire->FldCaption() ?></span></td>
		<td data-name="PrvDire"<?php echo $Prov->PrvDire->CellAttributes() ?>>
<span id="el_Prov_PrvDire">
<span<?php echo $Prov->PrvDire->ViewAttributes() ?>>
<?php echo $Prov->PrvDire->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($Prov->PrvMail->Visible) { // PrvMail ?>
	<tr id="r_PrvMail">
		<td><span id="elh_Prov_PrvMail"><?php echo $Prov->PrvMail->FldCaption() ?></span></td>
		<td data-name="PrvMail"<?php echo $Prov->PrvMail->CellAttributes() ?>>
<span id="el_Prov_PrvMail">
<span<?php echo $Prov->PrvMail->ViewAttributes() ?>>
<?php echo $Prov->PrvMail->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($Prov->PrvTele->Visible) { // PrvTele ?>
	<tr id="r_PrvTele">
		<td><span id="elh_Prov_PrvTele"><?php echo $Prov->PrvTele->FldCaption() ?></span></td>
		<td data-name="PrvTele"<?php echo $Prov->PrvTele->CellAttributes() ?>>
<span id="el_Prov_PrvTele">
<span<?php echo $Prov->PrvTele->ViewAttributes() ?>>
<?php echo $Prov->PrvTele->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($Prov->PrvRSoc->Visible) { // PrvRSoc ?>
	<tr id="r_PrvRSoc">
		<td><span id="elh_Prov_PrvRSoc"><?php echo $Prov->PrvRSoc->FldCaption() ?></span></td>
		<td data-name="PrvRSoc"<?php echo $Prov->PrvRSoc->CellAttributes() ?>>
<span id="el_Prov_PrvRSoc">
<span<?php echo $Prov->PrvRSoc->ViewAttributes() ?>>
<?php echo $Prov->PrvRSoc->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
</table>
</form>
<script type="text/javascript">
fProvview.Init();
</script>
<?php
$Prov_view->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$Prov_view->Page_Terminate();
?>
