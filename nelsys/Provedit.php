<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "Provinfo.php" ?>
<?php include_once "Usuainfo.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$Prov_edit = NULL; // Initialize page object first

class cProv_edit extends cProv {

	// Page ID
	var $PageID = 'edit';

	// Project ID
	var $ProjectID = "{04439FF7-B43F-460F-8514-F71C8FF9E679}";

	// Table name
	var $TableName = 'Prov';

	// Page object name
	var $PageObjName = 'Prov_edit';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (Prov)
		if (!isset($GLOBALS["Prov"]) || get_class($GLOBALS["Prov"]) == "cProv") {
			$GLOBALS["Prov"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["Prov"];
		}

		// Table object (Usua)
		if (!isset($GLOBALS['Usua'])) $GLOBALS['Usua'] = new cUsua();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'edit', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'Prov', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (Usua)
		if (!isset($UserTable)) {
			$UserTable = new cUsua();
			$UserTableConn = Conn($UserTable->DBID);
		}
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanEdit()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage($Language->Phrase("NoPermission")); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("Provlist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}

		// Create form object
		$objForm = new cFormObj();
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Process auto fill
		if (@$_POST["ajax"] == "autofill") {
			$results = $this->GetAutoFill(@$_POST["name"], @$_POST["q"]);
			if ($results) {

				// Clean output buffer
				if (!EW_DEBUG_ENABLED && ob_get_length())
					ob_end_clean();
				echo $results;
				$this->Page_Terminate();
				exit();
			}
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $Prov;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($Prov);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $FormClassName = "form-horizontal ewForm ewEditForm";
	var $DbMasterFilter;
	var $DbDetailFilter;

	// 
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError;

		// Load key from QueryString
		if (@$_GET["PrvCodi"] <> "") {
			$this->PrvCodi->setQueryStringValue($_GET["PrvCodi"]);
		}

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Process form if post back
		if (@$_POST["a_edit"] <> "") {
			$this->CurrentAction = $_POST["a_edit"]; // Get action code
			$this->LoadFormValues(); // Get form values
		} else {
			$this->CurrentAction = "I"; // Default action is display
		}

		// Check if valid key
		if ($this->PrvCodi->CurrentValue == "")
			$this->Page_Terminate("Provlist.php"); // Invalid key, return to list

		// Validate form if post back
		if (@$_POST["a_edit"] <> "") {
			if (!$this->ValidateForm()) {
				$this->CurrentAction = ""; // Form error, reset action
				$this->setFailureMessage($gsFormError);
				$this->EventCancelled = TRUE; // Event cancelled
				$this->RestoreFormValues();
			}
		}
		switch ($this->CurrentAction) {
			case "I": // Get a record to display
				if (!$this->LoadRow()) { // Load record based on key
					if ($this->getFailureMessage() == "") $this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
					$this->Page_Terminate("Provlist.php"); // No matching record, return to list
				}
				break;
			Case "U": // Update
				$sReturnUrl = $this->getReturnUrl();
				$this->SendEmail = TRUE; // Send email on update success
				if ($this->EditRow()) { // Update record based on key
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage($Language->Phrase("UpdateSuccess")); // Update success
					$this->Page_Terminate($sReturnUrl); // Return to caller
				} elseif ($this->getFailureMessage() == $Language->Phrase("NoRecord")) {
					$this->Page_Terminate($sReturnUrl); // Return to caller
				} else {
					$this->EventCancelled = TRUE; // Event cancelled
					$this->RestoreFormValues(); // Restore form values if update failed
				}
		}

		// Render the record
		if ($this->CurrentAction == "F") { // Confirm page
			$this->RowType = EW_ROWTYPE_VIEW; // Render as View
		} else {
			$this->RowType = EW_ROWTYPE_EDIT; // Render as Edit
		}
		$this->ResetAttrs();
		$this->RenderRow();
	}

	// Set up starting record parameters
	function SetUpStartRec() {
		if ($this->DisplayRecs == 0)
			return;
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET[EW_TABLE_START_REC] <> "") { // Check for "start" parameter
				$this->StartRec = $_GET[EW_TABLE_START_REC];
				$this->setStartRecordNumber($this->StartRec);
			} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
				$PageNo = $_GET[EW_TABLE_PAGE_NO];
				if (is_numeric($PageNo)) {
					$this->StartRec = ($PageNo-1)*$this->DisplayRecs+1;
					if ($this->StartRec <= 0) {
						$this->StartRec = 1;
					} elseif ($this->StartRec >= intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1) {
						$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1;
					}
					$this->setStartRecordNumber($this->StartRec);
				}
			}
		}
		$this->StartRec = $this->getStartRecordNumber();

		// Check if correct start record counter
		if (!is_numeric($this->StartRec) || $this->StartRec == "") { // Avoid invalid start record counter
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} elseif (intval($this->StartRec) > intval($this->TotalRecs)) { // Avoid starting record > total records
			$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to last page first record
			$this->setStartRecordNumber($this->StartRec);
		} elseif (($this->StartRec-1) % $this->DisplayRecs <> 0) {
			$this->StartRec = intval(($this->StartRec-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to page boundary
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Get upload files
	function GetUploadFiles() {
		global $objForm, $Language;

		// Get upload data
	}

	// Load form values
	function LoadFormValues() {

		// Load from form
		global $objForm;
		if (!$this->PrvFFin->FldIsDetailKey) {
			$this->PrvFFin->setFormValue($objForm->GetValue("x_PrvFFin"));
			$this->PrvFFin->CurrentValue = ew_UnFormatDateTime($this->PrvFFin->CurrentValue, 7);
		}
		if (!$this->PrvPais->FldIsDetailKey) {
			$this->PrvPais->setFormValue($objForm->GetValue("x_PrvPais"));
		}
		if (!$this->PrvDire->FldIsDetailKey) {
			$this->PrvDire->setFormValue($objForm->GetValue("x_PrvDire"));
		}
		if (!$this->PrvMail->FldIsDetailKey) {
			$this->PrvMail->setFormValue($objForm->GetValue("x_PrvMail"));
		}
		if (!$this->PrvTele->FldIsDetailKey) {
			$this->PrvTele->setFormValue($objForm->GetValue("x_PrvTele"));
		}
		if (!$this->PrvRSoc->FldIsDetailKey) {
			$this->PrvRSoc->setFormValue($objForm->GetValue("x_PrvRSoc"));
		}
		if (!$this->PrvCodi->FldIsDetailKey)
			$this->PrvCodi->setFormValue($objForm->GetValue("x_PrvCodi"));
	}

	// Restore form values
	function RestoreFormValues() {
		global $objForm;
		$this->LoadRow();
		$this->PrvCodi->CurrentValue = $this->PrvCodi->FormValue;
		$this->PrvFFin->CurrentValue = $this->PrvFFin->FormValue;
		$this->PrvFFin->CurrentValue = ew_UnFormatDateTime($this->PrvFFin->CurrentValue, 7);
		$this->PrvPais->CurrentValue = $this->PrvPais->FormValue;
		$this->PrvDire->CurrentValue = $this->PrvDire->FormValue;
		$this->PrvMail->CurrentValue = $this->PrvMail->FormValue;
		$this->PrvTele->CurrentValue = $this->PrvTele->FormValue;
		$this->PrvRSoc->CurrentValue = $this->PrvRSoc->FormValue;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->PrvCodi->setDbValue($rs->fields('PrvCodi'));
		$this->PrvFIni->setDbValue($rs->fields('PrvFIni'));
		$this->PrvFFin->setDbValue($rs->fields('PrvFFin'));
		$this->PrvPais->setDbValue($rs->fields('PrvPais'));
		$this->PrvTipo->setDbValue($rs->fields('PrvTipo'));
		$this->PrvNomb->setDbValue($rs->fields('PrvNomb'));
		$this->PrvApel->setDbValue($rs->fields('PrvApel'));
		$this->PrvCRuc->setDbValue($rs->fields('PrvCRuc'));
		$this->PrvDire->setDbValue($rs->fields('PrvDire'));
		$this->PrvMail->setDbValue($rs->fields('PrvMail'));
		$this->PrvTele->setDbValue($rs->fields('PrvTele'));
		$this->PrvRSoc->setDbValue($rs->fields('PrvRSoc'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->PrvCodi->DbValue = $row['PrvCodi'];
		$this->PrvFIni->DbValue = $row['PrvFIni'];
		$this->PrvFFin->DbValue = $row['PrvFFin'];
		$this->PrvPais->DbValue = $row['PrvPais'];
		$this->PrvTipo->DbValue = $row['PrvTipo'];
		$this->PrvNomb->DbValue = $row['PrvNomb'];
		$this->PrvApel->DbValue = $row['PrvApel'];
		$this->PrvCRuc->DbValue = $row['PrvCRuc'];
		$this->PrvDire->DbValue = $row['PrvDire'];
		$this->PrvMail->DbValue = $row['PrvMail'];
		$this->PrvTele->DbValue = $row['PrvTele'];
		$this->PrvRSoc->DbValue = $row['PrvRSoc'];
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		// Call Row_Rendering event

		$this->Row_Rendering();

		// Common render codes for all row types
		// PrvCodi
		// PrvFIni
		// PrvFFin
		// PrvPais
		// PrvTipo
		// PrvNomb
		// PrvApel
		// PrvCRuc
		// PrvDire
		// PrvMail
		// PrvTele
		// PrvRSoc

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// PrvCodi
		$this->PrvCodi->ViewValue = $this->PrvCodi->CurrentValue;
		$this->PrvCodi->ViewCustomAttributes = "";

		// PrvFIni
		$this->PrvFIni->ViewValue = $this->PrvFIni->CurrentValue;
		$this->PrvFIni->ViewValue = ew_FormatDateTime($this->PrvFIni->ViewValue, 7);
		$this->PrvFIni->ViewCustomAttributes = "";

		// PrvFFin
		$this->PrvFFin->ViewValue = $this->PrvFFin->CurrentValue;
		$this->PrvFFin->ViewValue = ew_FormatDateTime($this->PrvFFin->ViewValue, 7);
		$this->PrvFFin->ViewCustomAttributes = "";

		// PrvPais
		$this->PrvPais->ViewValue = $this->PrvPais->CurrentValue;
		$this->PrvPais->ViewCustomAttributes = "";

		// PrvTipo
		$this->PrvTipo->ViewValue = $this->PrvTipo->CurrentValue;
		$this->PrvTipo->ViewCustomAttributes = "";

		// PrvNomb
		$this->PrvNomb->ViewValue = $this->PrvNomb->CurrentValue;
		$this->PrvNomb->ViewCustomAttributes = "";

		// PrvApel
		$this->PrvApel->ViewValue = $this->PrvApel->CurrentValue;
		$this->PrvApel->ViewCustomAttributes = "";

		// PrvCRuc
		$this->PrvCRuc->ViewValue = $this->PrvCRuc->CurrentValue;
		$this->PrvCRuc->ViewCustomAttributes = "";

		// PrvDire
		$this->PrvDire->ViewValue = $this->PrvDire->CurrentValue;
		$this->PrvDire->ViewCustomAttributes = "";

		// PrvMail
		$this->PrvMail->ViewValue = $this->PrvMail->CurrentValue;
		$this->PrvMail->ViewCustomAttributes = "";

		// PrvTele
		$this->PrvTele->ViewValue = $this->PrvTele->CurrentValue;
		$this->PrvTele->ViewCustomAttributes = "";

		// PrvRSoc
		$this->PrvRSoc->ViewValue = $this->PrvRSoc->CurrentValue;
		$this->PrvRSoc->ViewCustomAttributes = "";

			// PrvFFin
			$this->PrvFFin->LinkCustomAttributes = "";
			$this->PrvFFin->HrefValue = "";
			$this->PrvFFin->TooltipValue = "";

			// PrvPais
			$this->PrvPais->LinkCustomAttributes = "";
			$this->PrvPais->HrefValue = "";
			$this->PrvPais->TooltipValue = "";

			// PrvDire
			$this->PrvDire->LinkCustomAttributes = "";
			$this->PrvDire->HrefValue = "";
			$this->PrvDire->TooltipValue = "";

			// PrvMail
			$this->PrvMail->LinkCustomAttributes = "";
			$this->PrvMail->HrefValue = "";
			$this->PrvMail->TooltipValue = "";

			// PrvTele
			$this->PrvTele->LinkCustomAttributes = "";
			$this->PrvTele->HrefValue = "";
			$this->PrvTele->TooltipValue = "";

			// PrvRSoc
			$this->PrvRSoc->LinkCustomAttributes = "";
			$this->PrvRSoc->HrefValue = "";
			$this->PrvRSoc->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_EDIT) { // Edit row

			// PrvFFin
			$this->PrvFFin->EditAttrs["class"] = "form-control";
			$this->PrvFFin->EditCustomAttributes = "";
			$this->PrvFFin->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->PrvFFin->CurrentValue, 7));
			$this->PrvFFin->PlaceHolder = ew_RemoveHtml($this->PrvFFin->FldCaption());

			// PrvPais
			$this->PrvPais->EditAttrs["class"] = "form-control";
			$this->PrvPais->EditCustomAttributes = "";
			$this->PrvPais->EditValue = ew_HtmlEncode($this->PrvPais->CurrentValue);
			$this->PrvPais->PlaceHolder = ew_RemoveHtml($this->PrvPais->FldCaption());

			// PrvDire
			$this->PrvDire->EditAttrs["class"] = "form-control";
			$this->PrvDire->EditCustomAttributes = "";
			$this->PrvDire->EditValue = ew_HtmlEncode($this->PrvDire->CurrentValue);
			$this->PrvDire->PlaceHolder = ew_RemoveHtml($this->PrvDire->FldCaption());

			// PrvMail
			$this->PrvMail->EditAttrs["class"] = "form-control";
			$this->PrvMail->EditCustomAttributes = "";
			$this->PrvMail->EditValue = ew_HtmlEncode($this->PrvMail->CurrentValue);
			$this->PrvMail->PlaceHolder = ew_RemoveHtml($this->PrvMail->FldCaption());

			// PrvTele
			$this->PrvTele->EditAttrs["class"] = "form-control";
			$this->PrvTele->EditCustomAttributes = "";
			$this->PrvTele->EditValue = ew_HtmlEncode($this->PrvTele->CurrentValue);
			$this->PrvTele->PlaceHolder = ew_RemoveHtml($this->PrvTele->FldCaption());

			// PrvRSoc
			$this->PrvRSoc->EditAttrs["class"] = "form-control";
			$this->PrvRSoc->EditCustomAttributes = "";
			$this->PrvRSoc->EditValue = ew_HtmlEncode($this->PrvRSoc->CurrentValue);
			$this->PrvRSoc->PlaceHolder = ew_RemoveHtml($this->PrvRSoc->FldCaption());

			// Edit refer script
			// PrvFFin

			$this->PrvFFin->HrefValue = "";

			// PrvPais
			$this->PrvPais->HrefValue = "";

			// PrvDire
			$this->PrvDire->HrefValue = "";

			// PrvMail
			$this->PrvMail->HrefValue = "";

			// PrvTele
			$this->PrvTele->HrefValue = "";

			// PrvRSoc
			$this->PrvRSoc->HrefValue = "";
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate form
	function ValidateForm() {
		global $Language, $gsFormError;

		// Initialize form error message
		$gsFormError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return ($gsFormError == "");
		if (!ew_CheckEuroDate($this->PrvFFin->FormValue)) {
			ew_AddMessage($gsFormError, $this->PrvFFin->FldErrMsg());
		}
		if (!$this->PrvPais->FldIsDetailKey && !is_null($this->PrvPais->FormValue) && $this->PrvPais->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->PrvPais->FldCaption(), $this->PrvPais->ReqErrMsg));
		}
		if (!$this->PrvMail->FldIsDetailKey && !is_null($this->PrvMail->FormValue) && $this->PrvMail->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->PrvMail->FldCaption(), $this->PrvMail->ReqErrMsg));
		}
		if (!ew_CheckEmail($this->PrvMail->FormValue)) {
			ew_AddMessage($gsFormError, $this->PrvMail->FldErrMsg());
		}
		if (!ew_CheckInteger($this->PrvTele->FormValue)) {
			ew_AddMessage($gsFormError, $this->PrvTele->FldErrMsg());
		}
		if (!$this->PrvRSoc->FldIsDetailKey && !is_null($this->PrvRSoc->FormValue) && $this->PrvRSoc->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->PrvRSoc->FldCaption(), $this->PrvRSoc->ReqErrMsg));
		}

		// Return validate result
		$ValidateForm = ($gsFormError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateForm = $ValidateForm && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsFormError, $sFormCustomError);
		}
		return $ValidateForm;
	}

	// Update record based on key values
	function EditRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$conn = &$this->Connection();
		if ($this->PrvMail->CurrentValue <> "") { // Check field with unique index
			$sFilterChk = "(\"PrvMail\" = '" . ew_AdjustSql($this->PrvMail->CurrentValue, $this->DBID) . "')";
			$sFilterChk .= " AND NOT (" . $sFilter . ")";
			$this->CurrentFilter = $sFilterChk;
			$sSqlChk = $this->SQL();
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			$rsChk = $conn->Execute($sSqlChk);
			$conn->raiseErrorFn = '';
			if ($rsChk === FALSE) {
				return FALSE;
			} elseif (!$rsChk->EOF) {
				$sIdxErrMsg = str_replace("%f", $this->PrvMail->FldCaption(), $Language->Phrase("DupIndex"));
				$sIdxErrMsg = str_replace("%v", $this->PrvMail->CurrentValue, $sIdxErrMsg);
				$this->setFailureMessage($sIdxErrMsg);
				$rsChk->Close();
				return FALSE;
			}
			$rsChk->Close();
		}
		if ($this->PrvRSoc->CurrentValue <> "") { // Check field with unique index
			$sFilterChk = "(\"PrvRSoc\" = '" . ew_AdjustSql($this->PrvRSoc->CurrentValue, $this->DBID) . "')";
			$sFilterChk .= " AND NOT (" . $sFilter . ")";
			$this->CurrentFilter = $sFilterChk;
			$sSqlChk = $this->SQL();
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			$rsChk = $conn->Execute($sSqlChk);
			$conn->raiseErrorFn = '';
			if ($rsChk === FALSE) {
				return FALSE;
			} elseif (!$rsChk->EOF) {
				$sIdxErrMsg = str_replace("%f", $this->PrvRSoc->FldCaption(), $Language->Phrase("DupIndex"));
				$sIdxErrMsg = str_replace("%v", $this->PrvRSoc->CurrentValue, $sIdxErrMsg);
				$this->setFailureMessage($sIdxErrMsg);
				$rsChk->Close();
				return FALSE;
			}
			$rsChk->Close();
		}
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE)
			return FALSE;
		if ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record message
			$EditRow = FALSE; // Update Failed
		} else {

			// Save old values
			$rsold = &$rs->fields;
			$this->LoadDbValues($rsold);
			$rsnew = array();

			// PrvFFin
			$this->PrvFFin->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->PrvFFin->CurrentValue, 7), NULL, $this->PrvFFin->ReadOnly);

			// PrvPais
			$this->PrvPais->SetDbValueDef($rsnew, $this->PrvPais->CurrentValue, "", $this->PrvPais->ReadOnly);

			// PrvDire
			$this->PrvDire->SetDbValueDef($rsnew, $this->PrvDire->CurrentValue, NULL, $this->PrvDire->ReadOnly);

			// PrvMail
			$this->PrvMail->SetDbValueDef($rsnew, $this->PrvMail->CurrentValue, NULL, $this->PrvMail->ReadOnly);

			// PrvTele
			$this->PrvTele->SetDbValueDef($rsnew, $this->PrvTele->CurrentValue, NULL, $this->PrvTele->ReadOnly);

			// PrvRSoc
			$this->PrvRSoc->SetDbValueDef($rsnew, $this->PrvRSoc->CurrentValue, NULL, $this->PrvRSoc->ReadOnly);

			// Call Row Updating event
			$bUpdateRow = $this->Row_Updating($rsold, $rsnew);
			if ($bUpdateRow) {
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				if (count($rsnew) > 0)
					$EditRow = $this->Update($rsnew, "", $rsold);
				else
					$EditRow = TRUE; // No field to update
				$conn->raiseErrorFn = '';
				if ($EditRow) {
				}
			} else {
				if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

					// Use the message, do nothing
				} elseif ($this->CancelMessage <> "") {
					$this->setFailureMessage($this->CancelMessage);
					$this->CancelMessage = "";
				} else {
					$this->setFailureMessage($Language->Phrase("UpdateCancelled"));
				}
				$EditRow = FALSE;
			}
		}

		// Call Row_Updated event
		if ($EditRow)
			$this->Row_Updated($rsold, $rsnew);
		$rs->Close();
		return $EditRow;
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, "Provlist.php", "", $this->TableVar, TRUE);
		$PageId = "edit";
		$Breadcrumb->Add("edit", $PageId, $url);
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($Prov_edit)) $Prov_edit = new cProv_edit();

// Page init
$Prov_edit->Page_Init();

// Page main
$Prov_edit->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$Prov_edit->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "edit";
var CurrentForm = fProvedit = new ew_Form("fProvedit", "edit");

// Validate form
fProvedit.Validate = function() {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.GetForm(), $fobj = $(fobj);
	if ($fobj.find("#a_confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.FormKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = $fobj.find("#a_list").val() == "gridinsert";
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
			elm = this.GetElements("x" + infix + "_PrvFFin");
			if (elm && !ew_CheckEuroDate(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($Prov->PrvFFin->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_PrvPais");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $Prov->PrvPais->FldCaption(), $Prov->PrvPais->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_PrvMail");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $Prov->PrvMail->FldCaption(), $Prov->PrvMail->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_PrvMail");
			if (elm && !ew_CheckEmail(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($Prov->PrvMail->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_PrvTele");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($Prov->PrvTele->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_PrvRSoc");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $Prov->PrvRSoc->FldCaption(), $Prov->PrvRSoc->ReqErrMsg)) ?>");

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
	}

	// Process detail forms
	var dfs = $fobj.find("input[name='detailpage']").get();
	for (var i = 0; i < dfs.length; i++) {
		var df = dfs[i], val = df.value;
		if (val && ewForms[val])
			if (!ewForms[val].Validate())
				return false;
	}
	return true;
}

// Form_CustomValidate event
fProvedit.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fProvedit.ValidateRequired = true;
<?php } else { ?>
fProvedit.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
// Form object for search

</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php $Prov_edit->ShowPageHeader(); ?>
<?php
$Prov_edit->ShowMessage();
?>
<form name="fProvedit" id="fProvedit" class="<?php echo $Prov_edit->FormClassName ?>" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($Prov_edit->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $Prov_edit->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="Prov">
<input type="hidden" name="a_edit" id="a_edit" value="U">
<?php if ($Prov->CurrentAction == "F") { // Confirm page ?>
<input type="hidden" name="a_confirm" id="a_confirm" value="F">
<?php } ?>
<div>
<?php if ($Prov->PrvFFin->Visible) { // PrvFFin ?>
	<div id="r_PrvFFin" class="form-group">
		<label id="elh_Prov_PrvFFin" for="x_PrvFFin" class="col-sm-2 control-label ewLabel"><?php echo $Prov->PrvFFin->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $Prov->PrvFFin->CellAttributes() ?>>
<?php if ($Prov->CurrentAction <> "F") { ?>
<span id="el_Prov_PrvFFin">
<input type="text" data-table="Prov" data-field="x_PrvFFin" data-format="7" name="x_PrvFFin" id="x_PrvFFin" placeholder="<?php echo ew_HtmlEncode($Prov->PrvFFin->getPlaceHolder()) ?>" value="<?php echo $Prov->PrvFFin->EditValue ?>"<?php echo $Prov->PrvFFin->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_Prov_PrvFFin">
<span<?php echo $Prov->PrvFFin->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $Prov->PrvFFin->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="Prov" data-field="x_PrvFFin" name="x_PrvFFin" id="x_PrvFFin" value="<?php echo ew_HtmlEncode($Prov->PrvFFin->FormValue) ?>">
<?php } ?>
<?php echo $Prov->PrvFFin->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($Prov->PrvPais->Visible) { // PrvPais ?>
	<div id="r_PrvPais" class="form-group">
		<label id="elh_Prov_PrvPais" for="x_PrvPais" class="col-sm-2 control-label ewLabel"><?php echo $Prov->PrvPais->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $Prov->PrvPais->CellAttributes() ?>>
<?php if ($Prov->CurrentAction <> "F") { ?>
<span id="el_Prov_PrvPais">
<input type="text" data-table="Prov" data-field="x_PrvPais" name="x_PrvPais" id="x_PrvPais" size="30" placeholder="<?php echo ew_HtmlEncode($Prov->PrvPais->getPlaceHolder()) ?>" value="<?php echo $Prov->PrvPais->EditValue ?>"<?php echo $Prov->PrvPais->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_Prov_PrvPais">
<span<?php echo $Prov->PrvPais->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $Prov->PrvPais->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="Prov" data-field="x_PrvPais" name="x_PrvPais" id="x_PrvPais" value="<?php echo ew_HtmlEncode($Prov->PrvPais->FormValue) ?>">
<?php } ?>
<?php echo $Prov->PrvPais->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($Prov->PrvDire->Visible) { // PrvDire ?>
	<div id="r_PrvDire" class="form-group">
		<label id="elh_Prov_PrvDire" for="x_PrvDire" class="col-sm-2 control-label ewLabel"><?php echo $Prov->PrvDire->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $Prov->PrvDire->CellAttributes() ?>>
<?php if ($Prov->CurrentAction <> "F") { ?>
<span id="el_Prov_PrvDire">
<input type="text" data-table="Prov" data-field="x_PrvDire" name="x_PrvDire" id="x_PrvDire" size="30" placeholder="<?php echo ew_HtmlEncode($Prov->PrvDire->getPlaceHolder()) ?>" value="<?php echo $Prov->PrvDire->EditValue ?>"<?php echo $Prov->PrvDire->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_Prov_PrvDire">
<span<?php echo $Prov->PrvDire->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $Prov->PrvDire->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="Prov" data-field="x_PrvDire" name="x_PrvDire" id="x_PrvDire" value="<?php echo ew_HtmlEncode($Prov->PrvDire->FormValue) ?>">
<?php } ?>
<?php echo $Prov->PrvDire->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($Prov->PrvMail->Visible) { // PrvMail ?>
	<div id="r_PrvMail" class="form-group">
		<label id="elh_Prov_PrvMail" for="x_PrvMail" class="col-sm-2 control-label ewLabel"><?php echo $Prov->PrvMail->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $Prov->PrvMail->CellAttributes() ?>>
<?php if ($Prov->CurrentAction <> "F") { ?>
<span id="el_Prov_PrvMail">
<input type="text" data-table="Prov" data-field="x_PrvMail" name="x_PrvMail" id="x_PrvMail" size="30" maxlength="50" placeholder="<?php echo ew_HtmlEncode($Prov->PrvMail->getPlaceHolder()) ?>" value="<?php echo $Prov->PrvMail->EditValue ?>"<?php echo $Prov->PrvMail->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_Prov_PrvMail">
<span<?php echo $Prov->PrvMail->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $Prov->PrvMail->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="Prov" data-field="x_PrvMail" name="x_PrvMail" id="x_PrvMail" value="<?php echo ew_HtmlEncode($Prov->PrvMail->FormValue) ?>">
<?php } ?>
<?php echo $Prov->PrvMail->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($Prov->PrvTele->Visible) { // PrvTele ?>
	<div id="r_PrvTele" class="form-group">
		<label id="elh_Prov_PrvTele" for="x_PrvTele" class="col-sm-2 control-label ewLabel"><?php echo $Prov->PrvTele->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $Prov->PrvTele->CellAttributes() ?>>
<?php if ($Prov->CurrentAction <> "F") { ?>
<span id="el_Prov_PrvTele">
<input type="text" data-table="Prov" data-field="x_PrvTele" name="x_PrvTele" id="x_PrvTele" size="30" placeholder="<?php echo ew_HtmlEncode($Prov->PrvTele->getPlaceHolder()) ?>" value="<?php echo $Prov->PrvTele->EditValue ?>"<?php echo $Prov->PrvTele->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_Prov_PrvTele">
<span<?php echo $Prov->PrvTele->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $Prov->PrvTele->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="Prov" data-field="x_PrvTele" name="x_PrvTele" id="x_PrvTele" value="<?php echo ew_HtmlEncode($Prov->PrvTele->FormValue) ?>">
<?php } ?>
<?php echo $Prov->PrvTele->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($Prov->PrvRSoc->Visible) { // PrvRSoc ?>
	<div id="r_PrvRSoc" class="form-group">
		<label id="elh_Prov_PrvRSoc" for="x_PrvRSoc" class="col-sm-2 control-label ewLabel"><?php echo $Prov->PrvRSoc->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $Prov->PrvRSoc->CellAttributes() ?>>
<?php if ($Prov->CurrentAction <> "F") { ?>
<span id="el_Prov_PrvRSoc">
<input type="text" data-table="Prov" data-field="x_PrvRSoc" name="x_PrvRSoc" id="x_PrvRSoc" size="30" placeholder="<?php echo ew_HtmlEncode($Prov->PrvRSoc->getPlaceHolder()) ?>" value="<?php echo $Prov->PrvRSoc->EditValue ?>"<?php echo $Prov->PrvRSoc->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_Prov_PrvRSoc">
<span<?php echo $Prov->PrvRSoc->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $Prov->PrvRSoc->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="Prov" data-field="x_PrvRSoc" name="x_PrvRSoc" id="x_PrvRSoc" value="<?php echo ew_HtmlEncode($Prov->PrvRSoc->FormValue) ?>">
<?php } ?>
<?php echo $Prov->PrvRSoc->CustomMsg ?></div></div>
	</div>
<?php } ?>
</div>
<input type="hidden" data-table="Prov" data-field="x_PrvCodi" name="x_PrvCodi" id="x_PrvCodi" value="<?php echo ew_HtmlEncode($Prov->PrvCodi->CurrentValue) ?>">
<div class="form-group">
	<div class="col-sm-offset-2 col-sm-10">
<?php if ($Prov->CurrentAction <> "F") { // Confirm page ?>
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit" onclick="this.form.a_edit.value='F';"><?php echo $Language->Phrase("SaveBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="button" data-href="<?php echo $Prov_edit->getReturnUrl() ?>"><?php echo $Language->Phrase("CancelBtn") ?></button>
<?php } else { ?>
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("ConfirmBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="submit" onclick="this.form.a_edit.value='X';"><?php echo $Language->Phrase("CancelBtn") ?></button>
<?php } ?>
	</div>
</div>
</form>
<script type="text/javascript">
fProvedit.Init();
</script>
<?php
$Prov_edit->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$Prov_edit->Page_Terminate();
?>
