<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "OVeninfo.php" ?>
<?php include_once "Usuainfo.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$OVen_delete = NULL; // Initialize page object first

class cOVen_delete extends cOVen {

	// Page ID
	var $PageID = 'delete';

	// Project ID
	var $ProjectID = "{04439FF7-B43F-460F-8514-F71C8FF9E679}";

	// Table name
	var $TableName = 'OVen';

	// Page object name
	var $PageObjName = 'OVen_delete';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (OVen)
		if (!isset($GLOBALS["OVen"]) || get_class($GLOBALS["OVen"]) == "cOVen") {
			$GLOBALS["OVen"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["OVen"];
		}

		// Table object (Usua)
		if (!isset($GLOBALS['Usua'])) $GLOBALS['Usua'] = new cUsua();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'delete', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'OVen', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (Usua)
		if (!isset($UserTable)) {
			$UserTable = new cUsua();
			$UserTableConn = Conn($UserTable->DBID);
		}
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanDelete()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage($Language->Phrase("NoPermission")); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("OVenlist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action
		$this->OveCodi->Visible = !$this->IsAdd() && !$this->IsCopy() && !$this->IsGridAdd();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $OVen;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($OVen);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $DbMasterFilter = "";
	var $DbDetailFilter = "";
	var $StartRec;
	var $TotalRecs = 0;
	var $RecCnt;
	var $RecKeys = array();
	var $Recordset;
	var $StartRowCnt = 1;
	var $RowCnt = 0;

	//
	// Page main
	//
	function Page_Main() {
		global $Language;

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Load key parameters
		$this->RecKeys = $this->GetRecordKeys(); // Load record keys
		$sFilter = $this->GetKeyFilter();
		if ($sFilter == "")
			$this->Page_Terminate("OVenlist.php"); // Prevent SQL injection, return to list

		// Set up filter (SQL WHHERE clause) and get return SQL
		// SQL constructor in OVen class, OVeninfo.php

		$this->CurrentFilter = $sFilter;

		// Get action
		if (@$_POST["a_delete"] <> "") {
			$this->CurrentAction = $_POST["a_delete"];
		} else {
			$this->CurrentAction = "I"; // Display record
		}
		switch ($this->CurrentAction) {
			case "D": // Delete
				$this->SendEmail = TRUE; // Send email on delete success
				if ($this->DeleteRows()) { // Delete rows
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage($Language->Phrase("DeleteSuccess")); // Set up success message
					$this->Page_Terminate($this->getReturnUrl()); // Return to caller
				}
		}
	}

	// Load recordset
	function LoadRecordset($offset = -1, $rowcnt = -1) {

		// Load List page SQL
		$sSql = $this->SelectSQL();
		$conn = &$this->Connection();

		// Load recordset
		$dbtype = ew_GetConnectionType($this->DBID);
		if ($this->UseSelectLimit) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			if ($dbtype == "MSSQL") {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset, array("_hasOrderBy" => trim($this->getOrderBy()) || trim($this->getSessionOrderBy())));
			} else {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset);
			}
			$conn->raiseErrorFn = '';
		} else {
			$rs = ew_LoadRecordset($sSql, $conn);
		}

		// Call Recordset Selected event
		$this->Recordset_Selected($rs);
		return $rs;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->OveCodi->setDbValue($rs->fields('OveCodi'));
		$this->ProCodi->setDbValue($rs->fields('ProCodi'));
		$this->OvePIni->setDbValue($rs->fields('OvePIni'));
		$this->OveCPro->setDbValue($rs->fields('OveCPro'));
		$this->OveUsua->setDbValue($rs->fields('OveUsua'));
		$this->OveFCre->setDbValue($rs->fields('OveFCre'));
		$this->OveCVen->setDbValue($rs->fields('OveCVen'));
		$this->CliCodi->setDbValue($rs->fields('CliCodi'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->OveCodi->DbValue = $row['OveCodi'];
		$this->ProCodi->DbValue = $row['ProCodi'];
		$this->OvePIni->DbValue = $row['OvePIni'];
		$this->OveCPro->DbValue = $row['OveCPro'];
		$this->OveUsua->DbValue = $row['OveUsua'];
		$this->OveFCre->DbValue = $row['OveFCre'];
		$this->OveCVen->DbValue = $row['OveCVen'];
		$this->CliCodi->DbValue = $row['CliCodi'];
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		// Convert decimal values if posted back

		if ($this->OvePIni->FormValue == $this->OvePIni->CurrentValue && is_numeric(ew_StrToFloat($this->OvePIni->CurrentValue)))
			$this->OvePIni->CurrentValue = ew_StrToFloat($this->OvePIni->CurrentValue);

		// Call Row_Rendering event
		$this->Row_Rendering();

		// Common render codes for all row types
		// OveCodi
		// ProCodi
		// OvePIni
		// OveCPro
		// OveUsua
		// OveFCre
		// OveCVen
		// CliCodi

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// OveCodi
		$this->OveCodi->ViewValue = $this->OveCodi->CurrentValue;
		$this->OveCodi->ViewCustomAttributes = "";

		// ProCodi
		if (strval($this->ProCodi->CurrentValue) <> "") {
			$sFilterWrk = "\"ProdCodi\"" . ew_SearchString("=", $this->ProCodi->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT \"ProdCodi\", \"CprCodi\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"Prod\"";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->ProCodi, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->ProCodi->ViewValue = $this->ProCodi->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->ProCodi->ViewValue = $this->ProCodi->CurrentValue;
			}
		} else {
			$this->ProCodi->ViewValue = NULL;
		}
		$this->ProCodi->ViewCustomAttributes = "";

		// OvePIni
		$this->OvePIni->ViewValue = $this->OvePIni->CurrentValue;
		$this->OvePIni->ViewCustomAttributes = "";

		// OveCPro
		$this->OveCPro->ViewValue = $this->OveCPro->CurrentValue;
		$this->OveCPro->ViewCustomAttributes = "";

		// OveUsua
		$this->OveUsua->ViewValue = $this->OveUsua->CurrentValue;
		$this->OveUsua->ViewCustomAttributes = "";

		// OveFCre
		$this->OveFCre->ViewValue = $this->OveFCre->CurrentValue;
		$this->OveFCre->ViewValue = ew_FormatDateTime($this->OveFCre->ViewValue, 7);
		$this->OveFCre->ViewCustomAttributes = "";

		// OveCVen
		$this->OveCVen->ViewValue = $this->OveCVen->CurrentValue;
		$this->OveCVen->ViewCustomAttributes = "";

		// CliCodi
		if (strval($this->CliCodi->CurrentValue) <> "") {
			$sFilterWrk = "\"CliCodi\"" . ew_SearchString("=", $this->CliCodi->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT \"CliCodi\", \"CliNomb\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"Clie\"";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->CliCodi, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->CliCodi->ViewValue = $this->CliCodi->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->CliCodi->ViewValue = $this->CliCodi->CurrentValue;
			}
		} else {
			$this->CliCodi->ViewValue = NULL;
		}
		$this->CliCodi->ViewCustomAttributes = "";

			// OveCodi
			$this->OveCodi->LinkCustomAttributes = "";
			$this->OveCodi->HrefValue = "";
			$this->OveCodi->TooltipValue = "";

			// ProCodi
			$this->ProCodi->LinkCustomAttributes = "";
			$this->ProCodi->HrefValue = "";
			$this->ProCodi->TooltipValue = "";

			// OvePIni
			$this->OvePIni->LinkCustomAttributes = "";
			$this->OvePIni->HrefValue = "";
			$this->OvePIni->TooltipValue = "";

			// OveCPro
			$this->OveCPro->LinkCustomAttributes = "";
			$this->OveCPro->HrefValue = "";
			$this->OveCPro->TooltipValue = "";

			// OveUsua
			$this->OveUsua->LinkCustomAttributes = "";
			$this->OveUsua->HrefValue = "";
			$this->OveUsua->TooltipValue = "";

			// OveFCre
			$this->OveFCre->LinkCustomAttributes = "";
			$this->OveFCre->HrefValue = "";
			$this->OveFCre->TooltipValue = "";

			// OveCVen
			$this->OveCVen->LinkCustomAttributes = "";
			$this->OveCVen->HrefValue = "";
			$this->OveCVen->TooltipValue = "";

			// CliCodi
			$this->CliCodi->LinkCustomAttributes = "";
			$this->CliCodi->HrefValue = "";
			$this->CliCodi->TooltipValue = "";
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	//
	// Delete records based on current filter
	//
	function DeleteRows() {
		global $Language, $Security;
		if (!$Security->CanDelete()) {
			$this->setFailureMessage($Language->Phrase("NoDeletePermission")); // No delete permission
			return FALSE;
		}
		$DeleteRows = TRUE;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE) {
			return FALSE;
		} elseif ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
			$rs->Close();
			return FALSE;

		//} else {
		//	$this->LoadRowValues($rs); // Load row values

		}
		$rows = ($rs) ? $rs->GetRows() : array();
		$conn->BeginTrans();

		// Clone old rows
		$rsold = $rows;
		if ($rs)
			$rs->Close();

		// Call row deleting event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$DeleteRows = $this->Row_Deleting($row);
				if (!$DeleteRows) break;
			}
		}
		if ($DeleteRows) {
			$sKey = "";
			foreach ($rsold as $row) {
				$sThisKey = "";
				if ($sThisKey <> "") $sThisKey .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
				$sThisKey .= $row['OveCodi'];
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				$DeleteRows = $this->Delete($row); // Delete
				$conn->raiseErrorFn = '';
				if ($DeleteRows === FALSE)
					break;
				if ($sKey <> "") $sKey .= ", ";
				$sKey .= $sThisKey;
			}
		} else {

			// Set up error message
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("DeleteCancelled"));
			}
		}
		if ($DeleteRows) {
			$conn->CommitTrans(); // Commit the changes
		} else {
			$conn->RollbackTrans(); // Rollback changes
		}

		// Call Row Deleted event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$this->Row_Deleted($row);
			}
		}
		return $DeleteRows;
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, "OVenlist.php", "", $this->TableVar, TRUE);
		$PageId = "delete";
		$Breadcrumb->Add("delete", $PageId, $url);
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($OVen_delete)) $OVen_delete = new cOVen_delete();

// Page init
$OVen_delete->Page_Init();

// Page main
$OVen_delete->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$OVen_delete->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "delete";
var CurrentForm = fOVendelete = new ew_Form("fOVendelete", "delete");

// Form_CustomValidate event
fOVendelete.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fOVendelete.ValidateRequired = true;
<?php } else { ?>
fOVendelete.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
fOVendelete.Lists["x_ProCodi"] = {"LinkField":"x_ProdCodi","Ajax":true,"AutoFill":false,"DisplayFields":["x_CprCodi","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fOVendelete.Lists["x_CliCodi"] = {"LinkField":"x_CliCodi","Ajax":true,"AutoFill":false,"DisplayFields":["x_CliNomb","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};

// Form object for search
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<?php

// Load records for display
if ($OVen_delete->Recordset = $OVen_delete->LoadRecordset())
	$OVen_deleteTotalRecs = $OVen_delete->Recordset->RecordCount(); // Get record count
if ($OVen_deleteTotalRecs <= 0) { // No record found, exit
	if ($OVen_delete->Recordset)
		$OVen_delete->Recordset->Close();
	$OVen_delete->Page_Terminate("OVenlist.php"); // Return to list
}
?>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php $OVen_delete->ShowPageHeader(); ?>
<?php
$OVen_delete->ShowMessage();
?>
<form name="fOVendelete" id="fOVendelete" class="form-inline ewForm ewDeleteForm" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($OVen_delete->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $OVen_delete->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="OVen">
<input type="hidden" name="a_delete" id="a_delete" value="D">
<?php foreach ($OVen_delete->RecKeys as $key) { ?>
<?php $keyvalue = is_array($key) ? implode($EW_COMPOSITE_KEY_SEPARATOR, $key) : $key; ?>
<input type="hidden" name="key_m[]" value="<?php echo ew_HtmlEncode($keyvalue) ?>">
<?php } ?>
<div class="ewGrid">
<div class="<?php if (ew_IsResponsiveLayout()) { echo "table-responsive "; } ?>ewGridMiddlePanel">
<table class="table ewTable">
<?php echo $OVen->TableCustomInnerHtml ?>
	<thead>
	<tr class="ewTableHeader">
<?php if ($OVen->OveCodi->Visible) { // OveCodi ?>
		<th><span id="elh_OVen_OveCodi" class="OVen_OveCodi"><?php echo $OVen->OveCodi->FldCaption() ?></span></th>
<?php } ?>
<?php if ($OVen->ProCodi->Visible) { // ProCodi ?>
		<th><span id="elh_OVen_ProCodi" class="OVen_ProCodi"><?php echo $OVen->ProCodi->FldCaption() ?></span></th>
<?php } ?>
<?php if ($OVen->OvePIni->Visible) { // OvePIni ?>
		<th><span id="elh_OVen_OvePIni" class="OVen_OvePIni"><?php echo $OVen->OvePIni->FldCaption() ?></span></th>
<?php } ?>
<?php if ($OVen->OveCPro->Visible) { // OveCPro ?>
		<th><span id="elh_OVen_OveCPro" class="OVen_OveCPro"><?php echo $OVen->OveCPro->FldCaption() ?></span></th>
<?php } ?>
<?php if ($OVen->OveUsua->Visible) { // OveUsua ?>
		<th><span id="elh_OVen_OveUsua" class="OVen_OveUsua"><?php echo $OVen->OveUsua->FldCaption() ?></span></th>
<?php } ?>
<?php if ($OVen->OveFCre->Visible) { // OveFCre ?>
		<th><span id="elh_OVen_OveFCre" class="OVen_OveFCre"><?php echo $OVen->OveFCre->FldCaption() ?></span></th>
<?php } ?>
<?php if ($OVen->OveCVen->Visible) { // OveCVen ?>
		<th><span id="elh_OVen_OveCVen" class="OVen_OveCVen"><?php echo $OVen->OveCVen->FldCaption() ?></span></th>
<?php } ?>
<?php if ($OVen->CliCodi->Visible) { // CliCodi ?>
		<th><span id="elh_OVen_CliCodi" class="OVen_CliCodi"><?php echo $OVen->CliCodi->FldCaption() ?></span></th>
<?php } ?>
	</tr>
	</thead>
	<tbody>
<?php
$OVen_delete->RecCnt = 0;
$i = 0;
while (!$OVen_delete->Recordset->EOF) {
	$OVen_delete->RecCnt++;
	$OVen_delete->RowCnt++;

	// Set row properties
	$OVen->ResetAttrs();
	$OVen->RowType = EW_ROWTYPE_VIEW; // View

	// Get the field contents
	$OVen_delete->LoadRowValues($OVen_delete->Recordset);

	// Render row
	$OVen_delete->RenderRow();
?>
	<tr<?php echo $OVen->RowAttributes() ?>>
<?php if ($OVen->OveCodi->Visible) { // OveCodi ?>
		<td<?php echo $OVen->OveCodi->CellAttributes() ?>>
<span id="el<?php echo $OVen_delete->RowCnt ?>_OVen_OveCodi" class="OVen_OveCodi">
<span<?php echo $OVen->OveCodi->ViewAttributes() ?>>
<?php echo $OVen->OveCodi->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($OVen->ProCodi->Visible) { // ProCodi ?>
		<td<?php echo $OVen->ProCodi->CellAttributes() ?>>
<span id="el<?php echo $OVen_delete->RowCnt ?>_OVen_ProCodi" class="OVen_ProCodi">
<span<?php echo $OVen->ProCodi->ViewAttributes() ?>>
<?php echo $OVen->ProCodi->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($OVen->OvePIni->Visible) { // OvePIni ?>
		<td<?php echo $OVen->OvePIni->CellAttributes() ?>>
<span id="el<?php echo $OVen_delete->RowCnt ?>_OVen_OvePIni" class="OVen_OvePIni">
<span<?php echo $OVen->OvePIni->ViewAttributes() ?>>
<?php echo $OVen->OvePIni->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($OVen->OveCPro->Visible) { // OveCPro ?>
		<td<?php echo $OVen->OveCPro->CellAttributes() ?>>
<span id="el<?php echo $OVen_delete->RowCnt ?>_OVen_OveCPro" class="OVen_OveCPro">
<span<?php echo $OVen->OveCPro->ViewAttributes() ?>>
<?php echo $OVen->OveCPro->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($OVen->OveUsua->Visible) { // OveUsua ?>
		<td<?php echo $OVen->OveUsua->CellAttributes() ?>>
<span id="el<?php echo $OVen_delete->RowCnt ?>_OVen_OveUsua" class="OVen_OveUsua">
<span<?php echo $OVen->OveUsua->ViewAttributes() ?>>
<?php echo $OVen->OveUsua->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($OVen->OveFCre->Visible) { // OveFCre ?>
		<td<?php echo $OVen->OveFCre->CellAttributes() ?>>
<span id="el<?php echo $OVen_delete->RowCnt ?>_OVen_OveFCre" class="OVen_OveFCre">
<span<?php echo $OVen->OveFCre->ViewAttributes() ?>>
<?php echo $OVen->OveFCre->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($OVen->OveCVen->Visible) { // OveCVen ?>
		<td<?php echo $OVen->OveCVen->CellAttributes() ?>>
<span id="el<?php echo $OVen_delete->RowCnt ?>_OVen_OveCVen" class="OVen_OveCVen">
<span<?php echo $OVen->OveCVen->ViewAttributes() ?>>
<?php echo $OVen->OveCVen->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($OVen->CliCodi->Visible) { // CliCodi ?>
		<td<?php echo $OVen->CliCodi->CellAttributes() ?>>
<span id="el<?php echo $OVen_delete->RowCnt ?>_OVen_CliCodi" class="OVen_CliCodi">
<span<?php echo $OVen->CliCodi->ViewAttributes() ?>>
<?php echo $OVen->CliCodi->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
	</tr>
<?php
	$OVen_delete->Recordset->MoveNext();
}
$OVen_delete->Recordset->Close();
?>
</tbody>
</table>
</div>
</div>
<div>
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("DeleteBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="button" data-href="<?php echo $OVen_delete->getReturnUrl() ?>"><?php echo $Language->Phrase("CancelBtn") ?></button>
</div>
</form>
<script type="text/javascript">
fOVendelete.Init();
</script>
<?php
$OVen_delete->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$OVen_delete->Page_Terminate();
?>
