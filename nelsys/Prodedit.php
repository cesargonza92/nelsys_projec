<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "Prodinfo.php" ?>
<?php include_once "Usuainfo.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$Prod_edit = NULL; // Initialize page object first

class cProd_edit extends cProd {

	// Page ID
	var $PageID = 'edit';

	// Project ID
	var $ProjectID = "{04439FF7-B43F-460F-8514-F71C8FF9E679}";

	// Table name
	var $TableName = 'Prod';

	// Page object name
	var $PageObjName = 'Prod_edit';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (Prod)
		if (!isset($GLOBALS["Prod"]) || get_class($GLOBALS["Prod"]) == "cProd") {
			$GLOBALS["Prod"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["Prod"];
		}

		// Table object (Usua)
		if (!isset($GLOBALS['Usua'])) $GLOBALS['Usua'] = new cUsua();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'edit', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'Prod', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (Usua)
		if (!isset($UserTable)) {
			$UserTable = new cUsua();
			$UserTableConn = Conn($UserTable->DBID);
		}
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanEdit()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage($Language->Phrase("NoPermission")); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("Prodlist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}

		// Create form object
		$objForm = new cFormObj();
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Process auto fill
		if (@$_POST["ajax"] == "autofill") {
			$results = $this->GetAutoFill(@$_POST["name"], @$_POST["q"]);
			if ($results) {

				// Clean output buffer
				if (!EW_DEBUG_ENABLED && ob_get_length())
					ob_end_clean();
				echo $results;
				$this->Page_Terminate();
				exit();
			}
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $Prod;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($Prod);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $FormClassName = "form-horizontal ewForm ewEditForm";
	var $DbMasterFilter;
	var $DbDetailFilter;

	// 
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError;

		// Load key from QueryString
		if (@$_GET["ProdCodi"] <> "") {
			$this->ProdCodi->setQueryStringValue($_GET["ProdCodi"]);
		}

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Process form if post back
		if (@$_POST["a_edit"] <> "") {
			$this->CurrentAction = $_POST["a_edit"]; // Get action code
			$this->LoadFormValues(); // Get form values
		} else {
			$this->CurrentAction = "I"; // Default action is display
		}

		// Check if valid key
		if ($this->ProdCodi->CurrentValue == "")
			$this->Page_Terminate("Prodlist.php"); // Invalid key, return to list

		// Validate form if post back
		if (@$_POST["a_edit"] <> "") {
			if (!$this->ValidateForm()) {
				$this->CurrentAction = ""; // Form error, reset action
				$this->setFailureMessage($gsFormError);
				$this->EventCancelled = TRUE; // Event cancelled
				$this->RestoreFormValues();
			}
		}
		switch ($this->CurrentAction) {
			case "I": // Get a record to display
				if (!$this->LoadRow()) { // Load record based on key
					if ($this->getFailureMessage() == "") $this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
					$this->Page_Terminate("Prodlist.php"); // No matching record, return to list
				}
				break;
			Case "U": // Update
				$sReturnUrl = $this->getReturnUrl();
				$this->SendEmail = TRUE; // Send email on update success
				if ($this->EditRow()) { // Update record based on key
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage($Language->Phrase("UpdateSuccess")); // Update success
					$this->Page_Terminate($sReturnUrl); // Return to caller
				} elseif ($this->getFailureMessage() == $Language->Phrase("NoRecord")) {
					$this->Page_Terminate($sReturnUrl); // Return to caller
				} else {
					$this->EventCancelled = TRUE; // Event cancelled
					$this->RestoreFormValues(); // Restore form values if update failed
				}
		}

		// Render the record
		if ($this->CurrentAction == "F") { // Confirm page
			$this->RowType = EW_ROWTYPE_VIEW; // Render as View
		} else {
			$this->RowType = EW_ROWTYPE_EDIT; // Render as Edit
		}
		$this->ResetAttrs();
		$this->RenderRow();
	}

	// Set up starting record parameters
	function SetUpStartRec() {
		if ($this->DisplayRecs == 0)
			return;
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET[EW_TABLE_START_REC] <> "") { // Check for "start" parameter
				$this->StartRec = $_GET[EW_TABLE_START_REC];
				$this->setStartRecordNumber($this->StartRec);
			} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
				$PageNo = $_GET[EW_TABLE_PAGE_NO];
				if (is_numeric($PageNo)) {
					$this->StartRec = ($PageNo-1)*$this->DisplayRecs+1;
					if ($this->StartRec <= 0) {
						$this->StartRec = 1;
					} elseif ($this->StartRec >= intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1) {
						$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1;
					}
					$this->setStartRecordNumber($this->StartRec);
				}
			}
		}
		$this->StartRec = $this->getStartRecordNumber();

		// Check if correct start record counter
		if (!is_numeric($this->StartRec) || $this->StartRec == "") { // Avoid invalid start record counter
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} elseif (intval($this->StartRec) > intval($this->TotalRecs)) { // Avoid starting record > total records
			$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to last page first record
			$this->setStartRecordNumber($this->StartRec);
		} elseif (($this->StartRec-1) % $this->DisplayRecs <> 0) {
			$this->StartRec = intval(($this->StartRec-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to page boundary
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Get upload files
	function GetUploadFiles() {
		global $objForm, $Language;

		// Get upload data
	}

	// Load form values
	function LoadFormValues() {

		// Load from form
		global $objForm;
		if (!$this->ProCant->FldIsDetailKey) {
			$this->ProCant->setFormValue($objForm->GetValue("x_ProCant"));
		}
		if (!$this->ProNMin->FldIsDetailKey) {
			$this->ProNMin->setFormValue($objForm->GetValue("x_ProNMin"));
		}
		if (!$this->ProPrec->FldIsDetailKey) {
			$this->ProPrec->setFormValue($objForm->GetValue("x_ProPrec"));
		}
		if (!$this->ProDesc->FldIsDetailKey) {
			$this->ProDesc->setFormValue($objForm->GetValue("x_ProDesc"));
		}
		if (!$this->ProdCodi->FldIsDetailKey)
			$this->ProdCodi->setFormValue($objForm->GetValue("x_ProdCodi"));
	}

	// Restore form values
	function RestoreFormValues() {
		global $objForm;
		$this->LoadRow();
		$this->ProdCodi->CurrentValue = $this->ProdCodi->FormValue;
		$this->ProCant->CurrentValue = $this->ProCant->FormValue;
		$this->ProNMin->CurrentValue = $this->ProNMin->FormValue;
		$this->ProPrec->CurrentValue = $this->ProPrec->FormValue;
		$this->ProDesc->CurrentValue = $this->ProDesc->FormValue;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->ProdCodi->setDbValue($rs->fields('ProdCodi'));
		$this->CprCodi->setDbValue($rs->fields('CprCodi'));
		$this->MprCodi->setDbValue($rs->fields('MprCodi'));
		$this->TprCodi->setDbValue($rs->fields('TprCodi'));
		$this->ProCant->setDbValue($rs->fields('ProCant'));
		$this->ProNMin->setDbValue($rs->fields('ProNMin'));
		$this->ProPrec->setDbValue($rs->fields('ProPrec'));
		$this->ProDesc->setDbValue($rs->fields('ProDesc'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->ProdCodi->DbValue = $row['ProdCodi'];
		$this->CprCodi->DbValue = $row['CprCodi'];
		$this->MprCodi->DbValue = $row['MprCodi'];
		$this->TprCodi->DbValue = $row['TprCodi'];
		$this->ProCant->DbValue = $row['ProCant'];
		$this->ProNMin->DbValue = $row['ProNMin'];
		$this->ProPrec->DbValue = $row['ProPrec'];
		$this->ProDesc->DbValue = $row['ProDesc'];
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		// Convert decimal values if posted back

		if ($this->ProPrec->FormValue == $this->ProPrec->CurrentValue && is_numeric(ew_StrToFloat($this->ProPrec->CurrentValue)))
			$this->ProPrec->CurrentValue = ew_StrToFloat($this->ProPrec->CurrentValue);

		// Call Row_Rendering event
		$this->Row_Rendering();

		// Common render codes for all row types
		// ProdCodi
		// CprCodi
		// MprCodi
		// TprCodi
		// ProCant
		// ProNMin
		// ProPrec
		// ProDesc

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// ProdCodi
		$this->ProdCodi->ViewValue = $this->ProdCodi->CurrentValue;
		$this->ProdCodi->ViewCustomAttributes = "";

		// CprCodi
		if (strval($this->CprCodi->CurrentValue) <> "") {
			$sFilterWrk = "\"CprCodi\"" . ew_SearchString("=", $this->CprCodi->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT \"CprCodi\", \"CprNomb\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"CPro\"";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->CprCodi, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->CprCodi->ViewValue = $this->CprCodi->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->CprCodi->ViewValue = $this->CprCodi->CurrentValue;
			}
		} else {
			$this->CprCodi->ViewValue = NULL;
		}
		$this->CprCodi->ViewCustomAttributes = "";

		// MprCodi
		if (strval($this->MprCodi->CurrentValue) <> "") {
			$sFilterWrk = "\"MprCodi\"" . ew_SearchString("=", $this->MprCodi->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT \"MprCodi\", \"MprNomb\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"Mpro\"";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->MprCodi, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->MprCodi->ViewValue = $this->MprCodi->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->MprCodi->ViewValue = $this->MprCodi->CurrentValue;
			}
		} else {
			$this->MprCodi->ViewValue = NULL;
		}
		$this->MprCodi->ViewCustomAttributes = "";

		// TprCodi
		if (strval($this->TprCodi->CurrentValue) <> "") {
			$sFilterWrk = "\"TprCodi\"" . ew_SearchString("=", $this->TprCodi->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT \"TprCodi\", \"TprNomb\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"TProd\"";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->TprCodi, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->TprCodi->ViewValue = $this->TprCodi->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->TprCodi->ViewValue = $this->TprCodi->CurrentValue;
			}
		} else {
			$this->TprCodi->ViewValue = NULL;
		}
		$this->TprCodi->ViewCustomAttributes = "";

		// ProCant
		$this->ProCant->ViewValue = $this->ProCant->CurrentValue;
		$this->ProCant->ViewCustomAttributes = "";

		// ProNMin
		$this->ProNMin->ViewValue = $this->ProNMin->CurrentValue;
		$this->ProNMin->ViewCustomAttributes = "";

		// ProPrec
		$this->ProPrec->ViewValue = $this->ProPrec->CurrentValue;
		$this->ProPrec->ViewCustomAttributes = "";

		// ProDesc
		$this->ProDesc->ViewValue = $this->ProDesc->CurrentValue;
		$this->ProDesc->ViewCustomAttributes = "";

			// ProCant
			$this->ProCant->LinkCustomAttributes = "";
			$this->ProCant->HrefValue = "";
			$this->ProCant->TooltipValue = "";

			// ProNMin
			$this->ProNMin->LinkCustomAttributes = "";
			$this->ProNMin->HrefValue = "";
			$this->ProNMin->TooltipValue = "";

			// ProPrec
			$this->ProPrec->LinkCustomAttributes = "";
			$this->ProPrec->HrefValue = "";
			$this->ProPrec->TooltipValue = "";

			// ProDesc
			$this->ProDesc->LinkCustomAttributes = "";
			$this->ProDesc->HrefValue = "";
			$this->ProDesc->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_EDIT) { // Edit row

			// ProCant
			$this->ProCant->EditAttrs["class"] = "form-control";
			$this->ProCant->EditCustomAttributes = "";
			$this->ProCant->EditValue = ew_HtmlEncode($this->ProCant->CurrentValue);
			$this->ProCant->PlaceHolder = ew_RemoveHtml($this->ProCant->FldCaption());

			// ProNMin
			$this->ProNMin->EditAttrs["class"] = "form-control";
			$this->ProNMin->EditCustomAttributes = "";
			$this->ProNMin->EditValue = ew_HtmlEncode($this->ProNMin->CurrentValue);
			$this->ProNMin->PlaceHolder = ew_RemoveHtml($this->ProNMin->FldCaption());

			// ProPrec
			$this->ProPrec->EditAttrs["class"] = "form-control";
			$this->ProPrec->EditCustomAttributes = "";
			$this->ProPrec->EditValue = ew_HtmlEncode($this->ProPrec->CurrentValue);
			$this->ProPrec->PlaceHolder = ew_RemoveHtml($this->ProPrec->FldCaption());
			if (strval($this->ProPrec->EditValue) <> "" && is_numeric($this->ProPrec->EditValue)) $this->ProPrec->EditValue = ew_FormatNumber($this->ProPrec->EditValue, -2, -1, -2, 0);

			// ProDesc
			$this->ProDesc->EditAttrs["class"] = "form-control";
			$this->ProDesc->EditCustomAttributes = "";
			$this->ProDesc->EditValue = ew_HtmlEncode($this->ProDesc->CurrentValue);
			$this->ProDesc->PlaceHolder = ew_RemoveHtml($this->ProDesc->FldCaption());

			// Edit refer script
			// ProCant

			$this->ProCant->HrefValue = "";

			// ProNMin
			$this->ProNMin->HrefValue = "";

			// ProPrec
			$this->ProPrec->HrefValue = "";

			// ProDesc
			$this->ProDesc->HrefValue = "";
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate form
	function ValidateForm() {
		global $Language, $gsFormError;

		// Initialize form error message
		$gsFormError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return ($gsFormError == "");
		if (!$this->ProCant->FldIsDetailKey && !is_null($this->ProCant->FormValue) && $this->ProCant->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->ProCant->FldCaption(), $this->ProCant->ReqErrMsg));
		}
		if (!ew_CheckInteger($this->ProCant->FormValue)) {
			ew_AddMessage($gsFormError, $this->ProCant->FldErrMsg());
		}
		if (!$this->ProNMin->FldIsDetailKey && !is_null($this->ProNMin->FormValue) && $this->ProNMin->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->ProNMin->FldCaption(), $this->ProNMin->ReqErrMsg));
		}
		if (!ew_CheckInteger($this->ProNMin->FormValue)) {
			ew_AddMessage($gsFormError, $this->ProNMin->FldErrMsg());
		}
		if (!$this->ProPrec->FldIsDetailKey && !is_null($this->ProPrec->FormValue) && $this->ProPrec->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->ProPrec->FldCaption(), $this->ProPrec->ReqErrMsg));
		}
		if (!ew_CheckNumber($this->ProPrec->FormValue)) {
			ew_AddMessage($gsFormError, $this->ProPrec->FldErrMsg());
		}

		// Return validate result
		$ValidateForm = ($gsFormError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateForm = $ValidateForm && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsFormError, $sFormCustomError);
		}
		return $ValidateForm;
	}

	// Update record based on key values
	function EditRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$conn = &$this->Connection();
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE)
			return FALSE;
		if ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record message
			$EditRow = FALSE; // Update Failed
		} else {

			// Save old values
			$rsold = &$rs->fields;
			$this->LoadDbValues($rsold);
			$rsnew = array();

			// ProCant
			$this->ProCant->SetDbValueDef($rsnew, $this->ProCant->CurrentValue, 0, $this->ProCant->ReadOnly);

			// ProNMin
			$this->ProNMin->SetDbValueDef($rsnew, $this->ProNMin->CurrentValue, 0, $this->ProNMin->ReadOnly);

			// ProPrec
			$this->ProPrec->SetDbValueDef($rsnew, $this->ProPrec->CurrentValue, 0, $this->ProPrec->ReadOnly);

			// ProDesc
			$this->ProDesc->SetDbValueDef($rsnew, $this->ProDesc->CurrentValue, NULL, $this->ProDesc->ReadOnly);

			// Call Row Updating event
			$bUpdateRow = $this->Row_Updating($rsold, $rsnew);
			if ($bUpdateRow) {
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				if (count($rsnew) > 0)
					$EditRow = $this->Update($rsnew, "", $rsold);
				else
					$EditRow = TRUE; // No field to update
				$conn->raiseErrorFn = '';
				if ($EditRow) {
				}
			} else {
				if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

					// Use the message, do nothing
				} elseif ($this->CancelMessage <> "") {
					$this->setFailureMessage($this->CancelMessage);
					$this->CancelMessage = "";
				} else {
					$this->setFailureMessage($Language->Phrase("UpdateCancelled"));
				}
				$EditRow = FALSE;
			}
		}

		// Call Row_Updated event
		if ($EditRow)
			$this->Row_Updated($rsold, $rsnew);
		$rs->Close();
		return $EditRow;
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, "Prodlist.php", "", $this->TableVar, TRUE);
		$PageId = "edit";
		$Breadcrumb->Add("edit", $PageId, $url);
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($Prod_edit)) $Prod_edit = new cProd_edit();

// Page init
$Prod_edit->Page_Init();

// Page main
$Prod_edit->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$Prod_edit->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "edit";
var CurrentForm = fProdedit = new ew_Form("fProdedit", "edit");

// Validate form
fProdedit.Validate = function() {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.GetForm(), $fobj = $(fobj);
	if ($fobj.find("#a_confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.FormKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = $fobj.find("#a_list").val() == "gridinsert";
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
			elm = this.GetElements("x" + infix + "_ProCant");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $Prod->ProCant->FldCaption(), $Prod->ProCant->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_ProCant");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($Prod->ProCant->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_ProNMin");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $Prod->ProNMin->FldCaption(), $Prod->ProNMin->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_ProNMin");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($Prod->ProNMin->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_ProPrec");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $Prod->ProPrec->FldCaption(), $Prod->ProPrec->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_ProPrec");
			if (elm && !ew_CheckNumber(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($Prod->ProPrec->FldErrMsg()) ?>");

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
	}

	// Process detail forms
	var dfs = $fobj.find("input[name='detailpage']").get();
	for (var i = 0; i < dfs.length; i++) {
		var df = dfs[i], val = df.value;
		if (val && ewForms[val])
			if (!ewForms[val].Validate())
				return false;
	}
	return true;
}

// Form_CustomValidate event
fProdedit.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fProdedit.ValidateRequired = true;
<?php } else { ?>
fProdedit.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
// Form object for search

</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php $Prod_edit->ShowPageHeader(); ?>
<?php
$Prod_edit->ShowMessage();
?>
<form name="fProdedit" id="fProdedit" class="<?php echo $Prod_edit->FormClassName ?>" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($Prod_edit->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $Prod_edit->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="Prod">
<input type="hidden" name="a_edit" id="a_edit" value="U">
<?php if ($Prod->CurrentAction == "F") { // Confirm page ?>
<input type="hidden" name="a_confirm" id="a_confirm" value="F">
<?php } ?>
<div>
<?php if ($Prod->ProCant->Visible) { // ProCant ?>
	<div id="r_ProCant" class="form-group">
		<label id="elh_Prod_ProCant" for="x_ProCant" class="col-sm-2 control-label ewLabel"><?php echo $Prod->ProCant->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $Prod->ProCant->CellAttributes() ?>>
<?php if ($Prod->CurrentAction <> "F") { ?>
<span id="el_Prod_ProCant">
<input type="text" data-table="Prod" data-field="x_ProCant" name="x_ProCant" id="x_ProCant" size="30" placeholder="<?php echo ew_HtmlEncode($Prod->ProCant->getPlaceHolder()) ?>" value="<?php echo $Prod->ProCant->EditValue ?>"<?php echo $Prod->ProCant->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_Prod_ProCant">
<span<?php echo $Prod->ProCant->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $Prod->ProCant->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="Prod" data-field="x_ProCant" name="x_ProCant" id="x_ProCant" value="<?php echo ew_HtmlEncode($Prod->ProCant->FormValue) ?>">
<?php } ?>
<?php echo $Prod->ProCant->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($Prod->ProNMin->Visible) { // ProNMin ?>
	<div id="r_ProNMin" class="form-group">
		<label id="elh_Prod_ProNMin" for="x_ProNMin" class="col-sm-2 control-label ewLabel"><?php echo $Prod->ProNMin->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $Prod->ProNMin->CellAttributes() ?>>
<?php if ($Prod->CurrentAction <> "F") { ?>
<span id="el_Prod_ProNMin">
<input type="text" data-table="Prod" data-field="x_ProNMin" name="x_ProNMin" id="x_ProNMin" size="30" placeholder="<?php echo ew_HtmlEncode($Prod->ProNMin->getPlaceHolder()) ?>" value="<?php echo $Prod->ProNMin->EditValue ?>"<?php echo $Prod->ProNMin->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_Prod_ProNMin">
<span<?php echo $Prod->ProNMin->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $Prod->ProNMin->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="Prod" data-field="x_ProNMin" name="x_ProNMin" id="x_ProNMin" value="<?php echo ew_HtmlEncode($Prod->ProNMin->FormValue) ?>">
<?php } ?>
<?php echo $Prod->ProNMin->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($Prod->ProPrec->Visible) { // ProPrec ?>
	<div id="r_ProPrec" class="form-group">
		<label id="elh_Prod_ProPrec" for="x_ProPrec" class="col-sm-2 control-label ewLabel"><?php echo $Prod->ProPrec->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $Prod->ProPrec->CellAttributes() ?>>
<?php if ($Prod->CurrentAction <> "F") { ?>
<span id="el_Prod_ProPrec">
<input type="text" data-table="Prod" data-field="x_ProPrec" name="x_ProPrec" id="x_ProPrec" size="30" placeholder="<?php echo ew_HtmlEncode($Prod->ProPrec->getPlaceHolder()) ?>" value="<?php echo $Prod->ProPrec->EditValue ?>"<?php echo $Prod->ProPrec->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_Prod_ProPrec">
<span<?php echo $Prod->ProPrec->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $Prod->ProPrec->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="Prod" data-field="x_ProPrec" name="x_ProPrec" id="x_ProPrec" value="<?php echo ew_HtmlEncode($Prod->ProPrec->FormValue) ?>">
<?php } ?>
<?php echo $Prod->ProPrec->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($Prod->ProDesc->Visible) { // ProDesc ?>
	<div id="r_ProDesc" class="form-group">
		<label id="elh_Prod_ProDesc" for="x_ProDesc" class="col-sm-2 control-label ewLabel"><?php echo $Prod->ProDesc->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $Prod->ProDesc->CellAttributes() ?>>
<?php if ($Prod->CurrentAction <> "F") { ?>
<span id="el_Prod_ProDesc">
<input type="text" data-table="Prod" data-field="x_ProDesc" name="x_ProDesc" id="x_ProDesc" size="30" placeholder="<?php echo ew_HtmlEncode($Prod->ProDesc->getPlaceHolder()) ?>" value="<?php echo $Prod->ProDesc->EditValue ?>"<?php echo $Prod->ProDesc->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_Prod_ProDesc">
<span<?php echo $Prod->ProDesc->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $Prod->ProDesc->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="Prod" data-field="x_ProDesc" name="x_ProDesc" id="x_ProDesc" value="<?php echo ew_HtmlEncode($Prod->ProDesc->FormValue) ?>">
<?php } ?>
<?php echo $Prod->ProDesc->CustomMsg ?></div></div>
	</div>
<?php } ?>
</div>
<input type="hidden" data-table="Prod" data-field="x_ProdCodi" name="x_ProdCodi" id="x_ProdCodi" value="<?php echo ew_HtmlEncode($Prod->ProdCodi->CurrentValue) ?>">
<div class="form-group">
	<div class="col-sm-offset-2 col-sm-10">
<?php if ($Prod->CurrentAction <> "F") { // Confirm page ?>
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit" onclick="this.form.a_edit.value='F';"><?php echo $Language->Phrase("SaveBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="button" data-href="<?php echo $Prod_edit->getReturnUrl() ?>"><?php echo $Language->Phrase("CancelBtn") ?></button>
<?php } else { ?>
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("ConfirmBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="submit" onclick="this.form.a_edit.value='X';"><?php echo $Language->Phrase("CancelBtn") ?></button>
<?php } ?>
	</div>
</div>
</form>
<script type="text/javascript">
fProdedit.Init();
</script>
<?php
$Prod_edit->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$Prod_edit->Page_Terminate();
?>
