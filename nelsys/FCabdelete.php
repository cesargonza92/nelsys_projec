<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "FCabinfo.php" ?>
<?php include_once "Usuainfo.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$FCab_delete = NULL; // Initialize page object first

class cFCab_delete extends cFCab {

	// Page ID
	var $PageID = 'delete';

	// Project ID
	var $ProjectID = "{04439FF7-B43F-460F-8514-F71C8FF9E679}";

	// Table name
	var $TableName = 'FCab';

	// Page object name
	var $PageObjName = 'FCab_delete';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (FCab)
		if (!isset($GLOBALS["FCab"]) || get_class($GLOBALS["FCab"]) == "cFCab") {
			$GLOBALS["FCab"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["FCab"];
		}

		// Table object (Usua)
		if (!isset($GLOBALS['Usua'])) $GLOBALS['Usua'] = new cUsua();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'delete', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'FCab', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (Usua)
		if (!isset($UserTable)) {
			$UserTable = new cUsua();
			$UserTableConn = Conn($UserTable->DBID);
		}
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanDelete()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage($Language->Phrase("NoPermission")); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("FCablist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action
		$this->FcaCodi->Visible = !$this->IsAdd() && !$this->IsCopy() && !$this->IsGridAdd();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $FCab;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($FCab);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $DbMasterFilter = "";
	var $DbDetailFilter = "";
	var $StartRec;
	var $TotalRecs = 0;
	var $RecCnt;
	var $RecKeys = array();
	var $Recordset;
	var $StartRowCnt = 1;
	var $RowCnt = 0;

	//
	// Page main
	//
	function Page_Main() {
		global $Language;

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Load key parameters
		$this->RecKeys = $this->GetRecordKeys(); // Load record keys
		$sFilter = $this->GetKeyFilter();
		if ($sFilter == "")
			$this->Page_Terminate("FCablist.php"); // Prevent SQL injection, return to list

		// Set up filter (SQL WHHERE clause) and get return SQL
		// SQL constructor in FCab class, FCabinfo.php

		$this->CurrentFilter = $sFilter;

		// Get action
		if (@$_POST["a_delete"] <> "") {
			$this->CurrentAction = $_POST["a_delete"];
		} else {
			$this->CurrentAction = "I"; // Display record
		}
		switch ($this->CurrentAction) {
			case "D": // Delete
				$this->SendEmail = TRUE; // Send email on delete success
				if ($this->DeleteRows()) { // Delete rows
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage($Language->Phrase("DeleteSuccess")); // Set up success message
					$this->Page_Terminate($this->getReturnUrl()); // Return to caller
				}
		}
	}

	// Load recordset
	function LoadRecordset($offset = -1, $rowcnt = -1) {

		// Load List page SQL
		$sSql = $this->SelectSQL();
		$conn = &$this->Connection();

		// Load recordset
		$dbtype = ew_GetConnectionType($this->DBID);
		if ($this->UseSelectLimit) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			if ($dbtype == "MSSQL") {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset, array("_hasOrderBy" => trim($this->getOrderBy()) || trim($this->getSessionOrderBy())));
			} else {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset);
			}
			$conn->raiseErrorFn = '';
		} else {
			$rs = ew_LoadRecordset($sSql, $conn);
		}

		// Call Recordset Selected event
		$this->Recordset_Selected($rs);
		return $rs;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->FcaCodi->setDbValue($rs->fields('FcaCodi'));
		$this->OveCodi->setDbValue($rs->fields('OveCodi'));
		$this->FcaVend->setDbValue($rs->fields('FcaVend'));
		$this->FcaCaje->setDbValue($rs->fields('FcaCaje'));
		$this->FcaFech->setDbValue($rs->fields('FcaFech'));
		$this->FcaTimb->setDbValue($rs->fields('FcaTimb'));
		$this->FcaTFac->setDbValue($rs->fields('FcaTFac'));
		$this->FcaAnul->setDbValue($rs->fields('FcaAnul'));
		$this->FcaMAnu->setDbValue($rs->fields('FcaMAnu'));
		$this->FcaFAnu->setDbValue($rs->fields('FcaFAnu'));
		$this->FcaFTip->setDbValue($rs->fields('FcaFTip'));
		$this->FcaUsua->setDbValue($rs->fields('FcaUsua'));
		$this->FcaFCre->setDbValue($rs->fields('FcaFCre'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->FcaCodi->DbValue = $row['FcaCodi'];
		$this->OveCodi->DbValue = $row['OveCodi'];
		$this->FcaVend->DbValue = $row['FcaVend'];
		$this->FcaCaje->DbValue = $row['FcaCaje'];
		$this->FcaFech->DbValue = $row['FcaFech'];
		$this->FcaTimb->DbValue = $row['FcaTimb'];
		$this->FcaTFac->DbValue = $row['FcaTFac'];
		$this->FcaAnul->DbValue = $row['FcaAnul'];
		$this->FcaMAnu->DbValue = $row['FcaMAnu'];
		$this->FcaFAnu->DbValue = $row['FcaFAnu'];
		$this->FcaFTip->DbValue = $row['FcaFTip'];
		$this->FcaUsua->DbValue = $row['FcaUsua'];
		$this->FcaFCre->DbValue = $row['FcaFCre'];
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		// Convert decimal values if posted back

		if ($this->FcaTFac->FormValue == $this->FcaTFac->CurrentValue && is_numeric(ew_StrToFloat($this->FcaTFac->CurrentValue)))
			$this->FcaTFac->CurrentValue = ew_StrToFloat($this->FcaTFac->CurrentValue);

		// Call Row_Rendering event
		$this->Row_Rendering();

		// Common render codes for all row types
		// FcaCodi
		// OveCodi
		// FcaVend
		// FcaCaje
		// FcaFech
		// FcaTimb
		// FcaTFac
		// FcaAnul
		// FcaMAnu
		// FcaFAnu
		// FcaFTip
		// FcaUsua
		// FcaFCre

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// FcaCodi
		$this->FcaCodi->ViewValue = $this->FcaCodi->CurrentValue;
		$this->FcaCodi->ViewCustomAttributes = "";

		// OveCodi
		if (strval($this->OveCodi->CurrentValue) <> "") {
			$sFilterWrk = "\"OveCodi\"" . ew_SearchString("=", $this->OveCodi->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT \"OveCodi\", \"OveCodi\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"OVen\"";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->OveCodi, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->OveCodi->ViewValue = $this->OveCodi->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->OveCodi->ViewValue = $this->OveCodi->CurrentValue;
			}
		} else {
			$this->OveCodi->ViewValue = NULL;
		}
		$this->OveCodi->ViewCustomAttributes = "";

		// FcaVend
		$this->FcaVend->ViewValue = $this->FcaVend->CurrentValue;
		$this->FcaVend->ViewCustomAttributes = "";

		// FcaCaje
		$this->FcaCaje->ViewValue = $this->FcaCaje->CurrentValue;
		$this->FcaCaje->ViewCustomAttributes = "";

		// FcaFech
		$this->FcaFech->ViewValue = $this->FcaFech->CurrentValue;
		$this->FcaFech->ViewValue = ew_FormatDateTime($this->FcaFech->ViewValue, 7);
		$this->FcaFech->ViewCustomAttributes = "";

		// FcaTimb
		$this->FcaTimb->ViewValue = $this->FcaTimb->CurrentValue;
		$this->FcaTimb->ViewCustomAttributes = "";

		// FcaTFac
		$this->FcaTFac->ViewValue = $this->FcaTFac->CurrentValue;
		$this->FcaTFac->ViewCustomAttributes = "";

		// FcaAnul
		$this->FcaAnul->ViewValue = $this->FcaAnul->CurrentValue;
		$this->FcaAnul->ViewCustomAttributes = "";

		// FcaMAnu
		$this->FcaMAnu->ViewValue = $this->FcaMAnu->CurrentValue;
		$this->FcaMAnu->ViewCustomAttributes = "";

		// FcaFAnu
		$this->FcaFAnu->ViewValue = $this->FcaFAnu->CurrentValue;
		$this->FcaFAnu->ViewValue = ew_FormatDateTime($this->FcaFAnu->ViewValue, 7);
		$this->FcaFAnu->ViewCustomAttributes = "";

		// FcaFTip
		$this->FcaFTip->ViewValue = $this->FcaFTip->CurrentValue;
		$this->FcaFTip->ViewCustomAttributes = "";

		// FcaUsua
		$this->FcaUsua->ViewValue = $this->FcaUsua->CurrentValue;
		$this->FcaUsua->ViewCustomAttributes = "";

		// FcaFCre
		$this->FcaFCre->ViewValue = $this->FcaFCre->CurrentValue;
		$this->FcaFCre->ViewValue = ew_FormatDateTime($this->FcaFCre->ViewValue, 7);
		$this->FcaFCre->ViewCustomAttributes = "";

			// FcaCodi
			$this->FcaCodi->LinkCustomAttributes = "";
			$this->FcaCodi->HrefValue = "";
			$this->FcaCodi->TooltipValue = "";

			// OveCodi
			$this->OveCodi->LinkCustomAttributes = "";
			$this->OveCodi->HrefValue = "";
			$this->OveCodi->TooltipValue = "";

			// FcaVend
			$this->FcaVend->LinkCustomAttributes = "";
			$this->FcaVend->HrefValue = "";
			$this->FcaVend->TooltipValue = "";

			// FcaCaje
			$this->FcaCaje->LinkCustomAttributes = "";
			$this->FcaCaje->HrefValue = "";
			$this->FcaCaje->TooltipValue = "";

			// FcaFech
			$this->FcaFech->LinkCustomAttributes = "";
			$this->FcaFech->HrefValue = "";
			$this->FcaFech->TooltipValue = "";

			// FcaTimb
			$this->FcaTimb->LinkCustomAttributes = "";
			$this->FcaTimb->HrefValue = "";
			$this->FcaTimb->TooltipValue = "";

			// FcaTFac
			$this->FcaTFac->LinkCustomAttributes = "";
			$this->FcaTFac->HrefValue = "";
			$this->FcaTFac->TooltipValue = "";

			// FcaAnul
			$this->FcaAnul->LinkCustomAttributes = "";
			$this->FcaAnul->HrefValue = "";
			$this->FcaAnul->TooltipValue = "";

			// FcaMAnu
			$this->FcaMAnu->LinkCustomAttributes = "";
			$this->FcaMAnu->HrefValue = "";
			$this->FcaMAnu->TooltipValue = "";

			// FcaFAnu
			$this->FcaFAnu->LinkCustomAttributes = "";
			$this->FcaFAnu->HrefValue = "";
			$this->FcaFAnu->TooltipValue = "";

			// FcaFTip
			$this->FcaFTip->LinkCustomAttributes = "";
			$this->FcaFTip->HrefValue = "";
			$this->FcaFTip->TooltipValue = "";

			// FcaUsua
			$this->FcaUsua->LinkCustomAttributes = "";
			$this->FcaUsua->HrefValue = "";
			$this->FcaUsua->TooltipValue = "";

			// FcaFCre
			$this->FcaFCre->LinkCustomAttributes = "";
			$this->FcaFCre->HrefValue = "";
			$this->FcaFCre->TooltipValue = "";
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	//
	// Delete records based on current filter
	//
	function DeleteRows() {
		global $Language, $Security;
		if (!$Security->CanDelete()) {
			$this->setFailureMessage($Language->Phrase("NoDeletePermission")); // No delete permission
			return FALSE;
		}
		$DeleteRows = TRUE;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE) {
			return FALSE;
		} elseif ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
			$rs->Close();
			return FALSE;

		//} else {
		//	$this->LoadRowValues($rs); // Load row values

		}
		$rows = ($rs) ? $rs->GetRows() : array();
		$conn->BeginTrans();

		// Clone old rows
		$rsold = $rows;
		if ($rs)
			$rs->Close();

		// Call row deleting event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$DeleteRows = $this->Row_Deleting($row);
				if (!$DeleteRows) break;
			}
		}
		if ($DeleteRows) {
			$sKey = "";
			foreach ($rsold as $row) {
				$sThisKey = "";
				if ($sThisKey <> "") $sThisKey .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
				$sThisKey .= $row['FcaCodi'];
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				$DeleteRows = $this->Delete($row); // Delete
				$conn->raiseErrorFn = '';
				if ($DeleteRows === FALSE)
					break;
				if ($sKey <> "") $sKey .= ", ";
				$sKey .= $sThisKey;
			}
		} else {

			// Set up error message
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("DeleteCancelled"));
			}
		}
		if ($DeleteRows) {
			$conn->CommitTrans(); // Commit the changes
		} else {
			$conn->RollbackTrans(); // Rollback changes
		}

		// Call Row Deleted event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$this->Row_Deleted($row);
			}
		}
		return $DeleteRows;
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, "FCablist.php", "", $this->TableVar, TRUE);
		$PageId = "delete";
		$Breadcrumb->Add("delete", $PageId, $url);
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($FCab_delete)) $FCab_delete = new cFCab_delete();

// Page init
$FCab_delete->Page_Init();

// Page main
$FCab_delete->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$FCab_delete->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "delete";
var CurrentForm = fFCabdelete = new ew_Form("fFCabdelete", "delete");

// Form_CustomValidate event
fFCabdelete.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fFCabdelete.ValidateRequired = true;
<?php } else { ?>
fFCabdelete.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
fFCabdelete.Lists["x_OveCodi"] = {"LinkField":"x_OveCodi","Ajax":true,"AutoFill":false,"DisplayFields":["x_OveCodi","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};

// Form object for search
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<?php

// Load records for display
if ($FCab_delete->Recordset = $FCab_delete->LoadRecordset())
	$FCab_deleteTotalRecs = $FCab_delete->Recordset->RecordCount(); // Get record count
if ($FCab_deleteTotalRecs <= 0) { // No record found, exit
	if ($FCab_delete->Recordset)
		$FCab_delete->Recordset->Close();
	$FCab_delete->Page_Terminate("FCablist.php"); // Return to list
}
?>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php $FCab_delete->ShowPageHeader(); ?>
<?php
$FCab_delete->ShowMessage();
?>
<form name="fFCabdelete" id="fFCabdelete" class="form-inline ewForm ewDeleteForm" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($FCab_delete->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $FCab_delete->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="FCab">
<input type="hidden" name="a_delete" id="a_delete" value="D">
<?php foreach ($FCab_delete->RecKeys as $key) { ?>
<?php $keyvalue = is_array($key) ? implode($EW_COMPOSITE_KEY_SEPARATOR, $key) : $key; ?>
<input type="hidden" name="key_m[]" value="<?php echo ew_HtmlEncode($keyvalue) ?>">
<?php } ?>
<div class="ewGrid">
<div class="<?php if (ew_IsResponsiveLayout()) { echo "table-responsive "; } ?>ewGridMiddlePanel">
<table class="table ewTable">
<?php echo $FCab->TableCustomInnerHtml ?>
	<thead>
	<tr class="ewTableHeader">
<?php if ($FCab->FcaCodi->Visible) { // FcaCodi ?>
		<th><span id="elh_FCab_FcaCodi" class="FCab_FcaCodi"><?php echo $FCab->FcaCodi->FldCaption() ?></span></th>
<?php } ?>
<?php if ($FCab->OveCodi->Visible) { // OveCodi ?>
		<th><span id="elh_FCab_OveCodi" class="FCab_OveCodi"><?php echo $FCab->OveCodi->FldCaption() ?></span></th>
<?php } ?>
<?php if ($FCab->FcaVend->Visible) { // FcaVend ?>
		<th><span id="elh_FCab_FcaVend" class="FCab_FcaVend"><?php echo $FCab->FcaVend->FldCaption() ?></span></th>
<?php } ?>
<?php if ($FCab->FcaCaje->Visible) { // FcaCaje ?>
		<th><span id="elh_FCab_FcaCaje" class="FCab_FcaCaje"><?php echo $FCab->FcaCaje->FldCaption() ?></span></th>
<?php } ?>
<?php if ($FCab->FcaFech->Visible) { // FcaFech ?>
		<th><span id="elh_FCab_FcaFech" class="FCab_FcaFech"><?php echo $FCab->FcaFech->FldCaption() ?></span></th>
<?php } ?>
<?php if ($FCab->FcaTimb->Visible) { // FcaTimb ?>
		<th><span id="elh_FCab_FcaTimb" class="FCab_FcaTimb"><?php echo $FCab->FcaTimb->FldCaption() ?></span></th>
<?php } ?>
<?php if ($FCab->FcaTFac->Visible) { // FcaTFac ?>
		<th><span id="elh_FCab_FcaTFac" class="FCab_FcaTFac"><?php echo $FCab->FcaTFac->FldCaption() ?></span></th>
<?php } ?>
<?php if ($FCab->FcaAnul->Visible) { // FcaAnul ?>
		<th><span id="elh_FCab_FcaAnul" class="FCab_FcaAnul"><?php echo $FCab->FcaAnul->FldCaption() ?></span></th>
<?php } ?>
<?php if ($FCab->FcaMAnu->Visible) { // FcaMAnu ?>
		<th><span id="elh_FCab_FcaMAnu" class="FCab_FcaMAnu"><?php echo $FCab->FcaMAnu->FldCaption() ?></span></th>
<?php } ?>
<?php if ($FCab->FcaFAnu->Visible) { // FcaFAnu ?>
		<th><span id="elh_FCab_FcaFAnu" class="FCab_FcaFAnu"><?php echo $FCab->FcaFAnu->FldCaption() ?></span></th>
<?php } ?>
<?php if ($FCab->FcaFTip->Visible) { // FcaFTip ?>
		<th><span id="elh_FCab_FcaFTip" class="FCab_FcaFTip"><?php echo $FCab->FcaFTip->FldCaption() ?></span></th>
<?php } ?>
<?php if ($FCab->FcaUsua->Visible) { // FcaUsua ?>
		<th><span id="elh_FCab_FcaUsua" class="FCab_FcaUsua"><?php echo $FCab->FcaUsua->FldCaption() ?></span></th>
<?php } ?>
<?php if ($FCab->FcaFCre->Visible) { // FcaFCre ?>
		<th><span id="elh_FCab_FcaFCre" class="FCab_FcaFCre"><?php echo $FCab->FcaFCre->FldCaption() ?></span></th>
<?php } ?>
	</tr>
	</thead>
	<tbody>
<?php
$FCab_delete->RecCnt = 0;
$i = 0;
while (!$FCab_delete->Recordset->EOF) {
	$FCab_delete->RecCnt++;
	$FCab_delete->RowCnt++;

	// Set row properties
	$FCab->ResetAttrs();
	$FCab->RowType = EW_ROWTYPE_VIEW; // View

	// Get the field contents
	$FCab_delete->LoadRowValues($FCab_delete->Recordset);

	// Render row
	$FCab_delete->RenderRow();
?>
	<tr<?php echo $FCab->RowAttributes() ?>>
<?php if ($FCab->FcaCodi->Visible) { // FcaCodi ?>
		<td<?php echo $FCab->FcaCodi->CellAttributes() ?>>
<span id="el<?php echo $FCab_delete->RowCnt ?>_FCab_FcaCodi" class="FCab_FcaCodi">
<span<?php echo $FCab->FcaCodi->ViewAttributes() ?>>
<?php echo $FCab->FcaCodi->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($FCab->OveCodi->Visible) { // OveCodi ?>
		<td<?php echo $FCab->OveCodi->CellAttributes() ?>>
<span id="el<?php echo $FCab_delete->RowCnt ?>_FCab_OveCodi" class="FCab_OveCodi">
<span<?php echo $FCab->OveCodi->ViewAttributes() ?>>
<?php echo $FCab->OveCodi->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($FCab->FcaVend->Visible) { // FcaVend ?>
		<td<?php echo $FCab->FcaVend->CellAttributes() ?>>
<span id="el<?php echo $FCab_delete->RowCnt ?>_FCab_FcaVend" class="FCab_FcaVend">
<span<?php echo $FCab->FcaVend->ViewAttributes() ?>>
<?php echo $FCab->FcaVend->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($FCab->FcaCaje->Visible) { // FcaCaje ?>
		<td<?php echo $FCab->FcaCaje->CellAttributes() ?>>
<span id="el<?php echo $FCab_delete->RowCnt ?>_FCab_FcaCaje" class="FCab_FcaCaje">
<span<?php echo $FCab->FcaCaje->ViewAttributes() ?>>
<?php echo $FCab->FcaCaje->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($FCab->FcaFech->Visible) { // FcaFech ?>
		<td<?php echo $FCab->FcaFech->CellAttributes() ?>>
<span id="el<?php echo $FCab_delete->RowCnt ?>_FCab_FcaFech" class="FCab_FcaFech">
<span<?php echo $FCab->FcaFech->ViewAttributes() ?>>
<?php echo $FCab->FcaFech->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($FCab->FcaTimb->Visible) { // FcaTimb ?>
		<td<?php echo $FCab->FcaTimb->CellAttributes() ?>>
<span id="el<?php echo $FCab_delete->RowCnt ?>_FCab_FcaTimb" class="FCab_FcaTimb">
<span<?php echo $FCab->FcaTimb->ViewAttributes() ?>>
<?php echo $FCab->FcaTimb->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($FCab->FcaTFac->Visible) { // FcaTFac ?>
		<td<?php echo $FCab->FcaTFac->CellAttributes() ?>>
<span id="el<?php echo $FCab_delete->RowCnt ?>_FCab_FcaTFac" class="FCab_FcaTFac">
<span<?php echo $FCab->FcaTFac->ViewAttributes() ?>>
<?php echo $FCab->FcaTFac->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($FCab->FcaAnul->Visible) { // FcaAnul ?>
		<td<?php echo $FCab->FcaAnul->CellAttributes() ?>>
<span id="el<?php echo $FCab_delete->RowCnt ?>_FCab_FcaAnul" class="FCab_FcaAnul">
<span<?php echo $FCab->FcaAnul->ViewAttributes() ?>>
<?php echo $FCab->FcaAnul->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($FCab->FcaMAnu->Visible) { // FcaMAnu ?>
		<td<?php echo $FCab->FcaMAnu->CellAttributes() ?>>
<span id="el<?php echo $FCab_delete->RowCnt ?>_FCab_FcaMAnu" class="FCab_FcaMAnu">
<span<?php echo $FCab->FcaMAnu->ViewAttributes() ?>>
<?php echo $FCab->FcaMAnu->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($FCab->FcaFAnu->Visible) { // FcaFAnu ?>
		<td<?php echo $FCab->FcaFAnu->CellAttributes() ?>>
<span id="el<?php echo $FCab_delete->RowCnt ?>_FCab_FcaFAnu" class="FCab_FcaFAnu">
<span<?php echo $FCab->FcaFAnu->ViewAttributes() ?>>
<?php echo $FCab->FcaFAnu->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($FCab->FcaFTip->Visible) { // FcaFTip ?>
		<td<?php echo $FCab->FcaFTip->CellAttributes() ?>>
<span id="el<?php echo $FCab_delete->RowCnt ?>_FCab_FcaFTip" class="FCab_FcaFTip">
<span<?php echo $FCab->FcaFTip->ViewAttributes() ?>>
<?php echo $FCab->FcaFTip->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($FCab->FcaUsua->Visible) { // FcaUsua ?>
		<td<?php echo $FCab->FcaUsua->CellAttributes() ?>>
<span id="el<?php echo $FCab_delete->RowCnt ?>_FCab_FcaUsua" class="FCab_FcaUsua">
<span<?php echo $FCab->FcaUsua->ViewAttributes() ?>>
<?php echo $FCab->FcaUsua->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($FCab->FcaFCre->Visible) { // FcaFCre ?>
		<td<?php echo $FCab->FcaFCre->CellAttributes() ?>>
<span id="el<?php echo $FCab_delete->RowCnt ?>_FCab_FcaFCre" class="FCab_FcaFCre">
<span<?php echo $FCab->FcaFCre->ViewAttributes() ?>>
<?php echo $FCab->FcaFCre->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
	</tr>
<?php
	$FCab_delete->Recordset->MoveNext();
}
$FCab_delete->Recordset->Close();
?>
</tbody>
</table>
</div>
</div>
<div>
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("DeleteBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="button" data-href="<?php echo $FCab_delete->getReturnUrl() ?>"><?php echo $Language->Phrase("CancelBtn") ?></button>
</div>
</form>
<script type="text/javascript">
fFCabdelete.Init();
</script>
<?php
$FCab_delete->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$FCab_delete->Page_Terminate();
?>
