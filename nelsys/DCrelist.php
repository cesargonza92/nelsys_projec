<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "DCreinfo.php" ?>
<?php include_once "Usuainfo.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$DCre_list = NULL; // Initialize page object first

class cDCre_list extends cDCre {

	// Page ID
	var $PageID = 'list';

	// Project ID
	var $ProjectID = "{04439FF7-B43F-460F-8514-F71C8FF9E679}";

	// Table name
	var $TableName = 'DCre';

	// Page object name
	var $PageObjName = 'DCre_list';

	// Grid form hidden field names
	var $FormName = 'fDCrelist';
	var $FormActionName = 'k_action';
	var $FormKeyName = 'k_key';
	var $FormOldKeyName = 'k_oldkey';
	var $FormBlankRowName = 'k_blankrow';
	var $FormKeyCountName = 'key_count';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Page URLs
	var $AddUrl;
	var $EditUrl;
	var $CopyUrl;
	var $DeleteUrl;
	var $ViewUrl;
	var $ListUrl;

	// Export URLs
	var $ExportPrintUrl;
	var $ExportHtmlUrl;
	var $ExportExcelUrl;
	var $ExportWordUrl;
	var $ExportXmlUrl;
	var $ExportCsvUrl;
	var $ExportPdfUrl;

	// Custom export
	var $ExportExcelCustom = FALSE;
	var $ExportWordCustom = FALSE;
	var $ExportPdfCustom = FALSE;
	var $ExportEmailCustom = FALSE;

	// Update URLs
	var $InlineAddUrl;
	var $InlineCopyUrl;
	var $InlineEditUrl;
	var $GridAddUrl;
	var $GridEditUrl;
	var $MultiDeleteUrl;
	var $MultiUpdateUrl;

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (DCre)
		if (!isset($GLOBALS["DCre"]) || get_class($GLOBALS["DCre"]) == "cDCre") {
			$GLOBALS["DCre"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["DCre"];
		}

		// Initialize URLs
		$this->ExportPrintUrl = $this->PageUrl() . "export=print";
		$this->ExportExcelUrl = $this->PageUrl() . "export=excel";
		$this->ExportWordUrl = $this->PageUrl() . "export=word";
		$this->ExportHtmlUrl = $this->PageUrl() . "export=html";
		$this->ExportXmlUrl = $this->PageUrl() . "export=xml";
		$this->ExportCsvUrl = $this->PageUrl() . "export=csv";
		$this->ExportPdfUrl = $this->PageUrl() . "export=pdf";
		$this->AddUrl = "DCreadd.php";
		$this->InlineAddUrl = $this->PageUrl() . "a=add";
		$this->GridAddUrl = $this->PageUrl() . "a=gridadd";
		$this->GridEditUrl = $this->PageUrl() . "a=gridedit";
		$this->MultiDeleteUrl = "DCredelete.php";
		$this->MultiUpdateUrl = "DCreupdate.php";

		// Table object (Usua)
		if (!isset($GLOBALS['Usua'])) $GLOBALS['Usua'] = new cUsua();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'list', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'DCre', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (Usua)
		if (!isset($UserTable)) {
			$UserTable = new cUsua();
			$UserTableConn = Conn($UserTable->DBID);
		}

		// List options
		$this->ListOptions = new cListOptions();
		$this->ListOptions->TableVar = $this->TableVar;

		// Export options
		$this->ExportOptions = new cListOptions();
		$this->ExportOptions->Tag = "div";
		$this->ExportOptions->TagClassName = "ewExportOption";

		// Other options
		$this->OtherOptions['addedit'] = new cListOptions();
		$this->OtherOptions['addedit']->Tag = "div";
		$this->OtherOptions['addedit']->TagClassName = "ewAddEditOption";
		$this->OtherOptions['detail'] = new cListOptions();
		$this->OtherOptions['detail']->Tag = "div";
		$this->OtherOptions['detail']->TagClassName = "ewDetailOption";
		$this->OtherOptions['action'] = new cListOptions();
		$this->OtherOptions['action']->Tag = "div";
		$this->OtherOptions['action']->TagClassName = "ewActionOption";

		// Filter options
		$this->FilterOptions = new cListOptions();
		$this->FilterOptions->Tag = "div";
		$this->FilterOptions->TagClassName = "ewFilterOption fDCrelistsrch";

		// List actions
		$this->ListActions = new cListActions();
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanList()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage($Language->Phrase("NoPermission")); // Set no permission
			$this->Page_Terminate(ew_GetUrl("index.php"));
		}

		// Create form object
		$objForm = new cFormObj();
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action

		// Get grid add count
		$gridaddcnt = @$_GET[EW_TABLE_GRID_ADD_ROW_COUNT];
		if (is_numeric($gridaddcnt) && $gridaddcnt > 0)
			$this->GridAddRowCount = $gridaddcnt;

		// Set up list options
		$this->SetupListOptions();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Process auto fill
		if (@$_POST["ajax"] == "autofill") {
			$results = $this->GetAutoFill(@$_POST["name"], @$_POST["q"]);
			if ($results) {

				// Clean output buffer
				if (!EW_DEBUG_ENABLED && ob_get_length())
					ob_end_clean();
				echo $results;
				$this->Page_Terminate();
				exit();
			}
		}

		// Create Token
		$this->CreateToken();

		// Setup other options
		$this->SetupOtherOptions();

		// Set up custom action (compatible with old version)
		foreach ($this->CustomActions as $name => $action)
			$this->ListActions->Add($name, $action);

		// Show checkbox column if multiple action
		foreach ($this->ListActions->Items as $listaction) {
			if ($listaction->Select == EW_ACTION_MULTIPLE && $listaction->Allow) {
				$this->ListOptions->Items["checkbox"]->Visible = TRUE;
				break;
			}
		}
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $DCre;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($DCre);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}

	// Class variables
	var $ListOptions; // List options
	var $ExportOptions; // Export options
	var $SearchOptions; // Search options
	var $OtherOptions = array(); // Other options
	var $FilterOptions; // Filter options
	var $ListActions; // List actions
	var $SelectedCount = 0;
	var $SelectedIndex = 0;
	var $DisplayRecs = 20;
	var $StartRec;
	var $StopRec;
	var $TotalRecs = 0;
	var $RecRange = 10;
	var $Pager;
	var $DefaultSearchWhere = ""; // Default search WHERE clause
	var $SearchWhere = ""; // Search WHERE clause
	var $RecCnt = 0; // Record count
	var $EditRowCnt;
	var $StartRowCnt = 1;
	var $RowCnt = 0;
	var $Attrs = array(); // Row attributes and cell attributes
	var $RowIndex = 0; // Row index
	var $KeyCount = 0; // Key count
	var $RowAction = ""; // Row action
	var $RowOldKey = ""; // Row old key (for copy)
	var $RecPerRow = 0;
	var $MultiColumnClass;
	var $MultiColumnEditClass = "col-sm-12";
	var $MultiColumnCnt = 12;
	var $MultiColumnEditCnt = 12;
	var $GridCnt = 0;
	var $ColCnt = 0;
	var $DbMasterFilter = ""; // Master filter
	var $DbDetailFilter = ""; // Detail filter
	var $MasterRecordExists;	
	var $MultiSelectKey;
	var $Command;
	var $RestoreSearch = FALSE;
	var $DetailPages;
	var $Recordset;
	var $OldRecordset;

	//
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError, $gsSearchError, $Security;

		// Search filters
		$sSrchAdvanced = ""; // Advanced search filter
		$sSrchBasic = ""; // Basic search filter
		$sFilter = "";

		// Get command
		$this->Command = strtolower(@$_GET["cmd"]);
		if ($this->IsPageRequest()) { // Validate request

			// Process list action first
			if ($this->ProcessListAction()) // Ajax request
				$this->Page_Terminate();

			// Handle reset command
			$this->ResetCmd();

			// Set up Breadcrumb
			if ($this->Export == "")
				$this->SetupBreadcrumb();

			// Check QueryString parameters
			if (@$_GET["a"] <> "") {
				$this->CurrentAction = $_GET["a"];

				// Clear inline mode
				if ($this->CurrentAction == "cancel")
					$this->ClearInlineMode();

				// Switch to grid edit mode
				if ($this->CurrentAction == "gridedit")
					$this->GridEditMode();

				// Switch to grid add mode
				if ($this->CurrentAction == "gridadd")
					$this->GridAddMode();
			} else {
				if (@$_POST["a_list"] <> "") {
					$this->CurrentAction = $_POST["a_list"]; // Get action

					// Grid Update
					if (($this->CurrentAction == "gridupdate" || $this->CurrentAction == "gridoverwrite") && @$_SESSION[EW_SESSION_INLINE_MODE] == "gridedit") {
						if ($this->ValidateGridForm()) {
							$bGridUpdate = $this->GridUpdate();
						} else {
							$bGridUpdate = FALSE;
							$this->setFailureMessage($gsFormError);
						}
						if (!$bGridUpdate) {
							$this->EventCancelled = TRUE;
							$this->CurrentAction = "gridedit"; // Stay in Grid Edit mode
						}
					}

					// Grid Insert
					if ($this->CurrentAction == "gridinsert" && @$_SESSION[EW_SESSION_INLINE_MODE] == "gridadd") {
						if ($this->ValidateGridForm()) {
							$bGridInsert = $this->GridInsert();
						} else {
							$bGridInsert = FALSE;
							$this->setFailureMessage($gsFormError);
						}
						if (!$bGridInsert) {
							$this->EventCancelled = TRUE;
							$this->CurrentAction = "gridadd"; // Stay in Grid Add mode
						}
					}
				}
			}

			// Hide list options
			if ($this->Export <> "") {
				$this->ListOptions->HideAllOptions(array("sequence"));
				$this->ListOptions->UseDropDownButton = FALSE; // Disable drop down button
				$this->ListOptions->UseButtonGroup = FALSE; // Disable button group
			} elseif ($this->CurrentAction == "gridadd" || $this->CurrentAction == "gridedit") {
				$this->ListOptions->HideAllOptions();
				$this->ListOptions->UseDropDownButton = FALSE; // Disable drop down button
				$this->ListOptions->UseButtonGroup = FALSE; // Disable button group
			}

			// Hide options
			if ($this->Export <> "" || $this->CurrentAction <> "") {
				$this->ExportOptions->HideAllOptions();
				$this->FilterOptions->HideAllOptions();
			}

			// Hide other options
			if ($this->Export <> "") {
				foreach ($this->OtherOptions as &$option)
					$option->HideAllOptions();
			}

			// Show grid delete link for grid add / grid edit
			if ($this->AllowAddDeleteRow) {
				if ($this->CurrentAction == "gridadd" || $this->CurrentAction == "gridedit") {
					$item = $this->ListOptions->GetItem("griddelete");
					if ($item) $item->Visible = TRUE;
				}
			}

			// Get default search criteria
			ew_AddFilter($this->DefaultSearchWhere, $this->BasicSearchWhere(TRUE));

			// Get basic search values
			$this->LoadBasicSearchValues();

			// Restore filter list
			$this->RestoreFilterList();

			// Restore search parms from Session if not searching / reset / export
			if (($this->Export <> "" || $this->Command <> "search" && $this->Command <> "reset" && $this->Command <> "resetall") && $this->CheckSearchParms())
				$this->RestoreSearchParms();

			// Call Recordset SearchValidated event
			$this->Recordset_SearchValidated();

			// Set up sorting order
			$this->SetUpSortOrder();

			// Get basic search criteria
			if ($gsSearchError == "")
				$sSrchBasic = $this->BasicSearchWhere();
		}

		// Restore display records
		if ($this->getRecordsPerPage() <> "") {
			$this->DisplayRecs = $this->getRecordsPerPage(); // Restore from Session
		} else {
			$this->DisplayRecs = 20; // Load default
		}

		// Load Sorting Order
		$this->LoadSortOrder();

		// Load search default if no existing search criteria
		if (!$this->CheckSearchParms()) {

			// Load basic search from default
			$this->BasicSearch->LoadDefault();
			if ($this->BasicSearch->Keyword != "")
				$sSrchBasic = $this->BasicSearchWhere();
		}

		// Build search criteria
		ew_AddFilter($this->SearchWhere, $sSrchAdvanced);
		ew_AddFilter($this->SearchWhere, $sSrchBasic);

		// Call Recordset_Searching event
		$this->Recordset_Searching($this->SearchWhere);

		// Save search criteria
		if ($this->Command == "search" && !$this->RestoreSearch) {
			$this->setSearchWhere($this->SearchWhere); // Save to Session
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} else {
			$this->SearchWhere = $this->getSearchWhere();
		}

		// Build filter
		$sFilter = "";
		if (!$Security->CanList())
			$sFilter = "(0=1)"; // Filter all records
		ew_AddFilter($sFilter, $this->DbDetailFilter);
		ew_AddFilter($sFilter, $this->SearchWhere);

		// Set up filter in session
		$this->setSessionWhere($sFilter);
		$this->CurrentFilter = "";

		// Load record count first
		if (!$this->IsAddOrEdit()) {
			$bSelectLimit = $this->UseSelectLimit;
			if ($bSelectLimit) {
				$this->TotalRecs = $this->SelectRecordCount();
			} else {
				if ($this->Recordset = $this->LoadRecordset())
					$this->TotalRecs = $this->Recordset->RecordCount();
			}
		}

		// Search options
		$this->SetupSearchOptions();
	}

	//  Exit inline mode
	function ClearInlineMode() {
		$this->DcrMont->FormValue = ""; // Clear form value
		$this->DcrSCuo->FormValue = ""; // Clear form value
		$this->LastAction = $this->CurrentAction; // Save last action
		$this->CurrentAction = ""; // Clear action
		$_SESSION[EW_SESSION_INLINE_MODE] = ""; // Clear inline mode
	}

	// Switch to Grid Add mode
	function GridAddMode() {
		$_SESSION[EW_SESSION_INLINE_MODE] = "gridadd"; // Enabled grid add
	}

	// Switch to Grid Edit mode
	function GridEditMode() {
		$_SESSION[EW_SESSION_INLINE_MODE] = "gridedit"; // Enable grid edit
	}

	// Perform update to grid
	function GridUpdate() {
		global $Language, $objForm, $gsFormError;
		$bGridUpdate = TRUE;

		// Get old recordset
		$this->CurrentFilter = $this->BuildKeyFilter();
		if ($this->CurrentFilter == "")
			$this->CurrentFilter = "0=1";
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		if ($rs = $conn->Execute($sSql)) {
			$rsold = $rs->GetRows();
			$rs->Close();
		}

		// Call Grid Updating event
		if (!$this->Grid_Updating($rsold)) {
			if ($this->getFailureMessage() == "")
				$this->setFailureMessage($Language->Phrase("GridEditCancelled")); // Set grid edit cancelled message
			return FALSE;
		}

		// Begin transaction
		$conn->BeginTrans();
		$sKey = "";

		// Update row index and get row key
		$objForm->Index = -1;
		$rowcnt = strval($objForm->GetValue($this->FormKeyCountName));
		if ($rowcnt == "" || !is_numeric($rowcnt))
			$rowcnt = 0;

		// Update all rows based on key
		for ($rowindex = 1; $rowindex <= $rowcnt; $rowindex++) {
			$objForm->Index = $rowindex;
			$rowkey = strval($objForm->GetValue($this->FormKeyName));
			$rowaction = strval($objForm->GetValue($this->FormActionName));

			// Load all values and keys
			if ($rowaction <> "insertdelete") { // Skip insert then deleted rows
				$this->LoadFormValues(); // Get form values
				if ($rowaction == "" || $rowaction == "edit" || $rowaction == "delete") {
					$bGridUpdate = $this->SetupKeyValues($rowkey); // Set up key values
				} else {
					$bGridUpdate = TRUE;
				}

				// Skip empty row
				if ($rowaction == "insert" && $this->EmptyRow()) {

					// No action required
				// Validate form and insert/update/delete record

				} elseif ($bGridUpdate) {
					if ($rowaction == "delete") {
						$this->CurrentFilter = $this->KeyFilter();
						$bGridUpdate = $this->DeleteRows(); // Delete this row
					} else if (!$this->ValidateForm()) {
						$bGridUpdate = FALSE; // Form error, reset action
						$this->setFailureMessage($gsFormError);
					} else {
						if ($rowaction == "insert") {
							$bGridUpdate = $this->AddRow(); // Insert this row
						} else {
							if ($rowkey <> "") {
								$this->SendEmail = FALSE; // Do not send email on update success
								$bGridUpdate = $this->EditRow(); // Update this row
							}
						} // End update
					}
				}
				if ($bGridUpdate) {
					if ($sKey <> "") $sKey .= ", ";
					$sKey .= $rowkey;
				} else {
					break;
				}
			}
		}
		if ($bGridUpdate) {
			$conn->CommitTrans(); // Commit transaction

			// Get new recordset
			if ($rs = $conn->Execute($sSql)) {
				$rsnew = $rs->GetRows();
				$rs->Close();
			}

			// Call Grid_Updated event
			$this->Grid_Updated($rsold, $rsnew);
			if ($this->getSuccessMessage() == "")
				$this->setSuccessMessage($Language->Phrase("UpdateSuccess")); // Set up update success message
			$this->ClearInlineMode(); // Clear inline edit mode
		} else {
			$conn->RollbackTrans(); // Rollback transaction
			if ($this->getFailureMessage() == "")
				$this->setFailureMessage($Language->Phrase("UpdateFailed")); // Set update failed message
		}
		return $bGridUpdate;
	}

	// Build filter for all keys
	function BuildKeyFilter() {
		global $objForm;
		$sWrkFilter = "";

		// Update row index and get row key
		$rowindex = 1;
		$objForm->Index = $rowindex;
		$sThisKey = strval($objForm->GetValue($this->FormKeyName));
		while ($sThisKey <> "") {
			if ($this->SetupKeyValues($sThisKey)) {
				$sFilter = $this->KeyFilter();
				if ($sWrkFilter <> "") $sWrkFilter .= " OR ";
				$sWrkFilter .= $sFilter;
			} else {
				$sWrkFilter = "0=1";
				break;
			}

			// Update row index and get row key
			$rowindex++; // Next row
			$objForm->Index = $rowindex;
			$sThisKey = strval($objForm->GetValue($this->FormKeyName));
		}
		return $sWrkFilter;
	}

	// Set up key values
	function SetupKeyValues($key) {
		$arrKeyFlds = explode($GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"], $key);
		if (count($arrKeyFlds) >= 1) {
			$this->DcrCodi->setFormValue($arrKeyFlds[0]);
			if (!is_numeric($this->DcrCodi->FormValue))
				return FALSE;
		}
		return TRUE;
	}

	// Perform Grid Add
	function GridInsert() {
		global $Language, $objForm, $gsFormError;
		$rowindex = 1;
		$bGridInsert = FALSE;
		$conn = &$this->Connection();

		// Call Grid Inserting event
		if (!$this->Grid_Inserting()) {
			if ($this->getFailureMessage() == "") {
				$this->setFailureMessage($Language->Phrase("GridAddCancelled")); // Set grid add cancelled message
			}
			return FALSE;
		}

		// Begin transaction
		$conn->BeginTrans();

		// Init key filter
		$sWrkFilter = "";
		$addcnt = 0;
		$sKey = "";

		// Get row count
		$objForm->Index = -1;
		$rowcnt = strval($objForm->GetValue($this->FormKeyCountName));
		if ($rowcnt == "" || !is_numeric($rowcnt))
			$rowcnt = 0;

		// Insert all rows
		for ($rowindex = 1; $rowindex <= $rowcnt; $rowindex++) {

			// Load current row values
			$objForm->Index = $rowindex;
			$rowaction = strval($objForm->GetValue($this->FormActionName));
			if ($rowaction <> "" && $rowaction <> "insert")
				continue; // Skip
			$this->LoadFormValues(); // Get form values
			if (!$this->EmptyRow()) {
				$addcnt++;
				$this->SendEmail = FALSE; // Do not send email on insert success

				// Validate form
				if (!$this->ValidateForm()) {
					$bGridInsert = FALSE; // Form error, reset action
					$this->setFailureMessage($gsFormError);
				} else {
					$bGridInsert = $this->AddRow($this->OldRecordset); // Insert this row
				}
				if ($bGridInsert) {
					if ($sKey <> "") $sKey .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
					$sKey .= $this->DcrCodi->CurrentValue;

					// Add filter for this record
					$sFilter = $this->KeyFilter();
					if ($sWrkFilter <> "") $sWrkFilter .= " OR ";
					$sWrkFilter .= $sFilter;
				} else {
					break;
				}
			}
		}
		if ($addcnt == 0) { // No record inserted
			$this->setFailureMessage($Language->Phrase("NoAddRecord"));
			$bGridInsert = FALSE;
		}
		if ($bGridInsert) {
			$conn->CommitTrans(); // Commit transaction

			// Get new recordset
			$this->CurrentFilter = $sWrkFilter;
			$sSql = $this->SQL();
			if ($rs = $conn->Execute($sSql)) {
				$rsnew = $rs->GetRows();
				$rs->Close();
			}

			// Call Grid_Inserted event
			$this->Grid_Inserted($rsnew);
			if ($this->getSuccessMessage() == "")
				$this->setSuccessMessage($Language->Phrase("InsertSuccess")); // Set up insert success message
			$this->ClearInlineMode(); // Clear grid add mode
		} else {
			$conn->RollbackTrans(); // Rollback transaction
			if ($this->getFailureMessage() == "") {
				$this->setFailureMessage($Language->Phrase("InsertFailed")); // Set insert failed message
			}
		}
		return $bGridInsert;
	}

	// Check if empty row
	function EmptyRow() {
		global $objForm;
		if ($objForm->HasValue("x_DcrCodi") && $objForm->HasValue("o_DcrCodi") && $this->DcrCodi->CurrentValue <> $this->DcrCodi->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_VcrCodi") && $objForm->HasValue("o_VcrCodi") && $this->VcrCodi->CurrentValue <> $this->VcrCodi->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_DcrFech") && $objForm->HasValue("o_DcrFech") && $this->DcrFech->CurrentValue <> $this->DcrFech->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_DcrMont") && $objForm->HasValue("o_DcrMont") && $this->DcrMont->CurrentValue <> $this->DcrMont->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_DcrSCuo") && $objForm->HasValue("o_DcrSCuo") && $this->DcrSCuo->CurrentValue <> $this->DcrSCuo->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_DcrNCuo") && $objForm->HasValue("o_DcrNCuo") && $this->DcrNCuo->CurrentValue <> $this->DcrNCuo->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_DcrEsta") && $objForm->HasValue("o_DcrEsta") && $this->DcrEsta->CurrentValue <> $this->DcrEsta->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_DcrUsu") && $objForm->HasValue("o_DcrUsu") && $this->DcrUsu->CurrentValue <> $this->DcrUsu->OldValue)
			return FALSE;
		return TRUE;
	}

	// Validate grid form
	function ValidateGridForm() {
		global $objForm;

		// Get row count
		$objForm->Index = -1;
		$rowcnt = strval($objForm->GetValue($this->FormKeyCountName));
		if ($rowcnt == "" || !is_numeric($rowcnt))
			$rowcnt = 0;

		// Validate all records
		for ($rowindex = 1; $rowindex <= $rowcnt; $rowindex++) {

			// Load current row values
			$objForm->Index = $rowindex;
			$rowaction = strval($objForm->GetValue($this->FormActionName));
			if ($rowaction <> "delete" && $rowaction <> "insertdelete") {
				$this->LoadFormValues(); // Get form values
				if ($rowaction == "insert" && $this->EmptyRow()) {

					// Ignore
				} else if (!$this->ValidateForm()) {
					return FALSE;
				}
			}
		}
		return TRUE;
	}

	// Get all form values of the grid
	function GetGridFormValues() {
		global $objForm;

		// Get row count
		$objForm->Index = -1;
		$rowcnt = strval($objForm->GetValue($this->FormKeyCountName));
		if ($rowcnt == "" || !is_numeric($rowcnt))
			$rowcnt = 0;
		$rows = array();

		// Loop through all records
		for ($rowindex = 1; $rowindex <= $rowcnt; $rowindex++) {

			// Load current row values
			$objForm->Index = $rowindex;
			$rowaction = strval($objForm->GetValue($this->FormActionName));
			if ($rowaction <> "delete" && $rowaction <> "insertdelete") {
				$this->LoadFormValues(); // Get form values
				if ($rowaction == "insert" && $this->EmptyRow()) {

					// Ignore
				} else {
					$rows[] = $this->GetFieldValues("FormValue"); // Return row as array
				}
			}
		}
		return $rows; // Return as array of array
	}

	// Restore form values for current row
	function RestoreCurrentRowFormValues($idx) {
		global $objForm;

		// Get row based on current index
		$objForm->Index = $idx;
		$this->LoadFormValues(); // Load form values
	}

	// Get list of filters
	function GetFilterList() {

		// Initialize
		$sFilterList = "";
		$sFilterList = ew_Concat($sFilterList, $this->DcrCodi->AdvancedSearch->ToJSON(), ","); // Field DcrCodi
		$sFilterList = ew_Concat($sFilterList, $this->VcrCodi->AdvancedSearch->ToJSON(), ","); // Field VcrCodi
		$sFilterList = ew_Concat($sFilterList, $this->DcrFech->AdvancedSearch->ToJSON(), ","); // Field DcrFech
		$sFilterList = ew_Concat($sFilterList, $this->DcrMont->AdvancedSearch->ToJSON(), ","); // Field DcrMont
		$sFilterList = ew_Concat($sFilterList, $this->DcrSCuo->AdvancedSearch->ToJSON(), ","); // Field DcrSCuo
		$sFilterList = ew_Concat($sFilterList, $this->DcrNCuo->AdvancedSearch->ToJSON(), ","); // Field DcrNCuo
		$sFilterList = ew_Concat($sFilterList, $this->DcrEsta->AdvancedSearch->ToJSON(), ","); // Field DcrEsta
		$sFilterList = ew_Concat($sFilterList, $this->DcrUsu->AdvancedSearch->ToJSON(), ","); // Field DcrUsu
		if ($this->BasicSearch->Keyword <> "") {
			$sWrk = "\"" . EW_TABLE_BASIC_SEARCH . "\":\"" . ew_JsEncode2($this->BasicSearch->Keyword) . "\",\"" . EW_TABLE_BASIC_SEARCH_TYPE . "\":\"" . ew_JsEncode2($this->BasicSearch->Type) . "\"";
			$sFilterList = ew_Concat($sFilterList, $sWrk, ",");
		}

		// Return filter list in json
		return ($sFilterList <> "") ? "{" . $sFilterList . "}" : "null";
	}

	// Restore list of filters
	function RestoreFilterList() {

		// Return if not reset filter
		if (@$_POST["cmd"] <> "resetfilter")
			return FALSE;
		$filter = json_decode(ew_StripSlashes(@$_POST["filter"]), TRUE);
		$this->Command = "search";

		// Field DcrCodi
		$this->DcrCodi->AdvancedSearch->SearchValue = @$filter["x_DcrCodi"];
		$this->DcrCodi->AdvancedSearch->SearchOperator = @$filter["z_DcrCodi"];
		$this->DcrCodi->AdvancedSearch->SearchCondition = @$filter["v_DcrCodi"];
		$this->DcrCodi->AdvancedSearch->SearchValue2 = @$filter["y_DcrCodi"];
		$this->DcrCodi->AdvancedSearch->SearchOperator2 = @$filter["w_DcrCodi"];
		$this->DcrCodi->AdvancedSearch->Save();

		// Field VcrCodi
		$this->VcrCodi->AdvancedSearch->SearchValue = @$filter["x_VcrCodi"];
		$this->VcrCodi->AdvancedSearch->SearchOperator = @$filter["z_VcrCodi"];
		$this->VcrCodi->AdvancedSearch->SearchCondition = @$filter["v_VcrCodi"];
		$this->VcrCodi->AdvancedSearch->SearchValue2 = @$filter["y_VcrCodi"];
		$this->VcrCodi->AdvancedSearch->SearchOperator2 = @$filter["w_VcrCodi"];
		$this->VcrCodi->AdvancedSearch->Save();

		// Field DcrFech
		$this->DcrFech->AdvancedSearch->SearchValue = @$filter["x_DcrFech"];
		$this->DcrFech->AdvancedSearch->SearchOperator = @$filter["z_DcrFech"];
		$this->DcrFech->AdvancedSearch->SearchCondition = @$filter["v_DcrFech"];
		$this->DcrFech->AdvancedSearch->SearchValue2 = @$filter["y_DcrFech"];
		$this->DcrFech->AdvancedSearch->SearchOperator2 = @$filter["w_DcrFech"];
		$this->DcrFech->AdvancedSearch->Save();

		// Field DcrMont
		$this->DcrMont->AdvancedSearch->SearchValue = @$filter["x_DcrMont"];
		$this->DcrMont->AdvancedSearch->SearchOperator = @$filter["z_DcrMont"];
		$this->DcrMont->AdvancedSearch->SearchCondition = @$filter["v_DcrMont"];
		$this->DcrMont->AdvancedSearch->SearchValue2 = @$filter["y_DcrMont"];
		$this->DcrMont->AdvancedSearch->SearchOperator2 = @$filter["w_DcrMont"];
		$this->DcrMont->AdvancedSearch->Save();

		// Field DcrSCuo
		$this->DcrSCuo->AdvancedSearch->SearchValue = @$filter["x_DcrSCuo"];
		$this->DcrSCuo->AdvancedSearch->SearchOperator = @$filter["z_DcrSCuo"];
		$this->DcrSCuo->AdvancedSearch->SearchCondition = @$filter["v_DcrSCuo"];
		$this->DcrSCuo->AdvancedSearch->SearchValue2 = @$filter["y_DcrSCuo"];
		$this->DcrSCuo->AdvancedSearch->SearchOperator2 = @$filter["w_DcrSCuo"];
		$this->DcrSCuo->AdvancedSearch->Save();

		// Field DcrNCuo
		$this->DcrNCuo->AdvancedSearch->SearchValue = @$filter["x_DcrNCuo"];
		$this->DcrNCuo->AdvancedSearch->SearchOperator = @$filter["z_DcrNCuo"];
		$this->DcrNCuo->AdvancedSearch->SearchCondition = @$filter["v_DcrNCuo"];
		$this->DcrNCuo->AdvancedSearch->SearchValue2 = @$filter["y_DcrNCuo"];
		$this->DcrNCuo->AdvancedSearch->SearchOperator2 = @$filter["w_DcrNCuo"];
		$this->DcrNCuo->AdvancedSearch->Save();

		// Field DcrEsta
		$this->DcrEsta->AdvancedSearch->SearchValue = @$filter["x_DcrEsta"];
		$this->DcrEsta->AdvancedSearch->SearchOperator = @$filter["z_DcrEsta"];
		$this->DcrEsta->AdvancedSearch->SearchCondition = @$filter["v_DcrEsta"];
		$this->DcrEsta->AdvancedSearch->SearchValue2 = @$filter["y_DcrEsta"];
		$this->DcrEsta->AdvancedSearch->SearchOperator2 = @$filter["w_DcrEsta"];
		$this->DcrEsta->AdvancedSearch->Save();

		// Field DcrUsu
		$this->DcrUsu->AdvancedSearch->SearchValue = @$filter["x_DcrUsu"];
		$this->DcrUsu->AdvancedSearch->SearchOperator = @$filter["z_DcrUsu"];
		$this->DcrUsu->AdvancedSearch->SearchCondition = @$filter["v_DcrUsu"];
		$this->DcrUsu->AdvancedSearch->SearchValue2 = @$filter["y_DcrUsu"];
		$this->DcrUsu->AdvancedSearch->SearchOperator2 = @$filter["w_DcrUsu"];
		$this->DcrUsu->AdvancedSearch->Save();
		$this->BasicSearch->setKeyword(@$filter[EW_TABLE_BASIC_SEARCH]);
		$this->BasicSearch->setType(@$filter[EW_TABLE_BASIC_SEARCH_TYPE]);
	}

	// Return basic search SQL
	function BasicSearchSQL($arKeywords, $type) {
		$sWhere = "";
		$this->BuildBasicSearchSQL($sWhere, $this->DcrEsta, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->DcrUsu, $arKeywords, $type);
		return $sWhere;
	}

	// Build basic search SQL
	function BuildBasicSearchSql(&$Where, &$Fld, $arKeywords, $type) {
		$sDefCond = ($type == "OR") ? "OR" : "AND";
		$arSQL = array(); // Array for SQL parts
		$arCond = array(); // Array for search conditions
		$cnt = count($arKeywords);
		$j = 0; // Number of SQL parts
		for ($i = 0; $i < $cnt; $i++) {
			$Keyword = $arKeywords[$i];
			$Keyword = trim($Keyword);
			if (EW_BASIC_SEARCH_IGNORE_PATTERN <> "") {
				$Keyword = preg_replace(EW_BASIC_SEARCH_IGNORE_PATTERN, "\\", $Keyword);
				$ar = explode("\\", $Keyword);
			} else {
				$ar = array($Keyword);
			}
			foreach ($ar as $Keyword) {
				if ($Keyword <> "") {
					$sWrk = "";
					if ($Keyword == "OR" && $type == "") {
						if ($j > 0)
							$arCond[$j-1] = "OR";
					} elseif ($Keyword == EW_NULL_VALUE) {
						$sWrk = $Fld->FldExpression . " IS NULL";
					} elseif ($Keyword == EW_NOT_NULL_VALUE) {
						$sWrk = $Fld->FldExpression . " IS NOT NULL";
					} elseif ($Fld->FldIsVirtual && $Fld->FldVirtualSearch) {
						$sWrk = $Fld->FldVirtualExpression . ew_Like(ew_QuotedValue("%" . $Keyword . "%", EW_DATATYPE_STRING, $this->DBID), $this->DBID);
					} elseif ($Fld->FldDataType != EW_DATATYPE_NUMBER || is_numeric($Keyword)) {
						$sWrk = $Fld->FldBasicSearchExpression . ew_Like(ew_QuotedValue("%" . $Keyword . "%", EW_DATATYPE_STRING, $this->DBID), $this->DBID);
					}
					if ($sWrk <> "") {
						$arSQL[$j] = $sWrk;
						$arCond[$j] = $sDefCond;
						$j += 1;
					}
				}
			}
		}
		$cnt = count($arSQL);
		$bQuoted = FALSE;
		$sSql = "";
		if ($cnt > 0) {
			for ($i = 0; $i < $cnt-1; $i++) {
				if ($arCond[$i] == "OR") {
					if (!$bQuoted) $sSql .= "(";
					$bQuoted = TRUE;
				}
				$sSql .= $arSQL[$i];
				if ($bQuoted && $arCond[$i] <> "OR") {
					$sSql .= ")";
					$bQuoted = FALSE;
				}
				$sSql .= " " . $arCond[$i] . " ";
			}
			$sSql .= $arSQL[$cnt-1];
			if ($bQuoted)
				$sSql .= ")";
		}
		if ($sSql <> "") {
			if ($Where <> "") $Where .= " OR ";
			$Where .=  "(" . $sSql . ")";
		}
	}

	// Return basic search WHERE clause based on search keyword and type
	function BasicSearchWhere($Default = FALSE) {
		global $Security;
		$sSearchStr = "";
		if (!$Security->CanSearch()) return "";
		$sSearchKeyword = ($Default) ? $this->BasicSearch->KeywordDefault : $this->BasicSearch->Keyword;
		$sSearchType = ($Default) ? $this->BasicSearch->TypeDefault : $this->BasicSearch->Type;
		if ($sSearchKeyword <> "") {
			$sSearch = trim($sSearchKeyword);
			if ($sSearchType <> "=") {
				$ar = array();

				// Match quoted keywords (i.e.: "...")
				if (preg_match_all('/"([^"]*)"/i', $sSearch, $matches, PREG_SET_ORDER)) {
					foreach ($matches as $match) {
						$p = strpos($sSearch, $match[0]);
						$str = substr($sSearch, 0, $p);
						$sSearch = substr($sSearch, $p + strlen($match[0]));
						if (strlen(trim($str)) > 0)
							$ar = array_merge($ar, explode(" ", trim($str)));
						$ar[] = $match[1]; // Save quoted keyword
					}
				}

				// Match individual keywords
				if (strlen(trim($sSearch)) > 0)
					$ar = array_merge($ar, explode(" ", trim($sSearch)));

				// Search keyword in any fields
				if (($sSearchType == "OR" || $sSearchType == "AND") && $this->BasicSearch->BasicSearchAnyFields) {
					foreach ($ar as $sKeyword) {
						if ($sKeyword <> "") {
							if ($sSearchStr <> "") $sSearchStr .= " " . $sSearchType . " ";
							$sSearchStr .= "(" . $this->BasicSearchSQL(array($sKeyword), $sSearchType) . ")";
						}
					}
				} else {
					$sSearchStr = $this->BasicSearchSQL($ar, $sSearchType);
				}
			} else {
				$sSearchStr = $this->BasicSearchSQL(array($sSearch), $sSearchType);
			}
			if (!$Default) $this->Command = "search";
		}
		if (!$Default && $this->Command == "search") {
			$this->BasicSearch->setKeyword($sSearchKeyword);
			$this->BasicSearch->setType($sSearchType);
		}
		return $sSearchStr;
	}

	// Check if search parm exists
	function CheckSearchParms() {

		// Check basic search
		if ($this->BasicSearch->IssetSession())
			return TRUE;
		return FALSE;
	}

	// Clear all search parameters
	function ResetSearchParms() {

		// Clear search WHERE clause
		$this->SearchWhere = "";
		$this->setSearchWhere($this->SearchWhere);

		// Clear basic search parameters
		$this->ResetBasicSearchParms();
	}

	// Load advanced search default values
	function LoadAdvancedSearchDefault() {
		return FALSE;
	}

	// Clear all basic search parameters
	function ResetBasicSearchParms() {
		$this->BasicSearch->UnsetSession();
	}

	// Restore all search parameters
	function RestoreSearchParms() {
		$this->RestoreSearch = TRUE;

		// Restore basic search values
		$this->BasicSearch->Load();
	}

	// Set up sort parameters
	function SetUpSortOrder() {

		// Check for Ctrl pressed
		$bCtrl = (@$_GET["ctrl"] <> "");

		// Check for "order" parameter
		if (@$_GET["order"] <> "") {
			$this->CurrentOrder = ew_StripSlashes(@$_GET["order"]);
			$this->CurrentOrderType = @$_GET["ordertype"];
			$this->UpdateSort($this->DcrCodi, $bCtrl); // DcrCodi
			$this->UpdateSort($this->VcrCodi, $bCtrl); // VcrCodi
			$this->UpdateSort($this->DcrFech, $bCtrl); // DcrFech
			$this->UpdateSort($this->DcrMont, $bCtrl); // DcrMont
			$this->UpdateSort($this->DcrSCuo, $bCtrl); // DcrSCuo
			$this->UpdateSort($this->DcrNCuo, $bCtrl); // DcrNCuo
			$this->UpdateSort($this->DcrEsta, $bCtrl); // DcrEsta
			$this->UpdateSort($this->DcrUsu, $bCtrl); // DcrUsu
			$this->setStartRecordNumber(1); // Reset start position
		}
	}

	// Load sort order parameters
	function LoadSortOrder() {
		$sOrderBy = $this->getSessionOrderBy(); // Get ORDER BY from Session
		if ($sOrderBy == "") {
			if ($this->getSqlOrderBy() <> "") {
				$sOrderBy = $this->getSqlOrderBy();
				$this->setSessionOrderBy($sOrderBy);
			}
		}
	}

	// Reset command
	// - cmd=reset (Reset search parameters)
	// - cmd=resetall (Reset search and master/detail parameters)
	// - cmd=resetsort (Reset sort parameters)
	function ResetCmd() {

		// Check if reset command
		if (substr($this->Command,0,5) == "reset") {

			// Reset search criteria
			if ($this->Command == "reset" || $this->Command == "resetall")
				$this->ResetSearchParms();

			// Reset sorting order
			if ($this->Command == "resetsort") {
				$sOrderBy = "";
				$this->setSessionOrderBy($sOrderBy);
				$this->DcrCodi->setSort("");
				$this->VcrCodi->setSort("");
				$this->DcrFech->setSort("");
				$this->DcrMont->setSort("");
				$this->DcrSCuo->setSort("");
				$this->DcrNCuo->setSort("");
				$this->DcrEsta->setSort("");
				$this->DcrUsu->setSort("");
			}

			// Reset start position
			$this->StartRec = 1;
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Set up list options
	function SetupListOptions() {
		global $Security, $Language;

		// "griddelete"
		if ($this->AllowAddDeleteRow) {
			$item = &$this->ListOptions->Add("griddelete");
			$item->CssStyle = "white-space: nowrap;";
			$item->OnLeft = FALSE;
			$item->Visible = FALSE; // Default hidden
		}

		// Add group option item
		$item = &$this->ListOptions->Add($this->ListOptions->GroupOptionName);
		$item->Body = "";
		$item->OnLeft = FALSE;
		$item->Visible = FALSE;

		// "view"
		$item = &$this->ListOptions->Add("view");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->CanView();
		$item->OnLeft = FALSE;

		// "edit"
		$item = &$this->ListOptions->Add("edit");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->CanEdit();
		$item->OnLeft = FALSE;

		// "copy"
		$item = &$this->ListOptions->Add("copy");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->CanAdd();
		$item->OnLeft = FALSE;

		// "delete"
		$item = &$this->ListOptions->Add("delete");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->CanDelete();
		$item->OnLeft = FALSE;

		// List actions
		$item = &$this->ListOptions->Add("listactions");
		$item->CssStyle = "white-space: nowrap;";
		$item->OnLeft = FALSE;
		$item->Visible = FALSE;
		$item->ShowInButtonGroup = FALSE;
		$item->ShowInDropDown = FALSE;

		// "checkbox"
		$item = &$this->ListOptions->Add("checkbox");
		$item->Visible = FALSE;
		$item->OnLeft = FALSE;
		$item->Header = "<input type=\"checkbox\" name=\"key\" id=\"key\" onclick=\"ew_SelectAllKey(this);\">";
		$item->ShowInDropDown = FALSE;
		$item->ShowInButtonGroup = FALSE;

		// Drop down button for ListOptions
		$this->ListOptions->UseImageAndText = TRUE;
		$this->ListOptions->UseDropDownButton = FALSE;
		$this->ListOptions->DropDownButtonPhrase = $Language->Phrase("ButtonListOptions");
		$this->ListOptions->UseButtonGroup = FALSE;
		if ($this->ListOptions->UseButtonGroup && ew_IsMobile())
			$this->ListOptions->UseDropDownButton = TRUE;
		$this->ListOptions->ButtonClass = "btn-sm"; // Class for button group

		// Call ListOptions_Load event
		$this->ListOptions_Load();
		$this->SetupListOptionsExt();
		$item = &$this->ListOptions->GetItem($this->ListOptions->GroupOptionName);
		$item->Visible = $this->ListOptions->GroupOptionVisible();
	}

	// Render list options
	function RenderListOptions() {
		global $Security, $Language, $objForm;
		$this->ListOptions->LoadDefault();

		// Set up row action and key
		if (is_numeric($this->RowIndex) && $this->CurrentMode <> "view") {
			$objForm->Index = $this->RowIndex;
			$ActionName = str_replace("k_", "k" . $this->RowIndex . "_", $this->FormActionName);
			$OldKeyName = str_replace("k_", "k" . $this->RowIndex . "_", $this->FormOldKeyName);
			$KeyName = str_replace("k_", "k" . $this->RowIndex . "_", $this->FormKeyName);
			$BlankRowName = str_replace("k_", "k" . $this->RowIndex . "_", $this->FormBlankRowName);
			if ($this->RowAction <> "")
				$this->MultiSelectKey .= "<input type=\"hidden\" name=\"" . $ActionName . "\" id=\"" . $ActionName . "\" value=\"" . $this->RowAction . "\">";
			if ($this->RowAction == "delete") {
				$rowkey = $objForm->GetValue($this->FormKeyName);
				$this->SetupKeyValues($rowkey);
			}
			if ($this->RowAction == "insert" && $this->CurrentAction == "F" && $this->EmptyRow())
				$this->MultiSelectKey .= "<input type=\"hidden\" name=\"" . $BlankRowName . "\" id=\"" . $BlankRowName . "\" value=\"1\">";
		}

		// "delete"
		if ($this->AllowAddDeleteRow) {
			if ($this->CurrentAction == "gridadd" || $this->CurrentAction == "gridedit") {
				$option = &$this->ListOptions;
				$option->UseButtonGroup = TRUE; // Use button group for grid delete button
				$option->UseImageAndText = TRUE; // Use image and text for grid delete button
				$oListOpt = &$option->Items["griddelete"];
				if (!$Security->CanDelete() && is_numeric($this->RowIndex) && ($this->RowAction == "" || $this->RowAction == "edit")) { // Do not allow delete existing record
					$oListOpt->Body = "&nbsp;";
				} else {
					$oListOpt->Body = "<a class=\"ewGridLink ewGridDelete\" title=\"" . ew_HtmlTitle($Language->Phrase("DeleteLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("DeleteLink")) . "\" onclick=\"return ew_DeleteGridRow(this, " . $this->RowIndex . ");\">" . $Language->Phrase("DeleteLink") . "</a>";
				}
			}
		}

		// "view"
		$oListOpt = &$this->ListOptions->Items["view"];
		if ($Security->CanView())
			$oListOpt->Body = "<a class=\"ewRowLink ewView\" title=\"" . ew_HtmlTitle($Language->Phrase("ViewLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("ViewLink")) . "\" href=\"" . ew_HtmlEncode($this->ViewUrl) . "\">" . $Language->Phrase("ViewLink") . "</a>";
		else
			$oListOpt->Body = "";

		// "edit"
		$oListOpt = &$this->ListOptions->Items["edit"];
		if ($Security->CanEdit()) {
			$oListOpt->Body = "<a class=\"ewRowLink ewEdit\" title=\"" . ew_HtmlTitle($Language->Phrase("EditLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("EditLink")) . "\" href=\"" . ew_HtmlEncode($this->EditUrl) . "\">" . $Language->Phrase("EditLink") . "</a>";
		} else {
			$oListOpt->Body = "";
		}

		// "copy"
		$oListOpt = &$this->ListOptions->Items["copy"];
		if ($Security->CanAdd()) {
			$oListOpt->Body = "<a class=\"ewRowLink ewCopy\" title=\"" . ew_HtmlTitle($Language->Phrase("CopyLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("CopyLink")) . "\" href=\"" . ew_HtmlEncode($this->CopyUrl) . "\">" . $Language->Phrase("CopyLink") . "</a>";
		} else {
			$oListOpt->Body = "";
		}

		// "delete"
		$oListOpt = &$this->ListOptions->Items["delete"];
		if ($Security->CanDelete())
			$oListOpt->Body = "<a class=\"ewRowLink ewDelete\"" . "" . " title=\"" . ew_HtmlTitle($Language->Phrase("DeleteLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("DeleteLink")) . "\" href=\"" . ew_HtmlEncode($this->DeleteUrl) . "\">" . $Language->Phrase("DeleteLink") . "</a>";
		else
			$oListOpt->Body = "";

		// Set up list action buttons
		$oListOpt = &$this->ListOptions->GetItem("listactions");
		if ($oListOpt) {
			$body = "";
			$links = array();
			foreach ($this->ListActions->Items as $listaction) {
				if ($listaction->Select == EW_ACTION_SINGLE && $listaction->Allow) {
					$action = $listaction->Action;
					$caption = $listaction->Caption;
					$icon = ($listaction->Icon <> "") ? "<span class=\"" . ew_HtmlEncode(str_replace(" ewIcon", "", $listaction->Icon)) . "\" data-caption=\"" . ew_HtmlTitle($caption) . "\"></span> " : "";
					$links[] = "<li><a class=\"ewAction ewListAction\" data-action=\"" . ew_HtmlEncode($action) . "\" data-caption=\"" . ew_HtmlTitle($caption) . "\" href=\"\" onclick=\"ew_SubmitAction(event,jQuery.extend({key:" . $this->KeyToJson() . "}," . $listaction->ToJson(TRUE) . "));return false;\">" . $icon . $listaction->Caption . "</a></li>";
					if (count($links) == 1) // Single button
						$body = "<a class=\"ewAction ewListAction\" data-action=\"" . ew_HtmlEncode($action) . "\" title=\"" . ew_HtmlTitle($caption) . "\" data-caption=\"" . ew_HtmlTitle($caption) . "\" href=\"\" onclick=\"ew_SubmitAction(event,jQuery.extend({key:" . $this->KeyToJson() . "}," . $listaction->ToJson(TRUE) . "));return false;\">" . $Language->Phrase("ListActionButton") . "</a>";
				}
			}
			if (count($links) > 1) { // More than one buttons, use dropdown
				$body = "<button class=\"dropdown-toggle btn btn-default btn-sm ewActions\" title=\"" . ew_HtmlTitle($Language->Phrase("ListActionButton")) . "\" data-toggle=\"dropdown\">" . $Language->Phrase("ListActionButton") . "<b class=\"caret\"></b></button>";
				$content = "";
				foreach ($links as $link)
					$content .= "<li>" . $link . "</li>";
				$body .= "<ul class=\"dropdown-menu" . ($oListOpt->OnLeft ? "" : " dropdown-menu-right") . "\">". $content . "</ul>";
				$body = "<div class=\"btn-group\">" . $body . "</div>";
			}
			if (count($links) > 0) {
				$oListOpt->Body = $body;
				$oListOpt->Visible = TRUE;
			}
		}

		// "checkbox"
		$oListOpt = &$this->ListOptions->Items["checkbox"];
		$oListOpt->Body = "<input type=\"checkbox\" name=\"key_m[]\" value=\"" . ew_HtmlEncode($this->DcrCodi->CurrentValue) . "\" onclick='ew_ClickMultiCheckbox(event);'>";
		if ($this->CurrentAction == "gridedit" && is_numeric($this->RowIndex)) {
			$this->MultiSelectKey .= "<input type=\"hidden\" name=\"" . $KeyName . "\" id=\"" . $KeyName . "\" value=\"" . $this->DcrCodi->CurrentValue . "\">";
		}
		$this->RenderListOptionsExt();

		// Call ListOptions_Rendered event
		$this->ListOptions_Rendered();
	}

	// Set up other options
	function SetupOtherOptions() {
		global $Language, $Security;
		$options = &$this->OtherOptions;
		$option = $options["addedit"];

		// Add
		$item = &$option->Add("add");
		$item->Body = "<a class=\"ewAddEdit ewAdd\" title=\"" . ew_HtmlTitle($Language->Phrase("AddLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("AddLink")) . "\" href=\"" . ew_HtmlEncode($this->AddUrl) . "\">" . $Language->Phrase("AddLink") . "</a>";
		$item->Visible = ($this->AddUrl <> "" && $Security->CanAdd());
		$item = &$option->Add("gridadd");
		$item->Body = "<a class=\"ewAddEdit ewGridAdd\" title=\"" . ew_HtmlTitle($Language->Phrase("GridAddLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("GridAddLink")) . "\" href=\"" . ew_HtmlEncode($this->GridAddUrl) . "\">" . $Language->Phrase("GridAddLink") . "</a>";
		$item->Visible = ($this->GridAddUrl <> "" && $Security->CanAdd());

		// Add grid edit
		$option = $options["addedit"];
		$item = &$option->Add("gridedit");
		$item->Body = "<a class=\"ewAddEdit ewGridEdit\" title=\"" . ew_HtmlTitle($Language->Phrase("GridEditLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("GridEditLink")) . "\" href=\"" . ew_HtmlEncode($this->GridEditUrl) . "\">" . $Language->Phrase("GridEditLink") . "</a>";
		$item->Visible = ($this->GridEditUrl <> "" && $Security->CanEdit());
		$option = $options["action"];

		// Set up options default
		foreach ($options as &$option) {
			$option->UseImageAndText = TRUE;
			$option->UseDropDownButton = FALSE;
			$option->UseButtonGroup = TRUE;
			$option->ButtonClass = "btn-sm"; // Class for button group
			$item = &$option->Add($option->GroupOptionName);
			$item->Body = "";
			$item->Visible = FALSE;
		}
		$options["addedit"]->DropDownButtonPhrase = $Language->Phrase("ButtonAddEdit");
		$options["detail"]->DropDownButtonPhrase = $Language->Phrase("ButtonDetails");
		$options["action"]->DropDownButtonPhrase = $Language->Phrase("ButtonActions");

		// Filter button
		$item = &$this->FilterOptions->Add("savecurrentfilter");
		$item->Body = "<a class=\"ewSaveFilter\" data-form=\"fDCrelistsrch\" href=\"#\">" . $Language->Phrase("SaveCurrentFilter") . "</a>";
		$item->Visible = TRUE;
		$item = &$this->FilterOptions->Add("deletefilter");
		$item->Body = "<a class=\"ewDeleteFilter\" data-form=\"fDCrelistsrch\" href=\"#\">" . $Language->Phrase("DeleteFilter") . "</a>";
		$item->Visible = TRUE;
		$this->FilterOptions->UseDropDownButton = TRUE;
		$this->FilterOptions->UseButtonGroup = !$this->FilterOptions->UseDropDownButton;
		$this->FilterOptions->DropDownButtonPhrase = $Language->Phrase("Filters");

		// Add group option item
		$item = &$this->FilterOptions->Add($this->FilterOptions->GroupOptionName);
		$item->Body = "";
		$item->Visible = FALSE;
	}

	// Render other options
	function RenderOtherOptions() {
		global $Language, $Security;
		$options = &$this->OtherOptions;
		if ($this->CurrentAction <> "gridadd" && $this->CurrentAction <> "gridedit") { // Not grid add/edit mode
			$option = &$options["action"];

			// Set up list action buttons
			foreach ($this->ListActions->Items as $listaction) {
				if ($listaction->Select == EW_ACTION_MULTIPLE) {
					$item = &$option->Add("custom_" . $listaction->Action);
					$caption = $listaction->Caption;
					$icon = ($listaction->Icon <> "") ? "<span class=\"" . ew_HtmlEncode($listaction->Icon) . "\" data-caption=\"" . ew_HtmlEncode($caption) . "\"></span> " : $caption;
					$item->Body = "<a class=\"ewAction ewListAction\" title=\"" . ew_HtmlEncode($caption) . "\" data-caption=\"" . ew_HtmlEncode($caption) . "\" href=\"\" onclick=\"ew_SubmitAction(event,jQuery.extend({f:document.fDCrelist}," . $listaction->ToJson(TRUE) . "));return false;\">" . $icon . "</a>";
					$item->Visible = $listaction->Allow;
				}
			}

			// Hide grid edit and other options
			if ($this->TotalRecs <= 0) {
				$option = &$options["addedit"];
				$item = &$option->GetItem("gridedit");
				if ($item) $item->Visible = FALSE;
				$option = &$options["action"];
				$option->HideAllOptions();
			}
		} else { // Grid add/edit mode

			// Hide all options first
			foreach ($options as &$option)
				$option->HideAllOptions();
			if ($this->CurrentAction == "gridadd") {
				if ($this->AllowAddDeleteRow) {

					// Add add blank row
					$option = &$options["addedit"];
					$option->UseDropDownButton = FALSE;
					$option->UseImageAndText = TRUE;
					$item = &$option->Add("addblankrow");
					$item->Body = "<a class=\"ewAddEdit ewAddBlankRow\" title=\"" . ew_HtmlTitle($Language->Phrase("AddBlankRow")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("AddBlankRow")) . "\" href=\"javascript:void(0);\" onclick=\"ew_AddGridRow(this);\">" . $Language->Phrase("AddBlankRow") . "</a>";
					$item->Visible = $Security->CanAdd();
				}
				$option = &$options["action"];
				$option->UseDropDownButton = FALSE;
				$option->UseImageAndText = TRUE;

				// Add grid insert
				$item = &$option->Add("gridinsert");
				$item->Body = "<a class=\"ewAction ewGridInsert\" title=\"" . ew_HtmlTitle($Language->Phrase("GridInsertLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("GridInsertLink")) . "\" href=\"\" onclick=\"return ewForms(this).Submit('" . $this->AddMasterUrl($this->PageName()) . "');\">" . $Language->Phrase("GridInsertLink") . "</a>";

				// Add grid cancel
				$item = &$option->Add("gridcancel");
				$cancelurl = $this->AddMasterUrl($this->PageUrl() . "a=cancel");
				$item->Body = "<a class=\"ewAction ewGridCancel\" title=\"" . ew_HtmlTitle($Language->Phrase("GridCancelLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("GridCancelLink")) . "\" href=\"" . $cancelurl . "\">" . $Language->Phrase("GridCancelLink") . "</a>";
			}
			if ($this->CurrentAction == "gridedit") {
				if ($this->AllowAddDeleteRow) {

					// Add add blank row
					$option = &$options["addedit"];
					$option->UseDropDownButton = FALSE;
					$option->UseImageAndText = TRUE;
					$item = &$option->Add("addblankrow");
					$item->Body = "<a class=\"ewAddEdit ewAddBlankRow\" title=\"" . ew_HtmlTitle($Language->Phrase("AddBlankRow")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("AddBlankRow")) . "\" href=\"javascript:void(0);\" onclick=\"ew_AddGridRow(this);\">" . $Language->Phrase("AddBlankRow") . "</a>";
					$item->Visible = $Security->CanAdd();
				}
				$option = &$options["action"];
				$option->UseDropDownButton = FALSE;
				$option->UseImageAndText = TRUE;
					$item = &$option->Add("gridsave");
					$item->Body = "<a class=\"ewAction ewGridSave\" title=\"" . ew_HtmlTitle($Language->Phrase("GridSaveLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("GridSaveLink")) . "\" href=\"\" onclick=\"return ewForms(this).Submit('" . $this->AddMasterUrl($this->PageName()) . "');\">" . $Language->Phrase("GridSaveLink") . "</a>";
					$item = &$option->Add("gridcancel");
					$cancelurl = $this->AddMasterUrl($this->PageUrl() . "a=cancel");
					$item->Body = "<a class=\"ewAction ewGridCancel\" title=\"" . ew_HtmlTitle($Language->Phrase("GridCancelLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("GridCancelLink")) . "\" href=\"" . $cancelurl . "\">" . $Language->Phrase("GridCancelLink") . "</a>";
			}
		}
	}

	// Process list action
	function ProcessListAction() {
		global $Language, $Security;
		$userlist = "";
		$user = "";
		$sFilter = $this->GetKeyFilter();
		$UserAction = @$_POST["useraction"];
		if ($sFilter <> "" && $UserAction <> "") {

			// Check permission first
			$ActionCaption = $UserAction;
			if (array_key_exists($UserAction, $this->ListActions->Items)) {
				$ActionCaption = $this->ListActions->Items[$UserAction]->Caption;
				if (!$this->ListActions->Items[$UserAction]->Allow) {
					$errmsg = str_replace('%s', $ActionCaption, $Language->Phrase("CustomActionNotAllowed"));
					if (@$_POST["ajax"] == $UserAction) // Ajax
						echo "<p class=\"text-danger\">" . $errmsg . "</p>";
					else
						$this->setFailureMessage($errmsg);
					return FALSE;
				}
			}
			$this->CurrentFilter = $sFilter;
			$sSql = $this->SQL();
			$conn = &$this->Connection();
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			$rs = $conn->Execute($sSql);
			$conn->raiseErrorFn = '';
			$this->CurrentAction = $UserAction;

			// Call row action event
			if ($rs && !$rs->EOF) {
				$conn->BeginTrans();
				$this->SelectedCount = $rs->RecordCount();
				$this->SelectedIndex = 0;
				while (!$rs->EOF) {
					$this->SelectedIndex++;
					$row = $rs->fields;
					$Processed = $this->Row_CustomAction($UserAction, $row);
					if (!$Processed) break;
					$rs->MoveNext();
				}
				if ($Processed) {
					$conn->CommitTrans(); // Commit the changes
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage(str_replace('%s', $ActionCaption, $Language->Phrase("CustomActionCompleted"))); // Set up success message
				} else {
					$conn->RollbackTrans(); // Rollback changes

					// Set up error message
					if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

						// Use the message, do nothing
					} elseif ($this->CancelMessage <> "") {
						$this->setFailureMessage($this->CancelMessage);
						$this->CancelMessage = "";
					} else {
						$this->setFailureMessage(str_replace('%s', $ActionCaption, $Language->Phrase("CustomActionFailed")));
					}
				}
			}
			if ($rs)
				$rs->Close();
			if (@$_POST["ajax"] == $UserAction) { // Ajax
				if ($this->getSuccessMessage() <> "") {
					echo "<p class=\"text-success\">" . $this->getSuccessMessage() . "</p>";
					$this->ClearSuccessMessage(); // Clear message
				}
				if ($this->getFailureMessage() <> "") {
					echo "<p class=\"text-danger\">" . $this->getFailureMessage() . "</p>";
					$this->ClearFailureMessage(); // Clear message
				}
				return TRUE;
			}
		}
		return FALSE; // Not ajax request
	}

	// Set up search options
	function SetupSearchOptions() {
		global $Language;
		$this->SearchOptions = new cListOptions();
		$this->SearchOptions->Tag = "div";
		$this->SearchOptions->TagClassName = "ewSearchOption";

		// Search button
		$item = &$this->SearchOptions->Add("searchtoggle");
		$SearchToggleClass = ($this->SearchWhere <> "") ? " active" : " active";
		$item->Body = "<button type=\"button\" class=\"btn btn-default ewSearchToggle" . $SearchToggleClass . "\" title=\"" . $Language->Phrase("SearchPanel") . "\" data-caption=\"" . $Language->Phrase("SearchPanel") . "\" data-toggle=\"button\" data-form=\"fDCrelistsrch\">" . $Language->Phrase("SearchBtn") . "</button>";
		$item->Visible = TRUE;

		// Show all button
		$item = &$this->SearchOptions->Add("showall");
		$item->Body = "<a class=\"btn btn-default ewShowAll\" title=\"" . $Language->Phrase("ShowAll") . "\" data-caption=\"" . $Language->Phrase("ShowAll") . "\" href=\"" . $this->PageUrl() . "cmd=reset\">" . $Language->Phrase("ShowAllBtn") . "</a>";
		$item->Visible = ($this->SearchWhere <> $this->DefaultSearchWhere && $this->SearchWhere <> "0=101");

		// Button group for search
		$this->SearchOptions->UseDropDownButton = FALSE;
		$this->SearchOptions->UseImageAndText = TRUE;
		$this->SearchOptions->UseButtonGroup = TRUE;
		$this->SearchOptions->DropDownButtonPhrase = $Language->Phrase("ButtonSearch");

		// Add group option item
		$item = &$this->SearchOptions->Add($this->SearchOptions->GroupOptionName);
		$item->Body = "";
		$item->Visible = FALSE;

		// Hide search options
		if ($this->Export <> "" || $this->CurrentAction <> "")
			$this->SearchOptions->HideAllOptions();
		global $Security;
		if (!$Security->CanSearch()) {
			$this->SearchOptions->HideAllOptions();
			$this->FilterOptions->HideAllOptions();
		}
	}

	function SetupListOptionsExt() {
		global $Security, $Language;
	}

	function RenderListOptionsExt() {
		global $Security, $Language;
	}

	// Set up starting record parameters
	function SetUpStartRec() {
		if ($this->DisplayRecs == 0)
			return;
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET[EW_TABLE_START_REC] <> "") { // Check for "start" parameter
				$this->StartRec = $_GET[EW_TABLE_START_REC];
				$this->setStartRecordNumber($this->StartRec);
			} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
				$PageNo = $_GET[EW_TABLE_PAGE_NO];
				if (is_numeric($PageNo)) {
					$this->StartRec = ($PageNo-1)*$this->DisplayRecs+1;
					if ($this->StartRec <= 0) {
						$this->StartRec = 1;
					} elseif ($this->StartRec >= intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1) {
						$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1;
					}
					$this->setStartRecordNumber($this->StartRec);
				}
			}
		}
		$this->StartRec = $this->getStartRecordNumber();

		// Check if correct start record counter
		if (!is_numeric($this->StartRec) || $this->StartRec == "") { // Avoid invalid start record counter
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} elseif (intval($this->StartRec) > intval($this->TotalRecs)) { // Avoid starting record > total records
			$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to last page first record
			$this->setStartRecordNumber($this->StartRec);
		} elseif (($this->StartRec-1) % $this->DisplayRecs <> 0) {
			$this->StartRec = intval(($this->StartRec-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to page boundary
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Load default values
	function LoadDefaultValues() {
		$this->DcrCodi->CurrentValue = NULL;
		$this->DcrCodi->OldValue = $this->DcrCodi->CurrentValue;
		$this->VcrCodi->CurrentValue = NULL;
		$this->VcrCodi->OldValue = $this->VcrCodi->CurrentValue;
		$this->DcrFech->CurrentValue = NULL;
		$this->DcrFech->OldValue = $this->DcrFech->CurrentValue;
		$this->DcrMont->CurrentValue = NULL;
		$this->DcrMont->OldValue = $this->DcrMont->CurrentValue;
		$this->DcrSCuo->CurrentValue = NULL;
		$this->DcrSCuo->OldValue = $this->DcrSCuo->CurrentValue;
		$this->DcrNCuo->CurrentValue = NULL;
		$this->DcrNCuo->OldValue = $this->DcrNCuo->CurrentValue;
		$this->DcrEsta->CurrentValue = NULL;
		$this->DcrEsta->OldValue = $this->DcrEsta->CurrentValue;
		$this->DcrUsu->CurrentValue = NULL;
		$this->DcrUsu->OldValue = $this->DcrUsu->CurrentValue;
	}

	// Load basic search values
	function LoadBasicSearchValues() {
		$this->BasicSearch->Keyword = @$_GET[EW_TABLE_BASIC_SEARCH];
		if ($this->BasicSearch->Keyword <> "") $this->Command = "search";
		$this->BasicSearch->Type = @$_GET[EW_TABLE_BASIC_SEARCH_TYPE];
	}

	// Load form values
	function LoadFormValues() {

		// Load from form
		global $objForm;
		if (!$this->DcrCodi->FldIsDetailKey) {
			$this->DcrCodi->setFormValue($objForm->GetValue("x_DcrCodi"));
		}
		$this->DcrCodi->setOldValue($objForm->GetValue("o_DcrCodi"));
		if (!$this->VcrCodi->FldIsDetailKey) {
			$this->VcrCodi->setFormValue($objForm->GetValue("x_VcrCodi"));
		}
		$this->VcrCodi->setOldValue($objForm->GetValue("o_VcrCodi"));
		if (!$this->DcrFech->FldIsDetailKey) {
			$this->DcrFech->setFormValue($objForm->GetValue("x_DcrFech"));
			$this->DcrFech->CurrentValue = ew_UnFormatDateTime($this->DcrFech->CurrentValue, 7);
		}
		$this->DcrFech->setOldValue($objForm->GetValue("o_DcrFech"));
		if (!$this->DcrMont->FldIsDetailKey) {
			$this->DcrMont->setFormValue($objForm->GetValue("x_DcrMont"));
		}
		$this->DcrMont->setOldValue($objForm->GetValue("o_DcrMont"));
		if (!$this->DcrSCuo->FldIsDetailKey) {
			$this->DcrSCuo->setFormValue($objForm->GetValue("x_DcrSCuo"));
		}
		$this->DcrSCuo->setOldValue($objForm->GetValue("o_DcrSCuo"));
		if (!$this->DcrNCuo->FldIsDetailKey) {
			$this->DcrNCuo->setFormValue($objForm->GetValue("x_DcrNCuo"));
		}
		$this->DcrNCuo->setOldValue($objForm->GetValue("o_DcrNCuo"));
		if (!$this->DcrEsta->FldIsDetailKey) {
			$this->DcrEsta->setFormValue($objForm->GetValue("x_DcrEsta"));
		}
		$this->DcrEsta->setOldValue($objForm->GetValue("o_DcrEsta"));
		if (!$this->DcrUsu->FldIsDetailKey) {
			$this->DcrUsu->setFormValue($objForm->GetValue("x_DcrUsu"));
		}
		$this->DcrUsu->setOldValue($objForm->GetValue("o_DcrUsu"));
	}

	// Restore form values
	function RestoreFormValues() {
		global $objForm;
		$this->DcrCodi->CurrentValue = $this->DcrCodi->FormValue;
		$this->VcrCodi->CurrentValue = $this->VcrCodi->FormValue;
		$this->DcrFech->CurrentValue = $this->DcrFech->FormValue;
		$this->DcrFech->CurrentValue = ew_UnFormatDateTime($this->DcrFech->CurrentValue, 7);
		$this->DcrMont->CurrentValue = $this->DcrMont->FormValue;
		$this->DcrSCuo->CurrentValue = $this->DcrSCuo->FormValue;
		$this->DcrNCuo->CurrentValue = $this->DcrNCuo->FormValue;
		$this->DcrEsta->CurrentValue = $this->DcrEsta->FormValue;
		$this->DcrUsu->CurrentValue = $this->DcrUsu->FormValue;
	}

	// Load recordset
	function LoadRecordset($offset = -1, $rowcnt = -1) {

		// Load List page SQL
		$sSql = $this->SelectSQL();
		$conn = &$this->Connection();

		// Load recordset
		$dbtype = ew_GetConnectionType($this->DBID);
		if ($this->UseSelectLimit) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			if ($dbtype == "MSSQL") {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset, array("_hasOrderBy" => trim($this->getOrderBy()) || trim($this->getSessionOrderBy())));
			} else {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset);
			}
			$conn->raiseErrorFn = '';
		} else {
			$rs = ew_LoadRecordset($sSql, $conn);
		}

		// Call Recordset Selected event
		$this->Recordset_Selected($rs);
		return $rs;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->DcrCodi->setDbValue($rs->fields('DcrCodi'));
		$this->VcrCodi->setDbValue($rs->fields('VcrCodi'));
		$this->DcrFech->setDbValue($rs->fields('DcrFech'));
		$this->DcrMont->setDbValue($rs->fields('DcrMont'));
		$this->DcrSCuo->setDbValue($rs->fields('DcrSCuo'));
		$this->DcrNCuo->setDbValue($rs->fields('DcrNCuo'));
		$this->DcrEsta->setDbValue($rs->fields('DcrEsta'));
		$this->DcrUsu->setDbValue($rs->fields('DcrUsu'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->DcrCodi->DbValue = $row['DcrCodi'];
		$this->VcrCodi->DbValue = $row['VcrCodi'];
		$this->DcrFech->DbValue = $row['DcrFech'];
		$this->DcrMont->DbValue = $row['DcrMont'];
		$this->DcrSCuo->DbValue = $row['DcrSCuo'];
		$this->DcrNCuo->DbValue = $row['DcrNCuo'];
		$this->DcrEsta->DbValue = $row['DcrEsta'];
		$this->DcrUsu->DbValue = $row['DcrUsu'];
	}

	// Load old record
	function LoadOldRecord() {

		// Load key values from Session
		$bValidKey = TRUE;
		if (strval($this->getKey("DcrCodi")) <> "")
			$this->DcrCodi->CurrentValue = $this->getKey("DcrCodi"); // DcrCodi
		else
			$bValidKey = FALSE;

		// Load old recordset
		if ($bValidKey) {
			$this->CurrentFilter = $this->KeyFilter();
			$sSql = $this->SQL();
			$conn = &$this->Connection();
			$this->OldRecordset = ew_LoadRecordset($sSql, $conn);
			$this->LoadRowValues($this->OldRecordset); // Load row values
		} else {
			$this->OldRecordset = NULL;
		}
		return $bValidKey;
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		$this->ViewUrl = $this->GetViewUrl();
		$this->EditUrl = $this->GetEditUrl();
		$this->InlineEditUrl = $this->GetInlineEditUrl();
		$this->CopyUrl = $this->GetCopyUrl();
		$this->InlineCopyUrl = $this->GetInlineCopyUrl();
		$this->DeleteUrl = $this->GetDeleteUrl();

		// Convert decimal values if posted back
		if ($this->DcrMont->FormValue == $this->DcrMont->CurrentValue && is_numeric(ew_StrToFloat($this->DcrMont->CurrentValue)))
			$this->DcrMont->CurrentValue = ew_StrToFloat($this->DcrMont->CurrentValue);

		// Convert decimal values if posted back
		if ($this->DcrSCuo->FormValue == $this->DcrSCuo->CurrentValue && is_numeric(ew_StrToFloat($this->DcrSCuo->CurrentValue)))
			$this->DcrSCuo->CurrentValue = ew_StrToFloat($this->DcrSCuo->CurrentValue);

		// Call Row_Rendering event
		$this->Row_Rendering();

		// Common render codes for all row types
		// DcrCodi
		// VcrCodi
		// DcrFech
		// DcrMont
		// DcrSCuo
		// DcrNCuo
		// DcrEsta
		// DcrUsu

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// DcrCodi
		$this->DcrCodi->ViewValue = $this->DcrCodi->CurrentValue;
		$this->DcrCodi->ViewCustomAttributes = "";

		// VcrCodi
		if (strval($this->VcrCodi->CurrentValue) <> "") {
			$sFilterWrk = "\"VcrCodi\"" . ew_SearchString("=", $this->VcrCodi->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT \"VcrCodi\", \"VcrCodi\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"VCre\"";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->VcrCodi, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->VcrCodi->ViewValue = $this->VcrCodi->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->VcrCodi->ViewValue = $this->VcrCodi->CurrentValue;
			}
		} else {
			$this->VcrCodi->ViewValue = NULL;
		}
		$this->VcrCodi->ViewCustomAttributes = "";

		// DcrFech
		$this->DcrFech->ViewValue = $this->DcrFech->CurrentValue;
		$this->DcrFech->ViewValue = ew_FormatDateTime($this->DcrFech->ViewValue, 7);
		$this->DcrFech->ViewCustomAttributes = "";

		// DcrMont
		$this->DcrMont->ViewValue = $this->DcrMont->CurrentValue;
		$this->DcrMont->ViewCustomAttributes = "";

		// DcrSCuo
		$this->DcrSCuo->ViewValue = $this->DcrSCuo->CurrentValue;
		$this->DcrSCuo->ViewCustomAttributes = "";

		// DcrNCuo
		$this->DcrNCuo->ViewValue = $this->DcrNCuo->CurrentValue;
		$this->DcrNCuo->ViewCustomAttributes = "";

		// DcrEsta
		$this->DcrEsta->ViewValue = $this->DcrEsta->CurrentValue;
		$this->DcrEsta->ViewCustomAttributes = "";

		// DcrUsu
		$this->DcrUsu->ViewValue = $this->DcrUsu->CurrentValue;
		$this->DcrUsu->ViewCustomAttributes = "";

			// DcrCodi
			$this->DcrCodi->LinkCustomAttributes = "";
			$this->DcrCodi->HrefValue = "";
			$this->DcrCodi->TooltipValue = "";

			// VcrCodi
			$this->VcrCodi->LinkCustomAttributes = "";
			$this->VcrCodi->HrefValue = "";
			$this->VcrCodi->TooltipValue = "";

			// DcrFech
			$this->DcrFech->LinkCustomAttributes = "";
			$this->DcrFech->HrefValue = "";
			$this->DcrFech->TooltipValue = "";

			// DcrMont
			$this->DcrMont->LinkCustomAttributes = "";
			$this->DcrMont->HrefValue = "";
			$this->DcrMont->TooltipValue = "";

			// DcrSCuo
			$this->DcrSCuo->LinkCustomAttributes = "";
			$this->DcrSCuo->HrefValue = "";
			$this->DcrSCuo->TooltipValue = "";

			// DcrNCuo
			$this->DcrNCuo->LinkCustomAttributes = "";
			$this->DcrNCuo->HrefValue = "";
			$this->DcrNCuo->TooltipValue = "";

			// DcrEsta
			$this->DcrEsta->LinkCustomAttributes = "";
			$this->DcrEsta->HrefValue = "";
			$this->DcrEsta->TooltipValue = "";

			// DcrUsu
			$this->DcrUsu->LinkCustomAttributes = "";
			$this->DcrUsu->HrefValue = "";
			$this->DcrUsu->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_ADD) { // Add row

			// DcrCodi
			$this->DcrCodi->EditAttrs["class"] = "form-control";
			$this->DcrCodi->EditCustomAttributes = "";
			$this->DcrCodi->EditValue = ew_HtmlEncode($this->DcrCodi->CurrentValue);
			$this->DcrCodi->PlaceHolder = ew_RemoveHtml($this->DcrCodi->FldCaption());

			// VcrCodi
			$this->VcrCodi->EditAttrs["class"] = "form-control";
			$this->VcrCodi->EditCustomAttributes = "";
			if (trim(strval($this->VcrCodi->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "\"VcrCodi\"" . ew_SearchString("=", $this->VcrCodi->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT \"VcrCodi\", \"VcrCodi\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\", '' AS \"SelectFilterFld\", '' AS \"SelectFilterFld2\", '' AS \"SelectFilterFld3\", '' AS \"SelectFilterFld4\" FROM \"public\".\"VCre\"";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->VcrCodi, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->VcrCodi->EditValue = $arwrk;

			// DcrFech
			$this->DcrFech->EditAttrs["class"] = "form-control";
			$this->DcrFech->EditCustomAttributes = "";
			$this->DcrFech->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->DcrFech->CurrentValue, 7));
			$this->DcrFech->PlaceHolder = ew_RemoveHtml($this->DcrFech->FldCaption());

			// DcrMont
			$this->DcrMont->EditAttrs["class"] = "form-control";
			$this->DcrMont->EditCustomAttributes = "";
			$this->DcrMont->EditValue = ew_HtmlEncode($this->DcrMont->CurrentValue);
			$this->DcrMont->PlaceHolder = ew_RemoveHtml($this->DcrMont->FldCaption());
			if (strval($this->DcrMont->EditValue) <> "" && is_numeric($this->DcrMont->EditValue)) {
			$this->DcrMont->EditValue = ew_FormatNumber($this->DcrMont->EditValue, -2, -1, -2, 0);
			$this->DcrMont->OldValue = $this->DcrMont->EditValue;
			}

			// DcrSCuo
			$this->DcrSCuo->EditAttrs["class"] = "form-control";
			$this->DcrSCuo->EditCustomAttributes = "";
			$this->DcrSCuo->EditValue = ew_HtmlEncode($this->DcrSCuo->CurrentValue);
			$this->DcrSCuo->PlaceHolder = ew_RemoveHtml($this->DcrSCuo->FldCaption());
			if (strval($this->DcrSCuo->EditValue) <> "" && is_numeric($this->DcrSCuo->EditValue)) {
			$this->DcrSCuo->EditValue = ew_FormatNumber($this->DcrSCuo->EditValue, -2, -1, -2, 0);
			$this->DcrSCuo->OldValue = $this->DcrSCuo->EditValue;
			}

			// DcrNCuo
			$this->DcrNCuo->EditAttrs["class"] = "form-control";
			$this->DcrNCuo->EditCustomAttributes = "";
			$this->DcrNCuo->EditValue = ew_HtmlEncode($this->DcrNCuo->CurrentValue);
			$this->DcrNCuo->PlaceHolder = ew_RemoveHtml($this->DcrNCuo->FldCaption());

			// DcrEsta
			$this->DcrEsta->EditAttrs["class"] = "form-control";
			$this->DcrEsta->EditCustomAttributes = "";
			$this->DcrEsta->EditValue = ew_HtmlEncode($this->DcrEsta->CurrentValue);
			$this->DcrEsta->PlaceHolder = ew_RemoveHtml($this->DcrEsta->FldCaption());

			// DcrUsu
			$this->DcrUsu->EditAttrs["class"] = "form-control";
			$this->DcrUsu->EditCustomAttributes = "";
			$this->DcrUsu->EditValue = ew_HtmlEncode($this->DcrUsu->CurrentValue);
			$this->DcrUsu->PlaceHolder = ew_RemoveHtml($this->DcrUsu->FldCaption());

			// Edit refer script
			// DcrCodi

			$this->DcrCodi->HrefValue = "";

			// VcrCodi
			$this->VcrCodi->HrefValue = "";

			// DcrFech
			$this->DcrFech->HrefValue = "";

			// DcrMont
			$this->DcrMont->HrefValue = "";

			// DcrSCuo
			$this->DcrSCuo->HrefValue = "";

			// DcrNCuo
			$this->DcrNCuo->HrefValue = "";

			// DcrEsta
			$this->DcrEsta->HrefValue = "";

			// DcrUsu
			$this->DcrUsu->HrefValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_EDIT) { // Edit row

			// DcrCodi
			$this->DcrCodi->EditAttrs["class"] = "form-control";
			$this->DcrCodi->EditCustomAttributes = "";
			$this->DcrCodi->EditValue = $this->DcrCodi->CurrentValue;
			$this->DcrCodi->ViewCustomAttributes = "";

			// VcrCodi
			$this->VcrCodi->EditAttrs["class"] = "form-control";
			$this->VcrCodi->EditCustomAttributes = "";
			if (trim(strval($this->VcrCodi->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "\"VcrCodi\"" . ew_SearchString("=", $this->VcrCodi->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT \"VcrCodi\", \"VcrCodi\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\", '' AS \"SelectFilterFld\", '' AS \"SelectFilterFld2\", '' AS \"SelectFilterFld3\", '' AS \"SelectFilterFld4\" FROM \"public\".\"VCre\"";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->VcrCodi, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->VcrCodi->EditValue = $arwrk;

			// DcrFech
			$this->DcrFech->EditAttrs["class"] = "form-control";
			$this->DcrFech->EditCustomAttributes = "";
			$this->DcrFech->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->DcrFech->CurrentValue, 7));
			$this->DcrFech->PlaceHolder = ew_RemoveHtml($this->DcrFech->FldCaption());

			// DcrMont
			$this->DcrMont->EditAttrs["class"] = "form-control";
			$this->DcrMont->EditCustomAttributes = "";
			$this->DcrMont->EditValue = ew_HtmlEncode($this->DcrMont->CurrentValue);
			$this->DcrMont->PlaceHolder = ew_RemoveHtml($this->DcrMont->FldCaption());
			if (strval($this->DcrMont->EditValue) <> "" && is_numeric($this->DcrMont->EditValue)) {
			$this->DcrMont->EditValue = ew_FormatNumber($this->DcrMont->EditValue, -2, -1, -2, 0);
			$this->DcrMont->OldValue = $this->DcrMont->EditValue;
			}

			// DcrSCuo
			$this->DcrSCuo->EditAttrs["class"] = "form-control";
			$this->DcrSCuo->EditCustomAttributes = "";
			$this->DcrSCuo->EditValue = ew_HtmlEncode($this->DcrSCuo->CurrentValue);
			$this->DcrSCuo->PlaceHolder = ew_RemoveHtml($this->DcrSCuo->FldCaption());
			if (strval($this->DcrSCuo->EditValue) <> "" && is_numeric($this->DcrSCuo->EditValue)) {
			$this->DcrSCuo->EditValue = ew_FormatNumber($this->DcrSCuo->EditValue, -2, -1, -2, 0);
			$this->DcrSCuo->OldValue = $this->DcrSCuo->EditValue;
			}

			// DcrNCuo
			$this->DcrNCuo->EditAttrs["class"] = "form-control";
			$this->DcrNCuo->EditCustomAttributes = "";
			$this->DcrNCuo->EditValue = ew_HtmlEncode($this->DcrNCuo->CurrentValue);
			$this->DcrNCuo->PlaceHolder = ew_RemoveHtml($this->DcrNCuo->FldCaption());

			// DcrEsta
			$this->DcrEsta->EditAttrs["class"] = "form-control";
			$this->DcrEsta->EditCustomAttributes = "";
			$this->DcrEsta->EditValue = ew_HtmlEncode($this->DcrEsta->CurrentValue);
			$this->DcrEsta->PlaceHolder = ew_RemoveHtml($this->DcrEsta->FldCaption());

			// DcrUsu
			$this->DcrUsu->EditAttrs["class"] = "form-control";
			$this->DcrUsu->EditCustomAttributes = "";
			$this->DcrUsu->EditValue = ew_HtmlEncode($this->DcrUsu->CurrentValue);
			$this->DcrUsu->PlaceHolder = ew_RemoveHtml($this->DcrUsu->FldCaption());

			// Edit refer script
			// DcrCodi

			$this->DcrCodi->HrefValue = "";

			// VcrCodi
			$this->VcrCodi->HrefValue = "";

			// DcrFech
			$this->DcrFech->HrefValue = "";

			// DcrMont
			$this->DcrMont->HrefValue = "";

			// DcrSCuo
			$this->DcrSCuo->HrefValue = "";

			// DcrNCuo
			$this->DcrNCuo->HrefValue = "";

			// DcrEsta
			$this->DcrEsta->HrefValue = "";

			// DcrUsu
			$this->DcrUsu->HrefValue = "";
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate form
	function ValidateForm() {
		global $Language, $gsFormError;

		// Initialize form error message
		$gsFormError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return ($gsFormError == "");
		if (!$this->DcrCodi->FldIsDetailKey && !is_null($this->DcrCodi->FormValue) && $this->DcrCodi->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->DcrCodi->FldCaption(), $this->DcrCodi->ReqErrMsg));
		}
		if (!ew_CheckInteger($this->DcrCodi->FormValue)) {
			ew_AddMessage($gsFormError, $this->DcrCodi->FldErrMsg());
		}
		if (!$this->VcrCodi->FldIsDetailKey && !is_null($this->VcrCodi->FormValue) && $this->VcrCodi->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->VcrCodi->FldCaption(), $this->VcrCodi->ReqErrMsg));
		}
		if (!$this->DcrFech->FldIsDetailKey && !is_null($this->DcrFech->FormValue) && $this->DcrFech->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->DcrFech->FldCaption(), $this->DcrFech->ReqErrMsg));
		}
		if (!ew_CheckEuroDate($this->DcrFech->FormValue)) {
			ew_AddMessage($gsFormError, $this->DcrFech->FldErrMsg());
		}
		if (!$this->DcrMont->FldIsDetailKey && !is_null($this->DcrMont->FormValue) && $this->DcrMont->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->DcrMont->FldCaption(), $this->DcrMont->ReqErrMsg));
		}
		if (!ew_CheckNumber($this->DcrMont->FormValue)) {
			ew_AddMessage($gsFormError, $this->DcrMont->FldErrMsg());
		}
		if (!$this->DcrSCuo->FldIsDetailKey && !is_null($this->DcrSCuo->FormValue) && $this->DcrSCuo->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->DcrSCuo->FldCaption(), $this->DcrSCuo->ReqErrMsg));
		}
		if (!ew_CheckNumber($this->DcrSCuo->FormValue)) {
			ew_AddMessage($gsFormError, $this->DcrSCuo->FldErrMsg());
		}
		if (!$this->DcrNCuo->FldIsDetailKey && !is_null($this->DcrNCuo->FormValue) && $this->DcrNCuo->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->DcrNCuo->FldCaption(), $this->DcrNCuo->ReqErrMsg));
		}
		if (!ew_CheckInteger($this->DcrNCuo->FormValue)) {
			ew_AddMessage($gsFormError, $this->DcrNCuo->FldErrMsg());
		}
		if (!$this->DcrEsta->FldIsDetailKey && !is_null($this->DcrEsta->FormValue) && $this->DcrEsta->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->DcrEsta->FldCaption(), $this->DcrEsta->ReqErrMsg));
		}
		if (!$this->DcrUsu->FldIsDetailKey && !is_null($this->DcrUsu->FormValue) && $this->DcrUsu->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->DcrUsu->FldCaption(), $this->DcrUsu->ReqErrMsg));
		}

		// Return validate result
		$ValidateForm = ($gsFormError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateForm = $ValidateForm && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsFormError, $sFormCustomError);
		}
		return $ValidateForm;
	}

	//
	// Delete records based on current filter
	//
	function DeleteRows() {
		global $Language, $Security;
		if (!$Security->CanDelete()) {
			$this->setFailureMessage($Language->Phrase("NoDeletePermission")); // No delete permission
			return FALSE;
		}
		$DeleteRows = TRUE;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE) {
			return FALSE;
		} elseif ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
			$rs->Close();
			return FALSE;

		//} else {
		//	$this->LoadRowValues($rs); // Load row values

		}
		$rows = ($rs) ? $rs->GetRows() : array();

		// Clone old rows
		$rsold = $rows;
		if ($rs)
			$rs->Close();

		// Call row deleting event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$DeleteRows = $this->Row_Deleting($row);
				if (!$DeleteRows) break;
			}
		}
		if ($DeleteRows) {
			$sKey = "";
			foreach ($rsold as $row) {
				$sThisKey = "";
				if ($sThisKey <> "") $sThisKey .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
				$sThisKey .= $row['DcrCodi'];
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				$DeleteRows = $this->Delete($row); // Delete
				$conn->raiseErrorFn = '';
				if ($DeleteRows === FALSE)
					break;
				if ($sKey <> "") $sKey .= ", ";
				$sKey .= $sThisKey;
			}
		} else {

			// Set up error message
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("DeleteCancelled"));
			}
		}
		if ($DeleteRows) {
		} else {
		}

		// Call Row Deleted event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$this->Row_Deleted($row);
			}
		}
		return $DeleteRows;
	}

	// Update record based on key values
	function EditRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$conn = &$this->Connection();
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE)
			return FALSE;
		if ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record message
			$EditRow = FALSE; // Update Failed
		} else {

			// Save old values
			$rsold = &$rs->fields;
			$this->LoadDbValues($rsold);
			$rsnew = array();

			// DcrCodi
			// VcrCodi

			$this->VcrCodi->SetDbValueDef($rsnew, $this->VcrCodi->CurrentValue, 0, $this->VcrCodi->ReadOnly);

			// DcrFech
			$this->DcrFech->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->DcrFech->CurrentValue, 7), ew_CurrentDate(), $this->DcrFech->ReadOnly);

			// DcrMont
			$this->DcrMont->SetDbValueDef($rsnew, $this->DcrMont->CurrentValue, 0, $this->DcrMont->ReadOnly);

			// DcrSCuo
			$this->DcrSCuo->SetDbValueDef($rsnew, $this->DcrSCuo->CurrentValue, 0, $this->DcrSCuo->ReadOnly);

			// DcrNCuo
			$this->DcrNCuo->SetDbValueDef($rsnew, $this->DcrNCuo->CurrentValue, 0, $this->DcrNCuo->ReadOnly);

			// DcrEsta
			$this->DcrEsta->SetDbValueDef($rsnew, $this->DcrEsta->CurrentValue, "", $this->DcrEsta->ReadOnly);

			// DcrUsu
			$this->DcrUsu->SetDbValueDef($rsnew, $this->DcrUsu->CurrentValue, "", $this->DcrUsu->ReadOnly);

			// Call Row Updating event
			$bUpdateRow = $this->Row_Updating($rsold, $rsnew);
			if ($bUpdateRow) {
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				if (count($rsnew) > 0)
					$EditRow = $this->Update($rsnew, "", $rsold);
				else
					$EditRow = TRUE; // No field to update
				$conn->raiseErrorFn = '';
				if ($EditRow) {
				}
			} else {
				if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

					// Use the message, do nothing
				} elseif ($this->CancelMessage <> "") {
					$this->setFailureMessage($this->CancelMessage);
					$this->CancelMessage = "";
				} else {
					$this->setFailureMessage($Language->Phrase("UpdateCancelled"));
				}
				$EditRow = FALSE;
			}
		}

		// Call Row_Updated event
		if ($EditRow)
			$this->Row_Updated($rsold, $rsnew);
		$rs->Close();
		return $EditRow;
	}

	// Add record
	function AddRow($rsold = NULL) {
		global $Language, $Security;
		if ($this->DcrCodi->CurrentValue <> "") { // Check field with unique index
			$sFilter = "(DcrCodi = " . ew_AdjustSql($this->DcrCodi->CurrentValue, $this->DBID) . ")";
			$rsChk = $this->LoadRs($sFilter);
			if ($rsChk && !$rsChk->EOF) {
				$sIdxErrMsg = str_replace("%f", $this->DcrCodi->FldCaption(), $Language->Phrase("DupIndex"));
				$sIdxErrMsg = str_replace("%v", $this->DcrCodi->CurrentValue, $sIdxErrMsg);
				$this->setFailureMessage($sIdxErrMsg);
				$rsChk->Close();
				return FALSE;
			}
		}
		$conn = &$this->Connection();

		// Load db values from rsold
		if ($rsold) {
			$this->LoadDbValues($rsold);
		}
		$rsnew = array();

		// DcrCodi
		$this->DcrCodi->SetDbValueDef($rsnew, $this->DcrCodi->CurrentValue, 0, FALSE);

		// VcrCodi
		$this->VcrCodi->SetDbValueDef($rsnew, $this->VcrCodi->CurrentValue, 0, FALSE);

		// DcrFech
		$this->DcrFech->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->DcrFech->CurrentValue, 7), ew_CurrentDate(), FALSE);

		// DcrMont
		$this->DcrMont->SetDbValueDef($rsnew, $this->DcrMont->CurrentValue, 0, FALSE);

		// DcrSCuo
		$this->DcrSCuo->SetDbValueDef($rsnew, $this->DcrSCuo->CurrentValue, 0, FALSE);

		// DcrNCuo
		$this->DcrNCuo->SetDbValueDef($rsnew, $this->DcrNCuo->CurrentValue, 0, FALSE);

		// DcrEsta
		$this->DcrEsta->SetDbValueDef($rsnew, $this->DcrEsta->CurrentValue, "", FALSE);

		// DcrUsu
		$this->DcrUsu->SetDbValueDef($rsnew, $this->DcrUsu->CurrentValue, "", FALSE);

		// Call Row Inserting event
		$rs = ($rsold == NULL) ? NULL : $rsold->fields;
		$bInsertRow = $this->Row_Inserting($rs, $rsnew);

		// Check if key value entered
		if ($bInsertRow && $this->ValidateKey && strval($rsnew['DcrCodi']) == "") {
			$this->setFailureMessage($Language->Phrase("InvalidKeyValue"));
			$bInsertRow = FALSE;
		}

		// Check for duplicate key
		if ($bInsertRow && $this->ValidateKey) {
			$sFilter = $this->KeyFilter();
			$rsChk = $this->LoadRs($sFilter);
			if ($rsChk && !$rsChk->EOF) {
				$sKeyErrMsg = str_replace("%f", $sFilter, $Language->Phrase("DupKey"));
				$this->setFailureMessage($sKeyErrMsg);
				$rsChk->Close();
				$bInsertRow = FALSE;
			}
		}
		if ($bInsertRow) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			$AddRow = $this->Insert($rsnew);
			$conn->raiseErrorFn = '';
			if ($AddRow) {
			}
		} else {
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("InsertCancelled"));
			}
			$AddRow = FALSE;
		}
		if ($AddRow) {

			// Call Row Inserted event
			$rs = ($rsold == NULL) ? NULL : $rsold->fields;
			$this->Row_Inserted($rs, $rsnew);
		}
		return $AddRow;
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$url = preg_replace('/\?cmd=reset(all){0,1}$/i', '', $url); // Remove cmd=reset / cmd=resetall
		$Breadcrumb->Add("list", $this->TableVar, $url, "", $this->TableVar, TRUE);
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}

	// ListOptions Load event
	function ListOptions_Load() {

		// Example:
		//$opt = &$this->ListOptions->Add("new");
		//$opt->Header = "xxx";
		//$opt->OnLeft = TRUE; // Link on left
		//$opt->MoveTo(0); // Move to first column

	}

	// ListOptions Rendered event
	function ListOptions_Rendered() {

		// Example: 
		//$this->ListOptions->Items["new"]->Body = "xxx";

	}

	// Row Custom Action event
	function Row_CustomAction($action, $row) {

		// Return FALSE to abort
		return TRUE;
	}

	// Page Exporting event
	// $this->ExportDoc = export document object
	function Page_Exporting() {

		//$this->ExportDoc->Text = "my header"; // Export header
		//return FALSE; // Return FALSE to skip default export and use Row_Export event

		return TRUE; // Return TRUE to use default export and skip Row_Export event
	}

	// Row Export event
	// $this->ExportDoc = export document object
	function Row_Export($rs) {

	    //$this->ExportDoc->Text .= "my content"; // Build HTML with field value: $rs["MyField"] or $this->MyField->ViewValue
	}

	// Page Exported event
	// $this->ExportDoc = export document object
	function Page_Exported() {

		//$this->ExportDoc->Text .= "my footer"; // Export footer
		//echo $this->ExportDoc->Text;

	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($DCre_list)) $DCre_list = new cDCre_list();

// Page init
$DCre_list->Page_Init();

// Page main
$DCre_list->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$DCre_list->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "list";
var CurrentForm = fDCrelist = new ew_Form("fDCrelist", "list");
fDCrelist.FormKeyCountName = '<?php echo $DCre_list->FormKeyCountName ?>';

// Validate form
fDCrelist.Validate = function() {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.GetForm(), $fobj = $(fobj);
	if ($fobj.find("#a_confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.FormKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = $fobj.find("#a_list").val() == "gridinsert";
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
		var checkrow = (gridinsert) ? !this.EmptyRow(infix) : true;
		if (checkrow) {
			addcnt++;
			elm = this.GetElements("x" + infix + "_DcrCodi");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $DCre->DcrCodi->FldCaption(), $DCre->DcrCodi->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_DcrCodi");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($DCre->DcrCodi->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_VcrCodi");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $DCre->VcrCodi->FldCaption(), $DCre->VcrCodi->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_DcrFech");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $DCre->DcrFech->FldCaption(), $DCre->DcrFech->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_DcrFech");
			if (elm && !ew_CheckEuroDate(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($DCre->DcrFech->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_DcrMont");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $DCre->DcrMont->FldCaption(), $DCre->DcrMont->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_DcrMont");
			if (elm && !ew_CheckNumber(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($DCre->DcrMont->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_DcrSCuo");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $DCre->DcrSCuo->FldCaption(), $DCre->DcrSCuo->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_DcrSCuo");
			if (elm && !ew_CheckNumber(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($DCre->DcrSCuo->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_DcrNCuo");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $DCre->DcrNCuo->FldCaption(), $DCre->DcrNCuo->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_DcrNCuo");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($DCre->DcrNCuo->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_DcrEsta");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $DCre->DcrEsta->FldCaption(), $DCre->DcrEsta->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_DcrUsu");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $DCre->DcrUsu->FldCaption(), $DCre->DcrUsu->ReqErrMsg)) ?>");

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
		} // End Grid Add checking
	}
	if (gridinsert && addcnt == 0) { // No row added
		ew_Alert(ewLanguage.Phrase("NoAddRecord"));
		return false;
	}
	return true;
}

// Check empty row
fDCrelist.EmptyRow = function(infix) {
	var fobj = this.Form;
	if (ew_ValueChanged(fobj, infix, "DcrCodi", false)) return false;
	if (ew_ValueChanged(fobj, infix, "VcrCodi", false)) return false;
	if (ew_ValueChanged(fobj, infix, "DcrFech", false)) return false;
	if (ew_ValueChanged(fobj, infix, "DcrMont", false)) return false;
	if (ew_ValueChanged(fobj, infix, "DcrSCuo", false)) return false;
	if (ew_ValueChanged(fobj, infix, "DcrNCuo", false)) return false;
	if (ew_ValueChanged(fobj, infix, "DcrEsta", false)) return false;
	if (ew_ValueChanged(fobj, infix, "DcrUsu", false)) return false;
	return true;
}

// Form_CustomValidate event
fDCrelist.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fDCrelist.ValidateRequired = true;
<?php } else { ?>
fDCrelist.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
fDCrelist.Lists["x_VcrCodi"] = {"LinkField":"x_VcrCodi","Ajax":true,"AutoFill":false,"DisplayFields":["x_VcrCodi","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};

// Form object for search
var CurrentSearchForm = fDCrelistsrch = new ew_Form("fDCrelistsrch");
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php if ($DCre_list->TotalRecs > 0 && $DCre_list->ExportOptions->Visible()) { ?>
<?php $DCre_list->ExportOptions->Render("body") ?>
<?php } ?>
<?php if ($DCre_list->SearchOptions->Visible()) { ?>
<?php $DCre_list->SearchOptions->Render("body") ?>
<?php } ?>
<?php if ($DCre_list->FilterOptions->Visible()) { ?>
<?php $DCre_list->FilterOptions->Render("body") ?>
<?php } ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php
if ($DCre->CurrentAction == "gridadd") {
	$DCre->CurrentFilter = "0=1";
	$DCre_list->StartRec = 1;
	$DCre_list->DisplayRecs = $DCre->GridAddRowCount;
	$DCre_list->TotalRecs = $DCre_list->DisplayRecs;
	$DCre_list->StopRec = $DCre_list->DisplayRecs;
} else {
	$bSelectLimit = $DCre_list->UseSelectLimit;
	if ($bSelectLimit) {
		if ($DCre_list->TotalRecs <= 0)
			$DCre_list->TotalRecs = $DCre->SelectRecordCount();
	} else {
		if (!$DCre_list->Recordset && ($DCre_list->Recordset = $DCre_list->LoadRecordset()))
			$DCre_list->TotalRecs = $DCre_list->Recordset->RecordCount();
	}
	$DCre_list->StartRec = 1;
	if ($DCre_list->DisplayRecs <= 0 || ($DCre->Export <> "" && $DCre->ExportAll)) // Display all records
		$DCre_list->DisplayRecs = $DCre_list->TotalRecs;
	if (!($DCre->Export <> "" && $DCre->ExportAll))
		$DCre_list->SetUpStartRec(); // Set up start record position
	if ($bSelectLimit)
		$DCre_list->Recordset = $DCre_list->LoadRecordset($DCre_list->StartRec-1, $DCre_list->DisplayRecs);

	// Set no record found message
	if ($DCre->CurrentAction == "" && $DCre_list->TotalRecs == 0) {
		if (!$Security->CanList())
			$DCre_list->setWarningMessage($Language->Phrase("NoPermission"));
		if ($DCre_list->SearchWhere == "0=101")
			$DCre_list->setWarningMessage($Language->Phrase("EnterSearchCriteria"));
		else
			$DCre_list->setWarningMessage($Language->Phrase("NoRecord"));
	}
}
$DCre_list->RenderOtherOptions();
?>
<?php if ($Security->CanSearch()) { ?>
<?php if ($DCre->Export == "" && $DCre->CurrentAction == "") { ?>
<form name="fDCrelistsrch" id="fDCrelistsrch" class="form-inline ewForm" action="<?php echo ew_CurrentPage() ?>">
<?php $SearchPanelClass = ($DCre_list->SearchWhere <> "") ? " in" : " in"; ?>
<div id="fDCrelistsrch_SearchPanel" class="ewSearchPanel collapse<?php echo $SearchPanelClass ?>">
<input type="hidden" name="cmd" value="search">
<input type="hidden" name="t" value="DCre">
	<div class="ewBasicSearch">
<div id="xsr_1" class="ewRow">
	<div class="ewQuickSearch input-group">
	<input type="text" name="<?php echo EW_TABLE_BASIC_SEARCH ?>" id="<?php echo EW_TABLE_BASIC_SEARCH ?>" class="form-control" value="<?php echo ew_HtmlEncode($DCre_list->BasicSearch->getKeyword()) ?>" placeholder="<?php echo ew_HtmlEncode($Language->Phrase("Search")) ?>">
	<input type="hidden" name="<?php echo EW_TABLE_BASIC_SEARCH_TYPE ?>" id="<?php echo EW_TABLE_BASIC_SEARCH_TYPE ?>" value="<?php echo ew_HtmlEncode($DCre_list->BasicSearch->getType()) ?>">
	<div class="input-group-btn">
		<button type="button" data-toggle="dropdown" class="btn btn-default"><span id="searchtype"><?php echo $DCre_list->BasicSearch->getTypeNameShort() ?></span><span class="caret"></span></button>
		<ul class="dropdown-menu pull-right" role="menu">
			<li<?php if ($DCre_list->BasicSearch->getType() == "") echo " class=\"active\""; ?>><a href="javascript:void(0);" onclick="ew_SetSearchType(this)"><?php echo $Language->Phrase("QuickSearchAuto") ?></a></li>
			<li<?php if ($DCre_list->BasicSearch->getType() == "=") echo " class=\"active\""; ?>><a href="javascript:void(0);" onclick="ew_SetSearchType(this,'=')"><?php echo $Language->Phrase("QuickSearchExact") ?></a></li>
			<li<?php if ($DCre_list->BasicSearch->getType() == "AND") echo " class=\"active\""; ?>><a href="javascript:void(0);" onclick="ew_SetSearchType(this,'AND')"><?php echo $Language->Phrase("QuickSearchAll") ?></a></li>
			<li<?php if ($DCre_list->BasicSearch->getType() == "OR") echo " class=\"active\""; ?>><a href="javascript:void(0);" onclick="ew_SetSearchType(this,'OR')"><?php echo $Language->Phrase("QuickSearchAny") ?></a></li>
		</ul>
	<button class="btn btn-primary ewButton" name="btnsubmit" id="btnsubmit" type="submit"><?php echo $Language->Phrase("QuickSearchBtn") ?></button>
	</div>
	</div>
</div>
	</div>
</div>
</form>
<?php } ?>
<?php } ?>
<?php $DCre_list->ShowPageHeader(); ?>
<?php
$DCre_list->ShowMessage();
?>
<?php if ($DCre_list->TotalRecs > 0 || $DCre->CurrentAction <> "") { ?>
<div class="panel panel-default ewGrid">
<form name="fDCrelist" id="fDCrelist" class="form-inline ewForm ewListForm" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($DCre_list->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $DCre_list->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="DCre">
<div id="gmp_DCre" class="<?php if (ew_IsResponsiveLayout()) { echo "table-responsive "; } ?>ewGridMiddlePanel">
<?php if ($DCre_list->TotalRecs > 0) { ?>
<table id="tbl_DCrelist" class="table ewTable">
<?php echo $DCre->TableCustomInnerHtml ?>
<thead><!-- Table header -->
	<tr class="ewTableHeader">
<?php

// Header row
$DCre_list->RowType = EW_ROWTYPE_HEADER;

// Render list options
$DCre_list->RenderListOptions();

// Render list options (header, left)
$DCre_list->ListOptions->Render("header", "left");
?>
<?php if ($DCre->DcrCodi->Visible) { // DcrCodi ?>
	<?php if ($DCre->SortUrl($DCre->DcrCodi) == "") { ?>
		<th data-name="DcrCodi"><div id="elh_DCre_DcrCodi" class="DCre_DcrCodi"><div class="ewTableHeaderCaption"><?php echo $DCre->DcrCodi->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="DcrCodi"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $DCre->SortUrl($DCre->DcrCodi) ?>',2);"><div id="elh_DCre_DcrCodi" class="DCre_DcrCodi">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $DCre->DcrCodi->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($DCre->DcrCodi->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($DCre->DcrCodi->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($DCre->VcrCodi->Visible) { // VcrCodi ?>
	<?php if ($DCre->SortUrl($DCre->VcrCodi) == "") { ?>
		<th data-name="VcrCodi"><div id="elh_DCre_VcrCodi" class="DCre_VcrCodi"><div class="ewTableHeaderCaption"><?php echo $DCre->VcrCodi->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="VcrCodi"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $DCre->SortUrl($DCre->VcrCodi) ?>',2);"><div id="elh_DCre_VcrCodi" class="DCre_VcrCodi">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $DCre->VcrCodi->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($DCre->VcrCodi->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($DCre->VcrCodi->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($DCre->DcrFech->Visible) { // DcrFech ?>
	<?php if ($DCre->SortUrl($DCre->DcrFech) == "") { ?>
		<th data-name="DcrFech"><div id="elh_DCre_DcrFech" class="DCre_DcrFech"><div class="ewTableHeaderCaption"><?php echo $DCre->DcrFech->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="DcrFech"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $DCre->SortUrl($DCre->DcrFech) ?>',2);"><div id="elh_DCre_DcrFech" class="DCre_DcrFech">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $DCre->DcrFech->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($DCre->DcrFech->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($DCre->DcrFech->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($DCre->DcrMont->Visible) { // DcrMont ?>
	<?php if ($DCre->SortUrl($DCre->DcrMont) == "") { ?>
		<th data-name="DcrMont"><div id="elh_DCre_DcrMont" class="DCre_DcrMont"><div class="ewTableHeaderCaption"><?php echo $DCre->DcrMont->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="DcrMont"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $DCre->SortUrl($DCre->DcrMont) ?>',2);"><div id="elh_DCre_DcrMont" class="DCre_DcrMont">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $DCre->DcrMont->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($DCre->DcrMont->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($DCre->DcrMont->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($DCre->DcrSCuo->Visible) { // DcrSCuo ?>
	<?php if ($DCre->SortUrl($DCre->DcrSCuo) == "") { ?>
		<th data-name="DcrSCuo"><div id="elh_DCre_DcrSCuo" class="DCre_DcrSCuo"><div class="ewTableHeaderCaption"><?php echo $DCre->DcrSCuo->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="DcrSCuo"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $DCre->SortUrl($DCre->DcrSCuo) ?>',2);"><div id="elh_DCre_DcrSCuo" class="DCre_DcrSCuo">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $DCre->DcrSCuo->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($DCre->DcrSCuo->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($DCre->DcrSCuo->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($DCre->DcrNCuo->Visible) { // DcrNCuo ?>
	<?php if ($DCre->SortUrl($DCre->DcrNCuo) == "") { ?>
		<th data-name="DcrNCuo"><div id="elh_DCre_DcrNCuo" class="DCre_DcrNCuo"><div class="ewTableHeaderCaption"><?php echo $DCre->DcrNCuo->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="DcrNCuo"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $DCre->SortUrl($DCre->DcrNCuo) ?>',2);"><div id="elh_DCre_DcrNCuo" class="DCre_DcrNCuo">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $DCre->DcrNCuo->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($DCre->DcrNCuo->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($DCre->DcrNCuo->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($DCre->DcrEsta->Visible) { // DcrEsta ?>
	<?php if ($DCre->SortUrl($DCre->DcrEsta) == "") { ?>
		<th data-name="DcrEsta"><div id="elh_DCre_DcrEsta" class="DCre_DcrEsta"><div class="ewTableHeaderCaption"><?php echo $DCre->DcrEsta->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="DcrEsta"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $DCre->SortUrl($DCre->DcrEsta) ?>',2);"><div id="elh_DCre_DcrEsta" class="DCre_DcrEsta">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $DCre->DcrEsta->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($DCre->DcrEsta->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($DCre->DcrEsta->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($DCre->DcrUsu->Visible) { // DcrUsu ?>
	<?php if ($DCre->SortUrl($DCre->DcrUsu) == "") { ?>
		<th data-name="DcrUsu"><div id="elh_DCre_DcrUsu" class="DCre_DcrUsu"><div class="ewTableHeaderCaption"><?php echo $DCre->DcrUsu->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="DcrUsu"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $DCre->SortUrl($DCre->DcrUsu) ?>',2);"><div id="elh_DCre_DcrUsu" class="DCre_DcrUsu">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $DCre->DcrUsu->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($DCre->DcrUsu->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($DCre->DcrUsu->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php

// Render list options (header, right)
$DCre_list->ListOptions->Render("header", "right");
?>
	</tr>
</thead>
<tbody>
<?php
if ($DCre->ExportAll && $DCre->Export <> "") {
	$DCre_list->StopRec = $DCre_list->TotalRecs;
} else {

	// Set the last record to display
	if ($DCre_list->TotalRecs > $DCre_list->StartRec + $DCre_list->DisplayRecs - 1)
		$DCre_list->StopRec = $DCre_list->StartRec + $DCre_list->DisplayRecs - 1;
	else
		$DCre_list->StopRec = $DCre_list->TotalRecs;
}

// Restore number of post back records
if ($objForm) {
	$objForm->Index = -1;
	if ($objForm->HasValue($DCre_list->FormKeyCountName) && ($DCre->CurrentAction == "gridadd" || $DCre->CurrentAction == "gridedit" || $DCre->CurrentAction == "F")) {
		$DCre_list->KeyCount = $objForm->GetValue($DCre_list->FormKeyCountName);
		$DCre_list->StopRec = $DCre_list->StartRec + $DCre_list->KeyCount - 1;
	}
}
$DCre_list->RecCnt = $DCre_list->StartRec - 1;
if ($DCre_list->Recordset && !$DCre_list->Recordset->EOF) {
	$DCre_list->Recordset->MoveFirst();
	$bSelectLimit = $DCre_list->UseSelectLimit;
	if (!$bSelectLimit && $DCre_list->StartRec > 1)
		$DCre_list->Recordset->Move($DCre_list->StartRec - 1);
} elseif (!$DCre->AllowAddDeleteRow && $DCre_list->StopRec == 0) {
	$DCre_list->StopRec = $DCre->GridAddRowCount;
}

// Initialize aggregate
$DCre->RowType = EW_ROWTYPE_AGGREGATEINIT;
$DCre->ResetAttrs();
$DCre_list->RenderRow();
if ($DCre->CurrentAction == "gridadd")
	$DCre_list->RowIndex = 0;
if ($DCre->CurrentAction == "gridedit")
	$DCre_list->RowIndex = 0;
while ($DCre_list->RecCnt < $DCre_list->StopRec) {
	$DCre_list->RecCnt++;
	if (intval($DCre_list->RecCnt) >= intval($DCre_list->StartRec)) {
		$DCre_list->RowCnt++;
		if ($DCre->CurrentAction == "gridadd" || $DCre->CurrentAction == "gridedit" || $DCre->CurrentAction == "F") {
			$DCre_list->RowIndex++;
			$objForm->Index = $DCre_list->RowIndex;
			if ($objForm->HasValue($DCre_list->FormActionName))
				$DCre_list->RowAction = strval($objForm->GetValue($DCre_list->FormActionName));
			elseif ($DCre->CurrentAction == "gridadd")
				$DCre_list->RowAction = "insert";
			else
				$DCre_list->RowAction = "";
		}

		// Set up key count
		$DCre_list->KeyCount = $DCre_list->RowIndex;

		// Init row class and style
		$DCre->ResetAttrs();
		$DCre->CssClass = "";
		if ($DCre->CurrentAction == "gridadd") {
			$DCre_list->LoadDefaultValues(); // Load default values
		} else {
			$DCre_list->LoadRowValues($DCre_list->Recordset); // Load row values
		}
		$DCre->RowType = EW_ROWTYPE_VIEW; // Render view
		if ($DCre->CurrentAction == "gridadd") // Grid add
			$DCre->RowType = EW_ROWTYPE_ADD; // Render add
		if ($DCre->CurrentAction == "gridadd" && $DCre->EventCancelled && !$objForm->HasValue("k_blankrow")) // Insert failed
			$DCre_list->RestoreCurrentRowFormValues($DCre_list->RowIndex); // Restore form values
		if ($DCre->CurrentAction == "gridedit") { // Grid edit
			if ($DCre->EventCancelled) {
				$DCre_list->RestoreCurrentRowFormValues($DCre_list->RowIndex); // Restore form values
			}
			if ($DCre_list->RowAction == "insert")
				$DCre->RowType = EW_ROWTYPE_ADD; // Render add
			else
				$DCre->RowType = EW_ROWTYPE_EDIT; // Render edit
		}
		if ($DCre->CurrentAction == "gridedit" && ($DCre->RowType == EW_ROWTYPE_EDIT || $DCre->RowType == EW_ROWTYPE_ADD) && $DCre->EventCancelled) // Update failed
			$DCre_list->RestoreCurrentRowFormValues($DCre_list->RowIndex); // Restore form values
		if ($DCre->RowType == EW_ROWTYPE_EDIT) // Edit row
			$DCre_list->EditRowCnt++;

		// Set up row id / data-rowindex
		$DCre->RowAttrs = array_merge($DCre->RowAttrs, array('data-rowindex'=>$DCre_list->RowCnt, 'id'=>'r' . $DCre_list->RowCnt . '_DCre', 'data-rowtype'=>$DCre->RowType));

		// Render row
		$DCre_list->RenderRow();

		// Render list options
		$DCre_list->RenderListOptions();

		// Skip delete row / empty row for confirm page
		if ($DCre_list->RowAction <> "delete" && $DCre_list->RowAction <> "insertdelete" && !($DCre_list->RowAction == "insert" && $DCre->CurrentAction == "F" && $DCre_list->EmptyRow())) {
?>
	<tr<?php echo $DCre->RowAttributes() ?>>
<?php

// Render list options (body, left)
$DCre_list->ListOptions->Render("body", "left", $DCre_list->RowCnt);
?>
	<?php if ($DCre->DcrCodi->Visible) { // DcrCodi ?>
		<td data-name="DcrCodi"<?php echo $DCre->DcrCodi->CellAttributes() ?>>
<?php if ($DCre->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $DCre_list->RowCnt ?>_DCre_DcrCodi" class="form-group DCre_DcrCodi">
<input type="text" data-table="DCre" data-field="x_DcrCodi" name="x<?php echo $DCre_list->RowIndex ?>_DcrCodi" id="x<?php echo $DCre_list->RowIndex ?>_DcrCodi" size="30" placeholder="<?php echo ew_HtmlEncode($DCre->DcrCodi->getPlaceHolder()) ?>" value="<?php echo $DCre->DcrCodi->EditValue ?>"<?php echo $DCre->DcrCodi->EditAttributes() ?>>
</span>
<input type="hidden" data-table="DCre" data-field="x_DcrCodi" name="o<?php echo $DCre_list->RowIndex ?>_DcrCodi" id="o<?php echo $DCre_list->RowIndex ?>_DcrCodi" value="<?php echo ew_HtmlEncode($DCre->DcrCodi->OldValue) ?>">
<?php } ?>
<?php if ($DCre->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $DCre_list->RowCnt ?>_DCre_DcrCodi" class="form-group DCre_DcrCodi">
<span<?php echo $DCre->DcrCodi->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $DCre->DcrCodi->EditValue ?></p></span>
</span>
<input type="hidden" data-table="DCre" data-field="x_DcrCodi" name="x<?php echo $DCre_list->RowIndex ?>_DcrCodi" id="x<?php echo $DCre_list->RowIndex ?>_DcrCodi" value="<?php echo ew_HtmlEncode($DCre->DcrCodi->CurrentValue) ?>">
<?php } ?>
<?php if ($DCre->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $DCre_list->RowCnt ?>_DCre_DcrCodi" class="DCre_DcrCodi">
<span<?php echo $DCre->DcrCodi->ViewAttributes() ?>>
<?php echo $DCre->DcrCodi->ListViewValue() ?></span>
</span>
<?php } ?>
<a id="<?php echo $DCre_list->PageObjName . "_row_" . $DCre_list->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($DCre->VcrCodi->Visible) { // VcrCodi ?>
		<td data-name="VcrCodi"<?php echo $DCre->VcrCodi->CellAttributes() ?>>
<?php if ($DCre->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $DCre_list->RowCnt ?>_DCre_VcrCodi" class="form-group DCre_VcrCodi">
<select data-table="DCre" data-field="x_VcrCodi" data-value-separator="<?php echo ew_HtmlEncode(is_array($DCre->VcrCodi->DisplayValueSeparator) ? json_encode($DCre->VcrCodi->DisplayValueSeparator) : $DCre->VcrCodi->DisplayValueSeparator) ?>" id="x<?php echo $DCre_list->RowIndex ?>_VcrCodi" name="x<?php echo $DCre_list->RowIndex ?>_VcrCodi"<?php echo $DCre->VcrCodi->EditAttributes() ?>>
<?php
if (is_array($DCre->VcrCodi->EditValue)) {
	$arwrk = $DCre->VcrCodi->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($DCre->VcrCodi->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $DCre->VcrCodi->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($DCre->VcrCodi->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($DCre->VcrCodi->CurrentValue) ?>" selected><?php echo $DCre->VcrCodi->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $DCre->VcrCodi->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT \"VcrCodi\", \"VcrCodi\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"VCre\"";
$sWhereWrk = "";
$DCre->VcrCodi->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$DCre->VcrCodi->LookupFilters += array("f0" => "\"VcrCodi\" = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$DCre->Lookup_Selecting($DCre->VcrCodi, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $DCre->VcrCodi->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $DCre_list->RowIndex ?>_VcrCodi" id="s_x<?php echo $DCre_list->RowIndex ?>_VcrCodi" value="<?php echo $DCre->VcrCodi->LookupFilterQuery() ?>">
</span>
<input type="hidden" data-table="DCre" data-field="x_VcrCodi" name="o<?php echo $DCre_list->RowIndex ?>_VcrCodi" id="o<?php echo $DCre_list->RowIndex ?>_VcrCodi" value="<?php echo ew_HtmlEncode($DCre->VcrCodi->OldValue) ?>">
<?php } ?>
<?php if ($DCre->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $DCre_list->RowCnt ?>_DCre_VcrCodi" class="form-group DCre_VcrCodi">
<select data-table="DCre" data-field="x_VcrCodi" data-value-separator="<?php echo ew_HtmlEncode(is_array($DCre->VcrCodi->DisplayValueSeparator) ? json_encode($DCre->VcrCodi->DisplayValueSeparator) : $DCre->VcrCodi->DisplayValueSeparator) ?>" id="x<?php echo $DCre_list->RowIndex ?>_VcrCodi" name="x<?php echo $DCre_list->RowIndex ?>_VcrCodi"<?php echo $DCre->VcrCodi->EditAttributes() ?>>
<?php
if (is_array($DCre->VcrCodi->EditValue)) {
	$arwrk = $DCre->VcrCodi->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($DCre->VcrCodi->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $DCre->VcrCodi->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($DCre->VcrCodi->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($DCre->VcrCodi->CurrentValue) ?>" selected><?php echo $DCre->VcrCodi->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $DCre->VcrCodi->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT \"VcrCodi\", \"VcrCodi\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"VCre\"";
$sWhereWrk = "";
$DCre->VcrCodi->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$DCre->VcrCodi->LookupFilters += array("f0" => "\"VcrCodi\" = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$DCre->Lookup_Selecting($DCre->VcrCodi, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $DCre->VcrCodi->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $DCre_list->RowIndex ?>_VcrCodi" id="s_x<?php echo $DCre_list->RowIndex ?>_VcrCodi" value="<?php echo $DCre->VcrCodi->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php if ($DCre->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $DCre_list->RowCnt ?>_DCre_VcrCodi" class="DCre_VcrCodi">
<span<?php echo $DCre->VcrCodi->ViewAttributes() ?>>
<?php echo $DCre->VcrCodi->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($DCre->DcrFech->Visible) { // DcrFech ?>
		<td data-name="DcrFech"<?php echo $DCre->DcrFech->CellAttributes() ?>>
<?php if ($DCre->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $DCre_list->RowCnt ?>_DCre_DcrFech" class="form-group DCre_DcrFech">
<input type="text" data-table="DCre" data-field="x_DcrFech" data-format="7" name="x<?php echo $DCre_list->RowIndex ?>_DcrFech" id="x<?php echo $DCre_list->RowIndex ?>_DcrFech" placeholder="<?php echo ew_HtmlEncode($DCre->DcrFech->getPlaceHolder()) ?>" value="<?php echo $DCre->DcrFech->EditValue ?>"<?php echo $DCre->DcrFech->EditAttributes() ?>>
</span>
<input type="hidden" data-table="DCre" data-field="x_DcrFech" name="o<?php echo $DCre_list->RowIndex ?>_DcrFech" id="o<?php echo $DCre_list->RowIndex ?>_DcrFech" value="<?php echo ew_HtmlEncode($DCre->DcrFech->OldValue) ?>">
<?php } ?>
<?php if ($DCre->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $DCre_list->RowCnt ?>_DCre_DcrFech" class="form-group DCre_DcrFech">
<input type="text" data-table="DCre" data-field="x_DcrFech" data-format="7" name="x<?php echo $DCre_list->RowIndex ?>_DcrFech" id="x<?php echo $DCre_list->RowIndex ?>_DcrFech" placeholder="<?php echo ew_HtmlEncode($DCre->DcrFech->getPlaceHolder()) ?>" value="<?php echo $DCre->DcrFech->EditValue ?>"<?php echo $DCre->DcrFech->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($DCre->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $DCre_list->RowCnt ?>_DCre_DcrFech" class="DCre_DcrFech">
<span<?php echo $DCre->DcrFech->ViewAttributes() ?>>
<?php echo $DCre->DcrFech->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($DCre->DcrMont->Visible) { // DcrMont ?>
		<td data-name="DcrMont"<?php echo $DCre->DcrMont->CellAttributes() ?>>
<?php if ($DCre->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $DCre_list->RowCnt ?>_DCre_DcrMont" class="form-group DCre_DcrMont">
<input type="text" data-table="DCre" data-field="x_DcrMont" name="x<?php echo $DCre_list->RowIndex ?>_DcrMont" id="x<?php echo $DCre_list->RowIndex ?>_DcrMont" size="30" placeholder="<?php echo ew_HtmlEncode($DCre->DcrMont->getPlaceHolder()) ?>" value="<?php echo $DCre->DcrMont->EditValue ?>"<?php echo $DCre->DcrMont->EditAttributes() ?>>
</span>
<input type="hidden" data-table="DCre" data-field="x_DcrMont" name="o<?php echo $DCre_list->RowIndex ?>_DcrMont" id="o<?php echo $DCre_list->RowIndex ?>_DcrMont" value="<?php echo ew_HtmlEncode($DCre->DcrMont->OldValue) ?>">
<?php } ?>
<?php if ($DCre->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $DCre_list->RowCnt ?>_DCre_DcrMont" class="form-group DCre_DcrMont">
<input type="text" data-table="DCre" data-field="x_DcrMont" name="x<?php echo $DCre_list->RowIndex ?>_DcrMont" id="x<?php echo $DCre_list->RowIndex ?>_DcrMont" size="30" placeholder="<?php echo ew_HtmlEncode($DCre->DcrMont->getPlaceHolder()) ?>" value="<?php echo $DCre->DcrMont->EditValue ?>"<?php echo $DCre->DcrMont->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($DCre->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $DCre_list->RowCnt ?>_DCre_DcrMont" class="DCre_DcrMont">
<span<?php echo $DCre->DcrMont->ViewAttributes() ?>>
<?php echo $DCre->DcrMont->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($DCre->DcrSCuo->Visible) { // DcrSCuo ?>
		<td data-name="DcrSCuo"<?php echo $DCre->DcrSCuo->CellAttributes() ?>>
<?php if ($DCre->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $DCre_list->RowCnt ?>_DCre_DcrSCuo" class="form-group DCre_DcrSCuo">
<input type="text" data-table="DCre" data-field="x_DcrSCuo" name="x<?php echo $DCre_list->RowIndex ?>_DcrSCuo" id="x<?php echo $DCre_list->RowIndex ?>_DcrSCuo" size="30" placeholder="<?php echo ew_HtmlEncode($DCre->DcrSCuo->getPlaceHolder()) ?>" value="<?php echo $DCre->DcrSCuo->EditValue ?>"<?php echo $DCre->DcrSCuo->EditAttributes() ?>>
</span>
<input type="hidden" data-table="DCre" data-field="x_DcrSCuo" name="o<?php echo $DCre_list->RowIndex ?>_DcrSCuo" id="o<?php echo $DCre_list->RowIndex ?>_DcrSCuo" value="<?php echo ew_HtmlEncode($DCre->DcrSCuo->OldValue) ?>">
<?php } ?>
<?php if ($DCre->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $DCre_list->RowCnt ?>_DCre_DcrSCuo" class="form-group DCre_DcrSCuo">
<input type="text" data-table="DCre" data-field="x_DcrSCuo" name="x<?php echo $DCre_list->RowIndex ?>_DcrSCuo" id="x<?php echo $DCre_list->RowIndex ?>_DcrSCuo" size="30" placeholder="<?php echo ew_HtmlEncode($DCre->DcrSCuo->getPlaceHolder()) ?>" value="<?php echo $DCre->DcrSCuo->EditValue ?>"<?php echo $DCre->DcrSCuo->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($DCre->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $DCre_list->RowCnt ?>_DCre_DcrSCuo" class="DCre_DcrSCuo">
<span<?php echo $DCre->DcrSCuo->ViewAttributes() ?>>
<?php echo $DCre->DcrSCuo->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($DCre->DcrNCuo->Visible) { // DcrNCuo ?>
		<td data-name="DcrNCuo"<?php echo $DCre->DcrNCuo->CellAttributes() ?>>
<?php if ($DCre->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $DCre_list->RowCnt ?>_DCre_DcrNCuo" class="form-group DCre_DcrNCuo">
<input type="text" data-table="DCre" data-field="x_DcrNCuo" name="x<?php echo $DCre_list->RowIndex ?>_DcrNCuo" id="x<?php echo $DCre_list->RowIndex ?>_DcrNCuo" size="30" placeholder="<?php echo ew_HtmlEncode($DCre->DcrNCuo->getPlaceHolder()) ?>" value="<?php echo $DCre->DcrNCuo->EditValue ?>"<?php echo $DCre->DcrNCuo->EditAttributes() ?>>
</span>
<input type="hidden" data-table="DCre" data-field="x_DcrNCuo" name="o<?php echo $DCre_list->RowIndex ?>_DcrNCuo" id="o<?php echo $DCre_list->RowIndex ?>_DcrNCuo" value="<?php echo ew_HtmlEncode($DCre->DcrNCuo->OldValue) ?>">
<?php } ?>
<?php if ($DCre->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $DCre_list->RowCnt ?>_DCre_DcrNCuo" class="form-group DCre_DcrNCuo">
<input type="text" data-table="DCre" data-field="x_DcrNCuo" name="x<?php echo $DCre_list->RowIndex ?>_DcrNCuo" id="x<?php echo $DCre_list->RowIndex ?>_DcrNCuo" size="30" placeholder="<?php echo ew_HtmlEncode($DCre->DcrNCuo->getPlaceHolder()) ?>" value="<?php echo $DCre->DcrNCuo->EditValue ?>"<?php echo $DCre->DcrNCuo->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($DCre->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $DCre_list->RowCnt ?>_DCre_DcrNCuo" class="DCre_DcrNCuo">
<span<?php echo $DCre->DcrNCuo->ViewAttributes() ?>>
<?php echo $DCre->DcrNCuo->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($DCre->DcrEsta->Visible) { // DcrEsta ?>
		<td data-name="DcrEsta"<?php echo $DCre->DcrEsta->CellAttributes() ?>>
<?php if ($DCre->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $DCre_list->RowCnt ?>_DCre_DcrEsta" class="form-group DCre_DcrEsta">
<input type="text" data-table="DCre" data-field="x_DcrEsta" name="x<?php echo $DCre_list->RowIndex ?>_DcrEsta" id="x<?php echo $DCre_list->RowIndex ?>_DcrEsta" size="30" maxlength="1" placeholder="<?php echo ew_HtmlEncode($DCre->DcrEsta->getPlaceHolder()) ?>" value="<?php echo $DCre->DcrEsta->EditValue ?>"<?php echo $DCre->DcrEsta->EditAttributes() ?>>
</span>
<input type="hidden" data-table="DCre" data-field="x_DcrEsta" name="o<?php echo $DCre_list->RowIndex ?>_DcrEsta" id="o<?php echo $DCre_list->RowIndex ?>_DcrEsta" value="<?php echo ew_HtmlEncode($DCre->DcrEsta->OldValue) ?>">
<?php } ?>
<?php if ($DCre->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $DCre_list->RowCnt ?>_DCre_DcrEsta" class="form-group DCre_DcrEsta">
<input type="text" data-table="DCre" data-field="x_DcrEsta" name="x<?php echo $DCre_list->RowIndex ?>_DcrEsta" id="x<?php echo $DCre_list->RowIndex ?>_DcrEsta" size="30" maxlength="1" placeholder="<?php echo ew_HtmlEncode($DCre->DcrEsta->getPlaceHolder()) ?>" value="<?php echo $DCre->DcrEsta->EditValue ?>"<?php echo $DCre->DcrEsta->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($DCre->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $DCre_list->RowCnt ?>_DCre_DcrEsta" class="DCre_DcrEsta">
<span<?php echo $DCre->DcrEsta->ViewAttributes() ?>>
<?php echo $DCre->DcrEsta->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($DCre->DcrUsu->Visible) { // DcrUsu ?>
		<td data-name="DcrUsu"<?php echo $DCre->DcrUsu->CellAttributes() ?>>
<?php if ($DCre->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $DCre_list->RowCnt ?>_DCre_DcrUsu" class="form-group DCre_DcrUsu">
<input type="text" data-table="DCre" data-field="x_DcrUsu" name="x<?php echo $DCre_list->RowIndex ?>_DcrUsu" id="x<?php echo $DCre_list->RowIndex ?>_DcrUsu" size="30" placeholder="<?php echo ew_HtmlEncode($DCre->DcrUsu->getPlaceHolder()) ?>" value="<?php echo $DCre->DcrUsu->EditValue ?>"<?php echo $DCre->DcrUsu->EditAttributes() ?>>
</span>
<input type="hidden" data-table="DCre" data-field="x_DcrUsu" name="o<?php echo $DCre_list->RowIndex ?>_DcrUsu" id="o<?php echo $DCre_list->RowIndex ?>_DcrUsu" value="<?php echo ew_HtmlEncode($DCre->DcrUsu->OldValue) ?>">
<?php } ?>
<?php if ($DCre->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $DCre_list->RowCnt ?>_DCre_DcrUsu" class="form-group DCre_DcrUsu">
<input type="text" data-table="DCre" data-field="x_DcrUsu" name="x<?php echo $DCre_list->RowIndex ?>_DcrUsu" id="x<?php echo $DCre_list->RowIndex ?>_DcrUsu" size="30" placeholder="<?php echo ew_HtmlEncode($DCre->DcrUsu->getPlaceHolder()) ?>" value="<?php echo $DCre->DcrUsu->EditValue ?>"<?php echo $DCre->DcrUsu->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($DCre->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $DCre_list->RowCnt ?>_DCre_DcrUsu" class="DCre_DcrUsu">
<span<?php echo $DCre->DcrUsu->ViewAttributes() ?>>
<?php echo $DCre->DcrUsu->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
<?php

// Render list options (body, right)
$DCre_list->ListOptions->Render("body", "right", $DCre_list->RowCnt);
?>
	</tr>
<?php if ($DCre->RowType == EW_ROWTYPE_ADD || $DCre->RowType == EW_ROWTYPE_EDIT) { ?>
<script type="text/javascript">
fDCrelist.UpdateOpts(<?php echo $DCre_list->RowIndex ?>);
</script>
<?php } ?>
<?php
	}
	} // End delete row checking
	if ($DCre->CurrentAction <> "gridadd")
		if (!$DCre_list->Recordset->EOF) $DCre_list->Recordset->MoveNext();
}
?>
<?php
	if ($DCre->CurrentAction == "gridadd" || $DCre->CurrentAction == "gridedit") {
		$DCre_list->RowIndex = '$rowindex$';
		$DCre_list->LoadDefaultValues();

		// Set row properties
		$DCre->ResetAttrs();
		$DCre->RowAttrs = array_merge($DCre->RowAttrs, array('data-rowindex'=>$DCre_list->RowIndex, 'id'=>'r0_DCre', 'data-rowtype'=>EW_ROWTYPE_ADD));
		ew_AppendClass($DCre->RowAttrs["class"], "ewTemplate");
		$DCre->RowType = EW_ROWTYPE_ADD;

		// Render row
		$DCre_list->RenderRow();

		// Render list options
		$DCre_list->RenderListOptions();
		$DCre_list->StartRowCnt = 0;
?>
	<tr<?php echo $DCre->RowAttributes() ?>>
<?php

// Render list options (body, left)
$DCre_list->ListOptions->Render("body", "left", $DCre_list->RowIndex);
?>
	<?php if ($DCre->DcrCodi->Visible) { // DcrCodi ?>
		<td data-name="DcrCodi">
<span id="el$rowindex$_DCre_DcrCodi" class="form-group DCre_DcrCodi">
<input type="text" data-table="DCre" data-field="x_DcrCodi" name="x<?php echo $DCre_list->RowIndex ?>_DcrCodi" id="x<?php echo $DCre_list->RowIndex ?>_DcrCodi" size="30" placeholder="<?php echo ew_HtmlEncode($DCre->DcrCodi->getPlaceHolder()) ?>" value="<?php echo $DCre->DcrCodi->EditValue ?>"<?php echo $DCre->DcrCodi->EditAttributes() ?>>
</span>
<input type="hidden" data-table="DCre" data-field="x_DcrCodi" name="o<?php echo $DCre_list->RowIndex ?>_DcrCodi" id="o<?php echo $DCre_list->RowIndex ?>_DcrCodi" value="<?php echo ew_HtmlEncode($DCre->DcrCodi->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($DCre->VcrCodi->Visible) { // VcrCodi ?>
		<td data-name="VcrCodi">
<span id="el$rowindex$_DCre_VcrCodi" class="form-group DCre_VcrCodi">
<select data-table="DCre" data-field="x_VcrCodi" data-value-separator="<?php echo ew_HtmlEncode(is_array($DCre->VcrCodi->DisplayValueSeparator) ? json_encode($DCre->VcrCodi->DisplayValueSeparator) : $DCre->VcrCodi->DisplayValueSeparator) ?>" id="x<?php echo $DCre_list->RowIndex ?>_VcrCodi" name="x<?php echo $DCre_list->RowIndex ?>_VcrCodi"<?php echo $DCre->VcrCodi->EditAttributes() ?>>
<?php
if (is_array($DCre->VcrCodi->EditValue)) {
	$arwrk = $DCre->VcrCodi->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($DCre->VcrCodi->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $DCre->VcrCodi->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($DCre->VcrCodi->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($DCre->VcrCodi->CurrentValue) ?>" selected><?php echo $DCre->VcrCodi->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $DCre->VcrCodi->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT \"VcrCodi\", \"VcrCodi\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"VCre\"";
$sWhereWrk = "";
$DCre->VcrCodi->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$DCre->VcrCodi->LookupFilters += array("f0" => "\"VcrCodi\" = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$DCre->Lookup_Selecting($DCre->VcrCodi, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $DCre->VcrCodi->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $DCre_list->RowIndex ?>_VcrCodi" id="s_x<?php echo $DCre_list->RowIndex ?>_VcrCodi" value="<?php echo $DCre->VcrCodi->LookupFilterQuery() ?>">
</span>
<input type="hidden" data-table="DCre" data-field="x_VcrCodi" name="o<?php echo $DCre_list->RowIndex ?>_VcrCodi" id="o<?php echo $DCre_list->RowIndex ?>_VcrCodi" value="<?php echo ew_HtmlEncode($DCre->VcrCodi->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($DCre->DcrFech->Visible) { // DcrFech ?>
		<td data-name="DcrFech">
<span id="el$rowindex$_DCre_DcrFech" class="form-group DCre_DcrFech">
<input type="text" data-table="DCre" data-field="x_DcrFech" data-format="7" name="x<?php echo $DCre_list->RowIndex ?>_DcrFech" id="x<?php echo $DCre_list->RowIndex ?>_DcrFech" placeholder="<?php echo ew_HtmlEncode($DCre->DcrFech->getPlaceHolder()) ?>" value="<?php echo $DCre->DcrFech->EditValue ?>"<?php echo $DCre->DcrFech->EditAttributes() ?>>
</span>
<input type="hidden" data-table="DCre" data-field="x_DcrFech" name="o<?php echo $DCre_list->RowIndex ?>_DcrFech" id="o<?php echo $DCre_list->RowIndex ?>_DcrFech" value="<?php echo ew_HtmlEncode($DCre->DcrFech->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($DCre->DcrMont->Visible) { // DcrMont ?>
		<td data-name="DcrMont">
<span id="el$rowindex$_DCre_DcrMont" class="form-group DCre_DcrMont">
<input type="text" data-table="DCre" data-field="x_DcrMont" name="x<?php echo $DCre_list->RowIndex ?>_DcrMont" id="x<?php echo $DCre_list->RowIndex ?>_DcrMont" size="30" placeholder="<?php echo ew_HtmlEncode($DCre->DcrMont->getPlaceHolder()) ?>" value="<?php echo $DCre->DcrMont->EditValue ?>"<?php echo $DCre->DcrMont->EditAttributes() ?>>
</span>
<input type="hidden" data-table="DCre" data-field="x_DcrMont" name="o<?php echo $DCre_list->RowIndex ?>_DcrMont" id="o<?php echo $DCre_list->RowIndex ?>_DcrMont" value="<?php echo ew_HtmlEncode($DCre->DcrMont->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($DCre->DcrSCuo->Visible) { // DcrSCuo ?>
		<td data-name="DcrSCuo">
<span id="el$rowindex$_DCre_DcrSCuo" class="form-group DCre_DcrSCuo">
<input type="text" data-table="DCre" data-field="x_DcrSCuo" name="x<?php echo $DCre_list->RowIndex ?>_DcrSCuo" id="x<?php echo $DCre_list->RowIndex ?>_DcrSCuo" size="30" placeholder="<?php echo ew_HtmlEncode($DCre->DcrSCuo->getPlaceHolder()) ?>" value="<?php echo $DCre->DcrSCuo->EditValue ?>"<?php echo $DCre->DcrSCuo->EditAttributes() ?>>
</span>
<input type="hidden" data-table="DCre" data-field="x_DcrSCuo" name="o<?php echo $DCre_list->RowIndex ?>_DcrSCuo" id="o<?php echo $DCre_list->RowIndex ?>_DcrSCuo" value="<?php echo ew_HtmlEncode($DCre->DcrSCuo->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($DCre->DcrNCuo->Visible) { // DcrNCuo ?>
		<td data-name="DcrNCuo">
<span id="el$rowindex$_DCre_DcrNCuo" class="form-group DCre_DcrNCuo">
<input type="text" data-table="DCre" data-field="x_DcrNCuo" name="x<?php echo $DCre_list->RowIndex ?>_DcrNCuo" id="x<?php echo $DCre_list->RowIndex ?>_DcrNCuo" size="30" placeholder="<?php echo ew_HtmlEncode($DCre->DcrNCuo->getPlaceHolder()) ?>" value="<?php echo $DCre->DcrNCuo->EditValue ?>"<?php echo $DCre->DcrNCuo->EditAttributes() ?>>
</span>
<input type="hidden" data-table="DCre" data-field="x_DcrNCuo" name="o<?php echo $DCre_list->RowIndex ?>_DcrNCuo" id="o<?php echo $DCre_list->RowIndex ?>_DcrNCuo" value="<?php echo ew_HtmlEncode($DCre->DcrNCuo->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($DCre->DcrEsta->Visible) { // DcrEsta ?>
		<td data-name="DcrEsta">
<span id="el$rowindex$_DCre_DcrEsta" class="form-group DCre_DcrEsta">
<input type="text" data-table="DCre" data-field="x_DcrEsta" name="x<?php echo $DCre_list->RowIndex ?>_DcrEsta" id="x<?php echo $DCre_list->RowIndex ?>_DcrEsta" size="30" maxlength="1" placeholder="<?php echo ew_HtmlEncode($DCre->DcrEsta->getPlaceHolder()) ?>" value="<?php echo $DCre->DcrEsta->EditValue ?>"<?php echo $DCre->DcrEsta->EditAttributes() ?>>
</span>
<input type="hidden" data-table="DCre" data-field="x_DcrEsta" name="o<?php echo $DCre_list->RowIndex ?>_DcrEsta" id="o<?php echo $DCre_list->RowIndex ?>_DcrEsta" value="<?php echo ew_HtmlEncode($DCre->DcrEsta->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($DCre->DcrUsu->Visible) { // DcrUsu ?>
		<td data-name="DcrUsu">
<span id="el$rowindex$_DCre_DcrUsu" class="form-group DCre_DcrUsu">
<input type="text" data-table="DCre" data-field="x_DcrUsu" name="x<?php echo $DCre_list->RowIndex ?>_DcrUsu" id="x<?php echo $DCre_list->RowIndex ?>_DcrUsu" size="30" placeholder="<?php echo ew_HtmlEncode($DCre->DcrUsu->getPlaceHolder()) ?>" value="<?php echo $DCre->DcrUsu->EditValue ?>"<?php echo $DCre->DcrUsu->EditAttributes() ?>>
</span>
<input type="hidden" data-table="DCre" data-field="x_DcrUsu" name="o<?php echo $DCre_list->RowIndex ?>_DcrUsu" id="o<?php echo $DCre_list->RowIndex ?>_DcrUsu" value="<?php echo ew_HtmlEncode($DCre->DcrUsu->OldValue) ?>">
</td>
	<?php } ?>
<?php

// Render list options (body, right)
$DCre_list->ListOptions->Render("body", "right", $DCre_list->RowCnt);
?>
<script type="text/javascript">
fDCrelist.UpdateOpts(<?php echo $DCre_list->RowIndex ?>);
</script>
	</tr>
<?php
}
?>
</tbody>
</table>
<?php } ?>
<?php if ($DCre->CurrentAction == "gridadd") { ?>
<input type="hidden" name="a_list" id="a_list" value="gridinsert">
<input type="hidden" name="<?php echo $DCre_list->FormKeyCountName ?>" id="<?php echo $DCre_list->FormKeyCountName ?>" value="<?php echo $DCre_list->KeyCount ?>">
<?php echo $DCre_list->MultiSelectKey ?>
<?php } ?>
<?php if ($DCre->CurrentAction == "gridedit") { ?>
<input type="hidden" name="a_list" id="a_list" value="gridupdate">
<input type="hidden" name="<?php echo $DCre_list->FormKeyCountName ?>" id="<?php echo $DCre_list->FormKeyCountName ?>" value="<?php echo $DCre_list->KeyCount ?>">
<?php echo $DCre_list->MultiSelectKey ?>
<?php } ?>
<?php if ($DCre->CurrentAction == "") { ?>
<input type="hidden" name="a_list" id="a_list" value="">
<?php } ?>
</div>
</form>
<?php

// Close recordset
if ($DCre_list->Recordset)
	$DCre_list->Recordset->Close();
?>
<div class="panel-footer ewGridLowerPanel">
<?php if ($DCre->CurrentAction <> "gridadd" && $DCre->CurrentAction <> "gridedit") { ?>
<form name="ewPagerForm" class="ewForm form-inline ewPagerForm" action="<?php echo ew_CurrentPage() ?>">
<?php if (!isset($DCre_list->Pager)) $DCre_list->Pager = new cPrevNextPager($DCre_list->StartRec, $DCre_list->DisplayRecs, $DCre_list->TotalRecs) ?>
<?php if ($DCre_list->Pager->RecordCount > 0) { ?>
<div class="ewPager">
<span><?php echo $Language->Phrase("Page") ?>&nbsp;</span>
<div class="ewPrevNext"><div class="input-group">
<div class="input-group-btn">
<!--first page button-->
	<?php if ($DCre_list->Pager->FirstButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerFirst") ?>" href="<?php echo $DCre_list->PageUrl() ?>start=<?php echo $DCre_list->Pager->FirstButton->Start ?>"><span class="icon-first ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerFirst") ?>"><span class="icon-first ewIcon"></span></a>
	<?php } ?>
<!--previous page button-->
	<?php if ($DCre_list->Pager->PrevButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerPrevious") ?>" href="<?php echo $DCre_list->PageUrl() ?>start=<?php echo $DCre_list->Pager->PrevButton->Start ?>"><span class="icon-prev ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerPrevious") ?>"><span class="icon-prev ewIcon"></span></a>
	<?php } ?>
</div>
<!--current page number-->
	<input class="form-control input-sm" type="text" name="<?php echo EW_TABLE_PAGE_NO ?>" value="<?php echo $DCre_list->Pager->CurrentPage ?>">
<div class="input-group-btn">
<!--next page button-->
	<?php if ($DCre_list->Pager->NextButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerNext") ?>" href="<?php echo $DCre_list->PageUrl() ?>start=<?php echo $DCre_list->Pager->NextButton->Start ?>"><span class="icon-next ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerNext") ?>"><span class="icon-next ewIcon"></span></a>
	<?php } ?>
<!--last page button-->
	<?php if ($DCre_list->Pager->LastButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerLast") ?>" href="<?php echo $DCre_list->PageUrl() ?>start=<?php echo $DCre_list->Pager->LastButton->Start ?>"><span class="icon-last ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerLast") ?>"><span class="icon-last ewIcon"></span></a>
	<?php } ?>
</div>
</div>
</div>
<span>&nbsp;<?php echo $Language->Phrase("of") ?>&nbsp;<?php echo $DCre_list->Pager->PageCount ?></span>
</div>
<div class="ewPager ewRec">
	<span><?php echo $Language->Phrase("Record") ?>&nbsp;<?php echo $DCre_list->Pager->FromIndex ?>&nbsp;<?php echo $Language->Phrase("To") ?>&nbsp;<?php echo $DCre_list->Pager->ToIndex ?>&nbsp;<?php echo $Language->Phrase("Of") ?>&nbsp;<?php echo $DCre_list->Pager->RecordCount ?></span>
</div>
<?php } ?>
</form>
<?php } ?>
<div class="ewListOtherOptions">
<?php
	foreach ($DCre_list->OtherOptions as &$option)
		$option->Render("body", "bottom");
?>
</div>
<div class="clearfix"></div>
</div>
</div>
<?php } ?>
<?php if ($DCre_list->TotalRecs == 0 && $DCre->CurrentAction == "") { // Show other options ?>
<div class="ewListOtherOptions">
<?php
	foreach ($DCre_list->OtherOptions as &$option) {
		$option->ButtonClass = "";
		$option->Render("body", "");
	}
?>
</div>
<div class="clearfix"></div>
<?php } ?>
<script type="text/javascript">
fDCrelistsrch.Init();
fDCrelistsrch.FilterList = <?php echo $DCre_list->GetFilterList() ?>;
fDCrelist.Init();
</script>
<?php
$DCre_list->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$DCre_list->Page_Terminate();
?>
