<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "Usuainfo.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$Usua_add = NULL; // Initialize page object first

class cUsua_add extends cUsua {

	// Page ID
	var $PageID = 'add';

	// Project ID
	var $ProjectID = "{04439FF7-B43F-460F-8514-F71C8FF9E679}";

	// Table name
	var $TableName = 'Usua';

	// Page object name
	var $PageObjName = 'Usua_add';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (Usua)
		if (!isset($GLOBALS["Usua"]) || get_class($GLOBALS["Usua"]) == "cUsua") {
			$GLOBALS["Usua"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["Usua"];
		}

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'add', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'Usua', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (Usua)
		if (!isset($UserTable)) {
			$UserTable = new cUsua();
			$UserTableConn = Conn($UserTable->DBID);
		}
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanAdd()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage($Language->Phrase("NoPermission")); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("Usualist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}

		// Create form object
		$objForm = new cFormObj();
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Process auto fill
		if (@$_POST["ajax"] == "autofill") {
			$results = $this->GetAutoFill(@$_POST["name"], @$_POST["q"]);
			if ($results) {

				// Clean output buffer
				if (!EW_DEBUG_ENABLED && ob_get_length())
					ob_end_clean();
				echo $results;
				$this->Page_Terminate();
				exit();
			}
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $Usua;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($Usua);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $FormClassName = "form-horizontal ewForm ewAddForm";
	var $DbMasterFilter = "";
	var $DbDetailFilter = "";
	var $StartRec;
	var $Priv = 0;
	var $OldRecordset;
	var $CopyRecord;

	// 
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError;

		// Process form if post back
		if (@$_POST["a_add"] <> "") {
			$this->CurrentAction = $_POST["a_add"]; // Get form action
			$this->CopyRecord = $this->LoadOldRecord(); // Load old recordset
			$this->LoadFormValues(); // Load form values
		} else { // Not post back

			// Load key values from QueryString
			$this->CopyRecord = TRUE;
			if (@$_GET["UsuCodi"] != "") {
				$this->UsuCodi->setQueryStringValue($_GET["UsuCodi"]);
				$this->setKey("UsuCodi", $this->UsuCodi->CurrentValue); // Set up key
			} else {
				$this->setKey("UsuCodi", ""); // Clear key
				$this->CopyRecord = FALSE;
			}
			if ($this->CopyRecord) {
				$this->CurrentAction = "C"; // Copy record
			} else {
				$this->CurrentAction = "I"; // Display blank record
				$this->LoadDefaultValues(); // Load default values
			}
		}

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Validate form if post back
		if (@$_POST["a_add"] <> "") {
			if (!$this->ValidateForm()) {
				$this->CurrentAction = "I"; // Form error, reset action
				$this->EventCancelled = TRUE; // Event cancelled
				$this->RestoreFormValues(); // Restore form values
				$this->setFailureMessage($gsFormError);
			}
		}

		// Perform action based on action code
		switch ($this->CurrentAction) {
			case "I": // Blank record, no action required
				break;
			case "C": // Copy an existing record
				if (!$this->LoadRow()) { // Load record based on key
					if ($this->getFailureMessage() == "") $this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
					$this->Page_Terminate("Usualist.php"); // No matching record, return to list
				}
				break;
			case "A": // Add new record
				$this->SendEmail = TRUE; // Send email on add success
				if ($this->AddRow($this->OldRecordset)) { // Add successful
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage($Language->Phrase("AddSuccess")); // Set up success message
					$sReturnUrl = $this->getReturnUrl();
					if (ew_GetPageName($sReturnUrl) == "Usuaview.php")
						$sReturnUrl = $this->GetViewUrl(); // View paging, return to view page with keyurl directly
					$this->Page_Terminate($sReturnUrl); // Clean up and return
				} else {
					$this->EventCancelled = TRUE; // Event cancelled
					$this->RestoreFormValues(); // Add failed, restore form values
				}
		}

		// Render row based on row type
		if ($this->CurrentAction == "F") { // Confirm page
			$this->RowType = EW_ROWTYPE_VIEW; // Render view type
		} else {
			$this->RowType = EW_ROWTYPE_ADD; // Render add type
		}

		// Render row
		$this->ResetAttrs();
		$this->RenderRow();
	}

	// Get upload files
	function GetUploadFiles() {
		global $objForm, $Language;

		// Get upload data
	}

	// Load default values
	function LoadDefaultValues() {
		$this->UsuUser->CurrentValue = NULL;
		$this->UsuUser->OldValue = $this->UsuUser->CurrentValue;
		$this->UsuCont->CurrentValue = NULL;
		$this->UsuCont->OldValue = $this->UsuCont->CurrentValue;
		$this->userlevel->CurrentValue = NULL;
		$this->userlevel->OldValue = $this->userlevel->CurrentValue;
		$this->EmpCodi->CurrentValue = NULL;
		$this->EmpCodi->OldValue = $this->EmpCodi->CurrentValue;
		$this->UsuMail->CurrentValue = NULL;
		$this->UsuMail->OldValue = $this->UsuMail->CurrentValue;
	}

	// Load form values
	function LoadFormValues() {

		// Load from form
		global $objForm;
		if (!$this->UsuUser->FldIsDetailKey) {
			$this->UsuUser->setFormValue($objForm->GetValue("x_UsuUser"));
		}
		if (!$this->UsuCont->FldIsDetailKey) {
			$this->UsuCont->setFormValue($objForm->GetValue("x_UsuCont"));
		}
		if (!$this->userlevel->FldIsDetailKey) {
			$this->userlevel->setFormValue($objForm->GetValue("x_userlevel"));
		}
		if (!$this->EmpCodi->FldIsDetailKey) {
			$this->EmpCodi->setFormValue($objForm->GetValue("x_EmpCodi"));
		}
		if (!$this->UsuMail->FldIsDetailKey) {
			$this->UsuMail->setFormValue($objForm->GetValue("x_UsuMail"));
		}
	}

	// Restore form values
	function RestoreFormValues() {
		global $objForm;
		$this->LoadOldRecord();
		$this->UsuUser->CurrentValue = $this->UsuUser->FormValue;
		$this->UsuCont->CurrentValue = $this->UsuCont->FormValue;
		$this->userlevel->CurrentValue = $this->userlevel->FormValue;
		$this->EmpCodi->CurrentValue = $this->EmpCodi->FormValue;
		$this->UsuMail->CurrentValue = $this->UsuMail->FormValue;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->UsuCodi->setDbValue($rs->fields('UsuCodi'));
		$this->UsuUser->setDbValue($rs->fields('UsuUser'));
		$this->UsuCont->setDbValue($rs->fields('UsuCont'));
		$this->userlevel->setDbValue($rs->fields('userlevel'));
		$this->EmpCodi->setDbValue($rs->fields('EmpCodi'));
		$this->UsuMail->setDbValue($rs->fields('UsuMail'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->UsuCodi->DbValue = $row['UsuCodi'];
		$this->UsuUser->DbValue = $row['UsuUser'];
		$this->UsuCont->DbValue = $row['UsuCont'];
		$this->userlevel->DbValue = $row['userlevel'];
		$this->EmpCodi->DbValue = $row['EmpCodi'];
		$this->UsuMail->DbValue = $row['UsuMail'];
	}

	// Load old record
	function LoadOldRecord() {

		// Load key values from Session
		$bValidKey = TRUE;
		if (strval($this->getKey("UsuCodi")) <> "")
			$this->UsuCodi->CurrentValue = $this->getKey("UsuCodi"); // UsuCodi
		else
			$bValidKey = FALSE;

		// Load old recordset
		if ($bValidKey) {
			$this->CurrentFilter = $this->KeyFilter();
			$sSql = $this->SQL();
			$conn = &$this->Connection();
			$this->OldRecordset = ew_LoadRecordset($sSql, $conn);
			$this->LoadRowValues($this->OldRecordset); // Load row values
		} else {
			$this->OldRecordset = NULL;
		}
		return $bValidKey;
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		// Call Row_Rendering event

		$this->Row_Rendering();

		// Common render codes for all row types
		// UsuCodi
		// UsuUser
		// UsuCont
		// userlevel
		// EmpCodi
		// UsuMail

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// UsuCodi
		$this->UsuCodi->ViewValue = $this->UsuCodi->CurrentValue;
		$this->UsuCodi->ViewCustomAttributes = "";

		// UsuUser
		$this->UsuUser->ViewValue = $this->UsuUser->CurrentValue;
		$this->UsuUser->ViewCustomAttributes = "";

		// UsuCont
		$this->UsuCont->ViewValue = $this->UsuCont->CurrentValue;
		$this->UsuCont->ViewCustomAttributes = "";

		// userlevel
		if ($Security->CanAdmin()) { // System admin
		if (strval($this->userlevel->CurrentValue) <> "") {
			$this->userlevel->ViewValue = $this->userlevel->OptionCaption($this->userlevel->CurrentValue);
		} else {
			$this->userlevel->ViewValue = NULL;
		}
		} else {
			$this->userlevel->ViewValue = $Language->Phrase("PasswordMask");
		}
		$this->userlevel->ViewCustomAttributes = "";

		// EmpCodi
		if (strval($this->EmpCodi->CurrentValue) <> "") {
			$sFilterWrk = "\"EmpCodi\"" . ew_SearchString("=", $this->EmpCodi->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT \"EmpCodi\", \"EmpNomb\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"Empl\"";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->EmpCodi, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->EmpCodi->ViewValue = $this->EmpCodi->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->EmpCodi->ViewValue = $this->EmpCodi->CurrentValue;
			}
		} else {
			$this->EmpCodi->ViewValue = NULL;
		}
		$this->EmpCodi->ViewCustomAttributes = "";

		// UsuMail
		$this->UsuMail->ViewValue = $this->UsuMail->CurrentValue;
		$this->UsuMail->ViewCustomAttributes = "";

			// UsuUser
			$this->UsuUser->LinkCustomAttributes = "";
			$this->UsuUser->HrefValue = "";
			$this->UsuUser->TooltipValue = "";

			// UsuCont
			$this->UsuCont->LinkCustomAttributes = "";
			$this->UsuCont->HrefValue = "";
			$this->UsuCont->TooltipValue = "";

			// userlevel
			$this->userlevel->LinkCustomAttributes = "";
			$this->userlevel->HrefValue = "";
			$this->userlevel->TooltipValue = "";

			// EmpCodi
			$this->EmpCodi->LinkCustomAttributes = "";
			$this->EmpCodi->HrefValue = "";
			$this->EmpCodi->TooltipValue = "";

			// UsuMail
			$this->UsuMail->LinkCustomAttributes = "";
			$this->UsuMail->HrefValue = "";
			$this->UsuMail->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_ADD) { // Add row

			// UsuUser
			$this->UsuUser->EditAttrs["class"] = "form-control";
			$this->UsuUser->EditCustomAttributes = "";
			$this->UsuUser->EditValue = ew_HtmlEncode($this->UsuUser->CurrentValue);
			$this->UsuUser->PlaceHolder = ew_RemoveHtml($this->UsuUser->FldCaption());

			// UsuCont
			$this->UsuCont->EditAttrs["class"] = "form-control ewPasswordStrength";
			$this->UsuCont->EditCustomAttributes = "";
			$this->UsuCont->EditValue = ew_HtmlEncode($this->UsuCont->CurrentValue);
			$this->UsuCont->PlaceHolder = ew_RemoveHtml($this->UsuCont->FldCaption());

			// userlevel
			$this->userlevel->EditAttrs["class"] = "form-control";
			$this->userlevel->EditCustomAttributes = "";
			if (!$Security->CanAdmin()) { // System admin
				$this->userlevel->EditValue = $Language->Phrase("PasswordMask");
			} else {
			$this->userlevel->EditValue = $this->userlevel->Options(TRUE);
			}

			// EmpCodi
			$this->EmpCodi->EditAttrs["class"] = "form-control";
			$this->EmpCodi->EditCustomAttributes = "";
			if (trim(strval($this->EmpCodi->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "\"EmpCodi\"" . ew_SearchString("=", $this->EmpCodi->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT \"EmpCodi\", \"EmpNomb\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\", '' AS \"SelectFilterFld\", '' AS \"SelectFilterFld2\", '' AS \"SelectFilterFld3\", '' AS \"SelectFilterFld4\" FROM \"public\".\"Empl\"";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->EmpCodi, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->EmpCodi->EditValue = $arwrk;

			// UsuMail
			$this->UsuMail->EditAttrs["class"] = "form-control";
			$this->UsuMail->EditCustomAttributes = "";
			$this->UsuMail->EditValue = ew_HtmlEncode($this->UsuMail->CurrentValue);
			$this->UsuMail->PlaceHolder = ew_RemoveHtml($this->UsuMail->FldCaption());

			// Edit refer script
			// UsuUser

			$this->UsuUser->HrefValue = "";

			// UsuCont
			$this->UsuCont->HrefValue = "";

			// userlevel
			$this->userlevel->HrefValue = "";

			// EmpCodi
			$this->EmpCodi->HrefValue = "";

			// UsuMail
			$this->UsuMail->HrefValue = "";
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate form
	function ValidateForm() {
		global $Language, $gsFormError;

		// Initialize form error message
		$gsFormError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return ($gsFormError == "");
		if (!$this->UsuUser->FldIsDetailKey && !is_null($this->UsuUser->FormValue) && $this->UsuUser->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->UsuUser->FldCaption(), $this->UsuUser->ReqErrMsg));
		}
		if (!$this->UsuCont->FldIsDetailKey && !is_null($this->UsuCont->FormValue) && $this->UsuCont->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->UsuCont->FldCaption(), $this->UsuCont->ReqErrMsg));
		}
		if (!$this->EmpCodi->FldIsDetailKey && !is_null($this->EmpCodi->FormValue) && $this->EmpCodi->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->EmpCodi->FldCaption(), $this->EmpCodi->ReqErrMsg));
		}
		if (!$this->UsuMail->FldIsDetailKey && !is_null($this->UsuMail->FormValue) && $this->UsuMail->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->UsuMail->FldCaption(), $this->UsuMail->ReqErrMsg));
		}
		if (!ew_CheckEmail($this->UsuMail->FormValue)) {
			ew_AddMessage($gsFormError, $this->UsuMail->FldErrMsg());
		}

		// Return validate result
		$ValidateForm = ($gsFormError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateForm = $ValidateForm && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsFormError, $sFormCustomError);
		}
		return $ValidateForm;
	}

	// Add record
	function AddRow($rsold = NULL) {
		global $Language, $Security;
		if ($this->UsuUser->CurrentValue <> "") { // Check field with unique index
			$sFilter = "(UsuUser = '" . ew_AdjustSql($this->UsuUser->CurrentValue, $this->DBID) . "')";
			$rsChk = $this->LoadRs($sFilter);
			if ($rsChk && !$rsChk->EOF) {
				$sIdxErrMsg = str_replace("%f", $this->UsuUser->FldCaption(), $Language->Phrase("DupIndex"));
				$sIdxErrMsg = str_replace("%v", $this->UsuUser->CurrentValue, $sIdxErrMsg);
				$this->setFailureMessage($sIdxErrMsg);
				$rsChk->Close();
				return FALSE;
			}
		}
		if ($this->UsuMail->CurrentValue <> "") { // Check field with unique index
			$sFilter = "(UsuMail = '" . ew_AdjustSql($this->UsuMail->CurrentValue, $this->DBID) . "')";
			$rsChk = $this->LoadRs($sFilter);
			if ($rsChk && !$rsChk->EOF) {
				$sIdxErrMsg = str_replace("%f", $this->UsuMail->FldCaption(), $Language->Phrase("DupIndex"));
				$sIdxErrMsg = str_replace("%v", $this->UsuMail->CurrentValue, $sIdxErrMsg);
				$this->setFailureMessage($sIdxErrMsg);
				$rsChk->Close();
				return FALSE;
			}
		}
		$conn = &$this->Connection();

		// Load db values from rsold
		if ($rsold) {
			$this->LoadDbValues($rsold);
		}
		$rsnew = array();

		// UsuUser
		$this->UsuUser->SetDbValueDef($rsnew, $this->UsuUser->CurrentValue, "", FALSE);

		// UsuCont
		$this->UsuCont->SetDbValueDef($rsnew, $this->UsuCont->CurrentValue, "", FALSE);

		// userlevel
		if ($Security->CanAdmin()) { // System admin
		$this->userlevel->SetDbValueDef($rsnew, $this->userlevel->CurrentValue, NULL, FALSE);
		}

		// EmpCodi
		$this->EmpCodi->SetDbValueDef($rsnew, $this->EmpCodi->CurrentValue, 0, FALSE);

		// UsuMail
		$this->UsuMail->SetDbValueDef($rsnew, $this->UsuMail->CurrentValue, NULL, FALSE);

		// Call Row Inserting event
		$rs = ($rsold == NULL) ? NULL : $rsold->fields;
		$bInsertRow = $this->Row_Inserting($rs, $rsnew);
		if ($bInsertRow) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			$AddRow = $this->Insert($rsnew);
			$conn->raiseErrorFn = '';
			if ($AddRow) {

				// Get insert id if necessary
				$this->UsuCodi->setDbValue($conn->GetOne("SELECT currval('\"Usua_UsuCodi_seq\"'::regclass)"));
				$rsnew['UsuCodi'] = $this->UsuCodi->DbValue;
			}
		} else {
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("InsertCancelled"));
			}
			$AddRow = FALSE;
		}
		if ($AddRow) {

			// Call Row Inserted event
			$rs = ($rsold == NULL) ? NULL : $rsold->fields;
			$this->Row_Inserted($rs, $rsnew);
		}
		return $AddRow;
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, "Usualist.php", "", $this->TableVar, TRUE);
		$PageId = ($this->CurrentAction == "C") ? "Copy" : "Add";
		$Breadcrumb->Add("add", $PageId, $url);
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($Usua_add)) $Usua_add = new cUsua_add();

// Page init
$Usua_add->Page_Init();

// Page main
$Usua_add->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$Usua_add->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "add";
var CurrentForm = fUsuaadd = new ew_Form("fUsuaadd", "add");

// Validate form
fUsuaadd.Validate = function() {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.GetForm(), $fobj = $(fobj);
	if ($fobj.find("#a_confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.FormKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = $fobj.find("#a_list").val() == "gridinsert";
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
			elm = this.GetElements("x" + infix + "_UsuUser");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $Usua->UsuUser->FldCaption(), $Usua->UsuUser->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_UsuCont");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $Usua->UsuCont->FldCaption(), $Usua->UsuCont->ReqErrMsg)) ?>");
			if ($(fobj.x_UsuCont).hasClass("ewPasswordStrength") && !$(fobj.x_UsuCont).data("validated"))
				return this.OnError(fobj.x_UsuCont, ewLanguage.Phrase("PasswordTooSimple"));
			elm = this.GetElements("x" + infix + "_EmpCodi");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $Usua->EmpCodi->FldCaption(), $Usua->EmpCodi->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_UsuMail");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $Usua->UsuMail->FldCaption(), $Usua->UsuMail->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_UsuMail");
			if (elm && !ew_CheckEmail(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($Usua->UsuMail->FldErrMsg()) ?>");

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
	}

	// Process detail forms
	var dfs = $fobj.find("input[name='detailpage']").get();
	for (var i = 0; i < dfs.length; i++) {
		var df = dfs[i], val = df.value;
		if (val && ewForms[val])
			if (!ewForms[val].Validate())
				return false;
	}
	return true;
}

// Form_CustomValidate event
fUsuaadd.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fUsuaadd.ValidateRequired = true;
<?php } else { ?>
fUsuaadd.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
fUsuaadd.Lists["x_userlevel"] = {"LinkField":"","Ajax":false,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fUsuaadd.Lists["x_userlevel"].Options = <?php echo json_encode($Usua->userlevel->Options()) ?>;
fUsuaadd.Lists["x_EmpCodi"] = {"LinkField":"x_EmpCodi","Ajax":true,"AutoFill":false,"DisplayFields":["x_EmpNomb","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};

// Form object for search
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php $Usua_add->ShowPageHeader(); ?>
<?php
$Usua_add->ShowMessage();
?>
<form name="fUsuaadd" id="fUsuaadd" class="<?php echo $Usua_add->FormClassName ?>" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($Usua_add->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $Usua_add->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="Usua">
<input type="hidden" name="a_add" id="a_add" value="A">
<?php if ($Usua->CurrentAction == "F") { // Confirm page ?>
<input type="hidden" name="a_confirm" id="a_confirm" value="F">
<?php } ?>
<!-- Fields to prevent google autofill -->
<input class="hidden" type="text" name="<?php echo ew_Encrypt(ew_Random()) ?>">
<input class="hidden" type="password" name="<?php echo ew_Encrypt(ew_Random()) ?>">
<div>
<?php if ($Usua->UsuUser->Visible) { // UsuUser ?>
	<div id="r_UsuUser" class="form-group">
		<label id="elh_Usua_UsuUser" for="x_UsuUser" class="col-sm-2 control-label ewLabel"><?php echo $Usua->UsuUser->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $Usua->UsuUser->CellAttributes() ?>>
<?php if ($Usua->CurrentAction <> "F") { ?>
<span id="el_Usua_UsuUser">
<input type="text" data-table="Usua" data-field="x_UsuUser" name="x_UsuUser" id="x_UsuUser" size="30" placeholder="<?php echo ew_HtmlEncode($Usua->UsuUser->getPlaceHolder()) ?>" value="<?php echo $Usua->UsuUser->EditValue ?>"<?php echo $Usua->UsuUser->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_Usua_UsuUser">
<span<?php echo $Usua->UsuUser->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $Usua->UsuUser->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="Usua" data-field="x_UsuUser" name="x_UsuUser" id="x_UsuUser" value="<?php echo ew_HtmlEncode($Usua->UsuUser->FormValue) ?>">
<?php } ?>
<?php echo $Usua->UsuUser->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($Usua->UsuCont->Visible) { // UsuCont ?>
	<div id="r_UsuCont" class="form-group">
		<label id="elh_Usua_UsuCont" for="x_UsuCont" class="col-sm-2 control-label ewLabel"><?php echo $Usua->UsuCont->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $Usua->UsuCont->CellAttributes() ?>>
<?php if ($Usua->CurrentAction <> "F") { ?>
<span id="el_Usua_UsuCont">
<div class="input-group" id="ig_x_UsuCont">
<input type="text" data-password-strength="pst_x_UsuCont" data-password-generated="pgt_x_UsuCont" data-table="Usua" data-field="x_UsuCont" name="x_UsuCont" id="x_UsuCont" value="<?php echo $Usua->UsuCont->EditValue ?>" size="30" placeholder="<?php echo ew_HtmlEncode($Usua->UsuCont->getPlaceHolder()) ?>"<?php echo $Usua->UsuCont->EditAttributes() ?>>
<span class="input-group-btn">
	<button type="button" class="btn btn-default ewPasswordGenerator" title="<?php echo ew_HtmlTitle($Language->Phrase("GeneratePassword")) ?>" data-password-field="x_UsuCont" data-password-confirm="c_UsuCont" data-password-strength="pst_x_UsuCont" data-password-generated="pgt_x_UsuCont"><?php echo $Language->Phrase("GeneratePassword") ?></button>
</span>
</div>
<span class="help-block" id="pgt_x_UsuCont" style="display: none;"></span>
<div class="progress ewPasswordStrengthBar" id="pst_x_UsuCont" style="display: none;">
	<div class="progress-bar" role="progressbar"></div>
</div>
</span>
<?php } else { ?>
<span id="el_Usua_UsuCont">
<span<?php echo $Usua->UsuCont->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $Usua->UsuCont->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="Usua" data-field="x_UsuCont" name="x_UsuCont" id="x_UsuCont" value="<?php echo ew_HtmlEncode($Usua->UsuCont->FormValue) ?>">
<?php } ?>
<?php echo $Usua->UsuCont->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($Usua->userlevel->Visible) { // userlevel ?>
	<div id="r_userlevel" class="form-group">
		<label id="elh_Usua_userlevel" for="x_userlevel" class="col-sm-2 control-label ewLabel"><?php echo $Usua->userlevel->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $Usua->userlevel->CellAttributes() ?>>
<?php if ($Usua->CurrentAction <> "F") { ?>
<?php if (!$Security->IsAdmin() && $Security->IsLoggedIn()) { // Non system admin ?>
<span id="el_Usua_userlevel">
<p class="form-control-static"><?php echo $Usua->userlevel->EditValue ?></p>
</span>
<?php } else { ?>
<span id="el_Usua_userlevel">
<select data-table="Usua" data-field="x_userlevel" data-value-separator="<?php echo ew_HtmlEncode(is_array($Usua->userlevel->DisplayValueSeparator) ? json_encode($Usua->userlevel->DisplayValueSeparator) : $Usua->userlevel->DisplayValueSeparator) ?>" id="x_userlevel" name="x_userlevel"<?php echo $Usua->userlevel->EditAttributes() ?>>
<?php
if (is_array($Usua->userlevel->EditValue)) {
	$arwrk = $Usua->userlevel->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($Usua->userlevel->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $Usua->userlevel->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($Usua->userlevel->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($Usua->userlevel->CurrentValue) ?>" selected><?php echo $Usua->userlevel->CurrentValue ?></option>
<?php
    }
}
?>
</select>
</span>
<?php } ?>
<?php } else { ?>
<span id="el_Usua_userlevel">
<span<?php echo $Usua->userlevel->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $Usua->userlevel->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="Usua" data-field="x_userlevel" name="x_userlevel" id="x_userlevel" value="<?php echo ew_HtmlEncode($Usua->userlevel->FormValue) ?>">
<?php } ?>
<?php echo $Usua->userlevel->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($Usua->EmpCodi->Visible) { // EmpCodi ?>
	<div id="r_EmpCodi" class="form-group">
		<label id="elh_Usua_EmpCodi" for="x_EmpCodi" class="col-sm-2 control-label ewLabel"><?php echo $Usua->EmpCodi->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $Usua->EmpCodi->CellAttributes() ?>>
<?php if ($Usua->CurrentAction <> "F") { ?>
<span id="el_Usua_EmpCodi">
<select data-table="Usua" data-field="x_EmpCodi" data-value-separator="<?php echo ew_HtmlEncode(is_array($Usua->EmpCodi->DisplayValueSeparator) ? json_encode($Usua->EmpCodi->DisplayValueSeparator) : $Usua->EmpCodi->DisplayValueSeparator) ?>" id="x_EmpCodi" name="x_EmpCodi"<?php echo $Usua->EmpCodi->EditAttributes() ?>>
<?php
if (is_array($Usua->EmpCodi->EditValue)) {
	$arwrk = $Usua->EmpCodi->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($Usua->EmpCodi->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $Usua->EmpCodi->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($Usua->EmpCodi->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($Usua->EmpCodi->CurrentValue) ?>" selected><?php echo $Usua->EmpCodi->CurrentValue ?></option>
<?php
    }
}
?>
</select>
<?php
$sSqlWrk = "SELECT \"EmpCodi\", \"EmpNomb\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"Empl\"";
$sWhereWrk = "";
$Usua->EmpCodi->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$Usua->EmpCodi->LookupFilters += array("f0" => "\"EmpCodi\" = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$Usua->Lookup_Selecting($Usua->EmpCodi, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $Usua->EmpCodi->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x_EmpCodi" id="s_x_EmpCodi" value="<?php echo $Usua->EmpCodi->LookupFilterQuery() ?>">
</span>
<?php } else { ?>
<span id="el_Usua_EmpCodi">
<span<?php echo $Usua->EmpCodi->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $Usua->EmpCodi->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="Usua" data-field="x_EmpCodi" name="x_EmpCodi" id="x_EmpCodi" value="<?php echo ew_HtmlEncode($Usua->EmpCodi->FormValue) ?>">
<?php } ?>
<?php echo $Usua->EmpCodi->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($Usua->UsuMail->Visible) { // UsuMail ?>
	<div id="r_UsuMail" class="form-group">
		<label id="elh_Usua_UsuMail" for="x_UsuMail" class="col-sm-2 control-label ewLabel"><?php echo $Usua->UsuMail->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $Usua->UsuMail->CellAttributes() ?>>
<?php if ($Usua->CurrentAction <> "F") { ?>
<span id="el_Usua_UsuMail">
<input type="text" data-table="Usua" data-field="x_UsuMail" name="x_UsuMail" id="x_UsuMail" size="30" maxlength="50" placeholder="<?php echo ew_HtmlEncode($Usua->UsuMail->getPlaceHolder()) ?>" value="<?php echo $Usua->UsuMail->EditValue ?>"<?php echo $Usua->UsuMail->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_Usua_UsuMail">
<span<?php echo $Usua->UsuMail->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $Usua->UsuMail->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="Usua" data-field="x_UsuMail" name="x_UsuMail" id="x_UsuMail" value="<?php echo ew_HtmlEncode($Usua->UsuMail->FormValue) ?>">
<?php } ?>
<?php echo $Usua->UsuMail->CustomMsg ?></div></div>
	</div>
<?php } ?>
</div>
<div class="form-group">
	<div class="col-sm-offset-2 col-sm-10">
<?php if ($Usua->CurrentAction <> "F") { // Confirm page ?>
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit" onclick="this.form.a_add.value='F';"><?php echo $Language->Phrase("AddBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="button" data-href="<?php echo $Usua_add->getReturnUrl() ?>"><?php echo $Language->Phrase("CancelBtn") ?></button>
<?php } else { ?>
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("ConfirmBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="submit" onclick="this.form.a_add.value='X';"><?php echo $Language->Phrase("CancelBtn") ?></button>
<?php } ?>
	</div>
</div>
</form>
<script type="text/javascript">
fUsuaadd.Init();
</script>
<?php
$Usua_add->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$Usua_add->Page_Terminate();
?>
