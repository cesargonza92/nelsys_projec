<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "OStoinfo.php" ?>
<?php include_once "Usuainfo.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$OSto_edit = NULL; // Initialize page object first

class cOSto_edit extends cOSto {

	// Page ID
	var $PageID = 'edit';

	// Project ID
	var $ProjectID = "{04439FF7-B43F-460F-8514-F71C8FF9E679}";

	// Table name
	var $TableName = 'OSto';

	// Page object name
	var $PageObjName = 'OSto_edit';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (OSto)
		if (!isset($GLOBALS["OSto"]) || get_class($GLOBALS["OSto"]) == "cOSto") {
			$GLOBALS["OSto"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["OSto"];
		}

		// Table object (Usua)
		if (!isset($GLOBALS['Usua'])) $GLOBALS['Usua'] = new cUsua();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'edit', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'OSto', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (Usua)
		if (!isset($UserTable)) {
			$UserTable = new cUsua();
			$UserTableConn = Conn($UserTable->DBID);
		}
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanEdit()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage($Language->Phrase("NoPermission")); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("OStolist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}

		// Create form object
		$objForm = new cFormObj();
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action
		$this->OstCodi->Visible = !$this->IsAdd() && !$this->IsCopy() && !$this->IsGridAdd();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Process auto fill
		if (@$_POST["ajax"] == "autofill") {
			$results = $this->GetAutoFill(@$_POST["name"], @$_POST["q"]);
			if ($results) {

				// Clean output buffer
				if (!EW_DEBUG_ENABLED && ob_get_length())
					ob_end_clean();
				echo $results;
				$this->Page_Terminate();
				exit();
			}
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $OSto;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($OSto);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $FormClassName = "form-horizontal ewForm ewEditForm";
	var $DbMasterFilter;
	var $DbDetailFilter;

	// 
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError;

		// Load key from QueryString
		if (@$_GET["OstCodi"] <> "") {
			$this->OstCodi->setQueryStringValue($_GET["OstCodi"]);
		}

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Process form if post back
		if (@$_POST["a_edit"] <> "") {
			$this->CurrentAction = $_POST["a_edit"]; // Get action code
			$this->LoadFormValues(); // Get form values
		} else {
			$this->CurrentAction = "I"; // Default action is display
		}

		// Check if valid key
		if ($this->OstCodi->CurrentValue == "")
			$this->Page_Terminate("OStolist.php"); // Invalid key, return to list

		// Validate form if post back
		if (@$_POST["a_edit"] <> "") {
			if (!$this->ValidateForm()) {
				$this->CurrentAction = ""; // Form error, reset action
				$this->setFailureMessage($gsFormError);
				$this->EventCancelled = TRUE; // Event cancelled
				$this->RestoreFormValues();
			}
		}
		switch ($this->CurrentAction) {
			case "I": // Get a record to display
				if (!$this->LoadRow()) { // Load record based on key
					if ($this->getFailureMessage() == "") $this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
					$this->Page_Terminate("OStolist.php"); // No matching record, return to list
				}
				break;
			Case "U": // Update
				$sReturnUrl = $this->getReturnUrl();
				$this->SendEmail = TRUE; // Send email on update success
				if ($this->EditRow()) { // Update record based on key
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage($Language->Phrase("UpdateSuccess")); // Update success
					$this->Page_Terminate($sReturnUrl); // Return to caller
				} elseif ($this->getFailureMessage() == $Language->Phrase("NoRecord")) {
					$this->Page_Terminate($sReturnUrl); // Return to caller
				} else {
					$this->EventCancelled = TRUE; // Event cancelled
					$this->RestoreFormValues(); // Restore form values if update failed
				}
		}

		// Render the record
		if ($this->CurrentAction == "F") { // Confirm page
			$this->RowType = EW_ROWTYPE_VIEW; // Render as View
		} else {
			$this->RowType = EW_ROWTYPE_EDIT; // Render as Edit
		}
		$this->ResetAttrs();
		$this->RenderRow();
	}

	// Set up starting record parameters
	function SetUpStartRec() {
		if ($this->DisplayRecs == 0)
			return;
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET[EW_TABLE_START_REC] <> "") { // Check for "start" parameter
				$this->StartRec = $_GET[EW_TABLE_START_REC];
				$this->setStartRecordNumber($this->StartRec);
			} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
				$PageNo = $_GET[EW_TABLE_PAGE_NO];
				if (is_numeric($PageNo)) {
					$this->StartRec = ($PageNo-1)*$this->DisplayRecs+1;
					if ($this->StartRec <= 0) {
						$this->StartRec = 1;
					} elseif ($this->StartRec >= intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1) {
						$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1;
					}
					$this->setStartRecordNumber($this->StartRec);
				}
			}
		}
		$this->StartRec = $this->getStartRecordNumber();

		// Check if correct start record counter
		if (!is_numeric($this->StartRec) || $this->StartRec == "") { // Avoid invalid start record counter
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} elseif (intval($this->StartRec) > intval($this->TotalRecs)) { // Avoid starting record > total records
			$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to last page first record
			$this->setStartRecordNumber($this->StartRec);
		} elseif (($this->StartRec-1) % $this->DisplayRecs <> 0) {
			$this->StartRec = intval(($this->StartRec-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to page boundary
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Get upload files
	function GetUploadFiles() {
		global $objForm, $Language;

		// Get upload data
	}

	// Load form values
	function LoadFormValues() {

		// Load from form
		global $objForm;
		if (!$this->OstCodi->FldIsDetailKey)
			$this->OstCodi->setFormValue($objForm->GetValue("x_OstCodi"));
		if (!$this->ProCodi->FldIsDetailKey) {
			$this->ProCodi->setFormValue($objForm->GetValue("x_ProCodi"));
		}
		if (!$this->OstSPro->FldIsDetailKey) {
			$this->OstSPro->setFormValue($objForm->GetValue("x_OstSPro"));
		}
		if (!$this->OstFIng->FldIsDetailKey) {
			$this->OstFIng->setFormValue($objForm->GetValue("x_OstFIng"));
			$this->OstFIng->CurrentValue = ew_UnFormatDateTime($this->OstFIng->CurrentValue, 7);
		}
		if (!$this->OstFSal->FldIsDetailKey) {
			$this->OstFSal->setFormValue($objForm->GetValue("x_OstFSal"));
			$this->OstFSal->CurrentValue = ew_UnFormatDateTime($this->OstFSal->CurrentValue, 7);
		}
		if (!$this->OstEsta->FldIsDetailKey) {
			$this->OstEsta->setFormValue($objForm->GetValue("x_OstEsta"));
		}
	}

	// Restore form values
	function RestoreFormValues() {
		global $objForm;
		$this->LoadRow();
		$this->OstCodi->CurrentValue = $this->OstCodi->FormValue;
		$this->ProCodi->CurrentValue = $this->ProCodi->FormValue;
		$this->OstSPro->CurrentValue = $this->OstSPro->FormValue;
		$this->OstFIng->CurrentValue = $this->OstFIng->FormValue;
		$this->OstFIng->CurrentValue = ew_UnFormatDateTime($this->OstFIng->CurrentValue, 7);
		$this->OstFSal->CurrentValue = $this->OstFSal->FormValue;
		$this->OstFSal->CurrentValue = ew_UnFormatDateTime($this->OstFSal->CurrentValue, 7);
		$this->OstEsta->CurrentValue = $this->OstEsta->FormValue;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->OstCodi->setDbValue($rs->fields('OstCodi'));
		$this->ProCodi->setDbValue($rs->fields('ProCodi'));
		$this->OstSPro->setDbValue($rs->fields('OstSPro'));
		$this->OstFIng->setDbValue($rs->fields('OstFIng'));
		$this->OstFSal->setDbValue($rs->fields('OstFSal'));
		$this->OstEsta->setDbValue($rs->fields('OstEsta'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->OstCodi->DbValue = $row['OstCodi'];
		$this->ProCodi->DbValue = $row['ProCodi'];
		$this->OstSPro->DbValue = $row['OstSPro'];
		$this->OstFIng->DbValue = $row['OstFIng'];
		$this->OstFSal->DbValue = $row['OstFSal'];
		$this->OstEsta->DbValue = $row['OstEsta'];
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		// Call Row_Rendering event

		$this->Row_Rendering();

		// Common render codes for all row types
		// OstCodi
		// ProCodi
		// OstSPro
		// OstFIng
		// OstFSal
		// OstEsta

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// OstCodi
		$this->OstCodi->ViewValue = $this->OstCodi->CurrentValue;
		$this->OstCodi->ViewCustomAttributes = "";

		// ProCodi
		if (strval($this->ProCodi->CurrentValue) <> "") {
			$sFilterWrk = "\"ProdCodi\"" . ew_SearchString("=", $this->ProCodi->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT \"ProdCodi\", \"CprCodi\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"Prod\"";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->ProCodi, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->ProCodi->ViewValue = $this->ProCodi->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->ProCodi->ViewValue = $this->ProCodi->CurrentValue;
			}
		} else {
			$this->ProCodi->ViewValue = NULL;
		}
		$this->ProCodi->ViewCustomAttributes = "";

		// OstSPro
		$this->OstSPro->ViewValue = $this->OstSPro->CurrentValue;
		$this->OstSPro->ViewCustomAttributes = "";

		// OstFIng
		$this->OstFIng->ViewValue = $this->OstFIng->CurrentValue;
		$this->OstFIng->ViewValue = ew_FormatDateTime($this->OstFIng->ViewValue, 7);
		$this->OstFIng->ViewCustomAttributes = "";

		// OstFSal
		$this->OstFSal->ViewValue = $this->OstFSal->CurrentValue;
		$this->OstFSal->ViewValue = ew_FormatDateTime($this->OstFSal->ViewValue, 7);
		$this->OstFSal->ViewCustomAttributes = "";

		// OstEsta
		$this->OstEsta->ViewValue = $this->OstEsta->CurrentValue;
		$this->OstEsta->ViewCustomAttributes = "";

			// OstCodi
			$this->OstCodi->LinkCustomAttributes = "";
			$this->OstCodi->HrefValue = "";
			$this->OstCodi->TooltipValue = "";

			// ProCodi
			$this->ProCodi->LinkCustomAttributes = "";
			$this->ProCodi->HrefValue = "";
			$this->ProCodi->TooltipValue = "";

			// OstSPro
			$this->OstSPro->LinkCustomAttributes = "";
			$this->OstSPro->HrefValue = "";
			$this->OstSPro->TooltipValue = "";

			// OstFIng
			$this->OstFIng->LinkCustomAttributes = "";
			$this->OstFIng->HrefValue = "";
			$this->OstFIng->TooltipValue = "";

			// OstFSal
			$this->OstFSal->LinkCustomAttributes = "";
			$this->OstFSal->HrefValue = "";
			$this->OstFSal->TooltipValue = "";

			// OstEsta
			$this->OstEsta->LinkCustomAttributes = "";
			$this->OstEsta->HrefValue = "";
			$this->OstEsta->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_EDIT) { // Edit row

			// OstCodi
			$this->OstCodi->EditAttrs["class"] = "form-control";
			$this->OstCodi->EditCustomAttributes = "";
			$this->OstCodi->EditValue = $this->OstCodi->CurrentValue;
			$this->OstCodi->ViewCustomAttributes = "";

			// ProCodi
			$this->ProCodi->EditAttrs["class"] = "form-control";
			$this->ProCodi->EditCustomAttributes = "";
			if (trim(strval($this->ProCodi->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "\"ProdCodi\"" . ew_SearchString("=", $this->ProCodi->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT \"ProdCodi\", \"CprCodi\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\", '' AS \"SelectFilterFld\", '' AS \"SelectFilterFld2\", '' AS \"SelectFilterFld3\", '' AS \"SelectFilterFld4\" FROM \"public\".\"Prod\"";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->ProCodi, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->ProCodi->EditValue = $arwrk;

			// OstSPro
			$this->OstSPro->EditAttrs["class"] = "form-control";
			$this->OstSPro->EditCustomAttributes = "";
			$this->OstSPro->EditValue = ew_HtmlEncode($this->OstSPro->CurrentValue);
			$this->OstSPro->PlaceHolder = ew_RemoveHtml($this->OstSPro->FldCaption());

			// OstFIng
			$this->OstFIng->EditAttrs["class"] = "form-control";
			$this->OstFIng->EditCustomAttributes = "";
			$this->OstFIng->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->OstFIng->CurrentValue, 7));
			$this->OstFIng->PlaceHolder = ew_RemoveHtml($this->OstFIng->FldCaption());

			// OstFSal
			$this->OstFSal->EditAttrs["class"] = "form-control";
			$this->OstFSal->EditCustomAttributes = "";
			$this->OstFSal->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->OstFSal->CurrentValue, 7));
			$this->OstFSal->PlaceHolder = ew_RemoveHtml($this->OstFSal->FldCaption());

			// OstEsta
			$this->OstEsta->EditAttrs["class"] = "form-control";
			$this->OstEsta->EditCustomAttributes = "";
			$this->OstEsta->EditValue = ew_HtmlEncode($this->OstEsta->CurrentValue);
			$this->OstEsta->PlaceHolder = ew_RemoveHtml($this->OstEsta->FldCaption());

			// Edit refer script
			// OstCodi

			$this->OstCodi->HrefValue = "";

			// ProCodi
			$this->ProCodi->HrefValue = "";

			// OstSPro
			$this->OstSPro->HrefValue = "";

			// OstFIng
			$this->OstFIng->HrefValue = "";

			// OstFSal
			$this->OstFSal->HrefValue = "";

			// OstEsta
			$this->OstEsta->HrefValue = "";
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate form
	function ValidateForm() {
		global $Language, $gsFormError;

		// Initialize form error message
		$gsFormError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return ($gsFormError == "");
		if (!$this->ProCodi->FldIsDetailKey && !is_null($this->ProCodi->FormValue) && $this->ProCodi->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->ProCodi->FldCaption(), $this->ProCodi->ReqErrMsg));
		}
		if (!$this->OstSPro->FldIsDetailKey && !is_null($this->OstSPro->FormValue) && $this->OstSPro->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->OstSPro->FldCaption(), $this->OstSPro->ReqErrMsg));
		}
		if (!ew_CheckInteger($this->OstSPro->FormValue)) {
			ew_AddMessage($gsFormError, $this->OstSPro->FldErrMsg());
		}
		if (!$this->OstFIng->FldIsDetailKey && !is_null($this->OstFIng->FormValue) && $this->OstFIng->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->OstFIng->FldCaption(), $this->OstFIng->ReqErrMsg));
		}
		if (!ew_CheckEuroDate($this->OstFIng->FormValue)) {
			ew_AddMessage($gsFormError, $this->OstFIng->FldErrMsg());
		}
		if (!ew_CheckEuroDate($this->OstFSal->FormValue)) {
			ew_AddMessage($gsFormError, $this->OstFSal->FldErrMsg());
		}
		if (!$this->OstEsta->FldIsDetailKey && !is_null($this->OstEsta->FormValue) && $this->OstEsta->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->OstEsta->FldCaption(), $this->OstEsta->ReqErrMsg));
		}

		// Return validate result
		$ValidateForm = ($gsFormError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateForm = $ValidateForm && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsFormError, $sFormCustomError);
		}
		return $ValidateForm;
	}

	// Update record based on key values
	function EditRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$conn = &$this->Connection();
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE)
			return FALSE;
		if ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record message
			$EditRow = FALSE; // Update Failed
		} else {

			// Save old values
			$rsold = &$rs->fields;
			$this->LoadDbValues($rsold);
			$rsnew = array();

			// ProCodi
			$this->ProCodi->SetDbValueDef($rsnew, $this->ProCodi->CurrentValue, 0, $this->ProCodi->ReadOnly);

			// OstSPro
			$this->OstSPro->SetDbValueDef($rsnew, $this->OstSPro->CurrentValue, 0, $this->OstSPro->ReadOnly);

			// OstFIng
			$this->OstFIng->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->OstFIng->CurrentValue, 7), ew_CurrentDate(), $this->OstFIng->ReadOnly);

			// OstFSal
			$this->OstFSal->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->OstFSal->CurrentValue, 7), NULL, $this->OstFSal->ReadOnly);

			// OstEsta
			$this->OstEsta->SetDbValueDef($rsnew, $this->OstEsta->CurrentValue, "", $this->OstEsta->ReadOnly);

			// Call Row Updating event
			$bUpdateRow = $this->Row_Updating($rsold, $rsnew);
			if ($bUpdateRow) {
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				if (count($rsnew) > 0)
					$EditRow = $this->Update($rsnew, "", $rsold);
				else
					$EditRow = TRUE; // No field to update
				$conn->raiseErrorFn = '';
				if ($EditRow) {
				}
			} else {
				if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

					// Use the message, do nothing
				} elseif ($this->CancelMessage <> "") {
					$this->setFailureMessage($this->CancelMessage);
					$this->CancelMessage = "";
				} else {
					$this->setFailureMessage($Language->Phrase("UpdateCancelled"));
				}
				$EditRow = FALSE;
			}
		}

		// Call Row_Updated event
		if ($EditRow)
			$this->Row_Updated($rsold, $rsnew);
		$rs->Close();
		return $EditRow;
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, "OStolist.php", "", $this->TableVar, TRUE);
		$PageId = "edit";
		$Breadcrumb->Add("edit", $PageId, $url);
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($OSto_edit)) $OSto_edit = new cOSto_edit();

// Page init
$OSto_edit->Page_Init();

// Page main
$OSto_edit->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$OSto_edit->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "edit";
var CurrentForm = fOStoedit = new ew_Form("fOStoedit", "edit");

// Validate form
fOStoedit.Validate = function() {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.GetForm(), $fobj = $(fobj);
	if ($fobj.find("#a_confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.FormKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = $fobj.find("#a_list").val() == "gridinsert";
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
			elm = this.GetElements("x" + infix + "_ProCodi");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $OSto->ProCodi->FldCaption(), $OSto->ProCodi->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_OstSPro");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $OSto->OstSPro->FldCaption(), $OSto->OstSPro->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_OstSPro");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($OSto->OstSPro->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_OstFIng");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $OSto->OstFIng->FldCaption(), $OSto->OstFIng->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_OstFIng");
			if (elm && !ew_CheckEuroDate(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($OSto->OstFIng->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_OstFSal");
			if (elm && !ew_CheckEuroDate(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($OSto->OstFSal->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_OstEsta");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $OSto->OstEsta->FldCaption(), $OSto->OstEsta->ReqErrMsg)) ?>");

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
	}

	// Process detail forms
	var dfs = $fobj.find("input[name='detailpage']").get();
	for (var i = 0; i < dfs.length; i++) {
		var df = dfs[i], val = df.value;
		if (val && ewForms[val])
			if (!ewForms[val].Validate())
				return false;
	}
	return true;
}

// Form_CustomValidate event
fOStoedit.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fOStoedit.ValidateRequired = true;
<?php } else { ?>
fOStoedit.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
fOStoedit.Lists["x_ProCodi"] = {"LinkField":"x_ProdCodi","Ajax":true,"AutoFill":false,"DisplayFields":["x_CprCodi","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};

// Form object for search
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php $OSto_edit->ShowPageHeader(); ?>
<?php
$OSto_edit->ShowMessage();
?>
<form name="fOStoedit" id="fOStoedit" class="<?php echo $OSto_edit->FormClassName ?>" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($OSto_edit->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $OSto_edit->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="OSto">
<input type="hidden" name="a_edit" id="a_edit" value="U">
<?php if ($OSto->CurrentAction == "F") { // Confirm page ?>
<input type="hidden" name="a_confirm" id="a_confirm" value="F">
<?php } ?>
<div>
<?php if ($OSto->OstCodi->Visible) { // OstCodi ?>
	<div id="r_OstCodi" class="form-group">
		<label id="elh_OSto_OstCodi" class="col-sm-2 control-label ewLabel"><?php echo $OSto->OstCodi->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $OSto->OstCodi->CellAttributes() ?>>
<?php if ($OSto->CurrentAction <> "F") { ?>
<span id="el_OSto_OstCodi">
<span<?php echo $OSto->OstCodi->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $OSto->OstCodi->EditValue ?></p></span>
</span>
<input type="hidden" data-table="OSto" data-field="x_OstCodi" name="x_OstCodi" id="x_OstCodi" value="<?php echo ew_HtmlEncode($OSto->OstCodi->CurrentValue) ?>">
<?php } else { ?>
<span id="el_OSto_OstCodi">
<span<?php echo $OSto->OstCodi->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $OSto->OstCodi->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="OSto" data-field="x_OstCodi" name="x_OstCodi" id="x_OstCodi" value="<?php echo ew_HtmlEncode($OSto->OstCodi->FormValue) ?>">
<?php } ?>
<?php echo $OSto->OstCodi->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($OSto->ProCodi->Visible) { // ProCodi ?>
	<div id="r_ProCodi" class="form-group">
		<label id="elh_OSto_ProCodi" for="x_ProCodi" class="col-sm-2 control-label ewLabel"><?php echo $OSto->ProCodi->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $OSto->ProCodi->CellAttributes() ?>>
<?php if ($OSto->CurrentAction <> "F") { ?>
<span id="el_OSto_ProCodi">
<select data-table="OSto" data-field="x_ProCodi" data-value-separator="<?php echo ew_HtmlEncode(is_array($OSto->ProCodi->DisplayValueSeparator) ? json_encode($OSto->ProCodi->DisplayValueSeparator) : $OSto->ProCodi->DisplayValueSeparator) ?>" id="x_ProCodi" name="x_ProCodi"<?php echo $OSto->ProCodi->EditAttributes() ?>>
<?php
if (is_array($OSto->ProCodi->EditValue)) {
	$arwrk = $OSto->ProCodi->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($OSto->ProCodi->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $OSto->ProCodi->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($OSto->ProCodi->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($OSto->ProCodi->CurrentValue) ?>" selected><?php echo $OSto->ProCodi->CurrentValue ?></option>
<?php
    }
}
?>
</select>
<?php
$sSqlWrk = "SELECT \"ProdCodi\", \"CprCodi\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"Prod\"";
$sWhereWrk = "";
$OSto->ProCodi->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$OSto->ProCodi->LookupFilters += array("f0" => "\"ProdCodi\" = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$OSto->Lookup_Selecting($OSto->ProCodi, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $OSto->ProCodi->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x_ProCodi" id="s_x_ProCodi" value="<?php echo $OSto->ProCodi->LookupFilterQuery() ?>">
</span>
<?php } else { ?>
<span id="el_OSto_ProCodi">
<span<?php echo $OSto->ProCodi->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $OSto->ProCodi->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="OSto" data-field="x_ProCodi" name="x_ProCodi" id="x_ProCodi" value="<?php echo ew_HtmlEncode($OSto->ProCodi->FormValue) ?>">
<?php } ?>
<?php echo $OSto->ProCodi->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($OSto->OstSPro->Visible) { // OstSPro ?>
	<div id="r_OstSPro" class="form-group">
		<label id="elh_OSto_OstSPro" for="x_OstSPro" class="col-sm-2 control-label ewLabel"><?php echo $OSto->OstSPro->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $OSto->OstSPro->CellAttributes() ?>>
<?php if ($OSto->CurrentAction <> "F") { ?>
<span id="el_OSto_OstSPro">
<input type="text" data-table="OSto" data-field="x_OstSPro" name="x_OstSPro" id="x_OstSPro" size="30" placeholder="<?php echo ew_HtmlEncode($OSto->OstSPro->getPlaceHolder()) ?>" value="<?php echo $OSto->OstSPro->EditValue ?>"<?php echo $OSto->OstSPro->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_OSto_OstSPro">
<span<?php echo $OSto->OstSPro->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $OSto->OstSPro->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="OSto" data-field="x_OstSPro" name="x_OstSPro" id="x_OstSPro" value="<?php echo ew_HtmlEncode($OSto->OstSPro->FormValue) ?>">
<?php } ?>
<?php echo $OSto->OstSPro->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($OSto->OstFIng->Visible) { // OstFIng ?>
	<div id="r_OstFIng" class="form-group">
		<label id="elh_OSto_OstFIng" for="x_OstFIng" class="col-sm-2 control-label ewLabel"><?php echo $OSto->OstFIng->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $OSto->OstFIng->CellAttributes() ?>>
<?php if ($OSto->CurrentAction <> "F") { ?>
<span id="el_OSto_OstFIng">
<input type="text" data-table="OSto" data-field="x_OstFIng" data-format="7" name="x_OstFIng" id="x_OstFIng" placeholder="<?php echo ew_HtmlEncode($OSto->OstFIng->getPlaceHolder()) ?>" value="<?php echo $OSto->OstFIng->EditValue ?>"<?php echo $OSto->OstFIng->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_OSto_OstFIng">
<span<?php echo $OSto->OstFIng->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $OSto->OstFIng->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="OSto" data-field="x_OstFIng" name="x_OstFIng" id="x_OstFIng" value="<?php echo ew_HtmlEncode($OSto->OstFIng->FormValue) ?>">
<?php } ?>
<?php echo $OSto->OstFIng->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($OSto->OstFSal->Visible) { // OstFSal ?>
	<div id="r_OstFSal" class="form-group">
		<label id="elh_OSto_OstFSal" for="x_OstFSal" class="col-sm-2 control-label ewLabel"><?php echo $OSto->OstFSal->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $OSto->OstFSal->CellAttributes() ?>>
<?php if ($OSto->CurrentAction <> "F") { ?>
<span id="el_OSto_OstFSal">
<input type="text" data-table="OSto" data-field="x_OstFSal" data-format="7" name="x_OstFSal" id="x_OstFSal" placeholder="<?php echo ew_HtmlEncode($OSto->OstFSal->getPlaceHolder()) ?>" value="<?php echo $OSto->OstFSal->EditValue ?>"<?php echo $OSto->OstFSal->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_OSto_OstFSal">
<span<?php echo $OSto->OstFSal->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $OSto->OstFSal->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="OSto" data-field="x_OstFSal" name="x_OstFSal" id="x_OstFSal" value="<?php echo ew_HtmlEncode($OSto->OstFSal->FormValue) ?>">
<?php } ?>
<?php echo $OSto->OstFSal->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($OSto->OstEsta->Visible) { // OstEsta ?>
	<div id="r_OstEsta" class="form-group">
		<label id="elh_OSto_OstEsta" for="x_OstEsta" class="col-sm-2 control-label ewLabel"><?php echo $OSto->OstEsta->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $OSto->OstEsta->CellAttributes() ?>>
<?php if ($OSto->CurrentAction <> "F") { ?>
<span id="el_OSto_OstEsta">
<input type="text" data-table="OSto" data-field="x_OstEsta" name="x_OstEsta" id="x_OstEsta" size="30" maxlength="1" placeholder="<?php echo ew_HtmlEncode($OSto->OstEsta->getPlaceHolder()) ?>" value="<?php echo $OSto->OstEsta->EditValue ?>"<?php echo $OSto->OstEsta->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_OSto_OstEsta">
<span<?php echo $OSto->OstEsta->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $OSto->OstEsta->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="OSto" data-field="x_OstEsta" name="x_OstEsta" id="x_OstEsta" value="<?php echo ew_HtmlEncode($OSto->OstEsta->FormValue) ?>">
<?php } ?>
<?php echo $OSto->OstEsta->CustomMsg ?></div></div>
	</div>
<?php } ?>
</div>
<div class="form-group">
	<div class="col-sm-offset-2 col-sm-10">
<?php if ($OSto->CurrentAction <> "F") { // Confirm page ?>
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit" onclick="this.form.a_edit.value='F';"><?php echo $Language->Phrase("SaveBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="button" data-href="<?php echo $OSto_edit->getReturnUrl() ?>"><?php echo $Language->Phrase("CancelBtn") ?></button>
<?php } else { ?>
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("ConfirmBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="submit" onclick="this.form.a_edit.value='X';"><?php echo $Language->Phrase("CancelBtn") ?></button>
<?php } ?>
	</div>
</div>
</form>
<script type="text/javascript">
fOStoedit.Init();
</script>
<?php
$OSto_edit->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$OSto_edit->Page_Terminate();
?>
