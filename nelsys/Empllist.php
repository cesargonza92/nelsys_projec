<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "Emplinfo.php" ?>
<?php include_once "Usuainfo.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$Empl_list = NULL; // Initialize page object first

class cEmpl_list extends cEmpl {

	// Page ID
	var $PageID = 'list';

	// Project ID
	var $ProjectID = "{04439FF7-B43F-460F-8514-F71C8FF9E679}";

	// Table name
	var $TableName = 'Empl';

	// Page object name
	var $PageObjName = 'Empl_list';

	// Grid form hidden field names
	var $FormName = 'fEmpllist';
	var $FormActionName = 'k_action';
	var $FormKeyName = 'k_key';
	var $FormOldKeyName = 'k_oldkey';
	var $FormBlankRowName = 'k_blankrow';
	var $FormKeyCountName = 'key_count';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Page URLs
	var $AddUrl;
	var $EditUrl;
	var $CopyUrl;
	var $DeleteUrl;
	var $ViewUrl;
	var $ListUrl;

	// Export URLs
	var $ExportPrintUrl;
	var $ExportHtmlUrl;
	var $ExportExcelUrl;
	var $ExportWordUrl;
	var $ExportXmlUrl;
	var $ExportCsvUrl;
	var $ExportPdfUrl;

	// Custom export
	var $ExportExcelCustom = FALSE;
	var $ExportWordCustom = FALSE;
	var $ExportPdfCustom = FALSE;
	var $ExportEmailCustom = FALSE;

	// Update URLs
	var $InlineAddUrl;
	var $InlineCopyUrl;
	var $InlineEditUrl;
	var $GridAddUrl;
	var $GridEditUrl;
	var $MultiDeleteUrl;
	var $MultiUpdateUrl;

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (Empl)
		if (!isset($GLOBALS["Empl"]) || get_class($GLOBALS["Empl"]) == "cEmpl") {
			$GLOBALS["Empl"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["Empl"];
		}

		// Initialize URLs
		$this->ExportPrintUrl = $this->PageUrl() . "export=print";
		$this->ExportExcelUrl = $this->PageUrl() . "export=excel";
		$this->ExportWordUrl = $this->PageUrl() . "export=word";
		$this->ExportHtmlUrl = $this->PageUrl() . "export=html";
		$this->ExportXmlUrl = $this->PageUrl() . "export=xml";
		$this->ExportCsvUrl = $this->PageUrl() . "export=csv";
		$this->ExportPdfUrl = $this->PageUrl() . "export=pdf";
		$this->AddUrl = "Empladd.php";
		$this->InlineAddUrl = $this->PageUrl() . "a=add";
		$this->GridAddUrl = $this->PageUrl() . "a=gridadd";
		$this->GridEditUrl = $this->PageUrl() . "a=gridedit";
		$this->MultiDeleteUrl = "Empldelete.php";
		$this->MultiUpdateUrl = "Emplupdate.php";

		// Table object (Usua)
		if (!isset($GLOBALS['Usua'])) $GLOBALS['Usua'] = new cUsua();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'list', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'Empl', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (Usua)
		if (!isset($UserTable)) {
			$UserTable = new cUsua();
			$UserTableConn = Conn($UserTable->DBID);
		}

		// List options
		$this->ListOptions = new cListOptions();
		$this->ListOptions->TableVar = $this->TableVar;

		// Export options
		$this->ExportOptions = new cListOptions();
		$this->ExportOptions->Tag = "div";
		$this->ExportOptions->TagClassName = "ewExportOption";

		// Other options
		$this->OtherOptions['addedit'] = new cListOptions();
		$this->OtherOptions['addedit']->Tag = "div";
		$this->OtherOptions['addedit']->TagClassName = "ewAddEditOption";
		$this->OtherOptions['detail'] = new cListOptions();
		$this->OtherOptions['detail']->Tag = "div";
		$this->OtherOptions['detail']->TagClassName = "ewDetailOption";
		$this->OtherOptions['action'] = new cListOptions();
		$this->OtherOptions['action']->Tag = "div";
		$this->OtherOptions['action']->TagClassName = "ewActionOption";

		// Filter options
		$this->FilterOptions = new cListOptions();
		$this->FilterOptions->Tag = "div";
		$this->FilterOptions->TagClassName = "ewFilterOption fEmpllistsrch";

		// List actions
		$this->ListActions = new cListActions();
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanList()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage($Language->Phrase("NoPermission")); // Set no permission
			$this->Page_Terminate(ew_GetUrl("index.php"));
		}

		// Create form object
		$objForm = new cFormObj();
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action

		// Get grid add count
		$gridaddcnt = @$_GET[EW_TABLE_GRID_ADD_ROW_COUNT];
		if (is_numeric($gridaddcnt) && $gridaddcnt > 0)
			$this->GridAddRowCount = $gridaddcnt;

		// Set up list options
		$this->SetupListOptions();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Process auto fill
		if (@$_POST["ajax"] == "autofill") {
			$results = $this->GetAutoFill(@$_POST["name"], @$_POST["q"]);
			if ($results) {

				// Clean output buffer
				if (!EW_DEBUG_ENABLED && ob_get_length())
					ob_end_clean();
				echo $results;
				$this->Page_Terminate();
				exit();
			}
		}

		// Create Token
		$this->CreateToken();

		// Setup other options
		$this->SetupOtherOptions();

		// Set up custom action (compatible with old version)
		foreach ($this->CustomActions as $name => $action)
			$this->ListActions->Add($name, $action);

		// Show checkbox column if multiple action
		foreach ($this->ListActions->Items as $listaction) {
			if ($listaction->Select == EW_ACTION_MULTIPLE && $listaction->Allow) {
				$this->ListOptions->Items["checkbox"]->Visible = TRUE;
				break;
			}
		}
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $Empl;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($Empl);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}

	// Class variables
	var $ListOptions; // List options
	var $ExportOptions; // Export options
	var $SearchOptions; // Search options
	var $OtherOptions = array(); // Other options
	var $FilterOptions; // Filter options
	var $ListActions; // List actions
	var $SelectedCount = 0;
	var $SelectedIndex = 0;
	var $DisplayRecs = 20;
	var $StartRec;
	var $StopRec;
	var $TotalRecs = 0;
	var $RecRange = 10;
	var $Pager;
	var $DefaultSearchWhere = ""; // Default search WHERE clause
	var $SearchWhere = ""; // Search WHERE clause
	var $RecCnt = 0; // Record count
	var $EditRowCnt;
	var $StartRowCnt = 1;
	var $RowCnt = 0;
	var $Attrs = array(); // Row attributes and cell attributes
	var $RowIndex = 0; // Row index
	var $KeyCount = 0; // Key count
	var $RowAction = ""; // Row action
	var $RowOldKey = ""; // Row old key (for copy)
	var $RecPerRow = 0;
	var $MultiColumnClass;
	var $MultiColumnEditClass = "col-sm-12";
	var $MultiColumnCnt = 12;
	var $MultiColumnEditCnt = 12;
	var $GridCnt = 0;
	var $ColCnt = 0;
	var $DbMasterFilter = ""; // Master filter
	var $DbDetailFilter = ""; // Detail filter
	var $MasterRecordExists;	
	var $MultiSelectKey;
	var $Command;
	var $RestoreSearch = FALSE;
	var $DetailPages;
	var $Recordset;
	var $OldRecordset;

	//
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError, $gsSearchError, $Security;

		// Search filters
		$sSrchAdvanced = ""; // Advanced search filter
		$sSrchBasic = ""; // Basic search filter
		$sFilter = "";

		// Get command
		$this->Command = strtolower(@$_GET["cmd"]);
		if ($this->IsPageRequest()) { // Validate request

			// Process list action first
			if ($this->ProcessListAction()) // Ajax request
				$this->Page_Terminate();

			// Handle reset command
			$this->ResetCmd();

			// Set up Breadcrumb
			if ($this->Export == "")
				$this->SetupBreadcrumb();

			// Check QueryString parameters
			if (@$_GET["a"] <> "") {
				$this->CurrentAction = $_GET["a"];

				// Clear inline mode
				if ($this->CurrentAction == "cancel")
					$this->ClearInlineMode();

				// Switch to grid edit mode
				if ($this->CurrentAction == "gridedit")
					$this->GridEditMode();

				// Switch to grid add mode
				if ($this->CurrentAction == "gridadd")
					$this->GridAddMode();
			} else {
				if (@$_POST["a_list"] <> "") {
					$this->CurrentAction = $_POST["a_list"]; // Get action

					// Grid Update
					if (($this->CurrentAction == "gridupdate" || $this->CurrentAction == "gridoverwrite") && @$_SESSION[EW_SESSION_INLINE_MODE] == "gridedit") {
						if ($this->ValidateGridForm()) {
							$bGridUpdate = $this->GridUpdate();
						} else {
							$bGridUpdate = FALSE;
							$this->setFailureMessage($gsFormError);
						}
						if (!$bGridUpdate) {
							$this->EventCancelled = TRUE;
							$this->CurrentAction = "gridedit"; // Stay in Grid Edit mode
						}
					}

					// Grid Insert
					if ($this->CurrentAction == "gridinsert" && @$_SESSION[EW_SESSION_INLINE_MODE] == "gridadd") {
						if ($this->ValidateGridForm()) {
							$bGridInsert = $this->GridInsert();
						} else {
							$bGridInsert = FALSE;
							$this->setFailureMessage($gsFormError);
						}
						if (!$bGridInsert) {
							$this->EventCancelled = TRUE;
							$this->CurrentAction = "gridadd"; // Stay in Grid Add mode
						}
					}
				}
			}

			// Hide list options
			if ($this->Export <> "") {
				$this->ListOptions->HideAllOptions(array("sequence"));
				$this->ListOptions->UseDropDownButton = FALSE; // Disable drop down button
				$this->ListOptions->UseButtonGroup = FALSE; // Disable button group
			} elseif ($this->CurrentAction == "gridadd" || $this->CurrentAction == "gridedit") {
				$this->ListOptions->HideAllOptions();
				$this->ListOptions->UseDropDownButton = FALSE; // Disable drop down button
				$this->ListOptions->UseButtonGroup = FALSE; // Disable button group
			}

			// Hide options
			if ($this->Export <> "" || $this->CurrentAction <> "") {
				$this->ExportOptions->HideAllOptions();
				$this->FilterOptions->HideAllOptions();
			}

			// Hide other options
			if ($this->Export <> "") {
				foreach ($this->OtherOptions as &$option)
					$option->HideAllOptions();
			}

			// Show grid delete link for grid add / grid edit
			if ($this->AllowAddDeleteRow) {
				if ($this->CurrentAction == "gridadd" || $this->CurrentAction == "gridedit") {
					$item = $this->ListOptions->GetItem("griddelete");
					if ($item) $item->Visible = TRUE;
				}
			}

			// Get default search criteria
			ew_AddFilter($this->DefaultSearchWhere, $this->BasicSearchWhere(TRUE));

			// Get basic search values
			$this->LoadBasicSearchValues();

			// Restore filter list
			$this->RestoreFilterList();

			// Restore search parms from Session if not searching / reset / export
			if (($this->Export <> "" || $this->Command <> "search" && $this->Command <> "reset" && $this->Command <> "resetall") && $this->CheckSearchParms())
				$this->RestoreSearchParms();

			// Call Recordset SearchValidated event
			$this->Recordset_SearchValidated();

			// Set up sorting order
			$this->SetUpSortOrder();

			// Get basic search criteria
			if ($gsSearchError == "")
				$sSrchBasic = $this->BasicSearchWhere();
		}

		// Restore display records
		if ($this->getRecordsPerPage() <> "") {
			$this->DisplayRecs = $this->getRecordsPerPage(); // Restore from Session
		} else {
			$this->DisplayRecs = 20; // Load default
		}

		// Load Sorting Order
		$this->LoadSortOrder();

		// Load search default if no existing search criteria
		if (!$this->CheckSearchParms()) {

			// Load basic search from default
			$this->BasicSearch->LoadDefault();
			if ($this->BasicSearch->Keyword != "")
				$sSrchBasic = $this->BasicSearchWhere();
		}

		// Build search criteria
		ew_AddFilter($this->SearchWhere, $sSrchAdvanced);
		ew_AddFilter($this->SearchWhere, $sSrchBasic);

		// Call Recordset_Searching event
		$this->Recordset_Searching($this->SearchWhere);

		// Save search criteria
		if ($this->Command == "search" && !$this->RestoreSearch) {
			$this->setSearchWhere($this->SearchWhere); // Save to Session
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} else {
			$this->SearchWhere = $this->getSearchWhere();
		}

		// Build filter
		$sFilter = "";
		if (!$Security->CanList())
			$sFilter = "(0=1)"; // Filter all records
		ew_AddFilter($sFilter, $this->DbDetailFilter);
		ew_AddFilter($sFilter, $this->SearchWhere);

		// Set up filter in session
		$this->setSessionWhere($sFilter);
		$this->CurrentFilter = "";

		// Load record count first
		if (!$this->IsAddOrEdit()) {
			$bSelectLimit = $this->UseSelectLimit;
			if ($bSelectLimit) {
				$this->TotalRecs = $this->SelectRecordCount();
			} else {
				if ($this->Recordset = $this->LoadRecordset())
					$this->TotalRecs = $this->Recordset->RecordCount();
			}
		}

		// Search options
		$this->SetupSearchOptions();
	}

	//  Exit inline mode
	function ClearInlineMode() {
		$this->EmpSala->FormValue = ""; // Clear form value
		$this->LastAction = $this->CurrentAction; // Save last action
		$this->CurrentAction = ""; // Clear action
		$_SESSION[EW_SESSION_INLINE_MODE] = ""; // Clear inline mode
	}

	// Switch to Grid Add mode
	function GridAddMode() {
		$_SESSION[EW_SESSION_INLINE_MODE] = "gridadd"; // Enabled grid add
	}

	// Switch to Grid Edit mode
	function GridEditMode() {
		$_SESSION[EW_SESSION_INLINE_MODE] = "gridedit"; // Enable grid edit
	}

	// Perform update to grid
	function GridUpdate() {
		global $Language, $objForm, $gsFormError;
		$bGridUpdate = TRUE;

		// Get old recordset
		$this->CurrentFilter = $this->BuildKeyFilter();
		if ($this->CurrentFilter == "")
			$this->CurrentFilter = "0=1";
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		if ($rs = $conn->Execute($sSql)) {
			$rsold = $rs->GetRows();
			$rs->Close();
		}

		// Call Grid Updating event
		if (!$this->Grid_Updating($rsold)) {
			if ($this->getFailureMessage() == "")
				$this->setFailureMessage($Language->Phrase("GridEditCancelled")); // Set grid edit cancelled message
			return FALSE;
		}

		// Begin transaction
		$conn->BeginTrans();
		$sKey = "";

		// Update row index and get row key
		$objForm->Index = -1;
		$rowcnt = strval($objForm->GetValue($this->FormKeyCountName));
		if ($rowcnt == "" || !is_numeric($rowcnt))
			$rowcnt = 0;

		// Update all rows based on key
		for ($rowindex = 1; $rowindex <= $rowcnt; $rowindex++) {
			$objForm->Index = $rowindex;
			$rowkey = strval($objForm->GetValue($this->FormKeyName));
			$rowaction = strval($objForm->GetValue($this->FormActionName));

			// Load all values and keys
			if ($rowaction <> "insertdelete") { // Skip insert then deleted rows
				$this->LoadFormValues(); // Get form values
				if ($rowaction == "" || $rowaction == "edit" || $rowaction == "delete") {
					$bGridUpdate = $this->SetupKeyValues($rowkey); // Set up key values
				} else {
					$bGridUpdate = TRUE;
				}

				// Skip empty row
				if ($rowaction == "insert" && $this->EmptyRow()) {

					// No action required
				// Validate form and insert/update/delete record

				} elseif ($bGridUpdate) {
					if ($rowaction == "delete") {
						$this->CurrentFilter = $this->KeyFilter();
						$bGridUpdate = $this->DeleteRows(); // Delete this row
					} else if (!$this->ValidateForm()) {
						$bGridUpdate = FALSE; // Form error, reset action
						$this->setFailureMessage($gsFormError);
					} else {
						if ($rowaction == "insert") {
							$bGridUpdate = $this->AddRow(); // Insert this row
						} else {
							if ($rowkey <> "") {
								$this->SendEmail = FALSE; // Do not send email on update success
								$bGridUpdate = $this->EditRow(); // Update this row
							}
						} // End update
					}
				}
				if ($bGridUpdate) {
					if ($sKey <> "") $sKey .= ", ";
					$sKey .= $rowkey;
				} else {
					break;
				}
			}
		}
		if ($bGridUpdate) {
			$conn->CommitTrans(); // Commit transaction

			// Get new recordset
			if ($rs = $conn->Execute($sSql)) {
				$rsnew = $rs->GetRows();
				$rs->Close();
			}

			// Call Grid_Updated event
			$this->Grid_Updated($rsold, $rsnew);
			if ($this->getSuccessMessage() == "")
				$this->setSuccessMessage($Language->Phrase("UpdateSuccess")); // Set up update success message
			$this->ClearInlineMode(); // Clear inline edit mode
		} else {
			$conn->RollbackTrans(); // Rollback transaction
			if ($this->getFailureMessage() == "")
				$this->setFailureMessage($Language->Phrase("UpdateFailed")); // Set update failed message
		}
		return $bGridUpdate;
	}

	// Build filter for all keys
	function BuildKeyFilter() {
		global $objForm;
		$sWrkFilter = "";

		// Update row index and get row key
		$rowindex = 1;
		$objForm->Index = $rowindex;
		$sThisKey = strval($objForm->GetValue($this->FormKeyName));
		while ($sThisKey <> "") {
			if ($this->SetupKeyValues($sThisKey)) {
				$sFilter = $this->KeyFilter();
				if ($sWrkFilter <> "") $sWrkFilter .= " OR ";
				$sWrkFilter .= $sFilter;
			} else {
				$sWrkFilter = "0=1";
				break;
			}

			// Update row index and get row key
			$rowindex++; // Next row
			$objForm->Index = $rowindex;
			$sThisKey = strval($objForm->GetValue($this->FormKeyName));
		}
		return $sWrkFilter;
	}

	// Set up key values
	function SetupKeyValues($key) {
		$arrKeyFlds = explode($GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"], $key);
		if (count($arrKeyFlds) >= 1) {
			$this->EmpCodi->setFormValue($arrKeyFlds[0]);
			if (!is_numeric($this->EmpCodi->FormValue))
				return FALSE;
		}
		return TRUE;
	}

	// Perform Grid Add
	function GridInsert() {
		global $Language, $objForm, $gsFormError;
		$rowindex = 1;
		$bGridInsert = FALSE;
		$conn = &$this->Connection();

		// Call Grid Inserting event
		if (!$this->Grid_Inserting()) {
			if ($this->getFailureMessage() == "") {
				$this->setFailureMessage($Language->Phrase("GridAddCancelled")); // Set grid add cancelled message
			}
			return FALSE;
		}

		// Begin transaction
		$conn->BeginTrans();

		// Init key filter
		$sWrkFilter = "";
		$addcnt = 0;
		$sKey = "";

		// Get row count
		$objForm->Index = -1;
		$rowcnt = strval($objForm->GetValue($this->FormKeyCountName));
		if ($rowcnt == "" || !is_numeric($rowcnt))
			$rowcnt = 0;

		// Insert all rows
		for ($rowindex = 1; $rowindex <= $rowcnt; $rowindex++) {

			// Load current row values
			$objForm->Index = $rowindex;
			$rowaction = strval($objForm->GetValue($this->FormActionName));
			if ($rowaction <> "" && $rowaction <> "insert")
				continue; // Skip
			$this->LoadFormValues(); // Get form values
			if (!$this->EmptyRow()) {
				$addcnt++;
				$this->SendEmail = FALSE; // Do not send email on insert success

				// Validate form
				if (!$this->ValidateForm()) {
					$bGridInsert = FALSE; // Form error, reset action
					$this->setFailureMessage($gsFormError);
				} else {
					$bGridInsert = $this->AddRow($this->OldRecordset); // Insert this row
				}
				if ($bGridInsert) {
					if ($sKey <> "") $sKey .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
					$sKey .= $this->EmpCodi->CurrentValue;

					// Add filter for this record
					$sFilter = $this->KeyFilter();
					if ($sWrkFilter <> "") $sWrkFilter .= " OR ";
					$sWrkFilter .= $sFilter;
				} else {
					break;
				}
			}
		}
		if ($addcnt == 0) { // No record inserted
			$this->setFailureMessage($Language->Phrase("NoAddRecord"));
			$bGridInsert = FALSE;
		}
		if ($bGridInsert) {
			$conn->CommitTrans(); // Commit transaction

			// Get new recordset
			$this->CurrentFilter = $sWrkFilter;
			$sSql = $this->SQL();
			if ($rs = $conn->Execute($sSql)) {
				$rsnew = $rs->GetRows();
				$rs->Close();
			}

			// Call Grid_Inserted event
			$this->Grid_Inserted($rsnew);
			if ($this->getSuccessMessage() == "")
				$this->setSuccessMessage($Language->Phrase("InsertSuccess")); // Set up insert success message
			$this->ClearInlineMode(); // Clear grid add mode
		} else {
			$conn->RollbackTrans(); // Rollback transaction
			if ($this->getFailureMessage() == "") {
				$this->setFailureMessage($Language->Phrase("InsertFailed")); // Set insert failed message
			}
		}
		return $bGridInsert;
	}

	// Check if empty row
	function EmptyRow() {
		global $objForm;
		if ($objForm->HasValue("x_EmpCarg") && $objForm->HasValue("o_EmpCarg") && $this->EmpCarg->CurrentValue <> $this->EmpCarg->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_EmpFIng") && $objForm->HasValue("o_EmpFIng") && $this->EmpFIng->CurrentValue <> $this->EmpFIng->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_EmpFSal") && $objForm->HasValue("o_EmpFSal") && $this->EmpFSal->CurrentValue <> $this->EmpFSal->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_EmpSala") && $objForm->HasValue("o_EmpSala") && $this->EmpSala->CurrentValue <> $this->EmpSala->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_EmpNomb") && $objForm->HasValue("o_EmpNomb") && $this->EmpNomb->CurrentValue <> $this->EmpNomb->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_EmpApel") && $objForm->HasValue("o_EmpApel") && $this->EmpApel->CurrentValue <> $this->EmpApel->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_EmpCedu") && $objForm->HasValue("o_EmpCedu") && $this->EmpCedu->CurrentValue <> $this->EmpCedu->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_EmpTele") && $objForm->HasValue("o_EmpTele") && $this->EmpTele->CurrentValue <> $this->EmpTele->OldValue)
			return FALSE;
		return TRUE;
	}

	// Validate grid form
	function ValidateGridForm() {
		global $objForm;

		// Get row count
		$objForm->Index = -1;
		$rowcnt = strval($objForm->GetValue($this->FormKeyCountName));
		if ($rowcnt == "" || !is_numeric($rowcnt))
			$rowcnt = 0;

		// Validate all records
		for ($rowindex = 1; $rowindex <= $rowcnt; $rowindex++) {

			// Load current row values
			$objForm->Index = $rowindex;
			$rowaction = strval($objForm->GetValue($this->FormActionName));
			if ($rowaction <> "delete" && $rowaction <> "insertdelete") {
				$this->LoadFormValues(); // Get form values
				if ($rowaction == "insert" && $this->EmptyRow()) {

					// Ignore
				} else if (!$this->ValidateForm()) {
					return FALSE;
				}
			}
		}
		return TRUE;
	}

	// Get all form values of the grid
	function GetGridFormValues() {
		global $objForm;

		// Get row count
		$objForm->Index = -1;
		$rowcnt = strval($objForm->GetValue($this->FormKeyCountName));
		if ($rowcnt == "" || !is_numeric($rowcnt))
			$rowcnt = 0;
		$rows = array();

		// Loop through all records
		for ($rowindex = 1; $rowindex <= $rowcnt; $rowindex++) {

			// Load current row values
			$objForm->Index = $rowindex;
			$rowaction = strval($objForm->GetValue($this->FormActionName));
			if ($rowaction <> "delete" && $rowaction <> "insertdelete") {
				$this->LoadFormValues(); // Get form values
				if ($rowaction == "insert" && $this->EmptyRow()) {

					// Ignore
				} else {
					$rows[] = $this->GetFieldValues("FormValue"); // Return row as array
				}
			}
		}
		return $rows; // Return as array of array
	}

	// Restore form values for current row
	function RestoreCurrentRowFormValues($idx) {
		global $objForm;

		// Get row based on current index
		$objForm->Index = $idx;
		$this->LoadFormValues(); // Load form values
	}

	// Get list of filters
	function GetFilterList() {

		// Initialize
		$sFilterList = "";
		$sFilterList = ew_Concat($sFilterList, $this->EmpCodi->AdvancedSearch->ToJSON(), ","); // Field EmpCodi
		$sFilterList = ew_Concat($sFilterList, $this->EmpCarg->AdvancedSearch->ToJSON(), ","); // Field EmpCarg
		$sFilterList = ew_Concat($sFilterList, $this->EmpFIng->AdvancedSearch->ToJSON(), ","); // Field EmpFIng
		$sFilterList = ew_Concat($sFilterList, $this->EmpFSal->AdvancedSearch->ToJSON(), ","); // Field EmpFSal
		$sFilterList = ew_Concat($sFilterList, $this->EmpSala->AdvancedSearch->ToJSON(), ","); // Field EmpSala
		$sFilterList = ew_Concat($sFilterList, $this->EmpNomb->AdvancedSearch->ToJSON(), ","); // Field EmpNomb
		$sFilterList = ew_Concat($sFilterList, $this->EmpApel->AdvancedSearch->ToJSON(), ","); // Field EmpApel
		$sFilterList = ew_Concat($sFilterList, $this->EmpCedu->AdvancedSearch->ToJSON(), ","); // Field EmpCedu
		$sFilterList = ew_Concat($sFilterList, $this->EmpTele->AdvancedSearch->ToJSON(), ","); // Field EmpTele
		if ($this->BasicSearch->Keyword <> "") {
			$sWrk = "\"" . EW_TABLE_BASIC_SEARCH . "\":\"" . ew_JsEncode2($this->BasicSearch->Keyword) . "\",\"" . EW_TABLE_BASIC_SEARCH_TYPE . "\":\"" . ew_JsEncode2($this->BasicSearch->Type) . "\"";
			$sFilterList = ew_Concat($sFilterList, $sWrk, ",");
		}

		// Return filter list in json
		return ($sFilterList <> "") ? "{" . $sFilterList . "}" : "null";
	}

	// Restore list of filters
	function RestoreFilterList() {

		// Return if not reset filter
		if (@$_POST["cmd"] <> "resetfilter")
			return FALSE;
		$filter = json_decode(ew_StripSlashes(@$_POST["filter"]), TRUE);
		$this->Command = "search";

		// Field EmpCodi
		$this->EmpCodi->AdvancedSearch->SearchValue = @$filter["x_EmpCodi"];
		$this->EmpCodi->AdvancedSearch->SearchOperator = @$filter["z_EmpCodi"];
		$this->EmpCodi->AdvancedSearch->SearchCondition = @$filter["v_EmpCodi"];
		$this->EmpCodi->AdvancedSearch->SearchValue2 = @$filter["y_EmpCodi"];
		$this->EmpCodi->AdvancedSearch->SearchOperator2 = @$filter["w_EmpCodi"];
		$this->EmpCodi->AdvancedSearch->Save();

		// Field EmpCarg
		$this->EmpCarg->AdvancedSearch->SearchValue = @$filter["x_EmpCarg"];
		$this->EmpCarg->AdvancedSearch->SearchOperator = @$filter["z_EmpCarg"];
		$this->EmpCarg->AdvancedSearch->SearchCondition = @$filter["v_EmpCarg"];
		$this->EmpCarg->AdvancedSearch->SearchValue2 = @$filter["y_EmpCarg"];
		$this->EmpCarg->AdvancedSearch->SearchOperator2 = @$filter["w_EmpCarg"];
		$this->EmpCarg->AdvancedSearch->Save();

		// Field EmpFIng
		$this->EmpFIng->AdvancedSearch->SearchValue = @$filter["x_EmpFIng"];
		$this->EmpFIng->AdvancedSearch->SearchOperator = @$filter["z_EmpFIng"];
		$this->EmpFIng->AdvancedSearch->SearchCondition = @$filter["v_EmpFIng"];
		$this->EmpFIng->AdvancedSearch->SearchValue2 = @$filter["y_EmpFIng"];
		$this->EmpFIng->AdvancedSearch->SearchOperator2 = @$filter["w_EmpFIng"];
		$this->EmpFIng->AdvancedSearch->Save();

		// Field EmpFSal
		$this->EmpFSal->AdvancedSearch->SearchValue = @$filter["x_EmpFSal"];
		$this->EmpFSal->AdvancedSearch->SearchOperator = @$filter["z_EmpFSal"];
		$this->EmpFSal->AdvancedSearch->SearchCondition = @$filter["v_EmpFSal"];
		$this->EmpFSal->AdvancedSearch->SearchValue2 = @$filter["y_EmpFSal"];
		$this->EmpFSal->AdvancedSearch->SearchOperator2 = @$filter["w_EmpFSal"];
		$this->EmpFSal->AdvancedSearch->Save();

		// Field EmpSala
		$this->EmpSala->AdvancedSearch->SearchValue = @$filter["x_EmpSala"];
		$this->EmpSala->AdvancedSearch->SearchOperator = @$filter["z_EmpSala"];
		$this->EmpSala->AdvancedSearch->SearchCondition = @$filter["v_EmpSala"];
		$this->EmpSala->AdvancedSearch->SearchValue2 = @$filter["y_EmpSala"];
		$this->EmpSala->AdvancedSearch->SearchOperator2 = @$filter["w_EmpSala"];
		$this->EmpSala->AdvancedSearch->Save();

		// Field EmpNomb
		$this->EmpNomb->AdvancedSearch->SearchValue = @$filter["x_EmpNomb"];
		$this->EmpNomb->AdvancedSearch->SearchOperator = @$filter["z_EmpNomb"];
		$this->EmpNomb->AdvancedSearch->SearchCondition = @$filter["v_EmpNomb"];
		$this->EmpNomb->AdvancedSearch->SearchValue2 = @$filter["y_EmpNomb"];
		$this->EmpNomb->AdvancedSearch->SearchOperator2 = @$filter["w_EmpNomb"];
		$this->EmpNomb->AdvancedSearch->Save();

		// Field EmpApel
		$this->EmpApel->AdvancedSearch->SearchValue = @$filter["x_EmpApel"];
		$this->EmpApel->AdvancedSearch->SearchOperator = @$filter["z_EmpApel"];
		$this->EmpApel->AdvancedSearch->SearchCondition = @$filter["v_EmpApel"];
		$this->EmpApel->AdvancedSearch->SearchValue2 = @$filter["y_EmpApel"];
		$this->EmpApel->AdvancedSearch->SearchOperator2 = @$filter["w_EmpApel"];
		$this->EmpApel->AdvancedSearch->Save();

		// Field EmpCedu
		$this->EmpCedu->AdvancedSearch->SearchValue = @$filter["x_EmpCedu"];
		$this->EmpCedu->AdvancedSearch->SearchOperator = @$filter["z_EmpCedu"];
		$this->EmpCedu->AdvancedSearch->SearchCondition = @$filter["v_EmpCedu"];
		$this->EmpCedu->AdvancedSearch->SearchValue2 = @$filter["y_EmpCedu"];
		$this->EmpCedu->AdvancedSearch->SearchOperator2 = @$filter["w_EmpCedu"];
		$this->EmpCedu->AdvancedSearch->Save();

		// Field EmpTele
		$this->EmpTele->AdvancedSearch->SearchValue = @$filter["x_EmpTele"];
		$this->EmpTele->AdvancedSearch->SearchOperator = @$filter["z_EmpTele"];
		$this->EmpTele->AdvancedSearch->SearchCondition = @$filter["v_EmpTele"];
		$this->EmpTele->AdvancedSearch->SearchValue2 = @$filter["y_EmpTele"];
		$this->EmpTele->AdvancedSearch->SearchOperator2 = @$filter["w_EmpTele"];
		$this->EmpTele->AdvancedSearch->Save();
		$this->BasicSearch->setKeyword(@$filter[EW_TABLE_BASIC_SEARCH]);
		$this->BasicSearch->setType(@$filter[EW_TABLE_BASIC_SEARCH_TYPE]);
	}

	// Return basic search SQL
	function BasicSearchSQL($arKeywords, $type) {
		$sWhere = "";
		$this->BuildBasicSearchSQL($sWhere, $this->EmpCarg, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->EmpNomb, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->EmpApel, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->EmpCedu, $arKeywords, $type);
		return $sWhere;
	}

	// Build basic search SQL
	function BuildBasicSearchSql(&$Where, &$Fld, $arKeywords, $type) {
		$sDefCond = ($type == "OR") ? "OR" : "AND";
		$arSQL = array(); // Array for SQL parts
		$arCond = array(); // Array for search conditions
		$cnt = count($arKeywords);
		$j = 0; // Number of SQL parts
		for ($i = 0; $i < $cnt; $i++) {
			$Keyword = $arKeywords[$i];
			$Keyword = trim($Keyword);
			if (EW_BASIC_SEARCH_IGNORE_PATTERN <> "") {
				$Keyword = preg_replace(EW_BASIC_SEARCH_IGNORE_PATTERN, "\\", $Keyword);
				$ar = explode("\\", $Keyword);
			} else {
				$ar = array($Keyword);
			}
			foreach ($ar as $Keyword) {
				if ($Keyword <> "") {
					$sWrk = "";
					if ($Keyword == "OR" && $type == "") {
						if ($j > 0)
							$arCond[$j-1] = "OR";
					} elseif ($Keyword == EW_NULL_VALUE) {
						$sWrk = $Fld->FldExpression . " IS NULL";
					} elseif ($Keyword == EW_NOT_NULL_VALUE) {
						$sWrk = $Fld->FldExpression . " IS NOT NULL";
					} elseif ($Fld->FldIsVirtual && $Fld->FldVirtualSearch) {
						$sWrk = $Fld->FldVirtualExpression . ew_Like(ew_QuotedValue("%" . $Keyword . "%", EW_DATATYPE_STRING, $this->DBID), $this->DBID);
					} elseif ($Fld->FldDataType != EW_DATATYPE_NUMBER || is_numeric($Keyword)) {
						$sWrk = $Fld->FldBasicSearchExpression . ew_Like(ew_QuotedValue("%" . $Keyword . "%", EW_DATATYPE_STRING, $this->DBID), $this->DBID);
					}
					if ($sWrk <> "") {
						$arSQL[$j] = $sWrk;
						$arCond[$j] = $sDefCond;
						$j += 1;
					}
				}
			}
		}
		$cnt = count($arSQL);
		$bQuoted = FALSE;
		$sSql = "";
		if ($cnt > 0) {
			for ($i = 0; $i < $cnt-1; $i++) {
				if ($arCond[$i] == "OR") {
					if (!$bQuoted) $sSql .= "(";
					$bQuoted = TRUE;
				}
				$sSql .= $arSQL[$i];
				if ($bQuoted && $arCond[$i] <> "OR") {
					$sSql .= ")";
					$bQuoted = FALSE;
				}
				$sSql .= " " . $arCond[$i] . " ";
			}
			$sSql .= $arSQL[$cnt-1];
			if ($bQuoted)
				$sSql .= ")";
		}
		if ($sSql <> "") {
			if ($Where <> "") $Where .= " OR ";
			$Where .=  "(" . $sSql . ")";
		}
	}

	// Return basic search WHERE clause based on search keyword and type
	function BasicSearchWhere($Default = FALSE) {
		global $Security;
		$sSearchStr = "";
		if (!$Security->CanSearch()) return "";
		$sSearchKeyword = ($Default) ? $this->BasicSearch->KeywordDefault : $this->BasicSearch->Keyword;
		$sSearchType = ($Default) ? $this->BasicSearch->TypeDefault : $this->BasicSearch->Type;
		if ($sSearchKeyword <> "") {
			$sSearch = trim($sSearchKeyword);
			if ($sSearchType <> "=") {
				$ar = array();

				// Match quoted keywords (i.e.: "...")
				if (preg_match_all('/"([^"]*)"/i', $sSearch, $matches, PREG_SET_ORDER)) {
					foreach ($matches as $match) {
						$p = strpos($sSearch, $match[0]);
						$str = substr($sSearch, 0, $p);
						$sSearch = substr($sSearch, $p + strlen($match[0]));
						if (strlen(trim($str)) > 0)
							$ar = array_merge($ar, explode(" ", trim($str)));
						$ar[] = $match[1]; // Save quoted keyword
					}
				}

				// Match individual keywords
				if (strlen(trim($sSearch)) > 0)
					$ar = array_merge($ar, explode(" ", trim($sSearch)));

				// Search keyword in any fields
				if (($sSearchType == "OR" || $sSearchType == "AND") && $this->BasicSearch->BasicSearchAnyFields) {
					foreach ($ar as $sKeyword) {
						if ($sKeyword <> "") {
							if ($sSearchStr <> "") $sSearchStr .= " " . $sSearchType . " ";
							$sSearchStr .= "(" . $this->BasicSearchSQL(array($sKeyword), $sSearchType) . ")";
						}
					}
				} else {
					$sSearchStr = $this->BasicSearchSQL($ar, $sSearchType);
				}
			} else {
				$sSearchStr = $this->BasicSearchSQL(array($sSearch), $sSearchType);
			}
			if (!$Default) $this->Command = "search";
		}
		if (!$Default && $this->Command == "search") {
			$this->BasicSearch->setKeyword($sSearchKeyword);
			$this->BasicSearch->setType($sSearchType);
		}
		return $sSearchStr;
	}

	// Check if search parm exists
	function CheckSearchParms() {

		// Check basic search
		if ($this->BasicSearch->IssetSession())
			return TRUE;
		return FALSE;
	}

	// Clear all search parameters
	function ResetSearchParms() {

		// Clear search WHERE clause
		$this->SearchWhere = "";
		$this->setSearchWhere($this->SearchWhere);

		// Clear basic search parameters
		$this->ResetBasicSearchParms();
	}

	// Load advanced search default values
	function LoadAdvancedSearchDefault() {
		return FALSE;
	}

	// Clear all basic search parameters
	function ResetBasicSearchParms() {
		$this->BasicSearch->UnsetSession();
	}

	// Restore all search parameters
	function RestoreSearchParms() {
		$this->RestoreSearch = TRUE;

		// Restore basic search values
		$this->BasicSearch->Load();
	}

	// Set up sort parameters
	function SetUpSortOrder() {

		// Check for Ctrl pressed
		$bCtrl = (@$_GET["ctrl"] <> "");

		// Check for "order" parameter
		if (@$_GET["order"] <> "") {
			$this->CurrentOrder = ew_StripSlashes(@$_GET["order"]);
			$this->CurrentOrderType = @$_GET["ordertype"];
			$this->UpdateSort($this->EmpCarg, $bCtrl); // EmpCarg
			$this->UpdateSort($this->EmpFIng, $bCtrl); // EmpFIng
			$this->UpdateSort($this->EmpFSal, $bCtrl); // EmpFSal
			$this->UpdateSort($this->EmpSala, $bCtrl); // EmpSala
			$this->UpdateSort($this->EmpNomb, $bCtrl); // EmpNomb
			$this->UpdateSort($this->EmpApel, $bCtrl); // EmpApel
			$this->UpdateSort($this->EmpCedu, $bCtrl); // EmpCedu
			$this->UpdateSort($this->EmpTele, $bCtrl); // EmpTele
			$this->setStartRecordNumber(1); // Reset start position
		}
	}

	// Load sort order parameters
	function LoadSortOrder() {
		$sOrderBy = $this->getSessionOrderBy(); // Get ORDER BY from Session
		if ($sOrderBy == "") {
			if ($this->getSqlOrderBy() <> "") {
				$sOrderBy = $this->getSqlOrderBy();
				$this->setSessionOrderBy($sOrderBy);
			}
		}
	}

	// Reset command
	// - cmd=reset (Reset search parameters)
	// - cmd=resetall (Reset search and master/detail parameters)
	// - cmd=resetsort (Reset sort parameters)
	function ResetCmd() {

		// Check if reset command
		if (substr($this->Command,0,5) == "reset") {

			// Reset search criteria
			if ($this->Command == "reset" || $this->Command == "resetall")
				$this->ResetSearchParms();

			// Reset sorting order
			if ($this->Command == "resetsort") {
				$sOrderBy = "";
				$this->setSessionOrderBy($sOrderBy);
				$this->EmpCarg->setSort("");
				$this->EmpFIng->setSort("");
				$this->EmpFSal->setSort("");
				$this->EmpSala->setSort("");
				$this->EmpNomb->setSort("");
				$this->EmpApel->setSort("");
				$this->EmpCedu->setSort("");
				$this->EmpTele->setSort("");
			}

			// Reset start position
			$this->StartRec = 1;
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Set up list options
	function SetupListOptions() {
		global $Security, $Language;

		// "griddelete"
		if ($this->AllowAddDeleteRow) {
			$item = &$this->ListOptions->Add("griddelete");
			$item->CssStyle = "white-space: nowrap;";
			$item->OnLeft = FALSE;
			$item->Visible = FALSE; // Default hidden
		}

		// Add group option item
		$item = &$this->ListOptions->Add($this->ListOptions->GroupOptionName);
		$item->Body = "";
		$item->OnLeft = FALSE;
		$item->Visible = FALSE;

		// "view"
		$item = &$this->ListOptions->Add("view");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->CanView();
		$item->OnLeft = FALSE;

		// "edit"
		$item = &$this->ListOptions->Add("edit");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->CanEdit();
		$item->OnLeft = FALSE;

		// "copy"
		$item = &$this->ListOptions->Add("copy");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->CanAdd();
		$item->OnLeft = FALSE;

		// List actions
		$item = &$this->ListOptions->Add("listactions");
		$item->CssStyle = "white-space: nowrap;";
		$item->OnLeft = FALSE;
		$item->Visible = FALSE;
		$item->ShowInButtonGroup = FALSE;
		$item->ShowInDropDown = FALSE;

		// "checkbox"
		$item = &$this->ListOptions->Add("checkbox");
		$item->Visible = FALSE;
		$item->OnLeft = FALSE;
		$item->Header = "<input type=\"checkbox\" name=\"key\" id=\"key\" onclick=\"ew_SelectAllKey(this);\">";
		$item->ShowInDropDown = FALSE;
		$item->ShowInButtonGroup = FALSE;

		// Drop down button for ListOptions
		$this->ListOptions->UseImageAndText = TRUE;
		$this->ListOptions->UseDropDownButton = FALSE;
		$this->ListOptions->DropDownButtonPhrase = $Language->Phrase("ButtonListOptions");
		$this->ListOptions->UseButtonGroup = FALSE;
		if ($this->ListOptions->UseButtonGroup && ew_IsMobile())
			$this->ListOptions->UseDropDownButton = TRUE;
		$this->ListOptions->ButtonClass = "btn-sm"; // Class for button group

		// Call ListOptions_Load event
		$this->ListOptions_Load();
		$this->SetupListOptionsExt();
		$item = &$this->ListOptions->GetItem($this->ListOptions->GroupOptionName);
		$item->Visible = $this->ListOptions->GroupOptionVisible();
	}

	// Render list options
	function RenderListOptions() {
		global $Security, $Language, $objForm;
		$this->ListOptions->LoadDefault();

		// Set up row action and key
		if (is_numeric($this->RowIndex) && $this->CurrentMode <> "view") {
			$objForm->Index = $this->RowIndex;
			$ActionName = str_replace("k_", "k" . $this->RowIndex . "_", $this->FormActionName);
			$OldKeyName = str_replace("k_", "k" . $this->RowIndex . "_", $this->FormOldKeyName);
			$KeyName = str_replace("k_", "k" . $this->RowIndex . "_", $this->FormKeyName);
			$BlankRowName = str_replace("k_", "k" . $this->RowIndex . "_", $this->FormBlankRowName);
			if ($this->RowAction <> "")
				$this->MultiSelectKey .= "<input type=\"hidden\" name=\"" . $ActionName . "\" id=\"" . $ActionName . "\" value=\"" . $this->RowAction . "\">";
			if ($this->RowAction == "delete") {
				$rowkey = $objForm->GetValue($this->FormKeyName);
				$this->SetupKeyValues($rowkey);
			}
			if ($this->RowAction == "insert" && $this->CurrentAction == "F" && $this->EmptyRow())
				$this->MultiSelectKey .= "<input type=\"hidden\" name=\"" . $BlankRowName . "\" id=\"" . $BlankRowName . "\" value=\"1\">";
		}

		// "delete"
		if ($this->AllowAddDeleteRow) {
			if ($this->CurrentAction == "gridadd" || $this->CurrentAction == "gridedit") {
				$option = &$this->ListOptions;
				$option->UseButtonGroup = TRUE; // Use button group for grid delete button
				$option->UseImageAndText = TRUE; // Use image and text for grid delete button
				$oListOpt = &$option->Items["griddelete"];
				if (is_numeric($this->RowIndex) && ($this->RowAction == "" || $this->RowAction == "edit")) { // Do not allow delete existing record
					$oListOpt->Body = "&nbsp;";
				} else {
					$oListOpt->Body = "<a class=\"ewGridLink ewGridDelete\" title=\"" . ew_HtmlTitle($Language->Phrase("DeleteLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("DeleteLink")) . "\" onclick=\"return ew_DeleteGridRow(this, " . $this->RowIndex . ");\">" . $Language->Phrase("DeleteLink") . "</a>";
				}
			}
		}

		// "view"
		$oListOpt = &$this->ListOptions->Items["view"];
		if ($Security->CanView())
			$oListOpt->Body = "<a class=\"ewRowLink ewView\" title=\"" . ew_HtmlTitle($Language->Phrase("ViewLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("ViewLink")) . "\" href=\"" . ew_HtmlEncode($this->ViewUrl) . "\">" . $Language->Phrase("ViewLink") . "</a>";
		else
			$oListOpt->Body = "";

		// "edit"
		$oListOpt = &$this->ListOptions->Items["edit"];
		if ($Security->CanEdit()) {
			$oListOpt->Body = "<a class=\"ewRowLink ewEdit\" title=\"" . ew_HtmlTitle($Language->Phrase("EditLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("EditLink")) . "\" href=\"" . ew_HtmlEncode($this->EditUrl) . "\">" . $Language->Phrase("EditLink") . "</a>";
		} else {
			$oListOpt->Body = "";
		}

		// "copy"
		$oListOpt = &$this->ListOptions->Items["copy"];
		if ($Security->CanAdd()) {
			$oListOpt->Body = "<a class=\"ewRowLink ewCopy\" title=\"" . ew_HtmlTitle($Language->Phrase("CopyLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("CopyLink")) . "\" href=\"" . ew_HtmlEncode($this->CopyUrl) . "\">" . $Language->Phrase("CopyLink") . "</a>";
		} else {
			$oListOpt->Body = "";
		}

		// Set up list action buttons
		$oListOpt = &$this->ListOptions->GetItem("listactions");
		if ($oListOpt) {
			$body = "";
			$links = array();
			foreach ($this->ListActions->Items as $listaction) {
				if ($listaction->Select == EW_ACTION_SINGLE && $listaction->Allow) {
					$action = $listaction->Action;
					$caption = $listaction->Caption;
					$icon = ($listaction->Icon <> "") ? "<span class=\"" . ew_HtmlEncode(str_replace(" ewIcon", "", $listaction->Icon)) . "\" data-caption=\"" . ew_HtmlTitle($caption) . "\"></span> " : "";
					$links[] = "<li><a class=\"ewAction ewListAction\" data-action=\"" . ew_HtmlEncode($action) . "\" data-caption=\"" . ew_HtmlTitle($caption) . "\" href=\"\" onclick=\"ew_SubmitAction(event,jQuery.extend({key:" . $this->KeyToJson() . "}," . $listaction->ToJson(TRUE) . "));return false;\">" . $icon . $listaction->Caption . "</a></li>";
					if (count($links) == 1) // Single button
						$body = "<a class=\"ewAction ewListAction\" data-action=\"" . ew_HtmlEncode($action) . "\" title=\"" . ew_HtmlTitle($caption) . "\" data-caption=\"" . ew_HtmlTitle($caption) . "\" href=\"\" onclick=\"ew_SubmitAction(event,jQuery.extend({key:" . $this->KeyToJson() . "}," . $listaction->ToJson(TRUE) . "));return false;\">" . $Language->Phrase("ListActionButton") . "</a>";
				}
			}
			if (count($links) > 1) { // More than one buttons, use dropdown
				$body = "<button class=\"dropdown-toggle btn btn-default btn-sm ewActions\" title=\"" . ew_HtmlTitle($Language->Phrase("ListActionButton")) . "\" data-toggle=\"dropdown\">" . $Language->Phrase("ListActionButton") . "<b class=\"caret\"></b></button>";
				$content = "";
				foreach ($links as $link)
					$content .= "<li>" . $link . "</li>";
				$body .= "<ul class=\"dropdown-menu" . ($oListOpt->OnLeft ? "" : " dropdown-menu-right") . "\">". $content . "</ul>";
				$body = "<div class=\"btn-group\">" . $body . "</div>";
			}
			if (count($links) > 0) {
				$oListOpt->Body = $body;
				$oListOpt->Visible = TRUE;
			}
		}

		// "checkbox"
		$oListOpt = &$this->ListOptions->Items["checkbox"];
		$oListOpt->Body = "<input type=\"checkbox\" name=\"key_m[]\" value=\"" . ew_HtmlEncode($this->EmpCodi->CurrentValue) . "\" onclick='ew_ClickMultiCheckbox(event);'>";
		if ($this->CurrentAction == "gridedit" && is_numeric($this->RowIndex)) {
			$this->MultiSelectKey .= "<input type=\"hidden\" name=\"" . $KeyName . "\" id=\"" . $KeyName . "\" value=\"" . $this->EmpCodi->CurrentValue . "\">";
		}
		$this->RenderListOptionsExt();

		// Call ListOptions_Rendered event
		$this->ListOptions_Rendered();
	}

	// Set up other options
	function SetupOtherOptions() {
		global $Language, $Security;
		$options = &$this->OtherOptions;
		$option = $options["addedit"];

		// Add
		$item = &$option->Add("add");
		$item->Body = "<a class=\"ewAddEdit ewAdd\" title=\"" . ew_HtmlTitle($Language->Phrase("AddLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("AddLink")) . "\" href=\"" . ew_HtmlEncode($this->AddUrl) . "\">" . $Language->Phrase("AddLink") . "</a>";
		$item->Visible = ($this->AddUrl <> "" && $Security->CanAdd());
		$item = &$option->Add("gridadd");
		$item->Body = "<a class=\"ewAddEdit ewGridAdd\" title=\"" . ew_HtmlTitle($Language->Phrase("GridAddLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("GridAddLink")) . "\" href=\"" . ew_HtmlEncode($this->GridAddUrl) . "\">" . $Language->Phrase("GridAddLink") . "</a>";
		$item->Visible = ($this->GridAddUrl <> "" && $Security->CanAdd());

		// Add grid edit
		$option = $options["addedit"];
		$item = &$option->Add("gridedit");
		$item->Body = "<a class=\"ewAddEdit ewGridEdit\" title=\"" . ew_HtmlTitle($Language->Phrase("GridEditLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("GridEditLink")) . "\" href=\"" . ew_HtmlEncode($this->GridEditUrl) . "\">" . $Language->Phrase("GridEditLink") . "</a>";
		$item->Visible = ($this->GridEditUrl <> "" && $Security->CanEdit());
		$option = $options["action"];

		// Set up options default
		foreach ($options as &$option) {
			$option->UseImageAndText = TRUE;
			$option->UseDropDownButton = TRUE;
			$option->UseButtonGroup = TRUE;
			$option->ButtonClass = "btn-sm"; // Class for button group
			$item = &$option->Add($option->GroupOptionName);
			$item->Body = "";
			$item->Visible = FALSE;
		}
		$options["addedit"]->DropDownButtonPhrase = $Language->Phrase("ButtonAddEdit");
		$options["detail"]->DropDownButtonPhrase = $Language->Phrase("ButtonDetails");
		$options["action"]->DropDownButtonPhrase = $Language->Phrase("ButtonActions");

		// Filter button
		$item = &$this->FilterOptions->Add("savecurrentfilter");
		$item->Body = "<a class=\"ewSaveFilter\" data-form=\"fEmpllistsrch\" href=\"#\">" . $Language->Phrase("SaveCurrentFilter") . "</a>";
		$item->Visible = TRUE;
		$item = &$this->FilterOptions->Add("deletefilter");
		$item->Body = "<a class=\"ewDeleteFilter\" data-form=\"fEmpllistsrch\" href=\"#\">" . $Language->Phrase("DeleteFilter") . "</a>";
		$item->Visible = TRUE;
		$this->FilterOptions->UseDropDownButton = TRUE;
		$this->FilterOptions->UseButtonGroup = !$this->FilterOptions->UseDropDownButton;
		$this->FilterOptions->DropDownButtonPhrase = $Language->Phrase("Filters");

		// Add group option item
		$item = &$this->FilterOptions->Add($this->FilterOptions->GroupOptionName);
		$item->Body = "";
		$item->Visible = FALSE;
	}

	// Render other options
	function RenderOtherOptions() {
		global $Language, $Security;
		$options = &$this->OtherOptions;
		if ($this->CurrentAction <> "gridadd" && $this->CurrentAction <> "gridedit") { // Not grid add/edit mode
			$option = &$options["action"];

			// Set up list action buttons
			foreach ($this->ListActions->Items as $listaction) {
				if ($listaction->Select == EW_ACTION_MULTIPLE) {
					$item = &$option->Add("custom_" . $listaction->Action);
					$caption = $listaction->Caption;
					$icon = ($listaction->Icon <> "") ? "<span class=\"" . ew_HtmlEncode($listaction->Icon) . "\" data-caption=\"" . ew_HtmlEncode($caption) . "\"></span> " : $caption;
					$item->Body = "<a class=\"ewAction ewListAction\" title=\"" . ew_HtmlEncode($caption) . "\" data-caption=\"" . ew_HtmlEncode($caption) . "\" href=\"\" onclick=\"ew_SubmitAction(event,jQuery.extend({f:document.fEmpllist}," . $listaction->ToJson(TRUE) . "));return false;\">" . $icon . "</a>";
					$item->Visible = $listaction->Allow;
				}
			}

			// Hide grid edit and other options
			if ($this->TotalRecs <= 0) {
				$option = &$options["addedit"];
				$item = &$option->GetItem("gridedit");
				if ($item) $item->Visible = FALSE;
				$option = &$options["action"];
				$option->HideAllOptions();
			}
		} else { // Grid add/edit mode

			// Hide all options first
			foreach ($options as &$option)
				$option->HideAllOptions();
			if ($this->CurrentAction == "gridadd") {
				if ($this->AllowAddDeleteRow) {

					// Add add blank row
					$option = &$options["addedit"];
					$option->UseDropDownButton = FALSE;
					$option->UseImageAndText = TRUE;
					$item = &$option->Add("addblankrow");
					$item->Body = "<a class=\"ewAddEdit ewAddBlankRow\" title=\"" . ew_HtmlTitle($Language->Phrase("AddBlankRow")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("AddBlankRow")) . "\" href=\"javascript:void(0);\" onclick=\"ew_AddGridRow(this);\">" . $Language->Phrase("AddBlankRow") . "</a>";
					$item->Visible = $Security->CanAdd();
				}
				$option = &$options["action"];
				$option->UseDropDownButton = FALSE;
				$option->UseImageAndText = TRUE;

				// Add grid insert
				$item = &$option->Add("gridinsert");
				$item->Body = "<a class=\"ewAction ewGridInsert\" title=\"" . ew_HtmlTitle($Language->Phrase("GridInsertLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("GridInsertLink")) . "\" href=\"\" onclick=\"return ewForms(this).Submit('" . $this->AddMasterUrl($this->PageName()) . "');\">" . $Language->Phrase("GridInsertLink") . "</a>";

				// Add grid cancel
				$item = &$option->Add("gridcancel");
				$cancelurl = $this->AddMasterUrl($this->PageUrl() . "a=cancel");
				$item->Body = "<a class=\"ewAction ewGridCancel\" title=\"" . ew_HtmlTitle($Language->Phrase("GridCancelLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("GridCancelLink")) . "\" href=\"" . $cancelurl . "\">" . $Language->Phrase("GridCancelLink") . "</a>";
			}
			if ($this->CurrentAction == "gridedit") {
				if ($this->AllowAddDeleteRow) {

					// Add add blank row
					$option = &$options["addedit"];
					$option->UseDropDownButton = FALSE;
					$option->UseImageAndText = TRUE;
					$item = &$option->Add("addblankrow");
					$item->Body = "<a class=\"ewAddEdit ewAddBlankRow\" title=\"" . ew_HtmlTitle($Language->Phrase("AddBlankRow")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("AddBlankRow")) . "\" href=\"javascript:void(0);\" onclick=\"ew_AddGridRow(this);\">" . $Language->Phrase("AddBlankRow") . "</a>";
					$item->Visible = $Security->CanAdd();
				}
				$option = &$options["action"];
				$option->UseDropDownButton = FALSE;
				$option->UseImageAndText = TRUE;
					$item = &$option->Add("gridsave");
					$item->Body = "<a class=\"ewAction ewGridSave\" title=\"" . ew_HtmlTitle($Language->Phrase("GridSaveLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("GridSaveLink")) . "\" href=\"\" onclick=\"return ewForms(this).Submit('" . $this->AddMasterUrl($this->PageName()) . "');\">" . $Language->Phrase("GridSaveLink") . "</a>";
					$item = &$option->Add("gridcancel");
					$cancelurl = $this->AddMasterUrl($this->PageUrl() . "a=cancel");
					$item->Body = "<a class=\"ewAction ewGridCancel\" title=\"" . ew_HtmlTitle($Language->Phrase("GridCancelLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("GridCancelLink")) . "\" href=\"" . $cancelurl . "\">" . $Language->Phrase("GridCancelLink") . "</a>";
			}
		}
	}

	// Process list action
	function ProcessListAction() {
		global $Language, $Security;
		$userlist = "";
		$user = "";
		$sFilter = $this->GetKeyFilter();
		$UserAction = @$_POST["useraction"];
		if ($sFilter <> "" && $UserAction <> "") {

			// Check permission first
			$ActionCaption = $UserAction;
			if (array_key_exists($UserAction, $this->ListActions->Items)) {
				$ActionCaption = $this->ListActions->Items[$UserAction]->Caption;
				if (!$this->ListActions->Items[$UserAction]->Allow) {
					$errmsg = str_replace('%s', $ActionCaption, $Language->Phrase("CustomActionNotAllowed"));
					if (@$_POST["ajax"] == $UserAction) // Ajax
						echo "<p class=\"text-danger\">" . $errmsg . "</p>";
					else
						$this->setFailureMessage($errmsg);
					return FALSE;
				}
			}
			$this->CurrentFilter = $sFilter;
			$sSql = $this->SQL();
			$conn = &$this->Connection();
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			$rs = $conn->Execute($sSql);
			$conn->raiseErrorFn = '';
			$this->CurrentAction = $UserAction;

			// Call row action event
			if ($rs && !$rs->EOF) {
				$conn->BeginTrans();
				$this->SelectedCount = $rs->RecordCount();
				$this->SelectedIndex = 0;
				while (!$rs->EOF) {
					$this->SelectedIndex++;
					$row = $rs->fields;
					$Processed = $this->Row_CustomAction($UserAction, $row);
					if (!$Processed) break;
					$rs->MoveNext();
				}
				if ($Processed) {
					$conn->CommitTrans(); // Commit the changes
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage(str_replace('%s', $ActionCaption, $Language->Phrase("CustomActionCompleted"))); // Set up success message
				} else {
					$conn->RollbackTrans(); // Rollback changes

					// Set up error message
					if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

						// Use the message, do nothing
					} elseif ($this->CancelMessage <> "") {
						$this->setFailureMessage($this->CancelMessage);
						$this->CancelMessage = "";
					} else {
						$this->setFailureMessage(str_replace('%s', $ActionCaption, $Language->Phrase("CustomActionFailed")));
					}
				}
			}
			if ($rs)
				$rs->Close();
			if (@$_POST["ajax"] == $UserAction) { // Ajax
				if ($this->getSuccessMessage() <> "") {
					echo "<p class=\"text-success\">" . $this->getSuccessMessage() . "</p>";
					$this->ClearSuccessMessage(); // Clear message
				}
				if ($this->getFailureMessage() <> "") {
					echo "<p class=\"text-danger\">" . $this->getFailureMessage() . "</p>";
					$this->ClearFailureMessage(); // Clear message
				}
				return TRUE;
			}
		}
		return FALSE; // Not ajax request
	}

	// Set up search options
	function SetupSearchOptions() {
		global $Language;
		$this->SearchOptions = new cListOptions();
		$this->SearchOptions->Tag = "div";
		$this->SearchOptions->TagClassName = "ewSearchOption";

		// Search button
		$item = &$this->SearchOptions->Add("searchtoggle");
		$SearchToggleClass = ($this->SearchWhere <> "") ? " active" : " active";
		$item->Body = "<button type=\"button\" class=\"btn btn-default ewSearchToggle" . $SearchToggleClass . "\" title=\"" . $Language->Phrase("SearchPanel") . "\" data-caption=\"" . $Language->Phrase("SearchPanel") . "\" data-toggle=\"button\" data-form=\"fEmpllistsrch\">" . $Language->Phrase("SearchBtn") . "</button>";
		$item->Visible = TRUE;

		// Show all button
		$item = &$this->SearchOptions->Add("showall");
		$item->Body = "<a class=\"btn btn-default ewShowAll\" title=\"" . $Language->Phrase("ShowAll") . "\" data-caption=\"" . $Language->Phrase("ShowAll") . "\" href=\"" . $this->PageUrl() . "cmd=reset\">" . $Language->Phrase("ShowAllBtn") . "</a>";
		$item->Visible = ($this->SearchWhere <> $this->DefaultSearchWhere && $this->SearchWhere <> "0=101");

		// Button group for search
		$this->SearchOptions->UseDropDownButton = FALSE;
		$this->SearchOptions->UseImageAndText = TRUE;
		$this->SearchOptions->UseButtonGroup = TRUE;
		$this->SearchOptions->DropDownButtonPhrase = $Language->Phrase("ButtonSearch");

		// Add group option item
		$item = &$this->SearchOptions->Add($this->SearchOptions->GroupOptionName);
		$item->Body = "";
		$item->Visible = FALSE;

		// Hide search options
		if ($this->Export <> "" || $this->CurrentAction <> "")
			$this->SearchOptions->HideAllOptions();
		global $Security;
		if (!$Security->CanSearch()) {
			$this->SearchOptions->HideAllOptions();
			$this->FilterOptions->HideAllOptions();
		}
	}

	function SetupListOptionsExt() {
		global $Security, $Language;
	}

	function RenderListOptionsExt() {
		global $Security, $Language;
	}

	// Set up starting record parameters
	function SetUpStartRec() {
		if ($this->DisplayRecs == 0)
			return;
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET[EW_TABLE_START_REC] <> "") { // Check for "start" parameter
				$this->StartRec = $_GET[EW_TABLE_START_REC];
				$this->setStartRecordNumber($this->StartRec);
			} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
				$PageNo = $_GET[EW_TABLE_PAGE_NO];
				if (is_numeric($PageNo)) {
					$this->StartRec = ($PageNo-1)*$this->DisplayRecs+1;
					if ($this->StartRec <= 0) {
						$this->StartRec = 1;
					} elseif ($this->StartRec >= intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1) {
						$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1;
					}
					$this->setStartRecordNumber($this->StartRec);
				}
			}
		}
		$this->StartRec = $this->getStartRecordNumber();

		// Check if correct start record counter
		if (!is_numeric($this->StartRec) || $this->StartRec == "") { // Avoid invalid start record counter
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} elseif (intval($this->StartRec) > intval($this->TotalRecs)) { // Avoid starting record > total records
			$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to last page first record
			$this->setStartRecordNumber($this->StartRec);
		} elseif (($this->StartRec-1) % $this->DisplayRecs <> 0) {
			$this->StartRec = intval(($this->StartRec-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to page boundary
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Load default values
	function LoadDefaultValues() {
		$this->EmpCarg->CurrentValue = NULL;
		$this->EmpCarg->OldValue = $this->EmpCarg->CurrentValue;
		$this->EmpFIng->CurrentValue = NULL;
		$this->EmpFIng->OldValue = $this->EmpFIng->CurrentValue;
		$this->EmpFSal->CurrentValue = NULL;
		$this->EmpFSal->OldValue = $this->EmpFSal->CurrentValue;
		$this->EmpSala->CurrentValue = NULL;
		$this->EmpSala->OldValue = $this->EmpSala->CurrentValue;
		$this->EmpNomb->CurrentValue = NULL;
		$this->EmpNomb->OldValue = $this->EmpNomb->CurrentValue;
		$this->EmpApel->CurrentValue = NULL;
		$this->EmpApel->OldValue = $this->EmpApel->CurrentValue;
		$this->EmpCedu->CurrentValue = NULL;
		$this->EmpCedu->OldValue = $this->EmpCedu->CurrentValue;
		$this->EmpTele->CurrentValue = NULL;
		$this->EmpTele->OldValue = $this->EmpTele->CurrentValue;
	}

	// Load basic search values
	function LoadBasicSearchValues() {
		$this->BasicSearch->Keyword = @$_GET[EW_TABLE_BASIC_SEARCH];
		if ($this->BasicSearch->Keyword <> "") $this->Command = "search";
		$this->BasicSearch->Type = @$_GET[EW_TABLE_BASIC_SEARCH_TYPE];
	}

	// Load form values
	function LoadFormValues() {

		// Load from form
		global $objForm;
		if (!$this->EmpCarg->FldIsDetailKey) {
			$this->EmpCarg->setFormValue($objForm->GetValue("x_EmpCarg"));
		}
		$this->EmpCarg->setOldValue($objForm->GetValue("o_EmpCarg"));
		if (!$this->EmpFIng->FldIsDetailKey) {
			$this->EmpFIng->setFormValue($objForm->GetValue("x_EmpFIng"));
			$this->EmpFIng->CurrentValue = ew_UnFormatDateTime($this->EmpFIng->CurrentValue, 7);
		}
		$this->EmpFIng->setOldValue($objForm->GetValue("o_EmpFIng"));
		if (!$this->EmpFSal->FldIsDetailKey) {
			$this->EmpFSal->setFormValue($objForm->GetValue("x_EmpFSal"));
			$this->EmpFSal->CurrentValue = ew_UnFormatDateTime($this->EmpFSal->CurrentValue, 7);
		}
		$this->EmpFSal->setOldValue($objForm->GetValue("o_EmpFSal"));
		if (!$this->EmpSala->FldIsDetailKey) {
			$this->EmpSala->setFormValue($objForm->GetValue("x_EmpSala"));
		}
		$this->EmpSala->setOldValue($objForm->GetValue("o_EmpSala"));
		if (!$this->EmpNomb->FldIsDetailKey) {
			$this->EmpNomb->setFormValue($objForm->GetValue("x_EmpNomb"));
		}
		$this->EmpNomb->setOldValue($objForm->GetValue("o_EmpNomb"));
		if (!$this->EmpApel->FldIsDetailKey) {
			$this->EmpApel->setFormValue($objForm->GetValue("x_EmpApel"));
		}
		$this->EmpApel->setOldValue($objForm->GetValue("o_EmpApel"));
		if (!$this->EmpCedu->FldIsDetailKey) {
			$this->EmpCedu->setFormValue($objForm->GetValue("x_EmpCedu"));
		}
		$this->EmpCedu->setOldValue($objForm->GetValue("o_EmpCedu"));
		if (!$this->EmpTele->FldIsDetailKey) {
			$this->EmpTele->setFormValue($objForm->GetValue("x_EmpTele"));
		}
		$this->EmpTele->setOldValue($objForm->GetValue("o_EmpTele"));
		if (!$this->EmpCodi->FldIsDetailKey && $this->CurrentAction <> "gridadd" && $this->CurrentAction <> "add")
			$this->EmpCodi->setFormValue($objForm->GetValue("x_EmpCodi"));
	}

	// Restore form values
	function RestoreFormValues() {
		global $objForm;
		if ($this->CurrentAction <> "gridadd" && $this->CurrentAction <> "add")
			$this->EmpCodi->CurrentValue = $this->EmpCodi->FormValue;
		$this->EmpCarg->CurrentValue = $this->EmpCarg->FormValue;
		$this->EmpFIng->CurrentValue = $this->EmpFIng->FormValue;
		$this->EmpFIng->CurrentValue = ew_UnFormatDateTime($this->EmpFIng->CurrentValue, 7);
		$this->EmpFSal->CurrentValue = $this->EmpFSal->FormValue;
		$this->EmpFSal->CurrentValue = ew_UnFormatDateTime($this->EmpFSal->CurrentValue, 7);
		$this->EmpSala->CurrentValue = $this->EmpSala->FormValue;
		$this->EmpNomb->CurrentValue = $this->EmpNomb->FormValue;
		$this->EmpApel->CurrentValue = $this->EmpApel->FormValue;
		$this->EmpCedu->CurrentValue = $this->EmpCedu->FormValue;
		$this->EmpTele->CurrentValue = $this->EmpTele->FormValue;
	}

	// Load recordset
	function LoadRecordset($offset = -1, $rowcnt = -1) {

		// Load List page SQL
		$sSql = $this->SelectSQL();
		$conn = &$this->Connection();

		// Load recordset
		$dbtype = ew_GetConnectionType($this->DBID);
		if ($this->UseSelectLimit) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			if ($dbtype == "MSSQL") {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset, array("_hasOrderBy" => trim($this->getOrderBy()) || trim($this->getSessionOrderBy())));
			} else {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset);
			}
			$conn->raiseErrorFn = '';
		} else {
			$rs = ew_LoadRecordset($sSql, $conn);
		}

		// Call Recordset Selected event
		$this->Recordset_Selected($rs);
		return $rs;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->EmpCodi->setDbValue($rs->fields('EmpCodi'));
		$this->EmpCarg->setDbValue($rs->fields('EmpCarg'));
		$this->EmpFIng->setDbValue($rs->fields('EmpFIng'));
		$this->EmpFSal->setDbValue($rs->fields('EmpFSal'));
		$this->EmpSala->setDbValue($rs->fields('EmpSala'));
		$this->EmpNomb->setDbValue($rs->fields('EmpNomb'));
		$this->EmpApel->setDbValue($rs->fields('EmpApel'));
		$this->EmpCedu->setDbValue($rs->fields('EmpCedu'));
		$this->EmpTele->setDbValue($rs->fields('EmpTele'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->EmpCodi->DbValue = $row['EmpCodi'];
		$this->EmpCarg->DbValue = $row['EmpCarg'];
		$this->EmpFIng->DbValue = $row['EmpFIng'];
		$this->EmpFSal->DbValue = $row['EmpFSal'];
		$this->EmpSala->DbValue = $row['EmpSala'];
		$this->EmpNomb->DbValue = $row['EmpNomb'];
		$this->EmpApel->DbValue = $row['EmpApel'];
		$this->EmpCedu->DbValue = $row['EmpCedu'];
		$this->EmpTele->DbValue = $row['EmpTele'];
	}

	// Load old record
	function LoadOldRecord() {

		// Load key values from Session
		$bValidKey = TRUE;
		if (strval($this->getKey("EmpCodi")) <> "")
			$this->EmpCodi->CurrentValue = $this->getKey("EmpCodi"); // EmpCodi
		else
			$bValidKey = FALSE;

		// Load old recordset
		if ($bValidKey) {
			$this->CurrentFilter = $this->KeyFilter();
			$sSql = $this->SQL();
			$conn = &$this->Connection();
			$this->OldRecordset = ew_LoadRecordset($sSql, $conn);
			$this->LoadRowValues($this->OldRecordset); // Load row values
		} else {
			$this->OldRecordset = NULL;
		}
		return $bValidKey;
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		$this->ViewUrl = $this->GetViewUrl();
		$this->EditUrl = $this->GetEditUrl();
		$this->InlineEditUrl = $this->GetInlineEditUrl();
		$this->CopyUrl = $this->GetCopyUrl();
		$this->InlineCopyUrl = $this->GetInlineCopyUrl();
		$this->DeleteUrl = $this->GetDeleteUrl();

		// Convert decimal values if posted back
		if ($this->EmpSala->FormValue == $this->EmpSala->CurrentValue && is_numeric(ew_StrToFloat($this->EmpSala->CurrentValue)))
			$this->EmpSala->CurrentValue = ew_StrToFloat($this->EmpSala->CurrentValue);

		// Call Row_Rendering event
		$this->Row_Rendering();

		// Common render codes for all row types
		// EmpCodi
		// EmpCarg
		// EmpFIng
		// EmpFSal
		// EmpSala

		$this->EmpSala->CellCssStyle = "width: 50px;";

		// EmpNomb
		// EmpApel
		// EmpCedu
		// EmpTele

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// EmpCodi
		$this->EmpCodi->ViewValue = $this->EmpCodi->CurrentValue;
		$this->EmpCodi->ViewCustomAttributes = "";

		// EmpCarg
		$this->EmpCarg->ViewValue = $this->EmpCarg->CurrentValue;
		$this->EmpCarg->ViewCustomAttributes = "";

		// EmpFIng
		$this->EmpFIng->ViewValue = $this->EmpFIng->CurrentValue;
		$this->EmpFIng->ViewValue = ew_FormatDateTime($this->EmpFIng->ViewValue, 7);
		$this->EmpFIng->ViewCustomAttributes = "";

		// EmpFSal
		$this->EmpFSal->ViewValue = $this->EmpFSal->CurrentValue;
		$this->EmpFSal->ViewValue = ew_FormatDateTime($this->EmpFSal->ViewValue, 7);
		$this->EmpFSal->ViewCustomAttributes = "";

		// EmpSala
		$this->EmpSala->ViewValue = $this->EmpSala->CurrentValue;
		$this->EmpSala->ViewValue = ew_FormatNumber($this->EmpSala->ViewValue, 0, -2, -2, -2);
		$this->EmpSala->ViewCustomAttributes = "";

		// EmpNomb
		$this->EmpNomb->ViewValue = $this->EmpNomb->CurrentValue;
		$this->EmpNomb->ViewCustomAttributes = "";

		// EmpApel
		$this->EmpApel->ViewValue = $this->EmpApel->CurrentValue;
		$this->EmpApel->ViewCustomAttributes = "";

		// EmpCedu
		$this->EmpCedu->ViewValue = $this->EmpCedu->CurrentValue;
		$this->EmpCedu->ViewCustomAttributes = "";

		// EmpTele
		$this->EmpTele->ViewValue = $this->EmpTele->CurrentValue;
		$this->EmpTele->ViewCustomAttributes = "";

			// EmpCarg
			$this->EmpCarg->LinkCustomAttributes = "";
			$this->EmpCarg->HrefValue = "";
			$this->EmpCarg->TooltipValue = "";

			// EmpFIng
			$this->EmpFIng->LinkCustomAttributes = "";
			$this->EmpFIng->HrefValue = "";
			$this->EmpFIng->TooltipValue = "";

			// EmpFSal
			$this->EmpFSal->LinkCustomAttributes = "";
			$this->EmpFSal->HrefValue = "";
			$this->EmpFSal->TooltipValue = "";

			// EmpSala
			$this->EmpSala->LinkCustomAttributes = "";
			$this->EmpSala->HrefValue = "";
			$this->EmpSala->TooltipValue = "";

			// EmpNomb
			$this->EmpNomb->LinkCustomAttributes = "";
			$this->EmpNomb->HrefValue = "";
			$this->EmpNomb->TooltipValue = "";

			// EmpApel
			$this->EmpApel->LinkCustomAttributes = "";
			$this->EmpApel->HrefValue = "";
			$this->EmpApel->TooltipValue = "";

			// EmpCedu
			$this->EmpCedu->LinkCustomAttributes = "";
			$this->EmpCedu->HrefValue = "";
			$this->EmpCedu->TooltipValue = "";

			// EmpTele
			$this->EmpTele->LinkCustomAttributes = "";
			$this->EmpTele->HrefValue = "";
			$this->EmpTele->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_ADD) { // Add row

			// EmpCarg
			$this->EmpCarg->EditAttrs["class"] = "form-control";
			$this->EmpCarg->EditCustomAttributes = "";
			$this->EmpCarg->EditValue = ew_HtmlEncode($this->EmpCarg->CurrentValue);
			$this->EmpCarg->PlaceHolder = ew_RemoveHtml($this->EmpCarg->FldCaption());

			// EmpFIng
			$this->EmpFIng->EditAttrs["class"] = "form-control";
			$this->EmpFIng->EditCustomAttributes = "";
			$this->EmpFIng->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->EmpFIng->CurrentValue, 7));
			$this->EmpFIng->PlaceHolder = ew_RemoveHtml($this->EmpFIng->FldCaption());

			// EmpFSal
			$this->EmpFSal->EditAttrs["class"] = "form-control";
			$this->EmpFSal->EditCustomAttributes = "";
			$this->EmpFSal->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->EmpFSal->CurrentValue, 7));
			$this->EmpFSal->PlaceHolder = ew_RemoveHtml($this->EmpFSal->FldCaption());

			// EmpSala
			$this->EmpSala->EditAttrs["class"] = "form-control";
			$this->EmpSala->EditCustomAttributes = "";
			$this->EmpSala->EditValue = ew_HtmlEncode($this->EmpSala->CurrentValue);
			$this->EmpSala->PlaceHolder = ew_RemoveHtml($this->EmpSala->FldCaption());
			if (strval($this->EmpSala->EditValue) <> "" && is_numeric($this->EmpSala->EditValue)) {
			$this->EmpSala->EditValue = ew_FormatNumber($this->EmpSala->EditValue, -2, -2, -2, -2);
			$this->EmpSala->OldValue = $this->EmpSala->EditValue;
			}

			// EmpNomb
			$this->EmpNomb->EditAttrs["class"] = "form-control";
			$this->EmpNomb->EditCustomAttributes = "";
			$this->EmpNomb->EditValue = ew_HtmlEncode($this->EmpNomb->CurrentValue);
			$this->EmpNomb->PlaceHolder = ew_RemoveHtml($this->EmpNomb->FldCaption());

			// EmpApel
			$this->EmpApel->EditAttrs["class"] = "form-control";
			$this->EmpApel->EditCustomAttributes = "";
			$this->EmpApel->EditValue = ew_HtmlEncode($this->EmpApel->CurrentValue);
			$this->EmpApel->PlaceHolder = ew_RemoveHtml($this->EmpApel->FldCaption());

			// EmpCedu
			$this->EmpCedu->EditAttrs["class"] = "form-control";
			$this->EmpCedu->EditCustomAttributes = "";
			$this->EmpCedu->EditValue = ew_HtmlEncode($this->EmpCedu->CurrentValue);
			$this->EmpCedu->PlaceHolder = ew_RemoveHtml($this->EmpCedu->FldCaption());

			// EmpTele
			$this->EmpTele->EditAttrs["class"] = "form-control";
			$this->EmpTele->EditCustomAttributes = "";
			$this->EmpTele->EditValue = ew_HtmlEncode($this->EmpTele->CurrentValue);
			$this->EmpTele->PlaceHolder = ew_RemoveHtml($this->EmpTele->FldCaption());

			// Edit refer script
			// EmpCarg

			$this->EmpCarg->HrefValue = "";

			// EmpFIng
			$this->EmpFIng->HrefValue = "";

			// EmpFSal
			$this->EmpFSal->HrefValue = "";

			// EmpSala
			$this->EmpSala->HrefValue = "";

			// EmpNomb
			$this->EmpNomb->HrefValue = "";

			// EmpApel
			$this->EmpApel->HrefValue = "";

			// EmpCedu
			$this->EmpCedu->HrefValue = "";

			// EmpTele
			$this->EmpTele->HrefValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_EDIT) { // Edit row

			// EmpCarg
			$this->EmpCarg->EditAttrs["class"] = "form-control";
			$this->EmpCarg->EditCustomAttributes = "";
			$this->EmpCarg->EditValue = ew_HtmlEncode($this->EmpCarg->CurrentValue);
			$this->EmpCarg->PlaceHolder = ew_RemoveHtml($this->EmpCarg->FldCaption());

			// EmpFIng
			$this->EmpFIng->EditAttrs["class"] = "form-control";
			$this->EmpFIng->EditCustomAttributes = "";
			$this->EmpFIng->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->EmpFIng->CurrentValue, 7));
			$this->EmpFIng->PlaceHolder = ew_RemoveHtml($this->EmpFIng->FldCaption());

			// EmpFSal
			$this->EmpFSal->EditAttrs["class"] = "form-control";
			$this->EmpFSal->EditCustomAttributes = "";
			$this->EmpFSal->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->EmpFSal->CurrentValue, 7));
			$this->EmpFSal->PlaceHolder = ew_RemoveHtml($this->EmpFSal->FldCaption());

			// EmpSala
			$this->EmpSala->EditAttrs["class"] = "form-control";
			$this->EmpSala->EditCustomAttributes = "";
			$this->EmpSala->EditValue = ew_HtmlEncode($this->EmpSala->CurrentValue);
			$this->EmpSala->PlaceHolder = ew_RemoveHtml($this->EmpSala->FldCaption());
			if (strval($this->EmpSala->EditValue) <> "" && is_numeric($this->EmpSala->EditValue)) {
			$this->EmpSala->EditValue = ew_FormatNumber($this->EmpSala->EditValue, -2, -2, -2, -2);
			$this->EmpSala->OldValue = $this->EmpSala->EditValue;
			}

			// EmpNomb
			$this->EmpNomb->EditAttrs["class"] = "form-control";
			$this->EmpNomb->EditCustomAttributes = "";
			$this->EmpNomb->EditValue = ew_HtmlEncode($this->EmpNomb->CurrentValue);
			$this->EmpNomb->PlaceHolder = ew_RemoveHtml($this->EmpNomb->FldCaption());

			// EmpApel
			$this->EmpApel->EditAttrs["class"] = "form-control";
			$this->EmpApel->EditCustomAttributes = "";
			$this->EmpApel->EditValue = ew_HtmlEncode($this->EmpApel->CurrentValue);
			$this->EmpApel->PlaceHolder = ew_RemoveHtml($this->EmpApel->FldCaption());

			// EmpCedu
			$this->EmpCedu->EditAttrs["class"] = "form-control";
			$this->EmpCedu->EditCustomAttributes = "";
			$this->EmpCedu->EditValue = ew_HtmlEncode($this->EmpCedu->CurrentValue);
			$this->EmpCedu->PlaceHolder = ew_RemoveHtml($this->EmpCedu->FldCaption());

			// EmpTele
			$this->EmpTele->EditAttrs["class"] = "form-control";
			$this->EmpTele->EditCustomAttributes = "";
			$this->EmpTele->EditValue = ew_HtmlEncode($this->EmpTele->CurrentValue);
			$this->EmpTele->PlaceHolder = ew_RemoveHtml($this->EmpTele->FldCaption());

			// Edit refer script
			// EmpCarg

			$this->EmpCarg->HrefValue = "";

			// EmpFIng
			$this->EmpFIng->HrefValue = "";

			// EmpFSal
			$this->EmpFSal->HrefValue = "";

			// EmpSala
			$this->EmpSala->HrefValue = "";

			// EmpNomb
			$this->EmpNomb->HrefValue = "";

			// EmpApel
			$this->EmpApel->HrefValue = "";

			// EmpCedu
			$this->EmpCedu->HrefValue = "";

			// EmpTele
			$this->EmpTele->HrefValue = "";
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate form
	function ValidateForm() {
		global $Language, $gsFormError;

		// Initialize form error message
		$gsFormError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return ($gsFormError == "");
		if (!$this->EmpCarg->FldIsDetailKey && !is_null($this->EmpCarg->FormValue) && $this->EmpCarg->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->EmpCarg->FldCaption(), $this->EmpCarg->ReqErrMsg));
		}
		if (!$this->EmpFIng->FldIsDetailKey && !is_null($this->EmpFIng->FormValue) && $this->EmpFIng->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->EmpFIng->FldCaption(), $this->EmpFIng->ReqErrMsg));
		}
		if (!ew_CheckEuroDate($this->EmpFIng->FormValue)) {
			ew_AddMessage($gsFormError, $this->EmpFIng->FldErrMsg());
		}
		if (!ew_CheckEuroDate($this->EmpFSal->FormValue)) {
			ew_AddMessage($gsFormError, $this->EmpFSal->FldErrMsg());
		}
		if (!$this->EmpSala->FldIsDetailKey && !is_null($this->EmpSala->FormValue) && $this->EmpSala->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->EmpSala->FldCaption(), $this->EmpSala->ReqErrMsg));
		}
		if (!ew_CheckNumber($this->EmpSala->FormValue)) {
			ew_AddMessage($gsFormError, $this->EmpSala->FldErrMsg());
		}
		if (!$this->EmpCedu->FldIsDetailKey && !is_null($this->EmpCedu->FormValue) && $this->EmpCedu->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->EmpCedu->FldCaption(), $this->EmpCedu->ReqErrMsg));
		}
		if (!ew_CheckInteger($this->EmpTele->FormValue)) {
			ew_AddMessage($gsFormError, $this->EmpTele->FldErrMsg());
		}

		// Return validate result
		$ValidateForm = ($gsFormError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateForm = $ValidateForm && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsFormError, $sFormCustomError);
		}
		return $ValidateForm;
	}

	//
	// Delete records based on current filter
	//
	function DeleteRows() {
		global $Language, $Security;
		if (!$Security->CanDelete()) {
			$this->setFailureMessage($Language->Phrase("NoDeletePermission")); // No delete permission
			return FALSE;
		}
		$DeleteRows = TRUE;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE) {
			return FALSE;
		} elseif ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
			$rs->Close();
			return FALSE;

		//} else {
		//	$this->LoadRowValues($rs); // Load row values

		}
		$rows = ($rs) ? $rs->GetRows() : array();

		// Clone old rows
		$rsold = $rows;
		if ($rs)
			$rs->Close();

		// Call row deleting event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$DeleteRows = $this->Row_Deleting($row);
				if (!$DeleteRows) break;
			}
		}
		if ($DeleteRows) {
			$sKey = "";
			foreach ($rsold as $row) {
				$sThisKey = "";
				if ($sThisKey <> "") $sThisKey .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
				$sThisKey .= $row['EmpCodi'];
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				$DeleteRows = $this->Delete($row); // Delete
				$conn->raiseErrorFn = '';
				if ($DeleteRows === FALSE)
					break;
				if ($sKey <> "") $sKey .= ", ";
				$sKey .= $sThisKey;
			}
		} else {

			// Set up error message
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("DeleteCancelled"));
			}
		}
		if ($DeleteRows) {
		} else {
		}

		// Call Row Deleted event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$this->Row_Deleted($row);
			}
		}
		return $DeleteRows;
	}

	// Update record based on key values
	function EditRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$conn = &$this->Connection();
		if ($this->EmpCarg->CurrentValue <> "") { // Check field with unique index
			$sFilterChk = "(\"EmpCarg\" = '" . ew_AdjustSql($this->EmpCarg->CurrentValue, $this->DBID) . "')";
			$sFilterChk .= " AND NOT (" . $sFilter . ")";
			$this->CurrentFilter = $sFilterChk;
			$sSqlChk = $this->SQL();
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			$rsChk = $conn->Execute($sSqlChk);
			$conn->raiseErrorFn = '';
			if ($rsChk === FALSE) {
				return FALSE;
			} elseif (!$rsChk->EOF) {
				$sIdxErrMsg = str_replace("%f", $this->EmpCarg->FldCaption(), $Language->Phrase("DupIndex"));
				$sIdxErrMsg = str_replace("%v", $this->EmpCarg->CurrentValue, $sIdxErrMsg);
				$this->setFailureMessage($sIdxErrMsg);
				$rsChk->Close();
				return FALSE;
			}
			$rsChk->Close();
		}
		if ($this->EmpCedu->CurrentValue <> "") { // Check field with unique index
			$sFilterChk = "(\"EmpCedu\" = '" . ew_AdjustSql($this->EmpCedu->CurrentValue, $this->DBID) . "')";
			$sFilterChk .= " AND NOT (" . $sFilter . ")";
			$this->CurrentFilter = $sFilterChk;
			$sSqlChk = $this->SQL();
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			$rsChk = $conn->Execute($sSqlChk);
			$conn->raiseErrorFn = '';
			if ($rsChk === FALSE) {
				return FALSE;
			} elseif (!$rsChk->EOF) {
				$sIdxErrMsg = str_replace("%f", $this->EmpCedu->FldCaption(), $Language->Phrase("DupIndex"));
				$sIdxErrMsg = str_replace("%v", $this->EmpCedu->CurrentValue, $sIdxErrMsg);
				$this->setFailureMessage($sIdxErrMsg);
				$rsChk->Close();
				return FALSE;
			}
			$rsChk->Close();
		}
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE)
			return FALSE;
		if ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record message
			$EditRow = FALSE; // Update Failed
		} else {

			// Save old values
			$rsold = &$rs->fields;
			$this->LoadDbValues($rsold);
			$rsnew = array();

			// EmpCarg
			$this->EmpCarg->SetDbValueDef($rsnew, $this->EmpCarg->CurrentValue, "", $this->EmpCarg->ReadOnly);

			// EmpFIng
			$this->EmpFIng->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->EmpFIng->CurrentValue, 7), ew_CurrentDate(), $this->EmpFIng->ReadOnly);

			// EmpFSal
			$this->EmpFSal->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->EmpFSal->CurrentValue, 7), NULL, $this->EmpFSal->ReadOnly);

			// EmpSala
			$this->EmpSala->SetDbValueDef($rsnew, $this->EmpSala->CurrentValue, 0, $this->EmpSala->ReadOnly);

			// EmpNomb
			$this->EmpNomb->SetDbValueDef($rsnew, $this->EmpNomb->CurrentValue, NULL, $this->EmpNomb->ReadOnly);

			// EmpApel
			$this->EmpApel->SetDbValueDef($rsnew, $this->EmpApel->CurrentValue, NULL, $this->EmpApel->ReadOnly);

			// EmpCedu
			$this->EmpCedu->SetDbValueDef($rsnew, $this->EmpCedu->CurrentValue, NULL, $this->EmpCedu->ReadOnly);

			// EmpTele
			$this->EmpTele->SetDbValueDef($rsnew, $this->EmpTele->CurrentValue, NULL, $this->EmpTele->ReadOnly);

			// Call Row Updating event
			$bUpdateRow = $this->Row_Updating($rsold, $rsnew);
			if ($bUpdateRow) {
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				if (count($rsnew) > 0)
					$EditRow = $this->Update($rsnew, "", $rsold);
				else
					$EditRow = TRUE; // No field to update
				$conn->raiseErrorFn = '';
				if ($EditRow) {
				}
			} else {
				if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

					// Use the message, do nothing
				} elseif ($this->CancelMessage <> "") {
					$this->setFailureMessage($this->CancelMessage);
					$this->CancelMessage = "";
				} else {
					$this->setFailureMessage($Language->Phrase("UpdateCancelled"));
				}
				$EditRow = FALSE;
			}
		}

		// Call Row_Updated event
		if ($EditRow)
			$this->Row_Updated($rsold, $rsnew);
		$rs->Close();
		return $EditRow;
	}

	// Add record
	function AddRow($rsold = NULL) {
		global $Language, $Security;
		if ($this->EmpCarg->CurrentValue <> "") { // Check field with unique index
			$sFilter = "(EmpCarg = '" . ew_AdjustSql($this->EmpCarg->CurrentValue, $this->DBID) . "')";
			$rsChk = $this->LoadRs($sFilter);
			if ($rsChk && !$rsChk->EOF) {
				$sIdxErrMsg = str_replace("%f", $this->EmpCarg->FldCaption(), $Language->Phrase("DupIndex"));
				$sIdxErrMsg = str_replace("%v", $this->EmpCarg->CurrentValue, $sIdxErrMsg);
				$this->setFailureMessage($sIdxErrMsg);
				$rsChk->Close();
				return FALSE;
			}
		}
		if ($this->EmpCedu->CurrentValue <> "") { // Check field with unique index
			$sFilter = "(EmpCedu = '" . ew_AdjustSql($this->EmpCedu->CurrentValue, $this->DBID) . "')";
			$rsChk = $this->LoadRs($sFilter);
			if ($rsChk && !$rsChk->EOF) {
				$sIdxErrMsg = str_replace("%f", $this->EmpCedu->FldCaption(), $Language->Phrase("DupIndex"));
				$sIdxErrMsg = str_replace("%v", $this->EmpCedu->CurrentValue, $sIdxErrMsg);
				$this->setFailureMessage($sIdxErrMsg);
				$rsChk->Close();
				return FALSE;
			}
		}
		$conn = &$this->Connection();

		// Load db values from rsold
		if ($rsold) {
			$this->LoadDbValues($rsold);
		}
		$rsnew = array();

		// EmpCarg
		$this->EmpCarg->SetDbValueDef($rsnew, $this->EmpCarg->CurrentValue, "", FALSE);

		// EmpFIng
		$this->EmpFIng->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->EmpFIng->CurrentValue, 7), ew_CurrentDate(), FALSE);

		// EmpFSal
		$this->EmpFSal->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->EmpFSal->CurrentValue, 7), NULL, FALSE);

		// EmpSala
		$this->EmpSala->SetDbValueDef($rsnew, $this->EmpSala->CurrentValue, 0, FALSE);

		// EmpNomb
		$this->EmpNomb->SetDbValueDef($rsnew, $this->EmpNomb->CurrentValue, NULL, FALSE);

		// EmpApel
		$this->EmpApel->SetDbValueDef($rsnew, $this->EmpApel->CurrentValue, NULL, FALSE);

		// EmpCedu
		$this->EmpCedu->SetDbValueDef($rsnew, $this->EmpCedu->CurrentValue, NULL, FALSE);

		// EmpTele
		$this->EmpTele->SetDbValueDef($rsnew, $this->EmpTele->CurrentValue, NULL, FALSE);

		// Call Row Inserting event
		$rs = ($rsold == NULL) ? NULL : $rsold->fields;
		$bInsertRow = $this->Row_Inserting($rs, $rsnew);
		if ($bInsertRow) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			$AddRow = $this->Insert($rsnew);
			$conn->raiseErrorFn = '';
			if ($AddRow) {

				// Get insert id if necessary
				$this->EmpCodi->setDbValue($conn->GetOne("SELECT currval('\"Empl_EmpCodi_seq\"'::regclass)"));
				$rsnew['EmpCodi'] = $this->EmpCodi->DbValue;
			}
		} else {
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("InsertCancelled"));
			}
			$AddRow = FALSE;
		}
		if ($AddRow) {

			// Call Row Inserted event
			$rs = ($rsold == NULL) ? NULL : $rsold->fields;
			$this->Row_Inserted($rs, $rsnew);
		}
		return $AddRow;
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$url = preg_replace('/\?cmd=reset(all){0,1}$/i', '', $url); // Remove cmd=reset / cmd=resetall
		$Breadcrumb->Add("list", $this->TableVar, $url, "", $this->TableVar, TRUE);
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}

	// ListOptions Load event
	function ListOptions_Load() {

		// Example:
		//$opt = &$this->ListOptions->Add("new");
		//$opt->Header = "xxx";
		//$opt->OnLeft = TRUE; // Link on left
		//$opt->MoveTo(0); // Move to first column

	}

	// ListOptions Rendered event
	function ListOptions_Rendered() {

		// Example: 
		//$this->ListOptions->Items["new"]->Body = "xxx";

	}

	// Row Custom Action event
	function Row_CustomAction($action, $row) {

		// Return FALSE to abort
		return TRUE;
	}

	// Page Exporting event
	// $this->ExportDoc = export document object
	function Page_Exporting() {

		//$this->ExportDoc->Text = "my header"; // Export header
		//return FALSE; // Return FALSE to skip default export and use Row_Export event

		return TRUE; // Return TRUE to use default export and skip Row_Export event
	}

	// Row Export event
	// $this->ExportDoc = export document object
	function Row_Export($rs) {

	    //$this->ExportDoc->Text .= "my content"; // Build HTML with field value: $rs["MyField"] or $this->MyField->ViewValue
	}

	// Page Exported event
	// $this->ExportDoc = export document object
	function Page_Exported() {

		//$this->ExportDoc->Text .= "my footer"; // Export footer
		//echo $this->ExportDoc->Text;

	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($Empl_list)) $Empl_list = new cEmpl_list();

// Page init
$Empl_list->Page_Init();

// Page main
$Empl_list->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$Empl_list->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "list";
var CurrentForm = fEmpllist = new ew_Form("fEmpllist", "list");
fEmpllist.FormKeyCountName = '<?php echo $Empl_list->FormKeyCountName ?>';

// Validate form
fEmpllist.Validate = function() {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.GetForm(), $fobj = $(fobj);
	if ($fobj.find("#a_confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.FormKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = $fobj.find("#a_list").val() == "gridinsert";
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
		var checkrow = (gridinsert) ? !this.EmptyRow(infix) : true;
		if (checkrow) {
			addcnt++;
			elm = this.GetElements("x" + infix + "_EmpCarg");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $Empl->EmpCarg->FldCaption(), $Empl->EmpCarg->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_EmpFIng");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $Empl->EmpFIng->FldCaption(), $Empl->EmpFIng->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_EmpFIng");
			if (elm && !ew_CheckEuroDate(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($Empl->EmpFIng->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_EmpFSal");
			if (elm && !ew_CheckEuroDate(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($Empl->EmpFSal->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_EmpSala");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $Empl->EmpSala->FldCaption(), $Empl->EmpSala->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_EmpSala");
			if (elm && !ew_CheckNumber(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($Empl->EmpSala->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_EmpCedu");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $Empl->EmpCedu->FldCaption(), $Empl->EmpCedu->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_EmpTele");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($Empl->EmpTele->FldErrMsg()) ?>");

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
		} // End Grid Add checking
	}
	if (gridinsert && addcnt == 0) { // No row added
		ew_Alert(ewLanguage.Phrase("NoAddRecord"));
		return false;
	}
	return true;
}

// Check empty row
fEmpllist.EmptyRow = function(infix) {
	var fobj = this.Form;
	if (ew_ValueChanged(fobj, infix, "EmpCarg", false)) return false;
	if (ew_ValueChanged(fobj, infix, "EmpFIng", false)) return false;
	if (ew_ValueChanged(fobj, infix, "EmpFSal", false)) return false;
	if (ew_ValueChanged(fobj, infix, "EmpSala", false)) return false;
	if (ew_ValueChanged(fobj, infix, "EmpNomb", false)) return false;
	if (ew_ValueChanged(fobj, infix, "EmpApel", false)) return false;
	if (ew_ValueChanged(fobj, infix, "EmpCedu", false)) return false;
	if (ew_ValueChanged(fobj, infix, "EmpTele", false)) return false;
	return true;
}

// Form_CustomValidate event
fEmpllist.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fEmpllist.ValidateRequired = true;
<?php } else { ?>
fEmpllist.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
// Form object for search

var CurrentSearchForm = fEmpllistsrch = new ew_Form("fEmpllistsrch");
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php if ($Empl_list->TotalRecs > 0 && $Empl_list->ExportOptions->Visible()) { ?>
<?php $Empl_list->ExportOptions->Render("body") ?>
<?php } ?>
<?php if ($Empl_list->SearchOptions->Visible()) { ?>
<?php $Empl_list->SearchOptions->Render("body") ?>
<?php } ?>
<?php if ($Empl_list->FilterOptions->Visible()) { ?>
<?php $Empl_list->FilterOptions->Render("body") ?>
<?php } ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php
if ($Empl->CurrentAction == "gridadd") {
	$Empl->CurrentFilter = "0=1";
	$Empl_list->StartRec = 1;
	$Empl_list->DisplayRecs = $Empl->GridAddRowCount;
	$Empl_list->TotalRecs = $Empl_list->DisplayRecs;
	$Empl_list->StopRec = $Empl_list->DisplayRecs;
} else {
	$bSelectLimit = $Empl_list->UseSelectLimit;
	if ($bSelectLimit) {
		if ($Empl_list->TotalRecs <= 0)
			$Empl_list->TotalRecs = $Empl->SelectRecordCount();
	} else {
		if (!$Empl_list->Recordset && ($Empl_list->Recordset = $Empl_list->LoadRecordset()))
			$Empl_list->TotalRecs = $Empl_list->Recordset->RecordCount();
	}
	$Empl_list->StartRec = 1;
	if ($Empl_list->DisplayRecs <= 0 || ($Empl->Export <> "" && $Empl->ExportAll)) // Display all records
		$Empl_list->DisplayRecs = $Empl_list->TotalRecs;
	if (!($Empl->Export <> "" && $Empl->ExportAll))
		$Empl_list->SetUpStartRec(); // Set up start record position
	if ($bSelectLimit)
		$Empl_list->Recordset = $Empl_list->LoadRecordset($Empl_list->StartRec-1, $Empl_list->DisplayRecs);

	// Set no record found message
	if ($Empl->CurrentAction == "" && $Empl_list->TotalRecs == 0) {
		if (!$Security->CanList())
			$Empl_list->setWarningMessage($Language->Phrase("NoPermission"));
		if ($Empl_list->SearchWhere == "0=101")
			$Empl_list->setWarningMessage($Language->Phrase("EnterSearchCriteria"));
		else
			$Empl_list->setWarningMessage($Language->Phrase("NoRecord"));
	}
}
$Empl_list->RenderOtherOptions();
?>
<?php if ($Security->CanSearch()) { ?>
<?php if ($Empl->Export == "" && $Empl->CurrentAction == "") { ?>
<form name="fEmpllistsrch" id="fEmpllistsrch" class="form-inline ewForm" action="<?php echo ew_CurrentPage() ?>">
<?php $SearchPanelClass = ($Empl_list->SearchWhere <> "") ? " in" : " in"; ?>
<div id="fEmpllistsrch_SearchPanel" class="ewSearchPanel collapse<?php echo $SearchPanelClass ?>">
<input type="hidden" name="cmd" value="search">
<input type="hidden" name="t" value="Empl">
	<div class="ewBasicSearch">
<div id="xsr_1" class="ewRow">
	<div class="ewQuickSearch input-group">
	<input type="text" name="<?php echo EW_TABLE_BASIC_SEARCH ?>" id="<?php echo EW_TABLE_BASIC_SEARCH ?>" class="form-control" value="<?php echo ew_HtmlEncode($Empl_list->BasicSearch->getKeyword()) ?>" placeholder="<?php echo ew_HtmlEncode($Language->Phrase("Search")) ?>">
	<input type="hidden" name="<?php echo EW_TABLE_BASIC_SEARCH_TYPE ?>" id="<?php echo EW_TABLE_BASIC_SEARCH_TYPE ?>" value="<?php echo ew_HtmlEncode($Empl_list->BasicSearch->getType()) ?>">
	<div class="input-group-btn">
		<button type="button" data-toggle="dropdown" class="btn btn-default"><span id="searchtype"><?php echo $Empl_list->BasicSearch->getTypeNameShort() ?></span><span class="caret"></span></button>
		<ul class="dropdown-menu pull-right" role="menu">
			<li<?php if ($Empl_list->BasicSearch->getType() == "") echo " class=\"active\""; ?>><a href="javascript:void(0);" onclick="ew_SetSearchType(this)"><?php echo $Language->Phrase("QuickSearchAuto") ?></a></li>
			<li<?php if ($Empl_list->BasicSearch->getType() == "=") echo " class=\"active\""; ?>><a href="javascript:void(0);" onclick="ew_SetSearchType(this,'=')"><?php echo $Language->Phrase("QuickSearchExact") ?></a></li>
			<li<?php if ($Empl_list->BasicSearch->getType() == "AND") echo " class=\"active\""; ?>><a href="javascript:void(0);" onclick="ew_SetSearchType(this,'AND')"><?php echo $Language->Phrase("QuickSearchAll") ?></a></li>
			<li<?php if ($Empl_list->BasicSearch->getType() == "OR") echo " class=\"active\""; ?>><a href="javascript:void(0);" onclick="ew_SetSearchType(this,'OR')"><?php echo $Language->Phrase("QuickSearchAny") ?></a></li>
		</ul>
	<button class="btn btn-primary ewButton" name="btnsubmit" id="btnsubmit" type="submit"><?php echo $Language->Phrase("QuickSearchBtn") ?></button>
	</div>
	</div>
</div>
	</div>
</div>
</form>
<?php } ?>
<?php } ?>
<?php $Empl_list->ShowPageHeader(); ?>
<?php
$Empl_list->ShowMessage();
?>
<?php if ($Empl_list->TotalRecs > 0 || $Empl->CurrentAction <> "") { ?>
<div class="panel panel-default ewGrid">
<form name="fEmpllist" id="fEmpllist" class="form-inline ewForm ewListForm" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($Empl_list->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $Empl_list->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="Empl">
<div id="gmp_Empl" class="<?php if (ew_IsResponsiveLayout()) { echo "table-responsive "; } ?>ewGridMiddlePanel">
<?php if ($Empl_list->TotalRecs > 0) { ?>
<table id="tbl_Empllist" class="table ewTable">
<?php echo $Empl->TableCustomInnerHtml ?>
<thead><!-- Table header -->
	<tr class="ewTableHeader">
<?php

// Header row
$Empl_list->RowType = EW_ROWTYPE_HEADER;

// Render list options
$Empl_list->RenderListOptions();

// Render list options (header, left)
$Empl_list->ListOptions->Render("header", "left");
?>
<?php if ($Empl->EmpCarg->Visible) { // EmpCarg ?>
	<?php if ($Empl->SortUrl($Empl->EmpCarg) == "") { ?>
		<th data-name="EmpCarg"><div id="elh_Empl_EmpCarg" class="Empl_EmpCarg"><div class="ewTableHeaderCaption"><?php echo $Empl->EmpCarg->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="EmpCarg"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $Empl->SortUrl($Empl->EmpCarg) ?>',2);"><div id="elh_Empl_EmpCarg" class="Empl_EmpCarg">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $Empl->EmpCarg->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($Empl->EmpCarg->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($Empl->EmpCarg->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($Empl->EmpFIng->Visible) { // EmpFIng ?>
	<?php if ($Empl->SortUrl($Empl->EmpFIng) == "") { ?>
		<th data-name="EmpFIng"><div id="elh_Empl_EmpFIng" class="Empl_EmpFIng"><div class="ewTableHeaderCaption"><?php echo $Empl->EmpFIng->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="EmpFIng"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $Empl->SortUrl($Empl->EmpFIng) ?>',2);"><div id="elh_Empl_EmpFIng" class="Empl_EmpFIng">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $Empl->EmpFIng->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($Empl->EmpFIng->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($Empl->EmpFIng->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($Empl->EmpFSal->Visible) { // EmpFSal ?>
	<?php if ($Empl->SortUrl($Empl->EmpFSal) == "") { ?>
		<th data-name="EmpFSal"><div id="elh_Empl_EmpFSal" class="Empl_EmpFSal"><div class="ewTableHeaderCaption"><?php echo $Empl->EmpFSal->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="EmpFSal"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $Empl->SortUrl($Empl->EmpFSal) ?>',2);"><div id="elh_Empl_EmpFSal" class="Empl_EmpFSal">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $Empl->EmpFSal->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($Empl->EmpFSal->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($Empl->EmpFSal->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($Empl->EmpSala->Visible) { // EmpSala ?>
	<?php if ($Empl->SortUrl($Empl->EmpSala) == "") { ?>
		<th data-name="EmpSala"><div id="elh_Empl_EmpSala" class="Empl_EmpSala"><div class="ewTableHeaderCaption" style="width: 50px;"><?php echo $Empl->EmpSala->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="EmpSala"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $Empl->SortUrl($Empl->EmpSala) ?>',2);"><div id="elh_Empl_EmpSala" class="Empl_EmpSala">
			<div class="ewTableHeaderBtn" style="width: 50px;"><span class="ewTableHeaderCaption"><?php echo $Empl->EmpSala->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($Empl->EmpSala->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($Empl->EmpSala->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($Empl->EmpNomb->Visible) { // EmpNomb ?>
	<?php if ($Empl->SortUrl($Empl->EmpNomb) == "") { ?>
		<th data-name="EmpNomb"><div id="elh_Empl_EmpNomb" class="Empl_EmpNomb"><div class="ewTableHeaderCaption"><?php echo $Empl->EmpNomb->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="EmpNomb"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $Empl->SortUrl($Empl->EmpNomb) ?>',2);"><div id="elh_Empl_EmpNomb" class="Empl_EmpNomb">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $Empl->EmpNomb->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($Empl->EmpNomb->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($Empl->EmpNomb->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($Empl->EmpApel->Visible) { // EmpApel ?>
	<?php if ($Empl->SortUrl($Empl->EmpApel) == "") { ?>
		<th data-name="EmpApel"><div id="elh_Empl_EmpApel" class="Empl_EmpApel"><div class="ewTableHeaderCaption"><?php echo $Empl->EmpApel->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="EmpApel"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $Empl->SortUrl($Empl->EmpApel) ?>',2);"><div id="elh_Empl_EmpApel" class="Empl_EmpApel">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $Empl->EmpApel->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($Empl->EmpApel->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($Empl->EmpApel->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($Empl->EmpCedu->Visible) { // EmpCedu ?>
	<?php if ($Empl->SortUrl($Empl->EmpCedu) == "") { ?>
		<th data-name="EmpCedu"><div id="elh_Empl_EmpCedu" class="Empl_EmpCedu"><div class="ewTableHeaderCaption"><?php echo $Empl->EmpCedu->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="EmpCedu"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $Empl->SortUrl($Empl->EmpCedu) ?>',2);"><div id="elh_Empl_EmpCedu" class="Empl_EmpCedu">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $Empl->EmpCedu->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($Empl->EmpCedu->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($Empl->EmpCedu->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($Empl->EmpTele->Visible) { // EmpTele ?>
	<?php if ($Empl->SortUrl($Empl->EmpTele) == "") { ?>
		<th data-name="EmpTele"><div id="elh_Empl_EmpTele" class="Empl_EmpTele"><div class="ewTableHeaderCaption"><?php echo $Empl->EmpTele->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="EmpTele"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $Empl->SortUrl($Empl->EmpTele) ?>',2);"><div id="elh_Empl_EmpTele" class="Empl_EmpTele">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $Empl->EmpTele->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($Empl->EmpTele->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($Empl->EmpTele->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php

// Render list options (header, right)
$Empl_list->ListOptions->Render("header", "right");
?>
	</tr>
</thead>
<tbody>
<?php
if ($Empl->ExportAll && $Empl->Export <> "") {
	$Empl_list->StopRec = $Empl_list->TotalRecs;
} else {

	// Set the last record to display
	if ($Empl_list->TotalRecs > $Empl_list->StartRec + $Empl_list->DisplayRecs - 1)
		$Empl_list->StopRec = $Empl_list->StartRec + $Empl_list->DisplayRecs - 1;
	else
		$Empl_list->StopRec = $Empl_list->TotalRecs;
}

// Restore number of post back records
if ($objForm) {
	$objForm->Index = -1;
	if ($objForm->HasValue($Empl_list->FormKeyCountName) && ($Empl->CurrentAction == "gridadd" || $Empl->CurrentAction == "gridedit" || $Empl->CurrentAction == "F")) {
		$Empl_list->KeyCount = $objForm->GetValue($Empl_list->FormKeyCountName);
		$Empl_list->StopRec = $Empl_list->StartRec + $Empl_list->KeyCount - 1;
	}
}
$Empl_list->RecCnt = $Empl_list->StartRec - 1;
if ($Empl_list->Recordset && !$Empl_list->Recordset->EOF) {
	$Empl_list->Recordset->MoveFirst();
	$bSelectLimit = $Empl_list->UseSelectLimit;
	if (!$bSelectLimit && $Empl_list->StartRec > 1)
		$Empl_list->Recordset->Move($Empl_list->StartRec - 1);
} elseif (!$Empl->AllowAddDeleteRow && $Empl_list->StopRec == 0) {
	$Empl_list->StopRec = $Empl->GridAddRowCount;
}

// Initialize aggregate
$Empl->RowType = EW_ROWTYPE_AGGREGATEINIT;
$Empl->ResetAttrs();
$Empl_list->RenderRow();
if ($Empl->CurrentAction == "gridadd")
	$Empl_list->RowIndex = 0;
if ($Empl->CurrentAction == "gridedit")
	$Empl_list->RowIndex = 0;
while ($Empl_list->RecCnt < $Empl_list->StopRec) {
	$Empl_list->RecCnt++;
	if (intval($Empl_list->RecCnt) >= intval($Empl_list->StartRec)) {
		$Empl_list->RowCnt++;
		if ($Empl->CurrentAction == "gridadd" || $Empl->CurrentAction == "gridedit" || $Empl->CurrentAction == "F") {
			$Empl_list->RowIndex++;
			$objForm->Index = $Empl_list->RowIndex;
			if ($objForm->HasValue($Empl_list->FormActionName))
				$Empl_list->RowAction = strval($objForm->GetValue($Empl_list->FormActionName));
			elseif ($Empl->CurrentAction == "gridadd")
				$Empl_list->RowAction = "insert";
			else
				$Empl_list->RowAction = "";
		}

		// Set up key count
		$Empl_list->KeyCount = $Empl_list->RowIndex;

		// Init row class and style
		$Empl->ResetAttrs();
		$Empl->CssClass = "";
		if ($Empl->CurrentAction == "gridadd") {
			$Empl_list->LoadDefaultValues(); // Load default values
		} else {
			$Empl_list->LoadRowValues($Empl_list->Recordset); // Load row values
		}
		$Empl->RowType = EW_ROWTYPE_VIEW; // Render view
		if ($Empl->CurrentAction == "gridadd") // Grid add
			$Empl->RowType = EW_ROWTYPE_ADD; // Render add
		if ($Empl->CurrentAction == "gridadd" && $Empl->EventCancelled && !$objForm->HasValue("k_blankrow")) // Insert failed
			$Empl_list->RestoreCurrentRowFormValues($Empl_list->RowIndex); // Restore form values
		if ($Empl->CurrentAction == "gridedit") { // Grid edit
			if ($Empl->EventCancelled) {
				$Empl_list->RestoreCurrentRowFormValues($Empl_list->RowIndex); // Restore form values
			}
			if ($Empl_list->RowAction == "insert")
				$Empl->RowType = EW_ROWTYPE_ADD; // Render add
			else
				$Empl->RowType = EW_ROWTYPE_EDIT; // Render edit
		}
		if ($Empl->CurrentAction == "gridedit" && ($Empl->RowType == EW_ROWTYPE_EDIT || $Empl->RowType == EW_ROWTYPE_ADD) && $Empl->EventCancelled) // Update failed
			$Empl_list->RestoreCurrentRowFormValues($Empl_list->RowIndex); // Restore form values
		if ($Empl->RowType == EW_ROWTYPE_EDIT) // Edit row
			$Empl_list->EditRowCnt++;

		// Set up row id / data-rowindex
		$Empl->RowAttrs = array_merge($Empl->RowAttrs, array('data-rowindex'=>$Empl_list->RowCnt, 'id'=>'r' . $Empl_list->RowCnt . '_Empl', 'data-rowtype'=>$Empl->RowType));

		// Render row
		$Empl_list->RenderRow();

		// Render list options
		$Empl_list->RenderListOptions();

		// Skip delete row / empty row for confirm page
		if ($Empl_list->RowAction <> "delete" && $Empl_list->RowAction <> "insertdelete" && !($Empl_list->RowAction == "insert" && $Empl->CurrentAction == "F" && $Empl_list->EmptyRow())) {
?>
	<tr<?php echo $Empl->RowAttributes() ?>>
<?php

// Render list options (body, left)
$Empl_list->ListOptions->Render("body", "left", $Empl_list->RowCnt);
?>
	<?php if ($Empl->EmpCarg->Visible) { // EmpCarg ?>
		<td data-name="EmpCarg"<?php echo $Empl->EmpCarg->CellAttributes() ?>>
<?php if ($Empl->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $Empl_list->RowCnt ?>_Empl_EmpCarg" class="form-group Empl_EmpCarg">
<input type="text" data-table="Empl" data-field="x_EmpCarg" name="x<?php echo $Empl_list->RowIndex ?>_EmpCarg" id="x<?php echo $Empl_list->RowIndex ?>_EmpCarg" size="30" placeholder="<?php echo ew_HtmlEncode($Empl->EmpCarg->getPlaceHolder()) ?>" value="<?php echo $Empl->EmpCarg->EditValue ?>"<?php echo $Empl->EmpCarg->EditAttributes() ?>>
</span>
<input type="hidden" data-table="Empl" data-field="x_EmpCarg" name="o<?php echo $Empl_list->RowIndex ?>_EmpCarg" id="o<?php echo $Empl_list->RowIndex ?>_EmpCarg" value="<?php echo ew_HtmlEncode($Empl->EmpCarg->OldValue) ?>">
<?php } ?>
<?php if ($Empl->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $Empl_list->RowCnt ?>_Empl_EmpCarg" class="form-group Empl_EmpCarg">
<input type="text" data-table="Empl" data-field="x_EmpCarg" name="x<?php echo $Empl_list->RowIndex ?>_EmpCarg" id="x<?php echo $Empl_list->RowIndex ?>_EmpCarg" size="30" placeholder="<?php echo ew_HtmlEncode($Empl->EmpCarg->getPlaceHolder()) ?>" value="<?php echo $Empl->EmpCarg->EditValue ?>"<?php echo $Empl->EmpCarg->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($Empl->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $Empl_list->RowCnt ?>_Empl_EmpCarg" class="Empl_EmpCarg">
<span<?php echo $Empl->EmpCarg->ViewAttributes() ?>>
<?php echo $Empl->EmpCarg->ListViewValue() ?></span>
</span>
<?php } ?>
<a id="<?php echo $Empl_list->PageObjName . "_row_" . $Empl_list->RowCnt ?>"></a></td>
	<?php } ?>
<?php if ($Empl->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<input type="hidden" data-table="Empl" data-field="x_EmpCodi" name="x<?php echo $Empl_list->RowIndex ?>_EmpCodi" id="x<?php echo $Empl_list->RowIndex ?>_EmpCodi" value="<?php echo ew_HtmlEncode($Empl->EmpCodi->CurrentValue) ?>">
<input type="hidden" data-table="Empl" data-field="x_EmpCodi" name="o<?php echo $Empl_list->RowIndex ?>_EmpCodi" id="o<?php echo $Empl_list->RowIndex ?>_EmpCodi" value="<?php echo ew_HtmlEncode($Empl->EmpCodi->OldValue) ?>">
<?php } ?>
<?php if ($Empl->RowType == EW_ROWTYPE_EDIT || $Empl->CurrentMode == "edit") { ?>
<input type="hidden" data-table="Empl" data-field="x_EmpCodi" name="x<?php echo $Empl_list->RowIndex ?>_EmpCodi" id="x<?php echo $Empl_list->RowIndex ?>_EmpCodi" value="<?php echo ew_HtmlEncode($Empl->EmpCodi->CurrentValue) ?>">
<?php } ?>
	<?php if ($Empl->EmpFIng->Visible) { // EmpFIng ?>
		<td data-name="EmpFIng"<?php echo $Empl->EmpFIng->CellAttributes() ?>>
<?php if ($Empl->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $Empl_list->RowCnt ?>_Empl_EmpFIng" class="form-group Empl_EmpFIng">
<input type="text" data-table="Empl" data-field="x_EmpFIng" data-format="7" name="x<?php echo $Empl_list->RowIndex ?>_EmpFIng" id="x<?php echo $Empl_list->RowIndex ?>_EmpFIng" placeholder="<?php echo ew_HtmlEncode($Empl->EmpFIng->getPlaceHolder()) ?>" value="<?php echo $Empl->EmpFIng->EditValue ?>"<?php echo $Empl->EmpFIng->EditAttributes() ?>>
<?php if (!$Empl->EmpFIng->ReadOnly && !$Empl->EmpFIng->Disabled && !isset($Empl->EmpFIng->EditAttrs["readonly"]) && !isset($Empl->EmpFIng->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("fEmpllist", "x<?php echo $Empl_list->RowIndex ?>_EmpFIng", "%d/%m/%Y");
</script>
<?php } ?>
</span>
<input type="hidden" data-table="Empl" data-field="x_EmpFIng" name="o<?php echo $Empl_list->RowIndex ?>_EmpFIng" id="o<?php echo $Empl_list->RowIndex ?>_EmpFIng" value="<?php echo ew_HtmlEncode($Empl->EmpFIng->OldValue) ?>">
<?php } ?>
<?php if ($Empl->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $Empl_list->RowCnt ?>_Empl_EmpFIng" class="form-group Empl_EmpFIng">
<input type="text" data-table="Empl" data-field="x_EmpFIng" data-format="7" name="x<?php echo $Empl_list->RowIndex ?>_EmpFIng" id="x<?php echo $Empl_list->RowIndex ?>_EmpFIng" placeholder="<?php echo ew_HtmlEncode($Empl->EmpFIng->getPlaceHolder()) ?>" value="<?php echo $Empl->EmpFIng->EditValue ?>"<?php echo $Empl->EmpFIng->EditAttributes() ?>>
<?php if (!$Empl->EmpFIng->ReadOnly && !$Empl->EmpFIng->Disabled && !isset($Empl->EmpFIng->EditAttrs["readonly"]) && !isset($Empl->EmpFIng->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("fEmpllist", "x<?php echo $Empl_list->RowIndex ?>_EmpFIng", "%d/%m/%Y");
</script>
<?php } ?>
</span>
<?php } ?>
<?php if ($Empl->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $Empl_list->RowCnt ?>_Empl_EmpFIng" class="Empl_EmpFIng">
<span<?php echo $Empl->EmpFIng->ViewAttributes() ?>>
<?php echo $Empl->EmpFIng->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($Empl->EmpFSal->Visible) { // EmpFSal ?>
		<td data-name="EmpFSal"<?php echo $Empl->EmpFSal->CellAttributes() ?>>
<?php if ($Empl->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $Empl_list->RowCnt ?>_Empl_EmpFSal" class="form-group Empl_EmpFSal">
<input type="text" data-table="Empl" data-field="x_EmpFSal" data-format="7" name="x<?php echo $Empl_list->RowIndex ?>_EmpFSal" id="x<?php echo $Empl_list->RowIndex ?>_EmpFSal" placeholder="<?php echo ew_HtmlEncode($Empl->EmpFSal->getPlaceHolder()) ?>" value="<?php echo $Empl->EmpFSal->EditValue ?>"<?php echo $Empl->EmpFSal->EditAttributes() ?>>
<?php if (!$Empl->EmpFSal->ReadOnly && !$Empl->EmpFSal->Disabled && !isset($Empl->EmpFSal->EditAttrs["readonly"]) && !isset($Empl->EmpFSal->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("fEmpllist", "x<?php echo $Empl_list->RowIndex ?>_EmpFSal", "%d/%m/%Y");
</script>
<?php } ?>
</span>
<input type="hidden" data-table="Empl" data-field="x_EmpFSal" name="o<?php echo $Empl_list->RowIndex ?>_EmpFSal" id="o<?php echo $Empl_list->RowIndex ?>_EmpFSal" value="<?php echo ew_HtmlEncode($Empl->EmpFSal->OldValue) ?>">
<?php } ?>
<?php if ($Empl->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $Empl_list->RowCnt ?>_Empl_EmpFSal" class="form-group Empl_EmpFSal">
<input type="text" data-table="Empl" data-field="x_EmpFSal" data-format="7" name="x<?php echo $Empl_list->RowIndex ?>_EmpFSal" id="x<?php echo $Empl_list->RowIndex ?>_EmpFSal" placeholder="<?php echo ew_HtmlEncode($Empl->EmpFSal->getPlaceHolder()) ?>" value="<?php echo $Empl->EmpFSal->EditValue ?>"<?php echo $Empl->EmpFSal->EditAttributes() ?>>
<?php if (!$Empl->EmpFSal->ReadOnly && !$Empl->EmpFSal->Disabled && !isset($Empl->EmpFSal->EditAttrs["readonly"]) && !isset($Empl->EmpFSal->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("fEmpllist", "x<?php echo $Empl_list->RowIndex ?>_EmpFSal", "%d/%m/%Y");
</script>
<?php } ?>
</span>
<?php } ?>
<?php if ($Empl->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $Empl_list->RowCnt ?>_Empl_EmpFSal" class="Empl_EmpFSal">
<span<?php echo $Empl->EmpFSal->ViewAttributes() ?>>
<?php echo $Empl->EmpFSal->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($Empl->EmpSala->Visible) { // EmpSala ?>
		<td data-name="EmpSala"<?php echo $Empl->EmpSala->CellAttributes() ?>>
<?php if ($Empl->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $Empl_list->RowCnt ?>_Empl_EmpSala" class="form-group Empl_EmpSala">
<input type="text" data-table="Empl" data-field="x_EmpSala" name="x<?php echo $Empl_list->RowIndex ?>_EmpSala" id="x<?php echo $Empl_list->RowIndex ?>_EmpSala" size="30" placeholder="<?php echo ew_HtmlEncode($Empl->EmpSala->getPlaceHolder()) ?>" value="<?php echo $Empl->EmpSala->EditValue ?>"<?php echo $Empl->EmpSala->EditAttributes() ?>>
</span>
<input type="hidden" data-table="Empl" data-field="x_EmpSala" name="o<?php echo $Empl_list->RowIndex ?>_EmpSala" id="o<?php echo $Empl_list->RowIndex ?>_EmpSala" value="<?php echo ew_HtmlEncode($Empl->EmpSala->OldValue) ?>">
<?php } ?>
<?php if ($Empl->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $Empl_list->RowCnt ?>_Empl_EmpSala" class="form-group Empl_EmpSala">
<input type="text" data-table="Empl" data-field="x_EmpSala" name="x<?php echo $Empl_list->RowIndex ?>_EmpSala" id="x<?php echo $Empl_list->RowIndex ?>_EmpSala" size="30" placeholder="<?php echo ew_HtmlEncode($Empl->EmpSala->getPlaceHolder()) ?>" value="<?php echo $Empl->EmpSala->EditValue ?>"<?php echo $Empl->EmpSala->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($Empl->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $Empl_list->RowCnt ?>_Empl_EmpSala" class="Empl_EmpSala">
<span<?php echo $Empl->EmpSala->ViewAttributes() ?>>
<?php echo $Empl->EmpSala->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($Empl->EmpNomb->Visible) { // EmpNomb ?>
		<td data-name="EmpNomb"<?php echo $Empl->EmpNomb->CellAttributes() ?>>
<?php if ($Empl->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $Empl_list->RowCnt ?>_Empl_EmpNomb" class="form-group Empl_EmpNomb">
<input type="text" data-table="Empl" data-field="x_EmpNomb" name="x<?php echo $Empl_list->RowIndex ?>_EmpNomb" id="x<?php echo $Empl_list->RowIndex ?>_EmpNomb" size="30" placeholder="<?php echo ew_HtmlEncode($Empl->EmpNomb->getPlaceHolder()) ?>" value="<?php echo $Empl->EmpNomb->EditValue ?>"<?php echo $Empl->EmpNomb->EditAttributes() ?>>
</span>
<input type="hidden" data-table="Empl" data-field="x_EmpNomb" name="o<?php echo $Empl_list->RowIndex ?>_EmpNomb" id="o<?php echo $Empl_list->RowIndex ?>_EmpNomb" value="<?php echo ew_HtmlEncode($Empl->EmpNomb->OldValue) ?>">
<?php } ?>
<?php if ($Empl->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $Empl_list->RowCnt ?>_Empl_EmpNomb" class="form-group Empl_EmpNomb">
<input type="text" data-table="Empl" data-field="x_EmpNomb" name="x<?php echo $Empl_list->RowIndex ?>_EmpNomb" id="x<?php echo $Empl_list->RowIndex ?>_EmpNomb" size="30" placeholder="<?php echo ew_HtmlEncode($Empl->EmpNomb->getPlaceHolder()) ?>" value="<?php echo $Empl->EmpNomb->EditValue ?>"<?php echo $Empl->EmpNomb->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($Empl->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $Empl_list->RowCnt ?>_Empl_EmpNomb" class="Empl_EmpNomb">
<span<?php echo $Empl->EmpNomb->ViewAttributes() ?>>
<?php echo $Empl->EmpNomb->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($Empl->EmpApel->Visible) { // EmpApel ?>
		<td data-name="EmpApel"<?php echo $Empl->EmpApel->CellAttributes() ?>>
<?php if ($Empl->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $Empl_list->RowCnt ?>_Empl_EmpApel" class="form-group Empl_EmpApel">
<input type="text" data-table="Empl" data-field="x_EmpApel" name="x<?php echo $Empl_list->RowIndex ?>_EmpApel" id="x<?php echo $Empl_list->RowIndex ?>_EmpApel" size="30" placeholder="<?php echo ew_HtmlEncode($Empl->EmpApel->getPlaceHolder()) ?>" value="<?php echo $Empl->EmpApel->EditValue ?>"<?php echo $Empl->EmpApel->EditAttributes() ?>>
</span>
<input type="hidden" data-table="Empl" data-field="x_EmpApel" name="o<?php echo $Empl_list->RowIndex ?>_EmpApel" id="o<?php echo $Empl_list->RowIndex ?>_EmpApel" value="<?php echo ew_HtmlEncode($Empl->EmpApel->OldValue) ?>">
<?php } ?>
<?php if ($Empl->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $Empl_list->RowCnt ?>_Empl_EmpApel" class="form-group Empl_EmpApel">
<input type="text" data-table="Empl" data-field="x_EmpApel" name="x<?php echo $Empl_list->RowIndex ?>_EmpApel" id="x<?php echo $Empl_list->RowIndex ?>_EmpApel" size="30" placeholder="<?php echo ew_HtmlEncode($Empl->EmpApel->getPlaceHolder()) ?>" value="<?php echo $Empl->EmpApel->EditValue ?>"<?php echo $Empl->EmpApel->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($Empl->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $Empl_list->RowCnt ?>_Empl_EmpApel" class="Empl_EmpApel">
<span<?php echo $Empl->EmpApel->ViewAttributes() ?>>
<?php echo $Empl->EmpApel->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($Empl->EmpCedu->Visible) { // EmpCedu ?>
		<td data-name="EmpCedu"<?php echo $Empl->EmpCedu->CellAttributes() ?>>
<?php if ($Empl->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $Empl_list->RowCnt ?>_Empl_EmpCedu" class="form-group Empl_EmpCedu">
<input type="text" data-table="Empl" data-field="x_EmpCedu" name="x<?php echo $Empl_list->RowIndex ?>_EmpCedu" id="x<?php echo $Empl_list->RowIndex ?>_EmpCedu" size="30" placeholder="<?php echo ew_HtmlEncode($Empl->EmpCedu->getPlaceHolder()) ?>" value="<?php echo $Empl->EmpCedu->EditValue ?>"<?php echo $Empl->EmpCedu->EditAttributes() ?>>
</span>
<input type="hidden" data-table="Empl" data-field="x_EmpCedu" name="o<?php echo $Empl_list->RowIndex ?>_EmpCedu" id="o<?php echo $Empl_list->RowIndex ?>_EmpCedu" value="<?php echo ew_HtmlEncode($Empl->EmpCedu->OldValue) ?>">
<?php } ?>
<?php if ($Empl->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $Empl_list->RowCnt ?>_Empl_EmpCedu" class="form-group Empl_EmpCedu">
<input type="text" data-table="Empl" data-field="x_EmpCedu" name="x<?php echo $Empl_list->RowIndex ?>_EmpCedu" id="x<?php echo $Empl_list->RowIndex ?>_EmpCedu" size="30" placeholder="<?php echo ew_HtmlEncode($Empl->EmpCedu->getPlaceHolder()) ?>" value="<?php echo $Empl->EmpCedu->EditValue ?>"<?php echo $Empl->EmpCedu->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($Empl->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $Empl_list->RowCnt ?>_Empl_EmpCedu" class="Empl_EmpCedu">
<span<?php echo $Empl->EmpCedu->ViewAttributes() ?>>
<?php echo $Empl->EmpCedu->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($Empl->EmpTele->Visible) { // EmpTele ?>
		<td data-name="EmpTele"<?php echo $Empl->EmpTele->CellAttributes() ?>>
<?php if ($Empl->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $Empl_list->RowCnt ?>_Empl_EmpTele" class="form-group Empl_EmpTele">
<input type="text" data-table="Empl" data-field="x_EmpTele" name="x<?php echo $Empl_list->RowIndex ?>_EmpTele" id="x<?php echo $Empl_list->RowIndex ?>_EmpTele" size="30" placeholder="<?php echo ew_HtmlEncode($Empl->EmpTele->getPlaceHolder()) ?>" value="<?php echo $Empl->EmpTele->EditValue ?>"<?php echo $Empl->EmpTele->EditAttributes() ?>>
</span>
<input type="hidden" data-table="Empl" data-field="x_EmpTele" name="o<?php echo $Empl_list->RowIndex ?>_EmpTele" id="o<?php echo $Empl_list->RowIndex ?>_EmpTele" value="<?php echo ew_HtmlEncode($Empl->EmpTele->OldValue) ?>">
<?php } ?>
<?php if ($Empl->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $Empl_list->RowCnt ?>_Empl_EmpTele" class="form-group Empl_EmpTele">
<input type="text" data-table="Empl" data-field="x_EmpTele" name="x<?php echo $Empl_list->RowIndex ?>_EmpTele" id="x<?php echo $Empl_list->RowIndex ?>_EmpTele" size="30" placeholder="<?php echo ew_HtmlEncode($Empl->EmpTele->getPlaceHolder()) ?>" value="<?php echo $Empl->EmpTele->EditValue ?>"<?php echo $Empl->EmpTele->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($Empl->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $Empl_list->RowCnt ?>_Empl_EmpTele" class="Empl_EmpTele">
<span<?php echo $Empl->EmpTele->ViewAttributes() ?>>
<?php echo $Empl->EmpTele->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
<?php

// Render list options (body, right)
$Empl_list->ListOptions->Render("body", "right", $Empl_list->RowCnt);
?>
	</tr>
<?php if ($Empl->RowType == EW_ROWTYPE_ADD || $Empl->RowType == EW_ROWTYPE_EDIT) { ?>
<script type="text/javascript">
fEmpllist.UpdateOpts(<?php echo $Empl_list->RowIndex ?>);
</script>
<?php } ?>
<?php
	}
	} // End delete row checking
	if ($Empl->CurrentAction <> "gridadd")
		if (!$Empl_list->Recordset->EOF) $Empl_list->Recordset->MoveNext();
}
?>
<?php
	if ($Empl->CurrentAction == "gridadd" || $Empl->CurrentAction == "gridedit") {
		$Empl_list->RowIndex = '$rowindex$';
		$Empl_list->LoadDefaultValues();

		// Set row properties
		$Empl->ResetAttrs();
		$Empl->RowAttrs = array_merge($Empl->RowAttrs, array('data-rowindex'=>$Empl_list->RowIndex, 'id'=>'r0_Empl', 'data-rowtype'=>EW_ROWTYPE_ADD));
		ew_AppendClass($Empl->RowAttrs["class"], "ewTemplate");
		$Empl->RowType = EW_ROWTYPE_ADD;

		// Render row
		$Empl_list->RenderRow();

		// Render list options
		$Empl_list->RenderListOptions();
		$Empl_list->StartRowCnt = 0;
?>
	<tr<?php echo $Empl->RowAttributes() ?>>
<?php

// Render list options (body, left)
$Empl_list->ListOptions->Render("body", "left", $Empl_list->RowIndex);
?>
	<?php if ($Empl->EmpCarg->Visible) { // EmpCarg ?>
		<td data-name="EmpCarg">
<span id="el$rowindex$_Empl_EmpCarg" class="form-group Empl_EmpCarg">
<input type="text" data-table="Empl" data-field="x_EmpCarg" name="x<?php echo $Empl_list->RowIndex ?>_EmpCarg" id="x<?php echo $Empl_list->RowIndex ?>_EmpCarg" size="30" placeholder="<?php echo ew_HtmlEncode($Empl->EmpCarg->getPlaceHolder()) ?>" value="<?php echo $Empl->EmpCarg->EditValue ?>"<?php echo $Empl->EmpCarg->EditAttributes() ?>>
</span>
<input type="hidden" data-table="Empl" data-field="x_EmpCarg" name="o<?php echo $Empl_list->RowIndex ?>_EmpCarg" id="o<?php echo $Empl_list->RowIndex ?>_EmpCarg" value="<?php echo ew_HtmlEncode($Empl->EmpCarg->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($Empl->EmpFIng->Visible) { // EmpFIng ?>
		<td data-name="EmpFIng">
<span id="el$rowindex$_Empl_EmpFIng" class="form-group Empl_EmpFIng">
<input type="text" data-table="Empl" data-field="x_EmpFIng" data-format="7" name="x<?php echo $Empl_list->RowIndex ?>_EmpFIng" id="x<?php echo $Empl_list->RowIndex ?>_EmpFIng" placeholder="<?php echo ew_HtmlEncode($Empl->EmpFIng->getPlaceHolder()) ?>" value="<?php echo $Empl->EmpFIng->EditValue ?>"<?php echo $Empl->EmpFIng->EditAttributes() ?>>
<?php if (!$Empl->EmpFIng->ReadOnly && !$Empl->EmpFIng->Disabled && !isset($Empl->EmpFIng->EditAttrs["readonly"]) && !isset($Empl->EmpFIng->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("fEmpllist", "x<?php echo $Empl_list->RowIndex ?>_EmpFIng", "%d/%m/%Y");
</script>
<?php } ?>
</span>
<input type="hidden" data-table="Empl" data-field="x_EmpFIng" name="o<?php echo $Empl_list->RowIndex ?>_EmpFIng" id="o<?php echo $Empl_list->RowIndex ?>_EmpFIng" value="<?php echo ew_HtmlEncode($Empl->EmpFIng->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($Empl->EmpFSal->Visible) { // EmpFSal ?>
		<td data-name="EmpFSal">
<span id="el$rowindex$_Empl_EmpFSal" class="form-group Empl_EmpFSal">
<input type="text" data-table="Empl" data-field="x_EmpFSal" data-format="7" name="x<?php echo $Empl_list->RowIndex ?>_EmpFSal" id="x<?php echo $Empl_list->RowIndex ?>_EmpFSal" placeholder="<?php echo ew_HtmlEncode($Empl->EmpFSal->getPlaceHolder()) ?>" value="<?php echo $Empl->EmpFSal->EditValue ?>"<?php echo $Empl->EmpFSal->EditAttributes() ?>>
<?php if (!$Empl->EmpFSal->ReadOnly && !$Empl->EmpFSal->Disabled && !isset($Empl->EmpFSal->EditAttrs["readonly"]) && !isset($Empl->EmpFSal->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("fEmpllist", "x<?php echo $Empl_list->RowIndex ?>_EmpFSal", "%d/%m/%Y");
</script>
<?php } ?>
</span>
<input type="hidden" data-table="Empl" data-field="x_EmpFSal" name="o<?php echo $Empl_list->RowIndex ?>_EmpFSal" id="o<?php echo $Empl_list->RowIndex ?>_EmpFSal" value="<?php echo ew_HtmlEncode($Empl->EmpFSal->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($Empl->EmpSala->Visible) { // EmpSala ?>
		<td data-name="EmpSala">
<span id="el$rowindex$_Empl_EmpSala" class="form-group Empl_EmpSala">
<input type="text" data-table="Empl" data-field="x_EmpSala" name="x<?php echo $Empl_list->RowIndex ?>_EmpSala" id="x<?php echo $Empl_list->RowIndex ?>_EmpSala" size="30" placeholder="<?php echo ew_HtmlEncode($Empl->EmpSala->getPlaceHolder()) ?>" value="<?php echo $Empl->EmpSala->EditValue ?>"<?php echo $Empl->EmpSala->EditAttributes() ?>>
</span>
<input type="hidden" data-table="Empl" data-field="x_EmpSala" name="o<?php echo $Empl_list->RowIndex ?>_EmpSala" id="o<?php echo $Empl_list->RowIndex ?>_EmpSala" value="<?php echo ew_HtmlEncode($Empl->EmpSala->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($Empl->EmpNomb->Visible) { // EmpNomb ?>
		<td data-name="EmpNomb">
<span id="el$rowindex$_Empl_EmpNomb" class="form-group Empl_EmpNomb">
<input type="text" data-table="Empl" data-field="x_EmpNomb" name="x<?php echo $Empl_list->RowIndex ?>_EmpNomb" id="x<?php echo $Empl_list->RowIndex ?>_EmpNomb" size="30" placeholder="<?php echo ew_HtmlEncode($Empl->EmpNomb->getPlaceHolder()) ?>" value="<?php echo $Empl->EmpNomb->EditValue ?>"<?php echo $Empl->EmpNomb->EditAttributes() ?>>
</span>
<input type="hidden" data-table="Empl" data-field="x_EmpNomb" name="o<?php echo $Empl_list->RowIndex ?>_EmpNomb" id="o<?php echo $Empl_list->RowIndex ?>_EmpNomb" value="<?php echo ew_HtmlEncode($Empl->EmpNomb->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($Empl->EmpApel->Visible) { // EmpApel ?>
		<td data-name="EmpApel">
<span id="el$rowindex$_Empl_EmpApel" class="form-group Empl_EmpApel">
<input type="text" data-table="Empl" data-field="x_EmpApel" name="x<?php echo $Empl_list->RowIndex ?>_EmpApel" id="x<?php echo $Empl_list->RowIndex ?>_EmpApel" size="30" placeholder="<?php echo ew_HtmlEncode($Empl->EmpApel->getPlaceHolder()) ?>" value="<?php echo $Empl->EmpApel->EditValue ?>"<?php echo $Empl->EmpApel->EditAttributes() ?>>
</span>
<input type="hidden" data-table="Empl" data-field="x_EmpApel" name="o<?php echo $Empl_list->RowIndex ?>_EmpApel" id="o<?php echo $Empl_list->RowIndex ?>_EmpApel" value="<?php echo ew_HtmlEncode($Empl->EmpApel->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($Empl->EmpCedu->Visible) { // EmpCedu ?>
		<td data-name="EmpCedu">
<span id="el$rowindex$_Empl_EmpCedu" class="form-group Empl_EmpCedu">
<input type="text" data-table="Empl" data-field="x_EmpCedu" name="x<?php echo $Empl_list->RowIndex ?>_EmpCedu" id="x<?php echo $Empl_list->RowIndex ?>_EmpCedu" size="30" placeholder="<?php echo ew_HtmlEncode($Empl->EmpCedu->getPlaceHolder()) ?>" value="<?php echo $Empl->EmpCedu->EditValue ?>"<?php echo $Empl->EmpCedu->EditAttributes() ?>>
</span>
<input type="hidden" data-table="Empl" data-field="x_EmpCedu" name="o<?php echo $Empl_list->RowIndex ?>_EmpCedu" id="o<?php echo $Empl_list->RowIndex ?>_EmpCedu" value="<?php echo ew_HtmlEncode($Empl->EmpCedu->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($Empl->EmpTele->Visible) { // EmpTele ?>
		<td data-name="EmpTele">
<span id="el$rowindex$_Empl_EmpTele" class="form-group Empl_EmpTele">
<input type="text" data-table="Empl" data-field="x_EmpTele" name="x<?php echo $Empl_list->RowIndex ?>_EmpTele" id="x<?php echo $Empl_list->RowIndex ?>_EmpTele" size="30" placeholder="<?php echo ew_HtmlEncode($Empl->EmpTele->getPlaceHolder()) ?>" value="<?php echo $Empl->EmpTele->EditValue ?>"<?php echo $Empl->EmpTele->EditAttributes() ?>>
</span>
<input type="hidden" data-table="Empl" data-field="x_EmpTele" name="o<?php echo $Empl_list->RowIndex ?>_EmpTele" id="o<?php echo $Empl_list->RowIndex ?>_EmpTele" value="<?php echo ew_HtmlEncode($Empl->EmpTele->OldValue) ?>">
</td>
	<?php } ?>
<?php

// Render list options (body, right)
$Empl_list->ListOptions->Render("body", "right", $Empl_list->RowCnt);
?>
<script type="text/javascript">
fEmpllist.UpdateOpts(<?php echo $Empl_list->RowIndex ?>);
</script>
	</tr>
<?php
}
?>
</tbody>
</table>
<?php } ?>
<?php if ($Empl->CurrentAction == "gridadd") { ?>
<input type="hidden" name="a_list" id="a_list" value="gridinsert">
<input type="hidden" name="<?php echo $Empl_list->FormKeyCountName ?>" id="<?php echo $Empl_list->FormKeyCountName ?>" value="<?php echo $Empl_list->KeyCount ?>">
<?php echo $Empl_list->MultiSelectKey ?>
<?php } ?>
<?php if ($Empl->CurrentAction == "gridedit") { ?>
<input type="hidden" name="a_list" id="a_list" value="gridupdate">
<input type="hidden" name="<?php echo $Empl_list->FormKeyCountName ?>" id="<?php echo $Empl_list->FormKeyCountName ?>" value="<?php echo $Empl_list->KeyCount ?>">
<?php echo $Empl_list->MultiSelectKey ?>
<?php } ?>
<?php if ($Empl->CurrentAction == "") { ?>
<input type="hidden" name="a_list" id="a_list" value="">
<?php } ?>
</div>
</form>
<?php

// Close recordset
if ($Empl_list->Recordset)
	$Empl_list->Recordset->Close();
?>
<div class="panel-footer ewGridLowerPanel">
<?php if ($Empl->CurrentAction <> "gridadd" && $Empl->CurrentAction <> "gridedit") { ?>
<form name="ewPagerForm" class="ewForm form-inline ewPagerForm" action="<?php echo ew_CurrentPage() ?>">
<?php if (!isset($Empl_list->Pager)) $Empl_list->Pager = new cPrevNextPager($Empl_list->StartRec, $Empl_list->DisplayRecs, $Empl_list->TotalRecs) ?>
<?php if ($Empl_list->Pager->RecordCount > 0) { ?>
<div class="ewPager">
<span><?php echo $Language->Phrase("Page") ?>&nbsp;</span>
<div class="ewPrevNext"><div class="input-group">
<div class="input-group-btn">
<!--first page button-->
	<?php if ($Empl_list->Pager->FirstButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerFirst") ?>" href="<?php echo $Empl_list->PageUrl() ?>start=<?php echo $Empl_list->Pager->FirstButton->Start ?>"><span class="icon-first ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerFirst") ?>"><span class="icon-first ewIcon"></span></a>
	<?php } ?>
<!--previous page button-->
	<?php if ($Empl_list->Pager->PrevButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerPrevious") ?>" href="<?php echo $Empl_list->PageUrl() ?>start=<?php echo $Empl_list->Pager->PrevButton->Start ?>"><span class="icon-prev ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerPrevious") ?>"><span class="icon-prev ewIcon"></span></a>
	<?php } ?>
</div>
<!--current page number-->
	<input class="form-control input-sm" type="text" name="<?php echo EW_TABLE_PAGE_NO ?>" value="<?php echo $Empl_list->Pager->CurrentPage ?>">
<div class="input-group-btn">
<!--next page button-->
	<?php if ($Empl_list->Pager->NextButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerNext") ?>" href="<?php echo $Empl_list->PageUrl() ?>start=<?php echo $Empl_list->Pager->NextButton->Start ?>"><span class="icon-next ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerNext") ?>"><span class="icon-next ewIcon"></span></a>
	<?php } ?>
<!--last page button-->
	<?php if ($Empl_list->Pager->LastButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerLast") ?>" href="<?php echo $Empl_list->PageUrl() ?>start=<?php echo $Empl_list->Pager->LastButton->Start ?>"><span class="icon-last ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerLast") ?>"><span class="icon-last ewIcon"></span></a>
	<?php } ?>
</div>
</div>
</div>
<span>&nbsp;<?php echo $Language->Phrase("of") ?>&nbsp;<?php echo $Empl_list->Pager->PageCount ?></span>
</div>
<div class="ewPager ewRec">
	<span><?php echo $Language->Phrase("Record") ?>&nbsp;<?php echo $Empl_list->Pager->FromIndex ?>&nbsp;<?php echo $Language->Phrase("To") ?>&nbsp;<?php echo $Empl_list->Pager->ToIndex ?>&nbsp;<?php echo $Language->Phrase("Of") ?>&nbsp;<?php echo $Empl_list->Pager->RecordCount ?></span>
</div>
<?php } ?>
</form>
<?php } ?>
<div class="ewListOtherOptions">
<?php
	foreach ($Empl_list->OtherOptions as &$option)
		$option->Render("body", "bottom");
?>
</div>
<div class="clearfix"></div>
</div>
</div>
<?php } ?>
<?php if ($Empl_list->TotalRecs == 0 && $Empl->CurrentAction == "") { // Show other options ?>
<div class="ewListOtherOptions">
<?php
	foreach ($Empl_list->OtherOptions as &$option) {
		$option->ButtonClass = "";
		$option->Render("body", "");
	}
?>
</div>
<div class="clearfix"></div>
<?php } ?>
<script type="text/javascript">
fEmpllistsrch.Init();
fEmpllistsrch.FilterList = <?php echo $Empl_list->GetFilterList() ?>;
fEmpllist.Init();
</script>
<?php
$Empl_list->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$Empl_list->Page_Terminate();
?>
