<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "OVeninfo.php" ?>
<?php include_once "Usuainfo.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$OVen_view = NULL; // Initialize page object first

class cOVen_view extends cOVen {

	// Page ID
	var $PageID = 'view';

	// Project ID
	var $ProjectID = "{04439FF7-B43F-460F-8514-F71C8FF9E679}";

	// Table name
	var $TableName = 'OVen';

	// Page object name
	var $PageObjName = 'OVen_view';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Page URLs
	var $AddUrl;
	var $EditUrl;
	var $CopyUrl;
	var $DeleteUrl;
	var $ViewUrl;
	var $ListUrl;

	// Export URLs
	var $ExportPrintUrl;
	var $ExportHtmlUrl;
	var $ExportExcelUrl;
	var $ExportWordUrl;
	var $ExportXmlUrl;
	var $ExportCsvUrl;
	var $ExportPdfUrl;

	// Custom export
	var $ExportExcelCustom = FALSE;
	var $ExportWordCustom = FALSE;
	var $ExportPdfCustom = FALSE;
	var $ExportEmailCustom = FALSE;

	// Update URLs
	var $InlineAddUrl;
	var $InlineCopyUrl;
	var $InlineEditUrl;
	var $GridAddUrl;
	var $GridEditUrl;
	var $MultiDeleteUrl;
	var $MultiUpdateUrl;

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (OVen)
		if (!isset($GLOBALS["OVen"]) || get_class($GLOBALS["OVen"]) == "cOVen") {
			$GLOBALS["OVen"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["OVen"];
		}
		$KeyUrl = "";
		if (@$_GET["OveCodi"] <> "") {
			$this->RecKey["OveCodi"] = $_GET["OveCodi"];
			$KeyUrl .= "&amp;OveCodi=" . urlencode($this->RecKey["OveCodi"]);
		}
		$this->ExportPrintUrl = $this->PageUrl() . "export=print" . $KeyUrl;
		$this->ExportHtmlUrl = $this->PageUrl() . "export=html" . $KeyUrl;
		$this->ExportExcelUrl = $this->PageUrl() . "export=excel" . $KeyUrl;
		$this->ExportWordUrl = $this->PageUrl() . "export=word" . $KeyUrl;
		$this->ExportXmlUrl = $this->PageUrl() . "export=xml" . $KeyUrl;
		$this->ExportCsvUrl = $this->PageUrl() . "export=csv" . $KeyUrl;
		$this->ExportPdfUrl = $this->PageUrl() . "export=pdf" . $KeyUrl;

		// Table object (Usua)
		if (!isset($GLOBALS['Usua'])) $GLOBALS['Usua'] = new cUsua();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'view', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'OVen', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (Usua)
		if (!isset($UserTable)) {
			$UserTable = new cUsua();
			$UserTableConn = Conn($UserTable->DBID);
		}

		// Export options
		$this->ExportOptions = new cListOptions();
		$this->ExportOptions->Tag = "div";
		$this->ExportOptions->TagClassName = "ewExportOption";

		// Other options
		$this->OtherOptions['action'] = new cListOptions();
		$this->OtherOptions['action']->Tag = "div";
		$this->OtherOptions['action']->TagClassName = "ewActionOption";
		$this->OtherOptions['detail'] = new cListOptions();
		$this->OtherOptions['detail']->Tag = "div";
		$this->OtherOptions['detail']->TagClassName = "ewDetailOption";
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanView()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage($Language->Phrase("NoPermission")); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("OVenlist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action
		$this->OveCodi->Visible = !$this->IsAdd() && !$this->IsCopy() && !$this->IsGridAdd();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $OVen;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($OVen);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $ExportOptions; // Export options
	var $OtherOptions = array(); // Other options
	var $DisplayRecs = 1;
	var $DbMasterFilter;
	var $DbDetailFilter;
	var $StartRec;
	var $StopRec;
	var $TotalRecs = 0;
	var $RecRange = 10;
	var $RecCnt;
	var $RecKey = array();
	var $Recordset;

	//
	// Page main
	//
	function Page_Main() {
		global $Language;
		$sReturnUrl = "";
		$bMatchRecord = FALSE;

		// Set up Breadcrumb
		if ($this->Export == "")
			$this->SetupBreadcrumb();
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET["OveCodi"] <> "") {
				$this->OveCodi->setQueryStringValue($_GET["OveCodi"]);
				$this->RecKey["OveCodi"] = $this->OveCodi->QueryStringValue;
			} elseif (@$_POST["OveCodi"] <> "") {
				$this->OveCodi->setFormValue($_POST["OveCodi"]);
				$this->RecKey["OveCodi"] = $this->OveCodi->FormValue;
			} else {
				$sReturnUrl = "OVenlist.php"; // Return to list
			}

			// Get action
			$this->CurrentAction = "I"; // Display form
			switch ($this->CurrentAction) {
				case "I": // Get a record to display
					if (!$this->LoadRow()) { // Load record based on key
						if ($this->getSuccessMessage() == "" && $this->getFailureMessage() == "")
							$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record message
						$sReturnUrl = "OVenlist.php"; // No matching record, return to list
					}
			}
		} else {
			$sReturnUrl = "OVenlist.php"; // Not page request, return to list
		}
		if ($sReturnUrl <> "")
			$this->Page_Terminate($sReturnUrl);

		// Render row
		$this->RowType = EW_ROWTYPE_VIEW;
		$this->ResetAttrs();
		$this->RenderRow();
	}

	// Set up other options
	function SetupOtherOptions() {
		global $Language, $Security;
		$options = &$this->OtherOptions;
		$option = &$options["action"];

		// Add
		$item = &$option->Add("add");
		$item->Body = "<a class=\"ewAction ewAdd\" title=\"" . ew_HtmlTitle($Language->Phrase("ViewPageAddLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("ViewPageAddLink")) . "\" href=\"" . ew_HtmlEncode($this->AddUrl) . "\">" . $Language->Phrase("ViewPageAddLink") . "</a>";
		$item->Visible = ($this->AddUrl <> "" && $Security->CanAdd());

		// Edit
		$item = &$option->Add("edit");
		$item->Body = "<a class=\"ewAction ewEdit\" title=\"" . ew_HtmlTitle($Language->Phrase("ViewPageEditLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("ViewPageEditLink")) . "\" href=\"" . ew_HtmlEncode($this->EditUrl) . "\">" . $Language->Phrase("ViewPageEditLink") . "</a>";
		$item->Visible = ($this->EditUrl <> "" && $Security->CanEdit());

		// Copy
		$item = &$option->Add("copy");
		$item->Body = "<a class=\"ewAction ewCopy\" title=\"" . ew_HtmlTitle($Language->Phrase("ViewPageCopyLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("ViewPageCopyLink")) . "\" href=\"" . ew_HtmlEncode($this->CopyUrl) . "\">" . $Language->Phrase("ViewPageCopyLink") . "</a>";
		$item->Visible = ($this->CopyUrl <> "" && $Security->CanAdd());

		// Delete
		$item = &$option->Add("delete");
		$item->Body = "<a class=\"ewAction ewDelete\" title=\"" . ew_HtmlTitle($Language->Phrase("ViewPageDeleteLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("ViewPageDeleteLink")) . "\" href=\"" . ew_HtmlEncode($this->DeleteUrl) . "\">" . $Language->Phrase("ViewPageDeleteLink") . "</a>";
		$item->Visible = ($this->DeleteUrl <> "" && $Security->CanDelete());

		// Set up action default
		$option = &$options["action"];
		$option->DropDownButtonPhrase = $Language->Phrase("ButtonActions");
		$option->UseImageAndText = TRUE;
		$option->UseDropDownButton = FALSE;
		$option->UseButtonGroup = TRUE;
		$item = &$option->Add($option->GroupOptionName);
		$item->Body = "";
		$item->Visible = FALSE;
	}

	// Set up starting record parameters
	function SetUpStartRec() {
		if ($this->DisplayRecs == 0)
			return;
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET[EW_TABLE_START_REC] <> "") { // Check for "start" parameter
				$this->StartRec = $_GET[EW_TABLE_START_REC];
				$this->setStartRecordNumber($this->StartRec);
			} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
				$PageNo = $_GET[EW_TABLE_PAGE_NO];
				if (is_numeric($PageNo)) {
					$this->StartRec = ($PageNo-1)*$this->DisplayRecs+1;
					if ($this->StartRec <= 0) {
						$this->StartRec = 1;
					} elseif ($this->StartRec >= intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1) {
						$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1;
					}
					$this->setStartRecordNumber($this->StartRec);
				}
			}
		}
		$this->StartRec = $this->getStartRecordNumber();

		// Check if correct start record counter
		if (!is_numeric($this->StartRec) || $this->StartRec == "") { // Avoid invalid start record counter
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} elseif (intval($this->StartRec) > intval($this->TotalRecs)) { // Avoid starting record > total records
			$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to last page first record
			$this->setStartRecordNumber($this->StartRec);
		} elseif (($this->StartRec-1) % $this->DisplayRecs <> 0) {
			$this->StartRec = intval(($this->StartRec-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to page boundary
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->OveCodi->setDbValue($rs->fields('OveCodi'));
		$this->ProCodi->setDbValue($rs->fields('ProCodi'));
		$this->OvePIni->setDbValue($rs->fields('OvePIni'));
		$this->OveCPro->setDbValue($rs->fields('OveCPro'));
		$this->OveUsua->setDbValue($rs->fields('OveUsua'));
		$this->OveFCre->setDbValue($rs->fields('OveFCre'));
		$this->OveCVen->setDbValue($rs->fields('OveCVen'));
		$this->CliCodi->setDbValue($rs->fields('CliCodi'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->OveCodi->DbValue = $row['OveCodi'];
		$this->ProCodi->DbValue = $row['ProCodi'];
		$this->OvePIni->DbValue = $row['OvePIni'];
		$this->OveCPro->DbValue = $row['OveCPro'];
		$this->OveUsua->DbValue = $row['OveUsua'];
		$this->OveFCre->DbValue = $row['OveFCre'];
		$this->OveCVen->DbValue = $row['OveCVen'];
		$this->CliCodi->DbValue = $row['CliCodi'];
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		$this->AddUrl = $this->GetAddUrl();
		$this->EditUrl = $this->GetEditUrl();
		$this->CopyUrl = $this->GetCopyUrl();
		$this->DeleteUrl = $this->GetDeleteUrl();
		$this->ListUrl = $this->GetListUrl();
		$this->SetupOtherOptions();

		// Convert decimal values if posted back
		if ($this->OvePIni->FormValue == $this->OvePIni->CurrentValue && is_numeric(ew_StrToFloat($this->OvePIni->CurrentValue)))
			$this->OvePIni->CurrentValue = ew_StrToFloat($this->OvePIni->CurrentValue);

		// Call Row_Rendering event
		$this->Row_Rendering();

		// Common render codes for all row types
		// OveCodi
		// ProCodi
		// OvePIni
		// OveCPro
		// OveUsua
		// OveFCre
		// OveCVen
		// CliCodi

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// OveCodi
		$this->OveCodi->ViewValue = $this->OveCodi->CurrentValue;
		$this->OveCodi->ViewCustomAttributes = "";

		// ProCodi
		if (strval($this->ProCodi->CurrentValue) <> "") {
			$sFilterWrk = "\"ProdCodi\"" . ew_SearchString("=", $this->ProCodi->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT \"ProdCodi\", \"CprCodi\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"Prod\"";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->ProCodi, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->ProCodi->ViewValue = $this->ProCodi->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->ProCodi->ViewValue = $this->ProCodi->CurrentValue;
			}
		} else {
			$this->ProCodi->ViewValue = NULL;
		}
		$this->ProCodi->ViewCustomAttributes = "";

		// OvePIni
		$this->OvePIni->ViewValue = $this->OvePIni->CurrentValue;
		$this->OvePIni->ViewCustomAttributes = "";

		// OveCPro
		$this->OveCPro->ViewValue = $this->OveCPro->CurrentValue;
		$this->OveCPro->ViewCustomAttributes = "";

		// OveUsua
		$this->OveUsua->ViewValue = $this->OveUsua->CurrentValue;
		$this->OveUsua->ViewCustomAttributes = "";

		// OveFCre
		$this->OveFCre->ViewValue = $this->OveFCre->CurrentValue;
		$this->OveFCre->ViewValue = ew_FormatDateTime($this->OveFCre->ViewValue, 7);
		$this->OveFCre->ViewCustomAttributes = "";

		// OveCVen
		$this->OveCVen->ViewValue = $this->OveCVen->CurrentValue;
		$this->OveCVen->ViewCustomAttributes = "";

		// CliCodi
		if (strval($this->CliCodi->CurrentValue) <> "") {
			$sFilterWrk = "\"CliCodi\"" . ew_SearchString("=", $this->CliCodi->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT \"CliCodi\", \"CliNomb\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"Clie\"";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->CliCodi, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->CliCodi->ViewValue = $this->CliCodi->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->CliCodi->ViewValue = $this->CliCodi->CurrentValue;
			}
		} else {
			$this->CliCodi->ViewValue = NULL;
		}
		$this->CliCodi->ViewCustomAttributes = "";

			// OveCodi
			$this->OveCodi->LinkCustomAttributes = "";
			$this->OveCodi->HrefValue = "";
			$this->OveCodi->TooltipValue = "";

			// ProCodi
			$this->ProCodi->LinkCustomAttributes = "";
			$this->ProCodi->HrefValue = "";
			$this->ProCodi->TooltipValue = "";

			// OvePIni
			$this->OvePIni->LinkCustomAttributes = "";
			$this->OvePIni->HrefValue = "";
			$this->OvePIni->TooltipValue = "";

			// OveCPro
			$this->OveCPro->LinkCustomAttributes = "";
			$this->OveCPro->HrefValue = "";
			$this->OveCPro->TooltipValue = "";

			// OveUsua
			$this->OveUsua->LinkCustomAttributes = "";
			$this->OveUsua->HrefValue = "";
			$this->OveUsua->TooltipValue = "";

			// OveFCre
			$this->OveFCre->LinkCustomAttributes = "";
			$this->OveFCre->HrefValue = "";
			$this->OveFCre->TooltipValue = "";

			// OveCVen
			$this->OveCVen->LinkCustomAttributes = "";
			$this->OveCVen->HrefValue = "";
			$this->OveCVen->TooltipValue = "";

			// CliCodi
			$this->CliCodi->LinkCustomAttributes = "";
			$this->CliCodi->HrefValue = "";
			$this->CliCodi->TooltipValue = "";
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, "OVenlist.php", "", $this->TableVar, TRUE);
		$PageId = "view";
		$Breadcrumb->Add("view", $PageId, $url);
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Page Exporting event
	// $this->ExportDoc = export document object
	function Page_Exporting() {

		//$this->ExportDoc->Text = "my header"; // Export header
		//return FALSE; // Return FALSE to skip default export and use Row_Export event

		return TRUE; // Return TRUE to use default export and skip Row_Export event
	}

	// Row Export event
	// $this->ExportDoc = export document object
	function Row_Export($rs) {

	    //$this->ExportDoc->Text .= "my content"; // Build HTML with field value: $rs["MyField"] or $this->MyField->ViewValue
	}

	// Page Exported event
	// $this->ExportDoc = export document object
	function Page_Exported() {

		//$this->ExportDoc->Text .= "my footer"; // Export footer
		//echo $this->ExportDoc->Text;

	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($OVen_view)) $OVen_view = new cOVen_view();

// Page init
$OVen_view->Page_Init();

// Page main
$OVen_view->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$OVen_view->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "view";
var CurrentForm = fOVenview = new ew_Form("fOVenview", "view");

// Form_CustomValidate event
fOVenview.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fOVenview.ValidateRequired = true;
<?php } else { ?>
fOVenview.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
fOVenview.Lists["x_ProCodi"] = {"LinkField":"x_ProdCodi","Ajax":true,"AutoFill":false,"DisplayFields":["x_CprCodi","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fOVenview.Lists["x_CliCodi"] = {"LinkField":"x_CliCodi","Ajax":true,"AutoFill":false,"DisplayFields":["x_CliNomb","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};

// Form object for search
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php $OVen_view->ExportOptions->Render("body") ?>
<?php
	foreach ($OVen_view->OtherOptions as &$option)
		$option->Render("body");
?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php $OVen_view->ShowPageHeader(); ?>
<?php
$OVen_view->ShowMessage();
?>
<form name="fOVenview" id="fOVenview" class="form-inline ewForm ewViewForm" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($OVen_view->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $OVen_view->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="OVen">
<table class="table table-bordered table-striped ewViewTable">
<?php if ($OVen->OveCodi->Visible) { // OveCodi ?>
	<tr id="r_OveCodi">
		<td><span id="elh_OVen_OveCodi"><?php echo $OVen->OveCodi->FldCaption() ?></span></td>
		<td data-name="OveCodi"<?php echo $OVen->OveCodi->CellAttributes() ?>>
<span id="el_OVen_OveCodi">
<span<?php echo $OVen->OveCodi->ViewAttributes() ?>>
<?php echo $OVen->OveCodi->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($OVen->ProCodi->Visible) { // ProCodi ?>
	<tr id="r_ProCodi">
		<td><span id="elh_OVen_ProCodi"><?php echo $OVen->ProCodi->FldCaption() ?></span></td>
		<td data-name="ProCodi"<?php echo $OVen->ProCodi->CellAttributes() ?>>
<span id="el_OVen_ProCodi">
<span<?php echo $OVen->ProCodi->ViewAttributes() ?>>
<?php echo $OVen->ProCodi->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($OVen->OvePIni->Visible) { // OvePIni ?>
	<tr id="r_OvePIni">
		<td><span id="elh_OVen_OvePIni"><?php echo $OVen->OvePIni->FldCaption() ?></span></td>
		<td data-name="OvePIni"<?php echo $OVen->OvePIni->CellAttributes() ?>>
<span id="el_OVen_OvePIni">
<span<?php echo $OVen->OvePIni->ViewAttributes() ?>>
<?php echo $OVen->OvePIni->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($OVen->OveCPro->Visible) { // OveCPro ?>
	<tr id="r_OveCPro">
		<td><span id="elh_OVen_OveCPro"><?php echo $OVen->OveCPro->FldCaption() ?></span></td>
		<td data-name="OveCPro"<?php echo $OVen->OveCPro->CellAttributes() ?>>
<span id="el_OVen_OveCPro">
<span<?php echo $OVen->OveCPro->ViewAttributes() ?>>
<?php echo $OVen->OveCPro->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($OVen->OveUsua->Visible) { // OveUsua ?>
	<tr id="r_OveUsua">
		<td><span id="elh_OVen_OveUsua"><?php echo $OVen->OveUsua->FldCaption() ?></span></td>
		<td data-name="OveUsua"<?php echo $OVen->OveUsua->CellAttributes() ?>>
<span id="el_OVen_OveUsua">
<span<?php echo $OVen->OveUsua->ViewAttributes() ?>>
<?php echo $OVen->OveUsua->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($OVen->OveFCre->Visible) { // OveFCre ?>
	<tr id="r_OveFCre">
		<td><span id="elh_OVen_OveFCre"><?php echo $OVen->OveFCre->FldCaption() ?></span></td>
		<td data-name="OveFCre"<?php echo $OVen->OveFCre->CellAttributes() ?>>
<span id="el_OVen_OveFCre">
<span<?php echo $OVen->OveFCre->ViewAttributes() ?>>
<?php echo $OVen->OveFCre->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($OVen->OveCVen->Visible) { // OveCVen ?>
	<tr id="r_OveCVen">
		<td><span id="elh_OVen_OveCVen"><?php echo $OVen->OveCVen->FldCaption() ?></span></td>
		<td data-name="OveCVen"<?php echo $OVen->OveCVen->CellAttributes() ?>>
<span id="el_OVen_OveCVen">
<span<?php echo $OVen->OveCVen->ViewAttributes() ?>>
<?php echo $OVen->OveCVen->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($OVen->CliCodi->Visible) { // CliCodi ?>
	<tr id="r_CliCodi">
		<td><span id="elh_OVen_CliCodi"><?php echo $OVen->CliCodi->FldCaption() ?></span></td>
		<td data-name="CliCodi"<?php echo $OVen->CliCodi->CellAttributes() ?>>
<span id="el_OVen_CliCodi">
<span<?php echo $OVen->CliCodi->ViewAttributes() ?>>
<?php echo $OVen->CliCodi->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
</table>
</form>
<script type="text/javascript">
fOVenview.Init();
</script>
<?php
$OVen_view->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$OVen_view->Page_Terminate();
?>
