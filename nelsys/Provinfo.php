<?php

// Global variable for table object
$Prov = NULL;

//
// Table class for Prov
//
class cProv extends cTable {
	var $PrvCodi;
	var $PrvFIni;
	var $PrvFFin;
	var $PrvPais;
	var $PrvTipo;
	var $PrvNomb;
	var $PrvApel;
	var $PrvCRuc;
	var $PrvDire;
	var $PrvMail;
	var $PrvTele;
	var $PrvRSoc;

	//
	// Table class constructor
	//
	function __construct() {
		global $Language;

		// Language object
		if (!isset($Language)) $Language = new cLanguage();
		$this->TableVar = 'Prov';
		$this->TableName = 'Prov';
		$this->TableType = 'TABLE';

		// Update Table
		$this->UpdateTable = "\"public\".\"Prov\"";
		$this->DBID = 'DB';
		$this->ExportAll = TRUE;
		$this->ExportPageBreakCount = 0; // Page break per every n record (PDF only)
		$this->ExportPageOrientation = "portrait"; // Page orientation (PDF only)
		$this->ExportPageSize = "a4"; // Page size (PDF only)
		$this->ExportExcelPageOrientation = ""; // Page orientation (PHPExcel only)
		$this->ExportExcelPageSize = ""; // Page size (PHPExcel only)
		$this->DetailAdd = TRUE; // Allow detail add
		$this->DetailEdit = TRUE; // Allow detail edit
		$this->DetailView = FALSE; // Allow detail view
		$this->ShowMultipleDetails = FALSE; // Show multiple details
		$this->GridAddRowCount = 5;
		$this->AllowAddDeleteRow = ew_AllowAddDeleteRow(); // Allow add/delete row
		$this->UserIDAllowSecurity = 0; // User ID Allow
		$this->BasicSearch = new cBasicSearch($this->TableVar);

		// PrvCodi
		$this->PrvCodi = new cField('Prov', 'Prov', 'x_PrvCodi', 'PrvCodi', '"PrvCodi"', 'CAST("PrvCodi" AS varchar(255))', 3, -1, FALSE, '"PrvCodi"', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'NO');
		$this->PrvCodi->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['PrvCodi'] = &$this->PrvCodi;

		// PrvFIni
		$this->PrvFIni = new cField('Prov', 'Prov', 'x_PrvFIni', 'PrvFIni', '"PrvFIni"', 'CAST("PrvFIni" AS varchar(255))', 133, 7, FALSE, '"PrvFIni"', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->PrvFIni->FldDefaultErrMsg = str_replace("%s", "/", $Language->Phrase("IncorrectDateDMY"));
		$this->fields['PrvFIni'] = &$this->PrvFIni;

		// PrvFFin
		$this->PrvFFin = new cField('Prov', 'Prov', 'x_PrvFFin', 'PrvFFin', '"PrvFFin"', 'CAST("PrvFFin" AS varchar(255))', 133, 7, FALSE, '"PrvFFin"', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->PrvFFin->FldDefaultErrMsg = str_replace("%s", "/", $Language->Phrase("IncorrectDateDMY"));
		$this->fields['PrvFFin'] = &$this->PrvFFin;

		// PrvPais
		$this->PrvPais = new cField('Prov', 'Prov', 'x_PrvPais', 'PrvPais', '"PrvPais"', '"PrvPais"', 200, -1, FALSE, '"PrvPais"', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->fields['PrvPais'] = &$this->PrvPais;

		// PrvTipo
		$this->PrvTipo = new cField('Prov', 'Prov', 'x_PrvTipo', 'PrvTipo', '"PrvTipo"', '"PrvTipo"', 200, -1, FALSE, '"PrvTipo"', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->fields['PrvTipo'] = &$this->PrvTipo;

		// PrvNomb
		$this->PrvNomb = new cField('Prov', 'Prov', 'x_PrvNomb', 'PrvNomb', '"PrvNomb"', '"PrvNomb"', 200, -1, FALSE, '"PrvNomb"', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->fields['PrvNomb'] = &$this->PrvNomb;

		// PrvApel
		$this->PrvApel = new cField('Prov', 'Prov', 'x_PrvApel', 'PrvApel', '"PrvApel"', '"PrvApel"', 200, -1, FALSE, '"PrvApel"', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->fields['PrvApel'] = &$this->PrvApel;

		// PrvCRuc
		$this->PrvCRuc = new cField('Prov', 'Prov', 'x_PrvCRuc', 'PrvCRuc', '"PrvCRuc"', '"PrvCRuc"', 200, -1, FALSE, '"PrvCRuc"', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->fields['PrvCRuc'] = &$this->PrvCRuc;

		// PrvDire
		$this->PrvDire = new cField('Prov', 'Prov', 'x_PrvDire', 'PrvDire', '"PrvDire"', '"PrvDire"', 200, -1, FALSE, '"PrvDire"', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->fields['PrvDire'] = &$this->PrvDire;

		// PrvMail
		$this->PrvMail = new cField('Prov', 'Prov', 'x_PrvMail', 'PrvMail', '"PrvMail"', '"PrvMail"', 200, -1, FALSE, '"PrvMail"', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->PrvMail->FldDefaultErrMsg = $Language->Phrase("IncorrectEmail");
		$this->fields['PrvMail'] = &$this->PrvMail;

		// PrvTele
		$this->PrvTele = new cField('Prov', 'Prov', 'x_PrvTele', 'PrvTele', '"PrvTele"', 'CAST("PrvTele" AS varchar(255))', 3, -1, FALSE, '"PrvTele"', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->PrvTele->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['PrvTele'] = &$this->PrvTele;

		// PrvRSoc
		$this->PrvRSoc = new cField('Prov', 'Prov', 'x_PrvRSoc', 'PrvRSoc', '"PrvRSoc"', '"PrvRSoc"', 200, -1, FALSE, '"PrvRSoc"', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->fields['PrvRSoc'] = &$this->PrvRSoc;
	}

	// Multiple column sort
	function UpdateSort(&$ofld, $ctrl) {
		if ($this->CurrentOrder == $ofld->FldName) {
			$sSortField = $ofld->FldExpression;
			$sLastSort = $ofld->getSort();
			if ($this->CurrentOrderType == "ASC" || $this->CurrentOrderType == "DESC") {
				$sThisSort = $this->CurrentOrderType;
			} else {
				$sThisSort = ($sLastSort == "ASC") ? "DESC" : "ASC";
			}
			$ofld->setSort($sThisSort);
			if ($ctrl) {
				$sOrderBy = $this->getSessionOrderBy();
				if (strpos($sOrderBy, $sSortField . " " . $sLastSort) !== FALSE) {
					$sOrderBy = str_replace($sSortField . " " . $sLastSort, $sSortField . " " . $sThisSort, $sOrderBy);
				} else {
					if ($sOrderBy <> "") $sOrderBy .= ", ";
					$sOrderBy .= $sSortField . " " . $sThisSort;
				}
				$this->setSessionOrderBy($sOrderBy); // Save to Session
			} else {
				$this->setSessionOrderBy($sSortField . " " . $sThisSort); // Save to Session
			}
		} else {
			if (!$ctrl) $ofld->setSort("");
		}
	}

	// Table level SQL
	var $_SqlFrom = "";

	function getSqlFrom() { // From
		return ($this->_SqlFrom <> "") ? $this->_SqlFrom : "\"public\".\"Prov\"";
	}

	function SqlFrom() { // For backward compatibility
    	return $this->getSqlFrom();
	}

	function setSqlFrom($v) {
    	$this->_SqlFrom = $v;
	}
	var $_SqlSelect = "";

	function getSqlSelect() { // Select
		return ($this->_SqlSelect <> "") ? $this->_SqlSelect : "SELECT * FROM " . $this->getSqlFrom();
	}

	function SqlSelect() { // For backward compatibility
    	return $this->getSqlSelect();
	}

	function setSqlSelect($v) {
    	$this->_SqlSelect = $v;
	}
	var $_SqlWhere = "";

	function getSqlWhere() { // Where
		$sWhere = ($this->_SqlWhere <> "") ? $this->_SqlWhere : "";
		$this->TableFilter = "";
		ew_AddFilter($sWhere, $this->TableFilter);
		return $sWhere;
	}

	function SqlWhere() { // For backward compatibility
    	return $this->getSqlWhere();
	}

	function setSqlWhere($v) {
    	$this->_SqlWhere = $v;
	}
	var $_SqlGroupBy = "";

	function getSqlGroupBy() { // Group By
		return ($this->_SqlGroupBy <> "") ? $this->_SqlGroupBy : "";
	}

	function SqlGroupBy() { // For backward compatibility
    	return $this->getSqlGroupBy();
	}

	function setSqlGroupBy($v) {
    	$this->_SqlGroupBy = $v;
	}
	var $_SqlHaving = "";

	function getSqlHaving() { // Having
		return ($this->_SqlHaving <> "") ? $this->_SqlHaving : "";
	}

	function SqlHaving() { // For backward compatibility
    	return $this->getSqlHaving();
	}

	function setSqlHaving($v) {
    	$this->_SqlHaving = $v;
	}
	var $_SqlOrderBy = "";

	function getSqlOrderBy() { // Order By
		return ($this->_SqlOrderBy <> "") ? $this->_SqlOrderBy : "";
	}

	function SqlOrderBy() { // For backward compatibility
    	return $this->getSqlOrderBy();
	}

	function setSqlOrderBy($v) {
    	$this->_SqlOrderBy = $v;
	}

	// Apply User ID filters
	function ApplyUserIDFilters($sFilter) {
		return $sFilter;
	}

	// Check if User ID security allows view all
	function UserIDAllow($id = "") {
		$allow = EW_USER_ID_ALLOW;
		switch ($id) {
			case "add":
			case "copy":
			case "gridadd":
			case "register":
			case "addopt":
				return (($allow & 1) == 1);
			case "edit":
			case "gridedit":
			case "update":
			case "changepwd":
			case "forgotpwd":
				return (($allow & 4) == 4);
			case "delete":
				return (($allow & 2) == 2);
			case "view":
				return (($allow & 32) == 32);
			case "search":
				return (($allow & 64) == 64);
			default:
				return (($allow & 8) == 8);
		}
	}

	// Get SQL
	function GetSQL($where, $orderby) {
		return ew_BuildSelectSql($this->getSqlSelect(), $this->getSqlWhere(),
			$this->getSqlGroupBy(), $this->getSqlHaving(), $this->getSqlOrderBy(),
			$where, $orderby);
	}

	// Table SQL
	function SQL() {
		$sFilter = $this->CurrentFilter;
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$sSort = $this->getSessionOrderBy();
		return ew_BuildSelectSql($this->getSqlSelect(), $this->getSqlWhere(),
			$this->getSqlGroupBy(), $this->getSqlHaving(), $this->getSqlOrderBy(),
			$sFilter, $sSort);
	}

	// Table SQL with List page filter
	function SelectSQL() {
		$sFilter = $this->getSessionWhere();
		ew_AddFilter($sFilter, $this->CurrentFilter);
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$this->Recordset_Selecting($sFilter);
		$sSort = $this->getSessionOrderBy();
		return ew_BuildSelectSql($this->getSqlSelect(), $this->getSqlWhere(), $this->getSqlGroupBy(),
			$this->getSqlHaving(), $this->getSqlOrderBy(), $sFilter, $sSort);
	}

	// Get ORDER BY clause
	function GetOrderBy() {
		$sSort = $this->getSessionOrderBy();
		return ew_BuildSelectSql("", "", "", "", $this->getSqlOrderBy(), "", $sSort);
	}

	// Try to get record count
	function TryGetRecordCount($sSql) {
		$cnt = -1;
		if (($this->TableType == 'TABLE' || $this->TableType == 'VIEW' || $this->TableType == 'LINKTABLE') && preg_match("/^SELECT \* FROM/i", $sSql)) {
			$sSql = "SELECT COUNT(*) FROM" . preg_replace('/^SELECT\s([\s\S]+)?\*\sFROM/i', "", $sSql);
			$sOrderBy = $this->GetOrderBy();
			if (substr($sSql, strlen($sOrderBy) * -1) == $sOrderBy)
				$sSql = substr($sSql, 0, strlen($sSql) - strlen($sOrderBy)); // Remove ORDER BY clause
		} else {
			$sSql = "SELECT COUNT(*) FROM (" . $sSql . ") EW_COUNT_TABLE";
		}
		$conn = &$this->Connection();
		if ($rs = $conn->Execute($sSql)) {
			if (!$rs->EOF && $rs->FieldCount() > 0) {
				$cnt = $rs->fields[0];
				$rs->Close();
			}
		}
		return intval($cnt);
	}

	// Get record count based on filter (for detail record count in master table pages)
	function LoadRecordCount($sFilter) {
		$origFilter = $this->CurrentFilter;
		$this->CurrentFilter = $sFilter;
		$this->Recordset_Selecting($this->CurrentFilter);

		//$sSql = $this->SQL();
		$sSql = $this->GetSQL($this->CurrentFilter, "");
		$cnt = $this->TryGetRecordCount($sSql);
		if ($cnt == -1) {
			if ($rs = $this->LoadRs($this->CurrentFilter)) {
				$cnt = $rs->RecordCount();
				$rs->Close();
			}
		}
		$this->CurrentFilter = $origFilter;
		return intval($cnt);
	}

	// Get record count (for current List page)
	function SelectRecordCount() {
		$sSql = $this->SelectSQL();
		$cnt = $this->TryGetRecordCount($sSql);
		if ($cnt == -1) {
			$conn = &$this->Connection();
			if ($rs = $conn->Execute($sSql)) {
				$cnt = $rs->RecordCount();
				$rs->Close();
			}
		}
		return intval($cnt);
	}

	// INSERT statement
	function InsertSQL(&$rs) {
		$names = "";
		$values = "";
		foreach ($rs as $name => $value) {
			if (!isset($this->fields[$name]) || $this->fields[$name]->FldIsCustom)
				continue;
			$names .= $this->fields[$name]->FldExpression . ",";
			$values .= ew_QuotedValue($value, $this->fields[$name]->FldDataType, $this->DBID) . ",";
		}
		while (substr($names, -1) == ",")
			$names = substr($names, 0, -1);
		while (substr($values, -1) == ",")
			$values = substr($values, 0, -1);
		return "INSERT INTO " . $this->UpdateTable . " ($names) VALUES ($values)";
	}

	// Insert
	function Insert(&$rs) {
		$conn = &$this->Connection();
		return $conn->Execute($this->InsertSQL($rs));
	}

	// UPDATE statement
	function UpdateSQL(&$rs, $where = "", $curfilter = TRUE) {
		$sql = "UPDATE " . $this->UpdateTable . " SET ";
		foreach ($rs as $name => $value) {
			if (!isset($this->fields[$name]) || $this->fields[$name]->FldIsCustom)
				continue;
			$sql .= $this->fields[$name]->FldExpression . "=";
			$sql .= ew_QuotedValue($value, $this->fields[$name]->FldDataType, $this->DBID) . ",";
		}
		while (substr($sql, -1) == ",")
			$sql = substr($sql, 0, -1);
		$filter = ($curfilter) ? $this->CurrentFilter : "";
		if (is_array($where))
			$where = $this->ArrayToFilter($where);
		ew_AddFilter($filter, $where);
		if ($filter <> "")	$sql .= " WHERE " . $filter;
		return $sql;
	}

	// Update
	function Update(&$rs, $where = "", $rsold = NULL, $curfilter = TRUE) {
		$conn = &$this->Connection();
		return $conn->Execute($this->UpdateSQL($rs, $where, $curfilter));
	}

	// DELETE statement
	function DeleteSQL(&$rs, $where = "", $curfilter = TRUE) {
		$sql = "DELETE FROM " . $this->UpdateTable . " WHERE ";
		if (is_array($where))
			$where = $this->ArrayToFilter($where);
		if ($rs) {
			if (array_key_exists('PrvCodi', $rs))
				ew_AddFilter($where, ew_QuotedName('PrvCodi', $this->DBID) . '=' . ew_QuotedValue($rs['PrvCodi'], $this->PrvCodi->FldDataType, $this->DBID));
		}
		$filter = ($curfilter) ? $this->CurrentFilter : "";
		ew_AddFilter($filter, $where);
		if ($filter <> "")
			$sql .= $filter;
		else
			$sql .= "0=1"; // Avoid delete
		return $sql;
	}

	// Delete
	function Delete(&$rs, $where = "", $curfilter = TRUE) {
		$conn = &$this->Connection();
		return $conn->Execute($this->DeleteSQL($rs, $where, $curfilter));
	}

	// Key filter WHERE clause
	function SqlKeyFilter() {
		return "\"PrvCodi\" = @PrvCodi@";
	}

	// Key filter
	function KeyFilter() {
		$sKeyFilter = $this->SqlKeyFilter();
		if (!is_numeric($this->PrvCodi->CurrentValue))
			$sKeyFilter = "0=1"; // Invalid key
		$sKeyFilter = str_replace("@PrvCodi@", ew_AdjustSql($this->PrvCodi->CurrentValue, $this->DBID), $sKeyFilter); // Replace key value
		return $sKeyFilter;
	}

	// Return page URL
	function getReturnUrl() {
		$name = EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_RETURN_URL;

		// Get referer URL automatically
		if (ew_ServerVar("HTTP_REFERER") <> "" && ew_ReferPage() <> ew_CurrentPage() && ew_ReferPage() <> "login.php") // Referer not same page or login page
			$_SESSION[$name] = ew_ServerVar("HTTP_REFERER"); // Save to Session
		if (@$_SESSION[$name] <> "") {
			return $_SESSION[$name];
		} else {
			return "Provlist.php";
		}
	}

	function setReturnUrl($v) {
		$_SESSION[EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_RETURN_URL] = $v;
	}

	// List URL
	function GetListUrl() {
		return "Provlist.php";
	}

	// View URL
	function GetViewUrl($parm = "") {
		if ($parm <> "")
			$url = $this->KeyUrl("Provview.php", $this->UrlParm($parm));
		else
			$url = $this->KeyUrl("Provview.php", $this->UrlParm(EW_TABLE_SHOW_DETAIL . "="));
		return $this->AddMasterUrl($url);
	}

	// Add URL
	function GetAddUrl($parm = "") {
		if ($parm <> "")
			$url = "Provadd.php?" . $this->UrlParm($parm);
		else
			$url = "Provadd.php";
		return $this->AddMasterUrl($url);
	}

	// Edit URL
	function GetEditUrl($parm = "") {
		$url = $this->KeyUrl("Provedit.php", $this->UrlParm($parm));
		return $this->AddMasterUrl($url);
	}

	// Inline edit URL
	function GetInlineEditUrl() {
		$url = $this->KeyUrl(ew_CurrentPage(), $this->UrlParm("a=edit"));
		return $this->AddMasterUrl($url);
	}

	// Copy URL
	function GetCopyUrl($parm = "") {
		$url = $this->KeyUrl("Provadd.php", $this->UrlParm($parm));
		return $this->AddMasterUrl($url);
	}

	// Inline copy URL
	function GetInlineCopyUrl() {
		$url = $this->KeyUrl(ew_CurrentPage(), $this->UrlParm("a=copy"));
		return $this->AddMasterUrl($url);
	}

	// Delete URL
	function GetDeleteUrl() {
		return $this->KeyUrl("Provdelete.php", $this->UrlParm());
	}

	// Add master url
	function AddMasterUrl($url) {
		return $url;
	}

	function KeyToJson() {
		$json = "";
		$json .= "PrvCodi:" . ew_VarToJson($this->PrvCodi->CurrentValue, "number", "'");
		return "{" . $json . "}";
	}

	// Add key value to URL
	function KeyUrl($url, $parm = "") {
		$sUrl = $url . "?";
		if ($parm <> "") $sUrl .= $parm . "&";
		if (!is_null($this->PrvCodi->CurrentValue)) {
			$sUrl .= "PrvCodi=" . urlencode($this->PrvCodi->CurrentValue);
		} else {
			return "javascript:ew_Alert(ewLanguage.Phrase('InvalidRecord'));";
		}
		return $sUrl;
	}

	// Sort URL
	function SortUrl(&$fld) {
		if ($this->CurrentAction <> "" || $this->Export <> "" ||
			in_array($fld->FldType, array(128, 204, 205))) { // Unsortable data type
				return "";
		} elseif ($fld->Sortable) {
			$sUrlParm = $this->UrlParm("order=" . urlencode($fld->FldName) . "&amp;ordertype=" . $fld->ReverseSort());
			return ew_CurrentPage() . "?" . $sUrlParm;
		} else {
			return "";
		}
	}

	// Get record keys from $_POST/$_GET/$_SESSION
	function GetRecordKeys() {
		global $EW_COMPOSITE_KEY_SEPARATOR;
		$arKeys = array();
		$arKey = array();
		if (isset($_POST["key_m"])) {
			$arKeys = ew_StripSlashes($_POST["key_m"]);
			$cnt = count($arKeys);
		} elseif (isset($_GET["key_m"])) {
			$arKeys = ew_StripSlashes($_GET["key_m"]);
			$cnt = count($arKeys);
		} elseif (!empty($_GET) || !empty($_POST)) {
			$isPost = ew_IsHttpPost();
			$arKeys[] = $isPost ? ew_StripSlashes(@$_POST["PrvCodi"]) : ew_StripSlashes(@$_GET["PrvCodi"]); // PrvCodi

			//return $arKeys; // Do not return yet, so the values will also be checked by the following code
		}

		// Check keys
		$ar = array();
		foreach ($arKeys as $key) {
			if (!is_numeric($key))
				continue;
			$ar[] = $key;
		}
		return $ar;
	}

	// Get key filter
	function GetKeyFilter() {
		$arKeys = $this->GetRecordKeys();
		$sKeyFilter = "";
		foreach ($arKeys as $key) {
			if ($sKeyFilter <> "") $sKeyFilter .= " OR ";
			$this->PrvCodi->CurrentValue = $key;
			$sKeyFilter .= "(" . $this->KeyFilter() . ")";
		}
		return $sKeyFilter;
	}

	// Load rows based on filter
	function &LoadRs($sFilter) {

		// Set up filter (SQL WHERE clause) and get return SQL
		//$this->CurrentFilter = $sFilter;
		//$sSql = $this->SQL();

		$sSql = $this->GetSQL($sFilter, "");
		$conn = &$this->Connection();
		$rs = $conn->Execute($sSql);
		return $rs;
	}

	// Load row values from recordset
	function LoadListRowValues(&$rs) {
		$this->PrvCodi->setDbValue($rs->fields('PrvCodi'));
		$this->PrvFIni->setDbValue($rs->fields('PrvFIni'));
		$this->PrvFFin->setDbValue($rs->fields('PrvFFin'));
		$this->PrvPais->setDbValue($rs->fields('PrvPais'));
		$this->PrvTipo->setDbValue($rs->fields('PrvTipo'));
		$this->PrvNomb->setDbValue($rs->fields('PrvNomb'));
		$this->PrvApel->setDbValue($rs->fields('PrvApel'));
		$this->PrvCRuc->setDbValue($rs->fields('PrvCRuc'));
		$this->PrvDire->setDbValue($rs->fields('PrvDire'));
		$this->PrvMail->setDbValue($rs->fields('PrvMail'));
		$this->PrvTele->setDbValue($rs->fields('PrvTele'));
		$this->PrvRSoc->setDbValue($rs->fields('PrvRSoc'));
	}

	// Render list row values
	function RenderListRow() {
		global $Security, $gsLanguage, $Language;

		// Call Row Rendering event
		$this->Row_Rendering();

   // Common render codes
		// PrvCodi
		// PrvFIni
		// PrvFFin
		// PrvPais
		// PrvTipo
		// PrvNomb
		// PrvApel
		// PrvCRuc
		// PrvDire
		// PrvMail
		// PrvTele
		// PrvRSoc
		// PrvCodi

		$this->PrvCodi->ViewValue = $this->PrvCodi->CurrentValue;
		$this->PrvCodi->ViewCustomAttributes = "";

		// PrvFIni
		$this->PrvFIni->ViewValue = $this->PrvFIni->CurrentValue;
		$this->PrvFIni->ViewValue = ew_FormatDateTime($this->PrvFIni->ViewValue, 7);
		$this->PrvFIni->ViewCustomAttributes = "";

		// PrvFFin
		$this->PrvFFin->ViewValue = $this->PrvFFin->CurrentValue;
		$this->PrvFFin->ViewValue = ew_FormatDateTime($this->PrvFFin->ViewValue, 7);
		$this->PrvFFin->ViewCustomAttributes = "";

		// PrvPais
		$this->PrvPais->ViewValue = $this->PrvPais->CurrentValue;
		$this->PrvPais->ViewCustomAttributes = "";

		// PrvTipo
		$this->PrvTipo->ViewValue = $this->PrvTipo->CurrentValue;
		$this->PrvTipo->ViewCustomAttributes = "";

		// PrvNomb
		$this->PrvNomb->ViewValue = $this->PrvNomb->CurrentValue;
		$this->PrvNomb->ViewCustomAttributes = "";

		// PrvApel
		$this->PrvApel->ViewValue = $this->PrvApel->CurrentValue;
		$this->PrvApel->ViewCustomAttributes = "";

		// PrvCRuc
		$this->PrvCRuc->ViewValue = $this->PrvCRuc->CurrentValue;
		$this->PrvCRuc->ViewCustomAttributes = "";

		// PrvDire
		$this->PrvDire->ViewValue = $this->PrvDire->CurrentValue;
		$this->PrvDire->ViewCustomAttributes = "";

		// PrvMail
		$this->PrvMail->ViewValue = $this->PrvMail->CurrentValue;
		$this->PrvMail->ViewCustomAttributes = "";

		// PrvTele
		$this->PrvTele->ViewValue = $this->PrvTele->CurrentValue;
		$this->PrvTele->ViewCustomAttributes = "";

		// PrvRSoc
		$this->PrvRSoc->ViewValue = $this->PrvRSoc->CurrentValue;
		$this->PrvRSoc->ViewCustomAttributes = "";

		// PrvCodi
		$this->PrvCodi->LinkCustomAttributes = "";
		$this->PrvCodi->HrefValue = "";
		$this->PrvCodi->TooltipValue = "";

		// PrvFIni
		$this->PrvFIni->LinkCustomAttributes = "";
		$this->PrvFIni->HrefValue = "";
		$this->PrvFIni->TooltipValue = "";

		// PrvFFin
		$this->PrvFFin->LinkCustomAttributes = "";
		$this->PrvFFin->HrefValue = "";
		$this->PrvFFin->TooltipValue = "";

		// PrvPais
		$this->PrvPais->LinkCustomAttributes = "";
		$this->PrvPais->HrefValue = "";
		$this->PrvPais->TooltipValue = "";

		// PrvTipo
		$this->PrvTipo->LinkCustomAttributes = "";
		$this->PrvTipo->HrefValue = "";
		$this->PrvTipo->TooltipValue = "";

		// PrvNomb
		$this->PrvNomb->LinkCustomAttributes = "";
		$this->PrvNomb->HrefValue = "";
		$this->PrvNomb->TooltipValue = "";

		// PrvApel
		$this->PrvApel->LinkCustomAttributes = "";
		$this->PrvApel->HrefValue = "";
		$this->PrvApel->TooltipValue = "";

		// PrvCRuc
		$this->PrvCRuc->LinkCustomAttributes = "";
		$this->PrvCRuc->HrefValue = "";
		$this->PrvCRuc->TooltipValue = "";

		// PrvDire
		$this->PrvDire->LinkCustomAttributes = "";
		$this->PrvDire->HrefValue = "";
		$this->PrvDire->TooltipValue = "";

		// PrvMail
		$this->PrvMail->LinkCustomAttributes = "";
		$this->PrvMail->HrefValue = "";
		$this->PrvMail->TooltipValue = "";

		// PrvTele
		$this->PrvTele->LinkCustomAttributes = "";
		$this->PrvTele->HrefValue = "";
		$this->PrvTele->TooltipValue = "";

		// PrvRSoc
		$this->PrvRSoc->LinkCustomAttributes = "";
		$this->PrvRSoc->HrefValue = "";
		$this->PrvRSoc->TooltipValue = "";

		// Call Row Rendered event
		$this->Row_Rendered();
	}

	// Render edit row values
	function RenderEditRow() {
		global $Security, $gsLanguage, $Language;

		// Call Row Rendering event
		$this->Row_Rendering();

		// PrvCodi
		$this->PrvCodi->EditAttrs["class"] = "form-control";
		$this->PrvCodi->EditCustomAttributes = "";
		$this->PrvCodi->EditValue = $this->PrvCodi->CurrentValue;
		$this->PrvCodi->ViewCustomAttributes = "";

		// PrvFIni
		$this->PrvFIni->EditAttrs["class"] = "form-control";
		$this->PrvFIni->EditCustomAttributes = "";
		$this->PrvFIni->EditValue = ew_FormatDateTime($this->PrvFIni->CurrentValue, 7);
		$this->PrvFIni->PlaceHolder = ew_RemoveHtml($this->PrvFIni->FldCaption());

		// PrvFFin
		$this->PrvFFin->EditAttrs["class"] = "form-control";
		$this->PrvFFin->EditCustomAttributes = "";
		$this->PrvFFin->EditValue = ew_FormatDateTime($this->PrvFFin->CurrentValue, 7);
		$this->PrvFFin->PlaceHolder = ew_RemoveHtml($this->PrvFFin->FldCaption());

		// PrvPais
		$this->PrvPais->EditAttrs["class"] = "form-control";
		$this->PrvPais->EditCustomAttributes = "";
		$this->PrvPais->EditValue = $this->PrvPais->CurrentValue;
		$this->PrvPais->PlaceHolder = ew_RemoveHtml($this->PrvPais->FldCaption());

		// PrvTipo
		$this->PrvTipo->EditAttrs["class"] = "form-control";
		$this->PrvTipo->EditCustomAttributes = "";
		$this->PrvTipo->EditValue = $this->PrvTipo->CurrentValue;
		$this->PrvTipo->PlaceHolder = ew_RemoveHtml($this->PrvTipo->FldCaption());

		// PrvNomb
		$this->PrvNomb->EditAttrs["class"] = "form-control";
		$this->PrvNomb->EditCustomAttributes = "";
		$this->PrvNomb->EditValue = $this->PrvNomb->CurrentValue;
		$this->PrvNomb->PlaceHolder = ew_RemoveHtml($this->PrvNomb->FldCaption());

		// PrvApel
		$this->PrvApel->EditAttrs["class"] = "form-control";
		$this->PrvApel->EditCustomAttributes = "";
		$this->PrvApel->EditValue = $this->PrvApel->CurrentValue;
		$this->PrvApel->PlaceHolder = ew_RemoveHtml($this->PrvApel->FldCaption());

		// PrvCRuc
		$this->PrvCRuc->EditAttrs["class"] = "form-control";
		$this->PrvCRuc->EditCustomAttributes = "";
		$this->PrvCRuc->EditValue = $this->PrvCRuc->CurrentValue;
		$this->PrvCRuc->PlaceHolder = ew_RemoveHtml($this->PrvCRuc->FldCaption());

		// PrvDire
		$this->PrvDire->EditAttrs["class"] = "form-control";
		$this->PrvDire->EditCustomAttributes = "";
		$this->PrvDire->EditValue = $this->PrvDire->CurrentValue;
		$this->PrvDire->PlaceHolder = ew_RemoveHtml($this->PrvDire->FldCaption());

		// PrvMail
		$this->PrvMail->EditAttrs["class"] = "form-control";
		$this->PrvMail->EditCustomAttributes = "";
		$this->PrvMail->EditValue = $this->PrvMail->CurrentValue;
		$this->PrvMail->PlaceHolder = ew_RemoveHtml($this->PrvMail->FldCaption());

		// PrvTele
		$this->PrvTele->EditAttrs["class"] = "form-control";
		$this->PrvTele->EditCustomAttributes = "";
		$this->PrvTele->EditValue = $this->PrvTele->CurrentValue;
		$this->PrvTele->PlaceHolder = ew_RemoveHtml($this->PrvTele->FldCaption());

		// PrvRSoc
		$this->PrvRSoc->EditAttrs["class"] = "form-control";
		$this->PrvRSoc->EditCustomAttributes = "";
		$this->PrvRSoc->EditValue = $this->PrvRSoc->CurrentValue;
		$this->PrvRSoc->PlaceHolder = ew_RemoveHtml($this->PrvRSoc->FldCaption());

		// Call Row Rendered event
		$this->Row_Rendered();
	}

	// Aggregate list row values
	function AggregateListRowValues() {
	}

	// Aggregate list row (for rendering)
	function AggregateListRow() {

		// Call Row Rendered event
		$this->Row_Rendered();
	}
	var $ExportDoc;

	// Export data in HTML/CSV/Word/Excel/Email/PDF format
	function ExportDocument(&$Doc, &$Recordset, $StartRec, $StopRec, $ExportPageType = "") {
		if (!$Recordset || !$Doc)
			return;
		if (!$Doc->ExportCustom) {

			// Write header
			$Doc->ExportTableHeader();
			if ($Doc->Horizontal) { // Horizontal format, write header
				$Doc->BeginExportRow();
				if ($ExportPageType == "view") {
					if ($this->PrvFIni->Exportable) $Doc->ExportCaption($this->PrvFIni);
					if ($this->PrvFFin->Exportable) $Doc->ExportCaption($this->PrvFFin);
					if ($this->PrvPais->Exportable) $Doc->ExportCaption($this->PrvPais);
					if ($this->PrvTipo->Exportable) $Doc->ExportCaption($this->PrvTipo);
					if ($this->PrvNomb->Exportable) $Doc->ExportCaption($this->PrvNomb);
					if ($this->PrvApel->Exportable) $Doc->ExportCaption($this->PrvApel);
					if ($this->PrvCRuc->Exportable) $Doc->ExportCaption($this->PrvCRuc);
					if ($this->PrvDire->Exportable) $Doc->ExportCaption($this->PrvDire);
					if ($this->PrvMail->Exportable) $Doc->ExportCaption($this->PrvMail);
					if ($this->PrvTele->Exportable) $Doc->ExportCaption($this->PrvTele);
					if ($this->PrvRSoc->Exportable) $Doc->ExportCaption($this->PrvRSoc);
				} else {
					if ($this->PrvCodi->Exportable) $Doc->ExportCaption($this->PrvCodi);
					if ($this->PrvFIni->Exportable) $Doc->ExportCaption($this->PrvFIni);
					if ($this->PrvFFin->Exportable) $Doc->ExportCaption($this->PrvFFin);
					if ($this->PrvPais->Exportable) $Doc->ExportCaption($this->PrvPais);
					if ($this->PrvTipo->Exportable) $Doc->ExportCaption($this->PrvTipo);
					if ($this->PrvNomb->Exportable) $Doc->ExportCaption($this->PrvNomb);
					if ($this->PrvApel->Exportable) $Doc->ExportCaption($this->PrvApel);
					if ($this->PrvCRuc->Exportable) $Doc->ExportCaption($this->PrvCRuc);
					if ($this->PrvDire->Exportable) $Doc->ExportCaption($this->PrvDire);
					if ($this->PrvMail->Exportable) $Doc->ExportCaption($this->PrvMail);
					if ($this->PrvTele->Exportable) $Doc->ExportCaption($this->PrvTele);
					if ($this->PrvRSoc->Exportable) $Doc->ExportCaption($this->PrvRSoc);
				}
				$Doc->EndExportRow();
			}
		}

		// Move to first record
		$RecCnt = $StartRec - 1;
		if (!$Recordset->EOF) {
			$Recordset->MoveFirst();
			if ($StartRec > 1)
				$Recordset->Move($StartRec - 1);
		}
		while (!$Recordset->EOF && $RecCnt < $StopRec) {
			$RecCnt++;
			if (intval($RecCnt) >= intval($StartRec)) {
				$RowCnt = intval($RecCnt) - intval($StartRec) + 1;

				// Page break
				if ($this->ExportPageBreakCount > 0) {
					if ($RowCnt > 1 && ($RowCnt - 1) % $this->ExportPageBreakCount == 0)
						$Doc->ExportPageBreak();
				}
				$this->LoadListRowValues($Recordset);

				// Render row
				$this->RowType = EW_ROWTYPE_VIEW; // Render view
				$this->ResetAttrs();
				$this->RenderListRow();
				if (!$Doc->ExportCustom) {
					$Doc->BeginExportRow($RowCnt); // Allow CSS styles if enabled
					if ($ExportPageType == "view") {
						if ($this->PrvFIni->Exportable) $Doc->ExportField($this->PrvFIni);
						if ($this->PrvFFin->Exportable) $Doc->ExportField($this->PrvFFin);
						if ($this->PrvPais->Exportable) $Doc->ExportField($this->PrvPais);
						if ($this->PrvTipo->Exportable) $Doc->ExportField($this->PrvTipo);
						if ($this->PrvNomb->Exportable) $Doc->ExportField($this->PrvNomb);
						if ($this->PrvApel->Exportable) $Doc->ExportField($this->PrvApel);
						if ($this->PrvCRuc->Exportable) $Doc->ExportField($this->PrvCRuc);
						if ($this->PrvDire->Exportable) $Doc->ExportField($this->PrvDire);
						if ($this->PrvMail->Exportable) $Doc->ExportField($this->PrvMail);
						if ($this->PrvTele->Exportable) $Doc->ExportField($this->PrvTele);
						if ($this->PrvRSoc->Exportable) $Doc->ExportField($this->PrvRSoc);
					} else {
						if ($this->PrvCodi->Exportable) $Doc->ExportField($this->PrvCodi);
						if ($this->PrvFIni->Exportable) $Doc->ExportField($this->PrvFIni);
						if ($this->PrvFFin->Exportable) $Doc->ExportField($this->PrvFFin);
						if ($this->PrvPais->Exportable) $Doc->ExportField($this->PrvPais);
						if ($this->PrvTipo->Exportable) $Doc->ExportField($this->PrvTipo);
						if ($this->PrvNomb->Exportable) $Doc->ExportField($this->PrvNomb);
						if ($this->PrvApel->Exportable) $Doc->ExportField($this->PrvApel);
						if ($this->PrvCRuc->Exportable) $Doc->ExportField($this->PrvCRuc);
						if ($this->PrvDire->Exportable) $Doc->ExportField($this->PrvDire);
						if ($this->PrvMail->Exportable) $Doc->ExportField($this->PrvMail);
						if ($this->PrvTele->Exportable) $Doc->ExportField($this->PrvTele);
						if ($this->PrvRSoc->Exportable) $Doc->ExportField($this->PrvRSoc);
					}
					$Doc->EndExportRow();
				}
			}

			// Call Row Export server event
			if ($Doc->ExportCustom)
				$this->Row_Export($Recordset->fields);
			$Recordset->MoveNext();
		}
		if (!$Doc->ExportCustom) {
			$Doc->ExportTableFooter();
		}
	}

	// Get auto fill value
	function GetAutoFill($id, $val) {
		$rsarr = array();
		$rowcnt = 0;

		// Output
		if (is_array($rsarr) && $rowcnt > 0) {
			$fldcnt = count($rsarr[0]);
			for ($i = 0; $i < $rowcnt; $i++) {
				for ($j = 0; $j < $fldcnt; $j++) {
					$str = strval($rsarr[$i][$j]);
					$str = ew_ConvertToUtf8($str);
					if (isset($post["keepCRLF"])) {
						$str = str_replace(array("\r", "\n"), array("\\r", "\\n"), $str);
					} else {
						$str = str_replace(array("\r", "\n"), array(" ", " "), $str);
					}
					$rsarr[$i][$j] = $str;
				}
			}
			return ew_ArrayToJson($rsarr);
		} else {
			return FALSE;
		}
	}

	// Table level events
	// Recordset Selecting event
	function Recordset_Selecting(&$filter) {

		// Enter your code here	
	}

	// Recordset Selected event
	function Recordset_Selected(&$rs) {

		//echo "Recordset Selected";
	}

	// Recordset Search Validated event
	function Recordset_SearchValidated() {

		// Example:
		//$this->MyField1->AdvancedSearch->SearchValue = "your search criteria"; // Search value

	}

	// Recordset Searching event
	function Recordset_Searching(&$filter) {

		// Enter your code here	
	}

	// Row_Selecting event
	function Row_Selecting(&$filter) {

		// Enter your code here	
	}

	// Row Selected event
	function Row_Selected(&$rs) {

		//echo "Row Selected";
	}

	// Row Inserting event
	function Row_Inserting($rsold, &$rsnew) {

		// Enter your code here
		// To cancel, set return value to FALSE

		return TRUE;
	}

	// Row Inserted event
	function Row_Inserted($rsold, &$rsnew) {

		//echo "Row Inserted"
	}

	// Row Updating event
	function Row_Updating($rsold, &$rsnew) {

		// Enter your code here
		// To cancel, set return value to FALSE

		return TRUE;
	}

	// Row Updated event
	function Row_Updated($rsold, &$rsnew) {

		//echo "Row Updated";
	}

	// Row Update Conflict event
	function Row_UpdateConflict($rsold, &$rsnew) {

		// Enter your code here
		// To ignore conflict, set return value to FALSE

		return TRUE;
	}

	// Grid Inserting event
	function Grid_Inserting() {

		// Enter your code here
		// To reject grid insert, set return value to FALSE

		return TRUE;
	}

	// Grid Inserted event
	function Grid_Inserted($rsnew) {

		//echo "Grid Inserted";
	}

	// Grid Updating event
	function Grid_Updating($rsold) {

		// Enter your code here
		// To reject grid update, set return value to FALSE

		return TRUE;
	}

	// Grid Updated event
	function Grid_Updated($rsold, $rsnew) {

		//echo "Grid Updated";
	}

	// Row Deleting event
	function Row_Deleting(&$rs) {

		// Enter your code here
		// To cancel, set return value to False

		return TRUE;
	}

	// Row Deleted event
	function Row_Deleted(&$rs) {

		//echo "Row Deleted";
	}

	// Email Sending event
	function Email_Sending(&$Email, &$Args) {

		//var_dump($Email); var_dump($Args); exit();
		return TRUE;
	}

	// Lookup Selecting event
	function Lookup_Selecting($fld, &$filter) {

		//var_dump($fld->FldName, $fld->LookupFilters, $filter); // Uncomment to view the filter
		// Enter your code here

	}

	// Row Rendering event
	function Row_Rendering() {

		// Enter your code here	
	}

	// Row Rendered event
	function Row_Rendered() {

		// To view properties of field class, use:
		//var_dump($this-><FieldName>); 

	}

	// User ID Filtering event
	function UserID_Filtering(&$filter) {

		// Enter your code here
	}
}
?>
