<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "CCreinfo.php" ?>
<?php include_once "Usuainfo.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$CCre_list = NULL; // Initialize page object first

class cCCre_list extends cCCre {

	// Page ID
	var $PageID = 'list';

	// Project ID
	var $ProjectID = "{04439FF7-B43F-460F-8514-F71C8FF9E679}";

	// Table name
	var $TableName = 'CCre';

	// Page object name
	var $PageObjName = 'CCre_list';

	// Grid form hidden field names
	var $FormName = 'fCCrelist';
	var $FormActionName = 'k_action';
	var $FormKeyName = 'k_key';
	var $FormOldKeyName = 'k_oldkey';
	var $FormBlankRowName = 'k_blankrow';
	var $FormKeyCountName = 'key_count';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Page URLs
	var $AddUrl;
	var $EditUrl;
	var $CopyUrl;
	var $DeleteUrl;
	var $ViewUrl;
	var $ListUrl;

	// Export URLs
	var $ExportPrintUrl;
	var $ExportHtmlUrl;
	var $ExportExcelUrl;
	var $ExportWordUrl;
	var $ExportXmlUrl;
	var $ExportCsvUrl;
	var $ExportPdfUrl;

	// Custom export
	var $ExportExcelCustom = FALSE;
	var $ExportWordCustom = FALSE;
	var $ExportPdfCustom = FALSE;
	var $ExportEmailCustom = FALSE;

	// Update URLs
	var $InlineAddUrl;
	var $InlineCopyUrl;
	var $InlineEditUrl;
	var $GridAddUrl;
	var $GridEditUrl;
	var $MultiDeleteUrl;
	var $MultiUpdateUrl;

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (CCre)
		if (!isset($GLOBALS["CCre"]) || get_class($GLOBALS["CCre"]) == "cCCre") {
			$GLOBALS["CCre"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["CCre"];
		}

		// Initialize URLs
		$this->ExportPrintUrl = $this->PageUrl() . "export=print";
		$this->ExportExcelUrl = $this->PageUrl() . "export=excel";
		$this->ExportWordUrl = $this->PageUrl() . "export=word";
		$this->ExportHtmlUrl = $this->PageUrl() . "export=html";
		$this->ExportXmlUrl = $this->PageUrl() . "export=xml";
		$this->ExportCsvUrl = $this->PageUrl() . "export=csv";
		$this->ExportPdfUrl = $this->PageUrl() . "export=pdf";
		$this->AddUrl = "CCreadd.php";
		$this->InlineAddUrl = $this->PageUrl() . "a=add";
		$this->GridAddUrl = $this->PageUrl() . "a=gridadd";
		$this->GridEditUrl = $this->PageUrl() . "a=gridedit";
		$this->MultiDeleteUrl = "CCredelete.php";
		$this->MultiUpdateUrl = "CCreupdate.php";

		// Table object (Usua)
		if (!isset($GLOBALS['Usua'])) $GLOBALS['Usua'] = new cUsua();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'list', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'CCre', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (Usua)
		if (!isset($UserTable)) {
			$UserTable = new cUsua();
			$UserTableConn = Conn($UserTable->DBID);
		}

		// List options
		$this->ListOptions = new cListOptions();
		$this->ListOptions->TableVar = $this->TableVar;

		// Export options
		$this->ExportOptions = new cListOptions();
		$this->ExportOptions->Tag = "div";
		$this->ExportOptions->TagClassName = "ewExportOption";

		// Other options
		$this->OtherOptions['addedit'] = new cListOptions();
		$this->OtherOptions['addedit']->Tag = "div";
		$this->OtherOptions['addedit']->TagClassName = "ewAddEditOption";
		$this->OtherOptions['detail'] = new cListOptions();
		$this->OtherOptions['detail']->Tag = "div";
		$this->OtherOptions['detail']->TagClassName = "ewDetailOption";
		$this->OtherOptions['action'] = new cListOptions();
		$this->OtherOptions['action']->Tag = "div";
		$this->OtherOptions['action']->TagClassName = "ewActionOption";

		// Filter options
		$this->FilterOptions = new cListOptions();
		$this->FilterOptions->Tag = "div";
		$this->FilterOptions->TagClassName = "ewFilterOption fCCrelistsrch";

		// List actions
		$this->ListActions = new cListActions();
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanList()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage($Language->Phrase("NoPermission")); // Set no permission
			$this->Page_Terminate(ew_GetUrl("index.php"));
		}

		// Create form object
		$objForm = new cFormObj();
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action

		// Get grid add count
		$gridaddcnt = @$_GET[EW_TABLE_GRID_ADD_ROW_COUNT];
		if (is_numeric($gridaddcnt) && $gridaddcnt > 0)
			$this->GridAddRowCount = $gridaddcnt;

		// Set up list options
		$this->SetupListOptions();
		$this->CcrCodi->Visible = !$this->IsAdd() && !$this->IsCopy() && !$this->IsGridAdd();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Process auto fill
		if (@$_POST["ajax"] == "autofill") {
			$results = $this->GetAutoFill(@$_POST["name"], @$_POST["q"]);
			if ($results) {

				// Clean output buffer
				if (!EW_DEBUG_ENABLED && ob_get_length())
					ob_end_clean();
				echo $results;
				$this->Page_Terminate();
				exit();
			}
		}

		// Create Token
		$this->CreateToken();

		// Setup other options
		$this->SetupOtherOptions();

		// Set up custom action (compatible with old version)
		foreach ($this->CustomActions as $name => $action)
			$this->ListActions->Add($name, $action);

		// Show checkbox column if multiple action
		foreach ($this->ListActions->Items as $listaction) {
			if ($listaction->Select == EW_ACTION_MULTIPLE && $listaction->Allow) {
				$this->ListOptions->Items["checkbox"]->Visible = TRUE;
				break;
			}
		}
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $CCre;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($CCre);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}

	// Class variables
	var $ListOptions; // List options
	var $ExportOptions; // Export options
	var $SearchOptions; // Search options
	var $OtherOptions = array(); // Other options
	var $FilterOptions; // Filter options
	var $ListActions; // List actions
	var $SelectedCount = 0;
	var $SelectedIndex = 0;
	var $DisplayRecs = 20;
	var $StartRec;
	var $StopRec;
	var $TotalRecs = 0;
	var $RecRange = 10;
	var $Pager;
	var $DefaultSearchWhere = ""; // Default search WHERE clause
	var $SearchWhere = ""; // Search WHERE clause
	var $RecCnt = 0; // Record count
	var $EditRowCnt;
	var $StartRowCnt = 1;
	var $RowCnt = 0;
	var $Attrs = array(); // Row attributes and cell attributes
	var $RowIndex = 0; // Row index
	var $KeyCount = 0; // Key count
	var $RowAction = ""; // Row action
	var $RowOldKey = ""; // Row old key (for copy)
	var $RecPerRow = 0;
	var $MultiColumnClass;
	var $MultiColumnEditClass = "col-sm-12";
	var $MultiColumnCnt = 12;
	var $MultiColumnEditCnt = 12;
	var $GridCnt = 0;
	var $ColCnt = 0;
	var $DbMasterFilter = ""; // Master filter
	var $DbDetailFilter = ""; // Detail filter
	var $MasterRecordExists;	
	var $MultiSelectKey;
	var $Command;
	var $RestoreSearch = FALSE;
	var $DetailPages;
	var $Recordset;
	var $OldRecordset;

	//
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError, $gsSearchError, $Security;

		// Search filters
		$sSrchAdvanced = ""; // Advanced search filter
		$sSrchBasic = ""; // Basic search filter
		$sFilter = "";

		// Get command
		$this->Command = strtolower(@$_GET["cmd"]);
		if ($this->IsPageRequest()) { // Validate request

			// Process list action first
			if ($this->ProcessListAction()) // Ajax request
				$this->Page_Terminate();

			// Handle reset command
			$this->ResetCmd();

			// Set up Breadcrumb
			if ($this->Export == "")
				$this->SetupBreadcrumb();

			// Check QueryString parameters
			if (@$_GET["a"] <> "") {
				$this->CurrentAction = $_GET["a"];

				// Clear inline mode
				if ($this->CurrentAction == "cancel")
					$this->ClearInlineMode();

				// Switch to grid edit mode
				if ($this->CurrentAction == "gridedit")
					$this->GridEditMode();

				// Switch to grid add mode
				if ($this->CurrentAction == "gridadd")
					$this->GridAddMode();
			} else {
				if (@$_POST["a_list"] <> "") {
					$this->CurrentAction = $_POST["a_list"]; // Get action

					// Grid Update
					if (($this->CurrentAction == "gridupdate" || $this->CurrentAction == "gridoverwrite") && @$_SESSION[EW_SESSION_INLINE_MODE] == "gridedit") {
						if ($this->ValidateGridForm()) {
							$bGridUpdate = $this->GridUpdate();
						} else {
							$bGridUpdate = FALSE;
							$this->setFailureMessage($gsFormError);
						}
						if (!$bGridUpdate) {
							$this->EventCancelled = TRUE;
							$this->CurrentAction = "gridedit"; // Stay in Grid Edit mode
						}
					}

					// Grid Insert
					if ($this->CurrentAction == "gridinsert" && @$_SESSION[EW_SESSION_INLINE_MODE] == "gridadd") {
						if ($this->ValidateGridForm()) {
							$bGridInsert = $this->GridInsert();
						} else {
							$bGridInsert = FALSE;
							$this->setFailureMessage($gsFormError);
						}
						if (!$bGridInsert) {
							$this->EventCancelled = TRUE;
							$this->CurrentAction = "gridadd"; // Stay in Grid Add mode
						}
					}
				}
			}

			// Hide list options
			if ($this->Export <> "") {
				$this->ListOptions->HideAllOptions(array("sequence"));
				$this->ListOptions->UseDropDownButton = FALSE; // Disable drop down button
				$this->ListOptions->UseButtonGroup = FALSE; // Disable button group
			} elseif ($this->CurrentAction == "gridadd" || $this->CurrentAction == "gridedit") {
				$this->ListOptions->HideAllOptions();
				$this->ListOptions->UseDropDownButton = FALSE; // Disable drop down button
				$this->ListOptions->UseButtonGroup = FALSE; // Disable button group
			}

			// Hide options
			if ($this->Export <> "" || $this->CurrentAction <> "") {
				$this->ExportOptions->HideAllOptions();
				$this->FilterOptions->HideAllOptions();
			}

			// Hide other options
			if ($this->Export <> "") {
				foreach ($this->OtherOptions as &$option)
					$option->HideAllOptions();
			}

			// Show grid delete link for grid add / grid edit
			if ($this->AllowAddDeleteRow) {
				if ($this->CurrentAction == "gridadd" || $this->CurrentAction == "gridedit") {
					$item = $this->ListOptions->GetItem("griddelete");
					if ($item) $item->Visible = TRUE;
				}
			}

			// Get default search criteria
			ew_AddFilter($this->DefaultSearchWhere, $this->BasicSearchWhere(TRUE));

			// Get basic search values
			$this->LoadBasicSearchValues();

			// Restore filter list
			$this->RestoreFilterList();

			// Restore search parms from Session if not searching / reset / export
			if (($this->Export <> "" || $this->Command <> "search" && $this->Command <> "reset" && $this->Command <> "resetall") && $this->CheckSearchParms())
				$this->RestoreSearchParms();

			// Call Recordset SearchValidated event
			$this->Recordset_SearchValidated();

			// Set up sorting order
			$this->SetUpSortOrder();

			// Get basic search criteria
			if ($gsSearchError == "")
				$sSrchBasic = $this->BasicSearchWhere();
		}

		// Restore display records
		if ($this->getRecordsPerPage() <> "") {
			$this->DisplayRecs = $this->getRecordsPerPage(); // Restore from Session
		} else {
			$this->DisplayRecs = 20; // Load default
		}

		// Load Sorting Order
		$this->LoadSortOrder();

		// Load search default if no existing search criteria
		if (!$this->CheckSearchParms()) {

			// Load basic search from default
			$this->BasicSearch->LoadDefault();
			if ($this->BasicSearch->Keyword != "")
				$sSrchBasic = $this->BasicSearchWhere();
		}

		// Build search criteria
		ew_AddFilter($this->SearchWhere, $sSrchAdvanced);
		ew_AddFilter($this->SearchWhere, $sSrchBasic);

		// Call Recordset_Searching event
		$this->Recordset_Searching($this->SearchWhere);

		// Save search criteria
		if ($this->Command == "search" && !$this->RestoreSearch) {
			$this->setSearchWhere($this->SearchWhere); // Save to Session
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} else {
			$this->SearchWhere = $this->getSearchWhere();
		}

		// Build filter
		$sFilter = "";
		if (!$Security->CanList())
			$sFilter = "(0=1)"; // Filter all records
		ew_AddFilter($sFilter, $this->DbDetailFilter);
		ew_AddFilter($sFilter, $this->SearchWhere);

		// Set up filter in session
		$this->setSessionWhere($sFilter);
		$this->CurrentFilter = "";

		// Load record count first
		if (!$this->IsAddOrEdit()) {
			$bSelectLimit = $this->UseSelectLimit;
			if ($bSelectLimit) {
				$this->TotalRecs = $this->SelectRecordCount();
			} else {
				if ($this->Recordset = $this->LoadRecordset())
					$this->TotalRecs = $this->Recordset->RecordCount();
			}
		}

		// Search options
		$this->SetupSearchOptions();
	}

	//  Exit inline mode
	function ClearInlineMode() {
		$this->CcrMont->FormValue = ""; // Clear form value
		$this->CcrMora->FormValue = ""; // Clear form value
		$this->LastAction = $this->CurrentAction; // Save last action
		$this->CurrentAction = ""; // Clear action
		$_SESSION[EW_SESSION_INLINE_MODE] = ""; // Clear inline mode
	}

	// Switch to Grid Add mode
	function GridAddMode() {
		$_SESSION[EW_SESSION_INLINE_MODE] = "gridadd"; // Enabled grid add
	}

	// Switch to Grid Edit mode
	function GridEditMode() {
		$_SESSION[EW_SESSION_INLINE_MODE] = "gridedit"; // Enable grid edit
	}

	// Perform update to grid
	function GridUpdate() {
		global $Language, $objForm, $gsFormError;
		$bGridUpdate = TRUE;

		// Get old recordset
		$this->CurrentFilter = $this->BuildKeyFilter();
		if ($this->CurrentFilter == "")
			$this->CurrentFilter = "0=1";
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		if ($rs = $conn->Execute($sSql)) {
			$rsold = $rs->GetRows();
			$rs->Close();
		}

		// Call Grid Updating event
		if (!$this->Grid_Updating($rsold)) {
			if ($this->getFailureMessage() == "")
				$this->setFailureMessage($Language->Phrase("GridEditCancelled")); // Set grid edit cancelled message
			return FALSE;
		}

		// Begin transaction
		$conn->BeginTrans();
		$sKey = "";

		// Update row index and get row key
		$objForm->Index = -1;
		$rowcnt = strval($objForm->GetValue($this->FormKeyCountName));
		if ($rowcnt == "" || !is_numeric($rowcnt))
			$rowcnt = 0;

		// Update all rows based on key
		for ($rowindex = 1; $rowindex <= $rowcnt; $rowindex++) {
			$objForm->Index = $rowindex;
			$rowkey = strval($objForm->GetValue($this->FormKeyName));
			$rowaction = strval($objForm->GetValue($this->FormActionName));

			// Load all values and keys
			if ($rowaction <> "insertdelete") { // Skip insert then deleted rows
				$this->LoadFormValues(); // Get form values
				if ($rowaction == "" || $rowaction == "edit" || $rowaction == "delete") {
					$bGridUpdate = $this->SetupKeyValues($rowkey); // Set up key values
				} else {
					$bGridUpdate = TRUE;
				}

				// Skip empty row
				if ($rowaction == "insert" && $this->EmptyRow()) {

					// No action required
				// Validate form and insert/update/delete record

				} elseif ($bGridUpdate) {
					if ($rowaction == "delete") {
						$this->CurrentFilter = $this->KeyFilter();
						$bGridUpdate = $this->DeleteRows(); // Delete this row
					} else if (!$this->ValidateForm()) {
						$bGridUpdate = FALSE; // Form error, reset action
						$this->setFailureMessage($gsFormError);
					} else {
						if ($rowaction == "insert") {
							$bGridUpdate = $this->AddRow(); // Insert this row
						} else {
							if ($rowkey <> "") {
								$this->SendEmail = FALSE; // Do not send email on update success
								$bGridUpdate = $this->EditRow(); // Update this row
							}
						} // End update
					}
				}
				if ($bGridUpdate) {
					if ($sKey <> "") $sKey .= ", ";
					$sKey .= $rowkey;
				} else {
					break;
				}
			}
		}
		if ($bGridUpdate) {
			$conn->CommitTrans(); // Commit transaction

			// Get new recordset
			if ($rs = $conn->Execute($sSql)) {
				$rsnew = $rs->GetRows();
				$rs->Close();
			}

			// Call Grid_Updated event
			$this->Grid_Updated($rsold, $rsnew);
			if ($this->getSuccessMessage() == "")
				$this->setSuccessMessage($Language->Phrase("UpdateSuccess")); // Set up update success message
			$this->ClearInlineMode(); // Clear inline edit mode
		} else {
			$conn->RollbackTrans(); // Rollback transaction
			if ($this->getFailureMessage() == "")
				$this->setFailureMessage($Language->Phrase("UpdateFailed")); // Set update failed message
		}
		return $bGridUpdate;
	}

	// Build filter for all keys
	function BuildKeyFilter() {
		global $objForm;
		$sWrkFilter = "";

		// Update row index and get row key
		$rowindex = 1;
		$objForm->Index = $rowindex;
		$sThisKey = strval($objForm->GetValue($this->FormKeyName));
		while ($sThisKey <> "") {
			if ($this->SetupKeyValues($sThisKey)) {
				$sFilter = $this->KeyFilter();
				if ($sWrkFilter <> "") $sWrkFilter .= " OR ";
				$sWrkFilter .= $sFilter;
			} else {
				$sWrkFilter = "0=1";
				break;
			}

			// Update row index and get row key
			$rowindex++; // Next row
			$objForm->Index = $rowindex;
			$sThisKey = strval($objForm->GetValue($this->FormKeyName));
		}
		return $sWrkFilter;
	}

	// Set up key values
	function SetupKeyValues($key) {
		$arrKeyFlds = explode($GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"], $key);
		if (count($arrKeyFlds) >= 1) {
			$this->CcrCodi->setFormValue($arrKeyFlds[0]);
			if (!is_numeric($this->CcrCodi->FormValue))
				return FALSE;
		}
		return TRUE;
	}

	// Perform Grid Add
	function GridInsert() {
		global $Language, $objForm, $gsFormError;
		$rowindex = 1;
		$bGridInsert = FALSE;
		$conn = &$this->Connection();

		// Call Grid Inserting event
		if (!$this->Grid_Inserting()) {
			if ($this->getFailureMessage() == "") {
				$this->setFailureMessage($Language->Phrase("GridAddCancelled")); // Set grid add cancelled message
			}
			return FALSE;
		}

		// Begin transaction
		$conn->BeginTrans();

		// Init key filter
		$sWrkFilter = "";
		$addcnt = 0;
		$sKey = "";

		// Get row count
		$objForm->Index = -1;
		$rowcnt = strval($objForm->GetValue($this->FormKeyCountName));
		if ($rowcnt == "" || !is_numeric($rowcnt))
			$rowcnt = 0;

		// Insert all rows
		for ($rowindex = 1; $rowindex <= $rowcnt; $rowindex++) {

			// Load current row values
			$objForm->Index = $rowindex;
			$rowaction = strval($objForm->GetValue($this->FormActionName));
			if ($rowaction <> "" && $rowaction <> "insert")
				continue; // Skip
			$this->LoadFormValues(); // Get form values
			if (!$this->EmptyRow()) {
				$addcnt++;
				$this->SendEmail = FALSE; // Do not send email on insert success

				// Validate form
				if (!$this->ValidateForm()) {
					$bGridInsert = FALSE; // Form error, reset action
					$this->setFailureMessage($gsFormError);
				} else {
					$bGridInsert = $this->AddRow($this->OldRecordset); // Insert this row
				}
				if ($bGridInsert) {
					if ($sKey <> "") $sKey .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
					$sKey .= $this->CcrCodi->CurrentValue;

					// Add filter for this record
					$sFilter = $this->KeyFilter();
					if ($sWrkFilter <> "") $sWrkFilter .= " OR ";
					$sWrkFilter .= $sFilter;
				} else {
					break;
				}
			}
		}
		if ($addcnt == 0) { // No record inserted
			$this->setFailureMessage($Language->Phrase("NoAddRecord"));
			$bGridInsert = FALSE;
		}
		if ($bGridInsert) {
			$conn->CommitTrans(); // Commit transaction

			// Get new recordset
			$this->CurrentFilter = $sWrkFilter;
			$sSql = $this->SQL();
			if ($rs = $conn->Execute($sSql)) {
				$rsnew = $rs->GetRows();
				$rs->Close();
			}

			// Call Grid_Inserted event
			$this->Grid_Inserted($rsnew);
			if ($this->getSuccessMessage() == "")
				$this->setSuccessMessage($Language->Phrase("InsertSuccess")); // Set up insert success message
			$this->ClearInlineMode(); // Clear grid add mode
		} else {
			$conn->RollbackTrans(); // Rollback transaction
			if ($this->getFailureMessage() == "") {
				$this->setFailureMessage($Language->Phrase("InsertFailed")); // Set insert failed message
			}
		}
		return $bGridInsert;
	}

	// Check if empty row
	function EmptyRow() {
		global $objForm;
		if ($objForm->HasValue("x_VcrCodi") && $objForm->HasValue("o_VcrCodi") && $this->VcrCodi->CurrentValue <> $this->VcrCodi->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_CcrNCuo") && $objForm->HasValue("o_CcrNCuo") && $this->CcrNCuo->CurrentValue <> $this->CcrNCuo->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_CcrFPag") && $objForm->HasValue("o_CcrFPag") && $this->CcrFPag->CurrentValue <> $this->CcrFPag->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_CcrMont") && $objForm->HasValue("o_CcrMont") && $this->CcrMont->CurrentValue <> $this->CcrMont->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_CcrMora") && $objForm->HasValue("o_CcrMora") && $this->CcrMora->CurrentValue <> $this->CcrMora->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_CcrDAtr") && $objForm->HasValue("o_CcrDAtr") && $this->CcrDAtr->CurrentValue <> $this->CcrDAtr->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_CcrEsta") && $objForm->HasValue("o_CcrEsta") && $this->CcrEsta->CurrentValue <> $this->CcrEsta->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_CcrAnho") && $objForm->HasValue("o_CcrAnho") && $this->CcrAnho->CurrentValue <> $this->CcrAnho->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_CcrMes") && $objForm->HasValue("o_CcrMes") && $this->CcrMes->CurrentValue <> $this->CcrMes->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_CcrConc") && $objForm->HasValue("o_CcrConc") && $this->CcrConc->CurrentValue <> $this->CcrConc->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_CcrUsua") && $objForm->HasValue("o_CcrUsua") && $this->CcrUsua->CurrentValue <> $this->CcrUsua->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_CcrFCre") && $objForm->HasValue("o_CcrFCre") && $this->CcrFCre->CurrentValue <> $this->CcrFCre->OldValue)
			return FALSE;
		return TRUE;
	}

	// Validate grid form
	function ValidateGridForm() {
		global $objForm;

		// Get row count
		$objForm->Index = -1;
		$rowcnt = strval($objForm->GetValue($this->FormKeyCountName));
		if ($rowcnt == "" || !is_numeric($rowcnt))
			$rowcnt = 0;

		// Validate all records
		for ($rowindex = 1; $rowindex <= $rowcnt; $rowindex++) {

			// Load current row values
			$objForm->Index = $rowindex;
			$rowaction = strval($objForm->GetValue($this->FormActionName));
			if ($rowaction <> "delete" && $rowaction <> "insertdelete") {
				$this->LoadFormValues(); // Get form values
				if ($rowaction == "insert" && $this->EmptyRow()) {

					// Ignore
				} else if (!$this->ValidateForm()) {
					return FALSE;
				}
			}
		}
		return TRUE;
	}

	// Get all form values of the grid
	function GetGridFormValues() {
		global $objForm;

		// Get row count
		$objForm->Index = -1;
		$rowcnt = strval($objForm->GetValue($this->FormKeyCountName));
		if ($rowcnt == "" || !is_numeric($rowcnt))
			$rowcnt = 0;
		$rows = array();

		// Loop through all records
		for ($rowindex = 1; $rowindex <= $rowcnt; $rowindex++) {

			// Load current row values
			$objForm->Index = $rowindex;
			$rowaction = strval($objForm->GetValue($this->FormActionName));
			if ($rowaction <> "delete" && $rowaction <> "insertdelete") {
				$this->LoadFormValues(); // Get form values
				if ($rowaction == "insert" && $this->EmptyRow()) {

					// Ignore
				} else {
					$rows[] = $this->GetFieldValues("FormValue"); // Return row as array
				}
			}
		}
		return $rows; // Return as array of array
	}

	// Restore form values for current row
	function RestoreCurrentRowFormValues($idx) {
		global $objForm;

		// Get row based on current index
		$objForm->Index = $idx;
		$this->LoadFormValues(); // Load form values
	}

	// Get list of filters
	function GetFilterList() {

		// Initialize
		$sFilterList = "";
		$sFilterList = ew_Concat($sFilterList, $this->CcrCodi->AdvancedSearch->ToJSON(), ","); // Field CcrCodi
		$sFilterList = ew_Concat($sFilterList, $this->VcrCodi->AdvancedSearch->ToJSON(), ","); // Field VcrCodi
		$sFilterList = ew_Concat($sFilterList, $this->CcrNCuo->AdvancedSearch->ToJSON(), ","); // Field CcrNCuo
		$sFilterList = ew_Concat($sFilterList, $this->CcrFPag->AdvancedSearch->ToJSON(), ","); // Field CcrFPag
		$sFilterList = ew_Concat($sFilterList, $this->CcrMont->AdvancedSearch->ToJSON(), ","); // Field CcrMont
		$sFilterList = ew_Concat($sFilterList, $this->CcrMora->AdvancedSearch->ToJSON(), ","); // Field CcrMora
		$sFilterList = ew_Concat($sFilterList, $this->CcrDAtr->AdvancedSearch->ToJSON(), ","); // Field CcrDAtr
		$sFilterList = ew_Concat($sFilterList, $this->CcrEsta->AdvancedSearch->ToJSON(), ","); // Field CcrEsta
		$sFilterList = ew_Concat($sFilterList, $this->CcrAnho->AdvancedSearch->ToJSON(), ","); // Field CcrAnho
		$sFilterList = ew_Concat($sFilterList, $this->CcrMes->AdvancedSearch->ToJSON(), ","); // Field CcrMes
		$sFilterList = ew_Concat($sFilterList, $this->CcrConc->AdvancedSearch->ToJSON(), ","); // Field CcrConc
		$sFilterList = ew_Concat($sFilterList, $this->CcrUsua->AdvancedSearch->ToJSON(), ","); // Field CcrUsua
		$sFilterList = ew_Concat($sFilterList, $this->CcrFCre->AdvancedSearch->ToJSON(), ","); // Field CcrFCre
		if ($this->BasicSearch->Keyword <> "") {
			$sWrk = "\"" . EW_TABLE_BASIC_SEARCH . "\":\"" . ew_JsEncode2($this->BasicSearch->Keyword) . "\",\"" . EW_TABLE_BASIC_SEARCH_TYPE . "\":\"" . ew_JsEncode2($this->BasicSearch->Type) . "\"";
			$sFilterList = ew_Concat($sFilterList, $sWrk, ",");
		}

		// Return filter list in json
		return ($sFilterList <> "") ? "{" . $sFilterList . "}" : "null";
	}

	// Restore list of filters
	function RestoreFilterList() {

		// Return if not reset filter
		if (@$_POST["cmd"] <> "resetfilter")
			return FALSE;
		$filter = json_decode(ew_StripSlashes(@$_POST["filter"]), TRUE);
		$this->Command = "search";

		// Field CcrCodi
		$this->CcrCodi->AdvancedSearch->SearchValue = @$filter["x_CcrCodi"];
		$this->CcrCodi->AdvancedSearch->SearchOperator = @$filter["z_CcrCodi"];
		$this->CcrCodi->AdvancedSearch->SearchCondition = @$filter["v_CcrCodi"];
		$this->CcrCodi->AdvancedSearch->SearchValue2 = @$filter["y_CcrCodi"];
		$this->CcrCodi->AdvancedSearch->SearchOperator2 = @$filter["w_CcrCodi"];
		$this->CcrCodi->AdvancedSearch->Save();

		// Field VcrCodi
		$this->VcrCodi->AdvancedSearch->SearchValue = @$filter["x_VcrCodi"];
		$this->VcrCodi->AdvancedSearch->SearchOperator = @$filter["z_VcrCodi"];
		$this->VcrCodi->AdvancedSearch->SearchCondition = @$filter["v_VcrCodi"];
		$this->VcrCodi->AdvancedSearch->SearchValue2 = @$filter["y_VcrCodi"];
		$this->VcrCodi->AdvancedSearch->SearchOperator2 = @$filter["w_VcrCodi"];
		$this->VcrCodi->AdvancedSearch->Save();

		// Field CcrNCuo
		$this->CcrNCuo->AdvancedSearch->SearchValue = @$filter["x_CcrNCuo"];
		$this->CcrNCuo->AdvancedSearch->SearchOperator = @$filter["z_CcrNCuo"];
		$this->CcrNCuo->AdvancedSearch->SearchCondition = @$filter["v_CcrNCuo"];
		$this->CcrNCuo->AdvancedSearch->SearchValue2 = @$filter["y_CcrNCuo"];
		$this->CcrNCuo->AdvancedSearch->SearchOperator2 = @$filter["w_CcrNCuo"];
		$this->CcrNCuo->AdvancedSearch->Save();

		// Field CcrFPag
		$this->CcrFPag->AdvancedSearch->SearchValue = @$filter["x_CcrFPag"];
		$this->CcrFPag->AdvancedSearch->SearchOperator = @$filter["z_CcrFPag"];
		$this->CcrFPag->AdvancedSearch->SearchCondition = @$filter["v_CcrFPag"];
		$this->CcrFPag->AdvancedSearch->SearchValue2 = @$filter["y_CcrFPag"];
		$this->CcrFPag->AdvancedSearch->SearchOperator2 = @$filter["w_CcrFPag"];
		$this->CcrFPag->AdvancedSearch->Save();

		// Field CcrMont
		$this->CcrMont->AdvancedSearch->SearchValue = @$filter["x_CcrMont"];
		$this->CcrMont->AdvancedSearch->SearchOperator = @$filter["z_CcrMont"];
		$this->CcrMont->AdvancedSearch->SearchCondition = @$filter["v_CcrMont"];
		$this->CcrMont->AdvancedSearch->SearchValue2 = @$filter["y_CcrMont"];
		$this->CcrMont->AdvancedSearch->SearchOperator2 = @$filter["w_CcrMont"];
		$this->CcrMont->AdvancedSearch->Save();

		// Field CcrMora
		$this->CcrMora->AdvancedSearch->SearchValue = @$filter["x_CcrMora"];
		$this->CcrMora->AdvancedSearch->SearchOperator = @$filter["z_CcrMora"];
		$this->CcrMora->AdvancedSearch->SearchCondition = @$filter["v_CcrMora"];
		$this->CcrMora->AdvancedSearch->SearchValue2 = @$filter["y_CcrMora"];
		$this->CcrMora->AdvancedSearch->SearchOperator2 = @$filter["w_CcrMora"];
		$this->CcrMora->AdvancedSearch->Save();

		// Field CcrDAtr
		$this->CcrDAtr->AdvancedSearch->SearchValue = @$filter["x_CcrDAtr"];
		$this->CcrDAtr->AdvancedSearch->SearchOperator = @$filter["z_CcrDAtr"];
		$this->CcrDAtr->AdvancedSearch->SearchCondition = @$filter["v_CcrDAtr"];
		$this->CcrDAtr->AdvancedSearch->SearchValue2 = @$filter["y_CcrDAtr"];
		$this->CcrDAtr->AdvancedSearch->SearchOperator2 = @$filter["w_CcrDAtr"];
		$this->CcrDAtr->AdvancedSearch->Save();

		// Field CcrEsta
		$this->CcrEsta->AdvancedSearch->SearchValue = @$filter["x_CcrEsta"];
		$this->CcrEsta->AdvancedSearch->SearchOperator = @$filter["z_CcrEsta"];
		$this->CcrEsta->AdvancedSearch->SearchCondition = @$filter["v_CcrEsta"];
		$this->CcrEsta->AdvancedSearch->SearchValue2 = @$filter["y_CcrEsta"];
		$this->CcrEsta->AdvancedSearch->SearchOperator2 = @$filter["w_CcrEsta"];
		$this->CcrEsta->AdvancedSearch->Save();

		// Field CcrAnho
		$this->CcrAnho->AdvancedSearch->SearchValue = @$filter["x_CcrAnho"];
		$this->CcrAnho->AdvancedSearch->SearchOperator = @$filter["z_CcrAnho"];
		$this->CcrAnho->AdvancedSearch->SearchCondition = @$filter["v_CcrAnho"];
		$this->CcrAnho->AdvancedSearch->SearchValue2 = @$filter["y_CcrAnho"];
		$this->CcrAnho->AdvancedSearch->SearchOperator2 = @$filter["w_CcrAnho"];
		$this->CcrAnho->AdvancedSearch->Save();

		// Field CcrMes
		$this->CcrMes->AdvancedSearch->SearchValue = @$filter["x_CcrMes"];
		$this->CcrMes->AdvancedSearch->SearchOperator = @$filter["z_CcrMes"];
		$this->CcrMes->AdvancedSearch->SearchCondition = @$filter["v_CcrMes"];
		$this->CcrMes->AdvancedSearch->SearchValue2 = @$filter["y_CcrMes"];
		$this->CcrMes->AdvancedSearch->SearchOperator2 = @$filter["w_CcrMes"];
		$this->CcrMes->AdvancedSearch->Save();

		// Field CcrConc
		$this->CcrConc->AdvancedSearch->SearchValue = @$filter["x_CcrConc"];
		$this->CcrConc->AdvancedSearch->SearchOperator = @$filter["z_CcrConc"];
		$this->CcrConc->AdvancedSearch->SearchCondition = @$filter["v_CcrConc"];
		$this->CcrConc->AdvancedSearch->SearchValue2 = @$filter["y_CcrConc"];
		$this->CcrConc->AdvancedSearch->SearchOperator2 = @$filter["w_CcrConc"];
		$this->CcrConc->AdvancedSearch->Save();

		// Field CcrUsua
		$this->CcrUsua->AdvancedSearch->SearchValue = @$filter["x_CcrUsua"];
		$this->CcrUsua->AdvancedSearch->SearchOperator = @$filter["z_CcrUsua"];
		$this->CcrUsua->AdvancedSearch->SearchCondition = @$filter["v_CcrUsua"];
		$this->CcrUsua->AdvancedSearch->SearchValue2 = @$filter["y_CcrUsua"];
		$this->CcrUsua->AdvancedSearch->SearchOperator2 = @$filter["w_CcrUsua"];
		$this->CcrUsua->AdvancedSearch->Save();

		// Field CcrFCre
		$this->CcrFCre->AdvancedSearch->SearchValue = @$filter["x_CcrFCre"];
		$this->CcrFCre->AdvancedSearch->SearchOperator = @$filter["z_CcrFCre"];
		$this->CcrFCre->AdvancedSearch->SearchCondition = @$filter["v_CcrFCre"];
		$this->CcrFCre->AdvancedSearch->SearchValue2 = @$filter["y_CcrFCre"];
		$this->CcrFCre->AdvancedSearch->SearchOperator2 = @$filter["w_CcrFCre"];
		$this->CcrFCre->AdvancedSearch->Save();
		$this->BasicSearch->setKeyword(@$filter[EW_TABLE_BASIC_SEARCH]);
		$this->BasicSearch->setType(@$filter[EW_TABLE_BASIC_SEARCH_TYPE]);
	}

	// Return basic search SQL
	function BasicSearchSQL($arKeywords, $type) {
		$sWhere = "";
		$this->BuildBasicSearchSQL($sWhere, $this->CcrEsta, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->CcrAnho, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->CcrMes, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->CcrConc, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->CcrUsua, $arKeywords, $type);
		return $sWhere;
	}

	// Build basic search SQL
	function BuildBasicSearchSql(&$Where, &$Fld, $arKeywords, $type) {
		$sDefCond = ($type == "OR") ? "OR" : "AND";
		$arSQL = array(); // Array for SQL parts
		$arCond = array(); // Array for search conditions
		$cnt = count($arKeywords);
		$j = 0; // Number of SQL parts
		for ($i = 0; $i < $cnt; $i++) {
			$Keyword = $arKeywords[$i];
			$Keyword = trim($Keyword);
			if (EW_BASIC_SEARCH_IGNORE_PATTERN <> "") {
				$Keyword = preg_replace(EW_BASIC_SEARCH_IGNORE_PATTERN, "\\", $Keyword);
				$ar = explode("\\", $Keyword);
			} else {
				$ar = array($Keyword);
			}
			foreach ($ar as $Keyword) {
				if ($Keyword <> "") {
					$sWrk = "";
					if ($Keyword == "OR" && $type == "") {
						if ($j > 0)
							$arCond[$j-1] = "OR";
					} elseif ($Keyword == EW_NULL_VALUE) {
						$sWrk = $Fld->FldExpression . " IS NULL";
					} elseif ($Keyword == EW_NOT_NULL_VALUE) {
						$sWrk = $Fld->FldExpression . " IS NOT NULL";
					} elseif ($Fld->FldIsVirtual && $Fld->FldVirtualSearch) {
						$sWrk = $Fld->FldVirtualExpression . ew_Like(ew_QuotedValue("%" . $Keyword . "%", EW_DATATYPE_STRING, $this->DBID), $this->DBID);
					} elseif ($Fld->FldDataType != EW_DATATYPE_NUMBER || is_numeric($Keyword)) {
						$sWrk = $Fld->FldBasicSearchExpression . ew_Like(ew_QuotedValue("%" . $Keyword . "%", EW_DATATYPE_STRING, $this->DBID), $this->DBID);
					}
					if ($sWrk <> "") {
						$arSQL[$j] = $sWrk;
						$arCond[$j] = $sDefCond;
						$j += 1;
					}
				}
			}
		}
		$cnt = count($arSQL);
		$bQuoted = FALSE;
		$sSql = "";
		if ($cnt > 0) {
			for ($i = 0; $i < $cnt-1; $i++) {
				if ($arCond[$i] == "OR") {
					if (!$bQuoted) $sSql .= "(";
					$bQuoted = TRUE;
				}
				$sSql .= $arSQL[$i];
				if ($bQuoted && $arCond[$i] <> "OR") {
					$sSql .= ")";
					$bQuoted = FALSE;
				}
				$sSql .= " " . $arCond[$i] . " ";
			}
			$sSql .= $arSQL[$cnt-1];
			if ($bQuoted)
				$sSql .= ")";
		}
		if ($sSql <> "") {
			if ($Where <> "") $Where .= " OR ";
			$Where .=  "(" . $sSql . ")";
		}
	}

	// Return basic search WHERE clause based on search keyword and type
	function BasicSearchWhere($Default = FALSE) {
		global $Security;
		$sSearchStr = "";
		if (!$Security->CanSearch()) return "";
		$sSearchKeyword = ($Default) ? $this->BasicSearch->KeywordDefault : $this->BasicSearch->Keyword;
		$sSearchType = ($Default) ? $this->BasicSearch->TypeDefault : $this->BasicSearch->Type;
		if ($sSearchKeyword <> "") {
			$sSearch = trim($sSearchKeyword);
			if ($sSearchType <> "=") {
				$ar = array();

				// Match quoted keywords (i.e.: "...")
				if (preg_match_all('/"([^"]*)"/i', $sSearch, $matches, PREG_SET_ORDER)) {
					foreach ($matches as $match) {
						$p = strpos($sSearch, $match[0]);
						$str = substr($sSearch, 0, $p);
						$sSearch = substr($sSearch, $p + strlen($match[0]));
						if (strlen(trim($str)) > 0)
							$ar = array_merge($ar, explode(" ", trim($str)));
						$ar[] = $match[1]; // Save quoted keyword
					}
				}

				// Match individual keywords
				if (strlen(trim($sSearch)) > 0)
					$ar = array_merge($ar, explode(" ", trim($sSearch)));

				// Search keyword in any fields
				if (($sSearchType == "OR" || $sSearchType == "AND") && $this->BasicSearch->BasicSearchAnyFields) {
					foreach ($ar as $sKeyword) {
						if ($sKeyword <> "") {
							if ($sSearchStr <> "") $sSearchStr .= " " . $sSearchType . " ";
							$sSearchStr .= "(" . $this->BasicSearchSQL(array($sKeyword), $sSearchType) . ")";
						}
					}
				} else {
					$sSearchStr = $this->BasicSearchSQL($ar, $sSearchType);
				}
			} else {
				$sSearchStr = $this->BasicSearchSQL(array($sSearch), $sSearchType);
			}
			if (!$Default) $this->Command = "search";
		}
		if (!$Default && $this->Command == "search") {
			$this->BasicSearch->setKeyword($sSearchKeyword);
			$this->BasicSearch->setType($sSearchType);
		}
		return $sSearchStr;
	}

	// Check if search parm exists
	function CheckSearchParms() {

		// Check basic search
		if ($this->BasicSearch->IssetSession())
			return TRUE;
		return FALSE;
	}

	// Clear all search parameters
	function ResetSearchParms() {

		// Clear search WHERE clause
		$this->SearchWhere = "";
		$this->setSearchWhere($this->SearchWhere);

		// Clear basic search parameters
		$this->ResetBasicSearchParms();
	}

	// Load advanced search default values
	function LoadAdvancedSearchDefault() {
		return FALSE;
	}

	// Clear all basic search parameters
	function ResetBasicSearchParms() {
		$this->BasicSearch->UnsetSession();
	}

	// Restore all search parameters
	function RestoreSearchParms() {
		$this->RestoreSearch = TRUE;

		// Restore basic search values
		$this->BasicSearch->Load();
	}

	// Set up sort parameters
	function SetUpSortOrder() {

		// Check for Ctrl pressed
		$bCtrl = (@$_GET["ctrl"] <> "");

		// Check for "order" parameter
		if (@$_GET["order"] <> "") {
			$this->CurrentOrder = ew_StripSlashes(@$_GET["order"]);
			$this->CurrentOrderType = @$_GET["ordertype"];
			$this->UpdateSort($this->CcrCodi, $bCtrl); // CcrCodi
			$this->UpdateSort($this->VcrCodi, $bCtrl); // VcrCodi
			$this->UpdateSort($this->CcrNCuo, $bCtrl); // CcrNCuo
			$this->UpdateSort($this->CcrFPag, $bCtrl); // CcrFPag
			$this->UpdateSort($this->CcrMont, $bCtrl); // CcrMont
			$this->UpdateSort($this->CcrMora, $bCtrl); // CcrMora
			$this->UpdateSort($this->CcrDAtr, $bCtrl); // CcrDAtr
			$this->UpdateSort($this->CcrEsta, $bCtrl); // CcrEsta
			$this->UpdateSort($this->CcrAnho, $bCtrl); // CcrAnho
			$this->UpdateSort($this->CcrMes, $bCtrl); // CcrMes
			$this->UpdateSort($this->CcrConc, $bCtrl); // CcrConc
			$this->UpdateSort($this->CcrUsua, $bCtrl); // CcrUsua
			$this->UpdateSort($this->CcrFCre, $bCtrl); // CcrFCre
			$this->setStartRecordNumber(1); // Reset start position
		}
	}

	// Load sort order parameters
	function LoadSortOrder() {
		$sOrderBy = $this->getSessionOrderBy(); // Get ORDER BY from Session
		if ($sOrderBy == "") {
			if ($this->getSqlOrderBy() <> "") {
				$sOrderBy = $this->getSqlOrderBy();
				$this->setSessionOrderBy($sOrderBy);
			}
		}
	}

	// Reset command
	// - cmd=reset (Reset search parameters)
	// - cmd=resetall (Reset search and master/detail parameters)
	// - cmd=resetsort (Reset sort parameters)
	function ResetCmd() {

		// Check if reset command
		if (substr($this->Command,0,5) == "reset") {

			// Reset search criteria
			if ($this->Command == "reset" || $this->Command == "resetall")
				$this->ResetSearchParms();

			// Reset sorting order
			if ($this->Command == "resetsort") {
				$sOrderBy = "";
				$this->setSessionOrderBy($sOrderBy);
				$this->CcrCodi->setSort("");
				$this->VcrCodi->setSort("");
				$this->CcrNCuo->setSort("");
				$this->CcrFPag->setSort("");
				$this->CcrMont->setSort("");
				$this->CcrMora->setSort("");
				$this->CcrDAtr->setSort("");
				$this->CcrEsta->setSort("");
				$this->CcrAnho->setSort("");
				$this->CcrMes->setSort("");
				$this->CcrConc->setSort("");
				$this->CcrUsua->setSort("");
				$this->CcrFCre->setSort("");
			}

			// Reset start position
			$this->StartRec = 1;
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Set up list options
	function SetupListOptions() {
		global $Security, $Language;

		// "griddelete"
		if ($this->AllowAddDeleteRow) {
			$item = &$this->ListOptions->Add("griddelete");
			$item->CssStyle = "white-space: nowrap;";
			$item->OnLeft = FALSE;
			$item->Visible = FALSE; // Default hidden
		}

		// Add group option item
		$item = &$this->ListOptions->Add($this->ListOptions->GroupOptionName);
		$item->Body = "";
		$item->OnLeft = FALSE;
		$item->Visible = FALSE;

		// "view"
		$item = &$this->ListOptions->Add("view");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->CanView();
		$item->OnLeft = FALSE;

		// "edit"
		$item = &$this->ListOptions->Add("edit");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->CanEdit();
		$item->OnLeft = FALSE;

		// "copy"
		$item = &$this->ListOptions->Add("copy");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->CanAdd();
		$item->OnLeft = FALSE;

		// "delete"
		$item = &$this->ListOptions->Add("delete");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->CanDelete();
		$item->OnLeft = FALSE;

		// List actions
		$item = &$this->ListOptions->Add("listactions");
		$item->CssStyle = "white-space: nowrap;";
		$item->OnLeft = FALSE;
		$item->Visible = FALSE;
		$item->ShowInButtonGroup = FALSE;
		$item->ShowInDropDown = FALSE;

		// "checkbox"
		$item = &$this->ListOptions->Add("checkbox");
		$item->Visible = FALSE;
		$item->OnLeft = FALSE;
		$item->Header = "<input type=\"checkbox\" name=\"key\" id=\"key\" onclick=\"ew_SelectAllKey(this);\">";
		$item->ShowInDropDown = FALSE;
		$item->ShowInButtonGroup = FALSE;

		// Drop down button for ListOptions
		$this->ListOptions->UseImageAndText = TRUE;
		$this->ListOptions->UseDropDownButton = FALSE;
		$this->ListOptions->DropDownButtonPhrase = $Language->Phrase("ButtonListOptions");
		$this->ListOptions->UseButtonGroup = FALSE;
		if ($this->ListOptions->UseButtonGroup && ew_IsMobile())
			$this->ListOptions->UseDropDownButton = TRUE;
		$this->ListOptions->ButtonClass = "btn-sm"; // Class for button group

		// Call ListOptions_Load event
		$this->ListOptions_Load();
		$this->SetupListOptionsExt();
		$item = &$this->ListOptions->GetItem($this->ListOptions->GroupOptionName);
		$item->Visible = $this->ListOptions->GroupOptionVisible();
	}

	// Render list options
	function RenderListOptions() {
		global $Security, $Language, $objForm;
		$this->ListOptions->LoadDefault();

		// Set up row action and key
		if (is_numeric($this->RowIndex) && $this->CurrentMode <> "view") {
			$objForm->Index = $this->RowIndex;
			$ActionName = str_replace("k_", "k" . $this->RowIndex . "_", $this->FormActionName);
			$OldKeyName = str_replace("k_", "k" . $this->RowIndex . "_", $this->FormOldKeyName);
			$KeyName = str_replace("k_", "k" . $this->RowIndex . "_", $this->FormKeyName);
			$BlankRowName = str_replace("k_", "k" . $this->RowIndex . "_", $this->FormBlankRowName);
			if ($this->RowAction <> "")
				$this->MultiSelectKey .= "<input type=\"hidden\" name=\"" . $ActionName . "\" id=\"" . $ActionName . "\" value=\"" . $this->RowAction . "\">";
			if ($this->RowAction == "delete") {
				$rowkey = $objForm->GetValue($this->FormKeyName);
				$this->SetupKeyValues($rowkey);
			}
			if ($this->RowAction == "insert" && $this->CurrentAction == "F" && $this->EmptyRow())
				$this->MultiSelectKey .= "<input type=\"hidden\" name=\"" . $BlankRowName . "\" id=\"" . $BlankRowName . "\" value=\"1\">";
		}

		// "delete"
		if ($this->AllowAddDeleteRow) {
			if ($this->CurrentAction == "gridadd" || $this->CurrentAction == "gridedit") {
				$option = &$this->ListOptions;
				$option->UseButtonGroup = TRUE; // Use button group for grid delete button
				$option->UseImageAndText = TRUE; // Use image and text for grid delete button
				$oListOpt = &$option->Items["griddelete"];
				if (!$Security->CanDelete() && is_numeric($this->RowIndex) && ($this->RowAction == "" || $this->RowAction == "edit")) { // Do not allow delete existing record
					$oListOpt->Body = "&nbsp;";
				} else {
					$oListOpt->Body = "<a class=\"ewGridLink ewGridDelete\" title=\"" . ew_HtmlTitle($Language->Phrase("DeleteLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("DeleteLink")) . "\" onclick=\"return ew_DeleteGridRow(this, " . $this->RowIndex . ");\">" . $Language->Phrase("DeleteLink") . "</a>";
				}
			}
		}

		// "view"
		$oListOpt = &$this->ListOptions->Items["view"];
		if ($Security->CanView())
			$oListOpt->Body = "<a class=\"ewRowLink ewView\" title=\"" . ew_HtmlTitle($Language->Phrase("ViewLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("ViewLink")) . "\" href=\"" . ew_HtmlEncode($this->ViewUrl) . "\">" . $Language->Phrase("ViewLink") . "</a>";
		else
			$oListOpt->Body = "";

		// "edit"
		$oListOpt = &$this->ListOptions->Items["edit"];
		if ($Security->CanEdit()) {
			$oListOpt->Body = "<a class=\"ewRowLink ewEdit\" title=\"" . ew_HtmlTitle($Language->Phrase("EditLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("EditLink")) . "\" href=\"" . ew_HtmlEncode($this->EditUrl) . "\">" . $Language->Phrase("EditLink") . "</a>";
		} else {
			$oListOpt->Body = "";
		}

		// "copy"
		$oListOpt = &$this->ListOptions->Items["copy"];
		if ($Security->CanAdd()) {
			$oListOpt->Body = "<a class=\"ewRowLink ewCopy\" title=\"" . ew_HtmlTitle($Language->Phrase("CopyLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("CopyLink")) . "\" href=\"" . ew_HtmlEncode($this->CopyUrl) . "\">" . $Language->Phrase("CopyLink") . "</a>";
		} else {
			$oListOpt->Body = "";
		}

		// "delete"
		$oListOpt = &$this->ListOptions->Items["delete"];
		if ($Security->CanDelete())
			$oListOpt->Body = "<a class=\"ewRowLink ewDelete\"" . "" . " title=\"" . ew_HtmlTitle($Language->Phrase("DeleteLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("DeleteLink")) . "\" href=\"" . ew_HtmlEncode($this->DeleteUrl) . "\">" . $Language->Phrase("DeleteLink") . "</a>";
		else
			$oListOpt->Body = "";

		// Set up list action buttons
		$oListOpt = &$this->ListOptions->GetItem("listactions");
		if ($oListOpt) {
			$body = "";
			$links = array();
			foreach ($this->ListActions->Items as $listaction) {
				if ($listaction->Select == EW_ACTION_SINGLE && $listaction->Allow) {
					$action = $listaction->Action;
					$caption = $listaction->Caption;
					$icon = ($listaction->Icon <> "") ? "<span class=\"" . ew_HtmlEncode(str_replace(" ewIcon", "", $listaction->Icon)) . "\" data-caption=\"" . ew_HtmlTitle($caption) . "\"></span> " : "";
					$links[] = "<li><a class=\"ewAction ewListAction\" data-action=\"" . ew_HtmlEncode($action) . "\" data-caption=\"" . ew_HtmlTitle($caption) . "\" href=\"\" onclick=\"ew_SubmitAction(event,jQuery.extend({key:" . $this->KeyToJson() . "}," . $listaction->ToJson(TRUE) . "));return false;\">" . $icon . $listaction->Caption . "</a></li>";
					if (count($links) == 1) // Single button
						$body = "<a class=\"ewAction ewListAction\" data-action=\"" . ew_HtmlEncode($action) . "\" title=\"" . ew_HtmlTitle($caption) . "\" data-caption=\"" . ew_HtmlTitle($caption) . "\" href=\"\" onclick=\"ew_SubmitAction(event,jQuery.extend({key:" . $this->KeyToJson() . "}," . $listaction->ToJson(TRUE) . "));return false;\">" . $Language->Phrase("ListActionButton") . "</a>";
				}
			}
			if (count($links) > 1) { // More than one buttons, use dropdown
				$body = "<button class=\"dropdown-toggle btn btn-default btn-sm ewActions\" title=\"" . ew_HtmlTitle($Language->Phrase("ListActionButton")) . "\" data-toggle=\"dropdown\">" . $Language->Phrase("ListActionButton") . "<b class=\"caret\"></b></button>";
				$content = "";
				foreach ($links as $link)
					$content .= "<li>" . $link . "</li>";
				$body .= "<ul class=\"dropdown-menu" . ($oListOpt->OnLeft ? "" : " dropdown-menu-right") . "\">". $content . "</ul>";
				$body = "<div class=\"btn-group\">" . $body . "</div>";
			}
			if (count($links) > 0) {
				$oListOpt->Body = $body;
				$oListOpt->Visible = TRUE;
			}
		}

		// "checkbox"
		$oListOpt = &$this->ListOptions->Items["checkbox"];
		$oListOpt->Body = "<input type=\"checkbox\" name=\"key_m[]\" value=\"" . ew_HtmlEncode($this->CcrCodi->CurrentValue) . "\" onclick='ew_ClickMultiCheckbox(event);'>";
		if ($this->CurrentAction == "gridedit" && is_numeric($this->RowIndex)) {
			$this->MultiSelectKey .= "<input type=\"hidden\" name=\"" . $KeyName . "\" id=\"" . $KeyName . "\" value=\"" . $this->CcrCodi->CurrentValue . "\">";
		}
		$this->RenderListOptionsExt();

		// Call ListOptions_Rendered event
		$this->ListOptions_Rendered();
	}

	// Set up other options
	function SetupOtherOptions() {
		global $Language, $Security;
		$options = &$this->OtherOptions;
		$option = $options["addedit"];

		// Add
		$item = &$option->Add("add");
		$item->Body = "<a class=\"ewAddEdit ewAdd\" title=\"" . ew_HtmlTitle($Language->Phrase("AddLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("AddLink")) . "\" href=\"" . ew_HtmlEncode($this->AddUrl) . "\">" . $Language->Phrase("AddLink") . "</a>";
		$item->Visible = ($this->AddUrl <> "" && $Security->CanAdd());
		$item = &$option->Add("gridadd");
		$item->Body = "<a class=\"ewAddEdit ewGridAdd\" title=\"" . ew_HtmlTitle($Language->Phrase("GridAddLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("GridAddLink")) . "\" href=\"" . ew_HtmlEncode($this->GridAddUrl) . "\">" . $Language->Phrase("GridAddLink") . "</a>";
		$item->Visible = ($this->GridAddUrl <> "" && $Security->CanAdd());

		// Add grid edit
		$option = $options["addedit"];
		$item = &$option->Add("gridedit");
		$item->Body = "<a class=\"ewAddEdit ewGridEdit\" title=\"" . ew_HtmlTitle($Language->Phrase("GridEditLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("GridEditLink")) . "\" href=\"" . ew_HtmlEncode($this->GridEditUrl) . "\">" . $Language->Phrase("GridEditLink") . "</a>";
		$item->Visible = ($this->GridEditUrl <> "" && $Security->CanEdit());
		$option = $options["action"];

		// Set up options default
		foreach ($options as &$option) {
			$option->UseImageAndText = TRUE;
			$option->UseDropDownButton = FALSE;
			$option->UseButtonGroup = TRUE;
			$option->ButtonClass = "btn-sm"; // Class for button group
			$item = &$option->Add($option->GroupOptionName);
			$item->Body = "";
			$item->Visible = FALSE;
		}
		$options["addedit"]->DropDownButtonPhrase = $Language->Phrase("ButtonAddEdit");
		$options["detail"]->DropDownButtonPhrase = $Language->Phrase("ButtonDetails");
		$options["action"]->DropDownButtonPhrase = $Language->Phrase("ButtonActions");

		// Filter button
		$item = &$this->FilterOptions->Add("savecurrentfilter");
		$item->Body = "<a class=\"ewSaveFilter\" data-form=\"fCCrelistsrch\" href=\"#\">" . $Language->Phrase("SaveCurrentFilter") . "</a>";
		$item->Visible = TRUE;
		$item = &$this->FilterOptions->Add("deletefilter");
		$item->Body = "<a class=\"ewDeleteFilter\" data-form=\"fCCrelistsrch\" href=\"#\">" . $Language->Phrase("DeleteFilter") . "</a>";
		$item->Visible = TRUE;
		$this->FilterOptions->UseDropDownButton = TRUE;
		$this->FilterOptions->UseButtonGroup = !$this->FilterOptions->UseDropDownButton;
		$this->FilterOptions->DropDownButtonPhrase = $Language->Phrase("Filters");

		// Add group option item
		$item = &$this->FilterOptions->Add($this->FilterOptions->GroupOptionName);
		$item->Body = "";
		$item->Visible = FALSE;
	}

	// Render other options
	function RenderOtherOptions() {
		global $Language, $Security;
		$options = &$this->OtherOptions;
		if ($this->CurrentAction <> "gridadd" && $this->CurrentAction <> "gridedit") { // Not grid add/edit mode
			$option = &$options["action"];

			// Set up list action buttons
			foreach ($this->ListActions->Items as $listaction) {
				if ($listaction->Select == EW_ACTION_MULTIPLE) {
					$item = &$option->Add("custom_" . $listaction->Action);
					$caption = $listaction->Caption;
					$icon = ($listaction->Icon <> "") ? "<span class=\"" . ew_HtmlEncode($listaction->Icon) . "\" data-caption=\"" . ew_HtmlEncode($caption) . "\"></span> " : $caption;
					$item->Body = "<a class=\"ewAction ewListAction\" title=\"" . ew_HtmlEncode($caption) . "\" data-caption=\"" . ew_HtmlEncode($caption) . "\" href=\"\" onclick=\"ew_SubmitAction(event,jQuery.extend({f:document.fCCrelist}," . $listaction->ToJson(TRUE) . "));return false;\">" . $icon . "</a>";
					$item->Visible = $listaction->Allow;
				}
			}

			// Hide grid edit and other options
			if ($this->TotalRecs <= 0) {
				$option = &$options["addedit"];
				$item = &$option->GetItem("gridedit");
				if ($item) $item->Visible = FALSE;
				$option = &$options["action"];
				$option->HideAllOptions();
			}
		} else { // Grid add/edit mode

			// Hide all options first
			foreach ($options as &$option)
				$option->HideAllOptions();
			if ($this->CurrentAction == "gridadd") {
				if ($this->AllowAddDeleteRow) {

					// Add add blank row
					$option = &$options["addedit"];
					$option->UseDropDownButton = FALSE;
					$option->UseImageAndText = TRUE;
					$item = &$option->Add("addblankrow");
					$item->Body = "<a class=\"ewAddEdit ewAddBlankRow\" title=\"" . ew_HtmlTitle($Language->Phrase("AddBlankRow")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("AddBlankRow")) . "\" href=\"javascript:void(0);\" onclick=\"ew_AddGridRow(this);\">" . $Language->Phrase("AddBlankRow") . "</a>";
					$item->Visible = $Security->CanAdd();
				}
				$option = &$options["action"];
				$option->UseDropDownButton = FALSE;
				$option->UseImageAndText = TRUE;

				// Add grid insert
				$item = &$option->Add("gridinsert");
				$item->Body = "<a class=\"ewAction ewGridInsert\" title=\"" . ew_HtmlTitle($Language->Phrase("GridInsertLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("GridInsertLink")) . "\" href=\"\" onclick=\"return ewForms(this).Submit('" . $this->AddMasterUrl($this->PageName()) . "');\">" . $Language->Phrase("GridInsertLink") . "</a>";

				// Add grid cancel
				$item = &$option->Add("gridcancel");
				$cancelurl = $this->AddMasterUrl($this->PageUrl() . "a=cancel");
				$item->Body = "<a class=\"ewAction ewGridCancel\" title=\"" . ew_HtmlTitle($Language->Phrase("GridCancelLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("GridCancelLink")) . "\" href=\"" . $cancelurl . "\">" . $Language->Phrase("GridCancelLink") . "</a>";
			}
			if ($this->CurrentAction == "gridedit") {
				if ($this->AllowAddDeleteRow) {

					// Add add blank row
					$option = &$options["addedit"];
					$option->UseDropDownButton = FALSE;
					$option->UseImageAndText = TRUE;
					$item = &$option->Add("addblankrow");
					$item->Body = "<a class=\"ewAddEdit ewAddBlankRow\" title=\"" . ew_HtmlTitle($Language->Phrase("AddBlankRow")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("AddBlankRow")) . "\" href=\"javascript:void(0);\" onclick=\"ew_AddGridRow(this);\">" . $Language->Phrase("AddBlankRow") . "</a>";
					$item->Visible = $Security->CanAdd();
				}
				$option = &$options["action"];
				$option->UseDropDownButton = FALSE;
				$option->UseImageAndText = TRUE;
					$item = &$option->Add("gridsave");
					$item->Body = "<a class=\"ewAction ewGridSave\" title=\"" . ew_HtmlTitle($Language->Phrase("GridSaveLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("GridSaveLink")) . "\" href=\"\" onclick=\"return ewForms(this).Submit('" . $this->AddMasterUrl($this->PageName()) . "');\">" . $Language->Phrase("GridSaveLink") . "</a>";
					$item = &$option->Add("gridcancel");
					$cancelurl = $this->AddMasterUrl($this->PageUrl() . "a=cancel");
					$item->Body = "<a class=\"ewAction ewGridCancel\" title=\"" . ew_HtmlTitle($Language->Phrase("GridCancelLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("GridCancelLink")) . "\" href=\"" . $cancelurl . "\">" . $Language->Phrase("GridCancelLink") . "</a>";
			}
		}
	}

	// Process list action
	function ProcessListAction() {
		global $Language, $Security;
		$userlist = "";
		$user = "";
		$sFilter = $this->GetKeyFilter();
		$UserAction = @$_POST["useraction"];
		if ($sFilter <> "" && $UserAction <> "") {

			// Check permission first
			$ActionCaption = $UserAction;
			if (array_key_exists($UserAction, $this->ListActions->Items)) {
				$ActionCaption = $this->ListActions->Items[$UserAction]->Caption;
				if (!$this->ListActions->Items[$UserAction]->Allow) {
					$errmsg = str_replace('%s', $ActionCaption, $Language->Phrase("CustomActionNotAllowed"));
					if (@$_POST["ajax"] == $UserAction) // Ajax
						echo "<p class=\"text-danger\">" . $errmsg . "</p>";
					else
						$this->setFailureMessage($errmsg);
					return FALSE;
				}
			}
			$this->CurrentFilter = $sFilter;
			$sSql = $this->SQL();
			$conn = &$this->Connection();
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			$rs = $conn->Execute($sSql);
			$conn->raiseErrorFn = '';
			$this->CurrentAction = $UserAction;

			// Call row action event
			if ($rs && !$rs->EOF) {
				$conn->BeginTrans();
				$this->SelectedCount = $rs->RecordCount();
				$this->SelectedIndex = 0;
				while (!$rs->EOF) {
					$this->SelectedIndex++;
					$row = $rs->fields;
					$Processed = $this->Row_CustomAction($UserAction, $row);
					if (!$Processed) break;
					$rs->MoveNext();
				}
				if ($Processed) {
					$conn->CommitTrans(); // Commit the changes
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage(str_replace('%s', $ActionCaption, $Language->Phrase("CustomActionCompleted"))); // Set up success message
				} else {
					$conn->RollbackTrans(); // Rollback changes

					// Set up error message
					if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

						// Use the message, do nothing
					} elseif ($this->CancelMessage <> "") {
						$this->setFailureMessage($this->CancelMessage);
						$this->CancelMessage = "";
					} else {
						$this->setFailureMessage(str_replace('%s', $ActionCaption, $Language->Phrase("CustomActionFailed")));
					}
				}
			}
			if ($rs)
				$rs->Close();
			if (@$_POST["ajax"] == $UserAction) { // Ajax
				if ($this->getSuccessMessage() <> "") {
					echo "<p class=\"text-success\">" . $this->getSuccessMessage() . "</p>";
					$this->ClearSuccessMessage(); // Clear message
				}
				if ($this->getFailureMessage() <> "") {
					echo "<p class=\"text-danger\">" . $this->getFailureMessage() . "</p>";
					$this->ClearFailureMessage(); // Clear message
				}
				return TRUE;
			}
		}
		return FALSE; // Not ajax request
	}

	// Set up search options
	function SetupSearchOptions() {
		global $Language;
		$this->SearchOptions = new cListOptions();
		$this->SearchOptions->Tag = "div";
		$this->SearchOptions->TagClassName = "ewSearchOption";

		// Search button
		$item = &$this->SearchOptions->Add("searchtoggle");
		$SearchToggleClass = ($this->SearchWhere <> "") ? " active" : " active";
		$item->Body = "<button type=\"button\" class=\"btn btn-default ewSearchToggle" . $SearchToggleClass . "\" title=\"" . $Language->Phrase("SearchPanel") . "\" data-caption=\"" . $Language->Phrase("SearchPanel") . "\" data-toggle=\"button\" data-form=\"fCCrelistsrch\">" . $Language->Phrase("SearchBtn") . "</button>";
		$item->Visible = TRUE;

		// Show all button
		$item = &$this->SearchOptions->Add("showall");
		$item->Body = "<a class=\"btn btn-default ewShowAll\" title=\"" . $Language->Phrase("ShowAll") . "\" data-caption=\"" . $Language->Phrase("ShowAll") . "\" href=\"" . $this->PageUrl() . "cmd=reset\">" . $Language->Phrase("ShowAllBtn") . "</a>";
		$item->Visible = ($this->SearchWhere <> $this->DefaultSearchWhere && $this->SearchWhere <> "0=101");

		// Button group for search
		$this->SearchOptions->UseDropDownButton = FALSE;
		$this->SearchOptions->UseImageAndText = TRUE;
		$this->SearchOptions->UseButtonGroup = TRUE;
		$this->SearchOptions->DropDownButtonPhrase = $Language->Phrase("ButtonSearch");

		// Add group option item
		$item = &$this->SearchOptions->Add($this->SearchOptions->GroupOptionName);
		$item->Body = "";
		$item->Visible = FALSE;

		// Hide search options
		if ($this->Export <> "" || $this->CurrentAction <> "")
			$this->SearchOptions->HideAllOptions();
		global $Security;
		if (!$Security->CanSearch()) {
			$this->SearchOptions->HideAllOptions();
			$this->FilterOptions->HideAllOptions();
		}
	}

	function SetupListOptionsExt() {
		global $Security, $Language;
	}

	function RenderListOptionsExt() {
		global $Security, $Language;
	}

	// Set up starting record parameters
	function SetUpStartRec() {
		if ($this->DisplayRecs == 0)
			return;
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET[EW_TABLE_START_REC] <> "") { // Check for "start" parameter
				$this->StartRec = $_GET[EW_TABLE_START_REC];
				$this->setStartRecordNumber($this->StartRec);
			} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
				$PageNo = $_GET[EW_TABLE_PAGE_NO];
				if (is_numeric($PageNo)) {
					$this->StartRec = ($PageNo-1)*$this->DisplayRecs+1;
					if ($this->StartRec <= 0) {
						$this->StartRec = 1;
					} elseif ($this->StartRec >= intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1) {
						$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1;
					}
					$this->setStartRecordNumber($this->StartRec);
				}
			}
		}
		$this->StartRec = $this->getStartRecordNumber();

		// Check if correct start record counter
		if (!is_numeric($this->StartRec) || $this->StartRec == "") { // Avoid invalid start record counter
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} elseif (intval($this->StartRec) > intval($this->TotalRecs)) { // Avoid starting record > total records
			$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to last page first record
			$this->setStartRecordNumber($this->StartRec);
		} elseif (($this->StartRec-1) % $this->DisplayRecs <> 0) {
			$this->StartRec = intval(($this->StartRec-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to page boundary
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Load default values
	function LoadDefaultValues() {
		$this->CcrCodi->CurrentValue = NULL;
		$this->CcrCodi->OldValue = $this->CcrCodi->CurrentValue;
		$this->VcrCodi->CurrentValue = NULL;
		$this->VcrCodi->OldValue = $this->VcrCodi->CurrentValue;
		$this->CcrNCuo->CurrentValue = NULL;
		$this->CcrNCuo->OldValue = $this->CcrNCuo->CurrentValue;
		$this->CcrFPag->CurrentValue = NULL;
		$this->CcrFPag->OldValue = $this->CcrFPag->CurrentValue;
		$this->CcrMont->CurrentValue = NULL;
		$this->CcrMont->OldValue = $this->CcrMont->CurrentValue;
		$this->CcrMora->CurrentValue = NULL;
		$this->CcrMora->OldValue = $this->CcrMora->CurrentValue;
		$this->CcrDAtr->CurrentValue = NULL;
		$this->CcrDAtr->OldValue = $this->CcrDAtr->CurrentValue;
		$this->CcrEsta->CurrentValue = NULL;
		$this->CcrEsta->OldValue = $this->CcrEsta->CurrentValue;
		$this->CcrAnho->CurrentValue = NULL;
		$this->CcrAnho->OldValue = $this->CcrAnho->CurrentValue;
		$this->CcrMes->CurrentValue = NULL;
		$this->CcrMes->OldValue = $this->CcrMes->CurrentValue;
		$this->CcrConc->CurrentValue = NULL;
		$this->CcrConc->OldValue = $this->CcrConc->CurrentValue;
		$this->CcrUsua->CurrentValue = NULL;
		$this->CcrUsua->OldValue = $this->CcrUsua->CurrentValue;
		$this->CcrFCre->CurrentValue = NULL;
		$this->CcrFCre->OldValue = $this->CcrFCre->CurrentValue;
	}

	// Load basic search values
	function LoadBasicSearchValues() {
		$this->BasicSearch->Keyword = @$_GET[EW_TABLE_BASIC_SEARCH];
		if ($this->BasicSearch->Keyword <> "") $this->Command = "search";
		$this->BasicSearch->Type = @$_GET[EW_TABLE_BASIC_SEARCH_TYPE];
	}

	// Load form values
	function LoadFormValues() {

		// Load from form
		global $objForm;
		if (!$this->CcrCodi->FldIsDetailKey && $this->CurrentAction <> "gridadd" && $this->CurrentAction <> "add")
			$this->CcrCodi->setFormValue($objForm->GetValue("x_CcrCodi"));
		if (!$this->VcrCodi->FldIsDetailKey) {
			$this->VcrCodi->setFormValue($objForm->GetValue("x_VcrCodi"));
		}
		$this->VcrCodi->setOldValue($objForm->GetValue("o_VcrCodi"));
		if (!$this->CcrNCuo->FldIsDetailKey) {
			$this->CcrNCuo->setFormValue($objForm->GetValue("x_CcrNCuo"));
		}
		$this->CcrNCuo->setOldValue($objForm->GetValue("o_CcrNCuo"));
		if (!$this->CcrFPag->FldIsDetailKey) {
			$this->CcrFPag->setFormValue($objForm->GetValue("x_CcrFPag"));
			$this->CcrFPag->CurrentValue = ew_UnFormatDateTime($this->CcrFPag->CurrentValue, 7);
		}
		$this->CcrFPag->setOldValue($objForm->GetValue("o_CcrFPag"));
		if (!$this->CcrMont->FldIsDetailKey) {
			$this->CcrMont->setFormValue($objForm->GetValue("x_CcrMont"));
		}
		$this->CcrMont->setOldValue($objForm->GetValue("o_CcrMont"));
		if (!$this->CcrMora->FldIsDetailKey) {
			$this->CcrMora->setFormValue($objForm->GetValue("x_CcrMora"));
		}
		$this->CcrMora->setOldValue($objForm->GetValue("o_CcrMora"));
		if (!$this->CcrDAtr->FldIsDetailKey) {
			$this->CcrDAtr->setFormValue($objForm->GetValue("x_CcrDAtr"));
		}
		$this->CcrDAtr->setOldValue($objForm->GetValue("o_CcrDAtr"));
		if (!$this->CcrEsta->FldIsDetailKey) {
			$this->CcrEsta->setFormValue($objForm->GetValue("x_CcrEsta"));
		}
		$this->CcrEsta->setOldValue($objForm->GetValue("o_CcrEsta"));
		if (!$this->CcrAnho->FldIsDetailKey) {
			$this->CcrAnho->setFormValue($objForm->GetValue("x_CcrAnho"));
		}
		$this->CcrAnho->setOldValue($objForm->GetValue("o_CcrAnho"));
		if (!$this->CcrMes->FldIsDetailKey) {
			$this->CcrMes->setFormValue($objForm->GetValue("x_CcrMes"));
		}
		$this->CcrMes->setOldValue($objForm->GetValue("o_CcrMes"));
		if (!$this->CcrConc->FldIsDetailKey) {
			$this->CcrConc->setFormValue($objForm->GetValue("x_CcrConc"));
		}
		$this->CcrConc->setOldValue($objForm->GetValue("o_CcrConc"));
		if (!$this->CcrUsua->FldIsDetailKey) {
			$this->CcrUsua->setFormValue($objForm->GetValue("x_CcrUsua"));
		}
		$this->CcrUsua->setOldValue($objForm->GetValue("o_CcrUsua"));
		if (!$this->CcrFCre->FldIsDetailKey) {
			$this->CcrFCre->setFormValue($objForm->GetValue("x_CcrFCre"));
			$this->CcrFCre->CurrentValue = ew_UnFormatDateTime($this->CcrFCre->CurrentValue, 7);
		}
		$this->CcrFCre->setOldValue($objForm->GetValue("o_CcrFCre"));
	}

	// Restore form values
	function RestoreFormValues() {
		global $objForm;
		if ($this->CurrentAction <> "gridadd" && $this->CurrentAction <> "add")
			$this->CcrCodi->CurrentValue = $this->CcrCodi->FormValue;
		$this->VcrCodi->CurrentValue = $this->VcrCodi->FormValue;
		$this->CcrNCuo->CurrentValue = $this->CcrNCuo->FormValue;
		$this->CcrFPag->CurrentValue = $this->CcrFPag->FormValue;
		$this->CcrFPag->CurrentValue = ew_UnFormatDateTime($this->CcrFPag->CurrentValue, 7);
		$this->CcrMont->CurrentValue = $this->CcrMont->FormValue;
		$this->CcrMora->CurrentValue = $this->CcrMora->FormValue;
		$this->CcrDAtr->CurrentValue = $this->CcrDAtr->FormValue;
		$this->CcrEsta->CurrentValue = $this->CcrEsta->FormValue;
		$this->CcrAnho->CurrentValue = $this->CcrAnho->FormValue;
		$this->CcrMes->CurrentValue = $this->CcrMes->FormValue;
		$this->CcrConc->CurrentValue = $this->CcrConc->FormValue;
		$this->CcrUsua->CurrentValue = $this->CcrUsua->FormValue;
		$this->CcrFCre->CurrentValue = $this->CcrFCre->FormValue;
		$this->CcrFCre->CurrentValue = ew_UnFormatDateTime($this->CcrFCre->CurrentValue, 7);
	}

	// Load recordset
	function LoadRecordset($offset = -1, $rowcnt = -1) {

		// Load List page SQL
		$sSql = $this->SelectSQL();
		$conn = &$this->Connection();

		// Load recordset
		$dbtype = ew_GetConnectionType($this->DBID);
		if ($this->UseSelectLimit) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			if ($dbtype == "MSSQL") {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset, array("_hasOrderBy" => trim($this->getOrderBy()) || trim($this->getSessionOrderBy())));
			} else {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset);
			}
			$conn->raiseErrorFn = '';
		} else {
			$rs = ew_LoadRecordset($sSql, $conn);
		}

		// Call Recordset Selected event
		$this->Recordset_Selected($rs);
		return $rs;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->CcrCodi->setDbValue($rs->fields('CcrCodi'));
		$this->VcrCodi->setDbValue($rs->fields('VcrCodi'));
		$this->CcrNCuo->setDbValue($rs->fields('CcrNCuo'));
		$this->CcrFPag->setDbValue($rs->fields('CcrFPag'));
		$this->CcrMont->setDbValue($rs->fields('CcrMont'));
		$this->CcrMora->setDbValue($rs->fields('CcrMora'));
		$this->CcrDAtr->setDbValue($rs->fields('CcrDAtr'));
		$this->CcrEsta->setDbValue($rs->fields('CcrEsta'));
		$this->CcrAnho->setDbValue($rs->fields('CcrAnho'));
		$this->CcrMes->setDbValue($rs->fields('CcrMes'));
		$this->CcrConc->setDbValue($rs->fields('CcrConc'));
		$this->CcrUsua->setDbValue($rs->fields('CcrUsua'));
		$this->CcrFCre->setDbValue($rs->fields('CcrFCre'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->CcrCodi->DbValue = $row['CcrCodi'];
		$this->VcrCodi->DbValue = $row['VcrCodi'];
		$this->CcrNCuo->DbValue = $row['CcrNCuo'];
		$this->CcrFPag->DbValue = $row['CcrFPag'];
		$this->CcrMont->DbValue = $row['CcrMont'];
		$this->CcrMora->DbValue = $row['CcrMora'];
		$this->CcrDAtr->DbValue = $row['CcrDAtr'];
		$this->CcrEsta->DbValue = $row['CcrEsta'];
		$this->CcrAnho->DbValue = $row['CcrAnho'];
		$this->CcrMes->DbValue = $row['CcrMes'];
		$this->CcrConc->DbValue = $row['CcrConc'];
		$this->CcrUsua->DbValue = $row['CcrUsua'];
		$this->CcrFCre->DbValue = $row['CcrFCre'];
	}

	// Load old record
	function LoadOldRecord() {

		// Load key values from Session
		$bValidKey = TRUE;
		if (strval($this->getKey("CcrCodi")) <> "")
			$this->CcrCodi->CurrentValue = $this->getKey("CcrCodi"); // CcrCodi
		else
			$bValidKey = FALSE;

		// Load old recordset
		if ($bValidKey) {
			$this->CurrentFilter = $this->KeyFilter();
			$sSql = $this->SQL();
			$conn = &$this->Connection();
			$this->OldRecordset = ew_LoadRecordset($sSql, $conn);
			$this->LoadRowValues($this->OldRecordset); // Load row values
		} else {
			$this->OldRecordset = NULL;
		}
		return $bValidKey;
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		$this->ViewUrl = $this->GetViewUrl();
		$this->EditUrl = $this->GetEditUrl();
		$this->InlineEditUrl = $this->GetInlineEditUrl();
		$this->CopyUrl = $this->GetCopyUrl();
		$this->InlineCopyUrl = $this->GetInlineCopyUrl();
		$this->DeleteUrl = $this->GetDeleteUrl();

		// Convert decimal values if posted back
		if ($this->CcrMont->FormValue == $this->CcrMont->CurrentValue && is_numeric(ew_StrToFloat($this->CcrMont->CurrentValue)))
			$this->CcrMont->CurrentValue = ew_StrToFloat($this->CcrMont->CurrentValue);

		// Convert decimal values if posted back
		if ($this->CcrMora->FormValue == $this->CcrMora->CurrentValue && is_numeric(ew_StrToFloat($this->CcrMora->CurrentValue)))
			$this->CcrMora->CurrentValue = ew_StrToFloat($this->CcrMora->CurrentValue);

		// Call Row_Rendering event
		$this->Row_Rendering();

		// Common render codes for all row types
		// CcrCodi
		// VcrCodi
		// CcrNCuo
		// CcrFPag
		// CcrMont
		// CcrMora
		// CcrDAtr
		// CcrEsta
		// CcrAnho
		// CcrMes
		// CcrConc
		// CcrUsua
		// CcrFCre

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// CcrCodi
		$this->CcrCodi->ViewValue = $this->CcrCodi->CurrentValue;
		$this->CcrCodi->ViewCustomAttributes = "";

		// VcrCodi
		if (strval($this->VcrCodi->CurrentValue) <> "") {
			$sFilterWrk = "\"VcrCodi\"" . ew_SearchString("=", $this->VcrCodi->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT \"VcrCodi\", \"VcrCodi\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"VCre\"";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->VcrCodi, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->VcrCodi->ViewValue = $this->VcrCodi->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->VcrCodi->ViewValue = $this->VcrCodi->CurrentValue;
			}
		} else {
			$this->VcrCodi->ViewValue = NULL;
		}
		$this->VcrCodi->ViewCustomAttributes = "";

		// CcrNCuo
		$this->CcrNCuo->ViewValue = $this->CcrNCuo->CurrentValue;
		$this->CcrNCuo->ViewCustomAttributes = "";

		// CcrFPag
		$this->CcrFPag->ViewValue = $this->CcrFPag->CurrentValue;
		$this->CcrFPag->ViewValue = ew_FormatDateTime($this->CcrFPag->ViewValue, 7);
		$this->CcrFPag->ViewCustomAttributes = "";

		// CcrMont
		$this->CcrMont->ViewValue = $this->CcrMont->CurrentValue;
		$this->CcrMont->ViewCustomAttributes = "";

		// CcrMora
		$this->CcrMora->ViewValue = $this->CcrMora->CurrentValue;
		$this->CcrMora->ViewCustomAttributes = "";

		// CcrDAtr
		$this->CcrDAtr->ViewValue = $this->CcrDAtr->CurrentValue;
		$this->CcrDAtr->ViewCustomAttributes = "";

		// CcrEsta
		$this->CcrEsta->ViewValue = $this->CcrEsta->CurrentValue;
		$this->CcrEsta->ViewCustomAttributes = "";

		// CcrAnho
		$this->CcrAnho->ViewValue = $this->CcrAnho->CurrentValue;
		$this->CcrAnho->ViewCustomAttributes = "";

		// CcrMes
		$this->CcrMes->ViewValue = $this->CcrMes->CurrentValue;
		$this->CcrMes->ViewCustomAttributes = "";

		// CcrConc
		$this->CcrConc->ViewValue = $this->CcrConc->CurrentValue;
		$this->CcrConc->ViewCustomAttributes = "";

		// CcrUsua
		$this->CcrUsua->ViewValue = $this->CcrUsua->CurrentValue;
		$this->CcrUsua->ViewCustomAttributes = "";

		// CcrFCre
		$this->CcrFCre->ViewValue = $this->CcrFCre->CurrentValue;
		$this->CcrFCre->ViewValue = ew_FormatDateTime($this->CcrFCre->ViewValue, 7);
		$this->CcrFCre->ViewCustomAttributes = "";

			// CcrCodi
			$this->CcrCodi->LinkCustomAttributes = "";
			$this->CcrCodi->HrefValue = "";
			$this->CcrCodi->TooltipValue = "";

			// VcrCodi
			$this->VcrCodi->LinkCustomAttributes = "";
			$this->VcrCodi->HrefValue = "";
			$this->VcrCodi->TooltipValue = "";

			// CcrNCuo
			$this->CcrNCuo->LinkCustomAttributes = "";
			$this->CcrNCuo->HrefValue = "";
			$this->CcrNCuo->TooltipValue = "";

			// CcrFPag
			$this->CcrFPag->LinkCustomAttributes = "";
			$this->CcrFPag->HrefValue = "";
			$this->CcrFPag->TooltipValue = "";

			// CcrMont
			$this->CcrMont->LinkCustomAttributes = "";
			$this->CcrMont->HrefValue = "";
			$this->CcrMont->TooltipValue = "";

			// CcrMora
			$this->CcrMora->LinkCustomAttributes = "";
			$this->CcrMora->HrefValue = "";
			$this->CcrMora->TooltipValue = "";

			// CcrDAtr
			$this->CcrDAtr->LinkCustomAttributes = "";
			$this->CcrDAtr->HrefValue = "";
			$this->CcrDAtr->TooltipValue = "";

			// CcrEsta
			$this->CcrEsta->LinkCustomAttributes = "";
			$this->CcrEsta->HrefValue = "";
			$this->CcrEsta->TooltipValue = "";

			// CcrAnho
			$this->CcrAnho->LinkCustomAttributes = "";
			$this->CcrAnho->HrefValue = "";
			$this->CcrAnho->TooltipValue = "";

			// CcrMes
			$this->CcrMes->LinkCustomAttributes = "";
			$this->CcrMes->HrefValue = "";
			$this->CcrMes->TooltipValue = "";

			// CcrConc
			$this->CcrConc->LinkCustomAttributes = "";
			$this->CcrConc->HrefValue = "";
			$this->CcrConc->TooltipValue = "";

			// CcrUsua
			$this->CcrUsua->LinkCustomAttributes = "";
			$this->CcrUsua->HrefValue = "";
			$this->CcrUsua->TooltipValue = "";

			// CcrFCre
			$this->CcrFCre->LinkCustomAttributes = "";
			$this->CcrFCre->HrefValue = "";
			$this->CcrFCre->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_ADD) { // Add row

			// CcrCodi
			// VcrCodi

			$this->VcrCodi->EditAttrs["class"] = "form-control";
			$this->VcrCodi->EditCustomAttributes = "";
			if (trim(strval($this->VcrCodi->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "\"VcrCodi\"" . ew_SearchString("=", $this->VcrCodi->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT \"VcrCodi\", \"VcrCodi\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\", '' AS \"SelectFilterFld\", '' AS \"SelectFilterFld2\", '' AS \"SelectFilterFld3\", '' AS \"SelectFilterFld4\" FROM \"public\".\"VCre\"";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->VcrCodi, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->VcrCodi->EditValue = $arwrk;

			// CcrNCuo
			$this->CcrNCuo->EditAttrs["class"] = "form-control";
			$this->CcrNCuo->EditCustomAttributes = "";
			$this->CcrNCuo->EditValue = ew_HtmlEncode($this->CcrNCuo->CurrentValue);
			$this->CcrNCuo->PlaceHolder = ew_RemoveHtml($this->CcrNCuo->FldCaption());

			// CcrFPag
			$this->CcrFPag->EditAttrs["class"] = "form-control";
			$this->CcrFPag->EditCustomAttributes = "";
			$this->CcrFPag->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->CcrFPag->CurrentValue, 7));
			$this->CcrFPag->PlaceHolder = ew_RemoveHtml($this->CcrFPag->FldCaption());

			// CcrMont
			$this->CcrMont->EditAttrs["class"] = "form-control";
			$this->CcrMont->EditCustomAttributes = "";
			$this->CcrMont->EditValue = ew_HtmlEncode($this->CcrMont->CurrentValue);
			$this->CcrMont->PlaceHolder = ew_RemoveHtml($this->CcrMont->FldCaption());
			if (strval($this->CcrMont->EditValue) <> "" && is_numeric($this->CcrMont->EditValue)) {
			$this->CcrMont->EditValue = ew_FormatNumber($this->CcrMont->EditValue, -2, -1, -2, 0);
			$this->CcrMont->OldValue = $this->CcrMont->EditValue;
			}

			// CcrMora
			$this->CcrMora->EditAttrs["class"] = "form-control";
			$this->CcrMora->EditCustomAttributes = "";
			$this->CcrMora->EditValue = ew_HtmlEncode($this->CcrMora->CurrentValue);
			$this->CcrMora->PlaceHolder = ew_RemoveHtml($this->CcrMora->FldCaption());
			if (strval($this->CcrMora->EditValue) <> "" && is_numeric($this->CcrMora->EditValue)) {
			$this->CcrMora->EditValue = ew_FormatNumber($this->CcrMora->EditValue, -2, -1, -2, 0);
			$this->CcrMora->OldValue = $this->CcrMora->EditValue;
			}

			// CcrDAtr
			$this->CcrDAtr->EditAttrs["class"] = "form-control";
			$this->CcrDAtr->EditCustomAttributes = "";
			$this->CcrDAtr->EditValue = ew_HtmlEncode($this->CcrDAtr->CurrentValue);
			$this->CcrDAtr->PlaceHolder = ew_RemoveHtml($this->CcrDAtr->FldCaption());

			// CcrEsta
			$this->CcrEsta->EditAttrs["class"] = "form-control";
			$this->CcrEsta->EditCustomAttributes = "";
			$this->CcrEsta->EditValue = ew_HtmlEncode($this->CcrEsta->CurrentValue);
			$this->CcrEsta->PlaceHolder = ew_RemoveHtml($this->CcrEsta->FldCaption());

			// CcrAnho
			$this->CcrAnho->EditAttrs["class"] = "form-control";
			$this->CcrAnho->EditCustomAttributes = "";
			$this->CcrAnho->EditValue = ew_HtmlEncode($this->CcrAnho->CurrentValue);
			$this->CcrAnho->PlaceHolder = ew_RemoveHtml($this->CcrAnho->FldCaption());

			// CcrMes
			$this->CcrMes->EditAttrs["class"] = "form-control";
			$this->CcrMes->EditCustomAttributes = "";
			$this->CcrMes->EditValue = ew_HtmlEncode($this->CcrMes->CurrentValue);
			$this->CcrMes->PlaceHolder = ew_RemoveHtml($this->CcrMes->FldCaption());

			// CcrConc
			$this->CcrConc->EditAttrs["class"] = "form-control";
			$this->CcrConc->EditCustomAttributes = "";
			$this->CcrConc->EditValue = ew_HtmlEncode($this->CcrConc->CurrentValue);
			$this->CcrConc->PlaceHolder = ew_RemoveHtml($this->CcrConc->FldCaption());

			// CcrUsua
			$this->CcrUsua->EditAttrs["class"] = "form-control";
			$this->CcrUsua->EditCustomAttributes = "";
			$this->CcrUsua->EditValue = ew_HtmlEncode($this->CcrUsua->CurrentValue);
			$this->CcrUsua->PlaceHolder = ew_RemoveHtml($this->CcrUsua->FldCaption());

			// CcrFCre
			$this->CcrFCre->EditAttrs["class"] = "form-control";
			$this->CcrFCre->EditCustomAttributes = "";
			$this->CcrFCre->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->CcrFCre->CurrentValue, 7));
			$this->CcrFCre->PlaceHolder = ew_RemoveHtml($this->CcrFCre->FldCaption());

			// Edit refer script
			// CcrCodi

			$this->CcrCodi->HrefValue = "";

			// VcrCodi
			$this->VcrCodi->HrefValue = "";

			// CcrNCuo
			$this->CcrNCuo->HrefValue = "";

			// CcrFPag
			$this->CcrFPag->HrefValue = "";

			// CcrMont
			$this->CcrMont->HrefValue = "";

			// CcrMora
			$this->CcrMora->HrefValue = "";

			// CcrDAtr
			$this->CcrDAtr->HrefValue = "";

			// CcrEsta
			$this->CcrEsta->HrefValue = "";

			// CcrAnho
			$this->CcrAnho->HrefValue = "";

			// CcrMes
			$this->CcrMes->HrefValue = "";

			// CcrConc
			$this->CcrConc->HrefValue = "";

			// CcrUsua
			$this->CcrUsua->HrefValue = "";

			// CcrFCre
			$this->CcrFCre->HrefValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_EDIT) { // Edit row

			// CcrCodi
			$this->CcrCodi->EditAttrs["class"] = "form-control";
			$this->CcrCodi->EditCustomAttributes = "";
			$this->CcrCodi->EditValue = $this->CcrCodi->CurrentValue;
			$this->CcrCodi->ViewCustomAttributes = "";

			// VcrCodi
			$this->VcrCodi->EditAttrs["class"] = "form-control";
			$this->VcrCodi->EditCustomAttributes = "";
			if (trim(strval($this->VcrCodi->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "\"VcrCodi\"" . ew_SearchString("=", $this->VcrCodi->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT \"VcrCodi\", \"VcrCodi\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\", '' AS \"SelectFilterFld\", '' AS \"SelectFilterFld2\", '' AS \"SelectFilterFld3\", '' AS \"SelectFilterFld4\" FROM \"public\".\"VCre\"";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->VcrCodi, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->VcrCodi->EditValue = $arwrk;

			// CcrNCuo
			$this->CcrNCuo->EditAttrs["class"] = "form-control";
			$this->CcrNCuo->EditCustomAttributes = "";
			$this->CcrNCuo->EditValue = ew_HtmlEncode($this->CcrNCuo->CurrentValue);
			$this->CcrNCuo->PlaceHolder = ew_RemoveHtml($this->CcrNCuo->FldCaption());

			// CcrFPag
			$this->CcrFPag->EditAttrs["class"] = "form-control";
			$this->CcrFPag->EditCustomAttributes = "";
			$this->CcrFPag->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->CcrFPag->CurrentValue, 7));
			$this->CcrFPag->PlaceHolder = ew_RemoveHtml($this->CcrFPag->FldCaption());

			// CcrMont
			$this->CcrMont->EditAttrs["class"] = "form-control";
			$this->CcrMont->EditCustomAttributes = "";
			$this->CcrMont->EditValue = ew_HtmlEncode($this->CcrMont->CurrentValue);
			$this->CcrMont->PlaceHolder = ew_RemoveHtml($this->CcrMont->FldCaption());
			if (strval($this->CcrMont->EditValue) <> "" && is_numeric($this->CcrMont->EditValue)) {
			$this->CcrMont->EditValue = ew_FormatNumber($this->CcrMont->EditValue, -2, -1, -2, 0);
			$this->CcrMont->OldValue = $this->CcrMont->EditValue;
			}

			// CcrMora
			$this->CcrMora->EditAttrs["class"] = "form-control";
			$this->CcrMora->EditCustomAttributes = "";
			$this->CcrMora->EditValue = ew_HtmlEncode($this->CcrMora->CurrentValue);
			$this->CcrMora->PlaceHolder = ew_RemoveHtml($this->CcrMora->FldCaption());
			if (strval($this->CcrMora->EditValue) <> "" && is_numeric($this->CcrMora->EditValue)) {
			$this->CcrMora->EditValue = ew_FormatNumber($this->CcrMora->EditValue, -2, -1, -2, 0);
			$this->CcrMora->OldValue = $this->CcrMora->EditValue;
			}

			// CcrDAtr
			$this->CcrDAtr->EditAttrs["class"] = "form-control";
			$this->CcrDAtr->EditCustomAttributes = "";
			$this->CcrDAtr->EditValue = ew_HtmlEncode($this->CcrDAtr->CurrentValue);
			$this->CcrDAtr->PlaceHolder = ew_RemoveHtml($this->CcrDAtr->FldCaption());

			// CcrEsta
			$this->CcrEsta->EditAttrs["class"] = "form-control";
			$this->CcrEsta->EditCustomAttributes = "";
			$this->CcrEsta->EditValue = ew_HtmlEncode($this->CcrEsta->CurrentValue);
			$this->CcrEsta->PlaceHolder = ew_RemoveHtml($this->CcrEsta->FldCaption());

			// CcrAnho
			$this->CcrAnho->EditAttrs["class"] = "form-control";
			$this->CcrAnho->EditCustomAttributes = "";
			$this->CcrAnho->EditValue = ew_HtmlEncode($this->CcrAnho->CurrentValue);
			$this->CcrAnho->PlaceHolder = ew_RemoveHtml($this->CcrAnho->FldCaption());

			// CcrMes
			$this->CcrMes->EditAttrs["class"] = "form-control";
			$this->CcrMes->EditCustomAttributes = "";
			$this->CcrMes->EditValue = ew_HtmlEncode($this->CcrMes->CurrentValue);
			$this->CcrMes->PlaceHolder = ew_RemoveHtml($this->CcrMes->FldCaption());

			// CcrConc
			$this->CcrConc->EditAttrs["class"] = "form-control";
			$this->CcrConc->EditCustomAttributes = "";
			$this->CcrConc->EditValue = ew_HtmlEncode($this->CcrConc->CurrentValue);
			$this->CcrConc->PlaceHolder = ew_RemoveHtml($this->CcrConc->FldCaption());

			// CcrUsua
			$this->CcrUsua->EditAttrs["class"] = "form-control";
			$this->CcrUsua->EditCustomAttributes = "";
			$this->CcrUsua->EditValue = ew_HtmlEncode($this->CcrUsua->CurrentValue);
			$this->CcrUsua->PlaceHolder = ew_RemoveHtml($this->CcrUsua->FldCaption());

			// CcrFCre
			$this->CcrFCre->EditAttrs["class"] = "form-control";
			$this->CcrFCre->EditCustomAttributes = "";
			$this->CcrFCre->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->CcrFCre->CurrentValue, 7));
			$this->CcrFCre->PlaceHolder = ew_RemoveHtml($this->CcrFCre->FldCaption());

			// Edit refer script
			// CcrCodi

			$this->CcrCodi->HrefValue = "";

			// VcrCodi
			$this->VcrCodi->HrefValue = "";

			// CcrNCuo
			$this->CcrNCuo->HrefValue = "";

			// CcrFPag
			$this->CcrFPag->HrefValue = "";

			// CcrMont
			$this->CcrMont->HrefValue = "";

			// CcrMora
			$this->CcrMora->HrefValue = "";

			// CcrDAtr
			$this->CcrDAtr->HrefValue = "";

			// CcrEsta
			$this->CcrEsta->HrefValue = "";

			// CcrAnho
			$this->CcrAnho->HrefValue = "";

			// CcrMes
			$this->CcrMes->HrefValue = "";

			// CcrConc
			$this->CcrConc->HrefValue = "";

			// CcrUsua
			$this->CcrUsua->HrefValue = "";

			// CcrFCre
			$this->CcrFCre->HrefValue = "";
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate form
	function ValidateForm() {
		global $Language, $gsFormError;

		// Initialize form error message
		$gsFormError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return ($gsFormError == "");
		if (!$this->VcrCodi->FldIsDetailKey && !is_null($this->VcrCodi->FormValue) && $this->VcrCodi->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->VcrCodi->FldCaption(), $this->VcrCodi->ReqErrMsg));
		}
		if (!$this->CcrNCuo->FldIsDetailKey && !is_null($this->CcrNCuo->FormValue) && $this->CcrNCuo->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->CcrNCuo->FldCaption(), $this->CcrNCuo->ReqErrMsg));
		}
		if (!ew_CheckInteger($this->CcrNCuo->FormValue)) {
			ew_AddMessage($gsFormError, $this->CcrNCuo->FldErrMsg());
		}
		if (!$this->CcrFPag->FldIsDetailKey && !is_null($this->CcrFPag->FormValue) && $this->CcrFPag->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->CcrFPag->FldCaption(), $this->CcrFPag->ReqErrMsg));
		}
		if (!ew_CheckEuroDate($this->CcrFPag->FormValue)) {
			ew_AddMessage($gsFormError, $this->CcrFPag->FldErrMsg());
		}
		if (!$this->CcrMont->FldIsDetailKey && !is_null($this->CcrMont->FormValue) && $this->CcrMont->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->CcrMont->FldCaption(), $this->CcrMont->ReqErrMsg));
		}
		if (!ew_CheckNumber($this->CcrMont->FormValue)) {
			ew_AddMessage($gsFormError, $this->CcrMont->FldErrMsg());
		}
		if (!ew_CheckNumber($this->CcrMora->FormValue)) {
			ew_AddMessage($gsFormError, $this->CcrMora->FldErrMsg());
		}
		if (!ew_CheckInteger($this->CcrDAtr->FormValue)) {
			ew_AddMessage($gsFormError, $this->CcrDAtr->FldErrMsg());
		}
		if (!$this->CcrEsta->FldIsDetailKey && !is_null($this->CcrEsta->FormValue) && $this->CcrEsta->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->CcrEsta->FldCaption(), $this->CcrEsta->ReqErrMsg));
		}
		if (!$this->CcrAnho->FldIsDetailKey && !is_null($this->CcrAnho->FormValue) && $this->CcrAnho->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->CcrAnho->FldCaption(), $this->CcrAnho->ReqErrMsg));
		}
		if (!$this->CcrMes->FldIsDetailKey && !is_null($this->CcrMes->FormValue) && $this->CcrMes->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->CcrMes->FldCaption(), $this->CcrMes->ReqErrMsg));
		}
		if (!$this->CcrConc->FldIsDetailKey && !is_null($this->CcrConc->FormValue) && $this->CcrConc->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->CcrConc->FldCaption(), $this->CcrConc->ReqErrMsg));
		}
		if (!$this->CcrUsua->FldIsDetailKey && !is_null($this->CcrUsua->FormValue) && $this->CcrUsua->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->CcrUsua->FldCaption(), $this->CcrUsua->ReqErrMsg));
		}
		if (!$this->CcrFCre->FldIsDetailKey && !is_null($this->CcrFCre->FormValue) && $this->CcrFCre->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->CcrFCre->FldCaption(), $this->CcrFCre->ReqErrMsg));
		}
		if (!ew_CheckEuroDate($this->CcrFCre->FormValue)) {
			ew_AddMessage($gsFormError, $this->CcrFCre->FldErrMsg());
		}

		// Return validate result
		$ValidateForm = ($gsFormError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateForm = $ValidateForm && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsFormError, $sFormCustomError);
		}
		return $ValidateForm;
	}

	//
	// Delete records based on current filter
	//
	function DeleteRows() {
		global $Language, $Security;
		if (!$Security->CanDelete()) {
			$this->setFailureMessage($Language->Phrase("NoDeletePermission")); // No delete permission
			return FALSE;
		}
		$DeleteRows = TRUE;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE) {
			return FALSE;
		} elseif ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
			$rs->Close();
			return FALSE;

		//} else {
		//	$this->LoadRowValues($rs); // Load row values

		}
		$rows = ($rs) ? $rs->GetRows() : array();

		// Clone old rows
		$rsold = $rows;
		if ($rs)
			$rs->Close();

		// Call row deleting event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$DeleteRows = $this->Row_Deleting($row);
				if (!$DeleteRows) break;
			}
		}
		if ($DeleteRows) {
			$sKey = "";
			foreach ($rsold as $row) {
				$sThisKey = "";
				if ($sThisKey <> "") $sThisKey .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
				$sThisKey .= $row['CcrCodi'];
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				$DeleteRows = $this->Delete($row); // Delete
				$conn->raiseErrorFn = '';
				if ($DeleteRows === FALSE)
					break;
				if ($sKey <> "") $sKey .= ", ";
				$sKey .= $sThisKey;
			}
		} else {

			// Set up error message
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("DeleteCancelled"));
			}
		}
		if ($DeleteRows) {
		} else {
		}

		// Call Row Deleted event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$this->Row_Deleted($row);
			}
		}
		return $DeleteRows;
	}

	// Update record based on key values
	function EditRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$conn = &$this->Connection();
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE)
			return FALSE;
		if ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record message
			$EditRow = FALSE; // Update Failed
		} else {

			// Save old values
			$rsold = &$rs->fields;
			$this->LoadDbValues($rsold);
			$rsnew = array();

			// VcrCodi
			$this->VcrCodi->SetDbValueDef($rsnew, $this->VcrCodi->CurrentValue, 0, $this->VcrCodi->ReadOnly);

			// CcrNCuo
			$this->CcrNCuo->SetDbValueDef($rsnew, $this->CcrNCuo->CurrentValue, 0, $this->CcrNCuo->ReadOnly);

			// CcrFPag
			$this->CcrFPag->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->CcrFPag->CurrentValue, 7), ew_CurrentDate(), $this->CcrFPag->ReadOnly);

			// CcrMont
			$this->CcrMont->SetDbValueDef($rsnew, $this->CcrMont->CurrentValue, 0, $this->CcrMont->ReadOnly);

			// CcrMora
			$this->CcrMora->SetDbValueDef($rsnew, $this->CcrMora->CurrentValue, NULL, $this->CcrMora->ReadOnly);

			// CcrDAtr
			$this->CcrDAtr->SetDbValueDef($rsnew, $this->CcrDAtr->CurrentValue, NULL, $this->CcrDAtr->ReadOnly);

			// CcrEsta
			$this->CcrEsta->SetDbValueDef($rsnew, $this->CcrEsta->CurrentValue, "", $this->CcrEsta->ReadOnly);

			// CcrAnho
			$this->CcrAnho->SetDbValueDef($rsnew, $this->CcrAnho->CurrentValue, "", $this->CcrAnho->ReadOnly);

			// CcrMes
			$this->CcrMes->SetDbValueDef($rsnew, $this->CcrMes->CurrentValue, "", $this->CcrMes->ReadOnly);

			// CcrConc
			$this->CcrConc->SetDbValueDef($rsnew, $this->CcrConc->CurrentValue, "", $this->CcrConc->ReadOnly);

			// CcrUsua
			$this->CcrUsua->SetDbValueDef($rsnew, $this->CcrUsua->CurrentValue, "", $this->CcrUsua->ReadOnly);

			// CcrFCre
			$this->CcrFCre->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->CcrFCre->CurrentValue, 7), ew_CurrentDate(), $this->CcrFCre->ReadOnly);

			// Call Row Updating event
			$bUpdateRow = $this->Row_Updating($rsold, $rsnew);
			if ($bUpdateRow) {
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				if (count($rsnew) > 0)
					$EditRow = $this->Update($rsnew, "", $rsold);
				else
					$EditRow = TRUE; // No field to update
				$conn->raiseErrorFn = '';
				if ($EditRow) {
				}
			} else {
				if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

					// Use the message, do nothing
				} elseif ($this->CancelMessage <> "") {
					$this->setFailureMessage($this->CancelMessage);
					$this->CancelMessage = "";
				} else {
					$this->setFailureMessage($Language->Phrase("UpdateCancelled"));
				}
				$EditRow = FALSE;
			}
		}

		// Call Row_Updated event
		if ($EditRow)
			$this->Row_Updated($rsold, $rsnew);
		$rs->Close();
		return $EditRow;
	}

	// Add record
	function AddRow($rsold = NULL) {
		global $Language, $Security;
		$conn = &$this->Connection();

		// Load db values from rsold
		if ($rsold) {
			$this->LoadDbValues($rsold);
		}
		$rsnew = array();

		// VcrCodi
		$this->VcrCodi->SetDbValueDef($rsnew, $this->VcrCodi->CurrentValue, 0, FALSE);

		// CcrNCuo
		$this->CcrNCuo->SetDbValueDef($rsnew, $this->CcrNCuo->CurrentValue, 0, FALSE);

		// CcrFPag
		$this->CcrFPag->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->CcrFPag->CurrentValue, 7), ew_CurrentDate(), FALSE);

		// CcrMont
		$this->CcrMont->SetDbValueDef($rsnew, $this->CcrMont->CurrentValue, 0, FALSE);

		// CcrMora
		$this->CcrMora->SetDbValueDef($rsnew, $this->CcrMora->CurrentValue, NULL, FALSE);

		// CcrDAtr
		$this->CcrDAtr->SetDbValueDef($rsnew, $this->CcrDAtr->CurrentValue, NULL, FALSE);

		// CcrEsta
		$this->CcrEsta->SetDbValueDef($rsnew, $this->CcrEsta->CurrentValue, "", FALSE);

		// CcrAnho
		$this->CcrAnho->SetDbValueDef($rsnew, $this->CcrAnho->CurrentValue, "", FALSE);

		// CcrMes
		$this->CcrMes->SetDbValueDef($rsnew, $this->CcrMes->CurrentValue, "", FALSE);

		// CcrConc
		$this->CcrConc->SetDbValueDef($rsnew, $this->CcrConc->CurrentValue, "", FALSE);

		// CcrUsua
		$this->CcrUsua->SetDbValueDef($rsnew, $this->CcrUsua->CurrentValue, "", FALSE);

		// CcrFCre
		$this->CcrFCre->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->CcrFCre->CurrentValue, 7), ew_CurrentDate(), FALSE);

		// Call Row Inserting event
		$rs = ($rsold == NULL) ? NULL : $rsold->fields;
		$bInsertRow = $this->Row_Inserting($rs, $rsnew);
		if ($bInsertRow) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			$AddRow = $this->Insert($rsnew);
			$conn->raiseErrorFn = '';
			if ($AddRow) {

				// Get insert id if necessary
				$this->CcrCodi->setDbValue($conn->GetOne("SELECT currval('\"CCre_CcrCodi_seq\"'::regclass)"));
				$rsnew['CcrCodi'] = $this->CcrCodi->DbValue;
			}
		} else {
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("InsertCancelled"));
			}
			$AddRow = FALSE;
		}
		if ($AddRow) {

			// Call Row Inserted event
			$rs = ($rsold == NULL) ? NULL : $rsold->fields;
			$this->Row_Inserted($rs, $rsnew);
		}
		return $AddRow;
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$url = preg_replace('/\?cmd=reset(all){0,1}$/i', '', $url); // Remove cmd=reset / cmd=resetall
		$Breadcrumb->Add("list", $this->TableVar, $url, "", $this->TableVar, TRUE);
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}

	// ListOptions Load event
	function ListOptions_Load() {

		// Example:
		//$opt = &$this->ListOptions->Add("new");
		//$opt->Header = "xxx";
		//$opt->OnLeft = TRUE; // Link on left
		//$opt->MoveTo(0); // Move to first column

	}

	// ListOptions Rendered event
	function ListOptions_Rendered() {

		// Example: 
		//$this->ListOptions->Items["new"]->Body = "xxx";

	}

	// Row Custom Action event
	function Row_CustomAction($action, $row) {

		// Return FALSE to abort
		return TRUE;
	}

	// Page Exporting event
	// $this->ExportDoc = export document object
	function Page_Exporting() {

		//$this->ExportDoc->Text = "my header"; // Export header
		//return FALSE; // Return FALSE to skip default export and use Row_Export event

		return TRUE; // Return TRUE to use default export and skip Row_Export event
	}

	// Row Export event
	// $this->ExportDoc = export document object
	function Row_Export($rs) {

	    //$this->ExportDoc->Text .= "my content"; // Build HTML with field value: $rs["MyField"] or $this->MyField->ViewValue
	}

	// Page Exported event
	// $this->ExportDoc = export document object
	function Page_Exported() {

		//$this->ExportDoc->Text .= "my footer"; // Export footer
		//echo $this->ExportDoc->Text;

	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($CCre_list)) $CCre_list = new cCCre_list();

// Page init
$CCre_list->Page_Init();

// Page main
$CCre_list->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$CCre_list->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "list";
var CurrentForm = fCCrelist = new ew_Form("fCCrelist", "list");
fCCrelist.FormKeyCountName = '<?php echo $CCre_list->FormKeyCountName ?>';

// Validate form
fCCrelist.Validate = function() {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.GetForm(), $fobj = $(fobj);
	if ($fobj.find("#a_confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.FormKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = $fobj.find("#a_list").val() == "gridinsert";
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
		var checkrow = (gridinsert) ? !this.EmptyRow(infix) : true;
		if (checkrow) {
			addcnt++;
			elm = this.GetElements("x" + infix + "_VcrCodi");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $CCre->VcrCodi->FldCaption(), $CCre->VcrCodi->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_CcrNCuo");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $CCre->CcrNCuo->FldCaption(), $CCre->CcrNCuo->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_CcrNCuo");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($CCre->CcrNCuo->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_CcrFPag");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $CCre->CcrFPag->FldCaption(), $CCre->CcrFPag->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_CcrFPag");
			if (elm && !ew_CheckEuroDate(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($CCre->CcrFPag->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_CcrMont");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $CCre->CcrMont->FldCaption(), $CCre->CcrMont->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_CcrMont");
			if (elm && !ew_CheckNumber(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($CCre->CcrMont->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_CcrMora");
			if (elm && !ew_CheckNumber(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($CCre->CcrMora->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_CcrDAtr");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($CCre->CcrDAtr->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_CcrEsta");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $CCre->CcrEsta->FldCaption(), $CCre->CcrEsta->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_CcrAnho");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $CCre->CcrAnho->FldCaption(), $CCre->CcrAnho->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_CcrMes");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $CCre->CcrMes->FldCaption(), $CCre->CcrMes->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_CcrConc");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $CCre->CcrConc->FldCaption(), $CCre->CcrConc->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_CcrUsua");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $CCre->CcrUsua->FldCaption(), $CCre->CcrUsua->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_CcrFCre");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $CCre->CcrFCre->FldCaption(), $CCre->CcrFCre->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_CcrFCre");
			if (elm && !ew_CheckEuroDate(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($CCre->CcrFCre->FldErrMsg()) ?>");

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
		} // End Grid Add checking
	}
	if (gridinsert && addcnt == 0) { // No row added
		ew_Alert(ewLanguage.Phrase("NoAddRecord"));
		return false;
	}
	return true;
}

// Check empty row
fCCrelist.EmptyRow = function(infix) {
	var fobj = this.Form;
	if (ew_ValueChanged(fobj, infix, "VcrCodi", false)) return false;
	if (ew_ValueChanged(fobj, infix, "CcrNCuo", false)) return false;
	if (ew_ValueChanged(fobj, infix, "CcrFPag", false)) return false;
	if (ew_ValueChanged(fobj, infix, "CcrMont", false)) return false;
	if (ew_ValueChanged(fobj, infix, "CcrMora", false)) return false;
	if (ew_ValueChanged(fobj, infix, "CcrDAtr", false)) return false;
	if (ew_ValueChanged(fobj, infix, "CcrEsta", false)) return false;
	if (ew_ValueChanged(fobj, infix, "CcrAnho", false)) return false;
	if (ew_ValueChanged(fobj, infix, "CcrMes", false)) return false;
	if (ew_ValueChanged(fobj, infix, "CcrConc", false)) return false;
	if (ew_ValueChanged(fobj, infix, "CcrUsua", false)) return false;
	if (ew_ValueChanged(fobj, infix, "CcrFCre", false)) return false;
	return true;
}

// Form_CustomValidate event
fCCrelist.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fCCrelist.ValidateRequired = true;
<?php } else { ?>
fCCrelist.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
fCCrelist.Lists["x_VcrCodi"] = {"LinkField":"x_VcrCodi","Ajax":true,"AutoFill":false,"DisplayFields":["x_VcrCodi","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};

// Form object for search
var CurrentSearchForm = fCCrelistsrch = new ew_Form("fCCrelistsrch");
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php if ($CCre_list->TotalRecs > 0 && $CCre_list->ExportOptions->Visible()) { ?>
<?php $CCre_list->ExportOptions->Render("body") ?>
<?php } ?>
<?php if ($CCre_list->SearchOptions->Visible()) { ?>
<?php $CCre_list->SearchOptions->Render("body") ?>
<?php } ?>
<?php if ($CCre_list->FilterOptions->Visible()) { ?>
<?php $CCre_list->FilterOptions->Render("body") ?>
<?php } ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php
if ($CCre->CurrentAction == "gridadd") {
	$CCre->CurrentFilter = "0=1";
	$CCre_list->StartRec = 1;
	$CCre_list->DisplayRecs = $CCre->GridAddRowCount;
	$CCre_list->TotalRecs = $CCre_list->DisplayRecs;
	$CCre_list->StopRec = $CCre_list->DisplayRecs;
} else {
	$bSelectLimit = $CCre_list->UseSelectLimit;
	if ($bSelectLimit) {
		if ($CCre_list->TotalRecs <= 0)
			$CCre_list->TotalRecs = $CCre->SelectRecordCount();
	} else {
		if (!$CCre_list->Recordset && ($CCre_list->Recordset = $CCre_list->LoadRecordset()))
			$CCre_list->TotalRecs = $CCre_list->Recordset->RecordCount();
	}
	$CCre_list->StartRec = 1;
	if ($CCre_list->DisplayRecs <= 0 || ($CCre->Export <> "" && $CCre->ExportAll)) // Display all records
		$CCre_list->DisplayRecs = $CCre_list->TotalRecs;
	if (!($CCre->Export <> "" && $CCre->ExportAll))
		$CCre_list->SetUpStartRec(); // Set up start record position
	if ($bSelectLimit)
		$CCre_list->Recordset = $CCre_list->LoadRecordset($CCre_list->StartRec-1, $CCre_list->DisplayRecs);

	// Set no record found message
	if ($CCre->CurrentAction == "" && $CCre_list->TotalRecs == 0) {
		if (!$Security->CanList())
			$CCre_list->setWarningMessage($Language->Phrase("NoPermission"));
		if ($CCre_list->SearchWhere == "0=101")
			$CCre_list->setWarningMessage($Language->Phrase("EnterSearchCriteria"));
		else
			$CCre_list->setWarningMessage($Language->Phrase("NoRecord"));
	}
}
$CCre_list->RenderOtherOptions();
?>
<?php if ($Security->CanSearch()) { ?>
<?php if ($CCre->Export == "" && $CCre->CurrentAction == "") { ?>
<form name="fCCrelistsrch" id="fCCrelistsrch" class="form-inline ewForm" action="<?php echo ew_CurrentPage() ?>">
<?php $SearchPanelClass = ($CCre_list->SearchWhere <> "") ? " in" : " in"; ?>
<div id="fCCrelistsrch_SearchPanel" class="ewSearchPanel collapse<?php echo $SearchPanelClass ?>">
<input type="hidden" name="cmd" value="search">
<input type="hidden" name="t" value="CCre">
	<div class="ewBasicSearch">
<div id="xsr_1" class="ewRow">
	<div class="ewQuickSearch input-group">
	<input type="text" name="<?php echo EW_TABLE_BASIC_SEARCH ?>" id="<?php echo EW_TABLE_BASIC_SEARCH ?>" class="form-control" value="<?php echo ew_HtmlEncode($CCre_list->BasicSearch->getKeyword()) ?>" placeholder="<?php echo ew_HtmlEncode($Language->Phrase("Search")) ?>">
	<input type="hidden" name="<?php echo EW_TABLE_BASIC_SEARCH_TYPE ?>" id="<?php echo EW_TABLE_BASIC_SEARCH_TYPE ?>" value="<?php echo ew_HtmlEncode($CCre_list->BasicSearch->getType()) ?>">
	<div class="input-group-btn">
		<button type="button" data-toggle="dropdown" class="btn btn-default"><span id="searchtype"><?php echo $CCre_list->BasicSearch->getTypeNameShort() ?></span><span class="caret"></span></button>
		<ul class="dropdown-menu pull-right" role="menu">
			<li<?php if ($CCre_list->BasicSearch->getType() == "") echo " class=\"active\""; ?>><a href="javascript:void(0);" onclick="ew_SetSearchType(this)"><?php echo $Language->Phrase("QuickSearchAuto") ?></a></li>
			<li<?php if ($CCre_list->BasicSearch->getType() == "=") echo " class=\"active\""; ?>><a href="javascript:void(0);" onclick="ew_SetSearchType(this,'=')"><?php echo $Language->Phrase("QuickSearchExact") ?></a></li>
			<li<?php if ($CCre_list->BasicSearch->getType() == "AND") echo " class=\"active\""; ?>><a href="javascript:void(0);" onclick="ew_SetSearchType(this,'AND')"><?php echo $Language->Phrase("QuickSearchAll") ?></a></li>
			<li<?php if ($CCre_list->BasicSearch->getType() == "OR") echo " class=\"active\""; ?>><a href="javascript:void(0);" onclick="ew_SetSearchType(this,'OR')"><?php echo $Language->Phrase("QuickSearchAny") ?></a></li>
		</ul>
	<button class="btn btn-primary ewButton" name="btnsubmit" id="btnsubmit" type="submit"><?php echo $Language->Phrase("QuickSearchBtn") ?></button>
	</div>
	</div>
</div>
	</div>
</div>
</form>
<?php } ?>
<?php } ?>
<?php $CCre_list->ShowPageHeader(); ?>
<?php
$CCre_list->ShowMessage();
?>
<?php if ($CCre_list->TotalRecs > 0 || $CCre->CurrentAction <> "") { ?>
<div class="panel panel-default ewGrid">
<form name="fCCrelist" id="fCCrelist" class="form-inline ewForm ewListForm" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($CCre_list->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $CCre_list->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="CCre">
<div id="gmp_CCre" class="<?php if (ew_IsResponsiveLayout()) { echo "table-responsive "; } ?>ewGridMiddlePanel">
<?php if ($CCre_list->TotalRecs > 0) { ?>
<table id="tbl_CCrelist" class="table ewTable">
<?php echo $CCre->TableCustomInnerHtml ?>
<thead><!-- Table header -->
	<tr class="ewTableHeader">
<?php

// Header row
$CCre_list->RowType = EW_ROWTYPE_HEADER;

// Render list options
$CCre_list->RenderListOptions();

// Render list options (header, left)
$CCre_list->ListOptions->Render("header", "left");
?>
<?php if ($CCre->CcrCodi->Visible) { // CcrCodi ?>
	<?php if ($CCre->SortUrl($CCre->CcrCodi) == "") { ?>
		<th data-name="CcrCodi"><div id="elh_CCre_CcrCodi" class="CCre_CcrCodi"><div class="ewTableHeaderCaption"><?php echo $CCre->CcrCodi->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="CcrCodi"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $CCre->SortUrl($CCre->CcrCodi) ?>',2);"><div id="elh_CCre_CcrCodi" class="CCre_CcrCodi">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $CCre->CcrCodi->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($CCre->CcrCodi->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($CCre->CcrCodi->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($CCre->VcrCodi->Visible) { // VcrCodi ?>
	<?php if ($CCre->SortUrl($CCre->VcrCodi) == "") { ?>
		<th data-name="VcrCodi"><div id="elh_CCre_VcrCodi" class="CCre_VcrCodi"><div class="ewTableHeaderCaption"><?php echo $CCre->VcrCodi->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="VcrCodi"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $CCre->SortUrl($CCre->VcrCodi) ?>',2);"><div id="elh_CCre_VcrCodi" class="CCre_VcrCodi">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $CCre->VcrCodi->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($CCre->VcrCodi->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($CCre->VcrCodi->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($CCre->CcrNCuo->Visible) { // CcrNCuo ?>
	<?php if ($CCre->SortUrl($CCre->CcrNCuo) == "") { ?>
		<th data-name="CcrNCuo"><div id="elh_CCre_CcrNCuo" class="CCre_CcrNCuo"><div class="ewTableHeaderCaption"><?php echo $CCre->CcrNCuo->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="CcrNCuo"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $CCre->SortUrl($CCre->CcrNCuo) ?>',2);"><div id="elh_CCre_CcrNCuo" class="CCre_CcrNCuo">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $CCre->CcrNCuo->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($CCre->CcrNCuo->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($CCre->CcrNCuo->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($CCre->CcrFPag->Visible) { // CcrFPag ?>
	<?php if ($CCre->SortUrl($CCre->CcrFPag) == "") { ?>
		<th data-name="CcrFPag"><div id="elh_CCre_CcrFPag" class="CCre_CcrFPag"><div class="ewTableHeaderCaption"><?php echo $CCre->CcrFPag->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="CcrFPag"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $CCre->SortUrl($CCre->CcrFPag) ?>',2);"><div id="elh_CCre_CcrFPag" class="CCre_CcrFPag">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $CCre->CcrFPag->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($CCre->CcrFPag->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($CCre->CcrFPag->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($CCre->CcrMont->Visible) { // CcrMont ?>
	<?php if ($CCre->SortUrl($CCre->CcrMont) == "") { ?>
		<th data-name="CcrMont"><div id="elh_CCre_CcrMont" class="CCre_CcrMont"><div class="ewTableHeaderCaption"><?php echo $CCre->CcrMont->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="CcrMont"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $CCre->SortUrl($CCre->CcrMont) ?>',2);"><div id="elh_CCre_CcrMont" class="CCre_CcrMont">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $CCre->CcrMont->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($CCre->CcrMont->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($CCre->CcrMont->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($CCre->CcrMora->Visible) { // CcrMora ?>
	<?php if ($CCre->SortUrl($CCre->CcrMora) == "") { ?>
		<th data-name="CcrMora"><div id="elh_CCre_CcrMora" class="CCre_CcrMora"><div class="ewTableHeaderCaption"><?php echo $CCre->CcrMora->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="CcrMora"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $CCre->SortUrl($CCre->CcrMora) ?>',2);"><div id="elh_CCre_CcrMora" class="CCre_CcrMora">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $CCre->CcrMora->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($CCre->CcrMora->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($CCre->CcrMora->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($CCre->CcrDAtr->Visible) { // CcrDAtr ?>
	<?php if ($CCre->SortUrl($CCre->CcrDAtr) == "") { ?>
		<th data-name="CcrDAtr"><div id="elh_CCre_CcrDAtr" class="CCre_CcrDAtr"><div class="ewTableHeaderCaption"><?php echo $CCre->CcrDAtr->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="CcrDAtr"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $CCre->SortUrl($CCre->CcrDAtr) ?>',2);"><div id="elh_CCre_CcrDAtr" class="CCre_CcrDAtr">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $CCre->CcrDAtr->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($CCre->CcrDAtr->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($CCre->CcrDAtr->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($CCre->CcrEsta->Visible) { // CcrEsta ?>
	<?php if ($CCre->SortUrl($CCre->CcrEsta) == "") { ?>
		<th data-name="CcrEsta"><div id="elh_CCre_CcrEsta" class="CCre_CcrEsta"><div class="ewTableHeaderCaption"><?php echo $CCre->CcrEsta->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="CcrEsta"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $CCre->SortUrl($CCre->CcrEsta) ?>',2);"><div id="elh_CCre_CcrEsta" class="CCre_CcrEsta">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $CCre->CcrEsta->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($CCre->CcrEsta->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($CCre->CcrEsta->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($CCre->CcrAnho->Visible) { // CcrAnho ?>
	<?php if ($CCre->SortUrl($CCre->CcrAnho) == "") { ?>
		<th data-name="CcrAnho"><div id="elh_CCre_CcrAnho" class="CCre_CcrAnho"><div class="ewTableHeaderCaption"><?php echo $CCre->CcrAnho->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="CcrAnho"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $CCre->SortUrl($CCre->CcrAnho) ?>',2);"><div id="elh_CCre_CcrAnho" class="CCre_CcrAnho">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $CCre->CcrAnho->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($CCre->CcrAnho->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($CCre->CcrAnho->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($CCre->CcrMes->Visible) { // CcrMes ?>
	<?php if ($CCre->SortUrl($CCre->CcrMes) == "") { ?>
		<th data-name="CcrMes"><div id="elh_CCre_CcrMes" class="CCre_CcrMes"><div class="ewTableHeaderCaption"><?php echo $CCre->CcrMes->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="CcrMes"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $CCre->SortUrl($CCre->CcrMes) ?>',2);"><div id="elh_CCre_CcrMes" class="CCre_CcrMes">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $CCre->CcrMes->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($CCre->CcrMes->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($CCre->CcrMes->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($CCre->CcrConc->Visible) { // CcrConc ?>
	<?php if ($CCre->SortUrl($CCre->CcrConc) == "") { ?>
		<th data-name="CcrConc"><div id="elh_CCre_CcrConc" class="CCre_CcrConc"><div class="ewTableHeaderCaption"><?php echo $CCre->CcrConc->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="CcrConc"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $CCre->SortUrl($CCre->CcrConc) ?>',2);"><div id="elh_CCre_CcrConc" class="CCre_CcrConc">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $CCre->CcrConc->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($CCre->CcrConc->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($CCre->CcrConc->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($CCre->CcrUsua->Visible) { // CcrUsua ?>
	<?php if ($CCre->SortUrl($CCre->CcrUsua) == "") { ?>
		<th data-name="CcrUsua"><div id="elh_CCre_CcrUsua" class="CCre_CcrUsua"><div class="ewTableHeaderCaption"><?php echo $CCre->CcrUsua->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="CcrUsua"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $CCre->SortUrl($CCre->CcrUsua) ?>',2);"><div id="elh_CCre_CcrUsua" class="CCre_CcrUsua">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $CCre->CcrUsua->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($CCre->CcrUsua->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($CCre->CcrUsua->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($CCre->CcrFCre->Visible) { // CcrFCre ?>
	<?php if ($CCre->SortUrl($CCre->CcrFCre) == "") { ?>
		<th data-name="CcrFCre"><div id="elh_CCre_CcrFCre" class="CCre_CcrFCre"><div class="ewTableHeaderCaption"><?php echo $CCre->CcrFCre->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="CcrFCre"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $CCre->SortUrl($CCre->CcrFCre) ?>',2);"><div id="elh_CCre_CcrFCre" class="CCre_CcrFCre">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $CCre->CcrFCre->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($CCre->CcrFCre->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($CCre->CcrFCre->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php

// Render list options (header, right)
$CCre_list->ListOptions->Render("header", "right");
?>
	</tr>
</thead>
<tbody>
<?php
if ($CCre->ExportAll && $CCre->Export <> "") {
	$CCre_list->StopRec = $CCre_list->TotalRecs;
} else {

	// Set the last record to display
	if ($CCre_list->TotalRecs > $CCre_list->StartRec + $CCre_list->DisplayRecs - 1)
		$CCre_list->StopRec = $CCre_list->StartRec + $CCre_list->DisplayRecs - 1;
	else
		$CCre_list->StopRec = $CCre_list->TotalRecs;
}

// Restore number of post back records
if ($objForm) {
	$objForm->Index = -1;
	if ($objForm->HasValue($CCre_list->FormKeyCountName) && ($CCre->CurrentAction == "gridadd" || $CCre->CurrentAction == "gridedit" || $CCre->CurrentAction == "F")) {
		$CCre_list->KeyCount = $objForm->GetValue($CCre_list->FormKeyCountName);
		$CCre_list->StopRec = $CCre_list->StartRec + $CCre_list->KeyCount - 1;
	}
}
$CCre_list->RecCnt = $CCre_list->StartRec - 1;
if ($CCre_list->Recordset && !$CCre_list->Recordset->EOF) {
	$CCre_list->Recordset->MoveFirst();
	$bSelectLimit = $CCre_list->UseSelectLimit;
	if (!$bSelectLimit && $CCre_list->StartRec > 1)
		$CCre_list->Recordset->Move($CCre_list->StartRec - 1);
} elseif (!$CCre->AllowAddDeleteRow && $CCre_list->StopRec == 0) {
	$CCre_list->StopRec = $CCre->GridAddRowCount;
}

// Initialize aggregate
$CCre->RowType = EW_ROWTYPE_AGGREGATEINIT;
$CCre->ResetAttrs();
$CCre_list->RenderRow();
if ($CCre->CurrentAction == "gridadd")
	$CCre_list->RowIndex = 0;
if ($CCre->CurrentAction == "gridedit")
	$CCre_list->RowIndex = 0;
while ($CCre_list->RecCnt < $CCre_list->StopRec) {
	$CCre_list->RecCnt++;
	if (intval($CCre_list->RecCnt) >= intval($CCre_list->StartRec)) {
		$CCre_list->RowCnt++;
		if ($CCre->CurrentAction == "gridadd" || $CCre->CurrentAction == "gridedit" || $CCre->CurrentAction == "F") {
			$CCre_list->RowIndex++;
			$objForm->Index = $CCre_list->RowIndex;
			if ($objForm->HasValue($CCre_list->FormActionName))
				$CCre_list->RowAction = strval($objForm->GetValue($CCre_list->FormActionName));
			elseif ($CCre->CurrentAction == "gridadd")
				$CCre_list->RowAction = "insert";
			else
				$CCre_list->RowAction = "";
		}

		// Set up key count
		$CCre_list->KeyCount = $CCre_list->RowIndex;

		// Init row class and style
		$CCre->ResetAttrs();
		$CCre->CssClass = "";
		if ($CCre->CurrentAction == "gridadd") {
			$CCre_list->LoadDefaultValues(); // Load default values
		} else {
			$CCre_list->LoadRowValues($CCre_list->Recordset); // Load row values
		}
		$CCre->RowType = EW_ROWTYPE_VIEW; // Render view
		if ($CCre->CurrentAction == "gridadd") // Grid add
			$CCre->RowType = EW_ROWTYPE_ADD; // Render add
		if ($CCre->CurrentAction == "gridadd" && $CCre->EventCancelled && !$objForm->HasValue("k_blankrow")) // Insert failed
			$CCre_list->RestoreCurrentRowFormValues($CCre_list->RowIndex); // Restore form values
		if ($CCre->CurrentAction == "gridedit") { // Grid edit
			if ($CCre->EventCancelled) {
				$CCre_list->RestoreCurrentRowFormValues($CCre_list->RowIndex); // Restore form values
			}
			if ($CCre_list->RowAction == "insert")
				$CCre->RowType = EW_ROWTYPE_ADD; // Render add
			else
				$CCre->RowType = EW_ROWTYPE_EDIT; // Render edit
		}
		if ($CCre->CurrentAction == "gridedit" && ($CCre->RowType == EW_ROWTYPE_EDIT || $CCre->RowType == EW_ROWTYPE_ADD) && $CCre->EventCancelled) // Update failed
			$CCre_list->RestoreCurrentRowFormValues($CCre_list->RowIndex); // Restore form values
		if ($CCre->RowType == EW_ROWTYPE_EDIT) // Edit row
			$CCre_list->EditRowCnt++;

		// Set up row id / data-rowindex
		$CCre->RowAttrs = array_merge($CCre->RowAttrs, array('data-rowindex'=>$CCre_list->RowCnt, 'id'=>'r' . $CCre_list->RowCnt . '_CCre', 'data-rowtype'=>$CCre->RowType));

		// Render row
		$CCre_list->RenderRow();

		// Render list options
		$CCre_list->RenderListOptions();

		// Skip delete row / empty row for confirm page
		if ($CCre_list->RowAction <> "delete" && $CCre_list->RowAction <> "insertdelete" && !($CCre_list->RowAction == "insert" && $CCre->CurrentAction == "F" && $CCre_list->EmptyRow())) {
?>
	<tr<?php echo $CCre->RowAttributes() ?>>
<?php

// Render list options (body, left)
$CCre_list->ListOptions->Render("body", "left", $CCre_list->RowCnt);
?>
	<?php if ($CCre->CcrCodi->Visible) { // CcrCodi ?>
		<td data-name="CcrCodi"<?php echo $CCre->CcrCodi->CellAttributes() ?>>
<?php if ($CCre->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<input type="hidden" data-table="CCre" data-field="x_CcrCodi" name="o<?php echo $CCre_list->RowIndex ?>_CcrCodi" id="o<?php echo $CCre_list->RowIndex ?>_CcrCodi" value="<?php echo ew_HtmlEncode($CCre->CcrCodi->OldValue) ?>">
<?php } ?>
<?php if ($CCre->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $CCre_list->RowCnt ?>_CCre_CcrCodi" class="form-group CCre_CcrCodi">
<span<?php echo $CCre->CcrCodi->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $CCre->CcrCodi->EditValue ?></p></span>
</span>
<input type="hidden" data-table="CCre" data-field="x_CcrCodi" name="x<?php echo $CCre_list->RowIndex ?>_CcrCodi" id="x<?php echo $CCre_list->RowIndex ?>_CcrCodi" value="<?php echo ew_HtmlEncode($CCre->CcrCodi->CurrentValue) ?>">
<?php } ?>
<?php if ($CCre->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $CCre_list->RowCnt ?>_CCre_CcrCodi" class="CCre_CcrCodi">
<span<?php echo $CCre->CcrCodi->ViewAttributes() ?>>
<?php echo $CCre->CcrCodi->ListViewValue() ?></span>
</span>
<?php } ?>
<a id="<?php echo $CCre_list->PageObjName . "_row_" . $CCre_list->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($CCre->VcrCodi->Visible) { // VcrCodi ?>
		<td data-name="VcrCodi"<?php echo $CCre->VcrCodi->CellAttributes() ?>>
<?php if ($CCre->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $CCre_list->RowCnt ?>_CCre_VcrCodi" class="form-group CCre_VcrCodi">
<select data-table="CCre" data-field="x_VcrCodi" data-value-separator="<?php echo ew_HtmlEncode(is_array($CCre->VcrCodi->DisplayValueSeparator) ? json_encode($CCre->VcrCodi->DisplayValueSeparator) : $CCre->VcrCodi->DisplayValueSeparator) ?>" id="x<?php echo $CCre_list->RowIndex ?>_VcrCodi" name="x<?php echo $CCre_list->RowIndex ?>_VcrCodi"<?php echo $CCre->VcrCodi->EditAttributes() ?>>
<?php
if (is_array($CCre->VcrCodi->EditValue)) {
	$arwrk = $CCre->VcrCodi->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($CCre->VcrCodi->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $CCre->VcrCodi->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($CCre->VcrCodi->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($CCre->VcrCodi->CurrentValue) ?>" selected><?php echo $CCre->VcrCodi->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $CCre->VcrCodi->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT \"VcrCodi\", \"VcrCodi\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"VCre\"";
$sWhereWrk = "";
$CCre->VcrCodi->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$CCre->VcrCodi->LookupFilters += array("f0" => "\"VcrCodi\" = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$CCre->Lookup_Selecting($CCre->VcrCodi, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $CCre->VcrCodi->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $CCre_list->RowIndex ?>_VcrCodi" id="s_x<?php echo $CCre_list->RowIndex ?>_VcrCodi" value="<?php echo $CCre->VcrCodi->LookupFilterQuery() ?>">
</span>
<input type="hidden" data-table="CCre" data-field="x_VcrCodi" name="o<?php echo $CCre_list->RowIndex ?>_VcrCodi" id="o<?php echo $CCre_list->RowIndex ?>_VcrCodi" value="<?php echo ew_HtmlEncode($CCre->VcrCodi->OldValue) ?>">
<?php } ?>
<?php if ($CCre->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $CCre_list->RowCnt ?>_CCre_VcrCodi" class="form-group CCre_VcrCodi">
<select data-table="CCre" data-field="x_VcrCodi" data-value-separator="<?php echo ew_HtmlEncode(is_array($CCre->VcrCodi->DisplayValueSeparator) ? json_encode($CCre->VcrCodi->DisplayValueSeparator) : $CCre->VcrCodi->DisplayValueSeparator) ?>" id="x<?php echo $CCre_list->RowIndex ?>_VcrCodi" name="x<?php echo $CCre_list->RowIndex ?>_VcrCodi"<?php echo $CCre->VcrCodi->EditAttributes() ?>>
<?php
if (is_array($CCre->VcrCodi->EditValue)) {
	$arwrk = $CCre->VcrCodi->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($CCre->VcrCodi->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $CCre->VcrCodi->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($CCre->VcrCodi->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($CCre->VcrCodi->CurrentValue) ?>" selected><?php echo $CCre->VcrCodi->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $CCre->VcrCodi->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT \"VcrCodi\", \"VcrCodi\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"VCre\"";
$sWhereWrk = "";
$CCre->VcrCodi->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$CCre->VcrCodi->LookupFilters += array("f0" => "\"VcrCodi\" = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$CCre->Lookup_Selecting($CCre->VcrCodi, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $CCre->VcrCodi->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $CCre_list->RowIndex ?>_VcrCodi" id="s_x<?php echo $CCre_list->RowIndex ?>_VcrCodi" value="<?php echo $CCre->VcrCodi->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php if ($CCre->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $CCre_list->RowCnt ?>_CCre_VcrCodi" class="CCre_VcrCodi">
<span<?php echo $CCre->VcrCodi->ViewAttributes() ?>>
<?php echo $CCre->VcrCodi->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($CCre->CcrNCuo->Visible) { // CcrNCuo ?>
		<td data-name="CcrNCuo"<?php echo $CCre->CcrNCuo->CellAttributes() ?>>
<?php if ($CCre->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $CCre_list->RowCnt ?>_CCre_CcrNCuo" class="form-group CCre_CcrNCuo">
<input type="text" data-table="CCre" data-field="x_CcrNCuo" name="x<?php echo $CCre_list->RowIndex ?>_CcrNCuo" id="x<?php echo $CCre_list->RowIndex ?>_CcrNCuo" size="30" placeholder="<?php echo ew_HtmlEncode($CCre->CcrNCuo->getPlaceHolder()) ?>" value="<?php echo $CCre->CcrNCuo->EditValue ?>"<?php echo $CCre->CcrNCuo->EditAttributes() ?>>
</span>
<input type="hidden" data-table="CCre" data-field="x_CcrNCuo" name="o<?php echo $CCre_list->RowIndex ?>_CcrNCuo" id="o<?php echo $CCre_list->RowIndex ?>_CcrNCuo" value="<?php echo ew_HtmlEncode($CCre->CcrNCuo->OldValue) ?>">
<?php } ?>
<?php if ($CCre->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $CCre_list->RowCnt ?>_CCre_CcrNCuo" class="form-group CCre_CcrNCuo">
<input type="text" data-table="CCre" data-field="x_CcrNCuo" name="x<?php echo $CCre_list->RowIndex ?>_CcrNCuo" id="x<?php echo $CCre_list->RowIndex ?>_CcrNCuo" size="30" placeholder="<?php echo ew_HtmlEncode($CCre->CcrNCuo->getPlaceHolder()) ?>" value="<?php echo $CCre->CcrNCuo->EditValue ?>"<?php echo $CCre->CcrNCuo->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($CCre->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $CCre_list->RowCnt ?>_CCre_CcrNCuo" class="CCre_CcrNCuo">
<span<?php echo $CCre->CcrNCuo->ViewAttributes() ?>>
<?php echo $CCre->CcrNCuo->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($CCre->CcrFPag->Visible) { // CcrFPag ?>
		<td data-name="CcrFPag"<?php echo $CCre->CcrFPag->CellAttributes() ?>>
<?php if ($CCre->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $CCre_list->RowCnt ?>_CCre_CcrFPag" class="form-group CCre_CcrFPag">
<input type="text" data-table="CCre" data-field="x_CcrFPag" data-format="7" name="x<?php echo $CCre_list->RowIndex ?>_CcrFPag" id="x<?php echo $CCre_list->RowIndex ?>_CcrFPag" placeholder="<?php echo ew_HtmlEncode($CCre->CcrFPag->getPlaceHolder()) ?>" value="<?php echo $CCre->CcrFPag->EditValue ?>"<?php echo $CCre->CcrFPag->EditAttributes() ?>>
</span>
<input type="hidden" data-table="CCre" data-field="x_CcrFPag" name="o<?php echo $CCre_list->RowIndex ?>_CcrFPag" id="o<?php echo $CCre_list->RowIndex ?>_CcrFPag" value="<?php echo ew_HtmlEncode($CCre->CcrFPag->OldValue) ?>">
<?php } ?>
<?php if ($CCre->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $CCre_list->RowCnt ?>_CCre_CcrFPag" class="form-group CCre_CcrFPag">
<input type="text" data-table="CCre" data-field="x_CcrFPag" data-format="7" name="x<?php echo $CCre_list->RowIndex ?>_CcrFPag" id="x<?php echo $CCre_list->RowIndex ?>_CcrFPag" placeholder="<?php echo ew_HtmlEncode($CCre->CcrFPag->getPlaceHolder()) ?>" value="<?php echo $CCre->CcrFPag->EditValue ?>"<?php echo $CCre->CcrFPag->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($CCre->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $CCre_list->RowCnt ?>_CCre_CcrFPag" class="CCre_CcrFPag">
<span<?php echo $CCre->CcrFPag->ViewAttributes() ?>>
<?php echo $CCre->CcrFPag->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($CCre->CcrMont->Visible) { // CcrMont ?>
		<td data-name="CcrMont"<?php echo $CCre->CcrMont->CellAttributes() ?>>
<?php if ($CCre->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $CCre_list->RowCnt ?>_CCre_CcrMont" class="form-group CCre_CcrMont">
<input type="text" data-table="CCre" data-field="x_CcrMont" name="x<?php echo $CCre_list->RowIndex ?>_CcrMont" id="x<?php echo $CCre_list->RowIndex ?>_CcrMont" size="30" placeholder="<?php echo ew_HtmlEncode($CCre->CcrMont->getPlaceHolder()) ?>" value="<?php echo $CCre->CcrMont->EditValue ?>"<?php echo $CCre->CcrMont->EditAttributes() ?>>
</span>
<input type="hidden" data-table="CCre" data-field="x_CcrMont" name="o<?php echo $CCre_list->RowIndex ?>_CcrMont" id="o<?php echo $CCre_list->RowIndex ?>_CcrMont" value="<?php echo ew_HtmlEncode($CCre->CcrMont->OldValue) ?>">
<?php } ?>
<?php if ($CCre->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $CCre_list->RowCnt ?>_CCre_CcrMont" class="form-group CCre_CcrMont">
<input type="text" data-table="CCre" data-field="x_CcrMont" name="x<?php echo $CCre_list->RowIndex ?>_CcrMont" id="x<?php echo $CCre_list->RowIndex ?>_CcrMont" size="30" placeholder="<?php echo ew_HtmlEncode($CCre->CcrMont->getPlaceHolder()) ?>" value="<?php echo $CCre->CcrMont->EditValue ?>"<?php echo $CCre->CcrMont->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($CCre->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $CCre_list->RowCnt ?>_CCre_CcrMont" class="CCre_CcrMont">
<span<?php echo $CCre->CcrMont->ViewAttributes() ?>>
<?php echo $CCre->CcrMont->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($CCre->CcrMora->Visible) { // CcrMora ?>
		<td data-name="CcrMora"<?php echo $CCre->CcrMora->CellAttributes() ?>>
<?php if ($CCre->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $CCre_list->RowCnt ?>_CCre_CcrMora" class="form-group CCre_CcrMora">
<input type="text" data-table="CCre" data-field="x_CcrMora" name="x<?php echo $CCre_list->RowIndex ?>_CcrMora" id="x<?php echo $CCre_list->RowIndex ?>_CcrMora" size="30" placeholder="<?php echo ew_HtmlEncode($CCre->CcrMora->getPlaceHolder()) ?>" value="<?php echo $CCre->CcrMora->EditValue ?>"<?php echo $CCre->CcrMora->EditAttributes() ?>>
</span>
<input type="hidden" data-table="CCre" data-field="x_CcrMora" name="o<?php echo $CCre_list->RowIndex ?>_CcrMora" id="o<?php echo $CCre_list->RowIndex ?>_CcrMora" value="<?php echo ew_HtmlEncode($CCre->CcrMora->OldValue) ?>">
<?php } ?>
<?php if ($CCre->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $CCre_list->RowCnt ?>_CCre_CcrMora" class="form-group CCre_CcrMora">
<input type="text" data-table="CCre" data-field="x_CcrMora" name="x<?php echo $CCre_list->RowIndex ?>_CcrMora" id="x<?php echo $CCre_list->RowIndex ?>_CcrMora" size="30" placeholder="<?php echo ew_HtmlEncode($CCre->CcrMora->getPlaceHolder()) ?>" value="<?php echo $CCre->CcrMora->EditValue ?>"<?php echo $CCre->CcrMora->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($CCre->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $CCre_list->RowCnt ?>_CCre_CcrMora" class="CCre_CcrMora">
<span<?php echo $CCre->CcrMora->ViewAttributes() ?>>
<?php echo $CCre->CcrMora->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($CCre->CcrDAtr->Visible) { // CcrDAtr ?>
		<td data-name="CcrDAtr"<?php echo $CCre->CcrDAtr->CellAttributes() ?>>
<?php if ($CCre->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $CCre_list->RowCnt ?>_CCre_CcrDAtr" class="form-group CCre_CcrDAtr">
<input type="text" data-table="CCre" data-field="x_CcrDAtr" name="x<?php echo $CCre_list->RowIndex ?>_CcrDAtr" id="x<?php echo $CCre_list->RowIndex ?>_CcrDAtr" size="30" placeholder="<?php echo ew_HtmlEncode($CCre->CcrDAtr->getPlaceHolder()) ?>" value="<?php echo $CCre->CcrDAtr->EditValue ?>"<?php echo $CCre->CcrDAtr->EditAttributes() ?>>
</span>
<input type="hidden" data-table="CCre" data-field="x_CcrDAtr" name="o<?php echo $CCre_list->RowIndex ?>_CcrDAtr" id="o<?php echo $CCre_list->RowIndex ?>_CcrDAtr" value="<?php echo ew_HtmlEncode($CCre->CcrDAtr->OldValue) ?>">
<?php } ?>
<?php if ($CCre->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $CCre_list->RowCnt ?>_CCre_CcrDAtr" class="form-group CCre_CcrDAtr">
<input type="text" data-table="CCre" data-field="x_CcrDAtr" name="x<?php echo $CCre_list->RowIndex ?>_CcrDAtr" id="x<?php echo $CCre_list->RowIndex ?>_CcrDAtr" size="30" placeholder="<?php echo ew_HtmlEncode($CCre->CcrDAtr->getPlaceHolder()) ?>" value="<?php echo $CCre->CcrDAtr->EditValue ?>"<?php echo $CCre->CcrDAtr->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($CCre->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $CCre_list->RowCnt ?>_CCre_CcrDAtr" class="CCre_CcrDAtr">
<span<?php echo $CCre->CcrDAtr->ViewAttributes() ?>>
<?php echo $CCre->CcrDAtr->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($CCre->CcrEsta->Visible) { // CcrEsta ?>
		<td data-name="CcrEsta"<?php echo $CCre->CcrEsta->CellAttributes() ?>>
<?php if ($CCre->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $CCre_list->RowCnt ?>_CCre_CcrEsta" class="form-group CCre_CcrEsta">
<input type="text" data-table="CCre" data-field="x_CcrEsta" name="x<?php echo $CCre_list->RowIndex ?>_CcrEsta" id="x<?php echo $CCre_list->RowIndex ?>_CcrEsta" size="30" maxlength="1" placeholder="<?php echo ew_HtmlEncode($CCre->CcrEsta->getPlaceHolder()) ?>" value="<?php echo $CCre->CcrEsta->EditValue ?>"<?php echo $CCre->CcrEsta->EditAttributes() ?>>
</span>
<input type="hidden" data-table="CCre" data-field="x_CcrEsta" name="o<?php echo $CCre_list->RowIndex ?>_CcrEsta" id="o<?php echo $CCre_list->RowIndex ?>_CcrEsta" value="<?php echo ew_HtmlEncode($CCre->CcrEsta->OldValue) ?>">
<?php } ?>
<?php if ($CCre->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $CCre_list->RowCnt ?>_CCre_CcrEsta" class="form-group CCre_CcrEsta">
<input type="text" data-table="CCre" data-field="x_CcrEsta" name="x<?php echo $CCre_list->RowIndex ?>_CcrEsta" id="x<?php echo $CCre_list->RowIndex ?>_CcrEsta" size="30" maxlength="1" placeholder="<?php echo ew_HtmlEncode($CCre->CcrEsta->getPlaceHolder()) ?>" value="<?php echo $CCre->CcrEsta->EditValue ?>"<?php echo $CCre->CcrEsta->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($CCre->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $CCre_list->RowCnt ?>_CCre_CcrEsta" class="CCre_CcrEsta">
<span<?php echo $CCre->CcrEsta->ViewAttributes() ?>>
<?php echo $CCre->CcrEsta->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($CCre->CcrAnho->Visible) { // CcrAnho ?>
		<td data-name="CcrAnho"<?php echo $CCre->CcrAnho->CellAttributes() ?>>
<?php if ($CCre->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $CCre_list->RowCnt ?>_CCre_CcrAnho" class="form-group CCre_CcrAnho">
<input type="text" data-table="CCre" data-field="x_CcrAnho" name="x<?php echo $CCre_list->RowIndex ?>_CcrAnho" id="x<?php echo $CCre_list->RowIndex ?>_CcrAnho" size="30" maxlength="4" placeholder="<?php echo ew_HtmlEncode($CCre->CcrAnho->getPlaceHolder()) ?>" value="<?php echo $CCre->CcrAnho->EditValue ?>"<?php echo $CCre->CcrAnho->EditAttributes() ?>>
</span>
<input type="hidden" data-table="CCre" data-field="x_CcrAnho" name="o<?php echo $CCre_list->RowIndex ?>_CcrAnho" id="o<?php echo $CCre_list->RowIndex ?>_CcrAnho" value="<?php echo ew_HtmlEncode($CCre->CcrAnho->OldValue) ?>">
<?php } ?>
<?php if ($CCre->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $CCre_list->RowCnt ?>_CCre_CcrAnho" class="form-group CCre_CcrAnho">
<input type="text" data-table="CCre" data-field="x_CcrAnho" name="x<?php echo $CCre_list->RowIndex ?>_CcrAnho" id="x<?php echo $CCre_list->RowIndex ?>_CcrAnho" size="30" maxlength="4" placeholder="<?php echo ew_HtmlEncode($CCre->CcrAnho->getPlaceHolder()) ?>" value="<?php echo $CCre->CcrAnho->EditValue ?>"<?php echo $CCre->CcrAnho->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($CCre->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $CCre_list->RowCnt ?>_CCre_CcrAnho" class="CCre_CcrAnho">
<span<?php echo $CCre->CcrAnho->ViewAttributes() ?>>
<?php echo $CCre->CcrAnho->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($CCre->CcrMes->Visible) { // CcrMes ?>
		<td data-name="CcrMes"<?php echo $CCre->CcrMes->CellAttributes() ?>>
<?php if ($CCre->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $CCre_list->RowCnt ?>_CCre_CcrMes" class="form-group CCre_CcrMes">
<input type="text" data-table="CCre" data-field="x_CcrMes" name="x<?php echo $CCre_list->RowIndex ?>_CcrMes" id="x<?php echo $CCre_list->RowIndex ?>_CcrMes" size="30" maxlength="2" placeholder="<?php echo ew_HtmlEncode($CCre->CcrMes->getPlaceHolder()) ?>" value="<?php echo $CCre->CcrMes->EditValue ?>"<?php echo $CCre->CcrMes->EditAttributes() ?>>
</span>
<input type="hidden" data-table="CCre" data-field="x_CcrMes" name="o<?php echo $CCre_list->RowIndex ?>_CcrMes" id="o<?php echo $CCre_list->RowIndex ?>_CcrMes" value="<?php echo ew_HtmlEncode($CCre->CcrMes->OldValue) ?>">
<?php } ?>
<?php if ($CCre->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $CCre_list->RowCnt ?>_CCre_CcrMes" class="form-group CCre_CcrMes">
<input type="text" data-table="CCre" data-field="x_CcrMes" name="x<?php echo $CCre_list->RowIndex ?>_CcrMes" id="x<?php echo $CCre_list->RowIndex ?>_CcrMes" size="30" maxlength="2" placeholder="<?php echo ew_HtmlEncode($CCre->CcrMes->getPlaceHolder()) ?>" value="<?php echo $CCre->CcrMes->EditValue ?>"<?php echo $CCre->CcrMes->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($CCre->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $CCre_list->RowCnt ?>_CCre_CcrMes" class="CCre_CcrMes">
<span<?php echo $CCre->CcrMes->ViewAttributes() ?>>
<?php echo $CCre->CcrMes->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($CCre->CcrConc->Visible) { // CcrConc ?>
		<td data-name="CcrConc"<?php echo $CCre->CcrConc->CellAttributes() ?>>
<?php if ($CCre->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $CCre_list->RowCnt ?>_CCre_CcrConc" class="form-group CCre_CcrConc">
<input type="text" data-table="CCre" data-field="x_CcrConc" name="x<?php echo $CCre_list->RowIndex ?>_CcrConc" id="x<?php echo $CCre_list->RowIndex ?>_CcrConc" size="30" maxlength="25" placeholder="<?php echo ew_HtmlEncode($CCre->CcrConc->getPlaceHolder()) ?>" value="<?php echo $CCre->CcrConc->EditValue ?>"<?php echo $CCre->CcrConc->EditAttributes() ?>>
</span>
<input type="hidden" data-table="CCre" data-field="x_CcrConc" name="o<?php echo $CCre_list->RowIndex ?>_CcrConc" id="o<?php echo $CCre_list->RowIndex ?>_CcrConc" value="<?php echo ew_HtmlEncode($CCre->CcrConc->OldValue) ?>">
<?php } ?>
<?php if ($CCre->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $CCre_list->RowCnt ?>_CCre_CcrConc" class="form-group CCre_CcrConc">
<input type="text" data-table="CCre" data-field="x_CcrConc" name="x<?php echo $CCre_list->RowIndex ?>_CcrConc" id="x<?php echo $CCre_list->RowIndex ?>_CcrConc" size="30" maxlength="25" placeholder="<?php echo ew_HtmlEncode($CCre->CcrConc->getPlaceHolder()) ?>" value="<?php echo $CCre->CcrConc->EditValue ?>"<?php echo $CCre->CcrConc->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($CCre->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $CCre_list->RowCnt ?>_CCre_CcrConc" class="CCre_CcrConc">
<span<?php echo $CCre->CcrConc->ViewAttributes() ?>>
<?php echo $CCre->CcrConc->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($CCre->CcrUsua->Visible) { // CcrUsua ?>
		<td data-name="CcrUsua"<?php echo $CCre->CcrUsua->CellAttributes() ?>>
<?php if ($CCre->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $CCre_list->RowCnt ?>_CCre_CcrUsua" class="form-group CCre_CcrUsua">
<input type="text" data-table="CCre" data-field="x_CcrUsua" name="x<?php echo $CCre_list->RowIndex ?>_CcrUsua" id="x<?php echo $CCre_list->RowIndex ?>_CcrUsua" size="30" maxlength="50" placeholder="<?php echo ew_HtmlEncode($CCre->CcrUsua->getPlaceHolder()) ?>" value="<?php echo $CCre->CcrUsua->EditValue ?>"<?php echo $CCre->CcrUsua->EditAttributes() ?>>
</span>
<input type="hidden" data-table="CCre" data-field="x_CcrUsua" name="o<?php echo $CCre_list->RowIndex ?>_CcrUsua" id="o<?php echo $CCre_list->RowIndex ?>_CcrUsua" value="<?php echo ew_HtmlEncode($CCre->CcrUsua->OldValue) ?>">
<?php } ?>
<?php if ($CCre->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $CCre_list->RowCnt ?>_CCre_CcrUsua" class="form-group CCre_CcrUsua">
<input type="text" data-table="CCre" data-field="x_CcrUsua" name="x<?php echo $CCre_list->RowIndex ?>_CcrUsua" id="x<?php echo $CCre_list->RowIndex ?>_CcrUsua" size="30" maxlength="50" placeholder="<?php echo ew_HtmlEncode($CCre->CcrUsua->getPlaceHolder()) ?>" value="<?php echo $CCre->CcrUsua->EditValue ?>"<?php echo $CCre->CcrUsua->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($CCre->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $CCre_list->RowCnt ?>_CCre_CcrUsua" class="CCre_CcrUsua">
<span<?php echo $CCre->CcrUsua->ViewAttributes() ?>>
<?php echo $CCre->CcrUsua->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($CCre->CcrFCre->Visible) { // CcrFCre ?>
		<td data-name="CcrFCre"<?php echo $CCre->CcrFCre->CellAttributes() ?>>
<?php if ($CCre->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $CCre_list->RowCnt ?>_CCre_CcrFCre" class="form-group CCre_CcrFCre">
<input type="text" data-table="CCre" data-field="x_CcrFCre" data-format="7" name="x<?php echo $CCre_list->RowIndex ?>_CcrFCre" id="x<?php echo $CCre_list->RowIndex ?>_CcrFCre" placeholder="<?php echo ew_HtmlEncode($CCre->CcrFCre->getPlaceHolder()) ?>" value="<?php echo $CCre->CcrFCre->EditValue ?>"<?php echo $CCre->CcrFCre->EditAttributes() ?>>
</span>
<input type="hidden" data-table="CCre" data-field="x_CcrFCre" name="o<?php echo $CCre_list->RowIndex ?>_CcrFCre" id="o<?php echo $CCre_list->RowIndex ?>_CcrFCre" value="<?php echo ew_HtmlEncode($CCre->CcrFCre->OldValue) ?>">
<?php } ?>
<?php if ($CCre->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $CCre_list->RowCnt ?>_CCre_CcrFCre" class="form-group CCre_CcrFCre">
<input type="text" data-table="CCre" data-field="x_CcrFCre" data-format="7" name="x<?php echo $CCre_list->RowIndex ?>_CcrFCre" id="x<?php echo $CCre_list->RowIndex ?>_CcrFCre" placeholder="<?php echo ew_HtmlEncode($CCre->CcrFCre->getPlaceHolder()) ?>" value="<?php echo $CCre->CcrFCre->EditValue ?>"<?php echo $CCre->CcrFCre->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($CCre->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $CCre_list->RowCnt ?>_CCre_CcrFCre" class="CCre_CcrFCre">
<span<?php echo $CCre->CcrFCre->ViewAttributes() ?>>
<?php echo $CCre->CcrFCre->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
<?php

// Render list options (body, right)
$CCre_list->ListOptions->Render("body", "right", $CCre_list->RowCnt);
?>
	</tr>
<?php if ($CCre->RowType == EW_ROWTYPE_ADD || $CCre->RowType == EW_ROWTYPE_EDIT) { ?>
<script type="text/javascript">
fCCrelist.UpdateOpts(<?php echo $CCre_list->RowIndex ?>);
</script>
<?php } ?>
<?php
	}
	} // End delete row checking
	if ($CCre->CurrentAction <> "gridadd")
		if (!$CCre_list->Recordset->EOF) $CCre_list->Recordset->MoveNext();
}
?>
<?php
	if ($CCre->CurrentAction == "gridadd" || $CCre->CurrentAction == "gridedit") {
		$CCre_list->RowIndex = '$rowindex$';
		$CCre_list->LoadDefaultValues();

		// Set row properties
		$CCre->ResetAttrs();
		$CCre->RowAttrs = array_merge($CCre->RowAttrs, array('data-rowindex'=>$CCre_list->RowIndex, 'id'=>'r0_CCre', 'data-rowtype'=>EW_ROWTYPE_ADD));
		ew_AppendClass($CCre->RowAttrs["class"], "ewTemplate");
		$CCre->RowType = EW_ROWTYPE_ADD;

		// Render row
		$CCre_list->RenderRow();

		// Render list options
		$CCre_list->RenderListOptions();
		$CCre_list->StartRowCnt = 0;
?>
	<tr<?php echo $CCre->RowAttributes() ?>>
<?php

// Render list options (body, left)
$CCre_list->ListOptions->Render("body", "left", $CCre_list->RowIndex);
?>
	<?php if ($CCre->CcrCodi->Visible) { // CcrCodi ?>
		<td data-name="CcrCodi">
<input type="hidden" data-table="CCre" data-field="x_CcrCodi" name="o<?php echo $CCre_list->RowIndex ?>_CcrCodi" id="o<?php echo $CCre_list->RowIndex ?>_CcrCodi" value="<?php echo ew_HtmlEncode($CCre->CcrCodi->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($CCre->VcrCodi->Visible) { // VcrCodi ?>
		<td data-name="VcrCodi">
<span id="el$rowindex$_CCre_VcrCodi" class="form-group CCre_VcrCodi">
<select data-table="CCre" data-field="x_VcrCodi" data-value-separator="<?php echo ew_HtmlEncode(is_array($CCre->VcrCodi->DisplayValueSeparator) ? json_encode($CCre->VcrCodi->DisplayValueSeparator) : $CCre->VcrCodi->DisplayValueSeparator) ?>" id="x<?php echo $CCre_list->RowIndex ?>_VcrCodi" name="x<?php echo $CCre_list->RowIndex ?>_VcrCodi"<?php echo $CCre->VcrCodi->EditAttributes() ?>>
<?php
if (is_array($CCre->VcrCodi->EditValue)) {
	$arwrk = $CCre->VcrCodi->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($CCre->VcrCodi->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $CCre->VcrCodi->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($CCre->VcrCodi->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($CCre->VcrCodi->CurrentValue) ?>" selected><?php echo $CCre->VcrCodi->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $CCre->VcrCodi->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT \"VcrCodi\", \"VcrCodi\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"VCre\"";
$sWhereWrk = "";
$CCre->VcrCodi->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$CCre->VcrCodi->LookupFilters += array("f0" => "\"VcrCodi\" = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$CCre->Lookup_Selecting($CCre->VcrCodi, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $CCre->VcrCodi->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $CCre_list->RowIndex ?>_VcrCodi" id="s_x<?php echo $CCre_list->RowIndex ?>_VcrCodi" value="<?php echo $CCre->VcrCodi->LookupFilterQuery() ?>">
</span>
<input type="hidden" data-table="CCre" data-field="x_VcrCodi" name="o<?php echo $CCre_list->RowIndex ?>_VcrCodi" id="o<?php echo $CCre_list->RowIndex ?>_VcrCodi" value="<?php echo ew_HtmlEncode($CCre->VcrCodi->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($CCre->CcrNCuo->Visible) { // CcrNCuo ?>
		<td data-name="CcrNCuo">
<span id="el$rowindex$_CCre_CcrNCuo" class="form-group CCre_CcrNCuo">
<input type="text" data-table="CCre" data-field="x_CcrNCuo" name="x<?php echo $CCre_list->RowIndex ?>_CcrNCuo" id="x<?php echo $CCre_list->RowIndex ?>_CcrNCuo" size="30" placeholder="<?php echo ew_HtmlEncode($CCre->CcrNCuo->getPlaceHolder()) ?>" value="<?php echo $CCre->CcrNCuo->EditValue ?>"<?php echo $CCre->CcrNCuo->EditAttributes() ?>>
</span>
<input type="hidden" data-table="CCre" data-field="x_CcrNCuo" name="o<?php echo $CCre_list->RowIndex ?>_CcrNCuo" id="o<?php echo $CCre_list->RowIndex ?>_CcrNCuo" value="<?php echo ew_HtmlEncode($CCre->CcrNCuo->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($CCre->CcrFPag->Visible) { // CcrFPag ?>
		<td data-name="CcrFPag">
<span id="el$rowindex$_CCre_CcrFPag" class="form-group CCre_CcrFPag">
<input type="text" data-table="CCre" data-field="x_CcrFPag" data-format="7" name="x<?php echo $CCre_list->RowIndex ?>_CcrFPag" id="x<?php echo $CCre_list->RowIndex ?>_CcrFPag" placeholder="<?php echo ew_HtmlEncode($CCre->CcrFPag->getPlaceHolder()) ?>" value="<?php echo $CCre->CcrFPag->EditValue ?>"<?php echo $CCre->CcrFPag->EditAttributes() ?>>
</span>
<input type="hidden" data-table="CCre" data-field="x_CcrFPag" name="o<?php echo $CCre_list->RowIndex ?>_CcrFPag" id="o<?php echo $CCre_list->RowIndex ?>_CcrFPag" value="<?php echo ew_HtmlEncode($CCre->CcrFPag->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($CCre->CcrMont->Visible) { // CcrMont ?>
		<td data-name="CcrMont">
<span id="el$rowindex$_CCre_CcrMont" class="form-group CCre_CcrMont">
<input type="text" data-table="CCre" data-field="x_CcrMont" name="x<?php echo $CCre_list->RowIndex ?>_CcrMont" id="x<?php echo $CCre_list->RowIndex ?>_CcrMont" size="30" placeholder="<?php echo ew_HtmlEncode($CCre->CcrMont->getPlaceHolder()) ?>" value="<?php echo $CCre->CcrMont->EditValue ?>"<?php echo $CCre->CcrMont->EditAttributes() ?>>
</span>
<input type="hidden" data-table="CCre" data-field="x_CcrMont" name="o<?php echo $CCre_list->RowIndex ?>_CcrMont" id="o<?php echo $CCre_list->RowIndex ?>_CcrMont" value="<?php echo ew_HtmlEncode($CCre->CcrMont->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($CCre->CcrMora->Visible) { // CcrMora ?>
		<td data-name="CcrMora">
<span id="el$rowindex$_CCre_CcrMora" class="form-group CCre_CcrMora">
<input type="text" data-table="CCre" data-field="x_CcrMora" name="x<?php echo $CCre_list->RowIndex ?>_CcrMora" id="x<?php echo $CCre_list->RowIndex ?>_CcrMora" size="30" placeholder="<?php echo ew_HtmlEncode($CCre->CcrMora->getPlaceHolder()) ?>" value="<?php echo $CCre->CcrMora->EditValue ?>"<?php echo $CCre->CcrMora->EditAttributes() ?>>
</span>
<input type="hidden" data-table="CCre" data-field="x_CcrMora" name="o<?php echo $CCre_list->RowIndex ?>_CcrMora" id="o<?php echo $CCre_list->RowIndex ?>_CcrMora" value="<?php echo ew_HtmlEncode($CCre->CcrMora->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($CCre->CcrDAtr->Visible) { // CcrDAtr ?>
		<td data-name="CcrDAtr">
<span id="el$rowindex$_CCre_CcrDAtr" class="form-group CCre_CcrDAtr">
<input type="text" data-table="CCre" data-field="x_CcrDAtr" name="x<?php echo $CCre_list->RowIndex ?>_CcrDAtr" id="x<?php echo $CCre_list->RowIndex ?>_CcrDAtr" size="30" placeholder="<?php echo ew_HtmlEncode($CCre->CcrDAtr->getPlaceHolder()) ?>" value="<?php echo $CCre->CcrDAtr->EditValue ?>"<?php echo $CCre->CcrDAtr->EditAttributes() ?>>
</span>
<input type="hidden" data-table="CCre" data-field="x_CcrDAtr" name="o<?php echo $CCre_list->RowIndex ?>_CcrDAtr" id="o<?php echo $CCre_list->RowIndex ?>_CcrDAtr" value="<?php echo ew_HtmlEncode($CCre->CcrDAtr->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($CCre->CcrEsta->Visible) { // CcrEsta ?>
		<td data-name="CcrEsta">
<span id="el$rowindex$_CCre_CcrEsta" class="form-group CCre_CcrEsta">
<input type="text" data-table="CCre" data-field="x_CcrEsta" name="x<?php echo $CCre_list->RowIndex ?>_CcrEsta" id="x<?php echo $CCre_list->RowIndex ?>_CcrEsta" size="30" maxlength="1" placeholder="<?php echo ew_HtmlEncode($CCre->CcrEsta->getPlaceHolder()) ?>" value="<?php echo $CCre->CcrEsta->EditValue ?>"<?php echo $CCre->CcrEsta->EditAttributes() ?>>
</span>
<input type="hidden" data-table="CCre" data-field="x_CcrEsta" name="o<?php echo $CCre_list->RowIndex ?>_CcrEsta" id="o<?php echo $CCre_list->RowIndex ?>_CcrEsta" value="<?php echo ew_HtmlEncode($CCre->CcrEsta->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($CCre->CcrAnho->Visible) { // CcrAnho ?>
		<td data-name="CcrAnho">
<span id="el$rowindex$_CCre_CcrAnho" class="form-group CCre_CcrAnho">
<input type="text" data-table="CCre" data-field="x_CcrAnho" name="x<?php echo $CCre_list->RowIndex ?>_CcrAnho" id="x<?php echo $CCre_list->RowIndex ?>_CcrAnho" size="30" maxlength="4" placeholder="<?php echo ew_HtmlEncode($CCre->CcrAnho->getPlaceHolder()) ?>" value="<?php echo $CCre->CcrAnho->EditValue ?>"<?php echo $CCre->CcrAnho->EditAttributes() ?>>
</span>
<input type="hidden" data-table="CCre" data-field="x_CcrAnho" name="o<?php echo $CCre_list->RowIndex ?>_CcrAnho" id="o<?php echo $CCre_list->RowIndex ?>_CcrAnho" value="<?php echo ew_HtmlEncode($CCre->CcrAnho->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($CCre->CcrMes->Visible) { // CcrMes ?>
		<td data-name="CcrMes">
<span id="el$rowindex$_CCre_CcrMes" class="form-group CCre_CcrMes">
<input type="text" data-table="CCre" data-field="x_CcrMes" name="x<?php echo $CCre_list->RowIndex ?>_CcrMes" id="x<?php echo $CCre_list->RowIndex ?>_CcrMes" size="30" maxlength="2" placeholder="<?php echo ew_HtmlEncode($CCre->CcrMes->getPlaceHolder()) ?>" value="<?php echo $CCre->CcrMes->EditValue ?>"<?php echo $CCre->CcrMes->EditAttributes() ?>>
</span>
<input type="hidden" data-table="CCre" data-field="x_CcrMes" name="o<?php echo $CCre_list->RowIndex ?>_CcrMes" id="o<?php echo $CCre_list->RowIndex ?>_CcrMes" value="<?php echo ew_HtmlEncode($CCre->CcrMes->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($CCre->CcrConc->Visible) { // CcrConc ?>
		<td data-name="CcrConc">
<span id="el$rowindex$_CCre_CcrConc" class="form-group CCre_CcrConc">
<input type="text" data-table="CCre" data-field="x_CcrConc" name="x<?php echo $CCre_list->RowIndex ?>_CcrConc" id="x<?php echo $CCre_list->RowIndex ?>_CcrConc" size="30" maxlength="25" placeholder="<?php echo ew_HtmlEncode($CCre->CcrConc->getPlaceHolder()) ?>" value="<?php echo $CCre->CcrConc->EditValue ?>"<?php echo $CCre->CcrConc->EditAttributes() ?>>
</span>
<input type="hidden" data-table="CCre" data-field="x_CcrConc" name="o<?php echo $CCre_list->RowIndex ?>_CcrConc" id="o<?php echo $CCre_list->RowIndex ?>_CcrConc" value="<?php echo ew_HtmlEncode($CCre->CcrConc->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($CCre->CcrUsua->Visible) { // CcrUsua ?>
		<td data-name="CcrUsua">
<span id="el$rowindex$_CCre_CcrUsua" class="form-group CCre_CcrUsua">
<input type="text" data-table="CCre" data-field="x_CcrUsua" name="x<?php echo $CCre_list->RowIndex ?>_CcrUsua" id="x<?php echo $CCre_list->RowIndex ?>_CcrUsua" size="30" maxlength="50" placeholder="<?php echo ew_HtmlEncode($CCre->CcrUsua->getPlaceHolder()) ?>" value="<?php echo $CCre->CcrUsua->EditValue ?>"<?php echo $CCre->CcrUsua->EditAttributes() ?>>
</span>
<input type="hidden" data-table="CCre" data-field="x_CcrUsua" name="o<?php echo $CCre_list->RowIndex ?>_CcrUsua" id="o<?php echo $CCre_list->RowIndex ?>_CcrUsua" value="<?php echo ew_HtmlEncode($CCre->CcrUsua->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($CCre->CcrFCre->Visible) { // CcrFCre ?>
		<td data-name="CcrFCre">
<span id="el$rowindex$_CCre_CcrFCre" class="form-group CCre_CcrFCre">
<input type="text" data-table="CCre" data-field="x_CcrFCre" data-format="7" name="x<?php echo $CCre_list->RowIndex ?>_CcrFCre" id="x<?php echo $CCre_list->RowIndex ?>_CcrFCre" placeholder="<?php echo ew_HtmlEncode($CCre->CcrFCre->getPlaceHolder()) ?>" value="<?php echo $CCre->CcrFCre->EditValue ?>"<?php echo $CCre->CcrFCre->EditAttributes() ?>>
</span>
<input type="hidden" data-table="CCre" data-field="x_CcrFCre" name="o<?php echo $CCre_list->RowIndex ?>_CcrFCre" id="o<?php echo $CCre_list->RowIndex ?>_CcrFCre" value="<?php echo ew_HtmlEncode($CCre->CcrFCre->OldValue) ?>">
</td>
	<?php } ?>
<?php

// Render list options (body, right)
$CCre_list->ListOptions->Render("body", "right", $CCre_list->RowCnt);
?>
<script type="text/javascript">
fCCrelist.UpdateOpts(<?php echo $CCre_list->RowIndex ?>);
</script>
	</tr>
<?php
}
?>
</tbody>
</table>
<?php } ?>
<?php if ($CCre->CurrentAction == "gridadd") { ?>
<input type="hidden" name="a_list" id="a_list" value="gridinsert">
<input type="hidden" name="<?php echo $CCre_list->FormKeyCountName ?>" id="<?php echo $CCre_list->FormKeyCountName ?>" value="<?php echo $CCre_list->KeyCount ?>">
<?php echo $CCre_list->MultiSelectKey ?>
<?php } ?>
<?php if ($CCre->CurrentAction == "gridedit") { ?>
<input type="hidden" name="a_list" id="a_list" value="gridupdate">
<input type="hidden" name="<?php echo $CCre_list->FormKeyCountName ?>" id="<?php echo $CCre_list->FormKeyCountName ?>" value="<?php echo $CCre_list->KeyCount ?>">
<?php echo $CCre_list->MultiSelectKey ?>
<?php } ?>
<?php if ($CCre->CurrentAction == "") { ?>
<input type="hidden" name="a_list" id="a_list" value="">
<?php } ?>
</div>
</form>
<?php

// Close recordset
if ($CCre_list->Recordset)
	$CCre_list->Recordset->Close();
?>
<div class="panel-footer ewGridLowerPanel">
<?php if ($CCre->CurrentAction <> "gridadd" && $CCre->CurrentAction <> "gridedit") { ?>
<form name="ewPagerForm" class="ewForm form-inline ewPagerForm" action="<?php echo ew_CurrentPage() ?>">
<?php if (!isset($CCre_list->Pager)) $CCre_list->Pager = new cPrevNextPager($CCre_list->StartRec, $CCre_list->DisplayRecs, $CCre_list->TotalRecs) ?>
<?php if ($CCre_list->Pager->RecordCount > 0) { ?>
<div class="ewPager">
<span><?php echo $Language->Phrase("Page") ?>&nbsp;</span>
<div class="ewPrevNext"><div class="input-group">
<div class="input-group-btn">
<!--first page button-->
	<?php if ($CCre_list->Pager->FirstButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerFirst") ?>" href="<?php echo $CCre_list->PageUrl() ?>start=<?php echo $CCre_list->Pager->FirstButton->Start ?>"><span class="icon-first ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerFirst") ?>"><span class="icon-first ewIcon"></span></a>
	<?php } ?>
<!--previous page button-->
	<?php if ($CCre_list->Pager->PrevButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerPrevious") ?>" href="<?php echo $CCre_list->PageUrl() ?>start=<?php echo $CCre_list->Pager->PrevButton->Start ?>"><span class="icon-prev ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerPrevious") ?>"><span class="icon-prev ewIcon"></span></a>
	<?php } ?>
</div>
<!--current page number-->
	<input class="form-control input-sm" type="text" name="<?php echo EW_TABLE_PAGE_NO ?>" value="<?php echo $CCre_list->Pager->CurrentPage ?>">
<div class="input-group-btn">
<!--next page button-->
	<?php if ($CCre_list->Pager->NextButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerNext") ?>" href="<?php echo $CCre_list->PageUrl() ?>start=<?php echo $CCre_list->Pager->NextButton->Start ?>"><span class="icon-next ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerNext") ?>"><span class="icon-next ewIcon"></span></a>
	<?php } ?>
<!--last page button-->
	<?php if ($CCre_list->Pager->LastButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerLast") ?>" href="<?php echo $CCre_list->PageUrl() ?>start=<?php echo $CCre_list->Pager->LastButton->Start ?>"><span class="icon-last ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerLast") ?>"><span class="icon-last ewIcon"></span></a>
	<?php } ?>
</div>
</div>
</div>
<span>&nbsp;<?php echo $Language->Phrase("of") ?>&nbsp;<?php echo $CCre_list->Pager->PageCount ?></span>
</div>
<div class="ewPager ewRec">
	<span><?php echo $Language->Phrase("Record") ?>&nbsp;<?php echo $CCre_list->Pager->FromIndex ?>&nbsp;<?php echo $Language->Phrase("To") ?>&nbsp;<?php echo $CCre_list->Pager->ToIndex ?>&nbsp;<?php echo $Language->Phrase("Of") ?>&nbsp;<?php echo $CCre_list->Pager->RecordCount ?></span>
</div>
<?php } ?>
</form>
<?php } ?>
<div class="ewListOtherOptions">
<?php
	foreach ($CCre_list->OtherOptions as &$option)
		$option->Render("body", "bottom");
?>
</div>
<div class="clearfix"></div>
</div>
</div>
<?php } ?>
<?php if ($CCre_list->TotalRecs == 0 && $CCre->CurrentAction == "") { // Show other options ?>
<div class="ewListOtherOptions">
<?php
	foreach ($CCre_list->OtherOptions as &$option) {
		$option->ButtonClass = "";
		$option->Render("body", "");
	}
?>
</div>
<div class="clearfix"></div>
<?php } ?>
<script type="text/javascript">
fCCrelistsrch.Init();
fCCrelistsrch.FilterList = <?php echo $CCre_list->GetFilterList() ?>;
fCCrelist.Init();
</script>
<?php
$CCre_list->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$CCre_list->Page_Terminate();
?>
