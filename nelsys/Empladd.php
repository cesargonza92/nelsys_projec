<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "Emplinfo.php" ?>
<?php include_once "Usuainfo.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$Empl_add = NULL; // Initialize page object first

class cEmpl_add extends cEmpl {

	// Page ID
	var $PageID = 'add';

	// Project ID
	var $ProjectID = "{04439FF7-B43F-460F-8514-F71C8FF9E679}";

	// Table name
	var $TableName = 'Empl';

	// Page object name
	var $PageObjName = 'Empl_add';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (Empl)
		if (!isset($GLOBALS["Empl"]) || get_class($GLOBALS["Empl"]) == "cEmpl") {
			$GLOBALS["Empl"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["Empl"];
		}

		// Table object (Usua)
		if (!isset($GLOBALS['Usua'])) $GLOBALS['Usua'] = new cUsua();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'add', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'Empl', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (Usua)
		if (!isset($UserTable)) {
			$UserTable = new cUsua();
			$UserTableConn = Conn($UserTable->DBID);
		}
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanAdd()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage($Language->Phrase("NoPermission")); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("Empllist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}

		// Create form object
		$objForm = new cFormObj();
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Process auto fill
		if (@$_POST["ajax"] == "autofill") {
			$results = $this->GetAutoFill(@$_POST["name"], @$_POST["q"]);
			if ($results) {

				// Clean output buffer
				if (!EW_DEBUG_ENABLED && ob_get_length())
					ob_end_clean();
				echo $results;
				$this->Page_Terminate();
				exit();
			}
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $Empl;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($Empl);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $FormClassName = "form-horizontal ewForm ewAddForm";
	var $DbMasterFilter = "";
	var $DbDetailFilter = "";
	var $StartRec;
	var $Priv = 0;
	var $OldRecordset;
	var $CopyRecord;

	// 
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError;

		// Process form if post back
		if (@$_POST["a_add"] <> "") {
			$this->CurrentAction = $_POST["a_add"]; // Get form action
			$this->CopyRecord = $this->LoadOldRecord(); // Load old recordset
			$this->LoadFormValues(); // Load form values
		} else { // Not post back

			// Load key values from QueryString
			$this->CopyRecord = TRUE;
			if (@$_GET["EmpCodi"] != "") {
				$this->EmpCodi->setQueryStringValue($_GET["EmpCodi"]);
				$this->setKey("EmpCodi", $this->EmpCodi->CurrentValue); // Set up key
			} else {
				$this->setKey("EmpCodi", ""); // Clear key
				$this->CopyRecord = FALSE;
			}
			if ($this->CopyRecord) {
				$this->CurrentAction = "C"; // Copy record
			} else {
				$this->CurrentAction = "I"; // Display blank record
				$this->LoadDefaultValues(); // Load default values
			}
		}

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Validate form if post back
		if (@$_POST["a_add"] <> "") {
			if (!$this->ValidateForm()) {
				$this->CurrentAction = "I"; // Form error, reset action
				$this->EventCancelled = TRUE; // Event cancelled
				$this->RestoreFormValues(); // Restore form values
				$this->setFailureMessage($gsFormError);
			}
		}

		// Perform action based on action code
		switch ($this->CurrentAction) {
			case "I": // Blank record, no action required
				break;
			case "C": // Copy an existing record
				if (!$this->LoadRow()) { // Load record based on key
					if ($this->getFailureMessage() == "") $this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
					$this->Page_Terminate("Empllist.php"); // No matching record, return to list
				}
				break;
			case "A": // Add new record
				$this->SendEmail = TRUE; // Send email on add success
				if ($this->AddRow($this->OldRecordset)) { // Add successful
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage($Language->Phrase("AddSuccess")); // Set up success message
					$sReturnUrl = $this->getReturnUrl();
					if (ew_GetPageName($sReturnUrl) == "Emplview.php")
						$sReturnUrl = $this->GetViewUrl(); // View paging, return to view page with keyurl directly
					$this->Page_Terminate($sReturnUrl); // Clean up and return
				} else {
					$this->EventCancelled = TRUE; // Event cancelled
					$this->RestoreFormValues(); // Add failed, restore form values
				}
		}

		// Render row based on row type
		if ($this->CurrentAction == "F") { // Confirm page
			$this->RowType = EW_ROWTYPE_VIEW; // Render view type
		} else {
			$this->RowType = EW_ROWTYPE_ADD; // Render add type
		}

		// Render row
		$this->ResetAttrs();
		$this->RenderRow();
	}

	// Get upload files
	function GetUploadFiles() {
		global $objForm, $Language;

		// Get upload data
	}

	// Load default values
	function LoadDefaultValues() {
		$this->EmpCarg->CurrentValue = NULL;
		$this->EmpCarg->OldValue = $this->EmpCarg->CurrentValue;
		$this->EmpFIng->CurrentValue = NULL;
		$this->EmpFIng->OldValue = $this->EmpFIng->CurrentValue;
		$this->EmpFSal->CurrentValue = NULL;
		$this->EmpFSal->OldValue = $this->EmpFSal->CurrentValue;
		$this->EmpSala->CurrentValue = NULL;
		$this->EmpSala->OldValue = $this->EmpSala->CurrentValue;
		$this->EmpNomb->CurrentValue = NULL;
		$this->EmpNomb->OldValue = $this->EmpNomb->CurrentValue;
		$this->EmpApel->CurrentValue = NULL;
		$this->EmpApel->OldValue = $this->EmpApel->CurrentValue;
		$this->EmpCedu->CurrentValue = NULL;
		$this->EmpCedu->OldValue = $this->EmpCedu->CurrentValue;
		$this->EmpTele->CurrentValue = NULL;
		$this->EmpTele->OldValue = $this->EmpTele->CurrentValue;
	}

	// Load form values
	function LoadFormValues() {

		// Load from form
		global $objForm;
		if (!$this->EmpCarg->FldIsDetailKey) {
			$this->EmpCarg->setFormValue($objForm->GetValue("x_EmpCarg"));
		}
		if (!$this->EmpFIng->FldIsDetailKey) {
			$this->EmpFIng->setFormValue($objForm->GetValue("x_EmpFIng"));
			$this->EmpFIng->CurrentValue = ew_UnFormatDateTime($this->EmpFIng->CurrentValue, 7);
		}
		if (!$this->EmpFSal->FldIsDetailKey) {
			$this->EmpFSal->setFormValue($objForm->GetValue("x_EmpFSal"));
			$this->EmpFSal->CurrentValue = ew_UnFormatDateTime($this->EmpFSal->CurrentValue, 7);
		}
		if (!$this->EmpSala->FldIsDetailKey) {
			$this->EmpSala->setFormValue($objForm->GetValue("x_EmpSala"));
		}
		if (!$this->EmpNomb->FldIsDetailKey) {
			$this->EmpNomb->setFormValue($objForm->GetValue("x_EmpNomb"));
		}
		if (!$this->EmpApel->FldIsDetailKey) {
			$this->EmpApel->setFormValue($objForm->GetValue("x_EmpApel"));
		}
		if (!$this->EmpCedu->FldIsDetailKey) {
			$this->EmpCedu->setFormValue($objForm->GetValue("x_EmpCedu"));
		}
		if (!$this->EmpTele->FldIsDetailKey) {
			$this->EmpTele->setFormValue($objForm->GetValue("x_EmpTele"));
		}
	}

	// Restore form values
	function RestoreFormValues() {
		global $objForm;
		$this->LoadOldRecord();
		$this->EmpCarg->CurrentValue = $this->EmpCarg->FormValue;
		$this->EmpFIng->CurrentValue = $this->EmpFIng->FormValue;
		$this->EmpFIng->CurrentValue = ew_UnFormatDateTime($this->EmpFIng->CurrentValue, 7);
		$this->EmpFSal->CurrentValue = $this->EmpFSal->FormValue;
		$this->EmpFSal->CurrentValue = ew_UnFormatDateTime($this->EmpFSal->CurrentValue, 7);
		$this->EmpSala->CurrentValue = $this->EmpSala->FormValue;
		$this->EmpNomb->CurrentValue = $this->EmpNomb->FormValue;
		$this->EmpApel->CurrentValue = $this->EmpApel->FormValue;
		$this->EmpCedu->CurrentValue = $this->EmpCedu->FormValue;
		$this->EmpTele->CurrentValue = $this->EmpTele->FormValue;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->EmpCodi->setDbValue($rs->fields('EmpCodi'));
		$this->EmpCarg->setDbValue($rs->fields('EmpCarg'));
		$this->EmpFIng->setDbValue($rs->fields('EmpFIng'));
		$this->EmpFSal->setDbValue($rs->fields('EmpFSal'));
		$this->EmpSala->setDbValue($rs->fields('EmpSala'));
		$this->EmpNomb->setDbValue($rs->fields('EmpNomb'));
		$this->EmpApel->setDbValue($rs->fields('EmpApel'));
		$this->EmpCedu->setDbValue($rs->fields('EmpCedu'));
		$this->EmpTele->setDbValue($rs->fields('EmpTele'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->EmpCodi->DbValue = $row['EmpCodi'];
		$this->EmpCarg->DbValue = $row['EmpCarg'];
		$this->EmpFIng->DbValue = $row['EmpFIng'];
		$this->EmpFSal->DbValue = $row['EmpFSal'];
		$this->EmpSala->DbValue = $row['EmpSala'];
		$this->EmpNomb->DbValue = $row['EmpNomb'];
		$this->EmpApel->DbValue = $row['EmpApel'];
		$this->EmpCedu->DbValue = $row['EmpCedu'];
		$this->EmpTele->DbValue = $row['EmpTele'];
	}

	// Load old record
	function LoadOldRecord() {

		// Load key values from Session
		$bValidKey = TRUE;
		if (strval($this->getKey("EmpCodi")) <> "")
			$this->EmpCodi->CurrentValue = $this->getKey("EmpCodi"); // EmpCodi
		else
			$bValidKey = FALSE;

		// Load old recordset
		if ($bValidKey) {
			$this->CurrentFilter = $this->KeyFilter();
			$sSql = $this->SQL();
			$conn = &$this->Connection();
			$this->OldRecordset = ew_LoadRecordset($sSql, $conn);
			$this->LoadRowValues($this->OldRecordset); // Load row values
		} else {
			$this->OldRecordset = NULL;
		}
		return $bValidKey;
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		// Convert decimal values if posted back

		if ($this->EmpSala->FormValue == $this->EmpSala->CurrentValue && is_numeric(ew_StrToFloat($this->EmpSala->CurrentValue)))
			$this->EmpSala->CurrentValue = ew_StrToFloat($this->EmpSala->CurrentValue);

		// Call Row_Rendering event
		$this->Row_Rendering();

		// Common render codes for all row types
		// EmpCodi
		// EmpCarg
		// EmpFIng
		// EmpFSal
		// EmpSala
		// EmpNomb
		// EmpApel
		// EmpCedu
		// EmpTele

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// EmpCodi
		$this->EmpCodi->ViewValue = $this->EmpCodi->CurrentValue;
		$this->EmpCodi->ViewCustomAttributes = "";

		// EmpCarg
		$this->EmpCarg->ViewValue = $this->EmpCarg->CurrentValue;
		$this->EmpCarg->ViewCustomAttributes = "";

		// EmpFIng
		$this->EmpFIng->ViewValue = $this->EmpFIng->CurrentValue;
		$this->EmpFIng->ViewValue = ew_FormatDateTime($this->EmpFIng->ViewValue, 7);
		$this->EmpFIng->ViewCustomAttributes = "";

		// EmpFSal
		$this->EmpFSal->ViewValue = $this->EmpFSal->CurrentValue;
		$this->EmpFSal->ViewValue = ew_FormatDateTime($this->EmpFSal->ViewValue, 7);
		$this->EmpFSal->ViewCustomAttributes = "";

		// EmpSala
		$this->EmpSala->ViewValue = $this->EmpSala->CurrentValue;
		$this->EmpSala->ViewValue = ew_FormatNumber($this->EmpSala->ViewValue, 0, -2, -2, -2);
		$this->EmpSala->ViewCustomAttributes = "";

		// EmpNomb
		$this->EmpNomb->ViewValue = $this->EmpNomb->CurrentValue;
		$this->EmpNomb->ViewCustomAttributes = "";

		// EmpApel
		$this->EmpApel->ViewValue = $this->EmpApel->CurrentValue;
		$this->EmpApel->ViewCustomAttributes = "";

		// EmpCedu
		$this->EmpCedu->ViewValue = $this->EmpCedu->CurrentValue;
		$this->EmpCedu->ViewCustomAttributes = "";

		// EmpTele
		$this->EmpTele->ViewValue = $this->EmpTele->CurrentValue;
		$this->EmpTele->ViewCustomAttributes = "";

			// EmpCarg
			$this->EmpCarg->LinkCustomAttributes = "";
			$this->EmpCarg->HrefValue = "";
			$this->EmpCarg->TooltipValue = "";

			// EmpFIng
			$this->EmpFIng->LinkCustomAttributes = "";
			$this->EmpFIng->HrefValue = "";
			$this->EmpFIng->TooltipValue = "";

			// EmpFSal
			$this->EmpFSal->LinkCustomAttributes = "";
			$this->EmpFSal->HrefValue = "";
			$this->EmpFSal->TooltipValue = "";

			// EmpSala
			$this->EmpSala->LinkCustomAttributes = "";
			$this->EmpSala->HrefValue = "";
			$this->EmpSala->TooltipValue = "";

			// EmpNomb
			$this->EmpNomb->LinkCustomAttributes = "";
			$this->EmpNomb->HrefValue = "";
			$this->EmpNomb->TooltipValue = "";

			// EmpApel
			$this->EmpApel->LinkCustomAttributes = "";
			$this->EmpApel->HrefValue = "";
			$this->EmpApel->TooltipValue = "";

			// EmpCedu
			$this->EmpCedu->LinkCustomAttributes = "";
			$this->EmpCedu->HrefValue = "";
			$this->EmpCedu->TooltipValue = "";

			// EmpTele
			$this->EmpTele->LinkCustomAttributes = "";
			$this->EmpTele->HrefValue = "";
			$this->EmpTele->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_ADD) { // Add row

			// EmpCarg
			$this->EmpCarg->EditAttrs["class"] = "form-control";
			$this->EmpCarg->EditCustomAttributes = "";
			$this->EmpCarg->EditValue = ew_HtmlEncode($this->EmpCarg->CurrentValue);
			$this->EmpCarg->PlaceHolder = ew_RemoveHtml($this->EmpCarg->FldCaption());

			// EmpFIng
			$this->EmpFIng->EditAttrs["class"] = "form-control";
			$this->EmpFIng->EditCustomAttributes = "";
			$this->EmpFIng->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->EmpFIng->CurrentValue, 7));
			$this->EmpFIng->PlaceHolder = ew_RemoveHtml($this->EmpFIng->FldCaption());

			// EmpFSal
			$this->EmpFSal->EditAttrs["class"] = "form-control";
			$this->EmpFSal->EditCustomAttributes = "";
			$this->EmpFSal->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->EmpFSal->CurrentValue, 7));
			$this->EmpFSal->PlaceHolder = ew_RemoveHtml($this->EmpFSal->FldCaption());

			// EmpSala
			$this->EmpSala->EditAttrs["class"] = "form-control";
			$this->EmpSala->EditCustomAttributes = "";
			$this->EmpSala->EditValue = ew_HtmlEncode($this->EmpSala->CurrentValue);
			$this->EmpSala->PlaceHolder = ew_RemoveHtml($this->EmpSala->FldCaption());
			if (strval($this->EmpSala->EditValue) <> "" && is_numeric($this->EmpSala->EditValue)) $this->EmpSala->EditValue = ew_FormatNumber($this->EmpSala->EditValue, -2, -2, -2, -2);

			// EmpNomb
			$this->EmpNomb->EditAttrs["class"] = "form-control";
			$this->EmpNomb->EditCustomAttributes = "";
			$this->EmpNomb->EditValue = ew_HtmlEncode($this->EmpNomb->CurrentValue);
			$this->EmpNomb->PlaceHolder = ew_RemoveHtml($this->EmpNomb->FldCaption());

			// EmpApel
			$this->EmpApel->EditAttrs["class"] = "form-control";
			$this->EmpApel->EditCustomAttributes = "";
			$this->EmpApel->EditValue = ew_HtmlEncode($this->EmpApel->CurrentValue);
			$this->EmpApel->PlaceHolder = ew_RemoveHtml($this->EmpApel->FldCaption());

			// EmpCedu
			$this->EmpCedu->EditAttrs["class"] = "form-control";
			$this->EmpCedu->EditCustomAttributes = "";
			$this->EmpCedu->EditValue = ew_HtmlEncode($this->EmpCedu->CurrentValue);
			$this->EmpCedu->PlaceHolder = ew_RemoveHtml($this->EmpCedu->FldCaption());

			// EmpTele
			$this->EmpTele->EditAttrs["class"] = "form-control";
			$this->EmpTele->EditCustomAttributes = "";
			$this->EmpTele->EditValue = ew_HtmlEncode($this->EmpTele->CurrentValue);
			$this->EmpTele->PlaceHolder = ew_RemoveHtml($this->EmpTele->FldCaption());

			// Edit refer script
			// EmpCarg

			$this->EmpCarg->HrefValue = "";

			// EmpFIng
			$this->EmpFIng->HrefValue = "";

			// EmpFSal
			$this->EmpFSal->HrefValue = "";

			// EmpSala
			$this->EmpSala->HrefValue = "";

			// EmpNomb
			$this->EmpNomb->HrefValue = "";

			// EmpApel
			$this->EmpApel->HrefValue = "";

			// EmpCedu
			$this->EmpCedu->HrefValue = "";

			// EmpTele
			$this->EmpTele->HrefValue = "";
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate form
	function ValidateForm() {
		global $Language, $gsFormError;

		// Initialize form error message
		$gsFormError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return ($gsFormError == "");
		if (!$this->EmpCarg->FldIsDetailKey && !is_null($this->EmpCarg->FormValue) && $this->EmpCarg->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->EmpCarg->FldCaption(), $this->EmpCarg->ReqErrMsg));
		}
		if (!$this->EmpFIng->FldIsDetailKey && !is_null($this->EmpFIng->FormValue) && $this->EmpFIng->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->EmpFIng->FldCaption(), $this->EmpFIng->ReqErrMsg));
		}
		if (!ew_CheckEuroDate($this->EmpFIng->FormValue)) {
			ew_AddMessage($gsFormError, $this->EmpFIng->FldErrMsg());
		}
		if (!ew_CheckEuroDate($this->EmpFSal->FormValue)) {
			ew_AddMessage($gsFormError, $this->EmpFSal->FldErrMsg());
		}
		if (!$this->EmpSala->FldIsDetailKey && !is_null($this->EmpSala->FormValue) && $this->EmpSala->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->EmpSala->FldCaption(), $this->EmpSala->ReqErrMsg));
		}
		if (!ew_CheckNumber($this->EmpSala->FormValue)) {
			ew_AddMessage($gsFormError, $this->EmpSala->FldErrMsg());
		}
		if (!$this->EmpCedu->FldIsDetailKey && !is_null($this->EmpCedu->FormValue) && $this->EmpCedu->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->EmpCedu->FldCaption(), $this->EmpCedu->ReqErrMsg));
		}
		if (!ew_CheckInteger($this->EmpTele->FormValue)) {
			ew_AddMessage($gsFormError, $this->EmpTele->FldErrMsg());
		}

		// Return validate result
		$ValidateForm = ($gsFormError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateForm = $ValidateForm && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsFormError, $sFormCustomError);
		}
		return $ValidateForm;
	}

	// Add record
	function AddRow($rsold = NULL) {
		global $Language, $Security;
		if ($this->EmpCarg->CurrentValue <> "") { // Check field with unique index
			$sFilter = "(EmpCarg = '" . ew_AdjustSql($this->EmpCarg->CurrentValue, $this->DBID) . "')";
			$rsChk = $this->LoadRs($sFilter);
			if ($rsChk && !$rsChk->EOF) {
				$sIdxErrMsg = str_replace("%f", $this->EmpCarg->FldCaption(), $Language->Phrase("DupIndex"));
				$sIdxErrMsg = str_replace("%v", $this->EmpCarg->CurrentValue, $sIdxErrMsg);
				$this->setFailureMessage($sIdxErrMsg);
				$rsChk->Close();
				return FALSE;
			}
		}
		if ($this->EmpCedu->CurrentValue <> "") { // Check field with unique index
			$sFilter = "(EmpCedu = '" . ew_AdjustSql($this->EmpCedu->CurrentValue, $this->DBID) . "')";
			$rsChk = $this->LoadRs($sFilter);
			if ($rsChk && !$rsChk->EOF) {
				$sIdxErrMsg = str_replace("%f", $this->EmpCedu->FldCaption(), $Language->Phrase("DupIndex"));
				$sIdxErrMsg = str_replace("%v", $this->EmpCedu->CurrentValue, $sIdxErrMsg);
				$this->setFailureMessage($sIdxErrMsg);
				$rsChk->Close();
				return FALSE;
			}
		}
		$conn = &$this->Connection();

		// Load db values from rsold
		if ($rsold) {
			$this->LoadDbValues($rsold);
		}
		$rsnew = array();

		// EmpCarg
		$this->EmpCarg->SetDbValueDef($rsnew, $this->EmpCarg->CurrentValue, "", FALSE);

		// EmpFIng
		$this->EmpFIng->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->EmpFIng->CurrentValue, 7), ew_CurrentDate(), FALSE);

		// EmpFSal
		$this->EmpFSal->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->EmpFSal->CurrentValue, 7), NULL, FALSE);

		// EmpSala
		$this->EmpSala->SetDbValueDef($rsnew, $this->EmpSala->CurrentValue, 0, FALSE);

		// EmpNomb
		$this->EmpNomb->SetDbValueDef($rsnew, $this->EmpNomb->CurrentValue, NULL, FALSE);

		// EmpApel
		$this->EmpApel->SetDbValueDef($rsnew, $this->EmpApel->CurrentValue, NULL, FALSE);

		// EmpCedu
		$this->EmpCedu->SetDbValueDef($rsnew, $this->EmpCedu->CurrentValue, NULL, FALSE);

		// EmpTele
		$this->EmpTele->SetDbValueDef($rsnew, $this->EmpTele->CurrentValue, NULL, FALSE);

		// Call Row Inserting event
		$rs = ($rsold == NULL) ? NULL : $rsold->fields;
		$bInsertRow = $this->Row_Inserting($rs, $rsnew);
		if ($bInsertRow) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			$AddRow = $this->Insert($rsnew);
			$conn->raiseErrorFn = '';
			if ($AddRow) {

				// Get insert id if necessary
				$this->EmpCodi->setDbValue($conn->GetOne("SELECT currval('\"Empl_EmpCodi_seq\"'::regclass)"));
				$rsnew['EmpCodi'] = $this->EmpCodi->DbValue;
			}
		} else {
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("InsertCancelled"));
			}
			$AddRow = FALSE;
		}
		if ($AddRow) {

			// Call Row Inserted event
			$rs = ($rsold == NULL) ? NULL : $rsold->fields;
			$this->Row_Inserted($rs, $rsnew);
		}
		return $AddRow;
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, "Empllist.php", "", $this->TableVar, TRUE);
		$PageId = ($this->CurrentAction == "C") ? "Copy" : "Add";
		$Breadcrumb->Add("add", $PageId, $url);
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($Empl_add)) $Empl_add = new cEmpl_add();

// Page init
$Empl_add->Page_Init();

// Page main
$Empl_add->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$Empl_add->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "add";
var CurrentForm = fEmpladd = new ew_Form("fEmpladd", "add");

// Validate form
fEmpladd.Validate = function() {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.GetForm(), $fobj = $(fobj);
	if ($fobj.find("#a_confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.FormKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = $fobj.find("#a_list").val() == "gridinsert";
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
			elm = this.GetElements("x" + infix + "_EmpCarg");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $Empl->EmpCarg->FldCaption(), $Empl->EmpCarg->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_EmpFIng");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $Empl->EmpFIng->FldCaption(), $Empl->EmpFIng->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_EmpFIng");
			if (elm && !ew_CheckEuroDate(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($Empl->EmpFIng->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_EmpFSal");
			if (elm && !ew_CheckEuroDate(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($Empl->EmpFSal->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_EmpSala");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $Empl->EmpSala->FldCaption(), $Empl->EmpSala->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_EmpSala");
			if (elm && !ew_CheckNumber(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($Empl->EmpSala->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_EmpCedu");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $Empl->EmpCedu->FldCaption(), $Empl->EmpCedu->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_EmpTele");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($Empl->EmpTele->FldErrMsg()) ?>");

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
	}

	// Process detail forms
	var dfs = $fobj.find("input[name='detailpage']").get();
	for (var i = 0; i < dfs.length; i++) {
		var df = dfs[i], val = df.value;
		if (val && ewForms[val])
			if (!ewForms[val].Validate())
				return false;
	}
	return true;
}

// Form_CustomValidate event
fEmpladd.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fEmpladd.ValidateRequired = true;
<?php } else { ?>
fEmpladd.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
// Form object for search

</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php $Empl_add->ShowPageHeader(); ?>
<?php
$Empl_add->ShowMessage();
?>
<form name="fEmpladd" id="fEmpladd" class="<?php echo $Empl_add->FormClassName ?>" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($Empl_add->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $Empl_add->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="Empl">
<input type="hidden" name="a_add" id="a_add" value="A">
<?php if ($Empl->CurrentAction == "F") { // Confirm page ?>
<input type="hidden" name="a_confirm" id="a_confirm" value="F">
<?php } ?>
<div>
<?php if ($Empl->EmpCarg->Visible) { // EmpCarg ?>
	<div id="r_EmpCarg" class="form-group">
		<label id="elh_Empl_EmpCarg" for="x_EmpCarg" class="col-sm-2 control-label ewLabel"><?php echo $Empl->EmpCarg->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $Empl->EmpCarg->CellAttributes() ?>>
<?php if ($Empl->CurrentAction <> "F") { ?>
<span id="el_Empl_EmpCarg">
<input type="text" data-table="Empl" data-field="x_EmpCarg" name="x_EmpCarg" id="x_EmpCarg" size="30" placeholder="<?php echo ew_HtmlEncode($Empl->EmpCarg->getPlaceHolder()) ?>" value="<?php echo $Empl->EmpCarg->EditValue ?>"<?php echo $Empl->EmpCarg->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_Empl_EmpCarg">
<span<?php echo $Empl->EmpCarg->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $Empl->EmpCarg->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="Empl" data-field="x_EmpCarg" name="x_EmpCarg" id="x_EmpCarg" value="<?php echo ew_HtmlEncode($Empl->EmpCarg->FormValue) ?>">
<?php } ?>
<?php echo $Empl->EmpCarg->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($Empl->EmpFIng->Visible) { // EmpFIng ?>
	<div id="r_EmpFIng" class="form-group">
		<label id="elh_Empl_EmpFIng" for="x_EmpFIng" class="col-sm-2 control-label ewLabel"><?php echo $Empl->EmpFIng->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $Empl->EmpFIng->CellAttributes() ?>>
<?php if ($Empl->CurrentAction <> "F") { ?>
<span id="el_Empl_EmpFIng">
<input type="text" data-table="Empl" data-field="x_EmpFIng" data-format="7" name="x_EmpFIng" id="x_EmpFIng" placeholder="<?php echo ew_HtmlEncode($Empl->EmpFIng->getPlaceHolder()) ?>" value="<?php echo $Empl->EmpFIng->EditValue ?>"<?php echo $Empl->EmpFIng->EditAttributes() ?>>
<?php if (!$Empl->EmpFIng->ReadOnly && !$Empl->EmpFIng->Disabled && !isset($Empl->EmpFIng->EditAttrs["readonly"]) && !isset($Empl->EmpFIng->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("fEmpladd", "x_EmpFIng", "%d/%m/%Y");
</script>
<?php } ?>
</span>
<?php } else { ?>
<span id="el_Empl_EmpFIng">
<span<?php echo $Empl->EmpFIng->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $Empl->EmpFIng->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="Empl" data-field="x_EmpFIng" name="x_EmpFIng" id="x_EmpFIng" value="<?php echo ew_HtmlEncode($Empl->EmpFIng->FormValue) ?>">
<?php } ?>
<?php echo $Empl->EmpFIng->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($Empl->EmpFSal->Visible) { // EmpFSal ?>
	<div id="r_EmpFSal" class="form-group">
		<label id="elh_Empl_EmpFSal" for="x_EmpFSal" class="col-sm-2 control-label ewLabel"><?php echo $Empl->EmpFSal->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $Empl->EmpFSal->CellAttributes() ?>>
<?php if ($Empl->CurrentAction <> "F") { ?>
<span id="el_Empl_EmpFSal">
<input type="text" data-table="Empl" data-field="x_EmpFSal" data-format="7" name="x_EmpFSal" id="x_EmpFSal" placeholder="<?php echo ew_HtmlEncode($Empl->EmpFSal->getPlaceHolder()) ?>" value="<?php echo $Empl->EmpFSal->EditValue ?>"<?php echo $Empl->EmpFSal->EditAttributes() ?>>
<?php if (!$Empl->EmpFSal->ReadOnly && !$Empl->EmpFSal->Disabled && !isset($Empl->EmpFSal->EditAttrs["readonly"]) && !isset($Empl->EmpFSal->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("fEmpladd", "x_EmpFSal", "%d/%m/%Y");
</script>
<?php } ?>
</span>
<?php } else { ?>
<span id="el_Empl_EmpFSal">
<span<?php echo $Empl->EmpFSal->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $Empl->EmpFSal->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="Empl" data-field="x_EmpFSal" name="x_EmpFSal" id="x_EmpFSal" value="<?php echo ew_HtmlEncode($Empl->EmpFSal->FormValue) ?>">
<?php } ?>
<?php echo $Empl->EmpFSal->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($Empl->EmpSala->Visible) { // EmpSala ?>
	<div id="r_EmpSala" class="form-group">
		<label id="elh_Empl_EmpSala" for="x_EmpSala" class="col-sm-2 control-label ewLabel"><?php echo $Empl->EmpSala->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $Empl->EmpSala->CellAttributes() ?>>
<?php if ($Empl->CurrentAction <> "F") { ?>
<span id="el_Empl_EmpSala">
<input type="text" data-table="Empl" data-field="x_EmpSala" name="x_EmpSala" id="x_EmpSala" size="30" placeholder="<?php echo ew_HtmlEncode($Empl->EmpSala->getPlaceHolder()) ?>" value="<?php echo $Empl->EmpSala->EditValue ?>"<?php echo $Empl->EmpSala->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_Empl_EmpSala">
<span<?php echo $Empl->EmpSala->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $Empl->EmpSala->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="Empl" data-field="x_EmpSala" name="x_EmpSala" id="x_EmpSala" value="<?php echo ew_HtmlEncode($Empl->EmpSala->FormValue) ?>">
<?php } ?>
<?php echo $Empl->EmpSala->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($Empl->EmpNomb->Visible) { // EmpNomb ?>
	<div id="r_EmpNomb" class="form-group">
		<label id="elh_Empl_EmpNomb" for="x_EmpNomb" class="col-sm-2 control-label ewLabel"><?php echo $Empl->EmpNomb->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $Empl->EmpNomb->CellAttributes() ?>>
<?php if ($Empl->CurrentAction <> "F") { ?>
<span id="el_Empl_EmpNomb">
<input type="text" data-table="Empl" data-field="x_EmpNomb" name="x_EmpNomb" id="x_EmpNomb" size="30" placeholder="<?php echo ew_HtmlEncode($Empl->EmpNomb->getPlaceHolder()) ?>" value="<?php echo $Empl->EmpNomb->EditValue ?>"<?php echo $Empl->EmpNomb->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_Empl_EmpNomb">
<span<?php echo $Empl->EmpNomb->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $Empl->EmpNomb->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="Empl" data-field="x_EmpNomb" name="x_EmpNomb" id="x_EmpNomb" value="<?php echo ew_HtmlEncode($Empl->EmpNomb->FormValue) ?>">
<?php } ?>
<?php echo $Empl->EmpNomb->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($Empl->EmpApel->Visible) { // EmpApel ?>
	<div id="r_EmpApel" class="form-group">
		<label id="elh_Empl_EmpApel" for="x_EmpApel" class="col-sm-2 control-label ewLabel"><?php echo $Empl->EmpApel->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $Empl->EmpApel->CellAttributes() ?>>
<?php if ($Empl->CurrentAction <> "F") { ?>
<span id="el_Empl_EmpApel">
<input type="text" data-table="Empl" data-field="x_EmpApel" name="x_EmpApel" id="x_EmpApel" size="30" placeholder="<?php echo ew_HtmlEncode($Empl->EmpApel->getPlaceHolder()) ?>" value="<?php echo $Empl->EmpApel->EditValue ?>"<?php echo $Empl->EmpApel->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_Empl_EmpApel">
<span<?php echo $Empl->EmpApel->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $Empl->EmpApel->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="Empl" data-field="x_EmpApel" name="x_EmpApel" id="x_EmpApel" value="<?php echo ew_HtmlEncode($Empl->EmpApel->FormValue) ?>">
<?php } ?>
<?php echo $Empl->EmpApel->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($Empl->EmpCedu->Visible) { // EmpCedu ?>
	<div id="r_EmpCedu" class="form-group">
		<label id="elh_Empl_EmpCedu" for="x_EmpCedu" class="col-sm-2 control-label ewLabel"><?php echo $Empl->EmpCedu->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $Empl->EmpCedu->CellAttributes() ?>>
<?php if ($Empl->CurrentAction <> "F") { ?>
<span id="el_Empl_EmpCedu">
<input type="text" data-table="Empl" data-field="x_EmpCedu" name="x_EmpCedu" id="x_EmpCedu" size="30" placeholder="<?php echo ew_HtmlEncode($Empl->EmpCedu->getPlaceHolder()) ?>" value="<?php echo $Empl->EmpCedu->EditValue ?>"<?php echo $Empl->EmpCedu->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_Empl_EmpCedu">
<span<?php echo $Empl->EmpCedu->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $Empl->EmpCedu->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="Empl" data-field="x_EmpCedu" name="x_EmpCedu" id="x_EmpCedu" value="<?php echo ew_HtmlEncode($Empl->EmpCedu->FormValue) ?>">
<?php } ?>
<?php echo $Empl->EmpCedu->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($Empl->EmpTele->Visible) { // EmpTele ?>
	<div id="r_EmpTele" class="form-group">
		<label id="elh_Empl_EmpTele" for="x_EmpTele" class="col-sm-2 control-label ewLabel"><?php echo $Empl->EmpTele->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $Empl->EmpTele->CellAttributes() ?>>
<?php if ($Empl->CurrentAction <> "F") { ?>
<span id="el_Empl_EmpTele">
<input type="text" data-table="Empl" data-field="x_EmpTele" name="x_EmpTele" id="x_EmpTele" size="30" placeholder="<?php echo ew_HtmlEncode($Empl->EmpTele->getPlaceHolder()) ?>" value="<?php echo $Empl->EmpTele->EditValue ?>"<?php echo $Empl->EmpTele->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_Empl_EmpTele">
<span<?php echo $Empl->EmpTele->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $Empl->EmpTele->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="Empl" data-field="x_EmpTele" name="x_EmpTele" id="x_EmpTele" value="<?php echo ew_HtmlEncode($Empl->EmpTele->FormValue) ?>">
<?php } ?>
<?php echo $Empl->EmpTele->CustomMsg ?></div></div>
	</div>
<?php } ?>
</div>
<div class="form-group">
	<div class="col-sm-offset-2 col-sm-10">
<?php if ($Empl->CurrentAction <> "F") { // Confirm page ?>
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit" onclick="this.form.a_add.value='F';"><?php echo $Language->Phrase("AddBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="button" data-href="<?php echo $Empl_add->getReturnUrl() ?>"><?php echo $Language->Phrase("CancelBtn") ?></button>
<?php } else { ?>
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("ConfirmBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="submit" onclick="this.form.a_add.value='X';"><?php echo $Language->Phrase("CancelBtn") ?></button>
<?php } ?>
	</div>
</div>
</form>
<script type="text/javascript">
fEmpladd.Init();
</script>
<?php
$Empl_add->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$Empl_add->Page_Terminate();
?>
