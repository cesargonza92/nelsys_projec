<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "Mproinfo.php" ?>
<?php include_once "Usuainfo.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$Mpro_edit = NULL; // Initialize page object first

class cMpro_edit extends cMpro {

	// Page ID
	var $PageID = 'edit';

	// Project ID
	var $ProjectID = "{04439FF7-B43F-460F-8514-F71C8FF9E679}";

	// Table name
	var $TableName = 'Mpro';

	// Page object name
	var $PageObjName = 'Mpro_edit';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (Mpro)
		if (!isset($GLOBALS["Mpro"]) || get_class($GLOBALS["Mpro"]) == "cMpro") {
			$GLOBALS["Mpro"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["Mpro"];
		}

		// Table object (Usua)
		if (!isset($GLOBALS['Usua'])) $GLOBALS['Usua'] = new cUsua();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'edit', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'Mpro', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (Usua)
		if (!isset($UserTable)) {
			$UserTable = new cUsua();
			$UserTableConn = Conn($UserTable->DBID);
		}
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanEdit()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage($Language->Phrase("NoPermission")); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("Mprolist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}

		// Create form object
		$objForm = new cFormObj();
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action
		$this->MprCodi->Visible = !$this->IsAdd() && !$this->IsCopy() && !$this->IsGridAdd();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Process auto fill
		if (@$_POST["ajax"] == "autofill") {
			$results = $this->GetAutoFill(@$_POST["name"], @$_POST["q"]);
			if ($results) {

				// Clean output buffer
				if (!EW_DEBUG_ENABLED && ob_get_length())
					ob_end_clean();
				echo $results;
				$this->Page_Terminate();
				exit();
			}
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $Mpro;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($Mpro);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $FormClassName = "form-horizontal ewForm ewEditForm";
	var $DbMasterFilter;
	var $DbDetailFilter;

	// 
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError;

		// Load key from QueryString
		if (@$_GET["MprCodi"] <> "") {
			$this->MprCodi->setQueryStringValue($_GET["MprCodi"]);
		}

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Process form if post back
		if (@$_POST["a_edit"] <> "") {
			$this->CurrentAction = $_POST["a_edit"]; // Get action code
			$this->LoadFormValues(); // Get form values
		} else {
			$this->CurrentAction = "I"; // Default action is display
		}

		// Check if valid key
		if ($this->MprCodi->CurrentValue == "")
			$this->Page_Terminate("Mprolist.php"); // Invalid key, return to list

		// Validate form if post back
		if (@$_POST["a_edit"] <> "") {
			if (!$this->ValidateForm()) {
				$this->CurrentAction = ""; // Form error, reset action
				$this->setFailureMessage($gsFormError);
				$this->EventCancelled = TRUE; // Event cancelled
				$this->RestoreFormValues();
			}
		}
		switch ($this->CurrentAction) {
			case "I": // Get a record to display
				if (!$this->LoadRow()) { // Load record based on key
					if ($this->getFailureMessage() == "") $this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
					$this->Page_Terminate("Mprolist.php"); // No matching record, return to list
				}
				break;
			Case "U": // Update
				$sReturnUrl = $this->getReturnUrl();
				$this->SendEmail = TRUE; // Send email on update success
				if ($this->EditRow()) { // Update record based on key
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage($Language->Phrase("UpdateSuccess")); // Update success
					$this->Page_Terminate($sReturnUrl); // Return to caller
				} elseif ($this->getFailureMessage() == $Language->Phrase("NoRecord")) {
					$this->Page_Terminate($sReturnUrl); // Return to caller
				} else {
					$this->EventCancelled = TRUE; // Event cancelled
					$this->RestoreFormValues(); // Restore form values if update failed
				}
		}

		// Render the record
		if ($this->CurrentAction == "F") { // Confirm page
			$this->RowType = EW_ROWTYPE_VIEW; // Render as View
		} else {
			$this->RowType = EW_ROWTYPE_EDIT; // Render as Edit
		}
		$this->ResetAttrs();
		$this->RenderRow();
	}

	// Set up starting record parameters
	function SetUpStartRec() {
		if ($this->DisplayRecs == 0)
			return;
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET[EW_TABLE_START_REC] <> "") { // Check for "start" parameter
				$this->StartRec = $_GET[EW_TABLE_START_REC];
				$this->setStartRecordNumber($this->StartRec);
			} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
				$PageNo = $_GET[EW_TABLE_PAGE_NO];
				if (is_numeric($PageNo)) {
					$this->StartRec = ($PageNo-1)*$this->DisplayRecs+1;
					if ($this->StartRec <= 0) {
						$this->StartRec = 1;
					} elseif ($this->StartRec >= intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1) {
						$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1;
					}
					$this->setStartRecordNumber($this->StartRec);
				}
			}
		}
		$this->StartRec = $this->getStartRecordNumber();

		// Check if correct start record counter
		if (!is_numeric($this->StartRec) || $this->StartRec == "") { // Avoid invalid start record counter
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} elseif (intval($this->StartRec) > intval($this->TotalRecs)) { // Avoid starting record > total records
			$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to last page first record
			$this->setStartRecordNumber($this->StartRec);
		} elseif (($this->StartRec-1) % $this->DisplayRecs <> 0) {
			$this->StartRec = intval(($this->StartRec-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to page boundary
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Get upload files
	function GetUploadFiles() {
		global $objForm, $Language;

		// Get upload data
	}

	// Load form values
	function LoadFormValues() {

		// Load from form
		global $objForm;
		if (!$this->MprCodi->FldIsDetailKey)
			$this->MprCodi->setFormValue($objForm->GetValue("x_MprCodi"));
		if (!$this->MprNomb->FldIsDetailKey) {
			$this->MprNomb->setFormValue($objForm->GetValue("x_MprNomb"));
		}
		if (!$this->MprDesc->FldIsDetailKey) {
			$this->MprDesc->setFormValue($objForm->GetValue("x_MprDesc"));
		}
		if (!$this->MprUsua->FldIsDetailKey) {
			$this->MprUsua->setFormValue($objForm->GetValue("x_MprUsua"));
		}
		if (!$this->MprFCre->FldIsDetailKey) {
			$this->MprFCre->setFormValue($objForm->GetValue("x_MprFCre"));
			$this->MprFCre->CurrentValue = ew_UnFormatDateTime($this->MprFCre->CurrentValue, 7);
		}
	}

	// Restore form values
	function RestoreFormValues() {
		global $objForm;
		$this->LoadRow();
		$this->MprCodi->CurrentValue = $this->MprCodi->FormValue;
		$this->MprNomb->CurrentValue = $this->MprNomb->FormValue;
		$this->MprDesc->CurrentValue = $this->MprDesc->FormValue;
		$this->MprUsua->CurrentValue = $this->MprUsua->FormValue;
		$this->MprFCre->CurrentValue = $this->MprFCre->FormValue;
		$this->MprFCre->CurrentValue = ew_UnFormatDateTime($this->MprFCre->CurrentValue, 7);
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->MprCodi->setDbValue($rs->fields('MprCodi'));
		$this->MprNomb->setDbValue($rs->fields('MprNomb'));
		$this->MprDesc->setDbValue($rs->fields('MprDesc'));
		$this->MprUsua->setDbValue($rs->fields('MprUsua'));
		$this->MprFCre->setDbValue($rs->fields('MprFCre'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->MprCodi->DbValue = $row['MprCodi'];
		$this->MprNomb->DbValue = $row['MprNomb'];
		$this->MprDesc->DbValue = $row['MprDesc'];
		$this->MprUsua->DbValue = $row['MprUsua'];
		$this->MprFCre->DbValue = $row['MprFCre'];
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		// Call Row_Rendering event

		$this->Row_Rendering();

		// Common render codes for all row types
		// MprCodi
		// MprNomb
		// MprDesc
		// MprUsua
		// MprFCre

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// MprCodi
		$this->MprCodi->ViewValue = $this->MprCodi->CurrentValue;
		$this->MprCodi->ViewCustomAttributes = "";

		// MprNomb
		$this->MprNomb->ViewValue = $this->MprNomb->CurrentValue;
		$this->MprNomb->ViewCustomAttributes = "";

		// MprDesc
		$this->MprDesc->ViewValue = $this->MprDesc->CurrentValue;
		$this->MprDesc->ViewCustomAttributes = "";

		// MprUsua
		$this->MprUsua->ViewValue = $this->MprUsua->CurrentValue;
		$this->MprUsua->ViewCustomAttributes = "";

		// MprFCre
		$this->MprFCre->ViewValue = $this->MprFCre->CurrentValue;
		$this->MprFCre->ViewValue = ew_FormatDateTime($this->MprFCre->ViewValue, 7);
		$this->MprFCre->ViewCustomAttributes = "";

			// MprCodi
			$this->MprCodi->LinkCustomAttributes = "";
			$this->MprCodi->HrefValue = "";
			$this->MprCodi->TooltipValue = "";

			// MprNomb
			$this->MprNomb->LinkCustomAttributes = "";
			$this->MprNomb->HrefValue = "";
			$this->MprNomb->TooltipValue = "";

			// MprDesc
			$this->MprDesc->LinkCustomAttributes = "";
			$this->MprDesc->HrefValue = "";
			$this->MprDesc->TooltipValue = "";

			// MprUsua
			$this->MprUsua->LinkCustomAttributes = "";
			$this->MprUsua->HrefValue = "";
			$this->MprUsua->TooltipValue = "";

			// MprFCre
			$this->MprFCre->LinkCustomAttributes = "";
			$this->MprFCre->HrefValue = "";
			$this->MprFCre->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_EDIT) { // Edit row

			// MprCodi
			$this->MprCodi->EditAttrs["class"] = "form-control";
			$this->MprCodi->EditCustomAttributes = "";
			$this->MprCodi->EditValue = $this->MprCodi->CurrentValue;
			$this->MprCodi->ViewCustomAttributes = "";

			// MprNomb
			$this->MprNomb->EditAttrs["class"] = "form-control";
			$this->MprNomb->EditCustomAttributes = "";
			$this->MprNomb->EditValue = ew_HtmlEncode($this->MprNomb->CurrentValue);
			$this->MprNomb->PlaceHolder = ew_RemoveHtml($this->MprNomb->FldCaption());

			// MprDesc
			$this->MprDesc->EditAttrs["class"] = "form-control";
			$this->MprDesc->EditCustomAttributes = "";
			$this->MprDesc->EditValue = ew_HtmlEncode($this->MprDesc->CurrentValue);
			$this->MprDesc->PlaceHolder = ew_RemoveHtml($this->MprDesc->FldCaption());

			// MprUsua
			$this->MprUsua->EditAttrs["class"] = "form-control";
			$this->MprUsua->EditCustomAttributes = "";
			$this->MprUsua->EditValue = ew_HtmlEncode($this->MprUsua->CurrentValue);
			$this->MprUsua->PlaceHolder = ew_RemoveHtml($this->MprUsua->FldCaption());

			// MprFCre
			$this->MprFCre->EditAttrs["class"] = "form-control";
			$this->MprFCre->EditCustomAttributes = "";
			$this->MprFCre->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->MprFCre->CurrentValue, 7));
			$this->MprFCre->PlaceHolder = ew_RemoveHtml($this->MprFCre->FldCaption());

			// Edit refer script
			// MprCodi

			$this->MprCodi->HrefValue = "";

			// MprNomb
			$this->MprNomb->HrefValue = "";

			// MprDesc
			$this->MprDesc->HrefValue = "";

			// MprUsua
			$this->MprUsua->HrefValue = "";

			// MprFCre
			$this->MprFCre->HrefValue = "";
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate form
	function ValidateForm() {
		global $Language, $gsFormError;

		// Initialize form error message
		$gsFormError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return ($gsFormError == "");
		if (!$this->MprNomb->FldIsDetailKey && !is_null($this->MprNomb->FormValue) && $this->MprNomb->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->MprNomb->FldCaption(), $this->MprNomb->ReqErrMsg));
		}
		if (!$this->MprUsua->FldIsDetailKey && !is_null($this->MprUsua->FormValue) && $this->MprUsua->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->MprUsua->FldCaption(), $this->MprUsua->ReqErrMsg));
		}
		if (!$this->MprFCre->FldIsDetailKey && !is_null($this->MprFCre->FormValue) && $this->MprFCre->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->MprFCre->FldCaption(), $this->MprFCre->ReqErrMsg));
		}
		if (!ew_CheckEuroDate($this->MprFCre->FormValue)) {
			ew_AddMessage($gsFormError, $this->MprFCre->FldErrMsg());
		}

		// Return validate result
		$ValidateForm = ($gsFormError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateForm = $ValidateForm && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsFormError, $sFormCustomError);
		}
		return $ValidateForm;
	}

	// Update record based on key values
	function EditRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$conn = &$this->Connection();
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE)
			return FALSE;
		if ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record message
			$EditRow = FALSE; // Update Failed
		} else {

			// Save old values
			$rsold = &$rs->fields;
			$this->LoadDbValues($rsold);
			$rsnew = array();

			// MprNomb
			$this->MprNomb->SetDbValueDef($rsnew, $this->MprNomb->CurrentValue, "", $this->MprNomb->ReadOnly);

			// MprDesc
			$this->MprDesc->SetDbValueDef($rsnew, $this->MprDesc->CurrentValue, NULL, $this->MprDesc->ReadOnly);

			// MprUsua
			$this->MprUsua->SetDbValueDef($rsnew, $this->MprUsua->CurrentValue, "", $this->MprUsua->ReadOnly);

			// MprFCre
			$this->MprFCre->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->MprFCre->CurrentValue, 7), ew_CurrentDate(), $this->MprFCre->ReadOnly);

			// Call Row Updating event
			$bUpdateRow = $this->Row_Updating($rsold, $rsnew);
			if ($bUpdateRow) {
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				if (count($rsnew) > 0)
					$EditRow = $this->Update($rsnew, "", $rsold);
				else
					$EditRow = TRUE; // No field to update
				$conn->raiseErrorFn = '';
				if ($EditRow) {
				}
			} else {
				if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

					// Use the message, do nothing
				} elseif ($this->CancelMessage <> "") {
					$this->setFailureMessage($this->CancelMessage);
					$this->CancelMessage = "";
				} else {
					$this->setFailureMessage($Language->Phrase("UpdateCancelled"));
				}
				$EditRow = FALSE;
			}
		}

		// Call Row_Updated event
		if ($EditRow)
			$this->Row_Updated($rsold, $rsnew);
		$rs->Close();
		return $EditRow;
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, "Mprolist.php", "", $this->TableVar, TRUE);
		$PageId = "edit";
		$Breadcrumb->Add("edit", $PageId, $url);
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($Mpro_edit)) $Mpro_edit = new cMpro_edit();

// Page init
$Mpro_edit->Page_Init();

// Page main
$Mpro_edit->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$Mpro_edit->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "edit";
var CurrentForm = fMproedit = new ew_Form("fMproedit", "edit");

// Validate form
fMproedit.Validate = function() {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.GetForm(), $fobj = $(fobj);
	if ($fobj.find("#a_confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.FormKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = $fobj.find("#a_list").val() == "gridinsert";
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
			elm = this.GetElements("x" + infix + "_MprNomb");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $Mpro->MprNomb->FldCaption(), $Mpro->MprNomb->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_MprUsua");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $Mpro->MprUsua->FldCaption(), $Mpro->MprUsua->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_MprFCre");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $Mpro->MprFCre->FldCaption(), $Mpro->MprFCre->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_MprFCre");
			if (elm && !ew_CheckEuroDate(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($Mpro->MprFCre->FldErrMsg()) ?>");

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
	}

	// Process detail forms
	var dfs = $fobj.find("input[name='detailpage']").get();
	for (var i = 0; i < dfs.length; i++) {
		var df = dfs[i], val = df.value;
		if (val && ewForms[val])
			if (!ewForms[val].Validate())
				return false;
	}
	return true;
}

// Form_CustomValidate event
fMproedit.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fMproedit.ValidateRequired = true;
<?php } else { ?>
fMproedit.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
// Form object for search

</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php $Mpro_edit->ShowPageHeader(); ?>
<?php
$Mpro_edit->ShowMessage();
?>
<form name="fMproedit" id="fMproedit" class="<?php echo $Mpro_edit->FormClassName ?>" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($Mpro_edit->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $Mpro_edit->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="Mpro">
<input type="hidden" name="a_edit" id="a_edit" value="U">
<?php if ($Mpro->CurrentAction == "F") { // Confirm page ?>
<input type="hidden" name="a_confirm" id="a_confirm" value="F">
<?php } ?>
<div>
<?php if ($Mpro->MprCodi->Visible) { // MprCodi ?>
	<div id="r_MprCodi" class="form-group">
		<label id="elh_Mpro_MprCodi" class="col-sm-2 control-label ewLabel"><?php echo $Mpro->MprCodi->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $Mpro->MprCodi->CellAttributes() ?>>
<?php if ($Mpro->CurrentAction <> "F") { ?>
<span id="el_Mpro_MprCodi">
<span<?php echo $Mpro->MprCodi->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $Mpro->MprCodi->EditValue ?></p></span>
</span>
<input type="hidden" data-table="Mpro" data-field="x_MprCodi" name="x_MprCodi" id="x_MprCodi" value="<?php echo ew_HtmlEncode($Mpro->MprCodi->CurrentValue) ?>">
<?php } else { ?>
<span id="el_Mpro_MprCodi">
<span<?php echo $Mpro->MprCodi->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $Mpro->MprCodi->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="Mpro" data-field="x_MprCodi" name="x_MprCodi" id="x_MprCodi" value="<?php echo ew_HtmlEncode($Mpro->MprCodi->FormValue) ?>">
<?php } ?>
<?php echo $Mpro->MprCodi->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($Mpro->MprNomb->Visible) { // MprNomb ?>
	<div id="r_MprNomb" class="form-group">
		<label id="elh_Mpro_MprNomb" for="x_MprNomb" class="col-sm-2 control-label ewLabel"><?php echo $Mpro->MprNomb->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $Mpro->MprNomb->CellAttributes() ?>>
<?php if ($Mpro->CurrentAction <> "F") { ?>
<span id="el_Mpro_MprNomb">
<input type="text" data-table="Mpro" data-field="x_MprNomb" name="x_MprNomb" id="x_MprNomb" size="30" placeholder="<?php echo ew_HtmlEncode($Mpro->MprNomb->getPlaceHolder()) ?>" value="<?php echo $Mpro->MprNomb->EditValue ?>"<?php echo $Mpro->MprNomb->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_Mpro_MprNomb">
<span<?php echo $Mpro->MprNomb->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $Mpro->MprNomb->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="Mpro" data-field="x_MprNomb" name="x_MprNomb" id="x_MprNomb" value="<?php echo ew_HtmlEncode($Mpro->MprNomb->FormValue) ?>">
<?php } ?>
<?php echo $Mpro->MprNomb->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($Mpro->MprDesc->Visible) { // MprDesc ?>
	<div id="r_MprDesc" class="form-group">
		<label id="elh_Mpro_MprDesc" for="x_MprDesc" class="col-sm-2 control-label ewLabel"><?php echo $Mpro->MprDesc->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $Mpro->MprDesc->CellAttributes() ?>>
<?php if ($Mpro->CurrentAction <> "F") { ?>
<span id="el_Mpro_MprDesc">
<input type="text" data-table="Mpro" data-field="x_MprDesc" name="x_MprDesc" id="x_MprDesc" size="30" placeholder="<?php echo ew_HtmlEncode($Mpro->MprDesc->getPlaceHolder()) ?>" value="<?php echo $Mpro->MprDesc->EditValue ?>"<?php echo $Mpro->MprDesc->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_Mpro_MprDesc">
<span<?php echo $Mpro->MprDesc->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $Mpro->MprDesc->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="Mpro" data-field="x_MprDesc" name="x_MprDesc" id="x_MprDesc" value="<?php echo ew_HtmlEncode($Mpro->MprDesc->FormValue) ?>">
<?php } ?>
<?php echo $Mpro->MprDesc->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($Mpro->MprUsua->Visible) { // MprUsua ?>
	<div id="r_MprUsua" class="form-group">
		<label id="elh_Mpro_MprUsua" for="x_MprUsua" class="col-sm-2 control-label ewLabel"><?php echo $Mpro->MprUsua->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $Mpro->MprUsua->CellAttributes() ?>>
<?php if ($Mpro->CurrentAction <> "F") { ?>
<span id="el_Mpro_MprUsua">
<input type="text" data-table="Mpro" data-field="x_MprUsua" name="x_MprUsua" id="x_MprUsua" size="30" placeholder="<?php echo ew_HtmlEncode($Mpro->MprUsua->getPlaceHolder()) ?>" value="<?php echo $Mpro->MprUsua->EditValue ?>"<?php echo $Mpro->MprUsua->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_Mpro_MprUsua">
<span<?php echo $Mpro->MprUsua->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $Mpro->MprUsua->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="Mpro" data-field="x_MprUsua" name="x_MprUsua" id="x_MprUsua" value="<?php echo ew_HtmlEncode($Mpro->MprUsua->FormValue) ?>">
<?php } ?>
<?php echo $Mpro->MprUsua->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($Mpro->MprFCre->Visible) { // MprFCre ?>
	<div id="r_MprFCre" class="form-group">
		<label id="elh_Mpro_MprFCre" for="x_MprFCre" class="col-sm-2 control-label ewLabel"><?php echo $Mpro->MprFCre->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $Mpro->MprFCre->CellAttributes() ?>>
<?php if ($Mpro->CurrentAction <> "F") { ?>
<span id="el_Mpro_MprFCre">
<input type="text" data-table="Mpro" data-field="x_MprFCre" data-format="7" name="x_MprFCre" id="x_MprFCre" placeholder="<?php echo ew_HtmlEncode($Mpro->MprFCre->getPlaceHolder()) ?>" value="<?php echo $Mpro->MprFCre->EditValue ?>"<?php echo $Mpro->MprFCre->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_Mpro_MprFCre">
<span<?php echo $Mpro->MprFCre->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $Mpro->MprFCre->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="Mpro" data-field="x_MprFCre" name="x_MprFCre" id="x_MprFCre" value="<?php echo ew_HtmlEncode($Mpro->MprFCre->FormValue) ?>">
<?php } ?>
<?php echo $Mpro->MprFCre->CustomMsg ?></div></div>
	</div>
<?php } ?>
</div>
<div class="form-group">
	<div class="col-sm-offset-2 col-sm-10">
<?php if ($Mpro->CurrentAction <> "F") { // Confirm page ?>
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit" onclick="this.form.a_edit.value='F';"><?php echo $Language->Phrase("SaveBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="button" data-href="<?php echo $Mpro_edit->getReturnUrl() ?>"><?php echo $Language->Phrase("CancelBtn") ?></button>
<?php } else { ?>
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("ConfirmBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="submit" onclick="this.form.a_edit.value='X';"><?php echo $Language->Phrase("CancelBtn") ?></button>
<?php } ?>
	</div>
</div>
</form>
<script type="text/javascript">
fMproedit.Init();
</script>
<?php
$Mpro_edit->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$Mpro_edit->Page_Terminate();
?>
