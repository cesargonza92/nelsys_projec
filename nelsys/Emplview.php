<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "Emplinfo.php" ?>
<?php include_once "Usuainfo.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$Empl_view = NULL; // Initialize page object first

class cEmpl_view extends cEmpl {

	// Page ID
	var $PageID = 'view';

	// Project ID
	var $ProjectID = "{04439FF7-B43F-460F-8514-F71C8FF9E679}";

	// Table name
	var $TableName = 'Empl';

	// Page object name
	var $PageObjName = 'Empl_view';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Page URLs
	var $AddUrl;
	var $EditUrl;
	var $CopyUrl;
	var $DeleteUrl;
	var $ViewUrl;
	var $ListUrl;

	// Export URLs
	var $ExportPrintUrl;
	var $ExportHtmlUrl;
	var $ExportExcelUrl;
	var $ExportWordUrl;
	var $ExportXmlUrl;
	var $ExportCsvUrl;
	var $ExportPdfUrl;

	// Custom export
	var $ExportExcelCustom = FALSE;
	var $ExportWordCustom = FALSE;
	var $ExportPdfCustom = FALSE;
	var $ExportEmailCustom = FALSE;

	// Update URLs
	var $InlineAddUrl;
	var $InlineCopyUrl;
	var $InlineEditUrl;
	var $GridAddUrl;
	var $GridEditUrl;
	var $MultiDeleteUrl;
	var $MultiUpdateUrl;

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (Empl)
		if (!isset($GLOBALS["Empl"]) || get_class($GLOBALS["Empl"]) == "cEmpl") {
			$GLOBALS["Empl"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["Empl"];
		}
		$KeyUrl = "";
		if (@$_GET["EmpCodi"] <> "") {
			$this->RecKey["EmpCodi"] = $_GET["EmpCodi"];
			$KeyUrl .= "&amp;EmpCodi=" . urlencode($this->RecKey["EmpCodi"]);
		}
		$this->ExportPrintUrl = $this->PageUrl() . "export=print" . $KeyUrl;
		$this->ExportHtmlUrl = $this->PageUrl() . "export=html" . $KeyUrl;
		$this->ExportExcelUrl = $this->PageUrl() . "export=excel" . $KeyUrl;
		$this->ExportWordUrl = $this->PageUrl() . "export=word" . $KeyUrl;
		$this->ExportXmlUrl = $this->PageUrl() . "export=xml" . $KeyUrl;
		$this->ExportCsvUrl = $this->PageUrl() . "export=csv" . $KeyUrl;
		$this->ExportPdfUrl = $this->PageUrl() . "export=pdf" . $KeyUrl;

		// Table object (Usua)
		if (!isset($GLOBALS['Usua'])) $GLOBALS['Usua'] = new cUsua();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'view', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'Empl', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (Usua)
		if (!isset($UserTable)) {
			$UserTable = new cUsua();
			$UserTableConn = Conn($UserTable->DBID);
		}

		// Export options
		$this->ExportOptions = new cListOptions();
		$this->ExportOptions->Tag = "div";
		$this->ExportOptions->TagClassName = "ewExportOption";

		// Other options
		$this->OtherOptions['action'] = new cListOptions();
		$this->OtherOptions['action']->Tag = "div";
		$this->OtherOptions['action']->TagClassName = "ewActionOption";
		$this->OtherOptions['detail'] = new cListOptions();
		$this->OtherOptions['detail']->Tag = "div";
		$this->OtherOptions['detail']->TagClassName = "ewDetailOption";
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanView()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage($Language->Phrase("NoPermission")); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("Empllist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $Empl;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($Empl);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $ExportOptions; // Export options
	var $OtherOptions = array(); // Other options
	var $DisplayRecs = 1;
	var $DbMasterFilter;
	var $DbDetailFilter;
	var $StartRec;
	var $StopRec;
	var $TotalRecs = 0;
	var $RecRange = 10;
	var $RecCnt;
	var $RecKey = array();
	var $Recordset;

	//
	// Page main
	//
	function Page_Main() {
		global $Language;
		$sReturnUrl = "";
		$bMatchRecord = FALSE;

		// Set up Breadcrumb
		if ($this->Export == "")
			$this->SetupBreadcrumb();
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET["EmpCodi"] <> "") {
				$this->EmpCodi->setQueryStringValue($_GET["EmpCodi"]);
				$this->RecKey["EmpCodi"] = $this->EmpCodi->QueryStringValue;
			} elseif (@$_POST["EmpCodi"] <> "") {
				$this->EmpCodi->setFormValue($_POST["EmpCodi"]);
				$this->RecKey["EmpCodi"] = $this->EmpCodi->FormValue;
			} else {
				$sReturnUrl = "Empllist.php"; // Return to list
			}

			// Get action
			$this->CurrentAction = "I"; // Display form
			switch ($this->CurrentAction) {
				case "I": // Get a record to display
					if (!$this->LoadRow()) { // Load record based on key
						if ($this->getSuccessMessage() == "" && $this->getFailureMessage() == "")
							$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record message
						$sReturnUrl = "Empllist.php"; // No matching record, return to list
					}
			}
		} else {
			$sReturnUrl = "Empllist.php"; // Not page request, return to list
		}
		if ($sReturnUrl <> "")
			$this->Page_Terminate($sReturnUrl);

		// Render row
		$this->RowType = EW_ROWTYPE_VIEW;
		$this->ResetAttrs();
		$this->RenderRow();
	}

	// Set up other options
	function SetupOtherOptions() {
		global $Language, $Security;
		$options = &$this->OtherOptions;
		$option = &$options["action"];

		// Add
		$item = &$option->Add("add");
		$item->Body = "<a class=\"ewAction ewAdd\" title=\"" . ew_HtmlTitle($Language->Phrase("ViewPageAddLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("ViewPageAddLink")) . "\" href=\"" . ew_HtmlEncode($this->AddUrl) . "\">" . $Language->Phrase("ViewPageAddLink") . "</a>";
		$item->Visible = ($this->AddUrl <> "" && $Security->CanAdd());

		// Edit
		$item = &$option->Add("edit");
		$item->Body = "<a class=\"ewAction ewEdit\" title=\"" . ew_HtmlTitle($Language->Phrase("ViewPageEditLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("ViewPageEditLink")) . "\" href=\"" . ew_HtmlEncode($this->EditUrl) . "\">" . $Language->Phrase("ViewPageEditLink") . "</a>";
		$item->Visible = ($this->EditUrl <> "" && $Security->CanEdit());

		// Copy
		$item = &$option->Add("copy");
		$item->Body = "<a class=\"ewAction ewCopy\" title=\"" . ew_HtmlTitle($Language->Phrase("ViewPageCopyLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("ViewPageCopyLink")) . "\" href=\"" . ew_HtmlEncode($this->CopyUrl) . "\">" . $Language->Phrase("ViewPageCopyLink") . "</a>";
		$item->Visible = ($this->CopyUrl <> "" && $Security->CanAdd());

		// Set up action default
		$option = &$options["action"];
		$option->DropDownButtonPhrase = $Language->Phrase("ButtonActions");
		$option->UseImageAndText = TRUE;
		$option->UseDropDownButton = FALSE;
		$option->UseButtonGroup = TRUE;
		$item = &$option->Add($option->GroupOptionName);
		$item->Body = "";
		$item->Visible = FALSE;
	}

	// Set up starting record parameters
	function SetUpStartRec() {
		if ($this->DisplayRecs == 0)
			return;
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET[EW_TABLE_START_REC] <> "") { // Check for "start" parameter
				$this->StartRec = $_GET[EW_TABLE_START_REC];
				$this->setStartRecordNumber($this->StartRec);
			} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
				$PageNo = $_GET[EW_TABLE_PAGE_NO];
				if (is_numeric($PageNo)) {
					$this->StartRec = ($PageNo-1)*$this->DisplayRecs+1;
					if ($this->StartRec <= 0) {
						$this->StartRec = 1;
					} elseif ($this->StartRec >= intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1) {
						$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1;
					}
					$this->setStartRecordNumber($this->StartRec);
				}
			}
		}
		$this->StartRec = $this->getStartRecordNumber();

		// Check if correct start record counter
		if (!is_numeric($this->StartRec) || $this->StartRec == "") { // Avoid invalid start record counter
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} elseif (intval($this->StartRec) > intval($this->TotalRecs)) { // Avoid starting record > total records
			$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to last page first record
			$this->setStartRecordNumber($this->StartRec);
		} elseif (($this->StartRec-1) % $this->DisplayRecs <> 0) {
			$this->StartRec = intval(($this->StartRec-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to page boundary
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->EmpCodi->setDbValue($rs->fields('EmpCodi'));
		$this->EmpCarg->setDbValue($rs->fields('EmpCarg'));
		$this->EmpFIng->setDbValue($rs->fields('EmpFIng'));
		$this->EmpFSal->setDbValue($rs->fields('EmpFSal'));
		$this->EmpSala->setDbValue($rs->fields('EmpSala'));
		$this->EmpNomb->setDbValue($rs->fields('EmpNomb'));
		$this->EmpApel->setDbValue($rs->fields('EmpApel'));
		$this->EmpCedu->setDbValue($rs->fields('EmpCedu'));
		$this->EmpTele->setDbValue($rs->fields('EmpTele'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->EmpCodi->DbValue = $row['EmpCodi'];
		$this->EmpCarg->DbValue = $row['EmpCarg'];
		$this->EmpFIng->DbValue = $row['EmpFIng'];
		$this->EmpFSal->DbValue = $row['EmpFSal'];
		$this->EmpSala->DbValue = $row['EmpSala'];
		$this->EmpNomb->DbValue = $row['EmpNomb'];
		$this->EmpApel->DbValue = $row['EmpApel'];
		$this->EmpCedu->DbValue = $row['EmpCedu'];
		$this->EmpTele->DbValue = $row['EmpTele'];
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		$this->AddUrl = $this->GetAddUrl();
		$this->EditUrl = $this->GetEditUrl();
		$this->CopyUrl = $this->GetCopyUrl();
		$this->DeleteUrl = $this->GetDeleteUrl();
		$this->ListUrl = $this->GetListUrl();
		$this->SetupOtherOptions();

		// Convert decimal values if posted back
		if ($this->EmpSala->FormValue == $this->EmpSala->CurrentValue && is_numeric(ew_StrToFloat($this->EmpSala->CurrentValue)))
			$this->EmpSala->CurrentValue = ew_StrToFloat($this->EmpSala->CurrentValue);

		// Call Row_Rendering event
		$this->Row_Rendering();

		// Common render codes for all row types
		// EmpCodi
		// EmpCarg
		// EmpFIng
		// EmpFSal
		// EmpSala
		// EmpNomb
		// EmpApel
		// EmpCedu
		// EmpTele

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// EmpCodi
		$this->EmpCodi->ViewValue = $this->EmpCodi->CurrentValue;
		$this->EmpCodi->ViewCustomAttributes = "";

		// EmpCarg
		$this->EmpCarg->ViewValue = $this->EmpCarg->CurrentValue;
		$this->EmpCarg->ViewCustomAttributes = "";

		// EmpFIng
		$this->EmpFIng->ViewValue = $this->EmpFIng->CurrentValue;
		$this->EmpFIng->ViewValue = ew_FormatDateTime($this->EmpFIng->ViewValue, 7);
		$this->EmpFIng->ViewCustomAttributes = "";

		// EmpFSal
		$this->EmpFSal->ViewValue = $this->EmpFSal->CurrentValue;
		$this->EmpFSal->ViewValue = ew_FormatDateTime($this->EmpFSal->ViewValue, 7);
		$this->EmpFSal->ViewCustomAttributes = "";

		// EmpSala
		$this->EmpSala->ViewValue = $this->EmpSala->CurrentValue;
		$this->EmpSala->ViewValue = ew_FormatNumber($this->EmpSala->ViewValue, 0, -2, -2, -2);
		$this->EmpSala->ViewCustomAttributes = "";

		// EmpNomb
		$this->EmpNomb->ViewValue = $this->EmpNomb->CurrentValue;
		$this->EmpNomb->ViewCustomAttributes = "";

		// EmpApel
		$this->EmpApel->ViewValue = $this->EmpApel->CurrentValue;
		$this->EmpApel->ViewCustomAttributes = "";

		// EmpCedu
		$this->EmpCedu->ViewValue = $this->EmpCedu->CurrentValue;
		$this->EmpCedu->ViewCustomAttributes = "";

		// EmpTele
		$this->EmpTele->ViewValue = $this->EmpTele->CurrentValue;
		$this->EmpTele->ViewCustomAttributes = "";

			// EmpCarg
			$this->EmpCarg->LinkCustomAttributes = "";
			$this->EmpCarg->HrefValue = "";
			$this->EmpCarg->TooltipValue = "";

			// EmpFIng
			$this->EmpFIng->LinkCustomAttributes = "";
			$this->EmpFIng->HrefValue = "";
			$this->EmpFIng->TooltipValue = "";

			// EmpFSal
			$this->EmpFSal->LinkCustomAttributes = "";
			$this->EmpFSal->HrefValue = "";
			$this->EmpFSal->TooltipValue = "";

			// EmpSala
			$this->EmpSala->LinkCustomAttributes = "";
			$this->EmpSala->HrefValue = "";
			$this->EmpSala->TooltipValue = "";

			// EmpNomb
			$this->EmpNomb->LinkCustomAttributes = "";
			$this->EmpNomb->HrefValue = "";
			$this->EmpNomb->TooltipValue = "";

			// EmpApel
			$this->EmpApel->LinkCustomAttributes = "";
			$this->EmpApel->HrefValue = "";
			$this->EmpApel->TooltipValue = "";

			// EmpCedu
			$this->EmpCedu->LinkCustomAttributes = "";
			$this->EmpCedu->HrefValue = "";
			$this->EmpCedu->TooltipValue = "";

			// EmpTele
			$this->EmpTele->LinkCustomAttributes = "";
			$this->EmpTele->HrefValue = "";
			$this->EmpTele->TooltipValue = "";
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, "Empllist.php", "", $this->TableVar, TRUE);
		$PageId = "view";
		$Breadcrumb->Add("view", $PageId, $url);
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Page Exporting event
	// $this->ExportDoc = export document object
	function Page_Exporting() {

		//$this->ExportDoc->Text = "my header"; // Export header
		//return FALSE; // Return FALSE to skip default export and use Row_Export event

		return TRUE; // Return TRUE to use default export and skip Row_Export event
	}

	// Row Export event
	// $this->ExportDoc = export document object
	function Row_Export($rs) {

	    //$this->ExportDoc->Text .= "my content"; // Build HTML with field value: $rs["MyField"] or $this->MyField->ViewValue
	}

	// Page Exported event
	// $this->ExportDoc = export document object
	function Page_Exported() {

		//$this->ExportDoc->Text .= "my footer"; // Export footer
		//echo $this->ExportDoc->Text;

	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($Empl_view)) $Empl_view = new cEmpl_view();

// Page init
$Empl_view->Page_Init();

// Page main
$Empl_view->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$Empl_view->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "view";
var CurrentForm = fEmplview = new ew_Form("fEmplview", "view");

// Form_CustomValidate event
fEmplview.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fEmplview.ValidateRequired = true;
<?php } else { ?>
fEmplview.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
// Form object for search

</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php $Empl_view->ExportOptions->Render("body") ?>
<?php
	foreach ($Empl_view->OtherOptions as &$option)
		$option->Render("body");
?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php $Empl_view->ShowPageHeader(); ?>
<?php
$Empl_view->ShowMessage();
?>
<form name="fEmplview" id="fEmplview" class="form-inline ewForm ewViewForm" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($Empl_view->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $Empl_view->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="Empl">
<table class="table table-bordered table-striped ewViewTable">
<?php if ($Empl->EmpCarg->Visible) { // EmpCarg ?>
	<tr id="r_EmpCarg">
		<td><span id="elh_Empl_EmpCarg"><?php echo $Empl->EmpCarg->FldCaption() ?></span></td>
		<td data-name="EmpCarg"<?php echo $Empl->EmpCarg->CellAttributes() ?>>
<span id="el_Empl_EmpCarg">
<span<?php echo $Empl->EmpCarg->ViewAttributes() ?>>
<?php echo $Empl->EmpCarg->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($Empl->EmpFIng->Visible) { // EmpFIng ?>
	<tr id="r_EmpFIng">
		<td><span id="elh_Empl_EmpFIng"><?php echo $Empl->EmpFIng->FldCaption() ?></span></td>
		<td data-name="EmpFIng"<?php echo $Empl->EmpFIng->CellAttributes() ?>>
<span id="el_Empl_EmpFIng">
<span<?php echo $Empl->EmpFIng->ViewAttributes() ?>>
<?php echo $Empl->EmpFIng->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($Empl->EmpFSal->Visible) { // EmpFSal ?>
	<tr id="r_EmpFSal">
		<td><span id="elh_Empl_EmpFSal"><?php echo $Empl->EmpFSal->FldCaption() ?></span></td>
		<td data-name="EmpFSal"<?php echo $Empl->EmpFSal->CellAttributes() ?>>
<span id="el_Empl_EmpFSal">
<span<?php echo $Empl->EmpFSal->ViewAttributes() ?>>
<?php echo $Empl->EmpFSal->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($Empl->EmpSala->Visible) { // EmpSala ?>
	<tr id="r_EmpSala">
		<td><span id="elh_Empl_EmpSala"><?php echo $Empl->EmpSala->FldCaption() ?></span></td>
		<td data-name="EmpSala"<?php echo $Empl->EmpSala->CellAttributes() ?>>
<span id="el_Empl_EmpSala">
<span<?php echo $Empl->EmpSala->ViewAttributes() ?>>
<?php echo $Empl->EmpSala->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($Empl->EmpNomb->Visible) { // EmpNomb ?>
	<tr id="r_EmpNomb">
		<td><span id="elh_Empl_EmpNomb"><?php echo $Empl->EmpNomb->FldCaption() ?></span></td>
		<td data-name="EmpNomb"<?php echo $Empl->EmpNomb->CellAttributes() ?>>
<span id="el_Empl_EmpNomb">
<span<?php echo $Empl->EmpNomb->ViewAttributes() ?>>
<?php echo $Empl->EmpNomb->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($Empl->EmpApel->Visible) { // EmpApel ?>
	<tr id="r_EmpApel">
		<td><span id="elh_Empl_EmpApel"><?php echo $Empl->EmpApel->FldCaption() ?></span></td>
		<td data-name="EmpApel"<?php echo $Empl->EmpApel->CellAttributes() ?>>
<span id="el_Empl_EmpApel">
<span<?php echo $Empl->EmpApel->ViewAttributes() ?>>
<?php echo $Empl->EmpApel->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($Empl->EmpCedu->Visible) { // EmpCedu ?>
	<tr id="r_EmpCedu">
		<td><span id="elh_Empl_EmpCedu"><?php echo $Empl->EmpCedu->FldCaption() ?></span></td>
		<td data-name="EmpCedu"<?php echo $Empl->EmpCedu->CellAttributes() ?>>
<span id="el_Empl_EmpCedu">
<span<?php echo $Empl->EmpCedu->ViewAttributes() ?>>
<?php echo $Empl->EmpCedu->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($Empl->EmpTele->Visible) { // EmpTele ?>
	<tr id="r_EmpTele">
		<td><span id="elh_Empl_EmpTele"><?php echo $Empl->EmpTele->FldCaption() ?></span></td>
		<td data-name="EmpTele"<?php echo $Empl->EmpTele->CellAttributes() ?>>
<span id="el_Empl_EmpTele">
<span<?php echo $Empl->EmpTele->ViewAttributes() ?>>
<?php echo $Empl->EmpTele->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
</table>
</form>
<script type="text/javascript">
fEmplview.Init();
</script>
<?php
$Empl_view->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$Empl_view->Page_Terminate();
?>
