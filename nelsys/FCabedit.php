<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "FCabinfo.php" ?>
<?php include_once "Usuainfo.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$FCab_edit = NULL; // Initialize page object first

class cFCab_edit extends cFCab {

	// Page ID
	var $PageID = 'edit';

	// Project ID
	var $ProjectID = "{04439FF7-B43F-460F-8514-F71C8FF9E679}";

	// Table name
	var $TableName = 'FCab';

	// Page object name
	var $PageObjName = 'FCab_edit';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (FCab)
		if (!isset($GLOBALS["FCab"]) || get_class($GLOBALS["FCab"]) == "cFCab") {
			$GLOBALS["FCab"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["FCab"];
		}

		// Table object (Usua)
		if (!isset($GLOBALS['Usua'])) $GLOBALS['Usua'] = new cUsua();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'edit', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'FCab', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (Usua)
		if (!isset($UserTable)) {
			$UserTable = new cUsua();
			$UserTableConn = Conn($UserTable->DBID);
		}
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanEdit()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage($Language->Phrase("NoPermission")); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("FCablist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}

		// Create form object
		$objForm = new cFormObj();
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action
		$this->FcaCodi->Visible = !$this->IsAdd() && !$this->IsCopy() && !$this->IsGridAdd();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Process auto fill
		if (@$_POST["ajax"] == "autofill") {
			$results = $this->GetAutoFill(@$_POST["name"], @$_POST["q"]);
			if ($results) {

				// Clean output buffer
				if (!EW_DEBUG_ENABLED && ob_get_length())
					ob_end_clean();
				echo $results;
				$this->Page_Terminate();
				exit();
			}
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $FCab;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($FCab);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $FormClassName = "form-horizontal ewForm ewEditForm";
	var $DbMasterFilter;
	var $DbDetailFilter;

	// 
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError;

		// Load key from QueryString
		if (@$_GET["FcaCodi"] <> "") {
			$this->FcaCodi->setQueryStringValue($_GET["FcaCodi"]);
		}

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Process form if post back
		if (@$_POST["a_edit"] <> "") {
			$this->CurrentAction = $_POST["a_edit"]; // Get action code
			$this->LoadFormValues(); // Get form values
		} else {
			$this->CurrentAction = "I"; // Default action is display
		}

		// Check if valid key
		if ($this->FcaCodi->CurrentValue == "")
			$this->Page_Terminate("FCablist.php"); // Invalid key, return to list

		// Validate form if post back
		if (@$_POST["a_edit"] <> "") {
			if (!$this->ValidateForm()) {
				$this->CurrentAction = ""; // Form error, reset action
				$this->setFailureMessage($gsFormError);
				$this->EventCancelled = TRUE; // Event cancelled
				$this->RestoreFormValues();
			}
		}
		switch ($this->CurrentAction) {
			case "I": // Get a record to display
				if (!$this->LoadRow()) { // Load record based on key
					if ($this->getFailureMessage() == "") $this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
					$this->Page_Terminate("FCablist.php"); // No matching record, return to list
				}
				break;
			Case "U": // Update
				$sReturnUrl = $this->getReturnUrl();
				$this->SendEmail = TRUE; // Send email on update success
				if ($this->EditRow()) { // Update record based on key
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage($Language->Phrase("UpdateSuccess")); // Update success
					$this->Page_Terminate($sReturnUrl); // Return to caller
				} elseif ($this->getFailureMessage() == $Language->Phrase("NoRecord")) {
					$this->Page_Terminate($sReturnUrl); // Return to caller
				} else {
					$this->EventCancelled = TRUE; // Event cancelled
					$this->RestoreFormValues(); // Restore form values if update failed
				}
		}

		// Render the record
		if ($this->CurrentAction == "F") { // Confirm page
			$this->RowType = EW_ROWTYPE_VIEW; // Render as View
		} else {
			$this->RowType = EW_ROWTYPE_EDIT; // Render as Edit
		}
		$this->ResetAttrs();
		$this->RenderRow();
	}

	// Set up starting record parameters
	function SetUpStartRec() {
		if ($this->DisplayRecs == 0)
			return;
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET[EW_TABLE_START_REC] <> "") { // Check for "start" parameter
				$this->StartRec = $_GET[EW_TABLE_START_REC];
				$this->setStartRecordNumber($this->StartRec);
			} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
				$PageNo = $_GET[EW_TABLE_PAGE_NO];
				if (is_numeric($PageNo)) {
					$this->StartRec = ($PageNo-1)*$this->DisplayRecs+1;
					if ($this->StartRec <= 0) {
						$this->StartRec = 1;
					} elseif ($this->StartRec >= intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1) {
						$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1;
					}
					$this->setStartRecordNumber($this->StartRec);
				}
			}
		}
		$this->StartRec = $this->getStartRecordNumber();

		// Check if correct start record counter
		if (!is_numeric($this->StartRec) || $this->StartRec == "") { // Avoid invalid start record counter
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} elseif (intval($this->StartRec) > intval($this->TotalRecs)) { // Avoid starting record > total records
			$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to last page first record
			$this->setStartRecordNumber($this->StartRec);
		} elseif (($this->StartRec-1) % $this->DisplayRecs <> 0) {
			$this->StartRec = intval(($this->StartRec-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to page boundary
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Get upload files
	function GetUploadFiles() {
		global $objForm, $Language;

		// Get upload data
	}

	// Load form values
	function LoadFormValues() {

		// Load from form
		global $objForm;
		if (!$this->FcaCodi->FldIsDetailKey)
			$this->FcaCodi->setFormValue($objForm->GetValue("x_FcaCodi"));
		if (!$this->OveCodi->FldIsDetailKey) {
			$this->OveCodi->setFormValue($objForm->GetValue("x_OveCodi"));
		}
		if (!$this->FcaVend->FldIsDetailKey) {
			$this->FcaVend->setFormValue($objForm->GetValue("x_FcaVend"));
		}
		if (!$this->FcaCaje->FldIsDetailKey) {
			$this->FcaCaje->setFormValue($objForm->GetValue("x_FcaCaje"));
		}
		if (!$this->FcaFech->FldIsDetailKey) {
			$this->FcaFech->setFormValue($objForm->GetValue("x_FcaFech"));
			$this->FcaFech->CurrentValue = ew_UnFormatDateTime($this->FcaFech->CurrentValue, 7);
		}
		if (!$this->FcaTimb->FldIsDetailKey) {
			$this->FcaTimb->setFormValue($objForm->GetValue("x_FcaTimb"));
		}
		if (!$this->FcaTFac->FldIsDetailKey) {
			$this->FcaTFac->setFormValue($objForm->GetValue("x_FcaTFac"));
		}
		if (!$this->FcaAnul->FldIsDetailKey) {
			$this->FcaAnul->setFormValue($objForm->GetValue("x_FcaAnul"));
		}
		if (!$this->FcaMAnu->FldIsDetailKey) {
			$this->FcaMAnu->setFormValue($objForm->GetValue("x_FcaMAnu"));
		}
		if (!$this->FcaFAnu->FldIsDetailKey) {
			$this->FcaFAnu->setFormValue($objForm->GetValue("x_FcaFAnu"));
			$this->FcaFAnu->CurrentValue = ew_UnFormatDateTime($this->FcaFAnu->CurrentValue, 7);
		}
		if (!$this->FcaFTip->FldIsDetailKey) {
			$this->FcaFTip->setFormValue($objForm->GetValue("x_FcaFTip"));
		}
		if (!$this->FcaUsua->FldIsDetailKey) {
			$this->FcaUsua->setFormValue($objForm->GetValue("x_FcaUsua"));
		}
		if (!$this->FcaFCre->FldIsDetailKey) {
			$this->FcaFCre->setFormValue($objForm->GetValue("x_FcaFCre"));
			$this->FcaFCre->CurrentValue = ew_UnFormatDateTime($this->FcaFCre->CurrentValue, 7);
		}
	}

	// Restore form values
	function RestoreFormValues() {
		global $objForm;
		$this->LoadRow();
		$this->FcaCodi->CurrentValue = $this->FcaCodi->FormValue;
		$this->OveCodi->CurrentValue = $this->OveCodi->FormValue;
		$this->FcaVend->CurrentValue = $this->FcaVend->FormValue;
		$this->FcaCaje->CurrentValue = $this->FcaCaje->FormValue;
		$this->FcaFech->CurrentValue = $this->FcaFech->FormValue;
		$this->FcaFech->CurrentValue = ew_UnFormatDateTime($this->FcaFech->CurrentValue, 7);
		$this->FcaTimb->CurrentValue = $this->FcaTimb->FormValue;
		$this->FcaTFac->CurrentValue = $this->FcaTFac->FormValue;
		$this->FcaAnul->CurrentValue = $this->FcaAnul->FormValue;
		$this->FcaMAnu->CurrentValue = $this->FcaMAnu->FormValue;
		$this->FcaFAnu->CurrentValue = $this->FcaFAnu->FormValue;
		$this->FcaFAnu->CurrentValue = ew_UnFormatDateTime($this->FcaFAnu->CurrentValue, 7);
		$this->FcaFTip->CurrentValue = $this->FcaFTip->FormValue;
		$this->FcaUsua->CurrentValue = $this->FcaUsua->FormValue;
		$this->FcaFCre->CurrentValue = $this->FcaFCre->FormValue;
		$this->FcaFCre->CurrentValue = ew_UnFormatDateTime($this->FcaFCre->CurrentValue, 7);
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->FcaCodi->setDbValue($rs->fields('FcaCodi'));
		$this->OveCodi->setDbValue($rs->fields('OveCodi'));
		$this->FcaVend->setDbValue($rs->fields('FcaVend'));
		$this->FcaCaje->setDbValue($rs->fields('FcaCaje'));
		$this->FcaFech->setDbValue($rs->fields('FcaFech'));
		$this->FcaTimb->setDbValue($rs->fields('FcaTimb'));
		$this->FcaTFac->setDbValue($rs->fields('FcaTFac'));
		$this->FcaAnul->setDbValue($rs->fields('FcaAnul'));
		$this->FcaMAnu->setDbValue($rs->fields('FcaMAnu'));
		$this->FcaFAnu->setDbValue($rs->fields('FcaFAnu'));
		$this->FcaFTip->setDbValue($rs->fields('FcaFTip'));
		$this->FcaUsua->setDbValue($rs->fields('FcaUsua'));
		$this->FcaFCre->setDbValue($rs->fields('FcaFCre'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->FcaCodi->DbValue = $row['FcaCodi'];
		$this->OveCodi->DbValue = $row['OveCodi'];
		$this->FcaVend->DbValue = $row['FcaVend'];
		$this->FcaCaje->DbValue = $row['FcaCaje'];
		$this->FcaFech->DbValue = $row['FcaFech'];
		$this->FcaTimb->DbValue = $row['FcaTimb'];
		$this->FcaTFac->DbValue = $row['FcaTFac'];
		$this->FcaAnul->DbValue = $row['FcaAnul'];
		$this->FcaMAnu->DbValue = $row['FcaMAnu'];
		$this->FcaFAnu->DbValue = $row['FcaFAnu'];
		$this->FcaFTip->DbValue = $row['FcaFTip'];
		$this->FcaUsua->DbValue = $row['FcaUsua'];
		$this->FcaFCre->DbValue = $row['FcaFCre'];
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		// Convert decimal values if posted back

		if ($this->FcaTFac->FormValue == $this->FcaTFac->CurrentValue && is_numeric(ew_StrToFloat($this->FcaTFac->CurrentValue)))
			$this->FcaTFac->CurrentValue = ew_StrToFloat($this->FcaTFac->CurrentValue);

		// Call Row_Rendering event
		$this->Row_Rendering();

		// Common render codes for all row types
		// FcaCodi
		// OveCodi
		// FcaVend
		// FcaCaje
		// FcaFech
		// FcaTimb
		// FcaTFac
		// FcaAnul
		// FcaMAnu
		// FcaFAnu
		// FcaFTip
		// FcaUsua
		// FcaFCre

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// FcaCodi
		$this->FcaCodi->ViewValue = $this->FcaCodi->CurrentValue;
		$this->FcaCodi->ViewCustomAttributes = "";

		// OveCodi
		if (strval($this->OveCodi->CurrentValue) <> "") {
			$sFilterWrk = "\"OveCodi\"" . ew_SearchString("=", $this->OveCodi->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT \"OveCodi\", \"OveCodi\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"OVen\"";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->OveCodi, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->OveCodi->ViewValue = $this->OveCodi->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->OveCodi->ViewValue = $this->OveCodi->CurrentValue;
			}
		} else {
			$this->OveCodi->ViewValue = NULL;
		}
		$this->OveCodi->ViewCustomAttributes = "";

		// FcaVend
		$this->FcaVend->ViewValue = $this->FcaVend->CurrentValue;
		$this->FcaVend->ViewCustomAttributes = "";

		// FcaCaje
		$this->FcaCaje->ViewValue = $this->FcaCaje->CurrentValue;
		$this->FcaCaje->ViewCustomAttributes = "";

		// FcaFech
		$this->FcaFech->ViewValue = $this->FcaFech->CurrentValue;
		$this->FcaFech->ViewValue = ew_FormatDateTime($this->FcaFech->ViewValue, 7);
		$this->FcaFech->ViewCustomAttributes = "";

		// FcaTimb
		$this->FcaTimb->ViewValue = $this->FcaTimb->CurrentValue;
		$this->FcaTimb->ViewCustomAttributes = "";

		// FcaTFac
		$this->FcaTFac->ViewValue = $this->FcaTFac->CurrentValue;
		$this->FcaTFac->ViewCustomAttributes = "";

		// FcaAnul
		$this->FcaAnul->ViewValue = $this->FcaAnul->CurrentValue;
		$this->FcaAnul->ViewCustomAttributes = "";

		// FcaMAnu
		$this->FcaMAnu->ViewValue = $this->FcaMAnu->CurrentValue;
		$this->FcaMAnu->ViewCustomAttributes = "";

		// FcaFAnu
		$this->FcaFAnu->ViewValue = $this->FcaFAnu->CurrentValue;
		$this->FcaFAnu->ViewValue = ew_FormatDateTime($this->FcaFAnu->ViewValue, 7);
		$this->FcaFAnu->ViewCustomAttributes = "";

		// FcaFTip
		$this->FcaFTip->ViewValue = $this->FcaFTip->CurrentValue;
		$this->FcaFTip->ViewCustomAttributes = "";

		// FcaUsua
		$this->FcaUsua->ViewValue = $this->FcaUsua->CurrentValue;
		$this->FcaUsua->ViewCustomAttributes = "";

		// FcaFCre
		$this->FcaFCre->ViewValue = $this->FcaFCre->CurrentValue;
		$this->FcaFCre->ViewValue = ew_FormatDateTime($this->FcaFCre->ViewValue, 7);
		$this->FcaFCre->ViewCustomAttributes = "";

			// FcaCodi
			$this->FcaCodi->LinkCustomAttributes = "";
			$this->FcaCodi->HrefValue = "";
			$this->FcaCodi->TooltipValue = "";

			// OveCodi
			$this->OveCodi->LinkCustomAttributes = "";
			$this->OveCodi->HrefValue = "";
			$this->OveCodi->TooltipValue = "";

			// FcaVend
			$this->FcaVend->LinkCustomAttributes = "";
			$this->FcaVend->HrefValue = "";
			$this->FcaVend->TooltipValue = "";

			// FcaCaje
			$this->FcaCaje->LinkCustomAttributes = "";
			$this->FcaCaje->HrefValue = "";
			$this->FcaCaje->TooltipValue = "";

			// FcaFech
			$this->FcaFech->LinkCustomAttributes = "";
			$this->FcaFech->HrefValue = "";
			$this->FcaFech->TooltipValue = "";

			// FcaTimb
			$this->FcaTimb->LinkCustomAttributes = "";
			$this->FcaTimb->HrefValue = "";
			$this->FcaTimb->TooltipValue = "";

			// FcaTFac
			$this->FcaTFac->LinkCustomAttributes = "";
			$this->FcaTFac->HrefValue = "";
			$this->FcaTFac->TooltipValue = "";

			// FcaAnul
			$this->FcaAnul->LinkCustomAttributes = "";
			$this->FcaAnul->HrefValue = "";
			$this->FcaAnul->TooltipValue = "";

			// FcaMAnu
			$this->FcaMAnu->LinkCustomAttributes = "";
			$this->FcaMAnu->HrefValue = "";
			$this->FcaMAnu->TooltipValue = "";

			// FcaFAnu
			$this->FcaFAnu->LinkCustomAttributes = "";
			$this->FcaFAnu->HrefValue = "";
			$this->FcaFAnu->TooltipValue = "";

			// FcaFTip
			$this->FcaFTip->LinkCustomAttributes = "";
			$this->FcaFTip->HrefValue = "";
			$this->FcaFTip->TooltipValue = "";

			// FcaUsua
			$this->FcaUsua->LinkCustomAttributes = "";
			$this->FcaUsua->HrefValue = "";
			$this->FcaUsua->TooltipValue = "";

			// FcaFCre
			$this->FcaFCre->LinkCustomAttributes = "";
			$this->FcaFCre->HrefValue = "";
			$this->FcaFCre->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_EDIT) { // Edit row

			// FcaCodi
			$this->FcaCodi->EditAttrs["class"] = "form-control";
			$this->FcaCodi->EditCustomAttributes = "";
			$this->FcaCodi->EditValue = $this->FcaCodi->CurrentValue;
			$this->FcaCodi->ViewCustomAttributes = "";

			// OveCodi
			$this->OveCodi->EditAttrs["class"] = "form-control";
			$this->OveCodi->EditCustomAttributes = "";
			if (trim(strval($this->OveCodi->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "\"OveCodi\"" . ew_SearchString("=", $this->OveCodi->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT \"OveCodi\", \"OveCodi\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\", '' AS \"SelectFilterFld\", '' AS \"SelectFilterFld2\", '' AS \"SelectFilterFld3\", '' AS \"SelectFilterFld4\" FROM \"public\".\"OVen\"";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->OveCodi, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->OveCodi->EditValue = $arwrk;

			// FcaVend
			$this->FcaVend->EditAttrs["class"] = "form-control";
			$this->FcaVend->EditCustomAttributes = "";
			$this->FcaVend->EditValue = ew_HtmlEncode($this->FcaVend->CurrentValue);
			$this->FcaVend->PlaceHolder = ew_RemoveHtml($this->FcaVend->FldCaption());

			// FcaCaje
			$this->FcaCaje->EditAttrs["class"] = "form-control";
			$this->FcaCaje->EditCustomAttributes = "";
			$this->FcaCaje->EditValue = ew_HtmlEncode($this->FcaCaje->CurrentValue);
			$this->FcaCaje->PlaceHolder = ew_RemoveHtml($this->FcaCaje->FldCaption());

			// FcaFech
			$this->FcaFech->EditAttrs["class"] = "form-control";
			$this->FcaFech->EditCustomAttributes = "";
			$this->FcaFech->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->FcaFech->CurrentValue, 7));
			$this->FcaFech->PlaceHolder = ew_RemoveHtml($this->FcaFech->FldCaption());

			// FcaTimb
			$this->FcaTimb->EditAttrs["class"] = "form-control";
			$this->FcaTimb->EditCustomAttributes = "";
			$this->FcaTimb->EditValue = ew_HtmlEncode($this->FcaTimb->CurrentValue);
			$this->FcaTimb->PlaceHolder = ew_RemoveHtml($this->FcaTimb->FldCaption());

			// FcaTFac
			$this->FcaTFac->EditAttrs["class"] = "form-control";
			$this->FcaTFac->EditCustomAttributes = "";
			$this->FcaTFac->EditValue = ew_HtmlEncode($this->FcaTFac->CurrentValue);
			$this->FcaTFac->PlaceHolder = ew_RemoveHtml($this->FcaTFac->FldCaption());
			if (strval($this->FcaTFac->EditValue) <> "" && is_numeric($this->FcaTFac->EditValue)) $this->FcaTFac->EditValue = ew_FormatNumber($this->FcaTFac->EditValue, -2, -1, -2, 0);

			// FcaAnul
			$this->FcaAnul->EditAttrs["class"] = "form-control";
			$this->FcaAnul->EditCustomAttributes = "";
			$this->FcaAnul->EditValue = ew_HtmlEncode($this->FcaAnul->CurrentValue);
			$this->FcaAnul->PlaceHolder = ew_RemoveHtml($this->FcaAnul->FldCaption());

			// FcaMAnu
			$this->FcaMAnu->EditAttrs["class"] = "form-control";
			$this->FcaMAnu->EditCustomAttributes = "";
			$this->FcaMAnu->EditValue = ew_HtmlEncode($this->FcaMAnu->CurrentValue);
			$this->FcaMAnu->PlaceHolder = ew_RemoveHtml($this->FcaMAnu->FldCaption());

			// FcaFAnu
			$this->FcaFAnu->EditAttrs["class"] = "form-control";
			$this->FcaFAnu->EditCustomAttributes = "";
			$this->FcaFAnu->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->FcaFAnu->CurrentValue, 7));
			$this->FcaFAnu->PlaceHolder = ew_RemoveHtml($this->FcaFAnu->FldCaption());

			// FcaFTip
			$this->FcaFTip->EditAttrs["class"] = "form-control";
			$this->FcaFTip->EditCustomAttributes = "";
			$this->FcaFTip->EditValue = ew_HtmlEncode($this->FcaFTip->CurrentValue);
			$this->FcaFTip->PlaceHolder = ew_RemoveHtml($this->FcaFTip->FldCaption());

			// FcaUsua
			$this->FcaUsua->EditAttrs["class"] = "form-control";
			$this->FcaUsua->EditCustomAttributes = "";
			$this->FcaUsua->EditValue = ew_HtmlEncode($this->FcaUsua->CurrentValue);
			$this->FcaUsua->PlaceHolder = ew_RemoveHtml($this->FcaUsua->FldCaption());

			// FcaFCre
			$this->FcaFCre->EditAttrs["class"] = "form-control";
			$this->FcaFCre->EditCustomAttributes = "";
			$this->FcaFCre->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->FcaFCre->CurrentValue, 7));
			$this->FcaFCre->PlaceHolder = ew_RemoveHtml($this->FcaFCre->FldCaption());

			// Edit refer script
			// FcaCodi

			$this->FcaCodi->HrefValue = "";

			// OveCodi
			$this->OveCodi->HrefValue = "";

			// FcaVend
			$this->FcaVend->HrefValue = "";

			// FcaCaje
			$this->FcaCaje->HrefValue = "";

			// FcaFech
			$this->FcaFech->HrefValue = "";

			// FcaTimb
			$this->FcaTimb->HrefValue = "";

			// FcaTFac
			$this->FcaTFac->HrefValue = "";

			// FcaAnul
			$this->FcaAnul->HrefValue = "";

			// FcaMAnu
			$this->FcaMAnu->HrefValue = "";

			// FcaFAnu
			$this->FcaFAnu->HrefValue = "";

			// FcaFTip
			$this->FcaFTip->HrefValue = "";

			// FcaUsua
			$this->FcaUsua->HrefValue = "";

			// FcaFCre
			$this->FcaFCre->HrefValue = "";
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate form
	function ValidateForm() {
		global $Language, $gsFormError;

		// Initialize form error message
		$gsFormError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return ($gsFormError == "");
		if (!$this->OveCodi->FldIsDetailKey && !is_null($this->OveCodi->FormValue) && $this->OveCodi->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->OveCodi->FldCaption(), $this->OveCodi->ReqErrMsg));
		}
		if (!$this->FcaVend->FldIsDetailKey && !is_null($this->FcaVend->FormValue) && $this->FcaVend->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->FcaVend->FldCaption(), $this->FcaVend->ReqErrMsg));
		}
		if (!ew_CheckInteger($this->FcaVend->FormValue)) {
			ew_AddMessage($gsFormError, $this->FcaVend->FldErrMsg());
		}
		if (!$this->FcaCaje->FldIsDetailKey && !is_null($this->FcaCaje->FormValue) && $this->FcaCaje->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->FcaCaje->FldCaption(), $this->FcaCaje->ReqErrMsg));
		}
		if (!ew_CheckInteger($this->FcaCaje->FormValue)) {
			ew_AddMessage($gsFormError, $this->FcaCaje->FldErrMsg());
		}
		if (!$this->FcaFech->FldIsDetailKey && !is_null($this->FcaFech->FormValue) && $this->FcaFech->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->FcaFech->FldCaption(), $this->FcaFech->ReqErrMsg));
		}
		if (!ew_CheckEuroDate($this->FcaFech->FormValue)) {
			ew_AddMessage($gsFormError, $this->FcaFech->FldErrMsg());
		}
		if (!$this->FcaTimb->FldIsDetailKey && !is_null($this->FcaTimb->FormValue) && $this->FcaTimb->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->FcaTimb->FldCaption(), $this->FcaTimb->ReqErrMsg));
		}
		if (!$this->FcaTFac->FldIsDetailKey && !is_null($this->FcaTFac->FormValue) && $this->FcaTFac->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->FcaTFac->FldCaption(), $this->FcaTFac->ReqErrMsg));
		}
		if (!ew_CheckNumber($this->FcaTFac->FormValue)) {
			ew_AddMessage($gsFormError, $this->FcaTFac->FldErrMsg());
		}
		if (!$this->FcaAnul->FldIsDetailKey && !is_null($this->FcaAnul->FormValue) && $this->FcaAnul->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->FcaAnul->FldCaption(), $this->FcaAnul->ReqErrMsg));
		}
		if (!ew_CheckEuroDate($this->FcaFAnu->FormValue)) {
			ew_AddMessage($gsFormError, $this->FcaFAnu->FldErrMsg());
		}
		if (!$this->FcaFTip->FldIsDetailKey && !is_null($this->FcaFTip->FormValue) && $this->FcaFTip->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->FcaFTip->FldCaption(), $this->FcaFTip->ReqErrMsg));
		}
		if (!$this->FcaUsua->FldIsDetailKey && !is_null($this->FcaUsua->FormValue) && $this->FcaUsua->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->FcaUsua->FldCaption(), $this->FcaUsua->ReqErrMsg));
		}
		if (!$this->FcaFCre->FldIsDetailKey && !is_null($this->FcaFCre->FormValue) && $this->FcaFCre->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->FcaFCre->FldCaption(), $this->FcaFCre->ReqErrMsg));
		}
		if (!ew_CheckEuroDate($this->FcaFCre->FormValue)) {
			ew_AddMessage($gsFormError, $this->FcaFCre->FldErrMsg());
		}

		// Return validate result
		$ValidateForm = ($gsFormError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateForm = $ValidateForm && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsFormError, $sFormCustomError);
		}
		return $ValidateForm;
	}

	// Update record based on key values
	function EditRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$conn = &$this->Connection();
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE)
			return FALSE;
		if ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record message
			$EditRow = FALSE; // Update Failed
		} else {

			// Save old values
			$rsold = &$rs->fields;
			$this->LoadDbValues($rsold);
			$rsnew = array();

			// OveCodi
			$this->OveCodi->SetDbValueDef($rsnew, $this->OveCodi->CurrentValue, 0, $this->OveCodi->ReadOnly);

			// FcaVend
			$this->FcaVend->SetDbValueDef($rsnew, $this->FcaVend->CurrentValue, 0, $this->FcaVend->ReadOnly);

			// FcaCaje
			$this->FcaCaje->SetDbValueDef($rsnew, $this->FcaCaje->CurrentValue, 0, $this->FcaCaje->ReadOnly);

			// FcaFech
			$this->FcaFech->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->FcaFech->CurrentValue, 7), ew_CurrentDate(), $this->FcaFech->ReadOnly);

			// FcaTimb
			$this->FcaTimb->SetDbValueDef($rsnew, $this->FcaTimb->CurrentValue, "", $this->FcaTimb->ReadOnly);

			// FcaTFac
			$this->FcaTFac->SetDbValueDef($rsnew, $this->FcaTFac->CurrentValue, 0, $this->FcaTFac->ReadOnly);

			// FcaAnul
			$this->FcaAnul->SetDbValueDef($rsnew, $this->FcaAnul->CurrentValue, "", $this->FcaAnul->ReadOnly);

			// FcaMAnu
			$this->FcaMAnu->SetDbValueDef($rsnew, $this->FcaMAnu->CurrentValue, NULL, $this->FcaMAnu->ReadOnly);

			// FcaFAnu
			$this->FcaFAnu->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->FcaFAnu->CurrentValue, 7), NULL, $this->FcaFAnu->ReadOnly);

			// FcaFTip
			$this->FcaFTip->SetDbValueDef($rsnew, $this->FcaFTip->CurrentValue, "", $this->FcaFTip->ReadOnly);

			// FcaUsua
			$this->FcaUsua->SetDbValueDef($rsnew, $this->FcaUsua->CurrentValue, "", $this->FcaUsua->ReadOnly);

			// FcaFCre
			$this->FcaFCre->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->FcaFCre->CurrentValue, 7), ew_CurrentDate(), $this->FcaFCre->ReadOnly);

			// Call Row Updating event
			$bUpdateRow = $this->Row_Updating($rsold, $rsnew);
			if ($bUpdateRow) {
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				if (count($rsnew) > 0)
					$EditRow = $this->Update($rsnew, "", $rsold);
				else
					$EditRow = TRUE; // No field to update
				$conn->raiseErrorFn = '';
				if ($EditRow) {
				}
			} else {
				if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

					// Use the message, do nothing
				} elseif ($this->CancelMessage <> "") {
					$this->setFailureMessage($this->CancelMessage);
					$this->CancelMessage = "";
				} else {
					$this->setFailureMessage($Language->Phrase("UpdateCancelled"));
				}
				$EditRow = FALSE;
			}
		}

		// Call Row_Updated event
		if ($EditRow)
			$this->Row_Updated($rsold, $rsnew);
		$rs->Close();
		return $EditRow;
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, "FCablist.php", "", $this->TableVar, TRUE);
		$PageId = "edit";
		$Breadcrumb->Add("edit", $PageId, $url);
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($FCab_edit)) $FCab_edit = new cFCab_edit();

// Page init
$FCab_edit->Page_Init();

// Page main
$FCab_edit->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$FCab_edit->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "edit";
var CurrentForm = fFCabedit = new ew_Form("fFCabedit", "edit");

// Validate form
fFCabedit.Validate = function() {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.GetForm(), $fobj = $(fobj);
	if ($fobj.find("#a_confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.FormKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = $fobj.find("#a_list").val() == "gridinsert";
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
			elm = this.GetElements("x" + infix + "_OveCodi");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $FCab->OveCodi->FldCaption(), $FCab->OveCodi->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_FcaVend");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $FCab->FcaVend->FldCaption(), $FCab->FcaVend->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_FcaVend");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($FCab->FcaVend->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_FcaCaje");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $FCab->FcaCaje->FldCaption(), $FCab->FcaCaje->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_FcaCaje");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($FCab->FcaCaje->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_FcaFech");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $FCab->FcaFech->FldCaption(), $FCab->FcaFech->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_FcaFech");
			if (elm && !ew_CheckEuroDate(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($FCab->FcaFech->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_FcaTimb");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $FCab->FcaTimb->FldCaption(), $FCab->FcaTimb->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_FcaTFac");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $FCab->FcaTFac->FldCaption(), $FCab->FcaTFac->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_FcaTFac");
			if (elm && !ew_CheckNumber(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($FCab->FcaTFac->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_FcaAnul");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $FCab->FcaAnul->FldCaption(), $FCab->FcaAnul->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_FcaFAnu");
			if (elm && !ew_CheckEuroDate(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($FCab->FcaFAnu->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_FcaFTip");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $FCab->FcaFTip->FldCaption(), $FCab->FcaFTip->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_FcaUsua");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $FCab->FcaUsua->FldCaption(), $FCab->FcaUsua->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_FcaFCre");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $FCab->FcaFCre->FldCaption(), $FCab->FcaFCre->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_FcaFCre");
			if (elm && !ew_CheckEuroDate(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($FCab->FcaFCre->FldErrMsg()) ?>");

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
	}

	// Process detail forms
	var dfs = $fobj.find("input[name='detailpage']").get();
	for (var i = 0; i < dfs.length; i++) {
		var df = dfs[i], val = df.value;
		if (val && ewForms[val])
			if (!ewForms[val].Validate())
				return false;
	}
	return true;
}

// Form_CustomValidate event
fFCabedit.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fFCabedit.ValidateRequired = true;
<?php } else { ?>
fFCabedit.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
fFCabedit.Lists["x_OveCodi"] = {"LinkField":"x_OveCodi","Ajax":true,"AutoFill":false,"DisplayFields":["x_OveCodi","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};

// Form object for search
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php $FCab_edit->ShowPageHeader(); ?>
<?php
$FCab_edit->ShowMessage();
?>
<form name="fFCabedit" id="fFCabedit" class="<?php echo $FCab_edit->FormClassName ?>" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($FCab_edit->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $FCab_edit->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="FCab">
<input type="hidden" name="a_edit" id="a_edit" value="U">
<?php if ($FCab->CurrentAction == "F") { // Confirm page ?>
<input type="hidden" name="a_confirm" id="a_confirm" value="F">
<?php } ?>
<div>
<?php if ($FCab->FcaCodi->Visible) { // FcaCodi ?>
	<div id="r_FcaCodi" class="form-group">
		<label id="elh_FCab_FcaCodi" class="col-sm-2 control-label ewLabel"><?php echo $FCab->FcaCodi->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $FCab->FcaCodi->CellAttributes() ?>>
<?php if ($FCab->CurrentAction <> "F") { ?>
<span id="el_FCab_FcaCodi">
<span<?php echo $FCab->FcaCodi->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $FCab->FcaCodi->EditValue ?></p></span>
</span>
<input type="hidden" data-table="FCab" data-field="x_FcaCodi" name="x_FcaCodi" id="x_FcaCodi" value="<?php echo ew_HtmlEncode($FCab->FcaCodi->CurrentValue) ?>">
<?php } else { ?>
<span id="el_FCab_FcaCodi">
<span<?php echo $FCab->FcaCodi->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $FCab->FcaCodi->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="FCab" data-field="x_FcaCodi" name="x_FcaCodi" id="x_FcaCodi" value="<?php echo ew_HtmlEncode($FCab->FcaCodi->FormValue) ?>">
<?php } ?>
<?php echo $FCab->FcaCodi->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($FCab->OveCodi->Visible) { // OveCodi ?>
	<div id="r_OveCodi" class="form-group">
		<label id="elh_FCab_OveCodi" for="x_OveCodi" class="col-sm-2 control-label ewLabel"><?php echo $FCab->OveCodi->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $FCab->OveCodi->CellAttributes() ?>>
<?php if ($FCab->CurrentAction <> "F") { ?>
<span id="el_FCab_OveCodi">
<select data-table="FCab" data-field="x_OveCodi" data-value-separator="<?php echo ew_HtmlEncode(is_array($FCab->OveCodi->DisplayValueSeparator) ? json_encode($FCab->OveCodi->DisplayValueSeparator) : $FCab->OveCodi->DisplayValueSeparator) ?>" id="x_OveCodi" name="x_OveCodi"<?php echo $FCab->OveCodi->EditAttributes() ?>>
<?php
if (is_array($FCab->OveCodi->EditValue)) {
	$arwrk = $FCab->OveCodi->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($FCab->OveCodi->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $FCab->OveCodi->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($FCab->OveCodi->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($FCab->OveCodi->CurrentValue) ?>" selected><?php echo $FCab->OveCodi->CurrentValue ?></option>
<?php
    }
}
?>
</select>
<?php
$sSqlWrk = "SELECT \"OveCodi\", \"OveCodi\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"OVen\"";
$sWhereWrk = "";
$FCab->OveCodi->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$FCab->OveCodi->LookupFilters += array("f0" => "\"OveCodi\" = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$FCab->Lookup_Selecting($FCab->OveCodi, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $FCab->OveCodi->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x_OveCodi" id="s_x_OveCodi" value="<?php echo $FCab->OveCodi->LookupFilterQuery() ?>">
</span>
<?php } else { ?>
<span id="el_FCab_OveCodi">
<span<?php echo $FCab->OveCodi->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $FCab->OveCodi->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="FCab" data-field="x_OveCodi" name="x_OveCodi" id="x_OveCodi" value="<?php echo ew_HtmlEncode($FCab->OveCodi->FormValue) ?>">
<?php } ?>
<?php echo $FCab->OveCodi->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($FCab->FcaVend->Visible) { // FcaVend ?>
	<div id="r_FcaVend" class="form-group">
		<label id="elh_FCab_FcaVend" for="x_FcaVend" class="col-sm-2 control-label ewLabel"><?php echo $FCab->FcaVend->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $FCab->FcaVend->CellAttributes() ?>>
<?php if ($FCab->CurrentAction <> "F") { ?>
<span id="el_FCab_FcaVend">
<input type="text" data-table="FCab" data-field="x_FcaVend" name="x_FcaVend" id="x_FcaVend" size="30" placeholder="<?php echo ew_HtmlEncode($FCab->FcaVend->getPlaceHolder()) ?>" value="<?php echo $FCab->FcaVend->EditValue ?>"<?php echo $FCab->FcaVend->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_FCab_FcaVend">
<span<?php echo $FCab->FcaVend->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $FCab->FcaVend->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="FCab" data-field="x_FcaVend" name="x_FcaVend" id="x_FcaVend" value="<?php echo ew_HtmlEncode($FCab->FcaVend->FormValue) ?>">
<?php } ?>
<?php echo $FCab->FcaVend->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($FCab->FcaCaje->Visible) { // FcaCaje ?>
	<div id="r_FcaCaje" class="form-group">
		<label id="elh_FCab_FcaCaje" for="x_FcaCaje" class="col-sm-2 control-label ewLabel"><?php echo $FCab->FcaCaje->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $FCab->FcaCaje->CellAttributes() ?>>
<?php if ($FCab->CurrentAction <> "F") { ?>
<span id="el_FCab_FcaCaje">
<input type="text" data-table="FCab" data-field="x_FcaCaje" name="x_FcaCaje" id="x_FcaCaje" size="30" placeholder="<?php echo ew_HtmlEncode($FCab->FcaCaje->getPlaceHolder()) ?>" value="<?php echo $FCab->FcaCaje->EditValue ?>"<?php echo $FCab->FcaCaje->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_FCab_FcaCaje">
<span<?php echo $FCab->FcaCaje->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $FCab->FcaCaje->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="FCab" data-field="x_FcaCaje" name="x_FcaCaje" id="x_FcaCaje" value="<?php echo ew_HtmlEncode($FCab->FcaCaje->FormValue) ?>">
<?php } ?>
<?php echo $FCab->FcaCaje->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($FCab->FcaFech->Visible) { // FcaFech ?>
	<div id="r_FcaFech" class="form-group">
		<label id="elh_FCab_FcaFech" for="x_FcaFech" class="col-sm-2 control-label ewLabel"><?php echo $FCab->FcaFech->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $FCab->FcaFech->CellAttributes() ?>>
<?php if ($FCab->CurrentAction <> "F") { ?>
<span id="el_FCab_FcaFech">
<input type="text" data-table="FCab" data-field="x_FcaFech" data-format="7" name="x_FcaFech" id="x_FcaFech" placeholder="<?php echo ew_HtmlEncode($FCab->FcaFech->getPlaceHolder()) ?>" value="<?php echo $FCab->FcaFech->EditValue ?>"<?php echo $FCab->FcaFech->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_FCab_FcaFech">
<span<?php echo $FCab->FcaFech->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $FCab->FcaFech->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="FCab" data-field="x_FcaFech" name="x_FcaFech" id="x_FcaFech" value="<?php echo ew_HtmlEncode($FCab->FcaFech->FormValue) ?>">
<?php } ?>
<?php echo $FCab->FcaFech->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($FCab->FcaTimb->Visible) { // FcaTimb ?>
	<div id="r_FcaTimb" class="form-group">
		<label id="elh_FCab_FcaTimb" for="x_FcaTimb" class="col-sm-2 control-label ewLabel"><?php echo $FCab->FcaTimb->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $FCab->FcaTimb->CellAttributes() ?>>
<?php if ($FCab->CurrentAction <> "F") { ?>
<span id="el_FCab_FcaTimb">
<input type="text" data-table="FCab" data-field="x_FcaTimb" name="x_FcaTimb" id="x_FcaTimb" size="30" placeholder="<?php echo ew_HtmlEncode($FCab->FcaTimb->getPlaceHolder()) ?>" value="<?php echo $FCab->FcaTimb->EditValue ?>"<?php echo $FCab->FcaTimb->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_FCab_FcaTimb">
<span<?php echo $FCab->FcaTimb->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $FCab->FcaTimb->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="FCab" data-field="x_FcaTimb" name="x_FcaTimb" id="x_FcaTimb" value="<?php echo ew_HtmlEncode($FCab->FcaTimb->FormValue) ?>">
<?php } ?>
<?php echo $FCab->FcaTimb->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($FCab->FcaTFac->Visible) { // FcaTFac ?>
	<div id="r_FcaTFac" class="form-group">
		<label id="elh_FCab_FcaTFac" for="x_FcaTFac" class="col-sm-2 control-label ewLabel"><?php echo $FCab->FcaTFac->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $FCab->FcaTFac->CellAttributes() ?>>
<?php if ($FCab->CurrentAction <> "F") { ?>
<span id="el_FCab_FcaTFac">
<input type="text" data-table="FCab" data-field="x_FcaTFac" name="x_FcaTFac" id="x_FcaTFac" size="30" placeholder="<?php echo ew_HtmlEncode($FCab->FcaTFac->getPlaceHolder()) ?>" value="<?php echo $FCab->FcaTFac->EditValue ?>"<?php echo $FCab->FcaTFac->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_FCab_FcaTFac">
<span<?php echo $FCab->FcaTFac->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $FCab->FcaTFac->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="FCab" data-field="x_FcaTFac" name="x_FcaTFac" id="x_FcaTFac" value="<?php echo ew_HtmlEncode($FCab->FcaTFac->FormValue) ?>">
<?php } ?>
<?php echo $FCab->FcaTFac->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($FCab->FcaAnul->Visible) { // FcaAnul ?>
	<div id="r_FcaAnul" class="form-group">
		<label id="elh_FCab_FcaAnul" for="x_FcaAnul" class="col-sm-2 control-label ewLabel"><?php echo $FCab->FcaAnul->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $FCab->FcaAnul->CellAttributes() ?>>
<?php if ($FCab->CurrentAction <> "F") { ?>
<span id="el_FCab_FcaAnul">
<input type="text" data-table="FCab" data-field="x_FcaAnul" name="x_FcaAnul" id="x_FcaAnul" size="30" maxlength="1" placeholder="<?php echo ew_HtmlEncode($FCab->FcaAnul->getPlaceHolder()) ?>" value="<?php echo $FCab->FcaAnul->EditValue ?>"<?php echo $FCab->FcaAnul->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_FCab_FcaAnul">
<span<?php echo $FCab->FcaAnul->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $FCab->FcaAnul->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="FCab" data-field="x_FcaAnul" name="x_FcaAnul" id="x_FcaAnul" value="<?php echo ew_HtmlEncode($FCab->FcaAnul->FormValue) ?>">
<?php } ?>
<?php echo $FCab->FcaAnul->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($FCab->FcaMAnu->Visible) { // FcaMAnu ?>
	<div id="r_FcaMAnu" class="form-group">
		<label id="elh_FCab_FcaMAnu" for="x_FcaMAnu" class="col-sm-2 control-label ewLabel"><?php echo $FCab->FcaMAnu->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $FCab->FcaMAnu->CellAttributes() ?>>
<?php if ($FCab->CurrentAction <> "F") { ?>
<span id="el_FCab_FcaMAnu">
<input type="text" data-table="FCab" data-field="x_FcaMAnu" name="x_FcaMAnu" id="x_FcaMAnu" size="30" placeholder="<?php echo ew_HtmlEncode($FCab->FcaMAnu->getPlaceHolder()) ?>" value="<?php echo $FCab->FcaMAnu->EditValue ?>"<?php echo $FCab->FcaMAnu->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_FCab_FcaMAnu">
<span<?php echo $FCab->FcaMAnu->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $FCab->FcaMAnu->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="FCab" data-field="x_FcaMAnu" name="x_FcaMAnu" id="x_FcaMAnu" value="<?php echo ew_HtmlEncode($FCab->FcaMAnu->FormValue) ?>">
<?php } ?>
<?php echo $FCab->FcaMAnu->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($FCab->FcaFAnu->Visible) { // FcaFAnu ?>
	<div id="r_FcaFAnu" class="form-group">
		<label id="elh_FCab_FcaFAnu" for="x_FcaFAnu" class="col-sm-2 control-label ewLabel"><?php echo $FCab->FcaFAnu->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $FCab->FcaFAnu->CellAttributes() ?>>
<?php if ($FCab->CurrentAction <> "F") { ?>
<span id="el_FCab_FcaFAnu">
<input type="text" data-table="FCab" data-field="x_FcaFAnu" data-format="7" name="x_FcaFAnu" id="x_FcaFAnu" placeholder="<?php echo ew_HtmlEncode($FCab->FcaFAnu->getPlaceHolder()) ?>" value="<?php echo $FCab->FcaFAnu->EditValue ?>"<?php echo $FCab->FcaFAnu->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_FCab_FcaFAnu">
<span<?php echo $FCab->FcaFAnu->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $FCab->FcaFAnu->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="FCab" data-field="x_FcaFAnu" name="x_FcaFAnu" id="x_FcaFAnu" value="<?php echo ew_HtmlEncode($FCab->FcaFAnu->FormValue) ?>">
<?php } ?>
<?php echo $FCab->FcaFAnu->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($FCab->FcaFTip->Visible) { // FcaFTip ?>
	<div id="r_FcaFTip" class="form-group">
		<label id="elh_FCab_FcaFTip" for="x_FcaFTip" class="col-sm-2 control-label ewLabel"><?php echo $FCab->FcaFTip->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $FCab->FcaFTip->CellAttributes() ?>>
<?php if ($FCab->CurrentAction <> "F") { ?>
<span id="el_FCab_FcaFTip">
<input type="text" data-table="FCab" data-field="x_FcaFTip" name="x_FcaFTip" id="x_FcaFTip" size="30" placeholder="<?php echo ew_HtmlEncode($FCab->FcaFTip->getPlaceHolder()) ?>" value="<?php echo $FCab->FcaFTip->EditValue ?>"<?php echo $FCab->FcaFTip->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_FCab_FcaFTip">
<span<?php echo $FCab->FcaFTip->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $FCab->FcaFTip->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="FCab" data-field="x_FcaFTip" name="x_FcaFTip" id="x_FcaFTip" value="<?php echo ew_HtmlEncode($FCab->FcaFTip->FormValue) ?>">
<?php } ?>
<?php echo $FCab->FcaFTip->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($FCab->FcaUsua->Visible) { // FcaUsua ?>
	<div id="r_FcaUsua" class="form-group">
		<label id="elh_FCab_FcaUsua" for="x_FcaUsua" class="col-sm-2 control-label ewLabel"><?php echo $FCab->FcaUsua->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $FCab->FcaUsua->CellAttributes() ?>>
<?php if ($FCab->CurrentAction <> "F") { ?>
<span id="el_FCab_FcaUsua">
<input type="text" data-table="FCab" data-field="x_FcaUsua" name="x_FcaUsua" id="x_FcaUsua" size="30" placeholder="<?php echo ew_HtmlEncode($FCab->FcaUsua->getPlaceHolder()) ?>" value="<?php echo $FCab->FcaUsua->EditValue ?>"<?php echo $FCab->FcaUsua->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_FCab_FcaUsua">
<span<?php echo $FCab->FcaUsua->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $FCab->FcaUsua->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="FCab" data-field="x_FcaUsua" name="x_FcaUsua" id="x_FcaUsua" value="<?php echo ew_HtmlEncode($FCab->FcaUsua->FormValue) ?>">
<?php } ?>
<?php echo $FCab->FcaUsua->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($FCab->FcaFCre->Visible) { // FcaFCre ?>
	<div id="r_FcaFCre" class="form-group">
		<label id="elh_FCab_FcaFCre" for="x_FcaFCre" class="col-sm-2 control-label ewLabel"><?php echo $FCab->FcaFCre->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $FCab->FcaFCre->CellAttributes() ?>>
<?php if ($FCab->CurrentAction <> "F") { ?>
<span id="el_FCab_FcaFCre">
<input type="text" data-table="FCab" data-field="x_FcaFCre" data-format="7" name="x_FcaFCre" id="x_FcaFCre" placeholder="<?php echo ew_HtmlEncode($FCab->FcaFCre->getPlaceHolder()) ?>" value="<?php echo $FCab->FcaFCre->EditValue ?>"<?php echo $FCab->FcaFCre->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_FCab_FcaFCre">
<span<?php echo $FCab->FcaFCre->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $FCab->FcaFCre->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="FCab" data-field="x_FcaFCre" name="x_FcaFCre" id="x_FcaFCre" value="<?php echo ew_HtmlEncode($FCab->FcaFCre->FormValue) ?>">
<?php } ?>
<?php echo $FCab->FcaFCre->CustomMsg ?></div></div>
	</div>
<?php } ?>
</div>
<div class="form-group">
	<div class="col-sm-offset-2 col-sm-10">
<?php if ($FCab->CurrentAction <> "F") { // Confirm page ?>
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit" onclick="this.form.a_edit.value='F';"><?php echo $Language->Phrase("SaveBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="button" data-href="<?php echo $FCab_edit->getReturnUrl() ?>"><?php echo $Language->Phrase("CancelBtn") ?></button>
<?php } else { ?>
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("ConfirmBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="submit" onclick="this.form.a_edit.value='X';"><?php echo $Language->Phrase("CancelBtn") ?></button>
<?php } ?>
	</div>
</div>
</form>
<script type="text/javascript">
fFCabedit.Init();
</script>
<?php
$FCab_edit->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$FCab_edit->Page_Terminate();
?>
