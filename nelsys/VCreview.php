<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "VCreinfo.php" ?>
<?php include_once "Usuainfo.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$VCre_view = NULL; // Initialize page object first

class cVCre_view extends cVCre {

	// Page ID
	var $PageID = 'view';

	// Project ID
	var $ProjectID = "{04439FF7-B43F-460F-8514-F71C8FF9E679}";

	// Table name
	var $TableName = 'VCre';

	// Page object name
	var $PageObjName = 'VCre_view';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Page URLs
	var $AddUrl;
	var $EditUrl;
	var $CopyUrl;
	var $DeleteUrl;
	var $ViewUrl;
	var $ListUrl;

	// Export URLs
	var $ExportPrintUrl;
	var $ExportHtmlUrl;
	var $ExportExcelUrl;
	var $ExportWordUrl;
	var $ExportXmlUrl;
	var $ExportCsvUrl;
	var $ExportPdfUrl;

	// Custom export
	var $ExportExcelCustom = FALSE;
	var $ExportWordCustom = FALSE;
	var $ExportPdfCustom = FALSE;
	var $ExportEmailCustom = FALSE;

	// Update URLs
	var $InlineAddUrl;
	var $InlineCopyUrl;
	var $InlineEditUrl;
	var $GridAddUrl;
	var $GridEditUrl;
	var $MultiDeleteUrl;
	var $MultiUpdateUrl;

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (VCre)
		if (!isset($GLOBALS["VCre"]) || get_class($GLOBALS["VCre"]) == "cVCre") {
			$GLOBALS["VCre"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["VCre"];
		}
		$KeyUrl = "";
		if (@$_GET["VcrCodi"] <> "") {
			$this->RecKey["VcrCodi"] = $_GET["VcrCodi"];
			$KeyUrl .= "&amp;VcrCodi=" . urlencode($this->RecKey["VcrCodi"]);
		}
		$this->ExportPrintUrl = $this->PageUrl() . "export=print" . $KeyUrl;
		$this->ExportHtmlUrl = $this->PageUrl() . "export=html" . $KeyUrl;
		$this->ExportExcelUrl = $this->PageUrl() . "export=excel" . $KeyUrl;
		$this->ExportWordUrl = $this->PageUrl() . "export=word" . $KeyUrl;
		$this->ExportXmlUrl = $this->PageUrl() . "export=xml" . $KeyUrl;
		$this->ExportCsvUrl = $this->PageUrl() . "export=csv" . $KeyUrl;
		$this->ExportPdfUrl = $this->PageUrl() . "export=pdf" . $KeyUrl;

		// Table object (Usua)
		if (!isset($GLOBALS['Usua'])) $GLOBALS['Usua'] = new cUsua();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'view', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'VCre', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (Usua)
		if (!isset($UserTable)) {
			$UserTable = new cUsua();
			$UserTableConn = Conn($UserTable->DBID);
		}

		// Export options
		$this->ExportOptions = new cListOptions();
		$this->ExportOptions->Tag = "div";
		$this->ExportOptions->TagClassName = "ewExportOption";

		// Other options
		$this->OtherOptions['action'] = new cListOptions();
		$this->OtherOptions['action']->Tag = "div";
		$this->OtherOptions['action']->TagClassName = "ewActionOption";
		$this->OtherOptions['detail'] = new cListOptions();
		$this->OtherOptions['detail']->Tag = "div";
		$this->OtherOptions['detail']->TagClassName = "ewDetailOption";
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanView()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage($Language->Phrase("NoPermission")); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("VCrelist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action
		$this->VcrCodi->Visible = !$this->IsAdd() && !$this->IsCopy() && !$this->IsGridAdd();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $VCre;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($VCre);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $ExportOptions; // Export options
	var $OtherOptions = array(); // Other options
	var $DisplayRecs = 1;
	var $DbMasterFilter;
	var $DbDetailFilter;
	var $StartRec;
	var $StopRec;
	var $TotalRecs = 0;
	var $RecRange = 10;
	var $RecCnt;
	var $RecKey = array();
	var $Recordset;

	//
	// Page main
	//
	function Page_Main() {
		global $Language;
		$sReturnUrl = "";
		$bMatchRecord = FALSE;

		// Set up Breadcrumb
		if ($this->Export == "")
			$this->SetupBreadcrumb();
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET["VcrCodi"] <> "") {
				$this->VcrCodi->setQueryStringValue($_GET["VcrCodi"]);
				$this->RecKey["VcrCodi"] = $this->VcrCodi->QueryStringValue;
			} elseif (@$_POST["VcrCodi"] <> "") {
				$this->VcrCodi->setFormValue($_POST["VcrCodi"]);
				$this->RecKey["VcrCodi"] = $this->VcrCodi->FormValue;
			} else {
				$sReturnUrl = "VCrelist.php"; // Return to list
			}

			// Get action
			$this->CurrentAction = "I"; // Display form
			switch ($this->CurrentAction) {
				case "I": // Get a record to display
					if (!$this->LoadRow()) { // Load record based on key
						if ($this->getSuccessMessage() == "" && $this->getFailureMessage() == "")
							$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record message
						$sReturnUrl = "VCrelist.php"; // No matching record, return to list
					}
			}
		} else {
			$sReturnUrl = "VCrelist.php"; // Not page request, return to list
		}
		if ($sReturnUrl <> "")
			$this->Page_Terminate($sReturnUrl);

		// Render row
		$this->RowType = EW_ROWTYPE_VIEW;
		$this->ResetAttrs();
		$this->RenderRow();
	}

	// Set up other options
	function SetupOtherOptions() {
		global $Language, $Security;
		$options = &$this->OtherOptions;
		$option = &$options["action"];

		// Add
		$item = &$option->Add("add");
		$item->Body = "<a class=\"ewAction ewAdd\" title=\"" . ew_HtmlTitle($Language->Phrase("ViewPageAddLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("ViewPageAddLink")) . "\" href=\"" . ew_HtmlEncode($this->AddUrl) . "\">" . $Language->Phrase("ViewPageAddLink") . "</a>";
		$item->Visible = ($this->AddUrl <> "" && $Security->CanAdd());

		// Edit
		$item = &$option->Add("edit");
		$item->Body = "<a class=\"ewAction ewEdit\" title=\"" . ew_HtmlTitle($Language->Phrase("ViewPageEditLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("ViewPageEditLink")) . "\" href=\"" . ew_HtmlEncode($this->EditUrl) . "\">" . $Language->Phrase("ViewPageEditLink") . "</a>";
		$item->Visible = ($this->EditUrl <> "" && $Security->CanEdit());

		// Copy
		$item = &$option->Add("copy");
		$item->Body = "<a class=\"ewAction ewCopy\" title=\"" . ew_HtmlTitle($Language->Phrase("ViewPageCopyLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("ViewPageCopyLink")) . "\" href=\"" . ew_HtmlEncode($this->CopyUrl) . "\">" . $Language->Phrase("ViewPageCopyLink") . "</a>";
		$item->Visible = ($this->CopyUrl <> "" && $Security->CanAdd());

		// Delete
		$item = &$option->Add("delete");
		$item->Body = "<a class=\"ewAction ewDelete\" title=\"" . ew_HtmlTitle($Language->Phrase("ViewPageDeleteLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("ViewPageDeleteLink")) . "\" href=\"" . ew_HtmlEncode($this->DeleteUrl) . "\">" . $Language->Phrase("ViewPageDeleteLink") . "</a>";
		$item->Visible = ($this->DeleteUrl <> "" && $Security->CanDelete());

		// Set up action default
		$option = &$options["action"];
		$option->DropDownButtonPhrase = $Language->Phrase("ButtonActions");
		$option->UseImageAndText = TRUE;
		$option->UseDropDownButton = FALSE;
		$option->UseButtonGroup = TRUE;
		$item = &$option->Add($option->GroupOptionName);
		$item->Body = "";
		$item->Visible = FALSE;
	}

	// Set up starting record parameters
	function SetUpStartRec() {
		if ($this->DisplayRecs == 0)
			return;
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET[EW_TABLE_START_REC] <> "") { // Check for "start" parameter
				$this->StartRec = $_GET[EW_TABLE_START_REC];
				$this->setStartRecordNumber($this->StartRec);
			} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
				$PageNo = $_GET[EW_TABLE_PAGE_NO];
				if (is_numeric($PageNo)) {
					$this->StartRec = ($PageNo-1)*$this->DisplayRecs+1;
					if ($this->StartRec <= 0) {
						$this->StartRec = 1;
					} elseif ($this->StartRec >= intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1) {
						$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1;
					}
					$this->setStartRecordNumber($this->StartRec);
				}
			}
		}
		$this->StartRec = $this->getStartRecordNumber();

		// Check if correct start record counter
		if (!is_numeric($this->StartRec) || $this->StartRec == "") { // Avoid invalid start record counter
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} elseif (intval($this->StartRec) > intval($this->TotalRecs)) { // Avoid starting record > total records
			$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to last page first record
			$this->setStartRecordNumber($this->StartRec);
		} elseif (($this->StartRec-1) % $this->DisplayRecs <> 0) {
			$this->StartRec = intval(($this->StartRec-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to page boundary
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->VcrCodi->setDbValue($rs->fields('VcrCodi'));
		$this->VcrCCuo->setDbValue($rs->fields('VcrCCuo'));
		$this->VcrMCuo->setDbValue($rs->fields('VcrMCuo'));
		$this->VcrEIni->setDbValue($rs->fields('VcrEIni'));
		$this->VcrTInt->setDbValue($rs->fields('VcrTInt'));
		$this->VcrMTot->setDbValue($rs->fields('VcrMTot'));
		$this->VcrEsta->setDbValue($rs->fields('VcrEsta'));
		$this->VcrDes->setDbValue($rs->fields('VcrDes'));
		$this->VcrHas->setDbValue($rs->fields('VcrHas'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->VcrCodi->DbValue = $row['VcrCodi'];
		$this->VcrCCuo->DbValue = $row['VcrCCuo'];
		$this->VcrMCuo->DbValue = $row['VcrMCuo'];
		$this->VcrEIni->DbValue = $row['VcrEIni'];
		$this->VcrTInt->DbValue = $row['VcrTInt'];
		$this->VcrMTot->DbValue = $row['VcrMTot'];
		$this->VcrEsta->DbValue = $row['VcrEsta'];
		$this->VcrDes->DbValue = $row['VcrDes'];
		$this->VcrHas->DbValue = $row['VcrHas'];
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		$this->AddUrl = $this->GetAddUrl();
		$this->EditUrl = $this->GetEditUrl();
		$this->CopyUrl = $this->GetCopyUrl();
		$this->DeleteUrl = $this->GetDeleteUrl();
		$this->ListUrl = $this->GetListUrl();
		$this->SetupOtherOptions();

		// Convert decimal values if posted back
		if ($this->VcrMCuo->FormValue == $this->VcrMCuo->CurrentValue && is_numeric(ew_StrToFloat($this->VcrMCuo->CurrentValue)))
			$this->VcrMCuo->CurrentValue = ew_StrToFloat($this->VcrMCuo->CurrentValue);

		// Convert decimal values if posted back
		if ($this->VcrEIni->FormValue == $this->VcrEIni->CurrentValue && is_numeric(ew_StrToFloat($this->VcrEIni->CurrentValue)))
			$this->VcrEIni->CurrentValue = ew_StrToFloat($this->VcrEIni->CurrentValue);

		// Convert decimal values if posted back
		if ($this->VcrTInt->FormValue == $this->VcrTInt->CurrentValue && is_numeric(ew_StrToFloat($this->VcrTInt->CurrentValue)))
			$this->VcrTInt->CurrentValue = ew_StrToFloat($this->VcrTInt->CurrentValue);

		// Convert decimal values if posted back
		if ($this->VcrMTot->FormValue == $this->VcrMTot->CurrentValue && is_numeric(ew_StrToFloat($this->VcrMTot->CurrentValue)))
			$this->VcrMTot->CurrentValue = ew_StrToFloat($this->VcrMTot->CurrentValue);

		// Call Row_Rendering event
		$this->Row_Rendering();

		// Common render codes for all row types
		// VcrCodi
		// VcrCCuo
		// VcrMCuo
		// VcrEIni
		// VcrTInt
		// VcrMTot
		// VcrEsta
		// VcrDes
		// VcrHas

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// VcrCodi
		$this->VcrCodi->ViewValue = $this->VcrCodi->CurrentValue;
		$this->VcrCodi->ViewCustomAttributes = "";

		// VcrCCuo
		$this->VcrCCuo->ViewValue = $this->VcrCCuo->CurrentValue;
		$this->VcrCCuo->ViewCustomAttributes = "";

		// VcrMCuo
		$this->VcrMCuo->ViewValue = $this->VcrMCuo->CurrentValue;
		$this->VcrMCuo->ViewCustomAttributes = "";

		// VcrEIni
		$this->VcrEIni->ViewValue = $this->VcrEIni->CurrentValue;
		$this->VcrEIni->ViewCustomAttributes = "";

		// VcrTInt
		$this->VcrTInt->ViewValue = $this->VcrTInt->CurrentValue;
		$this->VcrTInt->ViewCustomAttributes = "";

		// VcrMTot
		$this->VcrMTot->ViewValue = $this->VcrMTot->CurrentValue;
		$this->VcrMTot->ViewCustomAttributes = "";

		// VcrEsta
		$this->VcrEsta->ViewValue = $this->VcrEsta->CurrentValue;
		$this->VcrEsta->ViewCustomAttributes = "";

		// VcrDes
		$this->VcrDes->ViewValue = $this->VcrDes->CurrentValue;
		$this->VcrDes->ViewValue = ew_FormatDateTime($this->VcrDes->ViewValue, 7);
		$this->VcrDes->ViewCustomAttributes = "";

		// VcrHas
		$this->VcrHas->ViewValue = $this->VcrHas->CurrentValue;
		$this->VcrHas->ViewValue = ew_FormatDateTime($this->VcrHas->ViewValue, 7);
		$this->VcrHas->ViewCustomAttributes = "";

			// VcrCodi
			$this->VcrCodi->LinkCustomAttributes = "";
			$this->VcrCodi->HrefValue = "";
			$this->VcrCodi->TooltipValue = "";

			// VcrCCuo
			$this->VcrCCuo->LinkCustomAttributes = "";
			$this->VcrCCuo->HrefValue = "";
			$this->VcrCCuo->TooltipValue = "";

			// VcrMCuo
			$this->VcrMCuo->LinkCustomAttributes = "";
			$this->VcrMCuo->HrefValue = "";
			$this->VcrMCuo->TooltipValue = "";

			// VcrEIni
			$this->VcrEIni->LinkCustomAttributes = "";
			$this->VcrEIni->HrefValue = "";
			$this->VcrEIni->TooltipValue = "";

			// VcrTInt
			$this->VcrTInt->LinkCustomAttributes = "";
			$this->VcrTInt->HrefValue = "";
			$this->VcrTInt->TooltipValue = "";

			// VcrMTot
			$this->VcrMTot->LinkCustomAttributes = "";
			$this->VcrMTot->HrefValue = "";
			$this->VcrMTot->TooltipValue = "";

			// VcrEsta
			$this->VcrEsta->LinkCustomAttributes = "";
			$this->VcrEsta->HrefValue = "";
			$this->VcrEsta->TooltipValue = "";

			// VcrDes
			$this->VcrDes->LinkCustomAttributes = "";
			$this->VcrDes->HrefValue = "";
			$this->VcrDes->TooltipValue = "";

			// VcrHas
			$this->VcrHas->LinkCustomAttributes = "";
			$this->VcrHas->HrefValue = "";
			$this->VcrHas->TooltipValue = "";
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, "VCrelist.php", "", $this->TableVar, TRUE);
		$PageId = "view";
		$Breadcrumb->Add("view", $PageId, $url);
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Page Exporting event
	// $this->ExportDoc = export document object
	function Page_Exporting() {

		//$this->ExportDoc->Text = "my header"; // Export header
		//return FALSE; // Return FALSE to skip default export and use Row_Export event

		return TRUE; // Return TRUE to use default export and skip Row_Export event
	}

	// Row Export event
	// $this->ExportDoc = export document object
	function Row_Export($rs) {

	    //$this->ExportDoc->Text .= "my content"; // Build HTML with field value: $rs["MyField"] or $this->MyField->ViewValue
	}

	// Page Exported event
	// $this->ExportDoc = export document object
	function Page_Exported() {

		//$this->ExportDoc->Text .= "my footer"; // Export footer
		//echo $this->ExportDoc->Text;

	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($VCre_view)) $VCre_view = new cVCre_view();

// Page init
$VCre_view->Page_Init();

// Page main
$VCre_view->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$VCre_view->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "view";
var CurrentForm = fVCreview = new ew_Form("fVCreview", "view");

// Form_CustomValidate event
fVCreview.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fVCreview.ValidateRequired = true;
<?php } else { ?>
fVCreview.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
// Form object for search

</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php $VCre_view->ExportOptions->Render("body") ?>
<?php
	foreach ($VCre_view->OtherOptions as &$option)
		$option->Render("body");
?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php $VCre_view->ShowPageHeader(); ?>
<?php
$VCre_view->ShowMessage();
?>
<form name="fVCreview" id="fVCreview" class="form-inline ewForm ewViewForm" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($VCre_view->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $VCre_view->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="VCre">
<table class="table table-bordered table-striped ewViewTable">
<?php if ($VCre->VcrCodi->Visible) { // VcrCodi ?>
	<tr id="r_VcrCodi">
		<td><span id="elh_VCre_VcrCodi"><?php echo $VCre->VcrCodi->FldCaption() ?></span></td>
		<td data-name="VcrCodi"<?php echo $VCre->VcrCodi->CellAttributes() ?>>
<span id="el_VCre_VcrCodi">
<span<?php echo $VCre->VcrCodi->ViewAttributes() ?>>
<?php echo $VCre->VcrCodi->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($VCre->VcrCCuo->Visible) { // VcrCCuo ?>
	<tr id="r_VcrCCuo">
		<td><span id="elh_VCre_VcrCCuo"><?php echo $VCre->VcrCCuo->FldCaption() ?></span></td>
		<td data-name="VcrCCuo"<?php echo $VCre->VcrCCuo->CellAttributes() ?>>
<span id="el_VCre_VcrCCuo">
<span<?php echo $VCre->VcrCCuo->ViewAttributes() ?>>
<?php echo $VCre->VcrCCuo->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($VCre->VcrMCuo->Visible) { // VcrMCuo ?>
	<tr id="r_VcrMCuo">
		<td><span id="elh_VCre_VcrMCuo"><?php echo $VCre->VcrMCuo->FldCaption() ?></span></td>
		<td data-name="VcrMCuo"<?php echo $VCre->VcrMCuo->CellAttributes() ?>>
<span id="el_VCre_VcrMCuo">
<span<?php echo $VCre->VcrMCuo->ViewAttributes() ?>>
<?php echo $VCre->VcrMCuo->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($VCre->VcrEIni->Visible) { // VcrEIni ?>
	<tr id="r_VcrEIni">
		<td><span id="elh_VCre_VcrEIni"><?php echo $VCre->VcrEIni->FldCaption() ?></span></td>
		<td data-name="VcrEIni"<?php echo $VCre->VcrEIni->CellAttributes() ?>>
<span id="el_VCre_VcrEIni">
<span<?php echo $VCre->VcrEIni->ViewAttributes() ?>>
<?php echo $VCre->VcrEIni->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($VCre->VcrTInt->Visible) { // VcrTInt ?>
	<tr id="r_VcrTInt">
		<td><span id="elh_VCre_VcrTInt"><?php echo $VCre->VcrTInt->FldCaption() ?></span></td>
		<td data-name="VcrTInt"<?php echo $VCre->VcrTInt->CellAttributes() ?>>
<span id="el_VCre_VcrTInt">
<span<?php echo $VCre->VcrTInt->ViewAttributes() ?>>
<?php echo $VCre->VcrTInt->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($VCre->VcrMTot->Visible) { // VcrMTot ?>
	<tr id="r_VcrMTot">
		<td><span id="elh_VCre_VcrMTot"><?php echo $VCre->VcrMTot->FldCaption() ?></span></td>
		<td data-name="VcrMTot"<?php echo $VCre->VcrMTot->CellAttributes() ?>>
<span id="el_VCre_VcrMTot">
<span<?php echo $VCre->VcrMTot->ViewAttributes() ?>>
<?php echo $VCre->VcrMTot->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($VCre->VcrEsta->Visible) { // VcrEsta ?>
	<tr id="r_VcrEsta">
		<td><span id="elh_VCre_VcrEsta"><?php echo $VCre->VcrEsta->FldCaption() ?></span></td>
		<td data-name="VcrEsta"<?php echo $VCre->VcrEsta->CellAttributes() ?>>
<span id="el_VCre_VcrEsta">
<span<?php echo $VCre->VcrEsta->ViewAttributes() ?>>
<?php echo $VCre->VcrEsta->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($VCre->VcrDes->Visible) { // VcrDes ?>
	<tr id="r_VcrDes">
		<td><span id="elh_VCre_VcrDes"><?php echo $VCre->VcrDes->FldCaption() ?></span></td>
		<td data-name="VcrDes"<?php echo $VCre->VcrDes->CellAttributes() ?>>
<span id="el_VCre_VcrDes">
<span<?php echo $VCre->VcrDes->ViewAttributes() ?>>
<?php echo $VCre->VcrDes->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($VCre->VcrHas->Visible) { // VcrHas ?>
	<tr id="r_VcrHas">
		<td><span id="elh_VCre_VcrHas"><?php echo $VCre->VcrHas->FldCaption() ?></span></td>
		<td data-name="VcrHas"<?php echo $VCre->VcrHas->CellAttributes() ?>>
<span id="el_VCre_VcrHas">
<span<?php echo $VCre->VcrHas->ViewAttributes() ?>>
<?php echo $VCre->VcrHas->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
</table>
</form>
<script type="text/javascript">
fVCreview.Init();
</script>
<?php
$VCre_view->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$VCre_view->Page_Terminate();
?>
