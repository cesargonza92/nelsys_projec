<?php

// Global variable for table object
$VCre = NULL;

//
// Table class for VCre
//
class cVCre extends cTable {
	var $VcrCodi;
	var $VcrCCuo;
	var $VcrMCuo;
	var $VcrEIni;
	var $VcrTInt;
	var $VcrMTot;
	var $VcrEsta;
	var $VcrDes;
	var $VcrHas;

	//
	// Table class constructor
	//
	function __construct() {
		global $Language;

		// Language object
		if (!isset($Language)) $Language = new cLanguage();
		$this->TableVar = 'VCre';
		$this->TableName = 'VCre';
		$this->TableType = 'TABLE';

		// Update Table
		$this->UpdateTable = "\"public\".\"VCre\"";
		$this->DBID = 'DB';
		$this->ExportAll = TRUE;
		$this->ExportPageBreakCount = 0; // Page break per every n record (PDF only)
		$this->ExportPageOrientation = "portrait"; // Page orientation (PDF only)
		$this->ExportPageSize = "a4"; // Page size (PDF only)
		$this->ExportExcelPageOrientation = ""; // Page orientation (PHPExcel only)
		$this->ExportExcelPageSize = ""; // Page size (PHPExcel only)
		$this->DetailAdd = TRUE; // Allow detail add
		$this->DetailEdit = TRUE; // Allow detail edit
		$this->DetailView = FALSE; // Allow detail view
		$this->ShowMultipleDetails = FALSE; // Show multiple details
		$this->GridAddRowCount = 5;
		$this->AllowAddDeleteRow = ew_AllowAddDeleteRow(); // Allow add/delete row
		$this->UserIDAllowSecurity = 0; // User ID Allow
		$this->BasicSearch = new cBasicSearch($this->TableVar);

		// VcrCodi
		$this->VcrCodi = new cField('VCre', 'VCre', 'x_VcrCodi', 'VcrCodi', '"VcrCodi"', 'CAST("VcrCodi" AS varchar(255))', 3, -1, FALSE, '"VcrCodi"', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'NO');
		$this->VcrCodi->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['VcrCodi'] = &$this->VcrCodi;

		// VcrCCuo
		$this->VcrCCuo = new cField('VCre', 'VCre', 'x_VcrCCuo', 'VcrCCuo', '"VcrCCuo"', 'CAST("VcrCCuo" AS varchar(255))', 3, -1, FALSE, '"VcrCCuo"', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->VcrCCuo->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['VcrCCuo'] = &$this->VcrCCuo;

		// VcrMCuo
		$this->VcrMCuo = new cField('VCre', 'VCre', 'x_VcrMCuo', 'VcrMCuo', '"VcrMCuo"', 'CAST("VcrMCuo" AS varchar(255))', 5, -1, FALSE, '"VcrMCuo"', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->VcrMCuo->FldDefaultErrMsg = $Language->Phrase("IncorrectFloat");
		$this->fields['VcrMCuo'] = &$this->VcrMCuo;

		// VcrEIni
		$this->VcrEIni = new cField('VCre', 'VCre', 'x_VcrEIni', 'VcrEIni', '"VcrEIni"', 'CAST("VcrEIni" AS varchar(255))', 5, -1, FALSE, '"VcrEIni"', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->VcrEIni->FldDefaultErrMsg = $Language->Phrase("IncorrectFloat");
		$this->fields['VcrEIni'] = &$this->VcrEIni;

		// VcrTInt
		$this->VcrTInt = new cField('VCre', 'VCre', 'x_VcrTInt', 'VcrTInt', '"VcrTInt"', 'CAST("VcrTInt" AS varchar(255))', 5, -1, FALSE, '"VcrTInt"', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->VcrTInt->FldDefaultErrMsg = $Language->Phrase("IncorrectFloat");
		$this->fields['VcrTInt'] = &$this->VcrTInt;

		// VcrMTot
		$this->VcrMTot = new cField('VCre', 'VCre', 'x_VcrMTot', 'VcrMTot', '"VcrMTot"', 'CAST("VcrMTot" AS varchar(255))', 5, -1, FALSE, '"VcrMTot"', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->VcrMTot->FldDefaultErrMsg = $Language->Phrase("IncorrectFloat");
		$this->fields['VcrMTot'] = &$this->VcrMTot;

		// VcrEsta
		$this->VcrEsta = new cField('VCre', 'VCre', 'x_VcrEsta', 'VcrEsta', '"VcrEsta"', '"VcrEsta"', 200, -1, FALSE, '"VcrEsta"', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->fields['VcrEsta'] = &$this->VcrEsta;

		// VcrDes
		$this->VcrDes = new cField('VCre', 'VCre', 'x_VcrDes', 'VcrDes', '"VcrDes"', 'CAST("VcrDes" AS varchar(255))', 133, 7, FALSE, '"VcrDes"', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->VcrDes->FldDefaultErrMsg = str_replace("%s", "/", $Language->Phrase("IncorrectDateDMY"));
		$this->fields['VcrDes'] = &$this->VcrDes;

		// VcrHas
		$this->VcrHas = new cField('VCre', 'VCre', 'x_VcrHas', 'VcrHas', '"VcrHas"', 'CAST("VcrHas" AS varchar(255))', 133, 7, FALSE, '"VcrHas"', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->VcrHas->FldDefaultErrMsg = str_replace("%s", "/", $Language->Phrase("IncorrectDateDMY"));
		$this->fields['VcrHas'] = &$this->VcrHas;
	}

	// Multiple column sort
	function UpdateSort(&$ofld, $ctrl) {
		if ($this->CurrentOrder == $ofld->FldName) {
			$sSortField = $ofld->FldExpression;
			$sLastSort = $ofld->getSort();
			if ($this->CurrentOrderType == "ASC" || $this->CurrentOrderType == "DESC") {
				$sThisSort = $this->CurrentOrderType;
			} else {
				$sThisSort = ($sLastSort == "ASC") ? "DESC" : "ASC";
			}
			$ofld->setSort($sThisSort);
			if ($ctrl) {
				$sOrderBy = $this->getSessionOrderBy();
				if (strpos($sOrderBy, $sSortField . " " . $sLastSort) !== FALSE) {
					$sOrderBy = str_replace($sSortField . " " . $sLastSort, $sSortField . " " . $sThisSort, $sOrderBy);
				} else {
					if ($sOrderBy <> "") $sOrderBy .= ", ";
					$sOrderBy .= $sSortField . " " . $sThisSort;
				}
				$this->setSessionOrderBy($sOrderBy); // Save to Session
			} else {
				$this->setSessionOrderBy($sSortField . " " . $sThisSort); // Save to Session
			}
		} else {
			if (!$ctrl) $ofld->setSort("");
		}
	}

	// Table level SQL
	var $_SqlFrom = "";

	function getSqlFrom() { // From
		return ($this->_SqlFrom <> "") ? $this->_SqlFrom : "\"public\".\"VCre\"";
	}

	function SqlFrom() { // For backward compatibility
    	return $this->getSqlFrom();
	}

	function setSqlFrom($v) {
    	$this->_SqlFrom = $v;
	}
	var $_SqlSelect = "";

	function getSqlSelect() { // Select
		return ($this->_SqlSelect <> "") ? $this->_SqlSelect : "SELECT * FROM " . $this->getSqlFrom();
	}

	function SqlSelect() { // For backward compatibility
    	return $this->getSqlSelect();
	}

	function setSqlSelect($v) {
    	$this->_SqlSelect = $v;
	}
	var $_SqlWhere = "";

	function getSqlWhere() { // Where
		$sWhere = ($this->_SqlWhere <> "") ? $this->_SqlWhere : "";
		$this->TableFilter = "";
		ew_AddFilter($sWhere, $this->TableFilter);
		return $sWhere;
	}

	function SqlWhere() { // For backward compatibility
    	return $this->getSqlWhere();
	}

	function setSqlWhere($v) {
    	$this->_SqlWhere = $v;
	}
	var $_SqlGroupBy = "";

	function getSqlGroupBy() { // Group By
		return ($this->_SqlGroupBy <> "") ? $this->_SqlGroupBy : "";
	}

	function SqlGroupBy() { // For backward compatibility
    	return $this->getSqlGroupBy();
	}

	function setSqlGroupBy($v) {
    	$this->_SqlGroupBy = $v;
	}
	var $_SqlHaving = "";

	function getSqlHaving() { // Having
		return ($this->_SqlHaving <> "") ? $this->_SqlHaving : "";
	}

	function SqlHaving() { // For backward compatibility
    	return $this->getSqlHaving();
	}

	function setSqlHaving($v) {
    	$this->_SqlHaving = $v;
	}
	var $_SqlOrderBy = "";

	function getSqlOrderBy() { // Order By
		return ($this->_SqlOrderBy <> "") ? $this->_SqlOrderBy : "";
	}

	function SqlOrderBy() { // For backward compatibility
    	return $this->getSqlOrderBy();
	}

	function setSqlOrderBy($v) {
    	$this->_SqlOrderBy = $v;
	}

	// Apply User ID filters
	function ApplyUserIDFilters($sFilter) {
		return $sFilter;
	}

	// Check if User ID security allows view all
	function UserIDAllow($id = "") {
		$allow = EW_USER_ID_ALLOW;
		switch ($id) {
			case "add":
			case "copy":
			case "gridadd":
			case "register":
			case "addopt":
				return (($allow & 1) == 1);
			case "edit":
			case "gridedit":
			case "update":
			case "changepwd":
			case "forgotpwd":
				return (($allow & 4) == 4);
			case "delete":
				return (($allow & 2) == 2);
			case "view":
				return (($allow & 32) == 32);
			case "search":
				return (($allow & 64) == 64);
			default:
				return (($allow & 8) == 8);
		}
	}

	// Get SQL
	function GetSQL($where, $orderby) {
		return ew_BuildSelectSql($this->getSqlSelect(), $this->getSqlWhere(),
			$this->getSqlGroupBy(), $this->getSqlHaving(), $this->getSqlOrderBy(),
			$where, $orderby);
	}

	// Table SQL
	function SQL() {
		$sFilter = $this->CurrentFilter;
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$sSort = $this->getSessionOrderBy();
		return ew_BuildSelectSql($this->getSqlSelect(), $this->getSqlWhere(),
			$this->getSqlGroupBy(), $this->getSqlHaving(), $this->getSqlOrderBy(),
			$sFilter, $sSort);
	}

	// Table SQL with List page filter
	function SelectSQL() {
		$sFilter = $this->getSessionWhere();
		ew_AddFilter($sFilter, $this->CurrentFilter);
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$this->Recordset_Selecting($sFilter);
		$sSort = $this->getSessionOrderBy();
		return ew_BuildSelectSql($this->getSqlSelect(), $this->getSqlWhere(), $this->getSqlGroupBy(),
			$this->getSqlHaving(), $this->getSqlOrderBy(), $sFilter, $sSort);
	}

	// Get ORDER BY clause
	function GetOrderBy() {
		$sSort = $this->getSessionOrderBy();
		return ew_BuildSelectSql("", "", "", "", $this->getSqlOrderBy(), "", $sSort);
	}

	// Try to get record count
	function TryGetRecordCount($sSql) {
		$cnt = -1;
		if (($this->TableType == 'TABLE' || $this->TableType == 'VIEW' || $this->TableType == 'LINKTABLE') && preg_match("/^SELECT \* FROM/i", $sSql)) {
			$sSql = "SELECT COUNT(*) FROM" . preg_replace('/^SELECT\s([\s\S]+)?\*\sFROM/i', "", $sSql);
			$sOrderBy = $this->GetOrderBy();
			if (substr($sSql, strlen($sOrderBy) * -1) == $sOrderBy)
				$sSql = substr($sSql, 0, strlen($sSql) - strlen($sOrderBy)); // Remove ORDER BY clause
		} else {
			$sSql = "SELECT COUNT(*) FROM (" . $sSql . ") EW_COUNT_TABLE";
		}
		$conn = &$this->Connection();
		if ($rs = $conn->Execute($sSql)) {
			if (!$rs->EOF && $rs->FieldCount() > 0) {
				$cnt = $rs->fields[0];
				$rs->Close();
			}
		}
		return intval($cnt);
	}

	// Get record count based on filter (for detail record count in master table pages)
	function LoadRecordCount($sFilter) {
		$origFilter = $this->CurrentFilter;
		$this->CurrentFilter = $sFilter;
		$this->Recordset_Selecting($this->CurrentFilter);

		//$sSql = $this->SQL();
		$sSql = $this->GetSQL($this->CurrentFilter, "");
		$cnt = $this->TryGetRecordCount($sSql);
		if ($cnt == -1) {
			if ($rs = $this->LoadRs($this->CurrentFilter)) {
				$cnt = $rs->RecordCount();
				$rs->Close();
			}
		}
		$this->CurrentFilter = $origFilter;
		return intval($cnt);
	}

	// Get record count (for current List page)
	function SelectRecordCount() {
		$sSql = $this->SelectSQL();
		$cnt = $this->TryGetRecordCount($sSql);
		if ($cnt == -1) {
			$conn = &$this->Connection();
			if ($rs = $conn->Execute($sSql)) {
				$cnt = $rs->RecordCount();
				$rs->Close();
			}
		}
		return intval($cnt);
	}

	// INSERT statement
	function InsertSQL(&$rs) {
		$names = "";
		$values = "";
		foreach ($rs as $name => $value) {
			if (!isset($this->fields[$name]) || $this->fields[$name]->FldIsCustom)
				continue;
			$names .= $this->fields[$name]->FldExpression . ",";
			$values .= ew_QuotedValue($value, $this->fields[$name]->FldDataType, $this->DBID) . ",";
		}
		while (substr($names, -1) == ",")
			$names = substr($names, 0, -1);
		while (substr($values, -1) == ",")
			$values = substr($values, 0, -1);
		return "INSERT INTO " . $this->UpdateTable . " ($names) VALUES ($values)";
	}

	// Insert
	function Insert(&$rs) {
		$conn = &$this->Connection();
		return $conn->Execute($this->InsertSQL($rs));
	}

	// UPDATE statement
	function UpdateSQL(&$rs, $where = "", $curfilter = TRUE) {
		$sql = "UPDATE " . $this->UpdateTable . " SET ";
		foreach ($rs as $name => $value) {
			if (!isset($this->fields[$name]) || $this->fields[$name]->FldIsCustom)
				continue;
			$sql .= $this->fields[$name]->FldExpression . "=";
			$sql .= ew_QuotedValue($value, $this->fields[$name]->FldDataType, $this->DBID) . ",";
		}
		while (substr($sql, -1) == ",")
			$sql = substr($sql, 0, -1);
		$filter = ($curfilter) ? $this->CurrentFilter : "";
		if (is_array($where))
			$where = $this->ArrayToFilter($where);
		ew_AddFilter($filter, $where);
		if ($filter <> "")	$sql .= " WHERE " . $filter;
		return $sql;
	}

	// Update
	function Update(&$rs, $where = "", $rsold = NULL, $curfilter = TRUE) {
		$conn = &$this->Connection();
		return $conn->Execute($this->UpdateSQL($rs, $where, $curfilter));
	}

	// DELETE statement
	function DeleteSQL(&$rs, $where = "", $curfilter = TRUE) {
		$sql = "DELETE FROM " . $this->UpdateTable . " WHERE ";
		if (is_array($where))
			$where = $this->ArrayToFilter($where);
		if ($rs) {
			if (array_key_exists('VcrCodi', $rs))
				ew_AddFilter($where, ew_QuotedName('VcrCodi', $this->DBID) . '=' . ew_QuotedValue($rs['VcrCodi'], $this->VcrCodi->FldDataType, $this->DBID));
		}
		$filter = ($curfilter) ? $this->CurrentFilter : "";
		ew_AddFilter($filter, $where);
		if ($filter <> "")
			$sql .= $filter;
		else
			$sql .= "0=1"; // Avoid delete
		return $sql;
	}

	// Delete
	function Delete(&$rs, $where = "", $curfilter = TRUE) {
		$conn = &$this->Connection();
		return $conn->Execute($this->DeleteSQL($rs, $where, $curfilter));
	}

	// Key filter WHERE clause
	function SqlKeyFilter() {
		return "\"VcrCodi\" = @VcrCodi@";
	}

	// Key filter
	function KeyFilter() {
		$sKeyFilter = $this->SqlKeyFilter();
		if (!is_numeric($this->VcrCodi->CurrentValue))
			$sKeyFilter = "0=1"; // Invalid key
		$sKeyFilter = str_replace("@VcrCodi@", ew_AdjustSql($this->VcrCodi->CurrentValue, $this->DBID), $sKeyFilter); // Replace key value
		return $sKeyFilter;
	}

	// Return page URL
	function getReturnUrl() {
		$name = EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_RETURN_URL;

		// Get referer URL automatically
		if (ew_ServerVar("HTTP_REFERER") <> "" && ew_ReferPage() <> ew_CurrentPage() && ew_ReferPage() <> "login.php") // Referer not same page or login page
			$_SESSION[$name] = ew_ServerVar("HTTP_REFERER"); // Save to Session
		if (@$_SESSION[$name] <> "") {
			return $_SESSION[$name];
		} else {
			return "VCrelist.php";
		}
	}

	function setReturnUrl($v) {
		$_SESSION[EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_RETURN_URL] = $v;
	}

	// List URL
	function GetListUrl() {
		return "VCrelist.php";
	}

	// View URL
	function GetViewUrl($parm = "") {
		if ($parm <> "")
			$url = $this->KeyUrl("VCreview.php", $this->UrlParm($parm));
		else
			$url = $this->KeyUrl("VCreview.php", $this->UrlParm(EW_TABLE_SHOW_DETAIL . "="));
		return $this->AddMasterUrl($url);
	}

	// Add URL
	function GetAddUrl($parm = "") {
		if ($parm <> "")
			$url = "VCreadd.php?" . $this->UrlParm($parm);
		else
			$url = "VCreadd.php";
		return $this->AddMasterUrl($url);
	}

	// Edit URL
	function GetEditUrl($parm = "") {
		$url = $this->KeyUrl("VCreedit.php", $this->UrlParm($parm));
		return $this->AddMasterUrl($url);
	}

	// Inline edit URL
	function GetInlineEditUrl() {
		$url = $this->KeyUrl(ew_CurrentPage(), $this->UrlParm("a=edit"));
		return $this->AddMasterUrl($url);
	}

	// Copy URL
	function GetCopyUrl($parm = "") {
		$url = $this->KeyUrl("VCreadd.php", $this->UrlParm($parm));
		return $this->AddMasterUrl($url);
	}

	// Inline copy URL
	function GetInlineCopyUrl() {
		$url = $this->KeyUrl(ew_CurrentPage(), $this->UrlParm("a=copy"));
		return $this->AddMasterUrl($url);
	}

	// Delete URL
	function GetDeleteUrl() {
		return $this->KeyUrl("VCredelete.php", $this->UrlParm());
	}

	// Add master url
	function AddMasterUrl($url) {
		return $url;
	}

	function KeyToJson() {
		$json = "";
		$json .= "VcrCodi:" . ew_VarToJson($this->VcrCodi->CurrentValue, "number", "'");
		return "{" . $json . "}";
	}

	// Add key value to URL
	function KeyUrl($url, $parm = "") {
		$sUrl = $url . "?";
		if ($parm <> "") $sUrl .= $parm . "&";
		if (!is_null($this->VcrCodi->CurrentValue)) {
			$sUrl .= "VcrCodi=" . urlencode($this->VcrCodi->CurrentValue);
		} else {
			return "javascript:ew_Alert(ewLanguage.Phrase('InvalidRecord'));";
		}
		return $sUrl;
	}

	// Sort URL
	function SortUrl(&$fld) {
		if ($this->CurrentAction <> "" || $this->Export <> "" ||
			in_array($fld->FldType, array(128, 204, 205))) { // Unsortable data type
				return "";
		} elseif ($fld->Sortable) {
			$sUrlParm = $this->UrlParm("order=" . urlencode($fld->FldName) . "&amp;ordertype=" . $fld->ReverseSort());
			return ew_CurrentPage() . "?" . $sUrlParm;
		} else {
			return "";
		}
	}

	// Get record keys from $_POST/$_GET/$_SESSION
	function GetRecordKeys() {
		global $EW_COMPOSITE_KEY_SEPARATOR;
		$arKeys = array();
		$arKey = array();
		if (isset($_POST["key_m"])) {
			$arKeys = ew_StripSlashes($_POST["key_m"]);
			$cnt = count($arKeys);
		} elseif (isset($_GET["key_m"])) {
			$arKeys = ew_StripSlashes($_GET["key_m"]);
			$cnt = count($arKeys);
		} elseif (!empty($_GET) || !empty($_POST)) {
			$isPost = ew_IsHttpPost();
			$arKeys[] = $isPost ? ew_StripSlashes(@$_POST["VcrCodi"]) : ew_StripSlashes(@$_GET["VcrCodi"]); // VcrCodi

			//return $arKeys; // Do not return yet, so the values will also be checked by the following code
		}

		// Check keys
		$ar = array();
		foreach ($arKeys as $key) {
			if (!is_numeric($key))
				continue;
			$ar[] = $key;
		}
		return $ar;
	}

	// Get key filter
	function GetKeyFilter() {
		$arKeys = $this->GetRecordKeys();
		$sKeyFilter = "";
		foreach ($arKeys as $key) {
			if ($sKeyFilter <> "") $sKeyFilter .= " OR ";
			$this->VcrCodi->CurrentValue = $key;
			$sKeyFilter .= "(" . $this->KeyFilter() . ")";
		}
		return $sKeyFilter;
	}

	// Load rows based on filter
	function &LoadRs($sFilter) {

		// Set up filter (SQL WHERE clause) and get return SQL
		//$this->CurrentFilter = $sFilter;
		//$sSql = $this->SQL();

		$sSql = $this->GetSQL($sFilter, "");
		$conn = &$this->Connection();
		$rs = $conn->Execute($sSql);
		return $rs;
	}

	// Load row values from recordset
	function LoadListRowValues(&$rs) {
		$this->VcrCodi->setDbValue($rs->fields('VcrCodi'));
		$this->VcrCCuo->setDbValue($rs->fields('VcrCCuo'));
		$this->VcrMCuo->setDbValue($rs->fields('VcrMCuo'));
		$this->VcrEIni->setDbValue($rs->fields('VcrEIni'));
		$this->VcrTInt->setDbValue($rs->fields('VcrTInt'));
		$this->VcrMTot->setDbValue($rs->fields('VcrMTot'));
		$this->VcrEsta->setDbValue($rs->fields('VcrEsta'));
		$this->VcrDes->setDbValue($rs->fields('VcrDes'));
		$this->VcrHas->setDbValue($rs->fields('VcrHas'));
	}

	// Render list row values
	function RenderListRow() {
		global $Security, $gsLanguage, $Language;

		// Call Row Rendering event
		$this->Row_Rendering();

   // Common render codes
		// VcrCodi
		// VcrCCuo
		// VcrMCuo
		// VcrEIni
		// VcrTInt
		// VcrMTot
		// VcrEsta
		// VcrDes
		// VcrHas
		// VcrCodi

		$this->VcrCodi->ViewValue = $this->VcrCodi->CurrentValue;
		$this->VcrCodi->ViewCustomAttributes = "";

		// VcrCCuo
		$this->VcrCCuo->ViewValue = $this->VcrCCuo->CurrentValue;
		$this->VcrCCuo->ViewCustomAttributes = "";

		// VcrMCuo
		$this->VcrMCuo->ViewValue = $this->VcrMCuo->CurrentValue;
		$this->VcrMCuo->ViewCustomAttributes = "";

		// VcrEIni
		$this->VcrEIni->ViewValue = $this->VcrEIni->CurrentValue;
		$this->VcrEIni->ViewCustomAttributes = "";

		// VcrTInt
		$this->VcrTInt->ViewValue = $this->VcrTInt->CurrentValue;
		$this->VcrTInt->ViewCustomAttributes = "";

		// VcrMTot
		$this->VcrMTot->ViewValue = $this->VcrMTot->CurrentValue;
		$this->VcrMTot->ViewCustomAttributes = "";

		// VcrEsta
		$this->VcrEsta->ViewValue = $this->VcrEsta->CurrentValue;
		$this->VcrEsta->ViewCustomAttributes = "";

		// VcrDes
		$this->VcrDes->ViewValue = $this->VcrDes->CurrentValue;
		$this->VcrDes->ViewValue = ew_FormatDateTime($this->VcrDes->ViewValue, 7);
		$this->VcrDes->ViewCustomAttributes = "";

		// VcrHas
		$this->VcrHas->ViewValue = $this->VcrHas->CurrentValue;
		$this->VcrHas->ViewValue = ew_FormatDateTime($this->VcrHas->ViewValue, 7);
		$this->VcrHas->ViewCustomAttributes = "";

		// VcrCodi
		$this->VcrCodi->LinkCustomAttributes = "";
		$this->VcrCodi->HrefValue = "";
		$this->VcrCodi->TooltipValue = "";

		// VcrCCuo
		$this->VcrCCuo->LinkCustomAttributes = "";
		$this->VcrCCuo->HrefValue = "";
		$this->VcrCCuo->TooltipValue = "";

		// VcrMCuo
		$this->VcrMCuo->LinkCustomAttributes = "";
		$this->VcrMCuo->HrefValue = "";
		$this->VcrMCuo->TooltipValue = "";

		// VcrEIni
		$this->VcrEIni->LinkCustomAttributes = "";
		$this->VcrEIni->HrefValue = "";
		$this->VcrEIni->TooltipValue = "";

		// VcrTInt
		$this->VcrTInt->LinkCustomAttributes = "";
		$this->VcrTInt->HrefValue = "";
		$this->VcrTInt->TooltipValue = "";

		// VcrMTot
		$this->VcrMTot->LinkCustomAttributes = "";
		$this->VcrMTot->HrefValue = "";
		$this->VcrMTot->TooltipValue = "";

		// VcrEsta
		$this->VcrEsta->LinkCustomAttributes = "";
		$this->VcrEsta->HrefValue = "";
		$this->VcrEsta->TooltipValue = "";

		// VcrDes
		$this->VcrDes->LinkCustomAttributes = "";
		$this->VcrDes->HrefValue = "";
		$this->VcrDes->TooltipValue = "";

		// VcrHas
		$this->VcrHas->LinkCustomAttributes = "";
		$this->VcrHas->HrefValue = "";
		$this->VcrHas->TooltipValue = "";

		// Call Row Rendered event
		$this->Row_Rendered();
	}

	// Render edit row values
	function RenderEditRow() {
		global $Security, $gsLanguage, $Language;

		// Call Row Rendering event
		$this->Row_Rendering();

		// VcrCodi
		$this->VcrCodi->EditAttrs["class"] = "form-control";
		$this->VcrCodi->EditCustomAttributes = "";
		$this->VcrCodi->EditValue = $this->VcrCodi->CurrentValue;
		$this->VcrCodi->ViewCustomAttributes = "";

		// VcrCCuo
		$this->VcrCCuo->EditAttrs["class"] = "form-control";
		$this->VcrCCuo->EditCustomAttributes = "";
		$this->VcrCCuo->EditValue = $this->VcrCCuo->CurrentValue;
		$this->VcrCCuo->PlaceHolder = ew_RemoveHtml($this->VcrCCuo->FldCaption());

		// VcrMCuo
		$this->VcrMCuo->EditAttrs["class"] = "form-control";
		$this->VcrMCuo->EditCustomAttributes = "";
		$this->VcrMCuo->EditValue = $this->VcrMCuo->CurrentValue;
		$this->VcrMCuo->PlaceHolder = ew_RemoveHtml($this->VcrMCuo->FldCaption());
		if (strval($this->VcrMCuo->EditValue) <> "" && is_numeric($this->VcrMCuo->EditValue)) $this->VcrMCuo->EditValue = ew_FormatNumber($this->VcrMCuo->EditValue, -2, -1, -2, 0);

		// VcrEIni
		$this->VcrEIni->EditAttrs["class"] = "form-control";
		$this->VcrEIni->EditCustomAttributes = "";
		$this->VcrEIni->EditValue = $this->VcrEIni->CurrentValue;
		$this->VcrEIni->PlaceHolder = ew_RemoveHtml($this->VcrEIni->FldCaption());
		if (strval($this->VcrEIni->EditValue) <> "" && is_numeric($this->VcrEIni->EditValue)) $this->VcrEIni->EditValue = ew_FormatNumber($this->VcrEIni->EditValue, -2, -1, -2, 0);

		// VcrTInt
		$this->VcrTInt->EditAttrs["class"] = "form-control";
		$this->VcrTInt->EditCustomAttributes = "";
		$this->VcrTInt->EditValue = $this->VcrTInt->CurrentValue;
		$this->VcrTInt->PlaceHolder = ew_RemoveHtml($this->VcrTInt->FldCaption());
		if (strval($this->VcrTInt->EditValue) <> "" && is_numeric($this->VcrTInt->EditValue)) $this->VcrTInt->EditValue = ew_FormatNumber($this->VcrTInt->EditValue, -2, -1, -2, 0);

		// VcrMTot
		$this->VcrMTot->EditAttrs["class"] = "form-control";
		$this->VcrMTot->EditCustomAttributes = "";
		$this->VcrMTot->EditValue = $this->VcrMTot->CurrentValue;
		$this->VcrMTot->PlaceHolder = ew_RemoveHtml($this->VcrMTot->FldCaption());
		if (strval($this->VcrMTot->EditValue) <> "" && is_numeric($this->VcrMTot->EditValue)) $this->VcrMTot->EditValue = ew_FormatNumber($this->VcrMTot->EditValue, -2, -1, -2, 0);

		// VcrEsta
		$this->VcrEsta->EditAttrs["class"] = "form-control";
		$this->VcrEsta->EditCustomAttributes = "";
		$this->VcrEsta->EditValue = $this->VcrEsta->CurrentValue;
		$this->VcrEsta->PlaceHolder = ew_RemoveHtml($this->VcrEsta->FldCaption());

		// VcrDes
		$this->VcrDes->EditAttrs["class"] = "form-control";
		$this->VcrDes->EditCustomAttributes = "";
		$this->VcrDes->EditValue = ew_FormatDateTime($this->VcrDes->CurrentValue, 7);
		$this->VcrDes->PlaceHolder = ew_RemoveHtml($this->VcrDes->FldCaption());

		// VcrHas
		$this->VcrHas->EditAttrs["class"] = "form-control";
		$this->VcrHas->EditCustomAttributes = "";
		$this->VcrHas->EditValue = ew_FormatDateTime($this->VcrHas->CurrentValue, 7);
		$this->VcrHas->PlaceHolder = ew_RemoveHtml($this->VcrHas->FldCaption());

		// Call Row Rendered event
		$this->Row_Rendered();
	}

	// Aggregate list row values
	function AggregateListRowValues() {
	}

	// Aggregate list row (for rendering)
	function AggregateListRow() {

		// Call Row Rendered event
		$this->Row_Rendered();
	}
	var $ExportDoc;

	// Export data in HTML/CSV/Word/Excel/Email/PDF format
	function ExportDocument(&$Doc, &$Recordset, $StartRec, $StopRec, $ExportPageType = "") {
		if (!$Recordset || !$Doc)
			return;
		if (!$Doc->ExportCustom) {

			// Write header
			$Doc->ExportTableHeader();
			if ($Doc->Horizontal) { // Horizontal format, write header
				$Doc->BeginExportRow();
				if ($ExportPageType == "view") {
					if ($this->VcrCodi->Exportable) $Doc->ExportCaption($this->VcrCodi);
					if ($this->VcrCCuo->Exportable) $Doc->ExportCaption($this->VcrCCuo);
					if ($this->VcrMCuo->Exportable) $Doc->ExportCaption($this->VcrMCuo);
					if ($this->VcrEIni->Exportable) $Doc->ExportCaption($this->VcrEIni);
					if ($this->VcrTInt->Exportable) $Doc->ExportCaption($this->VcrTInt);
					if ($this->VcrMTot->Exportable) $Doc->ExportCaption($this->VcrMTot);
					if ($this->VcrEsta->Exportable) $Doc->ExportCaption($this->VcrEsta);
					if ($this->VcrDes->Exportable) $Doc->ExportCaption($this->VcrDes);
					if ($this->VcrHas->Exportable) $Doc->ExportCaption($this->VcrHas);
				} else {
					if ($this->VcrCodi->Exportable) $Doc->ExportCaption($this->VcrCodi);
					if ($this->VcrCCuo->Exportable) $Doc->ExportCaption($this->VcrCCuo);
					if ($this->VcrMCuo->Exportable) $Doc->ExportCaption($this->VcrMCuo);
					if ($this->VcrEIni->Exportable) $Doc->ExportCaption($this->VcrEIni);
					if ($this->VcrTInt->Exportable) $Doc->ExportCaption($this->VcrTInt);
					if ($this->VcrMTot->Exportable) $Doc->ExportCaption($this->VcrMTot);
					if ($this->VcrEsta->Exportable) $Doc->ExportCaption($this->VcrEsta);
					if ($this->VcrDes->Exportable) $Doc->ExportCaption($this->VcrDes);
					if ($this->VcrHas->Exportable) $Doc->ExportCaption($this->VcrHas);
				}
				$Doc->EndExportRow();
			}
		}

		// Move to first record
		$RecCnt = $StartRec - 1;
		if (!$Recordset->EOF) {
			$Recordset->MoveFirst();
			if ($StartRec > 1)
				$Recordset->Move($StartRec - 1);
		}
		while (!$Recordset->EOF && $RecCnt < $StopRec) {
			$RecCnt++;
			if (intval($RecCnt) >= intval($StartRec)) {
				$RowCnt = intval($RecCnt) - intval($StartRec) + 1;

				// Page break
				if ($this->ExportPageBreakCount > 0) {
					if ($RowCnt > 1 && ($RowCnt - 1) % $this->ExportPageBreakCount == 0)
						$Doc->ExportPageBreak();
				}
				$this->LoadListRowValues($Recordset);

				// Render row
				$this->RowType = EW_ROWTYPE_VIEW; // Render view
				$this->ResetAttrs();
				$this->RenderListRow();
				if (!$Doc->ExportCustom) {
					$Doc->BeginExportRow($RowCnt); // Allow CSS styles if enabled
					if ($ExportPageType == "view") {
						if ($this->VcrCodi->Exportable) $Doc->ExportField($this->VcrCodi);
						if ($this->VcrCCuo->Exportable) $Doc->ExportField($this->VcrCCuo);
						if ($this->VcrMCuo->Exportable) $Doc->ExportField($this->VcrMCuo);
						if ($this->VcrEIni->Exportable) $Doc->ExportField($this->VcrEIni);
						if ($this->VcrTInt->Exportable) $Doc->ExportField($this->VcrTInt);
						if ($this->VcrMTot->Exportable) $Doc->ExportField($this->VcrMTot);
						if ($this->VcrEsta->Exportable) $Doc->ExportField($this->VcrEsta);
						if ($this->VcrDes->Exportable) $Doc->ExportField($this->VcrDes);
						if ($this->VcrHas->Exportable) $Doc->ExportField($this->VcrHas);
					} else {
						if ($this->VcrCodi->Exportable) $Doc->ExportField($this->VcrCodi);
						if ($this->VcrCCuo->Exportable) $Doc->ExportField($this->VcrCCuo);
						if ($this->VcrMCuo->Exportable) $Doc->ExportField($this->VcrMCuo);
						if ($this->VcrEIni->Exportable) $Doc->ExportField($this->VcrEIni);
						if ($this->VcrTInt->Exportable) $Doc->ExportField($this->VcrTInt);
						if ($this->VcrMTot->Exportable) $Doc->ExportField($this->VcrMTot);
						if ($this->VcrEsta->Exportable) $Doc->ExportField($this->VcrEsta);
						if ($this->VcrDes->Exportable) $Doc->ExportField($this->VcrDes);
						if ($this->VcrHas->Exportable) $Doc->ExportField($this->VcrHas);
					}
					$Doc->EndExportRow();
				}
			}

			// Call Row Export server event
			if ($Doc->ExportCustom)
				$this->Row_Export($Recordset->fields);
			$Recordset->MoveNext();
		}
		if (!$Doc->ExportCustom) {
			$Doc->ExportTableFooter();
		}
	}

	// Get auto fill value
	function GetAutoFill($id, $val) {
		$rsarr = array();
		$rowcnt = 0;

		// Output
		if (is_array($rsarr) && $rowcnt > 0) {
			$fldcnt = count($rsarr[0]);
			for ($i = 0; $i < $rowcnt; $i++) {
				for ($j = 0; $j < $fldcnt; $j++) {
					$str = strval($rsarr[$i][$j]);
					$str = ew_ConvertToUtf8($str);
					if (isset($post["keepCRLF"])) {
						$str = str_replace(array("\r", "\n"), array("\\r", "\\n"), $str);
					} else {
						$str = str_replace(array("\r", "\n"), array(" ", " "), $str);
					}
					$rsarr[$i][$j] = $str;
				}
			}
			return ew_ArrayToJson($rsarr);
		} else {
			return FALSE;
		}
	}

	// Table level events
	// Recordset Selecting event
	function Recordset_Selecting(&$filter) {

		// Enter your code here	
	}

	// Recordset Selected event
	function Recordset_Selected(&$rs) {

		//echo "Recordset Selected";
	}

	// Recordset Search Validated event
	function Recordset_SearchValidated() {

		// Example:
		//$this->MyField1->AdvancedSearch->SearchValue = "your search criteria"; // Search value

	}

	// Recordset Searching event
	function Recordset_Searching(&$filter) {

		// Enter your code here	
	}

	// Row_Selecting event
	function Row_Selecting(&$filter) {

		// Enter your code here	
	}

	// Row Selected event
	function Row_Selected(&$rs) {

		//echo "Row Selected";
	}

	// Row Inserting event
	function Row_Inserting($rsold, &$rsnew) {

		// Enter your code here
		// To cancel, set return value to FALSE

		return TRUE;
	}

	// Row Inserted event
	function Row_Inserted($rsold, &$rsnew) {

		//echo "Row Inserted"
	}

	// Row Updating event
	function Row_Updating($rsold, &$rsnew) {

		// Enter your code here
		// To cancel, set return value to FALSE

		return TRUE;
	}

	// Row Updated event
	function Row_Updated($rsold, &$rsnew) {

		//echo "Row Updated";
	}

	// Row Update Conflict event
	function Row_UpdateConflict($rsold, &$rsnew) {

		// Enter your code here
		// To ignore conflict, set return value to FALSE

		return TRUE;
	}

	// Grid Inserting event
	function Grid_Inserting() {

		// Enter your code here
		// To reject grid insert, set return value to FALSE

		return TRUE;
	}

	// Grid Inserted event
	function Grid_Inserted($rsnew) {

		//echo "Grid Inserted";
	}

	// Grid Updating event
	function Grid_Updating($rsold) {

		// Enter your code here
		// To reject grid update, set return value to FALSE

		return TRUE;
	}

	// Grid Updated event
	function Grid_Updated($rsold, $rsnew) {

		//echo "Grid Updated";
	}

	// Row Deleting event
	function Row_Deleting(&$rs) {

		// Enter your code here
		// To cancel, set return value to False

		return TRUE;
	}

	// Row Deleted event
	function Row_Deleted(&$rs) {

		//echo "Row Deleted";
	}

	// Email Sending event
	function Email_Sending(&$Email, &$Args) {

		//var_dump($Email); var_dump($Args); exit();
		return TRUE;
	}

	// Lookup Selecting event
	function Lookup_Selecting($fld, &$filter) {

		//var_dump($fld->FldName, $fld->LookupFilters, $filter); // Uncomment to view the filter
		// Enter your code here

	}

	// Row Rendering event
	function Row_Rendering() {

		// Enter your code here	
	}

	// Row Rendered event
	function Row_Rendered() {

		// To view properties of field class, use:
		//var_dump($this-><FieldName>); 

	}

	// User ID Filtering event
	function UserID_Filtering(&$filter) {

		// Enter your code here
	}
}
?>
