<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "VCreinfo.php" ?>
<?php include_once "Usuainfo.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$VCre_delete = NULL; // Initialize page object first

class cVCre_delete extends cVCre {

	// Page ID
	var $PageID = 'delete';

	// Project ID
	var $ProjectID = "{04439FF7-B43F-460F-8514-F71C8FF9E679}";

	// Table name
	var $TableName = 'VCre';

	// Page object name
	var $PageObjName = 'VCre_delete';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (VCre)
		if (!isset($GLOBALS["VCre"]) || get_class($GLOBALS["VCre"]) == "cVCre") {
			$GLOBALS["VCre"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["VCre"];
		}

		// Table object (Usua)
		if (!isset($GLOBALS['Usua'])) $GLOBALS['Usua'] = new cUsua();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'delete', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'VCre', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (Usua)
		if (!isset($UserTable)) {
			$UserTable = new cUsua();
			$UserTableConn = Conn($UserTable->DBID);
		}
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanDelete()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage($Language->Phrase("NoPermission")); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("VCrelist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action
		$this->VcrCodi->Visible = !$this->IsAdd() && !$this->IsCopy() && !$this->IsGridAdd();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $VCre;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($VCre);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $DbMasterFilter = "";
	var $DbDetailFilter = "";
	var $StartRec;
	var $TotalRecs = 0;
	var $RecCnt;
	var $RecKeys = array();
	var $Recordset;
	var $StartRowCnt = 1;
	var $RowCnt = 0;

	//
	// Page main
	//
	function Page_Main() {
		global $Language;

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Load key parameters
		$this->RecKeys = $this->GetRecordKeys(); // Load record keys
		$sFilter = $this->GetKeyFilter();
		if ($sFilter == "")
			$this->Page_Terminate("VCrelist.php"); // Prevent SQL injection, return to list

		// Set up filter (SQL WHHERE clause) and get return SQL
		// SQL constructor in VCre class, VCreinfo.php

		$this->CurrentFilter = $sFilter;

		// Get action
		if (@$_POST["a_delete"] <> "") {
			$this->CurrentAction = $_POST["a_delete"];
		} else {
			$this->CurrentAction = "I"; // Display record
		}
		switch ($this->CurrentAction) {
			case "D": // Delete
				$this->SendEmail = TRUE; // Send email on delete success
				if ($this->DeleteRows()) { // Delete rows
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage($Language->Phrase("DeleteSuccess")); // Set up success message
					$this->Page_Terminate($this->getReturnUrl()); // Return to caller
				}
		}
	}

	// Load recordset
	function LoadRecordset($offset = -1, $rowcnt = -1) {

		// Load List page SQL
		$sSql = $this->SelectSQL();
		$conn = &$this->Connection();

		// Load recordset
		$dbtype = ew_GetConnectionType($this->DBID);
		if ($this->UseSelectLimit) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			if ($dbtype == "MSSQL") {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset, array("_hasOrderBy" => trim($this->getOrderBy()) || trim($this->getSessionOrderBy())));
			} else {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset);
			}
			$conn->raiseErrorFn = '';
		} else {
			$rs = ew_LoadRecordset($sSql, $conn);
		}

		// Call Recordset Selected event
		$this->Recordset_Selected($rs);
		return $rs;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->VcrCodi->setDbValue($rs->fields('VcrCodi'));
		$this->VcrCCuo->setDbValue($rs->fields('VcrCCuo'));
		$this->VcrMCuo->setDbValue($rs->fields('VcrMCuo'));
		$this->VcrEIni->setDbValue($rs->fields('VcrEIni'));
		$this->VcrTInt->setDbValue($rs->fields('VcrTInt'));
		$this->VcrMTot->setDbValue($rs->fields('VcrMTot'));
		$this->VcrEsta->setDbValue($rs->fields('VcrEsta'));
		$this->VcrDes->setDbValue($rs->fields('VcrDes'));
		$this->VcrHas->setDbValue($rs->fields('VcrHas'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->VcrCodi->DbValue = $row['VcrCodi'];
		$this->VcrCCuo->DbValue = $row['VcrCCuo'];
		$this->VcrMCuo->DbValue = $row['VcrMCuo'];
		$this->VcrEIni->DbValue = $row['VcrEIni'];
		$this->VcrTInt->DbValue = $row['VcrTInt'];
		$this->VcrMTot->DbValue = $row['VcrMTot'];
		$this->VcrEsta->DbValue = $row['VcrEsta'];
		$this->VcrDes->DbValue = $row['VcrDes'];
		$this->VcrHas->DbValue = $row['VcrHas'];
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		// Convert decimal values if posted back

		if ($this->VcrMCuo->FormValue == $this->VcrMCuo->CurrentValue && is_numeric(ew_StrToFloat($this->VcrMCuo->CurrentValue)))
			$this->VcrMCuo->CurrentValue = ew_StrToFloat($this->VcrMCuo->CurrentValue);

		// Convert decimal values if posted back
		if ($this->VcrEIni->FormValue == $this->VcrEIni->CurrentValue && is_numeric(ew_StrToFloat($this->VcrEIni->CurrentValue)))
			$this->VcrEIni->CurrentValue = ew_StrToFloat($this->VcrEIni->CurrentValue);

		// Convert decimal values if posted back
		if ($this->VcrTInt->FormValue == $this->VcrTInt->CurrentValue && is_numeric(ew_StrToFloat($this->VcrTInt->CurrentValue)))
			$this->VcrTInt->CurrentValue = ew_StrToFloat($this->VcrTInt->CurrentValue);

		// Convert decimal values if posted back
		if ($this->VcrMTot->FormValue == $this->VcrMTot->CurrentValue && is_numeric(ew_StrToFloat($this->VcrMTot->CurrentValue)))
			$this->VcrMTot->CurrentValue = ew_StrToFloat($this->VcrMTot->CurrentValue);

		// Call Row_Rendering event
		$this->Row_Rendering();

		// Common render codes for all row types
		// VcrCodi
		// VcrCCuo
		// VcrMCuo
		// VcrEIni
		// VcrTInt
		// VcrMTot
		// VcrEsta
		// VcrDes
		// VcrHas

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// VcrCodi
		$this->VcrCodi->ViewValue = $this->VcrCodi->CurrentValue;
		$this->VcrCodi->ViewCustomAttributes = "";

		// VcrCCuo
		$this->VcrCCuo->ViewValue = $this->VcrCCuo->CurrentValue;
		$this->VcrCCuo->ViewCustomAttributes = "";

		// VcrMCuo
		$this->VcrMCuo->ViewValue = $this->VcrMCuo->CurrentValue;
		$this->VcrMCuo->ViewCustomAttributes = "";

		// VcrEIni
		$this->VcrEIni->ViewValue = $this->VcrEIni->CurrentValue;
		$this->VcrEIni->ViewCustomAttributes = "";

		// VcrTInt
		$this->VcrTInt->ViewValue = $this->VcrTInt->CurrentValue;
		$this->VcrTInt->ViewCustomAttributes = "";

		// VcrMTot
		$this->VcrMTot->ViewValue = $this->VcrMTot->CurrentValue;
		$this->VcrMTot->ViewCustomAttributes = "";

		// VcrEsta
		$this->VcrEsta->ViewValue = $this->VcrEsta->CurrentValue;
		$this->VcrEsta->ViewCustomAttributes = "";

		// VcrDes
		$this->VcrDes->ViewValue = $this->VcrDes->CurrentValue;
		$this->VcrDes->ViewValue = ew_FormatDateTime($this->VcrDes->ViewValue, 7);
		$this->VcrDes->ViewCustomAttributes = "";

		// VcrHas
		$this->VcrHas->ViewValue = $this->VcrHas->CurrentValue;
		$this->VcrHas->ViewValue = ew_FormatDateTime($this->VcrHas->ViewValue, 7);
		$this->VcrHas->ViewCustomAttributes = "";

			// VcrCodi
			$this->VcrCodi->LinkCustomAttributes = "";
			$this->VcrCodi->HrefValue = "";
			$this->VcrCodi->TooltipValue = "";

			// VcrCCuo
			$this->VcrCCuo->LinkCustomAttributes = "";
			$this->VcrCCuo->HrefValue = "";
			$this->VcrCCuo->TooltipValue = "";

			// VcrMCuo
			$this->VcrMCuo->LinkCustomAttributes = "";
			$this->VcrMCuo->HrefValue = "";
			$this->VcrMCuo->TooltipValue = "";

			// VcrEIni
			$this->VcrEIni->LinkCustomAttributes = "";
			$this->VcrEIni->HrefValue = "";
			$this->VcrEIni->TooltipValue = "";

			// VcrTInt
			$this->VcrTInt->LinkCustomAttributes = "";
			$this->VcrTInt->HrefValue = "";
			$this->VcrTInt->TooltipValue = "";

			// VcrMTot
			$this->VcrMTot->LinkCustomAttributes = "";
			$this->VcrMTot->HrefValue = "";
			$this->VcrMTot->TooltipValue = "";

			// VcrEsta
			$this->VcrEsta->LinkCustomAttributes = "";
			$this->VcrEsta->HrefValue = "";
			$this->VcrEsta->TooltipValue = "";

			// VcrDes
			$this->VcrDes->LinkCustomAttributes = "";
			$this->VcrDes->HrefValue = "";
			$this->VcrDes->TooltipValue = "";

			// VcrHas
			$this->VcrHas->LinkCustomAttributes = "";
			$this->VcrHas->HrefValue = "";
			$this->VcrHas->TooltipValue = "";
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	//
	// Delete records based on current filter
	//
	function DeleteRows() {
		global $Language, $Security;
		if (!$Security->CanDelete()) {
			$this->setFailureMessage($Language->Phrase("NoDeletePermission")); // No delete permission
			return FALSE;
		}
		$DeleteRows = TRUE;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE) {
			return FALSE;
		} elseif ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
			$rs->Close();
			return FALSE;

		//} else {
		//	$this->LoadRowValues($rs); // Load row values

		}
		$rows = ($rs) ? $rs->GetRows() : array();
		$conn->BeginTrans();

		// Clone old rows
		$rsold = $rows;
		if ($rs)
			$rs->Close();

		// Call row deleting event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$DeleteRows = $this->Row_Deleting($row);
				if (!$DeleteRows) break;
			}
		}
		if ($DeleteRows) {
			$sKey = "";
			foreach ($rsold as $row) {
				$sThisKey = "";
				if ($sThisKey <> "") $sThisKey .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
				$sThisKey .= $row['VcrCodi'];
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				$DeleteRows = $this->Delete($row); // Delete
				$conn->raiseErrorFn = '';
				if ($DeleteRows === FALSE)
					break;
				if ($sKey <> "") $sKey .= ", ";
				$sKey .= $sThisKey;
			}
		} else {

			// Set up error message
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("DeleteCancelled"));
			}
		}
		if ($DeleteRows) {
			$conn->CommitTrans(); // Commit the changes
		} else {
			$conn->RollbackTrans(); // Rollback changes
		}

		// Call Row Deleted event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$this->Row_Deleted($row);
			}
		}
		return $DeleteRows;
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, "VCrelist.php", "", $this->TableVar, TRUE);
		$PageId = "delete";
		$Breadcrumb->Add("delete", $PageId, $url);
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($VCre_delete)) $VCre_delete = new cVCre_delete();

// Page init
$VCre_delete->Page_Init();

// Page main
$VCre_delete->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$VCre_delete->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "delete";
var CurrentForm = fVCredelete = new ew_Form("fVCredelete", "delete");

// Form_CustomValidate event
fVCredelete.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fVCredelete.ValidateRequired = true;
<?php } else { ?>
fVCredelete.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
// Form object for search

</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<?php

// Load records for display
if ($VCre_delete->Recordset = $VCre_delete->LoadRecordset())
	$VCre_deleteTotalRecs = $VCre_delete->Recordset->RecordCount(); // Get record count
if ($VCre_deleteTotalRecs <= 0) { // No record found, exit
	if ($VCre_delete->Recordset)
		$VCre_delete->Recordset->Close();
	$VCre_delete->Page_Terminate("VCrelist.php"); // Return to list
}
?>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php $VCre_delete->ShowPageHeader(); ?>
<?php
$VCre_delete->ShowMessage();
?>
<form name="fVCredelete" id="fVCredelete" class="form-inline ewForm ewDeleteForm" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($VCre_delete->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $VCre_delete->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="VCre">
<input type="hidden" name="a_delete" id="a_delete" value="D">
<?php foreach ($VCre_delete->RecKeys as $key) { ?>
<?php $keyvalue = is_array($key) ? implode($EW_COMPOSITE_KEY_SEPARATOR, $key) : $key; ?>
<input type="hidden" name="key_m[]" value="<?php echo ew_HtmlEncode($keyvalue) ?>">
<?php } ?>
<div class="ewGrid">
<div class="<?php if (ew_IsResponsiveLayout()) { echo "table-responsive "; } ?>ewGridMiddlePanel">
<table class="table ewTable">
<?php echo $VCre->TableCustomInnerHtml ?>
	<thead>
	<tr class="ewTableHeader">
<?php if ($VCre->VcrCodi->Visible) { // VcrCodi ?>
		<th><span id="elh_VCre_VcrCodi" class="VCre_VcrCodi"><?php echo $VCre->VcrCodi->FldCaption() ?></span></th>
<?php } ?>
<?php if ($VCre->VcrCCuo->Visible) { // VcrCCuo ?>
		<th><span id="elh_VCre_VcrCCuo" class="VCre_VcrCCuo"><?php echo $VCre->VcrCCuo->FldCaption() ?></span></th>
<?php } ?>
<?php if ($VCre->VcrMCuo->Visible) { // VcrMCuo ?>
		<th><span id="elh_VCre_VcrMCuo" class="VCre_VcrMCuo"><?php echo $VCre->VcrMCuo->FldCaption() ?></span></th>
<?php } ?>
<?php if ($VCre->VcrEIni->Visible) { // VcrEIni ?>
		<th><span id="elh_VCre_VcrEIni" class="VCre_VcrEIni"><?php echo $VCre->VcrEIni->FldCaption() ?></span></th>
<?php } ?>
<?php if ($VCre->VcrTInt->Visible) { // VcrTInt ?>
		<th><span id="elh_VCre_VcrTInt" class="VCre_VcrTInt"><?php echo $VCre->VcrTInt->FldCaption() ?></span></th>
<?php } ?>
<?php if ($VCre->VcrMTot->Visible) { // VcrMTot ?>
		<th><span id="elh_VCre_VcrMTot" class="VCre_VcrMTot"><?php echo $VCre->VcrMTot->FldCaption() ?></span></th>
<?php } ?>
<?php if ($VCre->VcrEsta->Visible) { // VcrEsta ?>
		<th><span id="elh_VCre_VcrEsta" class="VCre_VcrEsta"><?php echo $VCre->VcrEsta->FldCaption() ?></span></th>
<?php } ?>
<?php if ($VCre->VcrDes->Visible) { // VcrDes ?>
		<th><span id="elh_VCre_VcrDes" class="VCre_VcrDes"><?php echo $VCre->VcrDes->FldCaption() ?></span></th>
<?php } ?>
<?php if ($VCre->VcrHas->Visible) { // VcrHas ?>
		<th><span id="elh_VCre_VcrHas" class="VCre_VcrHas"><?php echo $VCre->VcrHas->FldCaption() ?></span></th>
<?php } ?>
	</tr>
	</thead>
	<tbody>
<?php
$VCre_delete->RecCnt = 0;
$i = 0;
while (!$VCre_delete->Recordset->EOF) {
	$VCre_delete->RecCnt++;
	$VCre_delete->RowCnt++;

	// Set row properties
	$VCre->ResetAttrs();
	$VCre->RowType = EW_ROWTYPE_VIEW; // View

	// Get the field contents
	$VCre_delete->LoadRowValues($VCre_delete->Recordset);

	// Render row
	$VCre_delete->RenderRow();
?>
	<tr<?php echo $VCre->RowAttributes() ?>>
<?php if ($VCre->VcrCodi->Visible) { // VcrCodi ?>
		<td<?php echo $VCre->VcrCodi->CellAttributes() ?>>
<span id="el<?php echo $VCre_delete->RowCnt ?>_VCre_VcrCodi" class="VCre_VcrCodi">
<span<?php echo $VCre->VcrCodi->ViewAttributes() ?>>
<?php echo $VCre->VcrCodi->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($VCre->VcrCCuo->Visible) { // VcrCCuo ?>
		<td<?php echo $VCre->VcrCCuo->CellAttributes() ?>>
<span id="el<?php echo $VCre_delete->RowCnt ?>_VCre_VcrCCuo" class="VCre_VcrCCuo">
<span<?php echo $VCre->VcrCCuo->ViewAttributes() ?>>
<?php echo $VCre->VcrCCuo->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($VCre->VcrMCuo->Visible) { // VcrMCuo ?>
		<td<?php echo $VCre->VcrMCuo->CellAttributes() ?>>
<span id="el<?php echo $VCre_delete->RowCnt ?>_VCre_VcrMCuo" class="VCre_VcrMCuo">
<span<?php echo $VCre->VcrMCuo->ViewAttributes() ?>>
<?php echo $VCre->VcrMCuo->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($VCre->VcrEIni->Visible) { // VcrEIni ?>
		<td<?php echo $VCre->VcrEIni->CellAttributes() ?>>
<span id="el<?php echo $VCre_delete->RowCnt ?>_VCre_VcrEIni" class="VCre_VcrEIni">
<span<?php echo $VCre->VcrEIni->ViewAttributes() ?>>
<?php echo $VCre->VcrEIni->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($VCre->VcrTInt->Visible) { // VcrTInt ?>
		<td<?php echo $VCre->VcrTInt->CellAttributes() ?>>
<span id="el<?php echo $VCre_delete->RowCnt ?>_VCre_VcrTInt" class="VCre_VcrTInt">
<span<?php echo $VCre->VcrTInt->ViewAttributes() ?>>
<?php echo $VCre->VcrTInt->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($VCre->VcrMTot->Visible) { // VcrMTot ?>
		<td<?php echo $VCre->VcrMTot->CellAttributes() ?>>
<span id="el<?php echo $VCre_delete->RowCnt ?>_VCre_VcrMTot" class="VCre_VcrMTot">
<span<?php echo $VCre->VcrMTot->ViewAttributes() ?>>
<?php echo $VCre->VcrMTot->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($VCre->VcrEsta->Visible) { // VcrEsta ?>
		<td<?php echo $VCre->VcrEsta->CellAttributes() ?>>
<span id="el<?php echo $VCre_delete->RowCnt ?>_VCre_VcrEsta" class="VCre_VcrEsta">
<span<?php echo $VCre->VcrEsta->ViewAttributes() ?>>
<?php echo $VCre->VcrEsta->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($VCre->VcrDes->Visible) { // VcrDes ?>
		<td<?php echo $VCre->VcrDes->CellAttributes() ?>>
<span id="el<?php echo $VCre_delete->RowCnt ?>_VCre_VcrDes" class="VCre_VcrDes">
<span<?php echo $VCre->VcrDes->ViewAttributes() ?>>
<?php echo $VCre->VcrDes->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($VCre->VcrHas->Visible) { // VcrHas ?>
		<td<?php echo $VCre->VcrHas->CellAttributes() ?>>
<span id="el<?php echo $VCre_delete->RowCnt ?>_VCre_VcrHas" class="VCre_VcrHas">
<span<?php echo $VCre->VcrHas->ViewAttributes() ?>>
<?php echo $VCre->VcrHas->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
	</tr>
<?php
	$VCre_delete->Recordset->MoveNext();
}
$VCre_delete->Recordset->Close();
?>
</tbody>
</table>
</div>
</div>
<div>
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("DeleteBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="button" data-href="<?php echo $VCre_delete->getReturnUrl() ?>"><?php echo $Language->Phrase("CancelBtn") ?></button>
</div>
</form>
<script type="text/javascript">
fVCredelete.Init();
</script>
<?php
$VCre_delete->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$VCre_delete->Page_Terminate();
?>
