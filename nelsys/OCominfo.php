<?php

// Global variable for table object
$OCom = NULL;

//
// Table class for OCom
//
class cOCom extends cTable {
	var $OcoCodi;
	var $PrvCodi;
	var $OcoLPed;
	var $OcoFCom;
	var $OcoProf;
	var $OcoEsta;

	//
	// Table class constructor
	//
	function __construct() {
		global $Language;

		// Language object
		if (!isset($Language)) $Language = new cLanguage();
		$this->TableVar = 'OCom';
		$this->TableName = 'OCom';
		$this->TableType = 'TABLE';

		// Update Table
		$this->UpdateTable = "\"public\".\"OCom\"";
		$this->DBID = 'DB';
		$this->ExportAll = TRUE;
		$this->ExportPageBreakCount = 0; // Page break per every n record (PDF only)
		$this->ExportPageOrientation = "portrait"; // Page orientation (PDF only)
		$this->ExportPageSize = "a4"; // Page size (PDF only)
		$this->ExportExcelPageOrientation = ""; // Page orientation (PHPExcel only)
		$this->ExportExcelPageSize = ""; // Page size (PHPExcel only)
		$this->DetailAdd = TRUE; // Allow detail add
		$this->DetailEdit = TRUE; // Allow detail edit
		$this->DetailView = FALSE; // Allow detail view
		$this->ShowMultipleDetails = FALSE; // Show multiple details
		$this->GridAddRowCount = 5;
		$this->AllowAddDeleteRow = ew_AllowAddDeleteRow(); // Allow add/delete row
		$this->UserIDAllowSecurity = 0; // User ID Allow
		$this->BasicSearch = new cBasicSearch($this->TableVar);

		// OcoCodi
		$this->OcoCodi = new cField('OCom', 'OCom', 'x_OcoCodi', 'OcoCodi', '"OcoCodi"', 'CAST("OcoCodi" AS varchar(255))', 3, -1, FALSE, '"OcoCodi"', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'NO');
		$this->OcoCodi->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['OcoCodi'] = &$this->OcoCodi;

		// PrvCodi
		$this->PrvCodi = new cField('OCom', 'OCom', 'x_PrvCodi', 'PrvCodi', '"PrvCodi"', 'CAST("PrvCodi" AS varchar(255))', 3, -1, FALSE, '"PrvCodi"', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'SELECT');
		$this->PrvCodi->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['PrvCodi'] = &$this->PrvCodi;

		// OcoLPed
		$this->OcoLPed = new cField('OCom', 'OCom', 'x_OcoLPed', 'OcoLPed', '"OcoLPed"', 'CAST("OcoLPed" AS varchar(255))', 205, -1, TRUE, '"OcoLPed"', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'FILE');
		$this->fields['OcoLPed'] = &$this->OcoLPed;

		// OcoFCom
		$this->OcoFCom = new cField('OCom', 'OCom', 'x_OcoFCom', 'OcoFCom', '"OcoFCom"', 'CAST("OcoFCom" AS varchar(255))', 133, 7, FALSE, '"OcoFCom"', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->OcoFCom->FldDefaultErrMsg = str_replace("%s", "/", $Language->Phrase("IncorrectDateDMY"));
		$this->fields['OcoFCom'] = &$this->OcoFCom;

		// OcoProf
		$this->OcoProf = new cField('OCom', 'OCom', 'x_OcoProf', 'OcoProf', '"OcoProf"', 'CAST("OcoProf" AS varchar(255))', 205, -1, TRUE, '"OcoProf"', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'FILE');
		$this->fields['OcoProf'] = &$this->OcoProf;

		// OcoEsta
		$this->OcoEsta = new cField('OCom', 'OCom', 'x_OcoEsta', 'OcoEsta', '"OcoEsta"', '"OcoEsta"', 200, -1, FALSE, '"OcoEsta"', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->fields['OcoEsta'] = &$this->OcoEsta;
	}

	// Multiple column sort
	function UpdateSort(&$ofld, $ctrl) {
		if ($this->CurrentOrder == $ofld->FldName) {
			$sSortField = $ofld->FldExpression;
			$sLastSort = $ofld->getSort();
			if ($this->CurrentOrderType == "ASC" || $this->CurrentOrderType == "DESC") {
				$sThisSort = $this->CurrentOrderType;
			} else {
				$sThisSort = ($sLastSort == "ASC") ? "DESC" : "ASC";
			}
			$ofld->setSort($sThisSort);
			if ($ctrl) {
				$sOrderBy = $this->getSessionOrderBy();
				if (strpos($sOrderBy, $sSortField . " " . $sLastSort) !== FALSE) {
					$sOrderBy = str_replace($sSortField . " " . $sLastSort, $sSortField . " " . $sThisSort, $sOrderBy);
				} else {
					if ($sOrderBy <> "") $sOrderBy .= ", ";
					$sOrderBy .= $sSortField . " " . $sThisSort;
				}
				$this->setSessionOrderBy($sOrderBy); // Save to Session
			} else {
				$this->setSessionOrderBy($sSortField . " " . $sThisSort); // Save to Session
			}
		} else {
			if (!$ctrl) $ofld->setSort("");
		}
	}

	// Table level SQL
	var $_SqlFrom = "";

	function getSqlFrom() { // From
		return ($this->_SqlFrom <> "") ? $this->_SqlFrom : "\"public\".\"OCom\"";
	}

	function SqlFrom() { // For backward compatibility
    	return $this->getSqlFrom();
	}

	function setSqlFrom($v) {
    	$this->_SqlFrom = $v;
	}
	var $_SqlSelect = "";

	function getSqlSelect() { // Select
		return ($this->_SqlSelect <> "") ? $this->_SqlSelect : "SELECT * FROM " . $this->getSqlFrom();
	}

	function SqlSelect() { // For backward compatibility
    	return $this->getSqlSelect();
	}

	function setSqlSelect($v) {
    	$this->_SqlSelect = $v;
	}
	var $_SqlWhere = "";

	function getSqlWhere() { // Where
		$sWhere = ($this->_SqlWhere <> "") ? $this->_SqlWhere : "";
		$this->TableFilter = "";
		ew_AddFilter($sWhere, $this->TableFilter);
		return $sWhere;
	}

	function SqlWhere() { // For backward compatibility
    	return $this->getSqlWhere();
	}

	function setSqlWhere($v) {
    	$this->_SqlWhere = $v;
	}
	var $_SqlGroupBy = "";

	function getSqlGroupBy() { // Group By
		return ($this->_SqlGroupBy <> "") ? $this->_SqlGroupBy : "";
	}

	function SqlGroupBy() { // For backward compatibility
    	return $this->getSqlGroupBy();
	}

	function setSqlGroupBy($v) {
    	$this->_SqlGroupBy = $v;
	}
	var $_SqlHaving = "";

	function getSqlHaving() { // Having
		return ($this->_SqlHaving <> "") ? $this->_SqlHaving : "";
	}

	function SqlHaving() { // For backward compatibility
    	return $this->getSqlHaving();
	}

	function setSqlHaving($v) {
    	$this->_SqlHaving = $v;
	}
	var $_SqlOrderBy = "";

	function getSqlOrderBy() { // Order By
		return ($this->_SqlOrderBy <> "") ? $this->_SqlOrderBy : "";
	}

	function SqlOrderBy() { // For backward compatibility
    	return $this->getSqlOrderBy();
	}

	function setSqlOrderBy($v) {
    	$this->_SqlOrderBy = $v;
	}

	// Apply User ID filters
	function ApplyUserIDFilters($sFilter) {
		return $sFilter;
	}

	// Check if User ID security allows view all
	function UserIDAllow($id = "") {
		$allow = EW_USER_ID_ALLOW;
		switch ($id) {
			case "add":
			case "copy":
			case "gridadd":
			case "register":
			case "addopt":
				return (($allow & 1) == 1);
			case "edit":
			case "gridedit":
			case "update":
			case "changepwd":
			case "forgotpwd":
				return (($allow & 4) == 4);
			case "delete":
				return (($allow & 2) == 2);
			case "view":
				return (($allow & 32) == 32);
			case "search":
				return (($allow & 64) == 64);
			default:
				return (($allow & 8) == 8);
		}
	}

	// Get SQL
	function GetSQL($where, $orderby) {
		return ew_BuildSelectSql($this->getSqlSelect(), $this->getSqlWhere(),
			$this->getSqlGroupBy(), $this->getSqlHaving(), $this->getSqlOrderBy(),
			$where, $orderby);
	}

	// Table SQL
	function SQL() {
		$sFilter = $this->CurrentFilter;
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$sSort = $this->getSessionOrderBy();
		return ew_BuildSelectSql($this->getSqlSelect(), $this->getSqlWhere(),
			$this->getSqlGroupBy(), $this->getSqlHaving(), $this->getSqlOrderBy(),
			$sFilter, $sSort);
	}

	// Table SQL with List page filter
	function SelectSQL() {
		$sFilter = $this->getSessionWhere();
		ew_AddFilter($sFilter, $this->CurrentFilter);
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$this->Recordset_Selecting($sFilter);
		$sSort = $this->getSessionOrderBy();
		return ew_BuildSelectSql($this->getSqlSelect(), $this->getSqlWhere(), $this->getSqlGroupBy(),
			$this->getSqlHaving(), $this->getSqlOrderBy(), $sFilter, $sSort);
	}

	// Get ORDER BY clause
	function GetOrderBy() {
		$sSort = $this->getSessionOrderBy();
		return ew_BuildSelectSql("", "", "", "", $this->getSqlOrderBy(), "", $sSort);
	}

	// Try to get record count
	function TryGetRecordCount($sSql) {
		$cnt = -1;
		if (($this->TableType == 'TABLE' || $this->TableType == 'VIEW' || $this->TableType == 'LINKTABLE') && preg_match("/^SELECT \* FROM/i", $sSql)) {
			$sSql = "SELECT COUNT(*) FROM" . preg_replace('/^SELECT\s([\s\S]+)?\*\sFROM/i', "", $sSql);
			$sOrderBy = $this->GetOrderBy();
			if (substr($sSql, strlen($sOrderBy) * -1) == $sOrderBy)
				$sSql = substr($sSql, 0, strlen($sSql) - strlen($sOrderBy)); // Remove ORDER BY clause
		} else {
			$sSql = "SELECT COUNT(*) FROM (" . $sSql . ") EW_COUNT_TABLE";
		}
		$conn = &$this->Connection();
		if ($rs = $conn->Execute($sSql)) {
			if (!$rs->EOF && $rs->FieldCount() > 0) {
				$cnt = $rs->fields[0];
				$rs->Close();
			}
		}
		return intval($cnt);
	}

	// Get record count based on filter (for detail record count in master table pages)
	function LoadRecordCount($sFilter) {
		$origFilter = $this->CurrentFilter;
		$this->CurrentFilter = $sFilter;
		$this->Recordset_Selecting($this->CurrentFilter);

		//$sSql = $this->SQL();
		$sSql = $this->GetSQL($this->CurrentFilter, "");
		$cnt = $this->TryGetRecordCount($sSql);
		if ($cnt == -1) {
			if ($rs = $this->LoadRs($this->CurrentFilter)) {
				$cnt = $rs->RecordCount();
				$rs->Close();
			}
		}
		$this->CurrentFilter = $origFilter;
		return intval($cnt);
	}

	// Get record count (for current List page)
	function SelectRecordCount() {
		$sSql = $this->SelectSQL();
		$cnt = $this->TryGetRecordCount($sSql);
		if ($cnt == -1) {
			$conn = &$this->Connection();
			if ($rs = $conn->Execute($sSql)) {
				$cnt = $rs->RecordCount();
				$rs->Close();
			}
		}
		return intval($cnt);
	}

	// INSERT statement
	function InsertSQL(&$rs) {
		$names = "";
		$values = "";
		foreach ($rs as $name => $value) {
			if (!isset($this->fields[$name]) || $this->fields[$name]->FldIsCustom)
				continue;
			$names .= $this->fields[$name]->FldExpression . ",";
			$values .= ew_QuotedValue($value, $this->fields[$name]->FldDataType, $this->DBID) . ",";
		}
		while (substr($names, -1) == ",")
			$names = substr($names, 0, -1);
		while (substr($values, -1) == ",")
			$values = substr($values, 0, -1);
		return "INSERT INTO " . $this->UpdateTable . " ($names) VALUES ($values)";
	}

	// Insert
	function Insert(&$rs) {
		$conn = &$this->Connection();
		return $conn->Execute($this->InsertSQL($rs));
	}

	// UPDATE statement
	function UpdateSQL(&$rs, $where = "", $curfilter = TRUE) {
		$sql = "UPDATE " . $this->UpdateTable . " SET ";
		foreach ($rs as $name => $value) {
			if (!isset($this->fields[$name]) || $this->fields[$name]->FldIsCustom)
				continue;
			$sql .= $this->fields[$name]->FldExpression . "=";
			$sql .= ew_QuotedValue($value, $this->fields[$name]->FldDataType, $this->DBID) . ",";
		}
		while (substr($sql, -1) == ",")
			$sql = substr($sql, 0, -1);
		$filter = ($curfilter) ? $this->CurrentFilter : "";
		if (is_array($where))
			$where = $this->ArrayToFilter($where);
		ew_AddFilter($filter, $where);
		if ($filter <> "")	$sql .= " WHERE " . $filter;
		return $sql;
	}

	// Update
	function Update(&$rs, $where = "", $rsold = NULL, $curfilter = TRUE) {
		$conn = &$this->Connection();
		return $conn->Execute($this->UpdateSQL($rs, $where, $curfilter));
	}

	// DELETE statement
	function DeleteSQL(&$rs, $where = "", $curfilter = TRUE) {
		$sql = "DELETE FROM " . $this->UpdateTable . " WHERE ";
		if (is_array($where))
			$where = $this->ArrayToFilter($where);
		if ($rs) {
			if (array_key_exists('OcoCodi', $rs))
				ew_AddFilter($where, ew_QuotedName('OcoCodi', $this->DBID) . '=' . ew_QuotedValue($rs['OcoCodi'], $this->OcoCodi->FldDataType, $this->DBID));
		}
		$filter = ($curfilter) ? $this->CurrentFilter : "";
		ew_AddFilter($filter, $where);
		if ($filter <> "")
			$sql .= $filter;
		else
			$sql .= "0=1"; // Avoid delete
		return $sql;
	}

	// Delete
	function Delete(&$rs, $where = "", $curfilter = TRUE) {
		$conn = &$this->Connection();
		return $conn->Execute($this->DeleteSQL($rs, $where, $curfilter));
	}

	// Key filter WHERE clause
	function SqlKeyFilter() {
		return "\"OcoCodi\" = @OcoCodi@";
	}

	// Key filter
	function KeyFilter() {
		$sKeyFilter = $this->SqlKeyFilter();
		if (!is_numeric($this->OcoCodi->CurrentValue))
			$sKeyFilter = "0=1"; // Invalid key
		$sKeyFilter = str_replace("@OcoCodi@", ew_AdjustSql($this->OcoCodi->CurrentValue, $this->DBID), $sKeyFilter); // Replace key value
		return $sKeyFilter;
	}

	// Return page URL
	function getReturnUrl() {
		$name = EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_RETURN_URL;

		// Get referer URL automatically
		if (ew_ServerVar("HTTP_REFERER") <> "" && ew_ReferPage() <> ew_CurrentPage() && ew_ReferPage() <> "login.php") // Referer not same page or login page
			$_SESSION[$name] = ew_ServerVar("HTTP_REFERER"); // Save to Session
		if (@$_SESSION[$name] <> "") {
			return $_SESSION[$name];
		} else {
			return "OComlist.php";
		}
	}

	function setReturnUrl($v) {
		$_SESSION[EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_RETURN_URL] = $v;
	}

	// List URL
	function GetListUrl() {
		return "OComlist.php";
	}

	// View URL
	function GetViewUrl($parm = "") {
		if ($parm <> "")
			$url = $this->KeyUrl("OComview.php", $this->UrlParm($parm));
		else
			$url = $this->KeyUrl("OComview.php", $this->UrlParm(EW_TABLE_SHOW_DETAIL . "="));
		return $this->AddMasterUrl($url);
	}

	// Add URL
	function GetAddUrl($parm = "") {
		if ($parm <> "")
			$url = "OComadd.php?" . $this->UrlParm($parm);
		else
			$url = "OComadd.php";
		return $this->AddMasterUrl($url);
	}

	// Edit URL
	function GetEditUrl($parm = "") {
		$url = $this->KeyUrl("OComedit.php", $this->UrlParm($parm));
		return $this->AddMasterUrl($url);
	}

	// Inline edit URL
	function GetInlineEditUrl() {
		$url = $this->KeyUrl(ew_CurrentPage(), $this->UrlParm("a=edit"));
		return $this->AddMasterUrl($url);
	}

	// Copy URL
	function GetCopyUrl($parm = "") {
		$url = $this->KeyUrl("OComadd.php", $this->UrlParm($parm));
		return $this->AddMasterUrl($url);
	}

	// Inline copy URL
	function GetInlineCopyUrl() {
		$url = $this->KeyUrl(ew_CurrentPage(), $this->UrlParm("a=copy"));
		return $this->AddMasterUrl($url);
	}

	// Delete URL
	function GetDeleteUrl() {
		return $this->KeyUrl("OComdelete.php", $this->UrlParm());
	}

	// Add master url
	function AddMasterUrl($url) {
		return $url;
	}

	function KeyToJson() {
		$json = "";
		$json .= "OcoCodi:" . ew_VarToJson($this->OcoCodi->CurrentValue, "number", "'");
		return "{" . $json . "}";
	}

	// Add key value to URL
	function KeyUrl($url, $parm = "") {
		$sUrl = $url . "?";
		if ($parm <> "") $sUrl .= $parm . "&";
		if (!is_null($this->OcoCodi->CurrentValue)) {
			$sUrl .= "OcoCodi=" . urlencode($this->OcoCodi->CurrentValue);
		} else {
			return "javascript:ew_Alert(ewLanguage.Phrase('InvalidRecord'));";
		}
		return $sUrl;
	}

	// Sort URL
	function SortUrl(&$fld) {
		if ($this->CurrentAction <> "" || $this->Export <> "" ||
			in_array($fld->FldType, array(128, 204, 205))) { // Unsortable data type
				return "";
		} elseif ($fld->Sortable) {
			$sUrlParm = $this->UrlParm("order=" . urlencode($fld->FldName) . "&amp;ordertype=" . $fld->ReverseSort());
			return ew_CurrentPage() . "?" . $sUrlParm;
		} else {
			return "";
		}
	}

	// Get record keys from $_POST/$_GET/$_SESSION
	function GetRecordKeys() {
		global $EW_COMPOSITE_KEY_SEPARATOR;
		$arKeys = array();
		$arKey = array();
		if (isset($_POST["key_m"])) {
			$arKeys = ew_StripSlashes($_POST["key_m"]);
			$cnt = count($arKeys);
		} elseif (isset($_GET["key_m"])) {
			$arKeys = ew_StripSlashes($_GET["key_m"]);
			$cnt = count($arKeys);
		} elseif (!empty($_GET) || !empty($_POST)) {
			$isPost = ew_IsHttpPost();
			$arKeys[] = $isPost ? ew_StripSlashes(@$_POST["OcoCodi"]) : ew_StripSlashes(@$_GET["OcoCodi"]); // OcoCodi

			//return $arKeys; // Do not return yet, so the values will also be checked by the following code
		}

		// Check keys
		$ar = array();
		foreach ($arKeys as $key) {
			if (!is_numeric($key))
				continue;
			$ar[] = $key;
		}
		return $ar;
	}

	// Get key filter
	function GetKeyFilter() {
		$arKeys = $this->GetRecordKeys();
		$sKeyFilter = "";
		foreach ($arKeys as $key) {
			if ($sKeyFilter <> "") $sKeyFilter .= " OR ";
			$this->OcoCodi->CurrentValue = $key;
			$sKeyFilter .= "(" . $this->KeyFilter() . ")";
		}
		return $sKeyFilter;
	}

	// Load rows based on filter
	function &LoadRs($sFilter) {

		// Set up filter (SQL WHERE clause) and get return SQL
		//$this->CurrentFilter = $sFilter;
		//$sSql = $this->SQL();

		$sSql = $this->GetSQL($sFilter, "");
		$conn = &$this->Connection();
		$rs = $conn->Execute($sSql);
		return $rs;
	}

	// Load row values from recordset
	function LoadListRowValues(&$rs) {
		$this->OcoCodi->setDbValue($rs->fields('OcoCodi'));
		$this->PrvCodi->setDbValue($rs->fields('PrvCodi'));
		$this->OcoLPed->Upload->DbValue = $rs->fields('OcoLPed');
		$this->OcoFCom->setDbValue($rs->fields('OcoFCom'));
		$this->OcoProf->Upload->DbValue = $rs->fields('OcoProf');
		$this->OcoEsta->setDbValue($rs->fields('OcoEsta'));
	}

	// Render list row values
	function RenderListRow() {
		global $Security, $gsLanguage, $Language;

		// Call Row Rendering event
		$this->Row_Rendering();

   // Common render codes
		// OcoCodi
		// PrvCodi
		// OcoLPed
		// OcoFCom
		// OcoProf
		// OcoEsta
		// OcoCodi

		$this->OcoCodi->ViewValue = $this->OcoCodi->CurrentValue;
		$this->OcoCodi->ViewCustomAttributes = "";

		// PrvCodi
		if (strval($this->PrvCodi->CurrentValue) <> "") {
			$sFilterWrk = "\"PrvCodi\"" . ew_SearchString("=", $this->PrvCodi->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT \"PrvCodi\", \"PrvRSoc\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"Prov\"";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->PrvCodi, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->PrvCodi->ViewValue = $this->PrvCodi->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->PrvCodi->ViewValue = $this->PrvCodi->CurrentValue;
			}
		} else {
			$this->PrvCodi->ViewValue = NULL;
		}
		$this->PrvCodi->ViewCustomAttributes = "";

		// OcoLPed
		if (!ew_Empty($this->OcoLPed->Upload->DbValue)) {
			$this->OcoLPed->ViewValue = "OCom_OcoLPed_bv.php?" . "OcoCodi=" . $this->OcoCodi->CurrentValue;
			$this->OcoLPed->IsBlobImage = ew_IsImageFile("image" . ew_ContentExt(substr($this->OcoLPed->Upload->DbValue, 0, 11)));
		} else {
			$this->OcoLPed->ViewValue = "";
		}
		$this->OcoLPed->ViewCustomAttributes = "";

		// OcoFCom
		$this->OcoFCom->ViewValue = $this->OcoFCom->CurrentValue;
		$this->OcoFCom->ViewValue = ew_FormatDateTime($this->OcoFCom->ViewValue, 7);
		$this->OcoFCom->ViewCustomAttributes = "";

		// OcoProf
		if (!ew_Empty($this->OcoProf->Upload->DbValue)) {
			$this->OcoProf->ViewValue = "OCom_OcoProf_bv.php?" . "OcoCodi=" . $this->OcoCodi->CurrentValue;
			$this->OcoProf->IsBlobImage = ew_IsImageFile("image" . ew_ContentExt(substr($this->OcoProf->Upload->DbValue, 0, 11)));
		} else {
			$this->OcoProf->ViewValue = "";
		}
		$this->OcoProf->ViewCustomAttributes = "";

		// OcoEsta
		$this->OcoEsta->ViewValue = $this->OcoEsta->CurrentValue;
		$this->OcoEsta->ViewCustomAttributes = "";

		// OcoCodi
		$this->OcoCodi->LinkCustomAttributes = "";
		$this->OcoCodi->HrefValue = "";
		$this->OcoCodi->TooltipValue = "";

		// PrvCodi
		$this->PrvCodi->LinkCustomAttributes = "";
		$this->PrvCodi->HrefValue = "";
		$this->PrvCodi->TooltipValue = "";

		// OcoLPed
		$this->OcoLPed->LinkCustomAttributes = "";
		if (!empty($this->OcoLPed->Upload->DbValue)) {
			$this->OcoLPed->HrefValue = "OCom_OcoLPed_bv.php?OcoCodi=" . $this->OcoCodi->CurrentValue;
			$this->OcoLPed->LinkAttrs["target"] = "_blank";
			if ($this->Export <> "") $this->OcoLPed->HrefValue = ew_ConvertFullUrl($this->OcoLPed->HrefValue);
		} else {
			$this->OcoLPed->HrefValue = "";
		}
		$this->OcoLPed->HrefValue2 = "OCom_OcoLPed_bv.php?OcoCodi=" . $this->OcoCodi->CurrentValue;
		$this->OcoLPed->TooltipValue = "";

		// OcoFCom
		$this->OcoFCom->LinkCustomAttributes = "";
		$this->OcoFCom->HrefValue = "";
		$this->OcoFCom->TooltipValue = "";

		// OcoProf
		$this->OcoProf->LinkCustomAttributes = "";
		if (!empty($this->OcoProf->Upload->DbValue)) {
			$this->OcoProf->HrefValue = "OCom_OcoProf_bv.php?OcoCodi=" . $this->OcoCodi->CurrentValue;
			$this->OcoProf->LinkAttrs["target"] = "_blank";
			if ($this->Export <> "") $this->OcoProf->HrefValue = ew_ConvertFullUrl($this->OcoProf->HrefValue);
		} else {
			$this->OcoProf->HrefValue = "";
		}
		$this->OcoProf->HrefValue2 = "OCom_OcoProf_bv.php?OcoCodi=" . $this->OcoCodi->CurrentValue;
		$this->OcoProf->TooltipValue = "";

		// OcoEsta
		$this->OcoEsta->LinkCustomAttributes = "";
		$this->OcoEsta->HrefValue = "";
		$this->OcoEsta->TooltipValue = "";

		// Call Row Rendered event
		$this->Row_Rendered();
	}

	// Render edit row values
	function RenderEditRow() {
		global $Security, $gsLanguage, $Language;

		// Call Row Rendering event
		$this->Row_Rendering();

		// OcoCodi
		$this->OcoCodi->EditAttrs["class"] = "form-control";
		$this->OcoCodi->EditCustomAttributes = "";
		$this->OcoCodi->EditValue = $this->OcoCodi->CurrentValue;
		$this->OcoCodi->ViewCustomAttributes = "";

		// PrvCodi
		$this->PrvCodi->EditAttrs["class"] = "form-control";
		$this->PrvCodi->EditCustomAttributes = "";

		// OcoLPed
		$this->OcoLPed->EditAttrs["class"] = "form-control";
		$this->OcoLPed->EditCustomAttributes = "";
		if (!ew_Empty($this->OcoLPed->Upload->DbValue)) {
			$this->OcoLPed->EditValue = "OCom_OcoLPed_bv.php?" . "OcoCodi=" . $this->OcoCodi->CurrentValue;
			$this->OcoLPed->IsBlobImage = ew_IsImageFile("image" . ew_ContentExt(substr($this->OcoLPed->Upload->DbValue, 0, 11)));
		} else {
			$this->OcoLPed->EditValue = "";
		}

		// OcoFCom
		$this->OcoFCom->EditAttrs["class"] = "form-control";
		$this->OcoFCom->EditCustomAttributes = "";
		$this->OcoFCom->EditValue = ew_FormatDateTime($this->OcoFCom->CurrentValue, 7);
		$this->OcoFCom->PlaceHolder = ew_RemoveHtml($this->OcoFCom->FldCaption());

		// OcoProf
		$this->OcoProf->EditAttrs["class"] = "form-control";
		$this->OcoProf->EditCustomAttributes = "";
		if (!ew_Empty($this->OcoProf->Upload->DbValue)) {
			$this->OcoProf->EditValue = "OCom_OcoProf_bv.php?" . "OcoCodi=" . $this->OcoCodi->CurrentValue;
			$this->OcoProf->IsBlobImage = ew_IsImageFile("image" . ew_ContentExt(substr($this->OcoProf->Upload->DbValue, 0, 11)));
		} else {
			$this->OcoProf->EditValue = "";
		}

		// OcoEsta
		$this->OcoEsta->EditAttrs["class"] = "form-control";
		$this->OcoEsta->EditCustomAttributes = "";
		$this->OcoEsta->EditValue = $this->OcoEsta->CurrentValue;
		$this->OcoEsta->PlaceHolder = ew_RemoveHtml($this->OcoEsta->FldCaption());

		// Call Row Rendered event
		$this->Row_Rendered();
	}

	// Aggregate list row values
	function AggregateListRowValues() {
	}

	// Aggregate list row (for rendering)
	function AggregateListRow() {

		// Call Row Rendered event
		$this->Row_Rendered();
	}
	var $ExportDoc;

	// Export data in HTML/CSV/Word/Excel/Email/PDF format
	function ExportDocument(&$Doc, &$Recordset, $StartRec, $StopRec, $ExportPageType = "") {
		if (!$Recordset || !$Doc)
			return;
		if (!$Doc->ExportCustom) {

			// Write header
			$Doc->ExportTableHeader();
			if ($Doc->Horizontal) { // Horizontal format, write header
				$Doc->BeginExportRow();
				if ($ExportPageType == "view") {
					if ($this->OcoCodi->Exportable) $Doc->ExportCaption($this->OcoCodi);
					if ($this->PrvCodi->Exportable) $Doc->ExportCaption($this->PrvCodi);
					if ($this->OcoLPed->Exportable) $Doc->ExportCaption($this->OcoLPed);
					if ($this->OcoFCom->Exportable) $Doc->ExportCaption($this->OcoFCom);
					if ($this->OcoProf->Exportable) $Doc->ExportCaption($this->OcoProf);
					if ($this->OcoEsta->Exportable) $Doc->ExportCaption($this->OcoEsta);
				} else {
					if ($this->OcoCodi->Exportable) $Doc->ExportCaption($this->OcoCodi);
					if ($this->PrvCodi->Exportable) $Doc->ExportCaption($this->PrvCodi);
					if ($this->OcoFCom->Exportable) $Doc->ExportCaption($this->OcoFCom);
					if ($this->OcoEsta->Exportable) $Doc->ExportCaption($this->OcoEsta);
				}
				$Doc->EndExportRow();
			}
		}

		// Move to first record
		$RecCnt = $StartRec - 1;
		if (!$Recordset->EOF) {
			$Recordset->MoveFirst();
			if ($StartRec > 1)
				$Recordset->Move($StartRec - 1);
		}
		while (!$Recordset->EOF && $RecCnt < $StopRec) {
			$RecCnt++;
			if (intval($RecCnt) >= intval($StartRec)) {
				$RowCnt = intval($RecCnt) - intval($StartRec) + 1;

				// Page break
				if ($this->ExportPageBreakCount > 0) {
					if ($RowCnt > 1 && ($RowCnt - 1) % $this->ExportPageBreakCount == 0)
						$Doc->ExportPageBreak();
				}
				$this->LoadListRowValues($Recordset);

				// Render row
				$this->RowType = EW_ROWTYPE_VIEW; // Render view
				$this->ResetAttrs();
				$this->RenderListRow();
				if (!$Doc->ExportCustom) {
					$Doc->BeginExportRow($RowCnt); // Allow CSS styles if enabled
					if ($ExportPageType == "view") {
						if ($this->OcoCodi->Exportable) $Doc->ExportField($this->OcoCodi);
						if ($this->PrvCodi->Exportable) $Doc->ExportField($this->PrvCodi);
						if ($this->OcoLPed->Exportable) $Doc->ExportField($this->OcoLPed);
						if ($this->OcoFCom->Exportable) $Doc->ExportField($this->OcoFCom);
						if ($this->OcoProf->Exportable) $Doc->ExportField($this->OcoProf);
						if ($this->OcoEsta->Exportable) $Doc->ExportField($this->OcoEsta);
					} else {
						if ($this->OcoCodi->Exportable) $Doc->ExportField($this->OcoCodi);
						if ($this->PrvCodi->Exportable) $Doc->ExportField($this->PrvCodi);
						if ($this->OcoFCom->Exportable) $Doc->ExportField($this->OcoFCom);
						if ($this->OcoEsta->Exportable) $Doc->ExportField($this->OcoEsta);
					}
					$Doc->EndExportRow();
				}
			}

			// Call Row Export server event
			if ($Doc->ExportCustom)
				$this->Row_Export($Recordset->fields);
			$Recordset->MoveNext();
		}
		if (!$Doc->ExportCustom) {
			$Doc->ExportTableFooter();
		}
	}

	// Get auto fill value
	function GetAutoFill($id, $val) {
		$rsarr = array();
		$rowcnt = 0;

		// Output
		if (is_array($rsarr) && $rowcnt > 0) {
			$fldcnt = count($rsarr[0]);
			for ($i = 0; $i < $rowcnt; $i++) {
				for ($j = 0; $j < $fldcnt; $j++) {
					$str = strval($rsarr[$i][$j]);
					$str = ew_ConvertToUtf8($str);
					if (isset($post["keepCRLF"])) {
						$str = str_replace(array("\r", "\n"), array("\\r", "\\n"), $str);
					} else {
						$str = str_replace(array("\r", "\n"), array(" ", " "), $str);
					}
					$rsarr[$i][$j] = $str;
				}
			}
			return ew_ArrayToJson($rsarr);
		} else {
			return FALSE;
		}
	}

	// Table level events
	// Recordset Selecting event
	function Recordset_Selecting(&$filter) {

		// Enter your code here	
	}

	// Recordset Selected event
	function Recordset_Selected(&$rs) {

		//echo "Recordset Selected";
	}

	// Recordset Search Validated event
	function Recordset_SearchValidated() {

		// Example:
		//$this->MyField1->AdvancedSearch->SearchValue = "your search criteria"; // Search value

	}

	// Recordset Searching event
	function Recordset_Searching(&$filter) {

		// Enter your code here	
	}

	// Row_Selecting event
	function Row_Selecting(&$filter) {

		// Enter your code here	
	}

	// Row Selected event
	function Row_Selected(&$rs) {

		//echo "Row Selected";
	}

	// Row Inserting event
	function Row_Inserting($rsold, &$rsnew) {

		// Enter your code here
		// To cancel, set return value to FALSE

		return TRUE;
	}

	// Row Inserted event
	function Row_Inserted($rsold, &$rsnew) {

		//echo "Row Inserted"
	}

	// Row Updating event
	function Row_Updating($rsold, &$rsnew) {

		// Enter your code here
		// To cancel, set return value to FALSE

		return TRUE;
	}

	// Row Updated event
	function Row_Updated($rsold, &$rsnew) {

		//echo "Row Updated";
	}

	// Row Update Conflict event
	function Row_UpdateConflict($rsold, &$rsnew) {

		// Enter your code here
		// To ignore conflict, set return value to FALSE

		return TRUE;
	}

	// Grid Inserting event
	function Grid_Inserting() {

		// Enter your code here
		// To reject grid insert, set return value to FALSE

		return TRUE;
	}

	// Grid Inserted event
	function Grid_Inserted($rsnew) {

		//echo "Grid Inserted";
	}

	// Grid Updating event
	function Grid_Updating($rsold) {

		// Enter your code here
		// To reject grid update, set return value to FALSE

		return TRUE;
	}

	// Grid Updated event
	function Grid_Updated($rsold, $rsnew) {

		//echo "Grid Updated";
	}

	// Row Deleting event
	function Row_Deleting(&$rs) {

		// Enter your code here
		// To cancel, set return value to False

		return TRUE;
	}

	// Row Deleted event
	function Row_Deleted(&$rs) {

		//echo "Row Deleted";
	}

	// Email Sending event
	function Email_Sending(&$Email, &$Args) {

		//var_dump($Email); var_dump($Args); exit();
		return TRUE;
	}

	// Lookup Selecting event
	function Lookup_Selecting($fld, &$filter) {

		//var_dump($fld->FldName, $fld->LookupFilters, $filter); // Uncomment to view the filter
		// Enter your code here

	}

	// Row Rendering event
	function Row_Rendering() {

		// Enter your code here	
	}

	// Row Rendered event
	function Row_Rendered() {

		// To view properties of field class, use:
		//var_dump($this-><FieldName>); 

	}

	// User ID Filtering event
	function UserID_Filtering(&$filter) {

		// Enter your code here
	}
}
?>
