<?php

// Global variable for table object
$Prod = NULL;

//
// Table class for Prod
//
class cProd extends cTable {
	var $ProdCodi;
	var $CprCodi;
	var $MprCodi;
	var $TprCodi;
	var $ProCant;
	var $ProNMin;
	var $ProPrec;
	var $ProDesc;

	//
	// Table class constructor
	//
	function __construct() {
		global $Language;

		// Language object
		if (!isset($Language)) $Language = new cLanguage();
		$this->TableVar = 'Prod';
		$this->TableName = 'Prod';
		$this->TableType = 'TABLE';

		// Update Table
		$this->UpdateTable = "\"public\".\"Prod\"";
		$this->DBID = 'DB';
		$this->ExportAll = TRUE;
		$this->ExportPageBreakCount = 0; // Page break per every n record (PDF only)
		$this->ExportPageOrientation = "portrait"; // Page orientation (PDF only)
		$this->ExportPageSize = "a4"; // Page size (PDF only)
		$this->ExportExcelPageOrientation = ""; // Page orientation (PHPExcel only)
		$this->ExportExcelPageSize = ""; // Page size (PHPExcel only)
		$this->DetailAdd = TRUE; // Allow detail add
		$this->DetailEdit = TRUE; // Allow detail edit
		$this->DetailView = FALSE; // Allow detail view
		$this->ShowMultipleDetails = FALSE; // Show multiple details
		$this->GridAddRowCount = 5;
		$this->AllowAddDeleteRow = ew_AllowAddDeleteRow(); // Allow add/delete row
		$this->UserIDAllowSecurity = 0; // User ID Allow
		$this->BasicSearch = new cBasicSearch($this->TableVar);

		// ProdCodi
		$this->ProdCodi = new cField('Prod', 'Prod', 'x_ProdCodi', 'ProdCodi', '"ProdCodi"', 'CAST("ProdCodi" AS varchar(255))', 3, -1, FALSE, '"ProdCodi"', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'NO');
		$this->ProdCodi->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['ProdCodi'] = &$this->ProdCodi;

		// CprCodi
		$this->CprCodi = new cField('Prod', 'Prod', 'x_CprCodi', 'CprCodi', '"CprCodi"', 'CAST("CprCodi" AS varchar(255))', 3, -1, FALSE, '"CprCodi"', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'SELECT');
		$this->CprCodi->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['CprCodi'] = &$this->CprCodi;

		// MprCodi
		$this->MprCodi = new cField('Prod', 'Prod', 'x_MprCodi', 'MprCodi', '"MprCodi"', 'CAST("MprCodi" AS varchar(255))', 3, -1, FALSE, '"MprCodi"', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'SELECT');
		$this->MprCodi->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['MprCodi'] = &$this->MprCodi;

		// TprCodi
		$this->TprCodi = new cField('Prod', 'Prod', 'x_TprCodi', 'TprCodi', '"TprCodi"', 'CAST("TprCodi" AS varchar(255))', 3, -1, FALSE, '"TprCodi"', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'SELECT');
		$this->TprCodi->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['TprCodi'] = &$this->TprCodi;

		// ProCant
		$this->ProCant = new cField('Prod', 'Prod', 'x_ProCant', 'ProCant', '"ProCant"', 'CAST("ProCant" AS varchar(255))', 3, -1, FALSE, '"ProCant"', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->ProCant->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['ProCant'] = &$this->ProCant;

		// ProNMin
		$this->ProNMin = new cField('Prod', 'Prod', 'x_ProNMin', 'ProNMin', '"ProNMin"', 'CAST("ProNMin" AS varchar(255))', 3, -1, FALSE, '"ProNMin"', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->ProNMin->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['ProNMin'] = &$this->ProNMin;

		// ProPrec
		$this->ProPrec = new cField('Prod', 'Prod', 'x_ProPrec', 'ProPrec', '"ProPrec"', 'CAST("ProPrec" AS varchar(255))', 5, -1, FALSE, '"ProPrec"', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->ProPrec->FldDefaultErrMsg = $Language->Phrase("IncorrectFloat");
		$this->fields['ProPrec'] = &$this->ProPrec;

		// ProDesc
		$this->ProDesc = new cField('Prod', 'Prod', 'x_ProDesc', 'ProDesc', '"ProDesc"', '"ProDesc"', 200, -1, FALSE, '"ProDesc"', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->fields['ProDesc'] = &$this->ProDesc;
	}

	// Multiple column sort
	function UpdateSort(&$ofld, $ctrl) {
		if ($this->CurrentOrder == $ofld->FldName) {
			$sSortField = $ofld->FldExpression;
			$sLastSort = $ofld->getSort();
			if ($this->CurrentOrderType == "ASC" || $this->CurrentOrderType == "DESC") {
				$sThisSort = $this->CurrentOrderType;
			} else {
				$sThisSort = ($sLastSort == "ASC") ? "DESC" : "ASC";
			}
			$ofld->setSort($sThisSort);
			if ($ctrl) {
				$sOrderBy = $this->getSessionOrderBy();
				if (strpos($sOrderBy, $sSortField . " " . $sLastSort) !== FALSE) {
					$sOrderBy = str_replace($sSortField . " " . $sLastSort, $sSortField . " " . $sThisSort, $sOrderBy);
				} else {
					if ($sOrderBy <> "") $sOrderBy .= ", ";
					$sOrderBy .= $sSortField . " " . $sThisSort;
				}
				$this->setSessionOrderBy($sOrderBy); // Save to Session
			} else {
				$this->setSessionOrderBy($sSortField . " " . $sThisSort); // Save to Session
			}
		} else {
			if (!$ctrl) $ofld->setSort("");
		}
	}

	// Table level SQL
	var $_SqlFrom = "";

	function getSqlFrom() { // From
		return ($this->_SqlFrom <> "") ? $this->_SqlFrom : "\"public\".\"Prod\"";
	}

	function SqlFrom() { // For backward compatibility
    	return $this->getSqlFrom();
	}

	function setSqlFrom($v) {
    	$this->_SqlFrom = $v;
	}
	var $_SqlSelect = "";

	function getSqlSelect() { // Select
		return ($this->_SqlSelect <> "") ? $this->_SqlSelect : "SELECT * FROM " . $this->getSqlFrom();
	}

	function SqlSelect() { // For backward compatibility
    	return $this->getSqlSelect();
	}

	function setSqlSelect($v) {
    	$this->_SqlSelect = $v;
	}
	var $_SqlWhere = "";

	function getSqlWhere() { // Where
		$sWhere = ($this->_SqlWhere <> "") ? $this->_SqlWhere : "";
		$this->TableFilter = "";
		ew_AddFilter($sWhere, $this->TableFilter);
		return $sWhere;
	}

	function SqlWhere() { // For backward compatibility
    	return $this->getSqlWhere();
	}

	function setSqlWhere($v) {
    	$this->_SqlWhere = $v;
	}
	var $_SqlGroupBy = "";

	function getSqlGroupBy() { // Group By
		return ($this->_SqlGroupBy <> "") ? $this->_SqlGroupBy : "";
	}

	function SqlGroupBy() { // For backward compatibility
    	return $this->getSqlGroupBy();
	}

	function setSqlGroupBy($v) {
    	$this->_SqlGroupBy = $v;
	}
	var $_SqlHaving = "";

	function getSqlHaving() { // Having
		return ($this->_SqlHaving <> "") ? $this->_SqlHaving : "";
	}

	function SqlHaving() { // For backward compatibility
    	return $this->getSqlHaving();
	}

	function setSqlHaving($v) {
    	$this->_SqlHaving = $v;
	}
	var $_SqlOrderBy = "";

	function getSqlOrderBy() { // Order By
		return ($this->_SqlOrderBy <> "") ? $this->_SqlOrderBy : "";
	}

	function SqlOrderBy() { // For backward compatibility
    	return $this->getSqlOrderBy();
	}

	function setSqlOrderBy($v) {
    	$this->_SqlOrderBy = $v;
	}

	// Apply User ID filters
	function ApplyUserIDFilters($sFilter) {
		return $sFilter;
	}

	// Check if User ID security allows view all
	function UserIDAllow($id = "") {
		$allow = EW_USER_ID_ALLOW;
		switch ($id) {
			case "add":
			case "copy":
			case "gridadd":
			case "register":
			case "addopt":
				return (($allow & 1) == 1);
			case "edit":
			case "gridedit":
			case "update":
			case "changepwd":
			case "forgotpwd":
				return (($allow & 4) == 4);
			case "delete":
				return (($allow & 2) == 2);
			case "view":
				return (($allow & 32) == 32);
			case "search":
				return (($allow & 64) == 64);
			default:
				return (($allow & 8) == 8);
		}
	}

	// Get SQL
	function GetSQL($where, $orderby) {
		return ew_BuildSelectSql($this->getSqlSelect(), $this->getSqlWhere(),
			$this->getSqlGroupBy(), $this->getSqlHaving(), $this->getSqlOrderBy(),
			$where, $orderby);
	}

	// Table SQL
	function SQL() {
		$sFilter = $this->CurrentFilter;
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$sSort = $this->getSessionOrderBy();
		return ew_BuildSelectSql($this->getSqlSelect(), $this->getSqlWhere(),
			$this->getSqlGroupBy(), $this->getSqlHaving(), $this->getSqlOrderBy(),
			$sFilter, $sSort);
	}

	// Table SQL with List page filter
	function SelectSQL() {
		$sFilter = $this->getSessionWhere();
		ew_AddFilter($sFilter, $this->CurrentFilter);
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$this->Recordset_Selecting($sFilter);
		$sSort = $this->getSessionOrderBy();
		return ew_BuildSelectSql($this->getSqlSelect(), $this->getSqlWhere(), $this->getSqlGroupBy(),
			$this->getSqlHaving(), $this->getSqlOrderBy(), $sFilter, $sSort);
	}

	// Get ORDER BY clause
	function GetOrderBy() {
		$sSort = $this->getSessionOrderBy();
		return ew_BuildSelectSql("", "", "", "", $this->getSqlOrderBy(), "", $sSort);
	}

	// Try to get record count
	function TryGetRecordCount($sSql) {
		$cnt = -1;
		if (($this->TableType == 'TABLE' || $this->TableType == 'VIEW' || $this->TableType == 'LINKTABLE') && preg_match("/^SELECT \* FROM/i", $sSql)) {
			$sSql = "SELECT COUNT(*) FROM" . preg_replace('/^SELECT\s([\s\S]+)?\*\sFROM/i', "", $sSql);
			$sOrderBy = $this->GetOrderBy();
			if (substr($sSql, strlen($sOrderBy) * -1) == $sOrderBy)
				$sSql = substr($sSql, 0, strlen($sSql) - strlen($sOrderBy)); // Remove ORDER BY clause
		} else {
			$sSql = "SELECT COUNT(*) FROM (" . $sSql . ") EW_COUNT_TABLE";
		}
		$conn = &$this->Connection();
		if ($rs = $conn->Execute($sSql)) {
			if (!$rs->EOF && $rs->FieldCount() > 0) {
				$cnt = $rs->fields[0];
				$rs->Close();
			}
		}
		return intval($cnt);
	}

	// Get record count based on filter (for detail record count in master table pages)
	function LoadRecordCount($sFilter) {
		$origFilter = $this->CurrentFilter;
		$this->CurrentFilter = $sFilter;
		$this->Recordset_Selecting($this->CurrentFilter);

		//$sSql = $this->SQL();
		$sSql = $this->GetSQL($this->CurrentFilter, "");
		$cnt = $this->TryGetRecordCount($sSql);
		if ($cnt == -1) {
			if ($rs = $this->LoadRs($this->CurrentFilter)) {
				$cnt = $rs->RecordCount();
				$rs->Close();
			}
		}
		$this->CurrentFilter = $origFilter;
		return intval($cnt);
	}

	// Get record count (for current List page)
	function SelectRecordCount() {
		$sSql = $this->SelectSQL();
		$cnt = $this->TryGetRecordCount($sSql);
		if ($cnt == -1) {
			$conn = &$this->Connection();
			if ($rs = $conn->Execute($sSql)) {
				$cnt = $rs->RecordCount();
				$rs->Close();
			}
		}
		return intval($cnt);
	}

	// INSERT statement
	function InsertSQL(&$rs) {
		$names = "";
		$values = "";
		foreach ($rs as $name => $value) {
			if (!isset($this->fields[$name]) || $this->fields[$name]->FldIsCustom)
				continue;
			$names .= $this->fields[$name]->FldExpression . ",";
			$values .= ew_QuotedValue($value, $this->fields[$name]->FldDataType, $this->DBID) . ",";
		}
		while (substr($names, -1) == ",")
			$names = substr($names, 0, -1);
		while (substr($values, -1) == ",")
			$values = substr($values, 0, -1);
		return "INSERT INTO " . $this->UpdateTable . " ($names) VALUES ($values)";
	}

	// Insert
	function Insert(&$rs) {
		$conn = &$this->Connection();
		return $conn->Execute($this->InsertSQL($rs));
	}

	// UPDATE statement
	function UpdateSQL(&$rs, $where = "", $curfilter = TRUE) {
		$sql = "UPDATE " . $this->UpdateTable . " SET ";
		foreach ($rs as $name => $value) {
			if (!isset($this->fields[$name]) || $this->fields[$name]->FldIsCustom)
				continue;
			$sql .= $this->fields[$name]->FldExpression . "=";
			$sql .= ew_QuotedValue($value, $this->fields[$name]->FldDataType, $this->DBID) . ",";
		}
		while (substr($sql, -1) == ",")
			$sql = substr($sql, 0, -1);
		$filter = ($curfilter) ? $this->CurrentFilter : "";
		if (is_array($where))
			$where = $this->ArrayToFilter($where);
		ew_AddFilter($filter, $where);
		if ($filter <> "")	$sql .= " WHERE " . $filter;
		return $sql;
	}

	// Update
	function Update(&$rs, $where = "", $rsold = NULL, $curfilter = TRUE) {
		$conn = &$this->Connection();
		return $conn->Execute($this->UpdateSQL($rs, $where, $curfilter));
	}

	// DELETE statement
	function DeleteSQL(&$rs, $where = "", $curfilter = TRUE) {
		$sql = "DELETE FROM " . $this->UpdateTable . " WHERE ";
		if (is_array($where))
			$where = $this->ArrayToFilter($where);
		if ($rs) {
			if (array_key_exists('ProdCodi', $rs))
				ew_AddFilter($where, ew_QuotedName('ProdCodi', $this->DBID) . '=' . ew_QuotedValue($rs['ProdCodi'], $this->ProdCodi->FldDataType, $this->DBID));
		}
		$filter = ($curfilter) ? $this->CurrentFilter : "";
		ew_AddFilter($filter, $where);
		if ($filter <> "")
			$sql .= $filter;
		else
			$sql .= "0=1"; // Avoid delete
		return $sql;
	}

	// Delete
	function Delete(&$rs, $where = "", $curfilter = TRUE) {
		$conn = &$this->Connection();
		return $conn->Execute($this->DeleteSQL($rs, $where, $curfilter));
	}

	// Key filter WHERE clause
	function SqlKeyFilter() {
		return "\"ProdCodi\" = @ProdCodi@";
	}

	// Key filter
	function KeyFilter() {
		$sKeyFilter = $this->SqlKeyFilter();
		if (!is_numeric($this->ProdCodi->CurrentValue))
			$sKeyFilter = "0=1"; // Invalid key
		$sKeyFilter = str_replace("@ProdCodi@", ew_AdjustSql($this->ProdCodi->CurrentValue, $this->DBID), $sKeyFilter); // Replace key value
		return $sKeyFilter;
	}

	// Return page URL
	function getReturnUrl() {
		$name = EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_RETURN_URL;

		// Get referer URL automatically
		if (ew_ServerVar("HTTP_REFERER") <> "" && ew_ReferPage() <> ew_CurrentPage() && ew_ReferPage() <> "login.php") // Referer not same page or login page
			$_SESSION[$name] = ew_ServerVar("HTTP_REFERER"); // Save to Session
		if (@$_SESSION[$name] <> "") {
			return $_SESSION[$name];
		} else {
			return "Prodlist.php";
		}
	}

	function setReturnUrl($v) {
		$_SESSION[EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_RETURN_URL] = $v;
	}

	// List URL
	function GetListUrl() {
		return "Prodlist.php";
	}

	// View URL
	function GetViewUrl($parm = "") {
		if ($parm <> "")
			$url = $this->KeyUrl("Prodview.php", $this->UrlParm($parm));
		else
			$url = $this->KeyUrl("Prodview.php", $this->UrlParm(EW_TABLE_SHOW_DETAIL . "="));
		return $this->AddMasterUrl($url);
	}

	// Add URL
	function GetAddUrl($parm = "") {
		if ($parm <> "")
			$url = "Prodadd.php?" . $this->UrlParm($parm);
		else
			$url = "Prodadd.php";
		return $this->AddMasterUrl($url);
	}

	// Edit URL
	function GetEditUrl($parm = "") {
		$url = $this->KeyUrl("Prodedit.php", $this->UrlParm($parm));
		return $this->AddMasterUrl($url);
	}

	// Inline edit URL
	function GetInlineEditUrl() {
		$url = $this->KeyUrl(ew_CurrentPage(), $this->UrlParm("a=edit"));
		return $this->AddMasterUrl($url);
	}

	// Copy URL
	function GetCopyUrl($parm = "") {
		$url = $this->KeyUrl("Prodadd.php", $this->UrlParm($parm));
		return $this->AddMasterUrl($url);
	}

	// Inline copy URL
	function GetInlineCopyUrl() {
		$url = $this->KeyUrl(ew_CurrentPage(), $this->UrlParm("a=copy"));
		return $this->AddMasterUrl($url);
	}

	// Delete URL
	function GetDeleteUrl() {
		return $this->KeyUrl("Proddelete.php", $this->UrlParm());
	}

	// Add master url
	function AddMasterUrl($url) {
		return $url;
	}

	function KeyToJson() {
		$json = "";
		$json .= "ProdCodi:" . ew_VarToJson($this->ProdCodi->CurrentValue, "number", "'");
		return "{" . $json . "}";
	}

	// Add key value to URL
	function KeyUrl($url, $parm = "") {
		$sUrl = $url . "?";
		if ($parm <> "") $sUrl .= $parm . "&";
		if (!is_null($this->ProdCodi->CurrentValue)) {
			$sUrl .= "ProdCodi=" . urlencode($this->ProdCodi->CurrentValue);
		} else {
			return "javascript:ew_Alert(ewLanguage.Phrase('InvalidRecord'));";
		}
		return $sUrl;
	}

	// Sort URL
	function SortUrl(&$fld) {
		if ($this->CurrentAction <> "" || $this->Export <> "" ||
			in_array($fld->FldType, array(128, 204, 205))) { // Unsortable data type
				return "";
		} elseif ($fld->Sortable) {
			$sUrlParm = $this->UrlParm("order=" . urlencode($fld->FldName) . "&amp;ordertype=" . $fld->ReverseSort());
			return ew_CurrentPage() . "?" . $sUrlParm;
		} else {
			return "";
		}
	}

	// Get record keys from $_POST/$_GET/$_SESSION
	function GetRecordKeys() {
		global $EW_COMPOSITE_KEY_SEPARATOR;
		$arKeys = array();
		$arKey = array();
		if (isset($_POST["key_m"])) {
			$arKeys = ew_StripSlashes($_POST["key_m"]);
			$cnt = count($arKeys);
		} elseif (isset($_GET["key_m"])) {
			$arKeys = ew_StripSlashes($_GET["key_m"]);
			$cnt = count($arKeys);
		} elseif (!empty($_GET) || !empty($_POST)) {
			$isPost = ew_IsHttpPost();
			$arKeys[] = $isPost ? ew_StripSlashes(@$_POST["ProdCodi"]) : ew_StripSlashes(@$_GET["ProdCodi"]); // ProdCodi

			//return $arKeys; // Do not return yet, so the values will also be checked by the following code
		}

		// Check keys
		$ar = array();
		foreach ($arKeys as $key) {
			if (!is_numeric($key))
				continue;
			$ar[] = $key;
		}
		return $ar;
	}

	// Get key filter
	function GetKeyFilter() {
		$arKeys = $this->GetRecordKeys();
		$sKeyFilter = "";
		foreach ($arKeys as $key) {
			if ($sKeyFilter <> "") $sKeyFilter .= " OR ";
			$this->ProdCodi->CurrentValue = $key;
			$sKeyFilter .= "(" . $this->KeyFilter() . ")";
		}
		return $sKeyFilter;
	}

	// Load rows based on filter
	function &LoadRs($sFilter) {

		// Set up filter (SQL WHERE clause) and get return SQL
		//$this->CurrentFilter = $sFilter;
		//$sSql = $this->SQL();

		$sSql = $this->GetSQL($sFilter, "");
		$conn = &$this->Connection();
		$rs = $conn->Execute($sSql);
		return $rs;
	}

	// Load row values from recordset
	function LoadListRowValues(&$rs) {
		$this->ProdCodi->setDbValue($rs->fields('ProdCodi'));
		$this->CprCodi->setDbValue($rs->fields('CprCodi'));
		$this->MprCodi->setDbValue($rs->fields('MprCodi'));
		$this->TprCodi->setDbValue($rs->fields('TprCodi'));
		$this->ProCant->setDbValue($rs->fields('ProCant'));
		$this->ProNMin->setDbValue($rs->fields('ProNMin'));
		$this->ProPrec->setDbValue($rs->fields('ProPrec'));
		$this->ProDesc->setDbValue($rs->fields('ProDesc'));
	}

	// Render list row values
	function RenderListRow() {
		global $Security, $gsLanguage, $Language;

		// Call Row Rendering event
		$this->Row_Rendering();

   // Common render codes
		// ProdCodi
		// CprCodi
		// MprCodi
		// TprCodi
		// ProCant
		// ProNMin
		// ProPrec
		// ProDesc
		// ProdCodi

		$this->ProdCodi->ViewValue = $this->ProdCodi->CurrentValue;
		$this->ProdCodi->ViewCustomAttributes = "";

		// CprCodi
		if (strval($this->CprCodi->CurrentValue) <> "") {
			$sFilterWrk = "\"CprCodi\"" . ew_SearchString("=", $this->CprCodi->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT \"CprCodi\", \"CprNomb\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"CPro\"";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->CprCodi, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->CprCodi->ViewValue = $this->CprCodi->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->CprCodi->ViewValue = $this->CprCodi->CurrentValue;
			}
		} else {
			$this->CprCodi->ViewValue = NULL;
		}
		$this->CprCodi->ViewCustomAttributes = "";

		// MprCodi
		if (strval($this->MprCodi->CurrentValue) <> "") {
			$sFilterWrk = "\"MprCodi\"" . ew_SearchString("=", $this->MprCodi->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT \"MprCodi\", \"MprNomb\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"Mpro\"";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->MprCodi, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->MprCodi->ViewValue = $this->MprCodi->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->MprCodi->ViewValue = $this->MprCodi->CurrentValue;
			}
		} else {
			$this->MprCodi->ViewValue = NULL;
		}
		$this->MprCodi->ViewCustomAttributes = "";

		// TprCodi
		if (strval($this->TprCodi->CurrentValue) <> "") {
			$sFilterWrk = "\"TprCodi\"" . ew_SearchString("=", $this->TprCodi->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT \"TprCodi\", \"TprNomb\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"TProd\"";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->TprCodi, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->TprCodi->ViewValue = $this->TprCodi->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->TprCodi->ViewValue = $this->TprCodi->CurrentValue;
			}
		} else {
			$this->TprCodi->ViewValue = NULL;
		}
		$this->TprCodi->ViewCustomAttributes = "";

		// ProCant
		$this->ProCant->ViewValue = $this->ProCant->CurrentValue;
		$this->ProCant->ViewCustomAttributes = "";

		// ProNMin
		$this->ProNMin->ViewValue = $this->ProNMin->CurrentValue;
		$this->ProNMin->ViewCustomAttributes = "";

		// ProPrec
		$this->ProPrec->ViewValue = $this->ProPrec->CurrentValue;
		$this->ProPrec->ViewCustomAttributes = "";

		// ProDesc
		$this->ProDesc->ViewValue = $this->ProDesc->CurrentValue;
		$this->ProDesc->ViewCustomAttributes = "";

		// ProdCodi
		$this->ProdCodi->LinkCustomAttributes = "";
		$this->ProdCodi->HrefValue = "";
		$this->ProdCodi->TooltipValue = "";

		// CprCodi
		$this->CprCodi->LinkCustomAttributes = "";
		$this->CprCodi->HrefValue = "";
		$this->CprCodi->TooltipValue = "";

		// MprCodi
		$this->MprCodi->LinkCustomAttributes = "";
		$this->MprCodi->HrefValue = "";
		$this->MprCodi->TooltipValue = "";

		// TprCodi
		$this->TprCodi->LinkCustomAttributes = "";
		$this->TprCodi->HrefValue = "";
		$this->TprCodi->TooltipValue = "";

		// ProCant
		$this->ProCant->LinkCustomAttributes = "";
		$this->ProCant->HrefValue = "";
		$this->ProCant->TooltipValue = "";

		// ProNMin
		$this->ProNMin->LinkCustomAttributes = "";
		$this->ProNMin->HrefValue = "";
		$this->ProNMin->TooltipValue = "";

		// ProPrec
		$this->ProPrec->LinkCustomAttributes = "";
		$this->ProPrec->HrefValue = "";
		$this->ProPrec->TooltipValue = "";

		// ProDesc
		$this->ProDesc->LinkCustomAttributes = "";
		$this->ProDesc->HrefValue = "";
		$this->ProDesc->TooltipValue = "";

		// Call Row Rendered event
		$this->Row_Rendered();
	}

	// Render edit row values
	function RenderEditRow() {
		global $Security, $gsLanguage, $Language;

		// Call Row Rendering event
		$this->Row_Rendering();

		// ProdCodi
		$this->ProdCodi->EditAttrs["class"] = "form-control";
		$this->ProdCodi->EditCustomAttributes = "";
		$this->ProdCodi->EditValue = $this->ProdCodi->CurrentValue;
		$this->ProdCodi->ViewCustomAttributes = "";

		// CprCodi
		$this->CprCodi->EditAttrs["class"] = "form-control";
		$this->CprCodi->EditCustomAttributes = "";

		// MprCodi
		$this->MprCodi->EditAttrs["class"] = "form-control";
		$this->MprCodi->EditCustomAttributes = "";

		// TprCodi
		$this->TprCodi->EditAttrs["class"] = "form-control";
		$this->TprCodi->EditCustomAttributes = "";

		// ProCant
		$this->ProCant->EditAttrs["class"] = "form-control";
		$this->ProCant->EditCustomAttributes = "";
		$this->ProCant->EditValue = $this->ProCant->CurrentValue;
		$this->ProCant->PlaceHolder = ew_RemoveHtml($this->ProCant->FldCaption());

		// ProNMin
		$this->ProNMin->EditAttrs["class"] = "form-control";
		$this->ProNMin->EditCustomAttributes = "";
		$this->ProNMin->EditValue = $this->ProNMin->CurrentValue;
		$this->ProNMin->PlaceHolder = ew_RemoveHtml($this->ProNMin->FldCaption());

		// ProPrec
		$this->ProPrec->EditAttrs["class"] = "form-control";
		$this->ProPrec->EditCustomAttributes = "";
		$this->ProPrec->EditValue = $this->ProPrec->CurrentValue;
		$this->ProPrec->PlaceHolder = ew_RemoveHtml($this->ProPrec->FldCaption());
		if (strval($this->ProPrec->EditValue) <> "" && is_numeric($this->ProPrec->EditValue)) $this->ProPrec->EditValue = ew_FormatNumber($this->ProPrec->EditValue, -2, -1, -2, 0);

		// ProDesc
		$this->ProDesc->EditAttrs["class"] = "form-control";
		$this->ProDesc->EditCustomAttributes = "";
		$this->ProDesc->EditValue = $this->ProDesc->CurrentValue;
		$this->ProDesc->PlaceHolder = ew_RemoveHtml($this->ProDesc->FldCaption());

		// Call Row Rendered event
		$this->Row_Rendered();
	}

	// Aggregate list row values
	function AggregateListRowValues() {
	}

	// Aggregate list row (for rendering)
	function AggregateListRow() {

		// Call Row Rendered event
		$this->Row_Rendered();
	}
	var $ExportDoc;

	// Export data in HTML/CSV/Word/Excel/Email/PDF format
	function ExportDocument(&$Doc, &$Recordset, $StartRec, $StopRec, $ExportPageType = "") {
		if (!$Recordset || !$Doc)
			return;
		if (!$Doc->ExportCustom) {

			// Write header
			$Doc->ExportTableHeader();
			if ($Doc->Horizontal) { // Horizontal format, write header
				$Doc->BeginExportRow();
				if ($ExportPageType == "view") {
					if ($this->CprCodi->Exportable) $Doc->ExportCaption($this->CprCodi);
					if ($this->MprCodi->Exportable) $Doc->ExportCaption($this->MprCodi);
					if ($this->TprCodi->Exportable) $Doc->ExportCaption($this->TprCodi);
					if ($this->ProCant->Exportable) $Doc->ExportCaption($this->ProCant);
					if ($this->ProNMin->Exportable) $Doc->ExportCaption($this->ProNMin);
					if ($this->ProPrec->Exportable) $Doc->ExportCaption($this->ProPrec);
					if ($this->ProDesc->Exportable) $Doc->ExportCaption($this->ProDesc);
				} else {
					if ($this->ProdCodi->Exportable) $Doc->ExportCaption($this->ProdCodi);
					if ($this->CprCodi->Exportable) $Doc->ExportCaption($this->CprCodi);
					if ($this->MprCodi->Exportable) $Doc->ExportCaption($this->MprCodi);
					if ($this->TprCodi->Exportable) $Doc->ExportCaption($this->TprCodi);
					if ($this->ProCant->Exportable) $Doc->ExportCaption($this->ProCant);
					if ($this->ProNMin->Exportable) $Doc->ExportCaption($this->ProNMin);
					if ($this->ProPrec->Exportable) $Doc->ExportCaption($this->ProPrec);
					if ($this->ProDesc->Exportable) $Doc->ExportCaption($this->ProDesc);
				}
				$Doc->EndExportRow();
			}
		}

		// Move to first record
		$RecCnt = $StartRec - 1;
		if (!$Recordset->EOF) {
			$Recordset->MoveFirst();
			if ($StartRec > 1)
				$Recordset->Move($StartRec - 1);
		}
		while (!$Recordset->EOF && $RecCnt < $StopRec) {
			$RecCnt++;
			if (intval($RecCnt) >= intval($StartRec)) {
				$RowCnt = intval($RecCnt) - intval($StartRec) + 1;

				// Page break
				if ($this->ExportPageBreakCount > 0) {
					if ($RowCnt > 1 && ($RowCnt - 1) % $this->ExportPageBreakCount == 0)
						$Doc->ExportPageBreak();
				}
				$this->LoadListRowValues($Recordset);

				// Render row
				$this->RowType = EW_ROWTYPE_VIEW; // Render view
				$this->ResetAttrs();
				$this->RenderListRow();
				if (!$Doc->ExportCustom) {
					$Doc->BeginExportRow($RowCnt); // Allow CSS styles if enabled
					if ($ExportPageType == "view") {
						if ($this->CprCodi->Exportable) $Doc->ExportField($this->CprCodi);
						if ($this->MprCodi->Exportable) $Doc->ExportField($this->MprCodi);
						if ($this->TprCodi->Exportable) $Doc->ExportField($this->TprCodi);
						if ($this->ProCant->Exportable) $Doc->ExportField($this->ProCant);
						if ($this->ProNMin->Exportable) $Doc->ExportField($this->ProNMin);
						if ($this->ProPrec->Exportable) $Doc->ExportField($this->ProPrec);
						if ($this->ProDesc->Exportable) $Doc->ExportField($this->ProDesc);
					} else {
						if ($this->ProdCodi->Exportable) $Doc->ExportField($this->ProdCodi);
						if ($this->CprCodi->Exportable) $Doc->ExportField($this->CprCodi);
						if ($this->MprCodi->Exportable) $Doc->ExportField($this->MprCodi);
						if ($this->TprCodi->Exportable) $Doc->ExportField($this->TprCodi);
						if ($this->ProCant->Exportable) $Doc->ExportField($this->ProCant);
						if ($this->ProNMin->Exportable) $Doc->ExportField($this->ProNMin);
						if ($this->ProPrec->Exportable) $Doc->ExportField($this->ProPrec);
						if ($this->ProDesc->Exportable) $Doc->ExportField($this->ProDesc);
					}
					$Doc->EndExportRow();
				}
			}

			// Call Row Export server event
			if ($Doc->ExportCustom)
				$this->Row_Export($Recordset->fields);
			$Recordset->MoveNext();
		}
		if (!$Doc->ExportCustom) {
			$Doc->ExportTableFooter();
		}
	}

	// Get auto fill value
	function GetAutoFill($id, $val) {
		$rsarr = array();
		$rowcnt = 0;

		// Output
		if (is_array($rsarr) && $rowcnt > 0) {
			$fldcnt = count($rsarr[0]);
			for ($i = 0; $i < $rowcnt; $i++) {
				for ($j = 0; $j < $fldcnt; $j++) {
					$str = strval($rsarr[$i][$j]);
					$str = ew_ConvertToUtf8($str);
					if (isset($post["keepCRLF"])) {
						$str = str_replace(array("\r", "\n"), array("\\r", "\\n"), $str);
					} else {
						$str = str_replace(array("\r", "\n"), array(" ", " "), $str);
					}
					$rsarr[$i][$j] = $str;
				}
			}
			return ew_ArrayToJson($rsarr);
		} else {
			return FALSE;
		}
	}

	// Table level events
	// Recordset Selecting event
	function Recordset_Selecting(&$filter) {

		// Enter your code here	
	}

	// Recordset Selected event
	function Recordset_Selected(&$rs) {

		//echo "Recordset Selected";
	}

	// Recordset Search Validated event
	function Recordset_SearchValidated() {

		// Example:
		//$this->MyField1->AdvancedSearch->SearchValue = "your search criteria"; // Search value

	}

	// Recordset Searching event
	function Recordset_Searching(&$filter) {

		// Enter your code here	
	}

	// Row_Selecting event
	function Row_Selecting(&$filter) {

		// Enter your code here	
	}

	// Row Selected event
	function Row_Selected(&$rs) {

		//echo "Row Selected";
	}

	// Row Inserting event
	function Row_Inserting($rsold, &$rsnew) {

		// Enter your code here
		// To cancel, set return value to FALSE

		return TRUE;
	}

	// Row Inserted event
	function Row_Inserted($rsold, &$rsnew) {

		//echo "Row Inserted"
	}

	// Row Updating event
	function Row_Updating($rsold, &$rsnew) {

		// Enter your code here
		// To cancel, set return value to FALSE

		return TRUE;
	}

	// Row Updated event
	function Row_Updated($rsold, &$rsnew) {

		//echo "Row Updated";
	}

	// Row Update Conflict event
	function Row_UpdateConflict($rsold, &$rsnew) {

		// Enter your code here
		// To ignore conflict, set return value to FALSE

		return TRUE;
	}

	// Grid Inserting event
	function Grid_Inserting() {

		// Enter your code here
		// To reject grid insert, set return value to FALSE

		return TRUE;
	}

	// Grid Inserted event
	function Grid_Inserted($rsnew) {

		//echo "Grid Inserted";
	}

	// Grid Updating event
	function Grid_Updating($rsold) {

		// Enter your code here
		// To reject grid update, set return value to FALSE

		return TRUE;
	}

	// Grid Updated event
	function Grid_Updated($rsold, $rsnew) {

		//echo "Grid Updated";
	}

	// Row Deleting event
	function Row_Deleting(&$rs) {

		// Enter your code here
		// To cancel, set return value to False

		return TRUE;
	}

	// Row Deleted event
	function Row_Deleted(&$rs) {

		//echo "Row Deleted";
	}

	// Email Sending event
	function Email_Sending(&$Email, &$Args) {

		//var_dump($Email); var_dump($Args); exit();
		return TRUE;
	}

	// Lookup Selecting event
	function Lookup_Selecting($fld, &$filter) {

		//var_dump($fld->FldName, $fld->LookupFilters, $filter); // Uncomment to view the filter
		// Enter your code here

	}

	// Row Rendering event
	function Row_Rendering() {

		// Enter your code here	
	}

	// Row Rendered event
	function Row_Rendered() {

		// To view properties of field class, use:
		//var_dump($this-><FieldName>); 

	}

	// User ID Filtering event
	function UserID_Filtering(&$filter) {

		// Enter your code here
	}
}
?>
