<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "FDetinfo.php" ?>
<?php include_once "Usuainfo.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$FDet_edit = NULL; // Initialize page object first

class cFDet_edit extends cFDet {

	// Page ID
	var $PageID = 'edit';

	// Project ID
	var $ProjectID = "{04439FF7-B43F-460F-8514-F71C8FF9E679}";

	// Table name
	var $TableName = 'FDet';

	// Page object name
	var $PageObjName = 'FDet_edit';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (FDet)
		if (!isset($GLOBALS["FDet"]) || get_class($GLOBALS["FDet"]) == "cFDet") {
			$GLOBALS["FDet"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["FDet"];
		}

		// Table object (Usua)
		if (!isset($GLOBALS['Usua'])) $GLOBALS['Usua'] = new cUsua();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'edit', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'FDet', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (Usua)
		if (!isset($UserTable)) {
			$UserTable = new cUsua();
			$UserTableConn = Conn($UserTable->DBID);
		}
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanEdit()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage($Language->Phrase("NoPermission")); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("FDetlist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}

		// Create form object
		$objForm = new cFormObj();
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Process auto fill
		if (@$_POST["ajax"] == "autofill") {
			$results = $this->GetAutoFill(@$_POST["name"], @$_POST["q"]);
			if ($results) {

				// Clean output buffer
				if (!EW_DEBUG_ENABLED && ob_get_length())
					ob_end_clean();
				echo $results;
				$this->Page_Terminate();
				exit();
			}
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $FDet;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($FDet);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $FormClassName = "form-horizontal ewForm ewEditForm";
	var $DbMasterFilter;
	var $DbDetailFilter;

	// 
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError;

		// Load key from QueryString
		if (@$_GET["FdeCodi"] <> "") {
			$this->FdeCodi->setQueryStringValue($_GET["FdeCodi"]);
		}
		if (@$_GET["FcaCodi"] <> "") {
			$this->FcaCodi->setQueryStringValue($_GET["FcaCodi"]);
		}

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Process form if post back
		if (@$_POST["a_edit"] <> "") {
			$this->CurrentAction = $_POST["a_edit"]; // Get action code
			$this->LoadFormValues(); // Get form values
		} else {
			$this->CurrentAction = "I"; // Default action is display
		}

		// Check if valid key
		if ($this->FdeCodi->CurrentValue == "")
			$this->Page_Terminate("FDetlist.php"); // Invalid key, return to list
		if ($this->FcaCodi->CurrentValue == "")
			$this->Page_Terminate("FDetlist.php"); // Invalid key, return to list

		// Validate form if post back
		if (@$_POST["a_edit"] <> "") {
			if (!$this->ValidateForm()) {
				$this->CurrentAction = ""; // Form error, reset action
				$this->setFailureMessage($gsFormError);
				$this->EventCancelled = TRUE; // Event cancelled
				$this->RestoreFormValues();
			}
		}
		switch ($this->CurrentAction) {
			case "I": // Get a record to display
				if (!$this->LoadRow()) { // Load record based on key
					if ($this->getFailureMessage() == "") $this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
					$this->Page_Terminate("FDetlist.php"); // No matching record, return to list
				}
				break;
			Case "U": // Update
				$sReturnUrl = $this->getReturnUrl();
				$this->SendEmail = TRUE; // Send email on update success
				if ($this->EditRow()) { // Update record based on key
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage($Language->Phrase("UpdateSuccess")); // Update success
					$this->Page_Terminate($sReturnUrl); // Return to caller
				} elseif ($this->getFailureMessage() == $Language->Phrase("NoRecord")) {
					$this->Page_Terminate($sReturnUrl); // Return to caller
				} else {
					$this->EventCancelled = TRUE; // Event cancelled
					$this->RestoreFormValues(); // Restore form values if update failed
				}
		}

		// Render the record
		if ($this->CurrentAction == "F") { // Confirm page
			$this->RowType = EW_ROWTYPE_VIEW; // Render as View
		} else {
			$this->RowType = EW_ROWTYPE_EDIT; // Render as Edit
		}
		$this->ResetAttrs();
		$this->RenderRow();
	}

	// Set up starting record parameters
	function SetUpStartRec() {
		if ($this->DisplayRecs == 0)
			return;
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET[EW_TABLE_START_REC] <> "") { // Check for "start" parameter
				$this->StartRec = $_GET[EW_TABLE_START_REC];
				$this->setStartRecordNumber($this->StartRec);
			} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
				$PageNo = $_GET[EW_TABLE_PAGE_NO];
				if (is_numeric($PageNo)) {
					$this->StartRec = ($PageNo-1)*$this->DisplayRecs+1;
					if ($this->StartRec <= 0) {
						$this->StartRec = 1;
					} elseif ($this->StartRec >= intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1) {
						$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1;
					}
					$this->setStartRecordNumber($this->StartRec);
				}
			}
		}
		$this->StartRec = $this->getStartRecordNumber();

		// Check if correct start record counter
		if (!is_numeric($this->StartRec) || $this->StartRec == "") { // Avoid invalid start record counter
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} elseif (intval($this->StartRec) > intval($this->TotalRecs)) { // Avoid starting record > total records
			$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to last page first record
			$this->setStartRecordNumber($this->StartRec);
		} elseif (($this->StartRec-1) % $this->DisplayRecs <> 0) {
			$this->StartRec = intval(($this->StartRec-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to page boundary
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Get upload files
	function GetUploadFiles() {
		global $objForm, $Language;

		// Get upload data
	}

	// Load form values
	function LoadFormValues() {

		// Load from form
		global $objForm;
		if (!$this->FdeCodi->FldIsDetailKey) {
			$this->FdeCodi->setFormValue($objForm->GetValue("x_FdeCodi"));
		}
		if (!$this->FcaCodi->FldIsDetailKey) {
			$this->FcaCodi->setFormValue($objForm->GetValue("x_FcaCodi"));
		}
		if (!$this->FdeCant->FldIsDetailKey) {
			$this->FdeCant->setFormValue($objForm->GetValue("x_FdeCant"));
		}
		if (!$this->FdePrec->FldIsDetailKey) {
			$this->FdePrec->setFormValue($objForm->GetValue("x_FdePrec"));
		}
		if (!$this->FdeDesc->FldIsDetailKey) {
			$this->FdeDesc->setFormValue($objForm->GetValue("x_FdeDesc"));
		}
		if (!$this->FdeUsua->FldIsDetailKey) {
			$this->FdeUsua->setFormValue($objForm->GetValue("x_FdeUsua"));
		}
		if (!$this->FdeFCre->FldIsDetailKey) {
			$this->FdeFCre->setFormValue($objForm->GetValue("x_FdeFCre"));
			$this->FdeFCre->CurrentValue = ew_UnFormatDateTime($this->FdeFCre->CurrentValue, 7);
		}
	}

	// Restore form values
	function RestoreFormValues() {
		global $objForm;
		$this->LoadRow();
		$this->FdeCodi->CurrentValue = $this->FdeCodi->FormValue;
		$this->FcaCodi->CurrentValue = $this->FcaCodi->FormValue;
		$this->FdeCant->CurrentValue = $this->FdeCant->FormValue;
		$this->FdePrec->CurrentValue = $this->FdePrec->FormValue;
		$this->FdeDesc->CurrentValue = $this->FdeDesc->FormValue;
		$this->FdeUsua->CurrentValue = $this->FdeUsua->FormValue;
		$this->FdeFCre->CurrentValue = $this->FdeFCre->FormValue;
		$this->FdeFCre->CurrentValue = ew_UnFormatDateTime($this->FdeFCre->CurrentValue, 7);
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->FdeCodi->setDbValue($rs->fields('FdeCodi'));
		$this->FcaCodi->setDbValue($rs->fields('FcaCodi'));
		$this->FdeCant->setDbValue($rs->fields('FdeCant'));
		$this->FdePrec->setDbValue($rs->fields('FdePrec'));
		$this->FdeDesc->setDbValue($rs->fields('FdeDesc'));
		$this->FdeUsua->setDbValue($rs->fields('FdeUsua'));
		$this->FdeFCre->setDbValue($rs->fields('FdeFCre'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->FdeCodi->DbValue = $row['FdeCodi'];
		$this->FcaCodi->DbValue = $row['FcaCodi'];
		$this->FdeCant->DbValue = $row['FdeCant'];
		$this->FdePrec->DbValue = $row['FdePrec'];
		$this->FdeDesc->DbValue = $row['FdeDesc'];
		$this->FdeUsua->DbValue = $row['FdeUsua'];
		$this->FdeFCre->DbValue = $row['FdeFCre'];
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		// Convert decimal values if posted back

		if ($this->FdePrec->FormValue == $this->FdePrec->CurrentValue && is_numeric(ew_StrToFloat($this->FdePrec->CurrentValue)))
			$this->FdePrec->CurrentValue = ew_StrToFloat($this->FdePrec->CurrentValue);

		// Call Row_Rendering event
		$this->Row_Rendering();

		// Common render codes for all row types
		// FdeCodi
		// FcaCodi
		// FdeCant
		// FdePrec
		// FdeDesc
		// FdeUsua
		// FdeFCre

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// FdeCodi
		$this->FdeCodi->ViewValue = $this->FdeCodi->CurrentValue;
		$this->FdeCodi->ViewCustomAttributes = "";

		// FcaCodi
		if (strval($this->FcaCodi->CurrentValue) <> "") {
			$sFilterWrk = "\"FcaCodi\"" . ew_SearchString("=", $this->FcaCodi->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT \"FcaCodi\", \"FcaCodi\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"FCab\"";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->FcaCodi, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->FcaCodi->ViewValue = $this->FcaCodi->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->FcaCodi->ViewValue = $this->FcaCodi->CurrentValue;
			}
		} else {
			$this->FcaCodi->ViewValue = NULL;
		}
		$this->FcaCodi->ViewCustomAttributes = "";

		// FdeCant
		$this->FdeCant->ViewValue = $this->FdeCant->CurrentValue;
		$this->FdeCant->ViewCustomAttributes = "";

		// FdePrec
		$this->FdePrec->ViewValue = $this->FdePrec->CurrentValue;
		$this->FdePrec->ViewCustomAttributes = "";

		// FdeDesc
		$this->FdeDesc->ViewValue = $this->FdeDesc->CurrentValue;
		$this->FdeDesc->ViewCustomAttributes = "";

		// FdeUsua
		$this->FdeUsua->ViewValue = $this->FdeUsua->CurrentValue;
		$this->FdeUsua->ViewCustomAttributes = "";

		// FdeFCre
		$this->FdeFCre->ViewValue = $this->FdeFCre->CurrentValue;
		$this->FdeFCre->ViewValue = ew_FormatDateTime($this->FdeFCre->ViewValue, 7);
		$this->FdeFCre->ViewCustomAttributes = "";

			// FdeCodi
			$this->FdeCodi->LinkCustomAttributes = "";
			$this->FdeCodi->HrefValue = "";
			$this->FdeCodi->TooltipValue = "";

			// FcaCodi
			$this->FcaCodi->LinkCustomAttributes = "";
			$this->FcaCodi->HrefValue = "";
			$this->FcaCodi->TooltipValue = "";

			// FdeCant
			$this->FdeCant->LinkCustomAttributes = "";
			$this->FdeCant->HrefValue = "";
			$this->FdeCant->TooltipValue = "";

			// FdePrec
			$this->FdePrec->LinkCustomAttributes = "";
			$this->FdePrec->HrefValue = "";
			$this->FdePrec->TooltipValue = "";

			// FdeDesc
			$this->FdeDesc->LinkCustomAttributes = "";
			$this->FdeDesc->HrefValue = "";
			$this->FdeDesc->TooltipValue = "";

			// FdeUsua
			$this->FdeUsua->LinkCustomAttributes = "";
			$this->FdeUsua->HrefValue = "";
			$this->FdeUsua->TooltipValue = "";

			// FdeFCre
			$this->FdeFCre->LinkCustomAttributes = "";
			$this->FdeFCre->HrefValue = "";
			$this->FdeFCre->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_EDIT) { // Edit row

			// FdeCodi
			$this->FdeCodi->EditAttrs["class"] = "form-control";
			$this->FdeCodi->EditCustomAttributes = "";
			$this->FdeCodi->EditValue = $this->FdeCodi->CurrentValue;
			$this->FdeCodi->ViewCustomAttributes = "";

			// FcaCodi
			$this->FcaCodi->EditAttrs["class"] = "form-control";
			$this->FcaCodi->EditCustomAttributes = "";
			if (strval($this->FcaCodi->CurrentValue) <> "") {
				$sFilterWrk = "\"FcaCodi\"" . ew_SearchString("=", $this->FcaCodi->CurrentValue, EW_DATATYPE_NUMBER, "");
			$sSqlWrk = "SELECT \"FcaCodi\", \"FcaCodi\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"FCab\"";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->FcaCodi, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
				$rswrk = Conn()->Execute($sSqlWrk);
				if ($rswrk && !$rswrk->EOF) { // Lookup values found
					$arwrk = array();
					$arwrk[1] = $rswrk->fields('DispFld');
					$this->FcaCodi->EditValue = $this->FcaCodi->DisplayValue($arwrk);
					$rswrk->Close();
				} else {
					$this->FcaCodi->EditValue = $this->FcaCodi->CurrentValue;
				}
			} else {
				$this->FcaCodi->EditValue = NULL;
			}
			$this->FcaCodi->ViewCustomAttributes = "";

			// FdeCant
			$this->FdeCant->EditAttrs["class"] = "form-control";
			$this->FdeCant->EditCustomAttributes = "";
			$this->FdeCant->EditValue = ew_HtmlEncode($this->FdeCant->CurrentValue);
			$this->FdeCant->PlaceHolder = ew_RemoveHtml($this->FdeCant->FldCaption());

			// FdePrec
			$this->FdePrec->EditAttrs["class"] = "form-control";
			$this->FdePrec->EditCustomAttributes = "";
			$this->FdePrec->EditValue = ew_HtmlEncode($this->FdePrec->CurrentValue);
			$this->FdePrec->PlaceHolder = ew_RemoveHtml($this->FdePrec->FldCaption());
			if (strval($this->FdePrec->EditValue) <> "" && is_numeric($this->FdePrec->EditValue)) $this->FdePrec->EditValue = ew_FormatNumber($this->FdePrec->EditValue, -2, -1, -2, 0);

			// FdeDesc
			$this->FdeDesc->EditAttrs["class"] = "form-control";
			$this->FdeDesc->EditCustomAttributes = "";
			$this->FdeDesc->EditValue = ew_HtmlEncode($this->FdeDesc->CurrentValue);
			$this->FdeDesc->PlaceHolder = ew_RemoveHtml($this->FdeDesc->FldCaption());

			// FdeUsua
			$this->FdeUsua->EditAttrs["class"] = "form-control";
			$this->FdeUsua->EditCustomAttributes = "";
			$this->FdeUsua->EditValue = ew_HtmlEncode($this->FdeUsua->CurrentValue);
			$this->FdeUsua->PlaceHolder = ew_RemoveHtml($this->FdeUsua->FldCaption());

			// FdeFCre
			$this->FdeFCre->EditAttrs["class"] = "form-control";
			$this->FdeFCre->EditCustomAttributes = "";
			$this->FdeFCre->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->FdeFCre->CurrentValue, 7));
			$this->FdeFCre->PlaceHolder = ew_RemoveHtml($this->FdeFCre->FldCaption());

			// Edit refer script
			// FdeCodi

			$this->FdeCodi->HrefValue = "";

			// FcaCodi
			$this->FcaCodi->HrefValue = "";

			// FdeCant
			$this->FdeCant->HrefValue = "";

			// FdePrec
			$this->FdePrec->HrefValue = "";

			// FdeDesc
			$this->FdeDesc->HrefValue = "";

			// FdeUsua
			$this->FdeUsua->HrefValue = "";

			// FdeFCre
			$this->FdeFCre->HrefValue = "";
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate form
	function ValidateForm() {
		global $Language, $gsFormError;

		// Initialize form error message
		$gsFormError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return ($gsFormError == "");
		if (!$this->FdeCodi->FldIsDetailKey && !is_null($this->FdeCodi->FormValue) && $this->FdeCodi->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->FdeCodi->FldCaption(), $this->FdeCodi->ReqErrMsg));
		}
		if (!ew_CheckInteger($this->FdeCodi->FormValue)) {
			ew_AddMessage($gsFormError, $this->FdeCodi->FldErrMsg());
		}
		if (!$this->FcaCodi->FldIsDetailKey && !is_null($this->FcaCodi->FormValue) && $this->FcaCodi->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->FcaCodi->FldCaption(), $this->FcaCodi->ReqErrMsg));
		}
		if (!$this->FdeCant->FldIsDetailKey && !is_null($this->FdeCant->FormValue) && $this->FdeCant->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->FdeCant->FldCaption(), $this->FdeCant->ReqErrMsg));
		}
		if (!ew_CheckInteger($this->FdeCant->FormValue)) {
			ew_AddMessage($gsFormError, $this->FdeCant->FldErrMsg());
		}
		if (!$this->FdePrec->FldIsDetailKey && !is_null($this->FdePrec->FormValue) && $this->FdePrec->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->FdePrec->FldCaption(), $this->FdePrec->ReqErrMsg));
		}
		if (!ew_CheckNumber($this->FdePrec->FormValue)) {
			ew_AddMessage($gsFormError, $this->FdePrec->FldErrMsg());
		}
		if (!$this->FdeDesc->FldIsDetailKey && !is_null($this->FdeDesc->FormValue) && $this->FdeDesc->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->FdeDesc->FldCaption(), $this->FdeDesc->ReqErrMsg));
		}
		if (!$this->FdeUsua->FldIsDetailKey && !is_null($this->FdeUsua->FormValue) && $this->FdeUsua->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->FdeUsua->FldCaption(), $this->FdeUsua->ReqErrMsg));
		}
		if (!$this->FdeFCre->FldIsDetailKey && !is_null($this->FdeFCre->FormValue) && $this->FdeFCre->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->FdeFCre->FldCaption(), $this->FdeFCre->ReqErrMsg));
		}
		if (!ew_CheckEuroDate($this->FdeFCre->FormValue)) {
			ew_AddMessage($gsFormError, $this->FdeFCre->FldErrMsg());
		}

		// Return validate result
		$ValidateForm = ($gsFormError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateForm = $ValidateForm && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsFormError, $sFormCustomError);
		}
		return $ValidateForm;
	}

	// Update record based on key values
	function EditRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$conn = &$this->Connection();
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE)
			return FALSE;
		if ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record message
			$EditRow = FALSE; // Update Failed
		} else {

			// Save old values
			$rsold = &$rs->fields;
			$this->LoadDbValues($rsold);
			$rsnew = array();

			// FdeCodi
			// FcaCodi
			// FdeCant

			$this->FdeCant->SetDbValueDef($rsnew, $this->FdeCant->CurrentValue, 0, $this->FdeCant->ReadOnly);

			// FdePrec
			$this->FdePrec->SetDbValueDef($rsnew, $this->FdePrec->CurrentValue, 0, $this->FdePrec->ReadOnly);

			// FdeDesc
			$this->FdeDesc->SetDbValueDef($rsnew, $this->FdeDesc->CurrentValue, "", $this->FdeDesc->ReadOnly);

			// FdeUsua
			$this->FdeUsua->SetDbValueDef($rsnew, $this->FdeUsua->CurrentValue, "", $this->FdeUsua->ReadOnly);

			// FdeFCre
			$this->FdeFCre->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->FdeFCre->CurrentValue, 7), ew_CurrentDate(), $this->FdeFCre->ReadOnly);

			// Call Row Updating event
			$bUpdateRow = $this->Row_Updating($rsold, $rsnew);
			if ($bUpdateRow) {
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				if (count($rsnew) > 0)
					$EditRow = $this->Update($rsnew, "", $rsold);
				else
					$EditRow = TRUE; // No field to update
				$conn->raiseErrorFn = '';
				if ($EditRow) {
				}
			} else {
				if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

					// Use the message, do nothing
				} elseif ($this->CancelMessage <> "") {
					$this->setFailureMessage($this->CancelMessage);
					$this->CancelMessage = "";
				} else {
					$this->setFailureMessage($Language->Phrase("UpdateCancelled"));
				}
				$EditRow = FALSE;
			}
		}

		// Call Row_Updated event
		if ($EditRow)
			$this->Row_Updated($rsold, $rsnew);
		$rs->Close();
		return $EditRow;
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, "FDetlist.php", "", $this->TableVar, TRUE);
		$PageId = "edit";
		$Breadcrumb->Add("edit", $PageId, $url);
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($FDet_edit)) $FDet_edit = new cFDet_edit();

// Page init
$FDet_edit->Page_Init();

// Page main
$FDet_edit->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$FDet_edit->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "edit";
var CurrentForm = fFDetedit = new ew_Form("fFDetedit", "edit");

// Validate form
fFDetedit.Validate = function() {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.GetForm(), $fobj = $(fobj);
	if ($fobj.find("#a_confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.FormKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = $fobj.find("#a_list").val() == "gridinsert";
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
			elm = this.GetElements("x" + infix + "_FdeCodi");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $FDet->FdeCodi->FldCaption(), $FDet->FdeCodi->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_FdeCodi");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($FDet->FdeCodi->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_FcaCodi");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $FDet->FcaCodi->FldCaption(), $FDet->FcaCodi->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_FdeCant");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $FDet->FdeCant->FldCaption(), $FDet->FdeCant->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_FdeCant");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($FDet->FdeCant->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_FdePrec");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $FDet->FdePrec->FldCaption(), $FDet->FdePrec->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_FdePrec");
			if (elm && !ew_CheckNumber(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($FDet->FdePrec->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_FdeDesc");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $FDet->FdeDesc->FldCaption(), $FDet->FdeDesc->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_FdeUsua");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $FDet->FdeUsua->FldCaption(), $FDet->FdeUsua->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_FdeFCre");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $FDet->FdeFCre->FldCaption(), $FDet->FdeFCre->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_FdeFCre");
			if (elm && !ew_CheckEuroDate(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($FDet->FdeFCre->FldErrMsg()) ?>");

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
	}

	// Process detail forms
	var dfs = $fobj.find("input[name='detailpage']").get();
	for (var i = 0; i < dfs.length; i++) {
		var df = dfs[i], val = df.value;
		if (val && ewForms[val])
			if (!ewForms[val].Validate())
				return false;
	}
	return true;
}

// Form_CustomValidate event
fFDetedit.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fFDetedit.ValidateRequired = true;
<?php } else { ?>
fFDetedit.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
fFDetedit.Lists["x_FcaCodi"] = {"LinkField":"x_FcaCodi","Ajax":true,"AutoFill":false,"DisplayFields":["x_FcaCodi","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};

// Form object for search
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php $FDet_edit->ShowPageHeader(); ?>
<?php
$FDet_edit->ShowMessage();
?>
<form name="fFDetedit" id="fFDetedit" class="<?php echo $FDet_edit->FormClassName ?>" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($FDet_edit->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $FDet_edit->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="FDet">
<input type="hidden" name="a_edit" id="a_edit" value="U">
<?php if ($FDet->CurrentAction == "F") { // Confirm page ?>
<input type="hidden" name="a_confirm" id="a_confirm" value="F">
<?php } ?>
<div>
<?php if ($FDet->FdeCodi->Visible) { // FdeCodi ?>
	<div id="r_FdeCodi" class="form-group">
		<label id="elh_FDet_FdeCodi" for="x_FdeCodi" class="col-sm-2 control-label ewLabel"><?php echo $FDet->FdeCodi->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $FDet->FdeCodi->CellAttributes() ?>>
<?php if ($FDet->CurrentAction <> "F") { ?>
<span id="el_FDet_FdeCodi">
<span<?php echo $FDet->FdeCodi->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $FDet->FdeCodi->EditValue ?></p></span>
</span>
<input type="hidden" data-table="FDet" data-field="x_FdeCodi" name="x_FdeCodi" id="x_FdeCodi" value="<?php echo ew_HtmlEncode($FDet->FdeCodi->CurrentValue) ?>">
<?php } else { ?>
<span id="el_FDet_FdeCodi">
<span<?php echo $FDet->FdeCodi->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $FDet->FdeCodi->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="FDet" data-field="x_FdeCodi" name="x_FdeCodi" id="x_FdeCodi" value="<?php echo ew_HtmlEncode($FDet->FdeCodi->FormValue) ?>">
<?php } ?>
<?php echo $FDet->FdeCodi->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($FDet->FcaCodi->Visible) { // FcaCodi ?>
	<div id="r_FcaCodi" class="form-group">
		<label id="elh_FDet_FcaCodi" for="x_FcaCodi" class="col-sm-2 control-label ewLabel"><?php echo $FDet->FcaCodi->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $FDet->FcaCodi->CellAttributes() ?>>
<?php if ($FDet->CurrentAction <> "F") { ?>
<span id="el_FDet_FcaCodi">
<span<?php echo $FDet->FcaCodi->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $FDet->FcaCodi->EditValue ?></p></span>
</span>
<input type="hidden" data-table="FDet" data-field="x_FcaCodi" name="x_FcaCodi" id="x_FcaCodi" value="<?php echo ew_HtmlEncode($FDet->FcaCodi->CurrentValue) ?>">
<?php } else { ?>
<span id="el_FDet_FcaCodi">
<span<?php echo $FDet->FcaCodi->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $FDet->FcaCodi->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="FDet" data-field="x_FcaCodi" name="x_FcaCodi" id="x_FcaCodi" value="<?php echo ew_HtmlEncode($FDet->FcaCodi->FormValue) ?>">
<?php } ?>
<?php echo $FDet->FcaCodi->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($FDet->FdeCant->Visible) { // FdeCant ?>
	<div id="r_FdeCant" class="form-group">
		<label id="elh_FDet_FdeCant" for="x_FdeCant" class="col-sm-2 control-label ewLabel"><?php echo $FDet->FdeCant->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $FDet->FdeCant->CellAttributes() ?>>
<?php if ($FDet->CurrentAction <> "F") { ?>
<span id="el_FDet_FdeCant">
<input type="text" data-table="FDet" data-field="x_FdeCant" name="x_FdeCant" id="x_FdeCant" size="30" placeholder="<?php echo ew_HtmlEncode($FDet->FdeCant->getPlaceHolder()) ?>" value="<?php echo $FDet->FdeCant->EditValue ?>"<?php echo $FDet->FdeCant->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_FDet_FdeCant">
<span<?php echo $FDet->FdeCant->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $FDet->FdeCant->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="FDet" data-field="x_FdeCant" name="x_FdeCant" id="x_FdeCant" value="<?php echo ew_HtmlEncode($FDet->FdeCant->FormValue) ?>">
<?php } ?>
<?php echo $FDet->FdeCant->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($FDet->FdePrec->Visible) { // FdePrec ?>
	<div id="r_FdePrec" class="form-group">
		<label id="elh_FDet_FdePrec" for="x_FdePrec" class="col-sm-2 control-label ewLabel"><?php echo $FDet->FdePrec->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $FDet->FdePrec->CellAttributes() ?>>
<?php if ($FDet->CurrentAction <> "F") { ?>
<span id="el_FDet_FdePrec">
<input type="text" data-table="FDet" data-field="x_FdePrec" name="x_FdePrec" id="x_FdePrec" size="30" placeholder="<?php echo ew_HtmlEncode($FDet->FdePrec->getPlaceHolder()) ?>" value="<?php echo $FDet->FdePrec->EditValue ?>"<?php echo $FDet->FdePrec->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_FDet_FdePrec">
<span<?php echo $FDet->FdePrec->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $FDet->FdePrec->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="FDet" data-field="x_FdePrec" name="x_FdePrec" id="x_FdePrec" value="<?php echo ew_HtmlEncode($FDet->FdePrec->FormValue) ?>">
<?php } ?>
<?php echo $FDet->FdePrec->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($FDet->FdeDesc->Visible) { // FdeDesc ?>
	<div id="r_FdeDesc" class="form-group">
		<label id="elh_FDet_FdeDesc" for="x_FdeDesc" class="col-sm-2 control-label ewLabel"><?php echo $FDet->FdeDesc->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $FDet->FdeDesc->CellAttributes() ?>>
<?php if ($FDet->CurrentAction <> "F") { ?>
<span id="el_FDet_FdeDesc">
<input type="text" data-table="FDet" data-field="x_FdeDesc" name="x_FdeDesc" id="x_FdeDesc" size="30" placeholder="<?php echo ew_HtmlEncode($FDet->FdeDesc->getPlaceHolder()) ?>" value="<?php echo $FDet->FdeDesc->EditValue ?>"<?php echo $FDet->FdeDesc->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_FDet_FdeDesc">
<span<?php echo $FDet->FdeDesc->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $FDet->FdeDesc->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="FDet" data-field="x_FdeDesc" name="x_FdeDesc" id="x_FdeDesc" value="<?php echo ew_HtmlEncode($FDet->FdeDesc->FormValue) ?>">
<?php } ?>
<?php echo $FDet->FdeDesc->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($FDet->FdeUsua->Visible) { // FdeUsua ?>
	<div id="r_FdeUsua" class="form-group">
		<label id="elh_FDet_FdeUsua" for="x_FdeUsua" class="col-sm-2 control-label ewLabel"><?php echo $FDet->FdeUsua->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $FDet->FdeUsua->CellAttributes() ?>>
<?php if ($FDet->CurrentAction <> "F") { ?>
<span id="el_FDet_FdeUsua">
<input type="text" data-table="FDet" data-field="x_FdeUsua" name="x_FdeUsua" id="x_FdeUsua" size="30" placeholder="<?php echo ew_HtmlEncode($FDet->FdeUsua->getPlaceHolder()) ?>" value="<?php echo $FDet->FdeUsua->EditValue ?>"<?php echo $FDet->FdeUsua->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_FDet_FdeUsua">
<span<?php echo $FDet->FdeUsua->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $FDet->FdeUsua->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="FDet" data-field="x_FdeUsua" name="x_FdeUsua" id="x_FdeUsua" value="<?php echo ew_HtmlEncode($FDet->FdeUsua->FormValue) ?>">
<?php } ?>
<?php echo $FDet->FdeUsua->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($FDet->FdeFCre->Visible) { // FdeFCre ?>
	<div id="r_FdeFCre" class="form-group">
		<label id="elh_FDet_FdeFCre" for="x_FdeFCre" class="col-sm-2 control-label ewLabel"><?php echo $FDet->FdeFCre->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $FDet->FdeFCre->CellAttributes() ?>>
<?php if ($FDet->CurrentAction <> "F") { ?>
<span id="el_FDet_FdeFCre">
<input type="text" data-table="FDet" data-field="x_FdeFCre" data-format="7" name="x_FdeFCre" id="x_FdeFCre" placeholder="<?php echo ew_HtmlEncode($FDet->FdeFCre->getPlaceHolder()) ?>" value="<?php echo $FDet->FdeFCre->EditValue ?>"<?php echo $FDet->FdeFCre->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_FDet_FdeFCre">
<span<?php echo $FDet->FdeFCre->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $FDet->FdeFCre->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="FDet" data-field="x_FdeFCre" name="x_FdeFCre" id="x_FdeFCre" value="<?php echo ew_HtmlEncode($FDet->FdeFCre->FormValue) ?>">
<?php } ?>
<?php echo $FDet->FdeFCre->CustomMsg ?></div></div>
	</div>
<?php } ?>
</div>
<div class="form-group">
	<div class="col-sm-offset-2 col-sm-10">
<?php if ($FDet->CurrentAction <> "F") { // Confirm page ?>
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit" onclick="this.form.a_edit.value='F';"><?php echo $Language->Phrase("SaveBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="button" data-href="<?php echo $FDet_edit->getReturnUrl() ?>"><?php echo $Language->Phrase("CancelBtn") ?></button>
<?php } else { ?>
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("ConfirmBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="submit" onclick="this.form.a_edit.value='X';"><?php echo $Language->Phrase("CancelBtn") ?></button>
<?php } ?>
	</div>
</div>
</form>
<script type="text/javascript">
fFDetedit.Init();
</script>
<?php
$FDet_edit->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$FDet_edit->Page_Terminate();
?>
