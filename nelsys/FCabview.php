<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "FCabinfo.php" ?>
<?php include_once "Usuainfo.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$FCab_view = NULL; // Initialize page object first

class cFCab_view extends cFCab {

	// Page ID
	var $PageID = 'view';

	// Project ID
	var $ProjectID = "{04439FF7-B43F-460F-8514-F71C8FF9E679}";

	// Table name
	var $TableName = 'FCab';

	// Page object name
	var $PageObjName = 'FCab_view';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Page URLs
	var $AddUrl;
	var $EditUrl;
	var $CopyUrl;
	var $DeleteUrl;
	var $ViewUrl;
	var $ListUrl;

	// Export URLs
	var $ExportPrintUrl;
	var $ExportHtmlUrl;
	var $ExportExcelUrl;
	var $ExportWordUrl;
	var $ExportXmlUrl;
	var $ExportCsvUrl;
	var $ExportPdfUrl;

	// Custom export
	var $ExportExcelCustom = FALSE;
	var $ExportWordCustom = FALSE;
	var $ExportPdfCustom = FALSE;
	var $ExportEmailCustom = FALSE;

	// Update URLs
	var $InlineAddUrl;
	var $InlineCopyUrl;
	var $InlineEditUrl;
	var $GridAddUrl;
	var $GridEditUrl;
	var $MultiDeleteUrl;
	var $MultiUpdateUrl;

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (FCab)
		if (!isset($GLOBALS["FCab"]) || get_class($GLOBALS["FCab"]) == "cFCab") {
			$GLOBALS["FCab"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["FCab"];
		}
		$KeyUrl = "";
		if (@$_GET["FcaCodi"] <> "") {
			$this->RecKey["FcaCodi"] = $_GET["FcaCodi"];
			$KeyUrl .= "&amp;FcaCodi=" . urlencode($this->RecKey["FcaCodi"]);
		}
		$this->ExportPrintUrl = $this->PageUrl() . "export=print" . $KeyUrl;
		$this->ExportHtmlUrl = $this->PageUrl() . "export=html" . $KeyUrl;
		$this->ExportExcelUrl = $this->PageUrl() . "export=excel" . $KeyUrl;
		$this->ExportWordUrl = $this->PageUrl() . "export=word" . $KeyUrl;
		$this->ExportXmlUrl = $this->PageUrl() . "export=xml" . $KeyUrl;
		$this->ExportCsvUrl = $this->PageUrl() . "export=csv" . $KeyUrl;
		$this->ExportPdfUrl = $this->PageUrl() . "export=pdf" . $KeyUrl;

		// Table object (Usua)
		if (!isset($GLOBALS['Usua'])) $GLOBALS['Usua'] = new cUsua();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'view', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'FCab', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (Usua)
		if (!isset($UserTable)) {
			$UserTable = new cUsua();
			$UserTableConn = Conn($UserTable->DBID);
		}

		// Export options
		$this->ExportOptions = new cListOptions();
		$this->ExportOptions->Tag = "div";
		$this->ExportOptions->TagClassName = "ewExportOption";

		// Other options
		$this->OtherOptions['action'] = new cListOptions();
		$this->OtherOptions['action']->Tag = "div";
		$this->OtherOptions['action']->TagClassName = "ewActionOption";
		$this->OtherOptions['detail'] = new cListOptions();
		$this->OtherOptions['detail']->Tag = "div";
		$this->OtherOptions['detail']->TagClassName = "ewDetailOption";
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanView()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage($Language->Phrase("NoPermission")); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("FCablist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action
		$this->FcaCodi->Visible = !$this->IsAdd() && !$this->IsCopy() && !$this->IsGridAdd();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $FCab;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($FCab);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $ExportOptions; // Export options
	var $OtherOptions = array(); // Other options
	var $DisplayRecs = 1;
	var $DbMasterFilter;
	var $DbDetailFilter;
	var $StartRec;
	var $StopRec;
	var $TotalRecs = 0;
	var $RecRange = 10;
	var $RecCnt;
	var $RecKey = array();
	var $Recordset;

	//
	// Page main
	//
	function Page_Main() {
		global $Language;
		$sReturnUrl = "";
		$bMatchRecord = FALSE;

		// Set up Breadcrumb
		if ($this->Export == "")
			$this->SetupBreadcrumb();
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET["FcaCodi"] <> "") {
				$this->FcaCodi->setQueryStringValue($_GET["FcaCodi"]);
				$this->RecKey["FcaCodi"] = $this->FcaCodi->QueryStringValue;
			} elseif (@$_POST["FcaCodi"] <> "") {
				$this->FcaCodi->setFormValue($_POST["FcaCodi"]);
				$this->RecKey["FcaCodi"] = $this->FcaCodi->FormValue;
			} else {
				$sReturnUrl = "FCablist.php"; // Return to list
			}

			// Get action
			$this->CurrentAction = "I"; // Display form
			switch ($this->CurrentAction) {
				case "I": // Get a record to display
					if (!$this->LoadRow()) { // Load record based on key
						if ($this->getSuccessMessage() == "" && $this->getFailureMessage() == "")
							$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record message
						$sReturnUrl = "FCablist.php"; // No matching record, return to list
					}
			}
		} else {
			$sReturnUrl = "FCablist.php"; // Not page request, return to list
		}
		if ($sReturnUrl <> "")
			$this->Page_Terminate($sReturnUrl);

		// Render row
		$this->RowType = EW_ROWTYPE_VIEW;
		$this->ResetAttrs();
		$this->RenderRow();
	}

	// Set up other options
	function SetupOtherOptions() {
		global $Language, $Security;
		$options = &$this->OtherOptions;
		$option = &$options["action"];

		// Add
		$item = &$option->Add("add");
		$item->Body = "<a class=\"ewAction ewAdd\" title=\"" . ew_HtmlTitle($Language->Phrase("ViewPageAddLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("ViewPageAddLink")) . "\" href=\"" . ew_HtmlEncode($this->AddUrl) . "\">" . $Language->Phrase("ViewPageAddLink") . "</a>";
		$item->Visible = ($this->AddUrl <> "" && $Security->CanAdd());

		// Edit
		$item = &$option->Add("edit");
		$item->Body = "<a class=\"ewAction ewEdit\" title=\"" . ew_HtmlTitle($Language->Phrase("ViewPageEditLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("ViewPageEditLink")) . "\" href=\"" . ew_HtmlEncode($this->EditUrl) . "\">" . $Language->Phrase("ViewPageEditLink") . "</a>";
		$item->Visible = ($this->EditUrl <> "" && $Security->CanEdit());

		// Copy
		$item = &$option->Add("copy");
		$item->Body = "<a class=\"ewAction ewCopy\" title=\"" . ew_HtmlTitle($Language->Phrase("ViewPageCopyLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("ViewPageCopyLink")) . "\" href=\"" . ew_HtmlEncode($this->CopyUrl) . "\">" . $Language->Phrase("ViewPageCopyLink") . "</a>";
		$item->Visible = ($this->CopyUrl <> "" && $Security->CanAdd());

		// Delete
		$item = &$option->Add("delete");
		$item->Body = "<a class=\"ewAction ewDelete\" title=\"" . ew_HtmlTitle($Language->Phrase("ViewPageDeleteLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("ViewPageDeleteLink")) . "\" href=\"" . ew_HtmlEncode($this->DeleteUrl) . "\">" . $Language->Phrase("ViewPageDeleteLink") . "</a>";
		$item->Visible = ($this->DeleteUrl <> "" && $Security->CanDelete());

		// Set up action default
		$option = &$options["action"];
		$option->DropDownButtonPhrase = $Language->Phrase("ButtonActions");
		$option->UseImageAndText = TRUE;
		$option->UseDropDownButton = FALSE;
		$option->UseButtonGroup = TRUE;
		$item = &$option->Add($option->GroupOptionName);
		$item->Body = "";
		$item->Visible = FALSE;
	}

	// Set up starting record parameters
	function SetUpStartRec() {
		if ($this->DisplayRecs == 0)
			return;
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET[EW_TABLE_START_REC] <> "") { // Check for "start" parameter
				$this->StartRec = $_GET[EW_TABLE_START_REC];
				$this->setStartRecordNumber($this->StartRec);
			} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
				$PageNo = $_GET[EW_TABLE_PAGE_NO];
				if (is_numeric($PageNo)) {
					$this->StartRec = ($PageNo-1)*$this->DisplayRecs+1;
					if ($this->StartRec <= 0) {
						$this->StartRec = 1;
					} elseif ($this->StartRec >= intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1) {
						$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1;
					}
					$this->setStartRecordNumber($this->StartRec);
				}
			}
		}
		$this->StartRec = $this->getStartRecordNumber();

		// Check if correct start record counter
		if (!is_numeric($this->StartRec) || $this->StartRec == "") { // Avoid invalid start record counter
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} elseif (intval($this->StartRec) > intval($this->TotalRecs)) { // Avoid starting record > total records
			$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to last page first record
			$this->setStartRecordNumber($this->StartRec);
		} elseif (($this->StartRec-1) % $this->DisplayRecs <> 0) {
			$this->StartRec = intval(($this->StartRec-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to page boundary
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->FcaCodi->setDbValue($rs->fields('FcaCodi'));
		$this->OveCodi->setDbValue($rs->fields('OveCodi'));
		$this->FcaVend->setDbValue($rs->fields('FcaVend'));
		$this->FcaCaje->setDbValue($rs->fields('FcaCaje'));
		$this->FcaFech->setDbValue($rs->fields('FcaFech'));
		$this->FcaTimb->setDbValue($rs->fields('FcaTimb'));
		$this->FcaTFac->setDbValue($rs->fields('FcaTFac'));
		$this->FcaAnul->setDbValue($rs->fields('FcaAnul'));
		$this->FcaMAnu->setDbValue($rs->fields('FcaMAnu'));
		$this->FcaFAnu->setDbValue($rs->fields('FcaFAnu'));
		$this->FcaFTip->setDbValue($rs->fields('FcaFTip'));
		$this->FcaUsua->setDbValue($rs->fields('FcaUsua'));
		$this->FcaFCre->setDbValue($rs->fields('FcaFCre'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->FcaCodi->DbValue = $row['FcaCodi'];
		$this->OveCodi->DbValue = $row['OveCodi'];
		$this->FcaVend->DbValue = $row['FcaVend'];
		$this->FcaCaje->DbValue = $row['FcaCaje'];
		$this->FcaFech->DbValue = $row['FcaFech'];
		$this->FcaTimb->DbValue = $row['FcaTimb'];
		$this->FcaTFac->DbValue = $row['FcaTFac'];
		$this->FcaAnul->DbValue = $row['FcaAnul'];
		$this->FcaMAnu->DbValue = $row['FcaMAnu'];
		$this->FcaFAnu->DbValue = $row['FcaFAnu'];
		$this->FcaFTip->DbValue = $row['FcaFTip'];
		$this->FcaUsua->DbValue = $row['FcaUsua'];
		$this->FcaFCre->DbValue = $row['FcaFCre'];
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		$this->AddUrl = $this->GetAddUrl();
		$this->EditUrl = $this->GetEditUrl();
		$this->CopyUrl = $this->GetCopyUrl();
		$this->DeleteUrl = $this->GetDeleteUrl();
		$this->ListUrl = $this->GetListUrl();
		$this->SetupOtherOptions();

		// Convert decimal values if posted back
		if ($this->FcaTFac->FormValue == $this->FcaTFac->CurrentValue && is_numeric(ew_StrToFloat($this->FcaTFac->CurrentValue)))
			$this->FcaTFac->CurrentValue = ew_StrToFloat($this->FcaTFac->CurrentValue);

		// Call Row_Rendering event
		$this->Row_Rendering();

		// Common render codes for all row types
		// FcaCodi
		// OveCodi
		// FcaVend
		// FcaCaje
		// FcaFech
		// FcaTimb
		// FcaTFac
		// FcaAnul
		// FcaMAnu
		// FcaFAnu
		// FcaFTip
		// FcaUsua
		// FcaFCre

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// FcaCodi
		$this->FcaCodi->ViewValue = $this->FcaCodi->CurrentValue;
		$this->FcaCodi->ViewCustomAttributes = "";

		// OveCodi
		if (strval($this->OveCodi->CurrentValue) <> "") {
			$sFilterWrk = "\"OveCodi\"" . ew_SearchString("=", $this->OveCodi->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT \"OveCodi\", \"OveCodi\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"OVen\"";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->OveCodi, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->OveCodi->ViewValue = $this->OveCodi->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->OveCodi->ViewValue = $this->OveCodi->CurrentValue;
			}
		} else {
			$this->OveCodi->ViewValue = NULL;
		}
		$this->OveCodi->ViewCustomAttributes = "";

		// FcaVend
		$this->FcaVend->ViewValue = $this->FcaVend->CurrentValue;
		$this->FcaVend->ViewCustomAttributes = "";

		// FcaCaje
		$this->FcaCaje->ViewValue = $this->FcaCaje->CurrentValue;
		$this->FcaCaje->ViewCustomAttributes = "";

		// FcaFech
		$this->FcaFech->ViewValue = $this->FcaFech->CurrentValue;
		$this->FcaFech->ViewValue = ew_FormatDateTime($this->FcaFech->ViewValue, 7);
		$this->FcaFech->ViewCustomAttributes = "";

		// FcaTimb
		$this->FcaTimb->ViewValue = $this->FcaTimb->CurrentValue;
		$this->FcaTimb->ViewCustomAttributes = "";

		// FcaTFac
		$this->FcaTFac->ViewValue = $this->FcaTFac->CurrentValue;
		$this->FcaTFac->ViewCustomAttributes = "";

		// FcaAnul
		$this->FcaAnul->ViewValue = $this->FcaAnul->CurrentValue;
		$this->FcaAnul->ViewCustomAttributes = "";

		// FcaMAnu
		$this->FcaMAnu->ViewValue = $this->FcaMAnu->CurrentValue;
		$this->FcaMAnu->ViewCustomAttributes = "";

		// FcaFAnu
		$this->FcaFAnu->ViewValue = $this->FcaFAnu->CurrentValue;
		$this->FcaFAnu->ViewValue = ew_FormatDateTime($this->FcaFAnu->ViewValue, 7);
		$this->FcaFAnu->ViewCustomAttributes = "";

		// FcaFTip
		$this->FcaFTip->ViewValue = $this->FcaFTip->CurrentValue;
		$this->FcaFTip->ViewCustomAttributes = "";

		// FcaUsua
		$this->FcaUsua->ViewValue = $this->FcaUsua->CurrentValue;
		$this->FcaUsua->ViewCustomAttributes = "";

		// FcaFCre
		$this->FcaFCre->ViewValue = $this->FcaFCre->CurrentValue;
		$this->FcaFCre->ViewValue = ew_FormatDateTime($this->FcaFCre->ViewValue, 7);
		$this->FcaFCre->ViewCustomAttributes = "";

			// FcaCodi
			$this->FcaCodi->LinkCustomAttributes = "";
			$this->FcaCodi->HrefValue = "";
			$this->FcaCodi->TooltipValue = "";

			// OveCodi
			$this->OveCodi->LinkCustomAttributes = "";
			$this->OveCodi->HrefValue = "";
			$this->OveCodi->TooltipValue = "";

			// FcaVend
			$this->FcaVend->LinkCustomAttributes = "";
			$this->FcaVend->HrefValue = "";
			$this->FcaVend->TooltipValue = "";

			// FcaCaje
			$this->FcaCaje->LinkCustomAttributes = "";
			$this->FcaCaje->HrefValue = "";
			$this->FcaCaje->TooltipValue = "";

			// FcaFech
			$this->FcaFech->LinkCustomAttributes = "";
			$this->FcaFech->HrefValue = "";
			$this->FcaFech->TooltipValue = "";

			// FcaTimb
			$this->FcaTimb->LinkCustomAttributes = "";
			$this->FcaTimb->HrefValue = "";
			$this->FcaTimb->TooltipValue = "";

			// FcaTFac
			$this->FcaTFac->LinkCustomAttributes = "";
			$this->FcaTFac->HrefValue = "";
			$this->FcaTFac->TooltipValue = "";

			// FcaAnul
			$this->FcaAnul->LinkCustomAttributes = "";
			$this->FcaAnul->HrefValue = "";
			$this->FcaAnul->TooltipValue = "";

			// FcaMAnu
			$this->FcaMAnu->LinkCustomAttributes = "";
			$this->FcaMAnu->HrefValue = "";
			$this->FcaMAnu->TooltipValue = "";

			// FcaFAnu
			$this->FcaFAnu->LinkCustomAttributes = "";
			$this->FcaFAnu->HrefValue = "";
			$this->FcaFAnu->TooltipValue = "";

			// FcaFTip
			$this->FcaFTip->LinkCustomAttributes = "";
			$this->FcaFTip->HrefValue = "";
			$this->FcaFTip->TooltipValue = "";

			// FcaUsua
			$this->FcaUsua->LinkCustomAttributes = "";
			$this->FcaUsua->HrefValue = "";
			$this->FcaUsua->TooltipValue = "";

			// FcaFCre
			$this->FcaFCre->LinkCustomAttributes = "";
			$this->FcaFCre->HrefValue = "";
			$this->FcaFCre->TooltipValue = "";
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, "FCablist.php", "", $this->TableVar, TRUE);
		$PageId = "view";
		$Breadcrumb->Add("view", $PageId, $url);
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Page Exporting event
	// $this->ExportDoc = export document object
	function Page_Exporting() {

		//$this->ExportDoc->Text = "my header"; // Export header
		//return FALSE; // Return FALSE to skip default export and use Row_Export event

		return TRUE; // Return TRUE to use default export and skip Row_Export event
	}

	// Row Export event
	// $this->ExportDoc = export document object
	function Row_Export($rs) {

	    //$this->ExportDoc->Text .= "my content"; // Build HTML with field value: $rs["MyField"] or $this->MyField->ViewValue
	}

	// Page Exported event
	// $this->ExportDoc = export document object
	function Page_Exported() {

		//$this->ExportDoc->Text .= "my footer"; // Export footer
		//echo $this->ExportDoc->Text;

	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($FCab_view)) $FCab_view = new cFCab_view();

// Page init
$FCab_view->Page_Init();

// Page main
$FCab_view->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$FCab_view->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "view";
var CurrentForm = fFCabview = new ew_Form("fFCabview", "view");

// Form_CustomValidate event
fFCabview.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fFCabview.ValidateRequired = true;
<?php } else { ?>
fFCabview.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
fFCabview.Lists["x_OveCodi"] = {"LinkField":"x_OveCodi","Ajax":true,"AutoFill":false,"DisplayFields":["x_OveCodi","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};

// Form object for search
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php $FCab_view->ExportOptions->Render("body") ?>
<?php
	foreach ($FCab_view->OtherOptions as &$option)
		$option->Render("body");
?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php $FCab_view->ShowPageHeader(); ?>
<?php
$FCab_view->ShowMessage();
?>
<form name="fFCabview" id="fFCabview" class="form-inline ewForm ewViewForm" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($FCab_view->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $FCab_view->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="FCab">
<table class="table table-bordered table-striped ewViewTable">
<?php if ($FCab->FcaCodi->Visible) { // FcaCodi ?>
	<tr id="r_FcaCodi">
		<td><span id="elh_FCab_FcaCodi"><?php echo $FCab->FcaCodi->FldCaption() ?></span></td>
		<td data-name="FcaCodi"<?php echo $FCab->FcaCodi->CellAttributes() ?>>
<span id="el_FCab_FcaCodi">
<span<?php echo $FCab->FcaCodi->ViewAttributes() ?>>
<?php echo $FCab->FcaCodi->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($FCab->OveCodi->Visible) { // OveCodi ?>
	<tr id="r_OveCodi">
		<td><span id="elh_FCab_OveCodi"><?php echo $FCab->OveCodi->FldCaption() ?></span></td>
		<td data-name="OveCodi"<?php echo $FCab->OveCodi->CellAttributes() ?>>
<span id="el_FCab_OveCodi">
<span<?php echo $FCab->OveCodi->ViewAttributes() ?>>
<?php echo $FCab->OveCodi->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($FCab->FcaVend->Visible) { // FcaVend ?>
	<tr id="r_FcaVend">
		<td><span id="elh_FCab_FcaVend"><?php echo $FCab->FcaVend->FldCaption() ?></span></td>
		<td data-name="FcaVend"<?php echo $FCab->FcaVend->CellAttributes() ?>>
<span id="el_FCab_FcaVend">
<span<?php echo $FCab->FcaVend->ViewAttributes() ?>>
<?php echo $FCab->FcaVend->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($FCab->FcaCaje->Visible) { // FcaCaje ?>
	<tr id="r_FcaCaje">
		<td><span id="elh_FCab_FcaCaje"><?php echo $FCab->FcaCaje->FldCaption() ?></span></td>
		<td data-name="FcaCaje"<?php echo $FCab->FcaCaje->CellAttributes() ?>>
<span id="el_FCab_FcaCaje">
<span<?php echo $FCab->FcaCaje->ViewAttributes() ?>>
<?php echo $FCab->FcaCaje->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($FCab->FcaFech->Visible) { // FcaFech ?>
	<tr id="r_FcaFech">
		<td><span id="elh_FCab_FcaFech"><?php echo $FCab->FcaFech->FldCaption() ?></span></td>
		<td data-name="FcaFech"<?php echo $FCab->FcaFech->CellAttributes() ?>>
<span id="el_FCab_FcaFech">
<span<?php echo $FCab->FcaFech->ViewAttributes() ?>>
<?php echo $FCab->FcaFech->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($FCab->FcaTimb->Visible) { // FcaTimb ?>
	<tr id="r_FcaTimb">
		<td><span id="elh_FCab_FcaTimb"><?php echo $FCab->FcaTimb->FldCaption() ?></span></td>
		<td data-name="FcaTimb"<?php echo $FCab->FcaTimb->CellAttributes() ?>>
<span id="el_FCab_FcaTimb">
<span<?php echo $FCab->FcaTimb->ViewAttributes() ?>>
<?php echo $FCab->FcaTimb->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($FCab->FcaTFac->Visible) { // FcaTFac ?>
	<tr id="r_FcaTFac">
		<td><span id="elh_FCab_FcaTFac"><?php echo $FCab->FcaTFac->FldCaption() ?></span></td>
		<td data-name="FcaTFac"<?php echo $FCab->FcaTFac->CellAttributes() ?>>
<span id="el_FCab_FcaTFac">
<span<?php echo $FCab->FcaTFac->ViewAttributes() ?>>
<?php echo $FCab->FcaTFac->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($FCab->FcaAnul->Visible) { // FcaAnul ?>
	<tr id="r_FcaAnul">
		<td><span id="elh_FCab_FcaAnul"><?php echo $FCab->FcaAnul->FldCaption() ?></span></td>
		<td data-name="FcaAnul"<?php echo $FCab->FcaAnul->CellAttributes() ?>>
<span id="el_FCab_FcaAnul">
<span<?php echo $FCab->FcaAnul->ViewAttributes() ?>>
<?php echo $FCab->FcaAnul->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($FCab->FcaMAnu->Visible) { // FcaMAnu ?>
	<tr id="r_FcaMAnu">
		<td><span id="elh_FCab_FcaMAnu"><?php echo $FCab->FcaMAnu->FldCaption() ?></span></td>
		<td data-name="FcaMAnu"<?php echo $FCab->FcaMAnu->CellAttributes() ?>>
<span id="el_FCab_FcaMAnu">
<span<?php echo $FCab->FcaMAnu->ViewAttributes() ?>>
<?php echo $FCab->FcaMAnu->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($FCab->FcaFAnu->Visible) { // FcaFAnu ?>
	<tr id="r_FcaFAnu">
		<td><span id="elh_FCab_FcaFAnu"><?php echo $FCab->FcaFAnu->FldCaption() ?></span></td>
		<td data-name="FcaFAnu"<?php echo $FCab->FcaFAnu->CellAttributes() ?>>
<span id="el_FCab_FcaFAnu">
<span<?php echo $FCab->FcaFAnu->ViewAttributes() ?>>
<?php echo $FCab->FcaFAnu->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($FCab->FcaFTip->Visible) { // FcaFTip ?>
	<tr id="r_FcaFTip">
		<td><span id="elh_FCab_FcaFTip"><?php echo $FCab->FcaFTip->FldCaption() ?></span></td>
		<td data-name="FcaFTip"<?php echo $FCab->FcaFTip->CellAttributes() ?>>
<span id="el_FCab_FcaFTip">
<span<?php echo $FCab->FcaFTip->ViewAttributes() ?>>
<?php echo $FCab->FcaFTip->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($FCab->FcaUsua->Visible) { // FcaUsua ?>
	<tr id="r_FcaUsua">
		<td><span id="elh_FCab_FcaUsua"><?php echo $FCab->FcaUsua->FldCaption() ?></span></td>
		<td data-name="FcaUsua"<?php echo $FCab->FcaUsua->CellAttributes() ?>>
<span id="el_FCab_FcaUsua">
<span<?php echo $FCab->FcaUsua->ViewAttributes() ?>>
<?php echo $FCab->FcaUsua->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($FCab->FcaFCre->Visible) { // FcaFCre ?>
	<tr id="r_FcaFCre">
		<td><span id="elh_FCab_FcaFCre"><?php echo $FCab->FcaFCre->FldCaption() ?></span></td>
		<td data-name="FcaFCre"<?php echo $FCab->FcaFCre->CellAttributes() ?>>
<span id="el_FCab_FcaFCre">
<span<?php echo $FCab->FcaFCre->ViewAttributes() ?>>
<?php echo $FCab->FcaFCre->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
</table>
</form>
<script type="text/javascript">
fFCabview.Init();
</script>
<?php
$FCab_view->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$FCab_view->Page_Terminate();
?>
