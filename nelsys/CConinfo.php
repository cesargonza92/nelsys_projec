<?php

// Global variable for table object
$CCon = NULL;

//
// Table class for CCon
//
class cCCon extends cTable {
	var $CcoCodi;
	var $VcoCodi;
	var $CcoFPag;
	var $CcoMont;
	var $CcoEsta;
	var $CcoAnho;
	var $CcoMes;
	var $CcoConc;
	var $CcoUsua;
	var $CcoFCre;

	//
	// Table class constructor
	//
	function __construct() {
		global $Language;

		// Language object
		if (!isset($Language)) $Language = new cLanguage();
		$this->TableVar = 'CCon';
		$this->TableName = 'CCon';
		$this->TableType = 'TABLE';

		// Update Table
		$this->UpdateTable = "\"public\".\"CCon\"";
		$this->DBID = 'DB';
		$this->ExportAll = TRUE;
		$this->ExportPageBreakCount = 0; // Page break per every n record (PDF only)
		$this->ExportPageOrientation = "portrait"; // Page orientation (PDF only)
		$this->ExportPageSize = "a4"; // Page size (PDF only)
		$this->ExportExcelPageOrientation = ""; // Page orientation (PHPExcel only)
		$this->ExportExcelPageSize = ""; // Page size (PHPExcel only)
		$this->DetailAdd = TRUE; // Allow detail add
		$this->DetailEdit = TRUE; // Allow detail edit
		$this->DetailView = FALSE; // Allow detail view
		$this->ShowMultipleDetails = FALSE; // Show multiple details
		$this->GridAddRowCount = 5;
		$this->AllowAddDeleteRow = ew_AllowAddDeleteRow(); // Allow add/delete row
		$this->UserIDAllowSecurity = 0; // User ID Allow
		$this->BasicSearch = new cBasicSearch($this->TableVar);

		// CcoCodi
		$this->CcoCodi = new cField('CCon', 'CCon', 'x_CcoCodi', 'CcoCodi', '"CcoCodi"', 'CAST("CcoCodi" AS varchar(255))', 3, -1, FALSE, '"CcoCodi"', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'NO');
		$this->CcoCodi->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['CcoCodi'] = &$this->CcoCodi;

		// VcoCodi
		$this->VcoCodi = new cField('CCon', 'CCon', 'x_VcoCodi', 'VcoCodi', '"VcoCodi"', 'CAST("VcoCodi" AS varchar(255))', 3, -1, FALSE, '"VcoCodi"', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'SELECT');
		$this->VcoCodi->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['VcoCodi'] = &$this->VcoCodi;

		// CcoFPag
		$this->CcoFPag = new cField('CCon', 'CCon', 'x_CcoFPag', 'CcoFPag', '"CcoFPag"', 'CAST("CcoFPag" AS varchar(255))', 133, 7, FALSE, '"CcoFPag"', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->CcoFPag->FldDefaultErrMsg = str_replace("%s", "/", $Language->Phrase("IncorrectDateDMY"));
		$this->fields['CcoFPag'] = &$this->CcoFPag;

		// CcoMont
		$this->CcoMont = new cField('CCon', 'CCon', 'x_CcoMont', 'CcoMont', '"CcoMont"', 'CAST("CcoMont" AS varchar(255))', 5, -1, FALSE, '"CcoMont"', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->CcoMont->FldDefaultErrMsg = $Language->Phrase("IncorrectFloat");
		$this->fields['CcoMont'] = &$this->CcoMont;

		// CcoEsta
		$this->CcoEsta = new cField('CCon', 'CCon', 'x_CcoEsta', 'CcoEsta', '"CcoEsta"', '"CcoEsta"', 200, -1, FALSE, '"CcoEsta"', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->fields['CcoEsta'] = &$this->CcoEsta;

		// CcoAnho
		$this->CcoAnho = new cField('CCon', 'CCon', 'x_CcoAnho', 'CcoAnho', '"CcoAnho"', '"CcoAnho"', 200, -1, FALSE, '"CcoAnho"', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->fields['CcoAnho'] = &$this->CcoAnho;

		// CcoMes
		$this->CcoMes = new cField('CCon', 'CCon', 'x_CcoMes', 'CcoMes', '"CcoMes"', '"CcoMes"', 200, -1, FALSE, '"CcoMes"', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->fields['CcoMes'] = &$this->CcoMes;

		// CcoConc
		$this->CcoConc = new cField('CCon', 'CCon', 'x_CcoConc', 'CcoConc', '"CcoConc"', '"CcoConc"', 200, -1, FALSE, '"CcoConc"', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->fields['CcoConc'] = &$this->CcoConc;

		// CcoUsua
		$this->CcoUsua = new cField('CCon', 'CCon', 'x_CcoUsua', 'CcoUsua', '"CcoUsua"', '"CcoUsua"', 200, -1, FALSE, '"CcoUsua"', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->fields['CcoUsua'] = &$this->CcoUsua;

		// CcoFCre
		$this->CcoFCre = new cField('CCon', 'CCon', 'x_CcoFCre', 'CcoFCre', '"CcoFCre"', 'CAST("CcoFCre" AS varchar(255))', 135, 7, FALSE, '"CcoFCre"', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->CcoFCre->FldDefaultErrMsg = str_replace("%s", "/", $Language->Phrase("IncorrectDateDMY"));
		$this->fields['CcoFCre'] = &$this->CcoFCre;
	}

	// Multiple column sort
	function UpdateSort(&$ofld, $ctrl) {
		if ($this->CurrentOrder == $ofld->FldName) {
			$sSortField = $ofld->FldExpression;
			$sLastSort = $ofld->getSort();
			if ($this->CurrentOrderType == "ASC" || $this->CurrentOrderType == "DESC") {
				$sThisSort = $this->CurrentOrderType;
			} else {
				$sThisSort = ($sLastSort == "ASC") ? "DESC" : "ASC";
			}
			$ofld->setSort($sThisSort);
			if ($ctrl) {
				$sOrderBy = $this->getSessionOrderBy();
				if (strpos($sOrderBy, $sSortField . " " . $sLastSort) !== FALSE) {
					$sOrderBy = str_replace($sSortField . " " . $sLastSort, $sSortField . " " . $sThisSort, $sOrderBy);
				} else {
					if ($sOrderBy <> "") $sOrderBy .= ", ";
					$sOrderBy .= $sSortField . " " . $sThisSort;
				}
				$this->setSessionOrderBy($sOrderBy); // Save to Session
			} else {
				$this->setSessionOrderBy($sSortField . " " . $sThisSort); // Save to Session
			}
		} else {
			if (!$ctrl) $ofld->setSort("");
		}
	}

	// Table level SQL
	var $_SqlFrom = "";

	function getSqlFrom() { // From
		return ($this->_SqlFrom <> "") ? $this->_SqlFrom : "\"public\".\"CCon\"";
	}

	function SqlFrom() { // For backward compatibility
    	return $this->getSqlFrom();
	}

	function setSqlFrom($v) {
    	$this->_SqlFrom = $v;
	}
	var $_SqlSelect = "";

	function getSqlSelect() { // Select
		return ($this->_SqlSelect <> "") ? $this->_SqlSelect : "SELECT * FROM " . $this->getSqlFrom();
	}

	function SqlSelect() { // For backward compatibility
    	return $this->getSqlSelect();
	}

	function setSqlSelect($v) {
    	$this->_SqlSelect = $v;
	}
	var $_SqlWhere = "";

	function getSqlWhere() { // Where
		$sWhere = ($this->_SqlWhere <> "") ? $this->_SqlWhere : "";
		$this->TableFilter = "";
		ew_AddFilter($sWhere, $this->TableFilter);
		return $sWhere;
	}

	function SqlWhere() { // For backward compatibility
    	return $this->getSqlWhere();
	}

	function setSqlWhere($v) {
    	$this->_SqlWhere = $v;
	}
	var $_SqlGroupBy = "";

	function getSqlGroupBy() { // Group By
		return ($this->_SqlGroupBy <> "") ? $this->_SqlGroupBy : "";
	}

	function SqlGroupBy() { // For backward compatibility
    	return $this->getSqlGroupBy();
	}

	function setSqlGroupBy($v) {
    	$this->_SqlGroupBy = $v;
	}
	var $_SqlHaving = "";

	function getSqlHaving() { // Having
		return ($this->_SqlHaving <> "") ? $this->_SqlHaving : "";
	}

	function SqlHaving() { // For backward compatibility
    	return $this->getSqlHaving();
	}

	function setSqlHaving($v) {
    	$this->_SqlHaving = $v;
	}
	var $_SqlOrderBy = "";

	function getSqlOrderBy() { // Order By
		return ($this->_SqlOrderBy <> "") ? $this->_SqlOrderBy : "";
	}

	function SqlOrderBy() { // For backward compatibility
    	return $this->getSqlOrderBy();
	}

	function setSqlOrderBy($v) {
    	$this->_SqlOrderBy = $v;
	}

	// Apply User ID filters
	function ApplyUserIDFilters($sFilter) {
		return $sFilter;
	}

	// Check if User ID security allows view all
	function UserIDAllow($id = "") {
		$allow = EW_USER_ID_ALLOW;
		switch ($id) {
			case "add":
			case "copy":
			case "gridadd":
			case "register":
			case "addopt":
				return (($allow & 1) == 1);
			case "edit":
			case "gridedit":
			case "update":
			case "changepwd":
			case "forgotpwd":
				return (($allow & 4) == 4);
			case "delete":
				return (($allow & 2) == 2);
			case "view":
				return (($allow & 32) == 32);
			case "search":
				return (($allow & 64) == 64);
			default:
				return (($allow & 8) == 8);
		}
	}

	// Get SQL
	function GetSQL($where, $orderby) {
		return ew_BuildSelectSql($this->getSqlSelect(), $this->getSqlWhere(),
			$this->getSqlGroupBy(), $this->getSqlHaving(), $this->getSqlOrderBy(),
			$where, $orderby);
	}

	// Table SQL
	function SQL() {
		$sFilter = $this->CurrentFilter;
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$sSort = $this->getSessionOrderBy();
		return ew_BuildSelectSql($this->getSqlSelect(), $this->getSqlWhere(),
			$this->getSqlGroupBy(), $this->getSqlHaving(), $this->getSqlOrderBy(),
			$sFilter, $sSort);
	}

	// Table SQL with List page filter
	function SelectSQL() {
		$sFilter = $this->getSessionWhere();
		ew_AddFilter($sFilter, $this->CurrentFilter);
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$this->Recordset_Selecting($sFilter);
		$sSort = $this->getSessionOrderBy();
		return ew_BuildSelectSql($this->getSqlSelect(), $this->getSqlWhere(), $this->getSqlGroupBy(),
			$this->getSqlHaving(), $this->getSqlOrderBy(), $sFilter, $sSort);
	}

	// Get ORDER BY clause
	function GetOrderBy() {
		$sSort = $this->getSessionOrderBy();
		return ew_BuildSelectSql("", "", "", "", $this->getSqlOrderBy(), "", $sSort);
	}

	// Try to get record count
	function TryGetRecordCount($sSql) {
		$cnt = -1;
		if (($this->TableType == 'TABLE' || $this->TableType == 'VIEW' || $this->TableType == 'LINKTABLE') && preg_match("/^SELECT \* FROM/i", $sSql)) {
			$sSql = "SELECT COUNT(*) FROM" . preg_replace('/^SELECT\s([\s\S]+)?\*\sFROM/i', "", $sSql);
			$sOrderBy = $this->GetOrderBy();
			if (substr($sSql, strlen($sOrderBy) * -1) == $sOrderBy)
				$sSql = substr($sSql, 0, strlen($sSql) - strlen($sOrderBy)); // Remove ORDER BY clause
		} else {
			$sSql = "SELECT COUNT(*) FROM (" . $sSql . ") EW_COUNT_TABLE";
		}
		$conn = &$this->Connection();
		if ($rs = $conn->Execute($sSql)) {
			if (!$rs->EOF && $rs->FieldCount() > 0) {
				$cnt = $rs->fields[0];
				$rs->Close();
			}
		}
		return intval($cnt);
	}

	// Get record count based on filter (for detail record count in master table pages)
	function LoadRecordCount($sFilter) {
		$origFilter = $this->CurrentFilter;
		$this->CurrentFilter = $sFilter;
		$this->Recordset_Selecting($this->CurrentFilter);

		//$sSql = $this->SQL();
		$sSql = $this->GetSQL($this->CurrentFilter, "");
		$cnt = $this->TryGetRecordCount($sSql);
		if ($cnt == -1) {
			if ($rs = $this->LoadRs($this->CurrentFilter)) {
				$cnt = $rs->RecordCount();
				$rs->Close();
			}
		}
		$this->CurrentFilter = $origFilter;
		return intval($cnt);
	}

	// Get record count (for current List page)
	function SelectRecordCount() {
		$sSql = $this->SelectSQL();
		$cnt = $this->TryGetRecordCount($sSql);
		if ($cnt == -1) {
			$conn = &$this->Connection();
			if ($rs = $conn->Execute($sSql)) {
				$cnt = $rs->RecordCount();
				$rs->Close();
			}
		}
		return intval($cnt);
	}

	// INSERT statement
	function InsertSQL(&$rs) {
		$names = "";
		$values = "";
		foreach ($rs as $name => $value) {
			if (!isset($this->fields[$name]) || $this->fields[$name]->FldIsCustom)
				continue;
			$names .= $this->fields[$name]->FldExpression . ",";
			$values .= ew_QuotedValue($value, $this->fields[$name]->FldDataType, $this->DBID) . ",";
		}
		while (substr($names, -1) == ",")
			$names = substr($names, 0, -1);
		while (substr($values, -1) == ",")
			$values = substr($values, 0, -1);
		return "INSERT INTO " . $this->UpdateTable . " ($names) VALUES ($values)";
	}

	// Insert
	function Insert(&$rs) {
		$conn = &$this->Connection();
		return $conn->Execute($this->InsertSQL($rs));
	}

	// UPDATE statement
	function UpdateSQL(&$rs, $where = "", $curfilter = TRUE) {
		$sql = "UPDATE " . $this->UpdateTable . " SET ";
		foreach ($rs as $name => $value) {
			if (!isset($this->fields[$name]) || $this->fields[$name]->FldIsCustom)
				continue;
			$sql .= $this->fields[$name]->FldExpression . "=";
			$sql .= ew_QuotedValue($value, $this->fields[$name]->FldDataType, $this->DBID) . ",";
		}
		while (substr($sql, -1) == ",")
			$sql = substr($sql, 0, -1);
		$filter = ($curfilter) ? $this->CurrentFilter : "";
		if (is_array($where))
			$where = $this->ArrayToFilter($where);
		ew_AddFilter($filter, $where);
		if ($filter <> "")	$sql .= " WHERE " . $filter;
		return $sql;
	}

	// Update
	function Update(&$rs, $where = "", $rsold = NULL, $curfilter = TRUE) {
		$conn = &$this->Connection();
		return $conn->Execute($this->UpdateSQL($rs, $where, $curfilter));
	}

	// DELETE statement
	function DeleteSQL(&$rs, $where = "", $curfilter = TRUE) {
		$sql = "DELETE FROM " . $this->UpdateTable . " WHERE ";
		if (is_array($where))
			$where = $this->ArrayToFilter($where);
		if ($rs) {
			if (array_key_exists('CcoCodi', $rs))
				ew_AddFilter($where, ew_QuotedName('CcoCodi', $this->DBID) . '=' . ew_QuotedValue($rs['CcoCodi'], $this->CcoCodi->FldDataType, $this->DBID));
		}
		$filter = ($curfilter) ? $this->CurrentFilter : "";
		ew_AddFilter($filter, $where);
		if ($filter <> "")
			$sql .= $filter;
		else
			$sql .= "0=1"; // Avoid delete
		return $sql;
	}

	// Delete
	function Delete(&$rs, $where = "", $curfilter = TRUE) {
		$conn = &$this->Connection();
		return $conn->Execute($this->DeleteSQL($rs, $where, $curfilter));
	}

	// Key filter WHERE clause
	function SqlKeyFilter() {
		return "\"CcoCodi\" = @CcoCodi@";
	}

	// Key filter
	function KeyFilter() {
		$sKeyFilter = $this->SqlKeyFilter();
		if (!is_numeric($this->CcoCodi->CurrentValue))
			$sKeyFilter = "0=1"; // Invalid key
		$sKeyFilter = str_replace("@CcoCodi@", ew_AdjustSql($this->CcoCodi->CurrentValue, $this->DBID), $sKeyFilter); // Replace key value
		return $sKeyFilter;
	}

	// Return page URL
	function getReturnUrl() {
		$name = EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_RETURN_URL;

		// Get referer URL automatically
		if (ew_ServerVar("HTTP_REFERER") <> "" && ew_ReferPage() <> ew_CurrentPage() && ew_ReferPage() <> "login.php") // Referer not same page or login page
			$_SESSION[$name] = ew_ServerVar("HTTP_REFERER"); // Save to Session
		if (@$_SESSION[$name] <> "") {
			return $_SESSION[$name];
		} else {
			return "CConlist.php";
		}
	}

	function setReturnUrl($v) {
		$_SESSION[EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_RETURN_URL] = $v;
	}

	// List URL
	function GetListUrl() {
		return "CConlist.php";
	}

	// View URL
	function GetViewUrl($parm = "") {
		if ($parm <> "")
			$url = $this->KeyUrl("CConview.php", $this->UrlParm($parm));
		else
			$url = $this->KeyUrl("CConview.php", $this->UrlParm(EW_TABLE_SHOW_DETAIL . "="));
		return $this->AddMasterUrl($url);
	}

	// Add URL
	function GetAddUrl($parm = "") {
		if ($parm <> "")
			$url = "CConadd.php?" . $this->UrlParm($parm);
		else
			$url = "CConadd.php";
		return $this->AddMasterUrl($url);
	}

	// Edit URL
	function GetEditUrl($parm = "") {
		$url = $this->KeyUrl("CConedit.php", $this->UrlParm($parm));
		return $this->AddMasterUrl($url);
	}

	// Inline edit URL
	function GetInlineEditUrl() {
		$url = $this->KeyUrl(ew_CurrentPage(), $this->UrlParm("a=edit"));
		return $this->AddMasterUrl($url);
	}

	// Copy URL
	function GetCopyUrl($parm = "") {
		$url = $this->KeyUrl("CConadd.php", $this->UrlParm($parm));
		return $this->AddMasterUrl($url);
	}

	// Inline copy URL
	function GetInlineCopyUrl() {
		$url = $this->KeyUrl(ew_CurrentPage(), $this->UrlParm("a=copy"));
		return $this->AddMasterUrl($url);
	}

	// Delete URL
	function GetDeleteUrl() {
		return $this->KeyUrl("CCondelete.php", $this->UrlParm());
	}

	// Add master url
	function AddMasterUrl($url) {
		return $url;
	}

	function KeyToJson() {
		$json = "";
		$json .= "CcoCodi:" . ew_VarToJson($this->CcoCodi->CurrentValue, "number", "'");
		return "{" . $json . "}";
	}

	// Add key value to URL
	function KeyUrl($url, $parm = "") {
		$sUrl = $url . "?";
		if ($parm <> "") $sUrl .= $parm . "&";
		if (!is_null($this->CcoCodi->CurrentValue)) {
			$sUrl .= "CcoCodi=" . urlencode($this->CcoCodi->CurrentValue);
		} else {
			return "javascript:ew_Alert(ewLanguage.Phrase('InvalidRecord'));";
		}
		return $sUrl;
	}

	// Sort URL
	function SortUrl(&$fld) {
		if ($this->CurrentAction <> "" || $this->Export <> "" ||
			in_array($fld->FldType, array(128, 204, 205))) { // Unsortable data type
				return "";
		} elseif ($fld->Sortable) {
			$sUrlParm = $this->UrlParm("order=" . urlencode($fld->FldName) . "&amp;ordertype=" . $fld->ReverseSort());
			return ew_CurrentPage() . "?" . $sUrlParm;
		} else {
			return "";
		}
	}

	// Get record keys from $_POST/$_GET/$_SESSION
	function GetRecordKeys() {
		global $EW_COMPOSITE_KEY_SEPARATOR;
		$arKeys = array();
		$arKey = array();
		if (isset($_POST["key_m"])) {
			$arKeys = ew_StripSlashes($_POST["key_m"]);
			$cnt = count($arKeys);
		} elseif (isset($_GET["key_m"])) {
			$arKeys = ew_StripSlashes($_GET["key_m"]);
			$cnt = count($arKeys);
		} elseif (!empty($_GET) || !empty($_POST)) {
			$isPost = ew_IsHttpPost();
			$arKeys[] = $isPost ? ew_StripSlashes(@$_POST["CcoCodi"]) : ew_StripSlashes(@$_GET["CcoCodi"]); // CcoCodi

			//return $arKeys; // Do not return yet, so the values will also be checked by the following code
		}

		// Check keys
		$ar = array();
		foreach ($arKeys as $key) {
			if (!is_numeric($key))
				continue;
			$ar[] = $key;
		}
		return $ar;
	}

	// Get key filter
	function GetKeyFilter() {
		$arKeys = $this->GetRecordKeys();
		$sKeyFilter = "";
		foreach ($arKeys as $key) {
			if ($sKeyFilter <> "") $sKeyFilter .= " OR ";
			$this->CcoCodi->CurrentValue = $key;
			$sKeyFilter .= "(" . $this->KeyFilter() . ")";
		}
		return $sKeyFilter;
	}

	// Load rows based on filter
	function &LoadRs($sFilter) {

		// Set up filter (SQL WHERE clause) and get return SQL
		//$this->CurrentFilter = $sFilter;
		//$sSql = $this->SQL();

		$sSql = $this->GetSQL($sFilter, "");
		$conn = &$this->Connection();
		$rs = $conn->Execute($sSql);
		return $rs;
	}

	// Load row values from recordset
	function LoadListRowValues(&$rs) {
		$this->CcoCodi->setDbValue($rs->fields('CcoCodi'));
		$this->VcoCodi->setDbValue($rs->fields('VcoCodi'));
		$this->CcoFPag->setDbValue($rs->fields('CcoFPag'));
		$this->CcoMont->setDbValue($rs->fields('CcoMont'));
		$this->CcoEsta->setDbValue($rs->fields('CcoEsta'));
		$this->CcoAnho->setDbValue($rs->fields('CcoAnho'));
		$this->CcoMes->setDbValue($rs->fields('CcoMes'));
		$this->CcoConc->setDbValue($rs->fields('CcoConc'));
		$this->CcoUsua->setDbValue($rs->fields('CcoUsua'));
		$this->CcoFCre->setDbValue($rs->fields('CcoFCre'));
	}

	// Render list row values
	function RenderListRow() {
		global $Security, $gsLanguage, $Language;

		// Call Row Rendering event
		$this->Row_Rendering();

   // Common render codes
		// CcoCodi
		// VcoCodi
		// CcoFPag
		// CcoMont
		// CcoEsta
		// CcoAnho
		// CcoMes
		// CcoConc
		// CcoUsua
		// CcoFCre
		// CcoCodi

		$this->CcoCodi->ViewValue = $this->CcoCodi->CurrentValue;
		$this->CcoCodi->ViewCustomAttributes = "";

		// VcoCodi
		if (strval($this->VcoCodi->CurrentValue) <> "") {
			$sFilterWrk = "\"VcoCodi\"" . ew_SearchString("=", $this->VcoCodi->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT \"VcoCodi\", \"VcoCodi\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"VCon\"";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->VcoCodi, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->VcoCodi->ViewValue = $this->VcoCodi->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->VcoCodi->ViewValue = $this->VcoCodi->CurrentValue;
			}
		} else {
			$this->VcoCodi->ViewValue = NULL;
		}
		$this->VcoCodi->ViewCustomAttributes = "";

		// CcoFPag
		$this->CcoFPag->ViewValue = $this->CcoFPag->CurrentValue;
		$this->CcoFPag->ViewValue = ew_FormatDateTime($this->CcoFPag->ViewValue, 7);
		$this->CcoFPag->ViewCustomAttributes = "";

		// CcoMont
		$this->CcoMont->ViewValue = $this->CcoMont->CurrentValue;
		$this->CcoMont->ViewCustomAttributes = "";

		// CcoEsta
		$this->CcoEsta->ViewValue = $this->CcoEsta->CurrentValue;
		$this->CcoEsta->ViewCustomAttributes = "";

		// CcoAnho
		$this->CcoAnho->ViewValue = $this->CcoAnho->CurrentValue;
		$this->CcoAnho->ViewCustomAttributes = "";

		// CcoMes
		$this->CcoMes->ViewValue = $this->CcoMes->CurrentValue;
		$this->CcoMes->ViewCustomAttributes = "";

		// CcoConc
		$this->CcoConc->ViewValue = $this->CcoConc->CurrentValue;
		$this->CcoConc->ViewCustomAttributes = "";

		// CcoUsua
		$this->CcoUsua->ViewValue = $this->CcoUsua->CurrentValue;
		$this->CcoUsua->ViewCustomAttributes = "";

		// CcoFCre
		$this->CcoFCre->ViewValue = $this->CcoFCre->CurrentValue;
		$this->CcoFCre->ViewValue = ew_FormatDateTime($this->CcoFCre->ViewValue, 7);
		$this->CcoFCre->ViewCustomAttributes = "";

		// CcoCodi
		$this->CcoCodi->LinkCustomAttributes = "";
		$this->CcoCodi->HrefValue = "";
		$this->CcoCodi->TooltipValue = "";

		// VcoCodi
		$this->VcoCodi->LinkCustomAttributes = "";
		$this->VcoCodi->HrefValue = "";
		$this->VcoCodi->TooltipValue = "";

		// CcoFPag
		$this->CcoFPag->LinkCustomAttributes = "";
		$this->CcoFPag->HrefValue = "";
		$this->CcoFPag->TooltipValue = "";

		// CcoMont
		$this->CcoMont->LinkCustomAttributes = "";
		$this->CcoMont->HrefValue = "";
		$this->CcoMont->TooltipValue = "";

		// CcoEsta
		$this->CcoEsta->LinkCustomAttributes = "";
		$this->CcoEsta->HrefValue = "";
		$this->CcoEsta->TooltipValue = "";

		// CcoAnho
		$this->CcoAnho->LinkCustomAttributes = "";
		$this->CcoAnho->HrefValue = "";
		$this->CcoAnho->TooltipValue = "";

		// CcoMes
		$this->CcoMes->LinkCustomAttributes = "";
		$this->CcoMes->HrefValue = "";
		$this->CcoMes->TooltipValue = "";

		// CcoConc
		$this->CcoConc->LinkCustomAttributes = "";
		$this->CcoConc->HrefValue = "";
		$this->CcoConc->TooltipValue = "";

		// CcoUsua
		$this->CcoUsua->LinkCustomAttributes = "";
		$this->CcoUsua->HrefValue = "";
		$this->CcoUsua->TooltipValue = "";

		// CcoFCre
		$this->CcoFCre->LinkCustomAttributes = "";
		$this->CcoFCre->HrefValue = "";
		$this->CcoFCre->TooltipValue = "";

		// Call Row Rendered event
		$this->Row_Rendered();
	}

	// Render edit row values
	function RenderEditRow() {
		global $Security, $gsLanguage, $Language;

		// Call Row Rendering event
		$this->Row_Rendering();

		// CcoCodi
		$this->CcoCodi->EditAttrs["class"] = "form-control";
		$this->CcoCodi->EditCustomAttributes = "";
		$this->CcoCodi->EditValue = $this->CcoCodi->CurrentValue;
		$this->CcoCodi->ViewCustomAttributes = "";

		// VcoCodi
		$this->VcoCodi->EditAttrs["class"] = "form-control";
		$this->VcoCodi->EditCustomAttributes = "";

		// CcoFPag
		$this->CcoFPag->EditAttrs["class"] = "form-control";
		$this->CcoFPag->EditCustomAttributes = "";
		$this->CcoFPag->EditValue = ew_FormatDateTime($this->CcoFPag->CurrentValue, 7);
		$this->CcoFPag->PlaceHolder = ew_RemoveHtml($this->CcoFPag->FldCaption());

		// CcoMont
		$this->CcoMont->EditAttrs["class"] = "form-control";
		$this->CcoMont->EditCustomAttributes = "";
		$this->CcoMont->EditValue = $this->CcoMont->CurrentValue;
		$this->CcoMont->PlaceHolder = ew_RemoveHtml($this->CcoMont->FldCaption());
		if (strval($this->CcoMont->EditValue) <> "" && is_numeric($this->CcoMont->EditValue)) $this->CcoMont->EditValue = ew_FormatNumber($this->CcoMont->EditValue, -2, -1, -2, 0);

		// CcoEsta
		$this->CcoEsta->EditAttrs["class"] = "form-control";
		$this->CcoEsta->EditCustomAttributes = "";
		$this->CcoEsta->EditValue = $this->CcoEsta->CurrentValue;
		$this->CcoEsta->PlaceHolder = ew_RemoveHtml($this->CcoEsta->FldCaption());

		// CcoAnho
		$this->CcoAnho->EditAttrs["class"] = "form-control";
		$this->CcoAnho->EditCustomAttributes = "";
		$this->CcoAnho->EditValue = $this->CcoAnho->CurrentValue;
		$this->CcoAnho->PlaceHolder = ew_RemoveHtml($this->CcoAnho->FldCaption());

		// CcoMes
		$this->CcoMes->EditAttrs["class"] = "form-control";
		$this->CcoMes->EditCustomAttributes = "";
		$this->CcoMes->EditValue = $this->CcoMes->CurrentValue;
		$this->CcoMes->PlaceHolder = ew_RemoveHtml($this->CcoMes->FldCaption());

		// CcoConc
		$this->CcoConc->EditAttrs["class"] = "form-control";
		$this->CcoConc->EditCustomAttributes = "";
		$this->CcoConc->EditValue = $this->CcoConc->CurrentValue;
		$this->CcoConc->PlaceHolder = ew_RemoveHtml($this->CcoConc->FldCaption());

		// CcoUsua
		$this->CcoUsua->EditAttrs["class"] = "form-control";
		$this->CcoUsua->EditCustomAttributes = "";
		$this->CcoUsua->EditValue = $this->CcoUsua->CurrentValue;
		$this->CcoUsua->PlaceHolder = ew_RemoveHtml($this->CcoUsua->FldCaption());

		// CcoFCre
		$this->CcoFCre->EditAttrs["class"] = "form-control";
		$this->CcoFCre->EditCustomAttributes = "";
		$this->CcoFCre->EditValue = ew_FormatDateTime($this->CcoFCre->CurrentValue, 7);
		$this->CcoFCre->PlaceHolder = ew_RemoveHtml($this->CcoFCre->FldCaption());

		// Call Row Rendered event
		$this->Row_Rendered();
	}

	// Aggregate list row values
	function AggregateListRowValues() {
	}

	// Aggregate list row (for rendering)
	function AggregateListRow() {

		// Call Row Rendered event
		$this->Row_Rendered();
	}
	var $ExportDoc;

	// Export data in HTML/CSV/Word/Excel/Email/PDF format
	function ExportDocument(&$Doc, &$Recordset, $StartRec, $StopRec, $ExportPageType = "") {
		if (!$Recordset || !$Doc)
			return;
		if (!$Doc->ExportCustom) {

			// Write header
			$Doc->ExportTableHeader();
			if ($Doc->Horizontal) { // Horizontal format, write header
				$Doc->BeginExportRow();
				if ($ExportPageType == "view") {
					if ($this->CcoCodi->Exportable) $Doc->ExportCaption($this->CcoCodi);
					if ($this->VcoCodi->Exportable) $Doc->ExportCaption($this->VcoCodi);
					if ($this->CcoFPag->Exportable) $Doc->ExportCaption($this->CcoFPag);
					if ($this->CcoMont->Exportable) $Doc->ExportCaption($this->CcoMont);
					if ($this->CcoEsta->Exportable) $Doc->ExportCaption($this->CcoEsta);
					if ($this->CcoAnho->Exportable) $Doc->ExportCaption($this->CcoAnho);
					if ($this->CcoMes->Exportable) $Doc->ExportCaption($this->CcoMes);
					if ($this->CcoConc->Exportable) $Doc->ExportCaption($this->CcoConc);
					if ($this->CcoUsua->Exportable) $Doc->ExportCaption($this->CcoUsua);
					if ($this->CcoFCre->Exportable) $Doc->ExportCaption($this->CcoFCre);
				} else {
					if ($this->CcoCodi->Exportable) $Doc->ExportCaption($this->CcoCodi);
					if ($this->VcoCodi->Exportable) $Doc->ExportCaption($this->VcoCodi);
					if ($this->CcoFPag->Exportable) $Doc->ExportCaption($this->CcoFPag);
					if ($this->CcoMont->Exportable) $Doc->ExportCaption($this->CcoMont);
					if ($this->CcoEsta->Exportable) $Doc->ExportCaption($this->CcoEsta);
					if ($this->CcoAnho->Exportable) $Doc->ExportCaption($this->CcoAnho);
					if ($this->CcoMes->Exportable) $Doc->ExportCaption($this->CcoMes);
					if ($this->CcoConc->Exportable) $Doc->ExportCaption($this->CcoConc);
					if ($this->CcoUsua->Exportable) $Doc->ExportCaption($this->CcoUsua);
					if ($this->CcoFCre->Exportable) $Doc->ExportCaption($this->CcoFCre);
				}
				$Doc->EndExportRow();
			}
		}

		// Move to first record
		$RecCnt = $StartRec - 1;
		if (!$Recordset->EOF) {
			$Recordset->MoveFirst();
			if ($StartRec > 1)
				$Recordset->Move($StartRec - 1);
		}
		while (!$Recordset->EOF && $RecCnt < $StopRec) {
			$RecCnt++;
			if (intval($RecCnt) >= intval($StartRec)) {
				$RowCnt = intval($RecCnt) - intval($StartRec) + 1;

				// Page break
				if ($this->ExportPageBreakCount > 0) {
					if ($RowCnt > 1 && ($RowCnt - 1) % $this->ExportPageBreakCount == 0)
						$Doc->ExportPageBreak();
				}
				$this->LoadListRowValues($Recordset);

				// Render row
				$this->RowType = EW_ROWTYPE_VIEW; // Render view
				$this->ResetAttrs();
				$this->RenderListRow();
				if (!$Doc->ExportCustom) {
					$Doc->BeginExportRow($RowCnt); // Allow CSS styles if enabled
					if ($ExportPageType == "view") {
						if ($this->CcoCodi->Exportable) $Doc->ExportField($this->CcoCodi);
						if ($this->VcoCodi->Exportable) $Doc->ExportField($this->VcoCodi);
						if ($this->CcoFPag->Exportable) $Doc->ExportField($this->CcoFPag);
						if ($this->CcoMont->Exportable) $Doc->ExportField($this->CcoMont);
						if ($this->CcoEsta->Exportable) $Doc->ExportField($this->CcoEsta);
						if ($this->CcoAnho->Exportable) $Doc->ExportField($this->CcoAnho);
						if ($this->CcoMes->Exportable) $Doc->ExportField($this->CcoMes);
						if ($this->CcoConc->Exportable) $Doc->ExportField($this->CcoConc);
						if ($this->CcoUsua->Exportable) $Doc->ExportField($this->CcoUsua);
						if ($this->CcoFCre->Exportable) $Doc->ExportField($this->CcoFCre);
					} else {
						if ($this->CcoCodi->Exportable) $Doc->ExportField($this->CcoCodi);
						if ($this->VcoCodi->Exportable) $Doc->ExportField($this->VcoCodi);
						if ($this->CcoFPag->Exportable) $Doc->ExportField($this->CcoFPag);
						if ($this->CcoMont->Exportable) $Doc->ExportField($this->CcoMont);
						if ($this->CcoEsta->Exportable) $Doc->ExportField($this->CcoEsta);
						if ($this->CcoAnho->Exportable) $Doc->ExportField($this->CcoAnho);
						if ($this->CcoMes->Exportable) $Doc->ExportField($this->CcoMes);
						if ($this->CcoConc->Exportable) $Doc->ExportField($this->CcoConc);
						if ($this->CcoUsua->Exportable) $Doc->ExportField($this->CcoUsua);
						if ($this->CcoFCre->Exportable) $Doc->ExportField($this->CcoFCre);
					}
					$Doc->EndExportRow();
				}
			}

			// Call Row Export server event
			if ($Doc->ExportCustom)
				$this->Row_Export($Recordset->fields);
			$Recordset->MoveNext();
		}
		if (!$Doc->ExportCustom) {
			$Doc->ExportTableFooter();
		}
	}

	// Get auto fill value
	function GetAutoFill($id, $val) {
		$rsarr = array();
		$rowcnt = 0;

		// Output
		if (is_array($rsarr) && $rowcnt > 0) {
			$fldcnt = count($rsarr[0]);
			for ($i = 0; $i < $rowcnt; $i++) {
				for ($j = 0; $j < $fldcnt; $j++) {
					$str = strval($rsarr[$i][$j]);
					$str = ew_ConvertToUtf8($str);
					if (isset($post["keepCRLF"])) {
						$str = str_replace(array("\r", "\n"), array("\\r", "\\n"), $str);
					} else {
						$str = str_replace(array("\r", "\n"), array(" ", " "), $str);
					}
					$rsarr[$i][$j] = $str;
				}
			}
			return ew_ArrayToJson($rsarr);
		} else {
			return FALSE;
		}
	}

	// Table level events
	// Recordset Selecting event
	function Recordset_Selecting(&$filter) {

		// Enter your code here	
	}

	// Recordset Selected event
	function Recordset_Selected(&$rs) {

		//echo "Recordset Selected";
	}

	// Recordset Search Validated event
	function Recordset_SearchValidated() {

		// Example:
		//$this->MyField1->AdvancedSearch->SearchValue = "your search criteria"; // Search value

	}

	// Recordset Searching event
	function Recordset_Searching(&$filter) {

		// Enter your code here	
	}

	// Row_Selecting event
	function Row_Selecting(&$filter) {

		// Enter your code here	
	}

	// Row Selected event
	function Row_Selected(&$rs) {

		//echo "Row Selected";
	}

	// Row Inserting event
	function Row_Inserting($rsold, &$rsnew) {

		// Enter your code here
		// To cancel, set return value to FALSE

		return TRUE;
	}

	// Row Inserted event
	function Row_Inserted($rsold, &$rsnew) {

		//echo "Row Inserted"
	}

	// Row Updating event
	function Row_Updating($rsold, &$rsnew) {

		// Enter your code here
		// To cancel, set return value to FALSE

		return TRUE;
	}

	// Row Updated event
	function Row_Updated($rsold, &$rsnew) {

		//echo "Row Updated";
	}

	// Row Update Conflict event
	function Row_UpdateConflict($rsold, &$rsnew) {

		// Enter your code here
		// To ignore conflict, set return value to FALSE

		return TRUE;
	}

	// Grid Inserting event
	function Grid_Inserting() {

		// Enter your code here
		// To reject grid insert, set return value to FALSE

		return TRUE;
	}

	// Grid Inserted event
	function Grid_Inserted($rsnew) {

		//echo "Grid Inserted";
	}

	// Grid Updating event
	function Grid_Updating($rsold) {

		// Enter your code here
		// To reject grid update, set return value to FALSE

		return TRUE;
	}

	// Grid Updated event
	function Grid_Updated($rsold, $rsnew) {

		//echo "Grid Updated";
	}

	// Row Deleting event
	function Row_Deleting(&$rs) {

		// Enter your code here
		// To cancel, set return value to False

		return TRUE;
	}

	// Row Deleted event
	function Row_Deleted(&$rs) {

		//echo "Row Deleted";
	}

	// Email Sending event
	function Email_Sending(&$Email, &$Args) {

		//var_dump($Email); var_dump($Args); exit();
		return TRUE;
	}

	// Lookup Selecting event
	function Lookup_Selecting($fld, &$filter) {

		//var_dump($fld->FldName, $fld->LookupFilters, $filter); // Uncomment to view the filter
		// Enter your code here

	}

	// Row Rendering event
	function Row_Rendering() {

		// Enter your code here	
	}

	// Row Rendered event
	function Row_Rendered() {

		// To view properties of field class, use:
		//var_dump($this-><FieldName>); 

	}

	// User ID Filtering event
	function UserID_Filtering(&$filter) {

		// Enter your code here
	}
}
?>
