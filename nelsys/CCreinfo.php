<?php

// Global variable for table object
$CCre = NULL;

//
// Table class for CCre
//
class cCCre extends cTable {
	var $CcrCodi;
	var $VcrCodi;
	var $CcrNCuo;
	var $CcrFPag;
	var $CcrMont;
	var $CcrMora;
	var $CcrDAtr;
	var $CcrEsta;
	var $CcrAnho;
	var $CcrMes;
	var $CcrConc;
	var $CcrUsua;
	var $CcrFCre;

	//
	// Table class constructor
	//
	function __construct() {
		global $Language;

		// Language object
		if (!isset($Language)) $Language = new cLanguage();
		$this->TableVar = 'CCre';
		$this->TableName = 'CCre';
		$this->TableType = 'TABLE';

		// Update Table
		$this->UpdateTable = "\"public\".\"CCre\"";
		$this->DBID = 'DB';
		$this->ExportAll = TRUE;
		$this->ExportPageBreakCount = 0; // Page break per every n record (PDF only)
		$this->ExportPageOrientation = "portrait"; // Page orientation (PDF only)
		$this->ExportPageSize = "a4"; // Page size (PDF only)
		$this->ExportExcelPageOrientation = ""; // Page orientation (PHPExcel only)
		$this->ExportExcelPageSize = ""; // Page size (PHPExcel only)
		$this->DetailAdd = TRUE; // Allow detail add
		$this->DetailEdit = TRUE; // Allow detail edit
		$this->DetailView = FALSE; // Allow detail view
		$this->ShowMultipleDetails = FALSE; // Show multiple details
		$this->GridAddRowCount = 5;
		$this->AllowAddDeleteRow = ew_AllowAddDeleteRow(); // Allow add/delete row
		$this->UserIDAllowSecurity = 0; // User ID Allow
		$this->BasicSearch = new cBasicSearch($this->TableVar);

		// CcrCodi
		$this->CcrCodi = new cField('CCre', 'CCre', 'x_CcrCodi', 'CcrCodi', '"CcrCodi"', 'CAST("CcrCodi" AS varchar(255))', 3, -1, FALSE, '"CcrCodi"', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'NO');
		$this->CcrCodi->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['CcrCodi'] = &$this->CcrCodi;

		// VcrCodi
		$this->VcrCodi = new cField('CCre', 'CCre', 'x_VcrCodi', 'VcrCodi', '"VcrCodi"', 'CAST("VcrCodi" AS varchar(255))', 3, -1, FALSE, '"VcrCodi"', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'SELECT');
		$this->VcrCodi->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['VcrCodi'] = &$this->VcrCodi;

		// CcrNCuo
		$this->CcrNCuo = new cField('CCre', 'CCre', 'x_CcrNCuo', 'CcrNCuo', '"CcrNCuo"', 'CAST("CcrNCuo" AS varchar(255))', 3, -1, FALSE, '"CcrNCuo"', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->CcrNCuo->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['CcrNCuo'] = &$this->CcrNCuo;

		// CcrFPag
		$this->CcrFPag = new cField('CCre', 'CCre', 'x_CcrFPag', 'CcrFPag', '"CcrFPag"', 'CAST("CcrFPag" AS varchar(255))', 133, 7, FALSE, '"CcrFPag"', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->CcrFPag->FldDefaultErrMsg = str_replace("%s", "/", $Language->Phrase("IncorrectDateDMY"));
		$this->fields['CcrFPag'] = &$this->CcrFPag;

		// CcrMont
		$this->CcrMont = new cField('CCre', 'CCre', 'x_CcrMont', 'CcrMont', '"CcrMont"', 'CAST("CcrMont" AS varchar(255))', 5, -1, FALSE, '"CcrMont"', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->CcrMont->FldDefaultErrMsg = $Language->Phrase("IncorrectFloat");
		$this->fields['CcrMont'] = &$this->CcrMont;

		// CcrMora
		$this->CcrMora = new cField('CCre', 'CCre', 'x_CcrMora', 'CcrMora', '"CcrMora"', 'CAST("CcrMora" AS varchar(255))', 5, -1, FALSE, '"CcrMora"', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->CcrMora->FldDefaultErrMsg = $Language->Phrase("IncorrectFloat");
		$this->fields['CcrMora'] = &$this->CcrMora;

		// CcrDAtr
		$this->CcrDAtr = new cField('CCre', 'CCre', 'x_CcrDAtr', 'CcrDAtr', '"CcrDAtr"', 'CAST("CcrDAtr" AS varchar(255))', 3, -1, FALSE, '"CcrDAtr"', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->CcrDAtr->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['CcrDAtr'] = &$this->CcrDAtr;

		// CcrEsta
		$this->CcrEsta = new cField('CCre', 'CCre', 'x_CcrEsta', 'CcrEsta', '"CcrEsta"', '"CcrEsta"', 200, -1, FALSE, '"CcrEsta"', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->fields['CcrEsta'] = &$this->CcrEsta;

		// CcrAnho
		$this->CcrAnho = new cField('CCre', 'CCre', 'x_CcrAnho', 'CcrAnho', '"CcrAnho"', '"CcrAnho"', 200, -1, FALSE, '"CcrAnho"', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->fields['CcrAnho'] = &$this->CcrAnho;

		// CcrMes
		$this->CcrMes = new cField('CCre', 'CCre', 'x_CcrMes', 'CcrMes', '"CcrMes"', '"CcrMes"', 200, -1, FALSE, '"CcrMes"', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->fields['CcrMes'] = &$this->CcrMes;

		// CcrConc
		$this->CcrConc = new cField('CCre', 'CCre', 'x_CcrConc', 'CcrConc', '"CcrConc"', '"CcrConc"', 200, -1, FALSE, '"CcrConc"', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->fields['CcrConc'] = &$this->CcrConc;

		// CcrUsua
		$this->CcrUsua = new cField('CCre', 'CCre', 'x_CcrUsua', 'CcrUsua', '"CcrUsua"', '"CcrUsua"', 200, -1, FALSE, '"CcrUsua"', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->fields['CcrUsua'] = &$this->CcrUsua;

		// CcrFCre
		$this->CcrFCre = new cField('CCre', 'CCre', 'x_CcrFCre', 'CcrFCre', '"CcrFCre"', 'CAST("CcrFCre" AS varchar(255))', 135, 7, FALSE, '"CcrFCre"', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->CcrFCre->FldDefaultErrMsg = str_replace("%s", "/", $Language->Phrase("IncorrectDateDMY"));
		$this->fields['CcrFCre'] = &$this->CcrFCre;
	}

	// Multiple column sort
	function UpdateSort(&$ofld, $ctrl) {
		if ($this->CurrentOrder == $ofld->FldName) {
			$sSortField = $ofld->FldExpression;
			$sLastSort = $ofld->getSort();
			if ($this->CurrentOrderType == "ASC" || $this->CurrentOrderType == "DESC") {
				$sThisSort = $this->CurrentOrderType;
			} else {
				$sThisSort = ($sLastSort == "ASC") ? "DESC" : "ASC";
			}
			$ofld->setSort($sThisSort);
			if ($ctrl) {
				$sOrderBy = $this->getSessionOrderBy();
				if (strpos($sOrderBy, $sSortField . " " . $sLastSort) !== FALSE) {
					$sOrderBy = str_replace($sSortField . " " . $sLastSort, $sSortField . " " . $sThisSort, $sOrderBy);
				} else {
					if ($sOrderBy <> "") $sOrderBy .= ", ";
					$sOrderBy .= $sSortField . " " . $sThisSort;
				}
				$this->setSessionOrderBy($sOrderBy); // Save to Session
			} else {
				$this->setSessionOrderBy($sSortField . " " . $sThisSort); // Save to Session
			}
		} else {
			if (!$ctrl) $ofld->setSort("");
		}
	}

	// Table level SQL
	var $_SqlFrom = "";

	function getSqlFrom() { // From
		return ($this->_SqlFrom <> "") ? $this->_SqlFrom : "\"public\".\"CCre\"";
	}

	function SqlFrom() { // For backward compatibility
    	return $this->getSqlFrom();
	}

	function setSqlFrom($v) {
    	$this->_SqlFrom = $v;
	}
	var $_SqlSelect = "";

	function getSqlSelect() { // Select
		return ($this->_SqlSelect <> "") ? $this->_SqlSelect : "SELECT * FROM " . $this->getSqlFrom();
	}

	function SqlSelect() { // For backward compatibility
    	return $this->getSqlSelect();
	}

	function setSqlSelect($v) {
    	$this->_SqlSelect = $v;
	}
	var $_SqlWhere = "";

	function getSqlWhere() { // Where
		$sWhere = ($this->_SqlWhere <> "") ? $this->_SqlWhere : "";
		$this->TableFilter = "";
		ew_AddFilter($sWhere, $this->TableFilter);
		return $sWhere;
	}

	function SqlWhere() { // For backward compatibility
    	return $this->getSqlWhere();
	}

	function setSqlWhere($v) {
    	$this->_SqlWhere = $v;
	}
	var $_SqlGroupBy = "";

	function getSqlGroupBy() { // Group By
		return ($this->_SqlGroupBy <> "") ? $this->_SqlGroupBy : "";
	}

	function SqlGroupBy() { // For backward compatibility
    	return $this->getSqlGroupBy();
	}

	function setSqlGroupBy($v) {
    	$this->_SqlGroupBy = $v;
	}
	var $_SqlHaving = "";

	function getSqlHaving() { // Having
		return ($this->_SqlHaving <> "") ? $this->_SqlHaving : "";
	}

	function SqlHaving() { // For backward compatibility
    	return $this->getSqlHaving();
	}

	function setSqlHaving($v) {
    	$this->_SqlHaving = $v;
	}
	var $_SqlOrderBy = "";

	function getSqlOrderBy() { // Order By
		return ($this->_SqlOrderBy <> "") ? $this->_SqlOrderBy : "";
	}

	function SqlOrderBy() { // For backward compatibility
    	return $this->getSqlOrderBy();
	}

	function setSqlOrderBy($v) {
    	$this->_SqlOrderBy = $v;
	}

	// Apply User ID filters
	function ApplyUserIDFilters($sFilter) {
		return $sFilter;
	}

	// Check if User ID security allows view all
	function UserIDAllow($id = "") {
		$allow = EW_USER_ID_ALLOW;
		switch ($id) {
			case "add":
			case "copy":
			case "gridadd":
			case "register":
			case "addopt":
				return (($allow & 1) == 1);
			case "edit":
			case "gridedit":
			case "update":
			case "changepwd":
			case "forgotpwd":
				return (($allow & 4) == 4);
			case "delete":
				return (($allow & 2) == 2);
			case "view":
				return (($allow & 32) == 32);
			case "search":
				return (($allow & 64) == 64);
			default:
				return (($allow & 8) == 8);
		}
	}

	// Get SQL
	function GetSQL($where, $orderby) {
		return ew_BuildSelectSql($this->getSqlSelect(), $this->getSqlWhere(),
			$this->getSqlGroupBy(), $this->getSqlHaving(), $this->getSqlOrderBy(),
			$where, $orderby);
	}

	// Table SQL
	function SQL() {
		$sFilter = $this->CurrentFilter;
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$sSort = $this->getSessionOrderBy();
		return ew_BuildSelectSql($this->getSqlSelect(), $this->getSqlWhere(),
			$this->getSqlGroupBy(), $this->getSqlHaving(), $this->getSqlOrderBy(),
			$sFilter, $sSort);
	}

	// Table SQL with List page filter
	function SelectSQL() {
		$sFilter = $this->getSessionWhere();
		ew_AddFilter($sFilter, $this->CurrentFilter);
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$this->Recordset_Selecting($sFilter);
		$sSort = $this->getSessionOrderBy();
		return ew_BuildSelectSql($this->getSqlSelect(), $this->getSqlWhere(), $this->getSqlGroupBy(),
			$this->getSqlHaving(), $this->getSqlOrderBy(), $sFilter, $sSort);
	}

	// Get ORDER BY clause
	function GetOrderBy() {
		$sSort = $this->getSessionOrderBy();
		return ew_BuildSelectSql("", "", "", "", $this->getSqlOrderBy(), "", $sSort);
	}

	// Try to get record count
	function TryGetRecordCount($sSql) {
		$cnt = -1;
		if (($this->TableType == 'TABLE' || $this->TableType == 'VIEW' || $this->TableType == 'LINKTABLE') && preg_match("/^SELECT \* FROM/i", $sSql)) {
			$sSql = "SELECT COUNT(*) FROM" . preg_replace('/^SELECT\s([\s\S]+)?\*\sFROM/i', "", $sSql);
			$sOrderBy = $this->GetOrderBy();
			if (substr($sSql, strlen($sOrderBy) * -1) == $sOrderBy)
				$sSql = substr($sSql, 0, strlen($sSql) - strlen($sOrderBy)); // Remove ORDER BY clause
		} else {
			$sSql = "SELECT COUNT(*) FROM (" . $sSql . ") EW_COUNT_TABLE";
		}
		$conn = &$this->Connection();
		if ($rs = $conn->Execute($sSql)) {
			if (!$rs->EOF && $rs->FieldCount() > 0) {
				$cnt = $rs->fields[0];
				$rs->Close();
			}
		}
		return intval($cnt);
	}

	// Get record count based on filter (for detail record count in master table pages)
	function LoadRecordCount($sFilter) {
		$origFilter = $this->CurrentFilter;
		$this->CurrentFilter = $sFilter;
		$this->Recordset_Selecting($this->CurrentFilter);

		//$sSql = $this->SQL();
		$sSql = $this->GetSQL($this->CurrentFilter, "");
		$cnt = $this->TryGetRecordCount($sSql);
		if ($cnt == -1) {
			if ($rs = $this->LoadRs($this->CurrentFilter)) {
				$cnt = $rs->RecordCount();
				$rs->Close();
			}
		}
		$this->CurrentFilter = $origFilter;
		return intval($cnt);
	}

	// Get record count (for current List page)
	function SelectRecordCount() {
		$sSql = $this->SelectSQL();
		$cnt = $this->TryGetRecordCount($sSql);
		if ($cnt == -1) {
			$conn = &$this->Connection();
			if ($rs = $conn->Execute($sSql)) {
				$cnt = $rs->RecordCount();
				$rs->Close();
			}
		}
		return intval($cnt);
	}

	// INSERT statement
	function InsertSQL(&$rs) {
		$names = "";
		$values = "";
		foreach ($rs as $name => $value) {
			if (!isset($this->fields[$name]) || $this->fields[$name]->FldIsCustom)
				continue;
			$names .= $this->fields[$name]->FldExpression . ",";
			$values .= ew_QuotedValue($value, $this->fields[$name]->FldDataType, $this->DBID) . ",";
		}
		while (substr($names, -1) == ",")
			$names = substr($names, 0, -1);
		while (substr($values, -1) == ",")
			$values = substr($values, 0, -1);
		return "INSERT INTO " . $this->UpdateTable . " ($names) VALUES ($values)";
	}

	// Insert
	function Insert(&$rs) {
		$conn = &$this->Connection();
		return $conn->Execute($this->InsertSQL($rs));
	}

	// UPDATE statement
	function UpdateSQL(&$rs, $where = "", $curfilter = TRUE) {
		$sql = "UPDATE " . $this->UpdateTable . " SET ";
		foreach ($rs as $name => $value) {
			if (!isset($this->fields[$name]) || $this->fields[$name]->FldIsCustom)
				continue;
			$sql .= $this->fields[$name]->FldExpression . "=";
			$sql .= ew_QuotedValue($value, $this->fields[$name]->FldDataType, $this->DBID) . ",";
		}
		while (substr($sql, -1) == ",")
			$sql = substr($sql, 0, -1);
		$filter = ($curfilter) ? $this->CurrentFilter : "";
		if (is_array($where))
			$where = $this->ArrayToFilter($where);
		ew_AddFilter($filter, $where);
		if ($filter <> "")	$sql .= " WHERE " . $filter;
		return $sql;
	}

	// Update
	function Update(&$rs, $where = "", $rsold = NULL, $curfilter = TRUE) {
		$conn = &$this->Connection();
		return $conn->Execute($this->UpdateSQL($rs, $where, $curfilter));
	}

	// DELETE statement
	function DeleteSQL(&$rs, $where = "", $curfilter = TRUE) {
		$sql = "DELETE FROM " . $this->UpdateTable . " WHERE ";
		if (is_array($where))
			$where = $this->ArrayToFilter($where);
		if ($rs) {
			if (array_key_exists('CcrCodi', $rs))
				ew_AddFilter($where, ew_QuotedName('CcrCodi', $this->DBID) . '=' . ew_QuotedValue($rs['CcrCodi'], $this->CcrCodi->FldDataType, $this->DBID));
		}
		$filter = ($curfilter) ? $this->CurrentFilter : "";
		ew_AddFilter($filter, $where);
		if ($filter <> "")
			$sql .= $filter;
		else
			$sql .= "0=1"; // Avoid delete
		return $sql;
	}

	// Delete
	function Delete(&$rs, $where = "", $curfilter = TRUE) {
		$conn = &$this->Connection();
		return $conn->Execute($this->DeleteSQL($rs, $where, $curfilter));
	}

	// Key filter WHERE clause
	function SqlKeyFilter() {
		return "\"CcrCodi\" = @CcrCodi@";
	}

	// Key filter
	function KeyFilter() {
		$sKeyFilter = $this->SqlKeyFilter();
		if (!is_numeric($this->CcrCodi->CurrentValue))
			$sKeyFilter = "0=1"; // Invalid key
		$sKeyFilter = str_replace("@CcrCodi@", ew_AdjustSql($this->CcrCodi->CurrentValue, $this->DBID), $sKeyFilter); // Replace key value
		return $sKeyFilter;
	}

	// Return page URL
	function getReturnUrl() {
		$name = EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_RETURN_URL;

		// Get referer URL automatically
		if (ew_ServerVar("HTTP_REFERER") <> "" && ew_ReferPage() <> ew_CurrentPage() && ew_ReferPage() <> "login.php") // Referer not same page or login page
			$_SESSION[$name] = ew_ServerVar("HTTP_REFERER"); // Save to Session
		if (@$_SESSION[$name] <> "") {
			return $_SESSION[$name];
		} else {
			return "CCrelist.php";
		}
	}

	function setReturnUrl($v) {
		$_SESSION[EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_RETURN_URL] = $v;
	}

	// List URL
	function GetListUrl() {
		return "CCrelist.php";
	}

	// View URL
	function GetViewUrl($parm = "") {
		if ($parm <> "")
			$url = $this->KeyUrl("CCreview.php", $this->UrlParm($parm));
		else
			$url = $this->KeyUrl("CCreview.php", $this->UrlParm(EW_TABLE_SHOW_DETAIL . "="));
		return $this->AddMasterUrl($url);
	}

	// Add URL
	function GetAddUrl($parm = "") {
		if ($parm <> "")
			$url = "CCreadd.php?" . $this->UrlParm($parm);
		else
			$url = "CCreadd.php";
		return $this->AddMasterUrl($url);
	}

	// Edit URL
	function GetEditUrl($parm = "") {
		$url = $this->KeyUrl("CCreedit.php", $this->UrlParm($parm));
		return $this->AddMasterUrl($url);
	}

	// Inline edit URL
	function GetInlineEditUrl() {
		$url = $this->KeyUrl(ew_CurrentPage(), $this->UrlParm("a=edit"));
		return $this->AddMasterUrl($url);
	}

	// Copy URL
	function GetCopyUrl($parm = "") {
		$url = $this->KeyUrl("CCreadd.php", $this->UrlParm($parm));
		return $this->AddMasterUrl($url);
	}

	// Inline copy URL
	function GetInlineCopyUrl() {
		$url = $this->KeyUrl(ew_CurrentPage(), $this->UrlParm("a=copy"));
		return $this->AddMasterUrl($url);
	}

	// Delete URL
	function GetDeleteUrl() {
		return $this->KeyUrl("CCredelete.php", $this->UrlParm());
	}

	// Add master url
	function AddMasterUrl($url) {
		return $url;
	}

	function KeyToJson() {
		$json = "";
		$json .= "CcrCodi:" . ew_VarToJson($this->CcrCodi->CurrentValue, "number", "'");
		return "{" . $json . "}";
	}

	// Add key value to URL
	function KeyUrl($url, $parm = "") {
		$sUrl = $url . "?";
		if ($parm <> "") $sUrl .= $parm . "&";
		if (!is_null($this->CcrCodi->CurrentValue)) {
			$sUrl .= "CcrCodi=" . urlencode($this->CcrCodi->CurrentValue);
		} else {
			return "javascript:ew_Alert(ewLanguage.Phrase('InvalidRecord'));";
		}
		return $sUrl;
	}

	// Sort URL
	function SortUrl(&$fld) {
		if ($this->CurrentAction <> "" || $this->Export <> "" ||
			in_array($fld->FldType, array(128, 204, 205))) { // Unsortable data type
				return "";
		} elseif ($fld->Sortable) {
			$sUrlParm = $this->UrlParm("order=" . urlencode($fld->FldName) . "&amp;ordertype=" . $fld->ReverseSort());
			return ew_CurrentPage() . "?" . $sUrlParm;
		} else {
			return "";
		}
	}

	// Get record keys from $_POST/$_GET/$_SESSION
	function GetRecordKeys() {
		global $EW_COMPOSITE_KEY_SEPARATOR;
		$arKeys = array();
		$arKey = array();
		if (isset($_POST["key_m"])) {
			$arKeys = ew_StripSlashes($_POST["key_m"]);
			$cnt = count($arKeys);
		} elseif (isset($_GET["key_m"])) {
			$arKeys = ew_StripSlashes($_GET["key_m"]);
			$cnt = count($arKeys);
		} elseif (!empty($_GET) || !empty($_POST)) {
			$isPost = ew_IsHttpPost();
			$arKeys[] = $isPost ? ew_StripSlashes(@$_POST["CcrCodi"]) : ew_StripSlashes(@$_GET["CcrCodi"]); // CcrCodi

			//return $arKeys; // Do not return yet, so the values will also be checked by the following code
		}

		// Check keys
		$ar = array();
		foreach ($arKeys as $key) {
			if (!is_numeric($key))
				continue;
			$ar[] = $key;
		}
		return $ar;
	}

	// Get key filter
	function GetKeyFilter() {
		$arKeys = $this->GetRecordKeys();
		$sKeyFilter = "";
		foreach ($arKeys as $key) {
			if ($sKeyFilter <> "") $sKeyFilter .= " OR ";
			$this->CcrCodi->CurrentValue = $key;
			$sKeyFilter .= "(" . $this->KeyFilter() . ")";
		}
		return $sKeyFilter;
	}

	// Load rows based on filter
	function &LoadRs($sFilter) {

		// Set up filter (SQL WHERE clause) and get return SQL
		//$this->CurrentFilter = $sFilter;
		//$sSql = $this->SQL();

		$sSql = $this->GetSQL($sFilter, "");
		$conn = &$this->Connection();
		$rs = $conn->Execute($sSql);
		return $rs;
	}

	// Load row values from recordset
	function LoadListRowValues(&$rs) {
		$this->CcrCodi->setDbValue($rs->fields('CcrCodi'));
		$this->VcrCodi->setDbValue($rs->fields('VcrCodi'));
		$this->CcrNCuo->setDbValue($rs->fields('CcrNCuo'));
		$this->CcrFPag->setDbValue($rs->fields('CcrFPag'));
		$this->CcrMont->setDbValue($rs->fields('CcrMont'));
		$this->CcrMora->setDbValue($rs->fields('CcrMora'));
		$this->CcrDAtr->setDbValue($rs->fields('CcrDAtr'));
		$this->CcrEsta->setDbValue($rs->fields('CcrEsta'));
		$this->CcrAnho->setDbValue($rs->fields('CcrAnho'));
		$this->CcrMes->setDbValue($rs->fields('CcrMes'));
		$this->CcrConc->setDbValue($rs->fields('CcrConc'));
		$this->CcrUsua->setDbValue($rs->fields('CcrUsua'));
		$this->CcrFCre->setDbValue($rs->fields('CcrFCre'));
	}

	// Render list row values
	function RenderListRow() {
		global $Security, $gsLanguage, $Language;

		// Call Row Rendering event
		$this->Row_Rendering();

   // Common render codes
		// CcrCodi
		// VcrCodi
		// CcrNCuo
		// CcrFPag
		// CcrMont
		// CcrMora
		// CcrDAtr
		// CcrEsta
		// CcrAnho
		// CcrMes
		// CcrConc
		// CcrUsua
		// CcrFCre
		// CcrCodi

		$this->CcrCodi->ViewValue = $this->CcrCodi->CurrentValue;
		$this->CcrCodi->ViewCustomAttributes = "";

		// VcrCodi
		if (strval($this->VcrCodi->CurrentValue) <> "") {
			$sFilterWrk = "\"VcrCodi\"" . ew_SearchString("=", $this->VcrCodi->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT \"VcrCodi\", \"VcrCodi\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"VCre\"";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->VcrCodi, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->VcrCodi->ViewValue = $this->VcrCodi->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->VcrCodi->ViewValue = $this->VcrCodi->CurrentValue;
			}
		} else {
			$this->VcrCodi->ViewValue = NULL;
		}
		$this->VcrCodi->ViewCustomAttributes = "";

		// CcrNCuo
		$this->CcrNCuo->ViewValue = $this->CcrNCuo->CurrentValue;
		$this->CcrNCuo->ViewCustomAttributes = "";

		// CcrFPag
		$this->CcrFPag->ViewValue = $this->CcrFPag->CurrentValue;
		$this->CcrFPag->ViewValue = ew_FormatDateTime($this->CcrFPag->ViewValue, 7);
		$this->CcrFPag->ViewCustomAttributes = "";

		// CcrMont
		$this->CcrMont->ViewValue = $this->CcrMont->CurrentValue;
		$this->CcrMont->ViewCustomAttributes = "";

		// CcrMora
		$this->CcrMora->ViewValue = $this->CcrMora->CurrentValue;
		$this->CcrMora->ViewCustomAttributes = "";

		// CcrDAtr
		$this->CcrDAtr->ViewValue = $this->CcrDAtr->CurrentValue;
		$this->CcrDAtr->ViewCustomAttributes = "";

		// CcrEsta
		$this->CcrEsta->ViewValue = $this->CcrEsta->CurrentValue;
		$this->CcrEsta->ViewCustomAttributes = "";

		// CcrAnho
		$this->CcrAnho->ViewValue = $this->CcrAnho->CurrentValue;
		$this->CcrAnho->ViewCustomAttributes = "";

		// CcrMes
		$this->CcrMes->ViewValue = $this->CcrMes->CurrentValue;
		$this->CcrMes->ViewCustomAttributes = "";

		// CcrConc
		$this->CcrConc->ViewValue = $this->CcrConc->CurrentValue;
		$this->CcrConc->ViewCustomAttributes = "";

		// CcrUsua
		$this->CcrUsua->ViewValue = $this->CcrUsua->CurrentValue;
		$this->CcrUsua->ViewCustomAttributes = "";

		// CcrFCre
		$this->CcrFCre->ViewValue = $this->CcrFCre->CurrentValue;
		$this->CcrFCre->ViewValue = ew_FormatDateTime($this->CcrFCre->ViewValue, 7);
		$this->CcrFCre->ViewCustomAttributes = "";

		// CcrCodi
		$this->CcrCodi->LinkCustomAttributes = "";
		$this->CcrCodi->HrefValue = "";
		$this->CcrCodi->TooltipValue = "";

		// VcrCodi
		$this->VcrCodi->LinkCustomAttributes = "";
		$this->VcrCodi->HrefValue = "";
		$this->VcrCodi->TooltipValue = "";

		// CcrNCuo
		$this->CcrNCuo->LinkCustomAttributes = "";
		$this->CcrNCuo->HrefValue = "";
		$this->CcrNCuo->TooltipValue = "";

		// CcrFPag
		$this->CcrFPag->LinkCustomAttributes = "";
		$this->CcrFPag->HrefValue = "";
		$this->CcrFPag->TooltipValue = "";

		// CcrMont
		$this->CcrMont->LinkCustomAttributes = "";
		$this->CcrMont->HrefValue = "";
		$this->CcrMont->TooltipValue = "";

		// CcrMora
		$this->CcrMora->LinkCustomAttributes = "";
		$this->CcrMora->HrefValue = "";
		$this->CcrMora->TooltipValue = "";

		// CcrDAtr
		$this->CcrDAtr->LinkCustomAttributes = "";
		$this->CcrDAtr->HrefValue = "";
		$this->CcrDAtr->TooltipValue = "";

		// CcrEsta
		$this->CcrEsta->LinkCustomAttributes = "";
		$this->CcrEsta->HrefValue = "";
		$this->CcrEsta->TooltipValue = "";

		// CcrAnho
		$this->CcrAnho->LinkCustomAttributes = "";
		$this->CcrAnho->HrefValue = "";
		$this->CcrAnho->TooltipValue = "";

		// CcrMes
		$this->CcrMes->LinkCustomAttributes = "";
		$this->CcrMes->HrefValue = "";
		$this->CcrMes->TooltipValue = "";

		// CcrConc
		$this->CcrConc->LinkCustomAttributes = "";
		$this->CcrConc->HrefValue = "";
		$this->CcrConc->TooltipValue = "";

		// CcrUsua
		$this->CcrUsua->LinkCustomAttributes = "";
		$this->CcrUsua->HrefValue = "";
		$this->CcrUsua->TooltipValue = "";

		// CcrFCre
		$this->CcrFCre->LinkCustomAttributes = "";
		$this->CcrFCre->HrefValue = "";
		$this->CcrFCre->TooltipValue = "";

		// Call Row Rendered event
		$this->Row_Rendered();
	}

	// Render edit row values
	function RenderEditRow() {
		global $Security, $gsLanguage, $Language;

		// Call Row Rendering event
		$this->Row_Rendering();

		// CcrCodi
		$this->CcrCodi->EditAttrs["class"] = "form-control";
		$this->CcrCodi->EditCustomAttributes = "";
		$this->CcrCodi->EditValue = $this->CcrCodi->CurrentValue;
		$this->CcrCodi->ViewCustomAttributes = "";

		// VcrCodi
		$this->VcrCodi->EditAttrs["class"] = "form-control";
		$this->VcrCodi->EditCustomAttributes = "";

		// CcrNCuo
		$this->CcrNCuo->EditAttrs["class"] = "form-control";
		$this->CcrNCuo->EditCustomAttributes = "";
		$this->CcrNCuo->EditValue = $this->CcrNCuo->CurrentValue;
		$this->CcrNCuo->PlaceHolder = ew_RemoveHtml($this->CcrNCuo->FldCaption());

		// CcrFPag
		$this->CcrFPag->EditAttrs["class"] = "form-control";
		$this->CcrFPag->EditCustomAttributes = "";
		$this->CcrFPag->EditValue = ew_FormatDateTime($this->CcrFPag->CurrentValue, 7);
		$this->CcrFPag->PlaceHolder = ew_RemoveHtml($this->CcrFPag->FldCaption());

		// CcrMont
		$this->CcrMont->EditAttrs["class"] = "form-control";
		$this->CcrMont->EditCustomAttributes = "";
		$this->CcrMont->EditValue = $this->CcrMont->CurrentValue;
		$this->CcrMont->PlaceHolder = ew_RemoveHtml($this->CcrMont->FldCaption());
		if (strval($this->CcrMont->EditValue) <> "" && is_numeric($this->CcrMont->EditValue)) $this->CcrMont->EditValue = ew_FormatNumber($this->CcrMont->EditValue, -2, -1, -2, 0);

		// CcrMora
		$this->CcrMora->EditAttrs["class"] = "form-control";
		$this->CcrMora->EditCustomAttributes = "";
		$this->CcrMora->EditValue = $this->CcrMora->CurrentValue;
		$this->CcrMora->PlaceHolder = ew_RemoveHtml($this->CcrMora->FldCaption());
		if (strval($this->CcrMora->EditValue) <> "" && is_numeric($this->CcrMora->EditValue)) $this->CcrMora->EditValue = ew_FormatNumber($this->CcrMora->EditValue, -2, -1, -2, 0);

		// CcrDAtr
		$this->CcrDAtr->EditAttrs["class"] = "form-control";
		$this->CcrDAtr->EditCustomAttributes = "";
		$this->CcrDAtr->EditValue = $this->CcrDAtr->CurrentValue;
		$this->CcrDAtr->PlaceHolder = ew_RemoveHtml($this->CcrDAtr->FldCaption());

		// CcrEsta
		$this->CcrEsta->EditAttrs["class"] = "form-control";
		$this->CcrEsta->EditCustomAttributes = "";
		$this->CcrEsta->EditValue = $this->CcrEsta->CurrentValue;
		$this->CcrEsta->PlaceHolder = ew_RemoveHtml($this->CcrEsta->FldCaption());

		// CcrAnho
		$this->CcrAnho->EditAttrs["class"] = "form-control";
		$this->CcrAnho->EditCustomAttributes = "";
		$this->CcrAnho->EditValue = $this->CcrAnho->CurrentValue;
		$this->CcrAnho->PlaceHolder = ew_RemoveHtml($this->CcrAnho->FldCaption());

		// CcrMes
		$this->CcrMes->EditAttrs["class"] = "form-control";
		$this->CcrMes->EditCustomAttributes = "";
		$this->CcrMes->EditValue = $this->CcrMes->CurrentValue;
		$this->CcrMes->PlaceHolder = ew_RemoveHtml($this->CcrMes->FldCaption());

		// CcrConc
		$this->CcrConc->EditAttrs["class"] = "form-control";
		$this->CcrConc->EditCustomAttributes = "";
		$this->CcrConc->EditValue = $this->CcrConc->CurrentValue;
		$this->CcrConc->PlaceHolder = ew_RemoveHtml($this->CcrConc->FldCaption());

		// CcrUsua
		$this->CcrUsua->EditAttrs["class"] = "form-control";
		$this->CcrUsua->EditCustomAttributes = "";
		$this->CcrUsua->EditValue = $this->CcrUsua->CurrentValue;
		$this->CcrUsua->PlaceHolder = ew_RemoveHtml($this->CcrUsua->FldCaption());

		// CcrFCre
		$this->CcrFCre->EditAttrs["class"] = "form-control";
		$this->CcrFCre->EditCustomAttributes = "";
		$this->CcrFCre->EditValue = ew_FormatDateTime($this->CcrFCre->CurrentValue, 7);
		$this->CcrFCre->PlaceHolder = ew_RemoveHtml($this->CcrFCre->FldCaption());

		// Call Row Rendered event
		$this->Row_Rendered();
	}

	// Aggregate list row values
	function AggregateListRowValues() {
	}

	// Aggregate list row (for rendering)
	function AggregateListRow() {

		// Call Row Rendered event
		$this->Row_Rendered();
	}
	var $ExportDoc;

	// Export data in HTML/CSV/Word/Excel/Email/PDF format
	function ExportDocument(&$Doc, &$Recordset, $StartRec, $StopRec, $ExportPageType = "") {
		if (!$Recordset || !$Doc)
			return;
		if (!$Doc->ExportCustom) {

			// Write header
			$Doc->ExportTableHeader();
			if ($Doc->Horizontal) { // Horizontal format, write header
				$Doc->BeginExportRow();
				if ($ExportPageType == "view") {
					if ($this->CcrCodi->Exportable) $Doc->ExportCaption($this->CcrCodi);
					if ($this->VcrCodi->Exportable) $Doc->ExportCaption($this->VcrCodi);
					if ($this->CcrNCuo->Exportable) $Doc->ExportCaption($this->CcrNCuo);
					if ($this->CcrFPag->Exportable) $Doc->ExportCaption($this->CcrFPag);
					if ($this->CcrMont->Exportable) $Doc->ExportCaption($this->CcrMont);
					if ($this->CcrMora->Exportable) $Doc->ExportCaption($this->CcrMora);
					if ($this->CcrDAtr->Exportable) $Doc->ExportCaption($this->CcrDAtr);
					if ($this->CcrEsta->Exportable) $Doc->ExportCaption($this->CcrEsta);
					if ($this->CcrAnho->Exportable) $Doc->ExportCaption($this->CcrAnho);
					if ($this->CcrMes->Exportable) $Doc->ExportCaption($this->CcrMes);
					if ($this->CcrConc->Exportable) $Doc->ExportCaption($this->CcrConc);
					if ($this->CcrUsua->Exportable) $Doc->ExportCaption($this->CcrUsua);
					if ($this->CcrFCre->Exportable) $Doc->ExportCaption($this->CcrFCre);
				} else {
					if ($this->CcrCodi->Exportable) $Doc->ExportCaption($this->CcrCodi);
					if ($this->VcrCodi->Exportable) $Doc->ExportCaption($this->VcrCodi);
					if ($this->CcrNCuo->Exportable) $Doc->ExportCaption($this->CcrNCuo);
					if ($this->CcrFPag->Exportable) $Doc->ExportCaption($this->CcrFPag);
					if ($this->CcrMont->Exportable) $Doc->ExportCaption($this->CcrMont);
					if ($this->CcrMora->Exportable) $Doc->ExportCaption($this->CcrMora);
					if ($this->CcrDAtr->Exportable) $Doc->ExportCaption($this->CcrDAtr);
					if ($this->CcrEsta->Exportable) $Doc->ExportCaption($this->CcrEsta);
					if ($this->CcrAnho->Exportable) $Doc->ExportCaption($this->CcrAnho);
					if ($this->CcrMes->Exportable) $Doc->ExportCaption($this->CcrMes);
					if ($this->CcrConc->Exportable) $Doc->ExportCaption($this->CcrConc);
					if ($this->CcrUsua->Exportable) $Doc->ExportCaption($this->CcrUsua);
					if ($this->CcrFCre->Exportable) $Doc->ExportCaption($this->CcrFCre);
				}
				$Doc->EndExportRow();
			}
		}

		// Move to first record
		$RecCnt = $StartRec - 1;
		if (!$Recordset->EOF) {
			$Recordset->MoveFirst();
			if ($StartRec > 1)
				$Recordset->Move($StartRec - 1);
		}
		while (!$Recordset->EOF && $RecCnt < $StopRec) {
			$RecCnt++;
			if (intval($RecCnt) >= intval($StartRec)) {
				$RowCnt = intval($RecCnt) - intval($StartRec) + 1;

				// Page break
				if ($this->ExportPageBreakCount > 0) {
					if ($RowCnt > 1 && ($RowCnt - 1) % $this->ExportPageBreakCount == 0)
						$Doc->ExportPageBreak();
				}
				$this->LoadListRowValues($Recordset);

				// Render row
				$this->RowType = EW_ROWTYPE_VIEW; // Render view
				$this->ResetAttrs();
				$this->RenderListRow();
				if (!$Doc->ExportCustom) {
					$Doc->BeginExportRow($RowCnt); // Allow CSS styles if enabled
					if ($ExportPageType == "view") {
						if ($this->CcrCodi->Exportable) $Doc->ExportField($this->CcrCodi);
						if ($this->VcrCodi->Exportable) $Doc->ExportField($this->VcrCodi);
						if ($this->CcrNCuo->Exportable) $Doc->ExportField($this->CcrNCuo);
						if ($this->CcrFPag->Exportable) $Doc->ExportField($this->CcrFPag);
						if ($this->CcrMont->Exportable) $Doc->ExportField($this->CcrMont);
						if ($this->CcrMora->Exportable) $Doc->ExportField($this->CcrMora);
						if ($this->CcrDAtr->Exportable) $Doc->ExportField($this->CcrDAtr);
						if ($this->CcrEsta->Exportable) $Doc->ExportField($this->CcrEsta);
						if ($this->CcrAnho->Exportable) $Doc->ExportField($this->CcrAnho);
						if ($this->CcrMes->Exportable) $Doc->ExportField($this->CcrMes);
						if ($this->CcrConc->Exportable) $Doc->ExportField($this->CcrConc);
						if ($this->CcrUsua->Exportable) $Doc->ExportField($this->CcrUsua);
						if ($this->CcrFCre->Exportable) $Doc->ExportField($this->CcrFCre);
					} else {
						if ($this->CcrCodi->Exportable) $Doc->ExportField($this->CcrCodi);
						if ($this->VcrCodi->Exportable) $Doc->ExportField($this->VcrCodi);
						if ($this->CcrNCuo->Exportable) $Doc->ExportField($this->CcrNCuo);
						if ($this->CcrFPag->Exportable) $Doc->ExportField($this->CcrFPag);
						if ($this->CcrMont->Exportable) $Doc->ExportField($this->CcrMont);
						if ($this->CcrMora->Exportable) $Doc->ExportField($this->CcrMora);
						if ($this->CcrDAtr->Exportable) $Doc->ExportField($this->CcrDAtr);
						if ($this->CcrEsta->Exportable) $Doc->ExportField($this->CcrEsta);
						if ($this->CcrAnho->Exportable) $Doc->ExportField($this->CcrAnho);
						if ($this->CcrMes->Exportable) $Doc->ExportField($this->CcrMes);
						if ($this->CcrConc->Exportable) $Doc->ExportField($this->CcrConc);
						if ($this->CcrUsua->Exportable) $Doc->ExportField($this->CcrUsua);
						if ($this->CcrFCre->Exportable) $Doc->ExportField($this->CcrFCre);
					}
					$Doc->EndExportRow();
				}
			}

			// Call Row Export server event
			if ($Doc->ExportCustom)
				$this->Row_Export($Recordset->fields);
			$Recordset->MoveNext();
		}
		if (!$Doc->ExportCustom) {
			$Doc->ExportTableFooter();
		}
	}

	// Get auto fill value
	function GetAutoFill($id, $val) {
		$rsarr = array();
		$rowcnt = 0;

		// Output
		if (is_array($rsarr) && $rowcnt > 0) {
			$fldcnt = count($rsarr[0]);
			for ($i = 0; $i < $rowcnt; $i++) {
				for ($j = 0; $j < $fldcnt; $j++) {
					$str = strval($rsarr[$i][$j]);
					$str = ew_ConvertToUtf8($str);
					if (isset($post["keepCRLF"])) {
						$str = str_replace(array("\r", "\n"), array("\\r", "\\n"), $str);
					} else {
						$str = str_replace(array("\r", "\n"), array(" ", " "), $str);
					}
					$rsarr[$i][$j] = $str;
				}
			}
			return ew_ArrayToJson($rsarr);
		} else {
			return FALSE;
		}
	}

	// Table level events
	// Recordset Selecting event
	function Recordset_Selecting(&$filter) {

		// Enter your code here	
	}

	// Recordset Selected event
	function Recordset_Selected(&$rs) {

		//echo "Recordset Selected";
	}

	// Recordset Search Validated event
	function Recordset_SearchValidated() {

		// Example:
		//$this->MyField1->AdvancedSearch->SearchValue = "your search criteria"; // Search value

	}

	// Recordset Searching event
	function Recordset_Searching(&$filter) {

		// Enter your code here	
	}

	// Row_Selecting event
	function Row_Selecting(&$filter) {

		// Enter your code here	
	}

	// Row Selected event
	function Row_Selected(&$rs) {

		//echo "Row Selected";
	}

	// Row Inserting event
	function Row_Inserting($rsold, &$rsnew) {

		// Enter your code here
		// To cancel, set return value to FALSE

		return TRUE;
	}

	// Row Inserted event
	function Row_Inserted($rsold, &$rsnew) {

		//echo "Row Inserted"
	}

	// Row Updating event
	function Row_Updating($rsold, &$rsnew) {

		// Enter your code here
		// To cancel, set return value to FALSE

		return TRUE;
	}

	// Row Updated event
	function Row_Updated($rsold, &$rsnew) {

		//echo "Row Updated";
	}

	// Row Update Conflict event
	function Row_UpdateConflict($rsold, &$rsnew) {

		// Enter your code here
		// To ignore conflict, set return value to FALSE

		return TRUE;
	}

	// Grid Inserting event
	function Grid_Inserting() {

		// Enter your code here
		// To reject grid insert, set return value to FALSE

		return TRUE;
	}

	// Grid Inserted event
	function Grid_Inserted($rsnew) {

		//echo "Grid Inserted";
	}

	// Grid Updating event
	function Grid_Updating($rsold) {

		// Enter your code here
		// To reject grid update, set return value to FALSE

		return TRUE;
	}

	// Grid Updated event
	function Grid_Updated($rsold, $rsnew) {

		//echo "Grid Updated";
	}

	// Row Deleting event
	function Row_Deleting(&$rs) {

		// Enter your code here
		// To cancel, set return value to False

		return TRUE;
	}

	// Row Deleted event
	function Row_Deleted(&$rs) {

		//echo "Row Deleted";
	}

	// Email Sending event
	function Email_Sending(&$Email, &$Args) {

		//var_dump($Email); var_dump($Args); exit();
		return TRUE;
	}

	// Lookup Selecting event
	function Lookup_Selecting($fld, &$filter) {

		//var_dump($fld->FldName, $fld->LookupFilters, $filter); // Uncomment to view the filter
		// Enter your code here

	}

	// Row Rendering event
	function Row_Rendering() {

		// Enter your code here	
	}

	// Row Rendered event
	function Row_Rendered() {

		// To view properties of field class, use:
		//var_dump($this-><FieldName>); 

	}

	// User ID Filtering event
	function UserID_Filtering(&$filter) {

		// Enter your code here
	}
}
?>
