<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "Clieinfo.php" ?>
<?php include_once "Usuainfo.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$Clie_add = NULL; // Initialize page object first

class cClie_add extends cClie {

	// Page ID
	var $PageID = 'add';

	// Project ID
	var $ProjectID = "{04439FF7-B43F-460F-8514-F71C8FF9E679}";

	// Table name
	var $TableName = 'Clie';

	// Page object name
	var $PageObjName = 'Clie_add';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (Clie)
		if (!isset($GLOBALS["Clie"]) || get_class($GLOBALS["Clie"]) == "cClie") {
			$GLOBALS["Clie"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["Clie"];
		}

		// Table object (Usua)
		if (!isset($GLOBALS['Usua'])) $GLOBALS['Usua'] = new cUsua();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'add', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'Clie', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (Usua)
		if (!isset($UserTable)) {
			$UserTable = new cUsua();
			$UserTableConn = Conn($UserTable->DBID);
		}
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanAdd()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage($Language->Phrase("NoPermission")); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("Clielist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}

		// Create form object
		$objForm = new cFormObj();
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Process auto fill
		if (@$_POST["ajax"] == "autofill") {
			$results = $this->GetAutoFill(@$_POST["name"], @$_POST["q"]);
			if ($results) {

				// Clean output buffer
				if (!EW_DEBUG_ENABLED && ob_get_length())
					ob_end_clean();
				echo $results;
				$this->Page_Terminate();
				exit();
			}
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $Clie;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($Clie);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $FormClassName = "form-horizontal ewForm ewAddForm";
	var $DbMasterFilter = "";
	var $DbDetailFilter = "";
	var $StartRec;
	var $Priv = 0;
	var $OldRecordset;
	var $CopyRecord;

	// 
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError;

		// Process form if post back
		if (@$_POST["a_add"] <> "") {
			$this->CurrentAction = $_POST["a_add"]; // Get form action
			$this->CopyRecord = $this->LoadOldRecord(); // Load old recordset
			$this->LoadFormValues(); // Load form values
		} else { // Not post back

			// Load key values from QueryString
			$this->CopyRecord = TRUE;
			if (@$_GET["CliCodi"] != "") {
				$this->CliCodi->setQueryStringValue($_GET["CliCodi"]);
				$this->setKey("CliCodi", $this->CliCodi->CurrentValue); // Set up key
			} else {
				$this->setKey("CliCodi", ""); // Clear key
				$this->CopyRecord = FALSE;
			}
			if ($this->CopyRecord) {
				$this->CurrentAction = "C"; // Copy record
			} else {
				$this->CurrentAction = "I"; // Display blank record
				$this->LoadDefaultValues(); // Load default values
			}
		}

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Validate form if post back
		if (@$_POST["a_add"] <> "") {
			if (!$this->ValidateForm()) {
				$this->CurrentAction = "I"; // Form error, reset action
				$this->EventCancelled = TRUE; // Event cancelled
				$this->RestoreFormValues(); // Restore form values
				$this->setFailureMessage($gsFormError);
			}
		}

		// Perform action based on action code
		switch ($this->CurrentAction) {
			case "I": // Blank record, no action required
				break;
			case "C": // Copy an existing record
				if (!$this->LoadRow()) { // Load record based on key
					if ($this->getFailureMessage() == "") $this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
					$this->Page_Terminate("Clielist.php"); // No matching record, return to list
				}
				break;
			case "A": // Add new record
				$this->SendEmail = TRUE; // Send email on add success
				if ($this->AddRow($this->OldRecordset)) { // Add successful
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage($Language->Phrase("AddSuccess")); // Set up success message
					$sReturnUrl = $this->getReturnUrl();
					if (ew_GetPageName($sReturnUrl) == "Clieview.php")
						$sReturnUrl = $this->GetViewUrl(); // View paging, return to view page with keyurl directly
					$this->Page_Terminate($sReturnUrl); // Clean up and return
				} else {
					$this->EventCancelled = TRUE; // Event cancelled
					$this->RestoreFormValues(); // Add failed, restore form values
				}
		}

		// Render row based on row type
		$this->RowType = EW_ROWTYPE_ADD; // Render add type

		// Render row
		$this->ResetAttrs();
		$this->RenderRow();
	}

	// Get upload files
	function GetUploadFiles() {
		global $objForm, $Language;

		// Get upload data
	}

	// Load default values
	function LoadDefaultValues() {
		$this->CliNomb->CurrentValue = NULL;
		$this->CliNomb->OldValue = $this->CliNomb->CurrentValue;
		$this->CliApel->CurrentValue = NULL;
		$this->CliApel->OldValue = $this->CliApel->CurrentValue;
		$this->CliRCed->CurrentValue = NULL;
		$this->CliRCed->OldValue = $this->CliRCed->CurrentValue;
		$this->CliDire->CurrentValue = NULL;
		$this->CliDire->OldValue = $this->CliDire->CurrentValue;
		$this->CliMail->CurrentValue = NULL;
		$this->CliMail->OldValue = $this->CliMail->CurrentValue;
		$this->CliTele->CurrentValue = NULL;
		$this->CliTele->OldValue = $this->CliTele->CurrentValue;
	}

	// Load form values
	function LoadFormValues() {

		// Load from form
		global $objForm;
		if (!$this->CliNomb->FldIsDetailKey) {
			$this->CliNomb->setFormValue($objForm->GetValue("x_CliNomb"));
		}
		if (!$this->CliApel->FldIsDetailKey) {
			$this->CliApel->setFormValue($objForm->GetValue("x_CliApel"));
		}
		if (!$this->CliRCed->FldIsDetailKey) {
			$this->CliRCed->setFormValue($objForm->GetValue("x_CliRCed"));
		}
		if (!$this->CliDire->FldIsDetailKey) {
			$this->CliDire->setFormValue($objForm->GetValue("x_CliDire"));
		}
		if (!$this->CliMail->FldIsDetailKey) {
			$this->CliMail->setFormValue($objForm->GetValue("x_CliMail"));
		}
		if (!$this->CliTele->FldIsDetailKey) {
			$this->CliTele->setFormValue($objForm->GetValue("x_CliTele"));
		}
	}

	// Restore form values
	function RestoreFormValues() {
		global $objForm;
		$this->LoadOldRecord();
		$this->CliNomb->CurrentValue = $this->CliNomb->FormValue;
		$this->CliApel->CurrentValue = $this->CliApel->FormValue;
		$this->CliRCed->CurrentValue = $this->CliRCed->FormValue;
		$this->CliDire->CurrentValue = $this->CliDire->FormValue;
		$this->CliMail->CurrentValue = $this->CliMail->FormValue;
		$this->CliTele->CurrentValue = $this->CliTele->FormValue;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->CliCodi->setDbValue($rs->fields('CliCodi'));
		$this->CliNomb->setDbValue($rs->fields('CliNomb'));
		$this->CliApel->setDbValue($rs->fields('CliApel'));
		$this->CliRCed->setDbValue($rs->fields('CliRCed'));
		$this->CliDire->setDbValue($rs->fields('CliDire'));
		$this->CliMail->setDbValue($rs->fields('CliMail'));
		$this->CliTele->setDbValue($rs->fields('CliTele'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->CliCodi->DbValue = $row['CliCodi'];
		$this->CliNomb->DbValue = $row['CliNomb'];
		$this->CliApel->DbValue = $row['CliApel'];
		$this->CliRCed->DbValue = $row['CliRCed'];
		$this->CliDire->DbValue = $row['CliDire'];
		$this->CliMail->DbValue = $row['CliMail'];
		$this->CliTele->DbValue = $row['CliTele'];
	}

	// Load old record
	function LoadOldRecord() {

		// Load key values from Session
		$bValidKey = TRUE;
		if (strval($this->getKey("CliCodi")) <> "")
			$this->CliCodi->CurrentValue = $this->getKey("CliCodi"); // CliCodi
		else
			$bValidKey = FALSE;

		// Load old recordset
		if ($bValidKey) {
			$this->CurrentFilter = $this->KeyFilter();
			$sSql = $this->SQL();
			$conn = &$this->Connection();
			$this->OldRecordset = ew_LoadRecordset($sSql, $conn);
			$this->LoadRowValues($this->OldRecordset); // Load row values
		} else {
			$this->OldRecordset = NULL;
		}
		return $bValidKey;
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		// Call Row_Rendering event

		$this->Row_Rendering();

		// Common render codes for all row types
		// CliCodi
		// CliNomb
		// CliApel
		// CliRCed
		// CliDire
		// CliMail
		// CliTele

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// CliCodi
		$this->CliCodi->ViewValue = $this->CliCodi->CurrentValue;
		$this->CliCodi->ViewCustomAttributes = "";

		// CliNomb
		$this->CliNomb->ViewValue = $this->CliNomb->CurrentValue;
		$this->CliNomb->ViewCustomAttributes = "";

		// CliApel
		$this->CliApel->ViewValue = $this->CliApel->CurrentValue;
		$this->CliApel->ViewCustomAttributes = "";

		// CliRCed
		$this->CliRCed->ViewValue = $this->CliRCed->CurrentValue;
		$this->CliRCed->ViewCustomAttributes = "";

		// CliDire
		$this->CliDire->ViewValue = $this->CliDire->CurrentValue;
		$this->CliDire->ViewCustomAttributes = "";

		// CliMail
		$this->CliMail->ViewValue = $this->CliMail->CurrentValue;
		$this->CliMail->ViewCustomAttributes = "";

		// CliTele
		$this->CliTele->ViewValue = $this->CliTele->CurrentValue;
		$this->CliTele->ViewCustomAttributes = "";

			// CliNomb
			$this->CliNomb->LinkCustomAttributes = "";
			$this->CliNomb->HrefValue = "";
			$this->CliNomb->TooltipValue = "";

			// CliApel
			$this->CliApel->LinkCustomAttributes = "";
			$this->CliApel->HrefValue = "";
			$this->CliApel->TooltipValue = "";

			// CliRCed
			$this->CliRCed->LinkCustomAttributes = "";
			$this->CliRCed->HrefValue = "";
			$this->CliRCed->TooltipValue = "";

			// CliDire
			$this->CliDire->LinkCustomAttributes = "";
			$this->CliDire->HrefValue = "";
			$this->CliDire->TooltipValue = "";

			// CliMail
			$this->CliMail->LinkCustomAttributes = "";
			$this->CliMail->HrefValue = "";
			$this->CliMail->TooltipValue = "";

			// CliTele
			$this->CliTele->LinkCustomAttributes = "";
			$this->CliTele->HrefValue = "";
			$this->CliTele->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_ADD) { // Add row

			// CliNomb
			$this->CliNomb->EditAttrs["class"] = "form-control";
			$this->CliNomb->EditCustomAttributes = "";
			$this->CliNomb->EditValue = ew_HtmlEncode($this->CliNomb->CurrentValue);
			$this->CliNomb->PlaceHolder = ew_RemoveHtml($this->CliNomb->FldCaption());

			// CliApel
			$this->CliApel->EditAttrs["class"] = "form-control";
			$this->CliApel->EditCustomAttributes = "";
			$this->CliApel->EditValue = ew_HtmlEncode($this->CliApel->CurrentValue);
			$this->CliApel->PlaceHolder = ew_RemoveHtml($this->CliApel->FldCaption());

			// CliRCed
			$this->CliRCed->EditAttrs["class"] = "form-control";
			$this->CliRCed->EditCustomAttributes = "";
			$this->CliRCed->EditValue = ew_HtmlEncode($this->CliRCed->CurrentValue);
			$this->CliRCed->PlaceHolder = ew_RemoveHtml($this->CliRCed->FldCaption());

			// CliDire
			$this->CliDire->EditAttrs["class"] = "form-control";
			$this->CliDire->EditCustomAttributes = "";
			$this->CliDire->EditValue = ew_HtmlEncode($this->CliDire->CurrentValue);
			$this->CliDire->PlaceHolder = ew_RemoveHtml($this->CliDire->FldCaption());

			// CliMail
			$this->CliMail->EditAttrs["class"] = "form-control";
			$this->CliMail->EditCustomAttributes = "";
			$this->CliMail->EditValue = ew_HtmlEncode($this->CliMail->CurrentValue);
			$this->CliMail->PlaceHolder = ew_RemoveHtml($this->CliMail->FldCaption());

			// CliTele
			$this->CliTele->EditAttrs["class"] = "form-control";
			$this->CliTele->EditCustomAttributes = "";
			$this->CliTele->EditValue = ew_HtmlEncode($this->CliTele->CurrentValue);
			$this->CliTele->PlaceHolder = ew_RemoveHtml($this->CliTele->FldCaption());

			// Edit refer script
			// CliNomb

			$this->CliNomb->HrefValue = "";

			// CliApel
			$this->CliApel->HrefValue = "";

			// CliRCed
			$this->CliRCed->HrefValue = "";

			// CliDire
			$this->CliDire->HrefValue = "";

			// CliMail
			$this->CliMail->HrefValue = "";

			// CliTele
			$this->CliTele->HrefValue = "";
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate form
	function ValidateForm() {
		global $Language, $gsFormError;

		// Initialize form error message
		$gsFormError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return ($gsFormError == "");
		if (!$this->CliMail->FldIsDetailKey && !is_null($this->CliMail->FormValue) && $this->CliMail->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->CliMail->FldCaption(), $this->CliMail->ReqErrMsg));
		}
		if (!ew_CheckEmail($this->CliMail->FormValue)) {
			ew_AddMessage($gsFormError, $this->CliMail->FldErrMsg());
		}
		if (!ew_CheckInteger($this->CliTele->FormValue)) {
			ew_AddMessage($gsFormError, $this->CliTele->FldErrMsg());
		}

		// Return validate result
		$ValidateForm = ($gsFormError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateForm = $ValidateForm && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsFormError, $sFormCustomError);
		}
		return $ValidateForm;
	}

	// Add record
	function AddRow($rsold = NULL) {
		global $Language, $Security;
		if ($this->CliRCed->CurrentValue <> "") { // Check field with unique index
			$sFilter = "(CliRCed = '" . ew_AdjustSql($this->CliRCed->CurrentValue, $this->DBID) . "')";
			$rsChk = $this->LoadRs($sFilter);
			if ($rsChk && !$rsChk->EOF) {
				$sIdxErrMsg = str_replace("%f", $this->CliRCed->FldCaption(), $Language->Phrase("DupIndex"));
				$sIdxErrMsg = str_replace("%v", $this->CliRCed->CurrentValue, $sIdxErrMsg);
				$this->setFailureMessage($sIdxErrMsg);
				$rsChk->Close();
				return FALSE;
			}
		}
		$conn = &$this->Connection();

		// Load db values from rsold
		if ($rsold) {
			$this->LoadDbValues($rsold);
		}
		$rsnew = array();

		// CliNomb
		$this->CliNomb->SetDbValueDef($rsnew, $this->CliNomb->CurrentValue, NULL, FALSE);

		// CliApel
		$this->CliApel->SetDbValueDef($rsnew, $this->CliApel->CurrentValue, NULL, FALSE);

		// CliRCed
		$this->CliRCed->SetDbValueDef($rsnew, $this->CliRCed->CurrentValue, NULL, FALSE);

		// CliDire
		$this->CliDire->SetDbValueDef($rsnew, $this->CliDire->CurrentValue, NULL, FALSE);

		// CliMail
		$this->CliMail->SetDbValueDef($rsnew, $this->CliMail->CurrentValue, NULL, FALSE);

		// CliTele
		$this->CliTele->SetDbValueDef($rsnew, $this->CliTele->CurrentValue, NULL, FALSE);

		// Call Row Inserting event
		$rs = ($rsold == NULL) ? NULL : $rsold->fields;
		$bInsertRow = $this->Row_Inserting($rs, $rsnew);
		if ($bInsertRow) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			$AddRow = $this->Insert($rsnew);
			$conn->raiseErrorFn = '';
			if ($AddRow) {

				// Get insert id if necessary
				$this->CliCodi->setDbValue($conn->GetOne("SELECT currval('\"Clie_CliCodi_seq\"'::regclass)"));
				$rsnew['CliCodi'] = $this->CliCodi->DbValue;
			}
		} else {
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("InsertCancelled"));
			}
			$AddRow = FALSE;
		}
		if ($AddRow) {

			// Call Row Inserted event
			$rs = ($rsold == NULL) ? NULL : $rsold->fields;
			$this->Row_Inserted($rs, $rsnew);
		}
		return $AddRow;
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, "Clielist.php", "", $this->TableVar, TRUE);
		$PageId = ($this->CurrentAction == "C") ? "Copy" : "Add";
		$Breadcrumb->Add("add", $PageId, $url);
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($Clie_add)) $Clie_add = new cClie_add();

// Page init
$Clie_add->Page_Init();

// Page main
$Clie_add->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$Clie_add->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "add";
var CurrentForm = fClieadd = new ew_Form("fClieadd", "add");

// Validate form
fClieadd.Validate = function() {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.GetForm(), $fobj = $(fobj);
	if ($fobj.find("#a_confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.FormKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = $fobj.find("#a_list").val() == "gridinsert";
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
			elm = this.GetElements("x" + infix + "_CliMail");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $Clie->CliMail->FldCaption(), $Clie->CliMail->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_CliMail");
			if (elm && !ew_CheckEmail(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($Clie->CliMail->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_CliTele");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($Clie->CliTele->FldErrMsg()) ?>");

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
	}

	// Process detail forms
	var dfs = $fobj.find("input[name='detailpage']").get();
	for (var i = 0; i < dfs.length; i++) {
		var df = dfs[i], val = df.value;
		if (val && ewForms[val])
			if (!ewForms[val].Validate())
				return false;
	}
	return true;
}

// Form_CustomValidate event
fClieadd.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fClieadd.ValidateRequired = true;
<?php } else { ?>
fClieadd.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
// Form object for search

</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php $Clie_add->ShowPageHeader(); ?>
<?php
$Clie_add->ShowMessage();
?>
<form name="fClieadd" id="fClieadd" class="<?php echo $Clie_add->FormClassName ?>" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($Clie_add->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $Clie_add->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="Clie">
<input type="hidden" name="a_add" id="a_add" value="A">
<div>
<?php if ($Clie->CliNomb->Visible) { // CliNomb ?>
	<div id="r_CliNomb" class="form-group">
		<label id="elh_Clie_CliNomb" for="x_CliNomb" class="col-sm-2 control-label ewLabel"><?php echo $Clie->CliNomb->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $Clie->CliNomb->CellAttributes() ?>>
<span id="el_Clie_CliNomb">
<input type="text" data-table="Clie" data-field="x_CliNomb" name="x_CliNomb" id="x_CliNomb" size="30" placeholder="<?php echo ew_HtmlEncode($Clie->CliNomb->getPlaceHolder()) ?>" value="<?php echo $Clie->CliNomb->EditValue ?>"<?php echo $Clie->CliNomb->EditAttributes() ?>>
</span>
<?php echo $Clie->CliNomb->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($Clie->CliApel->Visible) { // CliApel ?>
	<div id="r_CliApel" class="form-group">
		<label id="elh_Clie_CliApel" for="x_CliApel" class="col-sm-2 control-label ewLabel"><?php echo $Clie->CliApel->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $Clie->CliApel->CellAttributes() ?>>
<span id="el_Clie_CliApel">
<input type="text" data-table="Clie" data-field="x_CliApel" name="x_CliApel" id="x_CliApel" size="30" placeholder="<?php echo ew_HtmlEncode($Clie->CliApel->getPlaceHolder()) ?>" value="<?php echo $Clie->CliApel->EditValue ?>"<?php echo $Clie->CliApel->EditAttributes() ?>>
</span>
<?php echo $Clie->CliApel->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($Clie->CliRCed->Visible) { // CliRCed ?>
	<div id="r_CliRCed" class="form-group">
		<label id="elh_Clie_CliRCed" for="x_CliRCed" class="col-sm-2 control-label ewLabel"><?php echo $Clie->CliRCed->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $Clie->CliRCed->CellAttributes() ?>>
<span id="el_Clie_CliRCed">
<input type="text" data-table="Clie" data-field="x_CliRCed" name="x_CliRCed" id="x_CliRCed" size="30" placeholder="<?php echo ew_HtmlEncode($Clie->CliRCed->getPlaceHolder()) ?>" value="<?php echo $Clie->CliRCed->EditValue ?>"<?php echo $Clie->CliRCed->EditAttributes() ?>>
</span>
<?php echo $Clie->CliRCed->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($Clie->CliDire->Visible) { // CliDire ?>
	<div id="r_CliDire" class="form-group">
		<label id="elh_Clie_CliDire" for="x_CliDire" class="col-sm-2 control-label ewLabel"><?php echo $Clie->CliDire->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $Clie->CliDire->CellAttributes() ?>>
<span id="el_Clie_CliDire">
<input type="text" data-table="Clie" data-field="x_CliDire" name="x_CliDire" id="x_CliDire" size="30" placeholder="<?php echo ew_HtmlEncode($Clie->CliDire->getPlaceHolder()) ?>" value="<?php echo $Clie->CliDire->EditValue ?>"<?php echo $Clie->CliDire->EditAttributes() ?>>
</span>
<?php echo $Clie->CliDire->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($Clie->CliMail->Visible) { // CliMail ?>
	<div id="r_CliMail" class="form-group">
		<label id="elh_Clie_CliMail" for="x_CliMail" class="col-sm-2 control-label ewLabel"><?php echo $Clie->CliMail->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $Clie->CliMail->CellAttributes() ?>>
<span id="el_Clie_CliMail">
<input type="text" data-table="Clie" data-field="x_CliMail" name="x_CliMail" id="x_CliMail" size="30" maxlength="50" placeholder="<?php echo ew_HtmlEncode($Clie->CliMail->getPlaceHolder()) ?>" value="<?php echo $Clie->CliMail->EditValue ?>"<?php echo $Clie->CliMail->EditAttributes() ?>>
</span>
<?php echo $Clie->CliMail->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($Clie->CliTele->Visible) { // CliTele ?>
	<div id="r_CliTele" class="form-group">
		<label id="elh_Clie_CliTele" for="x_CliTele" class="col-sm-2 control-label ewLabel"><?php echo $Clie->CliTele->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $Clie->CliTele->CellAttributes() ?>>
<span id="el_Clie_CliTele">
<input type="text" data-table="Clie" data-field="x_CliTele" name="x_CliTele" id="x_CliTele" size="30" placeholder="<?php echo ew_HtmlEncode($Clie->CliTele->getPlaceHolder()) ?>" value="<?php echo $Clie->CliTele->EditValue ?>"<?php echo $Clie->CliTele->EditAttributes() ?>>
</span>
<?php echo $Clie->CliTele->CustomMsg ?></div></div>
	</div>
<?php } ?>
</div>
<div class="form-group">
	<div class="col-sm-offset-2 col-sm-10">
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("AddBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="button" data-href="<?php echo $Clie_add->getReturnUrl() ?>"><?php echo $Language->Phrase("CancelBtn") ?></button>
	</div>
</div>
</form>
<script type="text/javascript">
fClieadd.Init();
</script>
<?php
$Clie_add->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$Clie_add->Page_Terminate();
?>
