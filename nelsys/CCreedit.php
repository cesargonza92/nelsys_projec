<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "CCreinfo.php" ?>
<?php include_once "Usuainfo.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$CCre_edit = NULL; // Initialize page object first

class cCCre_edit extends cCCre {

	// Page ID
	var $PageID = 'edit';

	// Project ID
	var $ProjectID = "{04439FF7-B43F-460F-8514-F71C8FF9E679}";

	// Table name
	var $TableName = 'CCre';

	// Page object name
	var $PageObjName = 'CCre_edit';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (CCre)
		if (!isset($GLOBALS["CCre"]) || get_class($GLOBALS["CCre"]) == "cCCre") {
			$GLOBALS["CCre"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["CCre"];
		}

		// Table object (Usua)
		if (!isset($GLOBALS['Usua'])) $GLOBALS['Usua'] = new cUsua();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'edit', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'CCre', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (Usua)
		if (!isset($UserTable)) {
			$UserTable = new cUsua();
			$UserTableConn = Conn($UserTable->DBID);
		}
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanEdit()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage($Language->Phrase("NoPermission")); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("CCrelist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}

		// Create form object
		$objForm = new cFormObj();
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action
		$this->CcrCodi->Visible = !$this->IsAdd() && !$this->IsCopy() && !$this->IsGridAdd();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Process auto fill
		if (@$_POST["ajax"] == "autofill") {
			$results = $this->GetAutoFill(@$_POST["name"], @$_POST["q"]);
			if ($results) {

				// Clean output buffer
				if (!EW_DEBUG_ENABLED && ob_get_length())
					ob_end_clean();
				echo $results;
				$this->Page_Terminate();
				exit();
			}
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $CCre;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($CCre);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $FormClassName = "form-horizontal ewForm ewEditForm";
	var $DbMasterFilter;
	var $DbDetailFilter;

	// 
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError;

		// Load key from QueryString
		if (@$_GET["CcrCodi"] <> "") {
			$this->CcrCodi->setQueryStringValue($_GET["CcrCodi"]);
		}

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Process form if post back
		if (@$_POST["a_edit"] <> "") {
			$this->CurrentAction = $_POST["a_edit"]; // Get action code
			$this->LoadFormValues(); // Get form values
		} else {
			$this->CurrentAction = "I"; // Default action is display
		}

		// Check if valid key
		if ($this->CcrCodi->CurrentValue == "")
			$this->Page_Terminate("CCrelist.php"); // Invalid key, return to list

		// Validate form if post back
		if (@$_POST["a_edit"] <> "") {
			if (!$this->ValidateForm()) {
				$this->CurrentAction = ""; // Form error, reset action
				$this->setFailureMessage($gsFormError);
				$this->EventCancelled = TRUE; // Event cancelled
				$this->RestoreFormValues();
			}
		}
		switch ($this->CurrentAction) {
			case "I": // Get a record to display
				if (!$this->LoadRow()) { // Load record based on key
					if ($this->getFailureMessage() == "") $this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
					$this->Page_Terminate("CCrelist.php"); // No matching record, return to list
				}
				break;
			Case "U": // Update
				$sReturnUrl = $this->getReturnUrl();
				$this->SendEmail = TRUE; // Send email on update success
				if ($this->EditRow()) { // Update record based on key
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage($Language->Phrase("UpdateSuccess")); // Update success
					$this->Page_Terminate($sReturnUrl); // Return to caller
				} elseif ($this->getFailureMessage() == $Language->Phrase("NoRecord")) {
					$this->Page_Terminate($sReturnUrl); // Return to caller
				} else {
					$this->EventCancelled = TRUE; // Event cancelled
					$this->RestoreFormValues(); // Restore form values if update failed
				}
		}

		// Render the record
		if ($this->CurrentAction == "F") { // Confirm page
			$this->RowType = EW_ROWTYPE_VIEW; // Render as View
		} else {
			$this->RowType = EW_ROWTYPE_EDIT; // Render as Edit
		}
		$this->ResetAttrs();
		$this->RenderRow();
	}

	// Set up starting record parameters
	function SetUpStartRec() {
		if ($this->DisplayRecs == 0)
			return;
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET[EW_TABLE_START_REC] <> "") { // Check for "start" parameter
				$this->StartRec = $_GET[EW_TABLE_START_REC];
				$this->setStartRecordNumber($this->StartRec);
			} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
				$PageNo = $_GET[EW_TABLE_PAGE_NO];
				if (is_numeric($PageNo)) {
					$this->StartRec = ($PageNo-1)*$this->DisplayRecs+1;
					if ($this->StartRec <= 0) {
						$this->StartRec = 1;
					} elseif ($this->StartRec >= intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1) {
						$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1;
					}
					$this->setStartRecordNumber($this->StartRec);
				}
			}
		}
		$this->StartRec = $this->getStartRecordNumber();

		// Check if correct start record counter
		if (!is_numeric($this->StartRec) || $this->StartRec == "") { // Avoid invalid start record counter
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} elseif (intval($this->StartRec) > intval($this->TotalRecs)) { // Avoid starting record > total records
			$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to last page first record
			$this->setStartRecordNumber($this->StartRec);
		} elseif (($this->StartRec-1) % $this->DisplayRecs <> 0) {
			$this->StartRec = intval(($this->StartRec-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to page boundary
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Get upload files
	function GetUploadFiles() {
		global $objForm, $Language;

		// Get upload data
	}

	// Load form values
	function LoadFormValues() {

		// Load from form
		global $objForm;
		if (!$this->CcrCodi->FldIsDetailKey)
			$this->CcrCodi->setFormValue($objForm->GetValue("x_CcrCodi"));
		if (!$this->VcrCodi->FldIsDetailKey) {
			$this->VcrCodi->setFormValue($objForm->GetValue("x_VcrCodi"));
		}
		if (!$this->CcrNCuo->FldIsDetailKey) {
			$this->CcrNCuo->setFormValue($objForm->GetValue("x_CcrNCuo"));
		}
		if (!$this->CcrFPag->FldIsDetailKey) {
			$this->CcrFPag->setFormValue($objForm->GetValue("x_CcrFPag"));
			$this->CcrFPag->CurrentValue = ew_UnFormatDateTime($this->CcrFPag->CurrentValue, 7);
		}
		if (!$this->CcrMont->FldIsDetailKey) {
			$this->CcrMont->setFormValue($objForm->GetValue("x_CcrMont"));
		}
		if (!$this->CcrMora->FldIsDetailKey) {
			$this->CcrMora->setFormValue($objForm->GetValue("x_CcrMora"));
		}
		if (!$this->CcrDAtr->FldIsDetailKey) {
			$this->CcrDAtr->setFormValue($objForm->GetValue("x_CcrDAtr"));
		}
		if (!$this->CcrEsta->FldIsDetailKey) {
			$this->CcrEsta->setFormValue($objForm->GetValue("x_CcrEsta"));
		}
		if (!$this->CcrAnho->FldIsDetailKey) {
			$this->CcrAnho->setFormValue($objForm->GetValue("x_CcrAnho"));
		}
		if (!$this->CcrMes->FldIsDetailKey) {
			$this->CcrMes->setFormValue($objForm->GetValue("x_CcrMes"));
		}
		if (!$this->CcrConc->FldIsDetailKey) {
			$this->CcrConc->setFormValue($objForm->GetValue("x_CcrConc"));
		}
		if (!$this->CcrUsua->FldIsDetailKey) {
			$this->CcrUsua->setFormValue($objForm->GetValue("x_CcrUsua"));
		}
		if (!$this->CcrFCre->FldIsDetailKey) {
			$this->CcrFCre->setFormValue($objForm->GetValue("x_CcrFCre"));
			$this->CcrFCre->CurrentValue = ew_UnFormatDateTime($this->CcrFCre->CurrentValue, 7);
		}
	}

	// Restore form values
	function RestoreFormValues() {
		global $objForm;
		$this->LoadRow();
		$this->CcrCodi->CurrentValue = $this->CcrCodi->FormValue;
		$this->VcrCodi->CurrentValue = $this->VcrCodi->FormValue;
		$this->CcrNCuo->CurrentValue = $this->CcrNCuo->FormValue;
		$this->CcrFPag->CurrentValue = $this->CcrFPag->FormValue;
		$this->CcrFPag->CurrentValue = ew_UnFormatDateTime($this->CcrFPag->CurrentValue, 7);
		$this->CcrMont->CurrentValue = $this->CcrMont->FormValue;
		$this->CcrMora->CurrentValue = $this->CcrMora->FormValue;
		$this->CcrDAtr->CurrentValue = $this->CcrDAtr->FormValue;
		$this->CcrEsta->CurrentValue = $this->CcrEsta->FormValue;
		$this->CcrAnho->CurrentValue = $this->CcrAnho->FormValue;
		$this->CcrMes->CurrentValue = $this->CcrMes->FormValue;
		$this->CcrConc->CurrentValue = $this->CcrConc->FormValue;
		$this->CcrUsua->CurrentValue = $this->CcrUsua->FormValue;
		$this->CcrFCre->CurrentValue = $this->CcrFCre->FormValue;
		$this->CcrFCre->CurrentValue = ew_UnFormatDateTime($this->CcrFCre->CurrentValue, 7);
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->CcrCodi->setDbValue($rs->fields('CcrCodi'));
		$this->VcrCodi->setDbValue($rs->fields('VcrCodi'));
		$this->CcrNCuo->setDbValue($rs->fields('CcrNCuo'));
		$this->CcrFPag->setDbValue($rs->fields('CcrFPag'));
		$this->CcrMont->setDbValue($rs->fields('CcrMont'));
		$this->CcrMora->setDbValue($rs->fields('CcrMora'));
		$this->CcrDAtr->setDbValue($rs->fields('CcrDAtr'));
		$this->CcrEsta->setDbValue($rs->fields('CcrEsta'));
		$this->CcrAnho->setDbValue($rs->fields('CcrAnho'));
		$this->CcrMes->setDbValue($rs->fields('CcrMes'));
		$this->CcrConc->setDbValue($rs->fields('CcrConc'));
		$this->CcrUsua->setDbValue($rs->fields('CcrUsua'));
		$this->CcrFCre->setDbValue($rs->fields('CcrFCre'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->CcrCodi->DbValue = $row['CcrCodi'];
		$this->VcrCodi->DbValue = $row['VcrCodi'];
		$this->CcrNCuo->DbValue = $row['CcrNCuo'];
		$this->CcrFPag->DbValue = $row['CcrFPag'];
		$this->CcrMont->DbValue = $row['CcrMont'];
		$this->CcrMora->DbValue = $row['CcrMora'];
		$this->CcrDAtr->DbValue = $row['CcrDAtr'];
		$this->CcrEsta->DbValue = $row['CcrEsta'];
		$this->CcrAnho->DbValue = $row['CcrAnho'];
		$this->CcrMes->DbValue = $row['CcrMes'];
		$this->CcrConc->DbValue = $row['CcrConc'];
		$this->CcrUsua->DbValue = $row['CcrUsua'];
		$this->CcrFCre->DbValue = $row['CcrFCre'];
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		// Convert decimal values if posted back

		if ($this->CcrMont->FormValue == $this->CcrMont->CurrentValue && is_numeric(ew_StrToFloat($this->CcrMont->CurrentValue)))
			$this->CcrMont->CurrentValue = ew_StrToFloat($this->CcrMont->CurrentValue);

		// Convert decimal values if posted back
		if ($this->CcrMora->FormValue == $this->CcrMora->CurrentValue && is_numeric(ew_StrToFloat($this->CcrMora->CurrentValue)))
			$this->CcrMora->CurrentValue = ew_StrToFloat($this->CcrMora->CurrentValue);

		// Call Row_Rendering event
		$this->Row_Rendering();

		// Common render codes for all row types
		// CcrCodi
		// VcrCodi
		// CcrNCuo
		// CcrFPag
		// CcrMont
		// CcrMora
		// CcrDAtr
		// CcrEsta
		// CcrAnho
		// CcrMes
		// CcrConc
		// CcrUsua
		// CcrFCre

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// CcrCodi
		$this->CcrCodi->ViewValue = $this->CcrCodi->CurrentValue;
		$this->CcrCodi->ViewCustomAttributes = "";

		// VcrCodi
		if (strval($this->VcrCodi->CurrentValue) <> "") {
			$sFilterWrk = "\"VcrCodi\"" . ew_SearchString("=", $this->VcrCodi->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT \"VcrCodi\", \"VcrCodi\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"VCre\"";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->VcrCodi, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->VcrCodi->ViewValue = $this->VcrCodi->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->VcrCodi->ViewValue = $this->VcrCodi->CurrentValue;
			}
		} else {
			$this->VcrCodi->ViewValue = NULL;
		}
		$this->VcrCodi->ViewCustomAttributes = "";

		// CcrNCuo
		$this->CcrNCuo->ViewValue = $this->CcrNCuo->CurrentValue;
		$this->CcrNCuo->ViewCustomAttributes = "";

		// CcrFPag
		$this->CcrFPag->ViewValue = $this->CcrFPag->CurrentValue;
		$this->CcrFPag->ViewValue = ew_FormatDateTime($this->CcrFPag->ViewValue, 7);
		$this->CcrFPag->ViewCustomAttributes = "";

		// CcrMont
		$this->CcrMont->ViewValue = $this->CcrMont->CurrentValue;
		$this->CcrMont->ViewCustomAttributes = "";

		// CcrMora
		$this->CcrMora->ViewValue = $this->CcrMora->CurrentValue;
		$this->CcrMora->ViewCustomAttributes = "";

		// CcrDAtr
		$this->CcrDAtr->ViewValue = $this->CcrDAtr->CurrentValue;
		$this->CcrDAtr->ViewCustomAttributes = "";

		// CcrEsta
		$this->CcrEsta->ViewValue = $this->CcrEsta->CurrentValue;
		$this->CcrEsta->ViewCustomAttributes = "";

		// CcrAnho
		$this->CcrAnho->ViewValue = $this->CcrAnho->CurrentValue;
		$this->CcrAnho->ViewCustomAttributes = "";

		// CcrMes
		$this->CcrMes->ViewValue = $this->CcrMes->CurrentValue;
		$this->CcrMes->ViewCustomAttributes = "";

		// CcrConc
		$this->CcrConc->ViewValue = $this->CcrConc->CurrentValue;
		$this->CcrConc->ViewCustomAttributes = "";

		// CcrUsua
		$this->CcrUsua->ViewValue = $this->CcrUsua->CurrentValue;
		$this->CcrUsua->ViewCustomAttributes = "";

		// CcrFCre
		$this->CcrFCre->ViewValue = $this->CcrFCre->CurrentValue;
		$this->CcrFCre->ViewValue = ew_FormatDateTime($this->CcrFCre->ViewValue, 7);
		$this->CcrFCre->ViewCustomAttributes = "";

			// CcrCodi
			$this->CcrCodi->LinkCustomAttributes = "";
			$this->CcrCodi->HrefValue = "";
			$this->CcrCodi->TooltipValue = "";

			// VcrCodi
			$this->VcrCodi->LinkCustomAttributes = "";
			$this->VcrCodi->HrefValue = "";
			$this->VcrCodi->TooltipValue = "";

			// CcrNCuo
			$this->CcrNCuo->LinkCustomAttributes = "";
			$this->CcrNCuo->HrefValue = "";
			$this->CcrNCuo->TooltipValue = "";

			// CcrFPag
			$this->CcrFPag->LinkCustomAttributes = "";
			$this->CcrFPag->HrefValue = "";
			$this->CcrFPag->TooltipValue = "";

			// CcrMont
			$this->CcrMont->LinkCustomAttributes = "";
			$this->CcrMont->HrefValue = "";
			$this->CcrMont->TooltipValue = "";

			// CcrMora
			$this->CcrMora->LinkCustomAttributes = "";
			$this->CcrMora->HrefValue = "";
			$this->CcrMora->TooltipValue = "";

			// CcrDAtr
			$this->CcrDAtr->LinkCustomAttributes = "";
			$this->CcrDAtr->HrefValue = "";
			$this->CcrDAtr->TooltipValue = "";

			// CcrEsta
			$this->CcrEsta->LinkCustomAttributes = "";
			$this->CcrEsta->HrefValue = "";
			$this->CcrEsta->TooltipValue = "";

			// CcrAnho
			$this->CcrAnho->LinkCustomAttributes = "";
			$this->CcrAnho->HrefValue = "";
			$this->CcrAnho->TooltipValue = "";

			// CcrMes
			$this->CcrMes->LinkCustomAttributes = "";
			$this->CcrMes->HrefValue = "";
			$this->CcrMes->TooltipValue = "";

			// CcrConc
			$this->CcrConc->LinkCustomAttributes = "";
			$this->CcrConc->HrefValue = "";
			$this->CcrConc->TooltipValue = "";

			// CcrUsua
			$this->CcrUsua->LinkCustomAttributes = "";
			$this->CcrUsua->HrefValue = "";
			$this->CcrUsua->TooltipValue = "";

			// CcrFCre
			$this->CcrFCre->LinkCustomAttributes = "";
			$this->CcrFCre->HrefValue = "";
			$this->CcrFCre->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_EDIT) { // Edit row

			// CcrCodi
			$this->CcrCodi->EditAttrs["class"] = "form-control";
			$this->CcrCodi->EditCustomAttributes = "";
			$this->CcrCodi->EditValue = $this->CcrCodi->CurrentValue;
			$this->CcrCodi->ViewCustomAttributes = "";

			// VcrCodi
			$this->VcrCodi->EditAttrs["class"] = "form-control";
			$this->VcrCodi->EditCustomAttributes = "";
			if (trim(strval($this->VcrCodi->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "\"VcrCodi\"" . ew_SearchString("=", $this->VcrCodi->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT \"VcrCodi\", \"VcrCodi\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\", '' AS \"SelectFilterFld\", '' AS \"SelectFilterFld2\", '' AS \"SelectFilterFld3\", '' AS \"SelectFilterFld4\" FROM \"public\".\"VCre\"";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->VcrCodi, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->VcrCodi->EditValue = $arwrk;

			// CcrNCuo
			$this->CcrNCuo->EditAttrs["class"] = "form-control";
			$this->CcrNCuo->EditCustomAttributes = "";
			$this->CcrNCuo->EditValue = ew_HtmlEncode($this->CcrNCuo->CurrentValue);
			$this->CcrNCuo->PlaceHolder = ew_RemoveHtml($this->CcrNCuo->FldCaption());

			// CcrFPag
			$this->CcrFPag->EditAttrs["class"] = "form-control";
			$this->CcrFPag->EditCustomAttributes = "";
			$this->CcrFPag->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->CcrFPag->CurrentValue, 7));
			$this->CcrFPag->PlaceHolder = ew_RemoveHtml($this->CcrFPag->FldCaption());

			// CcrMont
			$this->CcrMont->EditAttrs["class"] = "form-control";
			$this->CcrMont->EditCustomAttributes = "";
			$this->CcrMont->EditValue = ew_HtmlEncode($this->CcrMont->CurrentValue);
			$this->CcrMont->PlaceHolder = ew_RemoveHtml($this->CcrMont->FldCaption());
			if (strval($this->CcrMont->EditValue) <> "" && is_numeric($this->CcrMont->EditValue)) $this->CcrMont->EditValue = ew_FormatNumber($this->CcrMont->EditValue, -2, -1, -2, 0);

			// CcrMora
			$this->CcrMora->EditAttrs["class"] = "form-control";
			$this->CcrMora->EditCustomAttributes = "";
			$this->CcrMora->EditValue = ew_HtmlEncode($this->CcrMora->CurrentValue);
			$this->CcrMora->PlaceHolder = ew_RemoveHtml($this->CcrMora->FldCaption());
			if (strval($this->CcrMora->EditValue) <> "" && is_numeric($this->CcrMora->EditValue)) $this->CcrMora->EditValue = ew_FormatNumber($this->CcrMora->EditValue, -2, -1, -2, 0);

			// CcrDAtr
			$this->CcrDAtr->EditAttrs["class"] = "form-control";
			$this->CcrDAtr->EditCustomAttributes = "";
			$this->CcrDAtr->EditValue = ew_HtmlEncode($this->CcrDAtr->CurrentValue);
			$this->CcrDAtr->PlaceHolder = ew_RemoveHtml($this->CcrDAtr->FldCaption());

			// CcrEsta
			$this->CcrEsta->EditAttrs["class"] = "form-control";
			$this->CcrEsta->EditCustomAttributes = "";
			$this->CcrEsta->EditValue = ew_HtmlEncode($this->CcrEsta->CurrentValue);
			$this->CcrEsta->PlaceHolder = ew_RemoveHtml($this->CcrEsta->FldCaption());

			// CcrAnho
			$this->CcrAnho->EditAttrs["class"] = "form-control";
			$this->CcrAnho->EditCustomAttributes = "";
			$this->CcrAnho->EditValue = ew_HtmlEncode($this->CcrAnho->CurrentValue);
			$this->CcrAnho->PlaceHolder = ew_RemoveHtml($this->CcrAnho->FldCaption());

			// CcrMes
			$this->CcrMes->EditAttrs["class"] = "form-control";
			$this->CcrMes->EditCustomAttributes = "";
			$this->CcrMes->EditValue = ew_HtmlEncode($this->CcrMes->CurrentValue);
			$this->CcrMes->PlaceHolder = ew_RemoveHtml($this->CcrMes->FldCaption());

			// CcrConc
			$this->CcrConc->EditAttrs["class"] = "form-control";
			$this->CcrConc->EditCustomAttributes = "";
			$this->CcrConc->EditValue = ew_HtmlEncode($this->CcrConc->CurrentValue);
			$this->CcrConc->PlaceHolder = ew_RemoveHtml($this->CcrConc->FldCaption());

			// CcrUsua
			$this->CcrUsua->EditAttrs["class"] = "form-control";
			$this->CcrUsua->EditCustomAttributes = "";
			$this->CcrUsua->EditValue = ew_HtmlEncode($this->CcrUsua->CurrentValue);
			$this->CcrUsua->PlaceHolder = ew_RemoveHtml($this->CcrUsua->FldCaption());

			// CcrFCre
			$this->CcrFCre->EditAttrs["class"] = "form-control";
			$this->CcrFCre->EditCustomAttributes = "";
			$this->CcrFCre->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->CcrFCre->CurrentValue, 7));
			$this->CcrFCre->PlaceHolder = ew_RemoveHtml($this->CcrFCre->FldCaption());

			// Edit refer script
			// CcrCodi

			$this->CcrCodi->HrefValue = "";

			// VcrCodi
			$this->VcrCodi->HrefValue = "";

			// CcrNCuo
			$this->CcrNCuo->HrefValue = "";

			// CcrFPag
			$this->CcrFPag->HrefValue = "";

			// CcrMont
			$this->CcrMont->HrefValue = "";

			// CcrMora
			$this->CcrMora->HrefValue = "";

			// CcrDAtr
			$this->CcrDAtr->HrefValue = "";

			// CcrEsta
			$this->CcrEsta->HrefValue = "";

			// CcrAnho
			$this->CcrAnho->HrefValue = "";

			// CcrMes
			$this->CcrMes->HrefValue = "";

			// CcrConc
			$this->CcrConc->HrefValue = "";

			// CcrUsua
			$this->CcrUsua->HrefValue = "";

			// CcrFCre
			$this->CcrFCre->HrefValue = "";
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate form
	function ValidateForm() {
		global $Language, $gsFormError;

		// Initialize form error message
		$gsFormError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return ($gsFormError == "");
		if (!$this->VcrCodi->FldIsDetailKey && !is_null($this->VcrCodi->FormValue) && $this->VcrCodi->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->VcrCodi->FldCaption(), $this->VcrCodi->ReqErrMsg));
		}
		if (!$this->CcrNCuo->FldIsDetailKey && !is_null($this->CcrNCuo->FormValue) && $this->CcrNCuo->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->CcrNCuo->FldCaption(), $this->CcrNCuo->ReqErrMsg));
		}
		if (!ew_CheckInteger($this->CcrNCuo->FormValue)) {
			ew_AddMessage($gsFormError, $this->CcrNCuo->FldErrMsg());
		}
		if (!$this->CcrFPag->FldIsDetailKey && !is_null($this->CcrFPag->FormValue) && $this->CcrFPag->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->CcrFPag->FldCaption(), $this->CcrFPag->ReqErrMsg));
		}
		if (!ew_CheckEuroDate($this->CcrFPag->FormValue)) {
			ew_AddMessage($gsFormError, $this->CcrFPag->FldErrMsg());
		}
		if (!$this->CcrMont->FldIsDetailKey && !is_null($this->CcrMont->FormValue) && $this->CcrMont->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->CcrMont->FldCaption(), $this->CcrMont->ReqErrMsg));
		}
		if (!ew_CheckNumber($this->CcrMont->FormValue)) {
			ew_AddMessage($gsFormError, $this->CcrMont->FldErrMsg());
		}
		if (!ew_CheckNumber($this->CcrMora->FormValue)) {
			ew_AddMessage($gsFormError, $this->CcrMora->FldErrMsg());
		}
		if (!ew_CheckInteger($this->CcrDAtr->FormValue)) {
			ew_AddMessage($gsFormError, $this->CcrDAtr->FldErrMsg());
		}
		if (!$this->CcrEsta->FldIsDetailKey && !is_null($this->CcrEsta->FormValue) && $this->CcrEsta->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->CcrEsta->FldCaption(), $this->CcrEsta->ReqErrMsg));
		}
		if (!$this->CcrAnho->FldIsDetailKey && !is_null($this->CcrAnho->FormValue) && $this->CcrAnho->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->CcrAnho->FldCaption(), $this->CcrAnho->ReqErrMsg));
		}
		if (!$this->CcrMes->FldIsDetailKey && !is_null($this->CcrMes->FormValue) && $this->CcrMes->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->CcrMes->FldCaption(), $this->CcrMes->ReqErrMsg));
		}
		if (!$this->CcrConc->FldIsDetailKey && !is_null($this->CcrConc->FormValue) && $this->CcrConc->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->CcrConc->FldCaption(), $this->CcrConc->ReqErrMsg));
		}
		if (!$this->CcrUsua->FldIsDetailKey && !is_null($this->CcrUsua->FormValue) && $this->CcrUsua->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->CcrUsua->FldCaption(), $this->CcrUsua->ReqErrMsg));
		}
		if (!$this->CcrFCre->FldIsDetailKey && !is_null($this->CcrFCre->FormValue) && $this->CcrFCre->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->CcrFCre->FldCaption(), $this->CcrFCre->ReqErrMsg));
		}
		if (!ew_CheckEuroDate($this->CcrFCre->FormValue)) {
			ew_AddMessage($gsFormError, $this->CcrFCre->FldErrMsg());
		}

		// Return validate result
		$ValidateForm = ($gsFormError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateForm = $ValidateForm && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsFormError, $sFormCustomError);
		}
		return $ValidateForm;
	}

	// Update record based on key values
	function EditRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$conn = &$this->Connection();
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE)
			return FALSE;
		if ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record message
			$EditRow = FALSE; // Update Failed
		} else {

			// Save old values
			$rsold = &$rs->fields;
			$this->LoadDbValues($rsold);
			$rsnew = array();

			// VcrCodi
			$this->VcrCodi->SetDbValueDef($rsnew, $this->VcrCodi->CurrentValue, 0, $this->VcrCodi->ReadOnly);

			// CcrNCuo
			$this->CcrNCuo->SetDbValueDef($rsnew, $this->CcrNCuo->CurrentValue, 0, $this->CcrNCuo->ReadOnly);

			// CcrFPag
			$this->CcrFPag->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->CcrFPag->CurrentValue, 7), ew_CurrentDate(), $this->CcrFPag->ReadOnly);

			// CcrMont
			$this->CcrMont->SetDbValueDef($rsnew, $this->CcrMont->CurrentValue, 0, $this->CcrMont->ReadOnly);

			// CcrMora
			$this->CcrMora->SetDbValueDef($rsnew, $this->CcrMora->CurrentValue, NULL, $this->CcrMora->ReadOnly);

			// CcrDAtr
			$this->CcrDAtr->SetDbValueDef($rsnew, $this->CcrDAtr->CurrentValue, NULL, $this->CcrDAtr->ReadOnly);

			// CcrEsta
			$this->CcrEsta->SetDbValueDef($rsnew, $this->CcrEsta->CurrentValue, "", $this->CcrEsta->ReadOnly);

			// CcrAnho
			$this->CcrAnho->SetDbValueDef($rsnew, $this->CcrAnho->CurrentValue, "", $this->CcrAnho->ReadOnly);

			// CcrMes
			$this->CcrMes->SetDbValueDef($rsnew, $this->CcrMes->CurrentValue, "", $this->CcrMes->ReadOnly);

			// CcrConc
			$this->CcrConc->SetDbValueDef($rsnew, $this->CcrConc->CurrentValue, "", $this->CcrConc->ReadOnly);

			// CcrUsua
			$this->CcrUsua->SetDbValueDef($rsnew, $this->CcrUsua->CurrentValue, "", $this->CcrUsua->ReadOnly);

			// CcrFCre
			$this->CcrFCre->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->CcrFCre->CurrentValue, 7), ew_CurrentDate(), $this->CcrFCre->ReadOnly);

			// Call Row Updating event
			$bUpdateRow = $this->Row_Updating($rsold, $rsnew);
			if ($bUpdateRow) {
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				if (count($rsnew) > 0)
					$EditRow = $this->Update($rsnew, "", $rsold);
				else
					$EditRow = TRUE; // No field to update
				$conn->raiseErrorFn = '';
				if ($EditRow) {
				}
			} else {
				if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

					// Use the message, do nothing
				} elseif ($this->CancelMessage <> "") {
					$this->setFailureMessage($this->CancelMessage);
					$this->CancelMessage = "";
				} else {
					$this->setFailureMessage($Language->Phrase("UpdateCancelled"));
				}
				$EditRow = FALSE;
			}
		}

		// Call Row_Updated event
		if ($EditRow)
			$this->Row_Updated($rsold, $rsnew);
		$rs->Close();
		return $EditRow;
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, "CCrelist.php", "", $this->TableVar, TRUE);
		$PageId = "edit";
		$Breadcrumb->Add("edit", $PageId, $url);
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($CCre_edit)) $CCre_edit = new cCCre_edit();

// Page init
$CCre_edit->Page_Init();

// Page main
$CCre_edit->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$CCre_edit->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "edit";
var CurrentForm = fCCreedit = new ew_Form("fCCreedit", "edit");

// Validate form
fCCreedit.Validate = function() {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.GetForm(), $fobj = $(fobj);
	if ($fobj.find("#a_confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.FormKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = $fobj.find("#a_list").val() == "gridinsert";
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
			elm = this.GetElements("x" + infix + "_VcrCodi");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $CCre->VcrCodi->FldCaption(), $CCre->VcrCodi->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_CcrNCuo");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $CCre->CcrNCuo->FldCaption(), $CCre->CcrNCuo->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_CcrNCuo");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($CCre->CcrNCuo->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_CcrFPag");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $CCre->CcrFPag->FldCaption(), $CCre->CcrFPag->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_CcrFPag");
			if (elm && !ew_CheckEuroDate(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($CCre->CcrFPag->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_CcrMont");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $CCre->CcrMont->FldCaption(), $CCre->CcrMont->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_CcrMont");
			if (elm && !ew_CheckNumber(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($CCre->CcrMont->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_CcrMora");
			if (elm && !ew_CheckNumber(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($CCre->CcrMora->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_CcrDAtr");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($CCre->CcrDAtr->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_CcrEsta");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $CCre->CcrEsta->FldCaption(), $CCre->CcrEsta->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_CcrAnho");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $CCre->CcrAnho->FldCaption(), $CCre->CcrAnho->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_CcrMes");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $CCre->CcrMes->FldCaption(), $CCre->CcrMes->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_CcrConc");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $CCre->CcrConc->FldCaption(), $CCre->CcrConc->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_CcrUsua");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $CCre->CcrUsua->FldCaption(), $CCre->CcrUsua->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_CcrFCre");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $CCre->CcrFCre->FldCaption(), $CCre->CcrFCre->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_CcrFCre");
			if (elm && !ew_CheckEuroDate(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($CCre->CcrFCre->FldErrMsg()) ?>");

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
	}

	// Process detail forms
	var dfs = $fobj.find("input[name='detailpage']").get();
	for (var i = 0; i < dfs.length; i++) {
		var df = dfs[i], val = df.value;
		if (val && ewForms[val])
			if (!ewForms[val].Validate())
				return false;
	}
	return true;
}

// Form_CustomValidate event
fCCreedit.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fCCreedit.ValidateRequired = true;
<?php } else { ?>
fCCreedit.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
fCCreedit.Lists["x_VcrCodi"] = {"LinkField":"x_VcrCodi","Ajax":true,"AutoFill":false,"DisplayFields":["x_VcrCodi","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};

// Form object for search
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php $CCre_edit->ShowPageHeader(); ?>
<?php
$CCre_edit->ShowMessage();
?>
<form name="fCCreedit" id="fCCreedit" class="<?php echo $CCre_edit->FormClassName ?>" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($CCre_edit->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $CCre_edit->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="CCre">
<input type="hidden" name="a_edit" id="a_edit" value="U">
<?php if ($CCre->CurrentAction == "F") { // Confirm page ?>
<input type="hidden" name="a_confirm" id="a_confirm" value="F">
<?php } ?>
<div>
<?php if ($CCre->CcrCodi->Visible) { // CcrCodi ?>
	<div id="r_CcrCodi" class="form-group">
		<label id="elh_CCre_CcrCodi" class="col-sm-2 control-label ewLabel"><?php echo $CCre->CcrCodi->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $CCre->CcrCodi->CellAttributes() ?>>
<?php if ($CCre->CurrentAction <> "F") { ?>
<span id="el_CCre_CcrCodi">
<span<?php echo $CCre->CcrCodi->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $CCre->CcrCodi->EditValue ?></p></span>
</span>
<input type="hidden" data-table="CCre" data-field="x_CcrCodi" name="x_CcrCodi" id="x_CcrCodi" value="<?php echo ew_HtmlEncode($CCre->CcrCodi->CurrentValue) ?>">
<?php } else { ?>
<span id="el_CCre_CcrCodi">
<span<?php echo $CCre->CcrCodi->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $CCre->CcrCodi->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="CCre" data-field="x_CcrCodi" name="x_CcrCodi" id="x_CcrCodi" value="<?php echo ew_HtmlEncode($CCre->CcrCodi->FormValue) ?>">
<?php } ?>
<?php echo $CCre->CcrCodi->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($CCre->VcrCodi->Visible) { // VcrCodi ?>
	<div id="r_VcrCodi" class="form-group">
		<label id="elh_CCre_VcrCodi" for="x_VcrCodi" class="col-sm-2 control-label ewLabel"><?php echo $CCre->VcrCodi->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $CCre->VcrCodi->CellAttributes() ?>>
<?php if ($CCre->CurrentAction <> "F") { ?>
<span id="el_CCre_VcrCodi">
<select data-table="CCre" data-field="x_VcrCodi" data-value-separator="<?php echo ew_HtmlEncode(is_array($CCre->VcrCodi->DisplayValueSeparator) ? json_encode($CCre->VcrCodi->DisplayValueSeparator) : $CCre->VcrCodi->DisplayValueSeparator) ?>" id="x_VcrCodi" name="x_VcrCodi"<?php echo $CCre->VcrCodi->EditAttributes() ?>>
<?php
if (is_array($CCre->VcrCodi->EditValue)) {
	$arwrk = $CCre->VcrCodi->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($CCre->VcrCodi->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $CCre->VcrCodi->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($CCre->VcrCodi->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($CCre->VcrCodi->CurrentValue) ?>" selected><?php echo $CCre->VcrCodi->CurrentValue ?></option>
<?php
    }
}
?>
</select>
<?php
$sSqlWrk = "SELECT \"VcrCodi\", \"VcrCodi\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"VCre\"";
$sWhereWrk = "";
$CCre->VcrCodi->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$CCre->VcrCodi->LookupFilters += array("f0" => "\"VcrCodi\" = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$CCre->Lookup_Selecting($CCre->VcrCodi, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $CCre->VcrCodi->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x_VcrCodi" id="s_x_VcrCodi" value="<?php echo $CCre->VcrCodi->LookupFilterQuery() ?>">
</span>
<?php } else { ?>
<span id="el_CCre_VcrCodi">
<span<?php echo $CCre->VcrCodi->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $CCre->VcrCodi->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="CCre" data-field="x_VcrCodi" name="x_VcrCodi" id="x_VcrCodi" value="<?php echo ew_HtmlEncode($CCre->VcrCodi->FormValue) ?>">
<?php } ?>
<?php echo $CCre->VcrCodi->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($CCre->CcrNCuo->Visible) { // CcrNCuo ?>
	<div id="r_CcrNCuo" class="form-group">
		<label id="elh_CCre_CcrNCuo" for="x_CcrNCuo" class="col-sm-2 control-label ewLabel"><?php echo $CCre->CcrNCuo->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $CCre->CcrNCuo->CellAttributes() ?>>
<?php if ($CCre->CurrentAction <> "F") { ?>
<span id="el_CCre_CcrNCuo">
<input type="text" data-table="CCre" data-field="x_CcrNCuo" name="x_CcrNCuo" id="x_CcrNCuo" size="30" placeholder="<?php echo ew_HtmlEncode($CCre->CcrNCuo->getPlaceHolder()) ?>" value="<?php echo $CCre->CcrNCuo->EditValue ?>"<?php echo $CCre->CcrNCuo->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_CCre_CcrNCuo">
<span<?php echo $CCre->CcrNCuo->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $CCre->CcrNCuo->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="CCre" data-field="x_CcrNCuo" name="x_CcrNCuo" id="x_CcrNCuo" value="<?php echo ew_HtmlEncode($CCre->CcrNCuo->FormValue) ?>">
<?php } ?>
<?php echo $CCre->CcrNCuo->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($CCre->CcrFPag->Visible) { // CcrFPag ?>
	<div id="r_CcrFPag" class="form-group">
		<label id="elh_CCre_CcrFPag" for="x_CcrFPag" class="col-sm-2 control-label ewLabel"><?php echo $CCre->CcrFPag->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $CCre->CcrFPag->CellAttributes() ?>>
<?php if ($CCre->CurrentAction <> "F") { ?>
<span id="el_CCre_CcrFPag">
<input type="text" data-table="CCre" data-field="x_CcrFPag" data-format="7" name="x_CcrFPag" id="x_CcrFPag" placeholder="<?php echo ew_HtmlEncode($CCre->CcrFPag->getPlaceHolder()) ?>" value="<?php echo $CCre->CcrFPag->EditValue ?>"<?php echo $CCre->CcrFPag->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_CCre_CcrFPag">
<span<?php echo $CCre->CcrFPag->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $CCre->CcrFPag->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="CCre" data-field="x_CcrFPag" name="x_CcrFPag" id="x_CcrFPag" value="<?php echo ew_HtmlEncode($CCre->CcrFPag->FormValue) ?>">
<?php } ?>
<?php echo $CCre->CcrFPag->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($CCre->CcrMont->Visible) { // CcrMont ?>
	<div id="r_CcrMont" class="form-group">
		<label id="elh_CCre_CcrMont" for="x_CcrMont" class="col-sm-2 control-label ewLabel"><?php echo $CCre->CcrMont->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $CCre->CcrMont->CellAttributes() ?>>
<?php if ($CCre->CurrentAction <> "F") { ?>
<span id="el_CCre_CcrMont">
<input type="text" data-table="CCre" data-field="x_CcrMont" name="x_CcrMont" id="x_CcrMont" size="30" placeholder="<?php echo ew_HtmlEncode($CCre->CcrMont->getPlaceHolder()) ?>" value="<?php echo $CCre->CcrMont->EditValue ?>"<?php echo $CCre->CcrMont->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_CCre_CcrMont">
<span<?php echo $CCre->CcrMont->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $CCre->CcrMont->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="CCre" data-field="x_CcrMont" name="x_CcrMont" id="x_CcrMont" value="<?php echo ew_HtmlEncode($CCre->CcrMont->FormValue) ?>">
<?php } ?>
<?php echo $CCre->CcrMont->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($CCre->CcrMora->Visible) { // CcrMora ?>
	<div id="r_CcrMora" class="form-group">
		<label id="elh_CCre_CcrMora" for="x_CcrMora" class="col-sm-2 control-label ewLabel"><?php echo $CCre->CcrMora->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $CCre->CcrMora->CellAttributes() ?>>
<?php if ($CCre->CurrentAction <> "F") { ?>
<span id="el_CCre_CcrMora">
<input type="text" data-table="CCre" data-field="x_CcrMora" name="x_CcrMora" id="x_CcrMora" size="30" placeholder="<?php echo ew_HtmlEncode($CCre->CcrMora->getPlaceHolder()) ?>" value="<?php echo $CCre->CcrMora->EditValue ?>"<?php echo $CCre->CcrMora->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_CCre_CcrMora">
<span<?php echo $CCre->CcrMora->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $CCre->CcrMora->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="CCre" data-field="x_CcrMora" name="x_CcrMora" id="x_CcrMora" value="<?php echo ew_HtmlEncode($CCre->CcrMora->FormValue) ?>">
<?php } ?>
<?php echo $CCre->CcrMora->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($CCre->CcrDAtr->Visible) { // CcrDAtr ?>
	<div id="r_CcrDAtr" class="form-group">
		<label id="elh_CCre_CcrDAtr" for="x_CcrDAtr" class="col-sm-2 control-label ewLabel"><?php echo $CCre->CcrDAtr->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $CCre->CcrDAtr->CellAttributes() ?>>
<?php if ($CCre->CurrentAction <> "F") { ?>
<span id="el_CCre_CcrDAtr">
<input type="text" data-table="CCre" data-field="x_CcrDAtr" name="x_CcrDAtr" id="x_CcrDAtr" size="30" placeholder="<?php echo ew_HtmlEncode($CCre->CcrDAtr->getPlaceHolder()) ?>" value="<?php echo $CCre->CcrDAtr->EditValue ?>"<?php echo $CCre->CcrDAtr->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_CCre_CcrDAtr">
<span<?php echo $CCre->CcrDAtr->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $CCre->CcrDAtr->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="CCre" data-field="x_CcrDAtr" name="x_CcrDAtr" id="x_CcrDAtr" value="<?php echo ew_HtmlEncode($CCre->CcrDAtr->FormValue) ?>">
<?php } ?>
<?php echo $CCre->CcrDAtr->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($CCre->CcrEsta->Visible) { // CcrEsta ?>
	<div id="r_CcrEsta" class="form-group">
		<label id="elh_CCre_CcrEsta" for="x_CcrEsta" class="col-sm-2 control-label ewLabel"><?php echo $CCre->CcrEsta->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $CCre->CcrEsta->CellAttributes() ?>>
<?php if ($CCre->CurrentAction <> "F") { ?>
<span id="el_CCre_CcrEsta">
<input type="text" data-table="CCre" data-field="x_CcrEsta" name="x_CcrEsta" id="x_CcrEsta" size="30" maxlength="1" placeholder="<?php echo ew_HtmlEncode($CCre->CcrEsta->getPlaceHolder()) ?>" value="<?php echo $CCre->CcrEsta->EditValue ?>"<?php echo $CCre->CcrEsta->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_CCre_CcrEsta">
<span<?php echo $CCre->CcrEsta->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $CCre->CcrEsta->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="CCre" data-field="x_CcrEsta" name="x_CcrEsta" id="x_CcrEsta" value="<?php echo ew_HtmlEncode($CCre->CcrEsta->FormValue) ?>">
<?php } ?>
<?php echo $CCre->CcrEsta->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($CCre->CcrAnho->Visible) { // CcrAnho ?>
	<div id="r_CcrAnho" class="form-group">
		<label id="elh_CCre_CcrAnho" for="x_CcrAnho" class="col-sm-2 control-label ewLabel"><?php echo $CCre->CcrAnho->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $CCre->CcrAnho->CellAttributes() ?>>
<?php if ($CCre->CurrentAction <> "F") { ?>
<span id="el_CCre_CcrAnho">
<input type="text" data-table="CCre" data-field="x_CcrAnho" name="x_CcrAnho" id="x_CcrAnho" size="30" maxlength="4" placeholder="<?php echo ew_HtmlEncode($CCre->CcrAnho->getPlaceHolder()) ?>" value="<?php echo $CCre->CcrAnho->EditValue ?>"<?php echo $CCre->CcrAnho->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_CCre_CcrAnho">
<span<?php echo $CCre->CcrAnho->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $CCre->CcrAnho->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="CCre" data-field="x_CcrAnho" name="x_CcrAnho" id="x_CcrAnho" value="<?php echo ew_HtmlEncode($CCre->CcrAnho->FormValue) ?>">
<?php } ?>
<?php echo $CCre->CcrAnho->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($CCre->CcrMes->Visible) { // CcrMes ?>
	<div id="r_CcrMes" class="form-group">
		<label id="elh_CCre_CcrMes" for="x_CcrMes" class="col-sm-2 control-label ewLabel"><?php echo $CCre->CcrMes->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $CCre->CcrMes->CellAttributes() ?>>
<?php if ($CCre->CurrentAction <> "F") { ?>
<span id="el_CCre_CcrMes">
<input type="text" data-table="CCre" data-field="x_CcrMes" name="x_CcrMes" id="x_CcrMes" size="30" maxlength="2" placeholder="<?php echo ew_HtmlEncode($CCre->CcrMes->getPlaceHolder()) ?>" value="<?php echo $CCre->CcrMes->EditValue ?>"<?php echo $CCre->CcrMes->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_CCre_CcrMes">
<span<?php echo $CCre->CcrMes->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $CCre->CcrMes->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="CCre" data-field="x_CcrMes" name="x_CcrMes" id="x_CcrMes" value="<?php echo ew_HtmlEncode($CCre->CcrMes->FormValue) ?>">
<?php } ?>
<?php echo $CCre->CcrMes->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($CCre->CcrConc->Visible) { // CcrConc ?>
	<div id="r_CcrConc" class="form-group">
		<label id="elh_CCre_CcrConc" for="x_CcrConc" class="col-sm-2 control-label ewLabel"><?php echo $CCre->CcrConc->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $CCre->CcrConc->CellAttributes() ?>>
<?php if ($CCre->CurrentAction <> "F") { ?>
<span id="el_CCre_CcrConc">
<input type="text" data-table="CCre" data-field="x_CcrConc" name="x_CcrConc" id="x_CcrConc" size="30" maxlength="25" placeholder="<?php echo ew_HtmlEncode($CCre->CcrConc->getPlaceHolder()) ?>" value="<?php echo $CCre->CcrConc->EditValue ?>"<?php echo $CCre->CcrConc->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_CCre_CcrConc">
<span<?php echo $CCre->CcrConc->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $CCre->CcrConc->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="CCre" data-field="x_CcrConc" name="x_CcrConc" id="x_CcrConc" value="<?php echo ew_HtmlEncode($CCre->CcrConc->FormValue) ?>">
<?php } ?>
<?php echo $CCre->CcrConc->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($CCre->CcrUsua->Visible) { // CcrUsua ?>
	<div id="r_CcrUsua" class="form-group">
		<label id="elh_CCre_CcrUsua" for="x_CcrUsua" class="col-sm-2 control-label ewLabel"><?php echo $CCre->CcrUsua->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $CCre->CcrUsua->CellAttributes() ?>>
<?php if ($CCre->CurrentAction <> "F") { ?>
<span id="el_CCre_CcrUsua">
<input type="text" data-table="CCre" data-field="x_CcrUsua" name="x_CcrUsua" id="x_CcrUsua" size="30" maxlength="50" placeholder="<?php echo ew_HtmlEncode($CCre->CcrUsua->getPlaceHolder()) ?>" value="<?php echo $CCre->CcrUsua->EditValue ?>"<?php echo $CCre->CcrUsua->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_CCre_CcrUsua">
<span<?php echo $CCre->CcrUsua->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $CCre->CcrUsua->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="CCre" data-field="x_CcrUsua" name="x_CcrUsua" id="x_CcrUsua" value="<?php echo ew_HtmlEncode($CCre->CcrUsua->FormValue) ?>">
<?php } ?>
<?php echo $CCre->CcrUsua->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($CCre->CcrFCre->Visible) { // CcrFCre ?>
	<div id="r_CcrFCre" class="form-group">
		<label id="elh_CCre_CcrFCre" for="x_CcrFCre" class="col-sm-2 control-label ewLabel"><?php echo $CCre->CcrFCre->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $CCre->CcrFCre->CellAttributes() ?>>
<?php if ($CCre->CurrentAction <> "F") { ?>
<span id="el_CCre_CcrFCre">
<input type="text" data-table="CCre" data-field="x_CcrFCre" data-format="7" name="x_CcrFCre" id="x_CcrFCre" placeholder="<?php echo ew_HtmlEncode($CCre->CcrFCre->getPlaceHolder()) ?>" value="<?php echo $CCre->CcrFCre->EditValue ?>"<?php echo $CCre->CcrFCre->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_CCre_CcrFCre">
<span<?php echo $CCre->CcrFCre->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $CCre->CcrFCre->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="CCre" data-field="x_CcrFCre" name="x_CcrFCre" id="x_CcrFCre" value="<?php echo ew_HtmlEncode($CCre->CcrFCre->FormValue) ?>">
<?php } ?>
<?php echo $CCre->CcrFCre->CustomMsg ?></div></div>
	</div>
<?php } ?>
</div>
<div class="form-group">
	<div class="col-sm-offset-2 col-sm-10">
<?php if ($CCre->CurrentAction <> "F") { // Confirm page ?>
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit" onclick="this.form.a_edit.value='F';"><?php echo $Language->Phrase("SaveBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="button" data-href="<?php echo $CCre_edit->getReturnUrl() ?>"><?php echo $Language->Phrase("CancelBtn") ?></button>
<?php } else { ?>
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("ConfirmBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="submit" onclick="this.form.a_edit.value='X';"><?php echo $Language->Phrase("CancelBtn") ?></button>
<?php } ?>
	</div>
</div>
</form>
<script type="text/javascript">
fCCreedit.Init();
</script>
<?php
$CCre_edit->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$CCre_edit->Page_Terminate();
?>
