<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "Provinfo.php" ?>
<?php include_once "Usuainfo.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$Prov_delete = NULL; // Initialize page object first

class cProv_delete extends cProv {

	// Page ID
	var $PageID = 'delete';

	// Project ID
	var $ProjectID = "{04439FF7-B43F-460F-8514-F71C8FF9E679}";

	// Table name
	var $TableName = 'Prov';

	// Page object name
	var $PageObjName = 'Prov_delete';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (Prov)
		if (!isset($GLOBALS["Prov"]) || get_class($GLOBALS["Prov"]) == "cProv") {
			$GLOBALS["Prov"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["Prov"];
		}

		// Table object (Usua)
		if (!isset($GLOBALS['Usua'])) $GLOBALS['Usua'] = new cUsua();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'delete', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'Prov', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (Usua)
		if (!isset($UserTable)) {
			$UserTable = new cUsua();
			$UserTableConn = Conn($UserTable->DBID);
		}
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanDelete()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage($Language->Phrase("NoPermission")); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("Provlist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $Prov;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($Prov);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $DbMasterFilter = "";
	var $DbDetailFilter = "";
	var $StartRec;
	var $TotalRecs = 0;
	var $RecCnt;
	var $RecKeys = array();
	var $Recordset;
	var $StartRowCnt = 1;
	var $RowCnt = 0;

	//
	// Page main
	//
	function Page_Main() {
		global $Language;

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Load key parameters
		$this->RecKeys = $this->GetRecordKeys(); // Load record keys
		$sFilter = $this->GetKeyFilter();
		if ($sFilter == "")
			$this->Page_Terminate("Provlist.php"); // Prevent SQL injection, return to list

		// Set up filter (SQL WHHERE clause) and get return SQL
		// SQL constructor in Prov class, Provinfo.php

		$this->CurrentFilter = $sFilter;

		// Get action
		if (@$_POST["a_delete"] <> "") {
			$this->CurrentAction = $_POST["a_delete"];
		} else {
			$this->CurrentAction = "I"; // Display record
		}
		switch ($this->CurrentAction) {
			case "D": // Delete
				$this->SendEmail = TRUE; // Send email on delete success
				if ($this->DeleteRows()) { // Delete rows
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage($Language->Phrase("DeleteSuccess")); // Set up success message
					$this->Page_Terminate($this->getReturnUrl()); // Return to caller
				}
		}
	}

	// Load recordset
	function LoadRecordset($offset = -1, $rowcnt = -1) {

		// Load List page SQL
		$sSql = $this->SelectSQL();
		$conn = &$this->Connection();

		// Load recordset
		$dbtype = ew_GetConnectionType($this->DBID);
		if ($this->UseSelectLimit) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			if ($dbtype == "MSSQL") {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset, array("_hasOrderBy" => trim($this->getOrderBy()) || trim($this->getSessionOrderBy())));
			} else {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset);
			}
			$conn->raiseErrorFn = '';
		} else {
			$rs = ew_LoadRecordset($sSql, $conn);
		}

		// Call Recordset Selected event
		$this->Recordset_Selected($rs);
		return $rs;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->PrvCodi->setDbValue($rs->fields('PrvCodi'));
		$this->PrvFIni->setDbValue($rs->fields('PrvFIni'));
		$this->PrvFFin->setDbValue($rs->fields('PrvFFin'));
		$this->PrvPais->setDbValue($rs->fields('PrvPais'));
		$this->PrvTipo->setDbValue($rs->fields('PrvTipo'));
		$this->PrvNomb->setDbValue($rs->fields('PrvNomb'));
		$this->PrvApel->setDbValue($rs->fields('PrvApel'));
		$this->PrvCRuc->setDbValue($rs->fields('PrvCRuc'));
		$this->PrvDire->setDbValue($rs->fields('PrvDire'));
		$this->PrvMail->setDbValue($rs->fields('PrvMail'));
		$this->PrvTele->setDbValue($rs->fields('PrvTele'));
		$this->PrvRSoc->setDbValue($rs->fields('PrvRSoc'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->PrvCodi->DbValue = $row['PrvCodi'];
		$this->PrvFIni->DbValue = $row['PrvFIni'];
		$this->PrvFFin->DbValue = $row['PrvFFin'];
		$this->PrvPais->DbValue = $row['PrvPais'];
		$this->PrvTipo->DbValue = $row['PrvTipo'];
		$this->PrvNomb->DbValue = $row['PrvNomb'];
		$this->PrvApel->DbValue = $row['PrvApel'];
		$this->PrvCRuc->DbValue = $row['PrvCRuc'];
		$this->PrvDire->DbValue = $row['PrvDire'];
		$this->PrvMail->DbValue = $row['PrvMail'];
		$this->PrvTele->DbValue = $row['PrvTele'];
		$this->PrvRSoc->DbValue = $row['PrvRSoc'];
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		// Call Row_Rendering event

		$this->Row_Rendering();

		// Common render codes for all row types
		// PrvCodi
		// PrvFIni
		// PrvFFin
		// PrvPais
		// PrvTipo
		// PrvNomb
		// PrvApel
		// PrvCRuc
		// PrvDire
		// PrvMail
		// PrvTele
		// PrvRSoc

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// PrvCodi
		$this->PrvCodi->ViewValue = $this->PrvCodi->CurrentValue;
		$this->PrvCodi->ViewCustomAttributes = "";

		// PrvFIni
		$this->PrvFIni->ViewValue = $this->PrvFIni->CurrentValue;
		$this->PrvFIni->ViewValue = ew_FormatDateTime($this->PrvFIni->ViewValue, 7);
		$this->PrvFIni->ViewCustomAttributes = "";

		// PrvFFin
		$this->PrvFFin->ViewValue = $this->PrvFFin->CurrentValue;
		$this->PrvFFin->ViewValue = ew_FormatDateTime($this->PrvFFin->ViewValue, 7);
		$this->PrvFFin->ViewCustomAttributes = "";

		// PrvPais
		$this->PrvPais->ViewValue = $this->PrvPais->CurrentValue;
		$this->PrvPais->ViewCustomAttributes = "";

		// PrvTipo
		$this->PrvTipo->ViewValue = $this->PrvTipo->CurrentValue;
		$this->PrvTipo->ViewCustomAttributes = "";

		// PrvNomb
		$this->PrvNomb->ViewValue = $this->PrvNomb->CurrentValue;
		$this->PrvNomb->ViewCustomAttributes = "";

		// PrvApel
		$this->PrvApel->ViewValue = $this->PrvApel->CurrentValue;
		$this->PrvApel->ViewCustomAttributes = "";

		// PrvCRuc
		$this->PrvCRuc->ViewValue = $this->PrvCRuc->CurrentValue;
		$this->PrvCRuc->ViewCustomAttributes = "";

		// PrvDire
		$this->PrvDire->ViewValue = $this->PrvDire->CurrentValue;
		$this->PrvDire->ViewCustomAttributes = "";

		// PrvMail
		$this->PrvMail->ViewValue = $this->PrvMail->CurrentValue;
		$this->PrvMail->ViewCustomAttributes = "";

		// PrvTele
		$this->PrvTele->ViewValue = $this->PrvTele->CurrentValue;
		$this->PrvTele->ViewCustomAttributes = "";

		// PrvRSoc
		$this->PrvRSoc->ViewValue = $this->PrvRSoc->CurrentValue;
		$this->PrvRSoc->ViewCustomAttributes = "";

			// PrvFIni
			$this->PrvFIni->LinkCustomAttributes = "";
			$this->PrvFIni->HrefValue = "";
			$this->PrvFIni->TooltipValue = "";

			// PrvFFin
			$this->PrvFFin->LinkCustomAttributes = "";
			$this->PrvFFin->HrefValue = "";
			$this->PrvFFin->TooltipValue = "";

			// PrvPais
			$this->PrvPais->LinkCustomAttributes = "";
			$this->PrvPais->HrefValue = "";
			$this->PrvPais->TooltipValue = "";

			// PrvTipo
			$this->PrvTipo->LinkCustomAttributes = "";
			$this->PrvTipo->HrefValue = "";
			$this->PrvTipo->TooltipValue = "";

			// PrvNomb
			$this->PrvNomb->LinkCustomAttributes = "";
			$this->PrvNomb->HrefValue = "";
			$this->PrvNomb->TooltipValue = "";

			// PrvApel
			$this->PrvApel->LinkCustomAttributes = "";
			$this->PrvApel->HrefValue = "";
			$this->PrvApel->TooltipValue = "";

			// PrvCRuc
			$this->PrvCRuc->LinkCustomAttributes = "";
			$this->PrvCRuc->HrefValue = "";
			$this->PrvCRuc->TooltipValue = "";

			// PrvDire
			$this->PrvDire->LinkCustomAttributes = "";
			$this->PrvDire->HrefValue = "";
			$this->PrvDire->TooltipValue = "";

			// PrvMail
			$this->PrvMail->LinkCustomAttributes = "";
			$this->PrvMail->HrefValue = "";
			$this->PrvMail->TooltipValue = "";

			// PrvTele
			$this->PrvTele->LinkCustomAttributes = "";
			$this->PrvTele->HrefValue = "";
			$this->PrvTele->TooltipValue = "";

			// PrvRSoc
			$this->PrvRSoc->LinkCustomAttributes = "";
			$this->PrvRSoc->HrefValue = "";
			$this->PrvRSoc->TooltipValue = "";
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	//
	// Delete records based on current filter
	//
	function DeleteRows() {
		global $Language, $Security;
		if (!$Security->CanDelete()) {
			$this->setFailureMessage($Language->Phrase("NoDeletePermission")); // No delete permission
			return FALSE;
		}
		$DeleteRows = TRUE;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE) {
			return FALSE;
		} elseif ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
			$rs->Close();
			return FALSE;

		//} else {
		//	$this->LoadRowValues($rs); // Load row values

		}
		$rows = ($rs) ? $rs->GetRows() : array();
		$conn->BeginTrans();

		// Clone old rows
		$rsold = $rows;
		if ($rs)
			$rs->Close();

		// Call row deleting event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$DeleteRows = $this->Row_Deleting($row);
				if (!$DeleteRows) break;
			}
		}
		if ($DeleteRows) {
			$sKey = "";
			foreach ($rsold as $row) {
				$sThisKey = "";
				if ($sThisKey <> "") $sThisKey .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
				$sThisKey .= $row['PrvCodi'];
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				$DeleteRows = $this->Delete($row); // Delete
				$conn->raiseErrorFn = '';
				if ($DeleteRows === FALSE)
					break;
				if ($sKey <> "") $sKey .= ", ";
				$sKey .= $sThisKey;
			}
		} else {

			// Set up error message
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("DeleteCancelled"));
			}
		}
		if ($DeleteRows) {
			$conn->CommitTrans(); // Commit the changes
		} else {
			$conn->RollbackTrans(); // Rollback changes
		}

		// Call Row Deleted event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$this->Row_Deleted($row);
			}
		}
		return $DeleteRows;
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, "Provlist.php", "", $this->TableVar, TRUE);
		$PageId = "delete";
		$Breadcrumb->Add("delete", $PageId, $url);
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($Prov_delete)) $Prov_delete = new cProv_delete();

// Page init
$Prov_delete->Page_Init();

// Page main
$Prov_delete->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$Prov_delete->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "delete";
var CurrentForm = fProvdelete = new ew_Form("fProvdelete", "delete");

// Form_CustomValidate event
fProvdelete.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fProvdelete.ValidateRequired = true;
<?php } else { ?>
fProvdelete.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
// Form object for search

</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<?php

// Load records for display
if ($Prov_delete->Recordset = $Prov_delete->LoadRecordset())
	$Prov_deleteTotalRecs = $Prov_delete->Recordset->RecordCount(); // Get record count
if ($Prov_deleteTotalRecs <= 0) { // No record found, exit
	if ($Prov_delete->Recordset)
		$Prov_delete->Recordset->Close();
	$Prov_delete->Page_Terminate("Provlist.php"); // Return to list
}
?>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php $Prov_delete->ShowPageHeader(); ?>
<?php
$Prov_delete->ShowMessage();
?>
<form name="fProvdelete" id="fProvdelete" class="form-inline ewForm ewDeleteForm" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($Prov_delete->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $Prov_delete->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="Prov">
<input type="hidden" name="a_delete" id="a_delete" value="D">
<?php foreach ($Prov_delete->RecKeys as $key) { ?>
<?php $keyvalue = is_array($key) ? implode($EW_COMPOSITE_KEY_SEPARATOR, $key) : $key; ?>
<input type="hidden" name="key_m[]" value="<?php echo ew_HtmlEncode($keyvalue) ?>">
<?php } ?>
<div class="ewGrid">
<div class="<?php if (ew_IsResponsiveLayout()) { echo "table-responsive "; } ?>ewGridMiddlePanel">
<table class="table ewTable">
<?php echo $Prov->TableCustomInnerHtml ?>
	<thead>
	<tr class="ewTableHeader">
<?php if ($Prov->PrvFIni->Visible) { // PrvFIni ?>
		<th><span id="elh_Prov_PrvFIni" class="Prov_PrvFIni"><?php echo $Prov->PrvFIni->FldCaption() ?></span></th>
<?php } ?>
<?php if ($Prov->PrvFFin->Visible) { // PrvFFin ?>
		<th><span id="elh_Prov_PrvFFin" class="Prov_PrvFFin"><?php echo $Prov->PrvFFin->FldCaption() ?></span></th>
<?php } ?>
<?php if ($Prov->PrvPais->Visible) { // PrvPais ?>
		<th><span id="elh_Prov_PrvPais" class="Prov_PrvPais"><?php echo $Prov->PrvPais->FldCaption() ?></span></th>
<?php } ?>
<?php if ($Prov->PrvTipo->Visible) { // PrvTipo ?>
		<th><span id="elh_Prov_PrvTipo" class="Prov_PrvTipo"><?php echo $Prov->PrvTipo->FldCaption() ?></span></th>
<?php } ?>
<?php if ($Prov->PrvNomb->Visible) { // PrvNomb ?>
		<th><span id="elh_Prov_PrvNomb" class="Prov_PrvNomb"><?php echo $Prov->PrvNomb->FldCaption() ?></span></th>
<?php } ?>
<?php if ($Prov->PrvApel->Visible) { // PrvApel ?>
		<th><span id="elh_Prov_PrvApel" class="Prov_PrvApel"><?php echo $Prov->PrvApel->FldCaption() ?></span></th>
<?php } ?>
<?php if ($Prov->PrvCRuc->Visible) { // PrvCRuc ?>
		<th><span id="elh_Prov_PrvCRuc" class="Prov_PrvCRuc"><?php echo $Prov->PrvCRuc->FldCaption() ?></span></th>
<?php } ?>
<?php if ($Prov->PrvDire->Visible) { // PrvDire ?>
		<th><span id="elh_Prov_PrvDire" class="Prov_PrvDire"><?php echo $Prov->PrvDire->FldCaption() ?></span></th>
<?php } ?>
<?php if ($Prov->PrvMail->Visible) { // PrvMail ?>
		<th><span id="elh_Prov_PrvMail" class="Prov_PrvMail"><?php echo $Prov->PrvMail->FldCaption() ?></span></th>
<?php } ?>
<?php if ($Prov->PrvTele->Visible) { // PrvTele ?>
		<th><span id="elh_Prov_PrvTele" class="Prov_PrvTele"><?php echo $Prov->PrvTele->FldCaption() ?></span></th>
<?php } ?>
<?php if ($Prov->PrvRSoc->Visible) { // PrvRSoc ?>
		<th><span id="elh_Prov_PrvRSoc" class="Prov_PrvRSoc"><?php echo $Prov->PrvRSoc->FldCaption() ?></span></th>
<?php } ?>
	</tr>
	</thead>
	<tbody>
<?php
$Prov_delete->RecCnt = 0;
$i = 0;
while (!$Prov_delete->Recordset->EOF) {
	$Prov_delete->RecCnt++;
	$Prov_delete->RowCnt++;

	// Set row properties
	$Prov->ResetAttrs();
	$Prov->RowType = EW_ROWTYPE_VIEW; // View

	// Get the field contents
	$Prov_delete->LoadRowValues($Prov_delete->Recordset);

	// Render row
	$Prov_delete->RenderRow();
?>
	<tr<?php echo $Prov->RowAttributes() ?>>
<?php if ($Prov->PrvFIni->Visible) { // PrvFIni ?>
		<td<?php echo $Prov->PrvFIni->CellAttributes() ?>>
<span id="el<?php echo $Prov_delete->RowCnt ?>_Prov_PrvFIni" class="Prov_PrvFIni">
<span<?php echo $Prov->PrvFIni->ViewAttributes() ?>>
<?php echo $Prov->PrvFIni->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($Prov->PrvFFin->Visible) { // PrvFFin ?>
		<td<?php echo $Prov->PrvFFin->CellAttributes() ?>>
<span id="el<?php echo $Prov_delete->RowCnt ?>_Prov_PrvFFin" class="Prov_PrvFFin">
<span<?php echo $Prov->PrvFFin->ViewAttributes() ?>>
<?php echo $Prov->PrvFFin->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($Prov->PrvPais->Visible) { // PrvPais ?>
		<td<?php echo $Prov->PrvPais->CellAttributes() ?>>
<span id="el<?php echo $Prov_delete->RowCnt ?>_Prov_PrvPais" class="Prov_PrvPais">
<span<?php echo $Prov->PrvPais->ViewAttributes() ?>>
<?php echo $Prov->PrvPais->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($Prov->PrvTipo->Visible) { // PrvTipo ?>
		<td<?php echo $Prov->PrvTipo->CellAttributes() ?>>
<span id="el<?php echo $Prov_delete->RowCnt ?>_Prov_PrvTipo" class="Prov_PrvTipo">
<span<?php echo $Prov->PrvTipo->ViewAttributes() ?>>
<?php echo $Prov->PrvTipo->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($Prov->PrvNomb->Visible) { // PrvNomb ?>
		<td<?php echo $Prov->PrvNomb->CellAttributes() ?>>
<span id="el<?php echo $Prov_delete->RowCnt ?>_Prov_PrvNomb" class="Prov_PrvNomb">
<span<?php echo $Prov->PrvNomb->ViewAttributes() ?>>
<?php echo $Prov->PrvNomb->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($Prov->PrvApel->Visible) { // PrvApel ?>
		<td<?php echo $Prov->PrvApel->CellAttributes() ?>>
<span id="el<?php echo $Prov_delete->RowCnt ?>_Prov_PrvApel" class="Prov_PrvApel">
<span<?php echo $Prov->PrvApel->ViewAttributes() ?>>
<?php echo $Prov->PrvApel->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($Prov->PrvCRuc->Visible) { // PrvCRuc ?>
		<td<?php echo $Prov->PrvCRuc->CellAttributes() ?>>
<span id="el<?php echo $Prov_delete->RowCnt ?>_Prov_PrvCRuc" class="Prov_PrvCRuc">
<span<?php echo $Prov->PrvCRuc->ViewAttributes() ?>>
<?php echo $Prov->PrvCRuc->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($Prov->PrvDire->Visible) { // PrvDire ?>
		<td<?php echo $Prov->PrvDire->CellAttributes() ?>>
<span id="el<?php echo $Prov_delete->RowCnt ?>_Prov_PrvDire" class="Prov_PrvDire">
<span<?php echo $Prov->PrvDire->ViewAttributes() ?>>
<?php echo $Prov->PrvDire->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($Prov->PrvMail->Visible) { // PrvMail ?>
		<td<?php echo $Prov->PrvMail->CellAttributes() ?>>
<span id="el<?php echo $Prov_delete->RowCnt ?>_Prov_PrvMail" class="Prov_PrvMail">
<span<?php echo $Prov->PrvMail->ViewAttributes() ?>>
<?php echo $Prov->PrvMail->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($Prov->PrvTele->Visible) { // PrvTele ?>
		<td<?php echo $Prov->PrvTele->CellAttributes() ?>>
<span id="el<?php echo $Prov_delete->RowCnt ?>_Prov_PrvTele" class="Prov_PrvTele">
<span<?php echo $Prov->PrvTele->ViewAttributes() ?>>
<?php echo $Prov->PrvTele->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($Prov->PrvRSoc->Visible) { // PrvRSoc ?>
		<td<?php echo $Prov->PrvRSoc->CellAttributes() ?>>
<span id="el<?php echo $Prov_delete->RowCnt ?>_Prov_PrvRSoc" class="Prov_PrvRSoc">
<span<?php echo $Prov->PrvRSoc->ViewAttributes() ?>>
<?php echo $Prov->PrvRSoc->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
	</tr>
<?php
	$Prov_delete->Recordset->MoveNext();
}
$Prov_delete->Recordset->Close();
?>
</tbody>
</table>
</div>
</div>
<div>
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("DeleteBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="button" data-href="<?php echo $Prov_delete->getReturnUrl() ?>"><?php echo $Language->Phrase("CancelBtn") ?></button>
</div>
</form>
<script type="text/javascript">
fProvdelete.Init();
</script>
<?php
$Prov_delete->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$Prov_delete->Page_Terminate();
?>
