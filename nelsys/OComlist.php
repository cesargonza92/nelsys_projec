<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "OCominfo.php" ?>
<?php include_once "Usuainfo.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$OCom_list = NULL; // Initialize page object first

class cOCom_list extends cOCom {

	// Page ID
	var $PageID = 'list';

	// Project ID
	var $ProjectID = "{04439FF7-B43F-460F-8514-F71C8FF9E679}";

	// Table name
	var $TableName = 'OCom';

	// Page object name
	var $PageObjName = 'OCom_list';

	// Grid form hidden field names
	var $FormName = 'fOComlist';
	var $FormActionName = 'k_action';
	var $FormKeyName = 'k_key';
	var $FormOldKeyName = 'k_oldkey';
	var $FormBlankRowName = 'k_blankrow';
	var $FormKeyCountName = 'key_count';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Page URLs
	var $AddUrl;
	var $EditUrl;
	var $CopyUrl;
	var $DeleteUrl;
	var $ViewUrl;
	var $ListUrl;

	// Export URLs
	var $ExportPrintUrl;
	var $ExportHtmlUrl;
	var $ExportExcelUrl;
	var $ExportWordUrl;
	var $ExportXmlUrl;
	var $ExportCsvUrl;
	var $ExportPdfUrl;

	// Custom export
	var $ExportExcelCustom = FALSE;
	var $ExportWordCustom = FALSE;
	var $ExportPdfCustom = FALSE;
	var $ExportEmailCustom = FALSE;

	// Update URLs
	var $InlineAddUrl;
	var $InlineCopyUrl;
	var $InlineEditUrl;
	var $GridAddUrl;
	var $GridEditUrl;
	var $MultiDeleteUrl;
	var $MultiUpdateUrl;

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (OCom)
		if (!isset($GLOBALS["OCom"]) || get_class($GLOBALS["OCom"]) == "cOCom") {
			$GLOBALS["OCom"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["OCom"];
		}

		// Initialize URLs
		$this->ExportPrintUrl = $this->PageUrl() . "export=print";
		$this->ExportExcelUrl = $this->PageUrl() . "export=excel";
		$this->ExportWordUrl = $this->PageUrl() . "export=word";
		$this->ExportHtmlUrl = $this->PageUrl() . "export=html";
		$this->ExportXmlUrl = $this->PageUrl() . "export=xml";
		$this->ExportCsvUrl = $this->PageUrl() . "export=csv";
		$this->ExportPdfUrl = $this->PageUrl() . "export=pdf";
		$this->AddUrl = "OComadd.php";
		$this->InlineAddUrl = $this->PageUrl() . "a=add";
		$this->GridAddUrl = $this->PageUrl() . "a=gridadd";
		$this->GridEditUrl = $this->PageUrl() . "a=gridedit";
		$this->MultiDeleteUrl = "OComdelete.php";
		$this->MultiUpdateUrl = "OComupdate.php";

		// Table object (Usua)
		if (!isset($GLOBALS['Usua'])) $GLOBALS['Usua'] = new cUsua();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'list', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'OCom', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (Usua)
		if (!isset($UserTable)) {
			$UserTable = new cUsua();
			$UserTableConn = Conn($UserTable->DBID);
		}

		// List options
		$this->ListOptions = new cListOptions();
		$this->ListOptions->TableVar = $this->TableVar;

		// Export options
		$this->ExportOptions = new cListOptions();
		$this->ExportOptions->Tag = "div";
		$this->ExportOptions->TagClassName = "ewExportOption";

		// Other options
		$this->OtherOptions['addedit'] = new cListOptions();
		$this->OtherOptions['addedit']->Tag = "div";
		$this->OtherOptions['addedit']->TagClassName = "ewAddEditOption";
		$this->OtherOptions['detail'] = new cListOptions();
		$this->OtherOptions['detail']->Tag = "div";
		$this->OtherOptions['detail']->TagClassName = "ewDetailOption";
		$this->OtherOptions['action'] = new cListOptions();
		$this->OtherOptions['action']->Tag = "div";
		$this->OtherOptions['action']->TagClassName = "ewActionOption";

		// Filter options
		$this->FilterOptions = new cListOptions();
		$this->FilterOptions->Tag = "div";
		$this->FilterOptions->TagClassName = "ewFilterOption fOComlistsrch";

		// List actions
		$this->ListActions = new cListActions();
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanList()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage($Language->Phrase("NoPermission")); // Set no permission
			$this->Page_Terminate(ew_GetUrl("index.php"));
		}

		// Create form object
		$objForm = new cFormObj();
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action

		// Get grid add count
		$gridaddcnt = @$_GET[EW_TABLE_GRID_ADD_ROW_COUNT];
		if (is_numeric($gridaddcnt) && $gridaddcnt > 0)
			$this->GridAddRowCount = $gridaddcnt;

		// Set up list options
		$this->SetupListOptions();
		$this->OcoCodi->Visible = !$this->IsAdd() && !$this->IsCopy() && !$this->IsGridAdd();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Process auto fill
		if (@$_POST["ajax"] == "autofill") {
			$results = $this->GetAutoFill(@$_POST["name"], @$_POST["q"]);
			if ($results) {

				// Clean output buffer
				if (!EW_DEBUG_ENABLED && ob_get_length())
					ob_end_clean();
				echo $results;
				$this->Page_Terminate();
				exit();
			}
		}

		// Create Token
		$this->CreateToken();

		// Setup other options
		$this->SetupOtherOptions();

		// Set up custom action (compatible with old version)
		foreach ($this->CustomActions as $name => $action)
			$this->ListActions->Add($name, $action);

		// Show checkbox column if multiple action
		foreach ($this->ListActions->Items as $listaction) {
			if ($listaction->Select == EW_ACTION_MULTIPLE && $listaction->Allow) {
				$this->ListOptions->Items["checkbox"]->Visible = TRUE;
				break;
			}
		}
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $OCom;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($OCom);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}

	// Class variables
	var $ListOptions; // List options
	var $ExportOptions; // Export options
	var $SearchOptions; // Search options
	var $OtherOptions = array(); // Other options
	var $FilterOptions; // Filter options
	var $ListActions; // List actions
	var $SelectedCount = 0;
	var $SelectedIndex = 0;
	var $DisplayRecs = 20;
	var $StartRec;
	var $StopRec;
	var $TotalRecs = 0;
	var $RecRange = 10;
	var $Pager;
	var $DefaultSearchWhere = ""; // Default search WHERE clause
	var $SearchWhere = ""; // Search WHERE clause
	var $RecCnt = 0; // Record count
	var $EditRowCnt;
	var $StartRowCnt = 1;
	var $RowCnt = 0;
	var $Attrs = array(); // Row attributes and cell attributes
	var $RowIndex = 0; // Row index
	var $KeyCount = 0; // Key count
	var $RowAction = ""; // Row action
	var $RowOldKey = ""; // Row old key (for copy)
	var $RecPerRow = 0;
	var $MultiColumnClass;
	var $MultiColumnEditClass = "col-sm-12";
	var $MultiColumnCnt = 12;
	var $MultiColumnEditCnt = 12;
	var $GridCnt = 0;
	var $ColCnt = 0;
	var $DbMasterFilter = ""; // Master filter
	var $DbDetailFilter = ""; // Detail filter
	var $MasterRecordExists;	
	var $MultiSelectKey;
	var $Command;
	var $RestoreSearch = FALSE;
	var $DetailPages;
	var $Recordset;
	var $OldRecordset;

	//
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError, $gsSearchError, $Security;

		// Search filters
		$sSrchAdvanced = ""; // Advanced search filter
		$sSrchBasic = ""; // Basic search filter
		$sFilter = "";

		// Get command
		$this->Command = strtolower(@$_GET["cmd"]);
		if ($this->IsPageRequest()) { // Validate request

			// Process list action first
			if ($this->ProcessListAction()) // Ajax request
				$this->Page_Terminate();

			// Handle reset command
			$this->ResetCmd();

			// Set up Breadcrumb
			if ($this->Export == "")
				$this->SetupBreadcrumb();

			// Check QueryString parameters
			if (@$_GET["a"] <> "") {
				$this->CurrentAction = $_GET["a"];

				// Clear inline mode
				if ($this->CurrentAction == "cancel")
					$this->ClearInlineMode();

				// Switch to grid edit mode
				if ($this->CurrentAction == "gridedit")
					$this->GridEditMode();

				// Switch to grid add mode
				if ($this->CurrentAction == "gridadd")
					$this->GridAddMode();
			} else {
				if (@$_POST["a_list"] <> "") {
					$this->CurrentAction = $_POST["a_list"]; // Get action

					// Grid Update
					if (($this->CurrentAction == "gridupdate" || $this->CurrentAction == "gridoverwrite") && @$_SESSION[EW_SESSION_INLINE_MODE] == "gridedit") {
						if ($this->ValidateGridForm()) {
							$bGridUpdate = $this->GridUpdate();
						} else {
							$bGridUpdate = FALSE;
							$this->setFailureMessage($gsFormError);
						}
						if (!$bGridUpdate) {
							$this->EventCancelled = TRUE;
							$this->CurrentAction = "gridedit"; // Stay in Grid Edit mode
						}
					}

					// Grid Insert
					if ($this->CurrentAction == "gridinsert" && @$_SESSION[EW_SESSION_INLINE_MODE] == "gridadd") {
						if ($this->ValidateGridForm()) {
							$bGridInsert = $this->GridInsert();
						} else {
							$bGridInsert = FALSE;
							$this->setFailureMessage($gsFormError);
						}
						if (!$bGridInsert) {
							$this->EventCancelled = TRUE;
							$this->CurrentAction = "gridadd"; // Stay in Grid Add mode
						}
					}
				}
			}

			// Hide list options
			if ($this->Export <> "") {
				$this->ListOptions->HideAllOptions(array("sequence"));
				$this->ListOptions->UseDropDownButton = FALSE; // Disable drop down button
				$this->ListOptions->UseButtonGroup = FALSE; // Disable button group
			} elseif ($this->CurrentAction == "gridadd" || $this->CurrentAction == "gridedit") {
				$this->ListOptions->HideAllOptions();
				$this->ListOptions->UseDropDownButton = FALSE; // Disable drop down button
				$this->ListOptions->UseButtonGroup = FALSE; // Disable button group
			}

			// Hide options
			if ($this->Export <> "" || $this->CurrentAction <> "") {
				$this->ExportOptions->HideAllOptions();
				$this->FilterOptions->HideAllOptions();
			}

			// Hide other options
			if ($this->Export <> "") {
				foreach ($this->OtherOptions as &$option)
					$option->HideAllOptions();
			}

			// Show grid delete link for grid add / grid edit
			if ($this->AllowAddDeleteRow) {
				if ($this->CurrentAction == "gridadd" || $this->CurrentAction == "gridedit") {
					$item = $this->ListOptions->GetItem("griddelete");
					if ($item) $item->Visible = TRUE;
				}
			}

			// Get default search criteria
			ew_AddFilter($this->DefaultSearchWhere, $this->BasicSearchWhere(TRUE));

			// Get basic search values
			$this->LoadBasicSearchValues();

			// Restore filter list
			$this->RestoreFilterList();

			// Restore search parms from Session if not searching / reset / export
			if (($this->Export <> "" || $this->Command <> "search" && $this->Command <> "reset" && $this->Command <> "resetall") && $this->CheckSearchParms())
				$this->RestoreSearchParms();

			// Call Recordset SearchValidated event
			$this->Recordset_SearchValidated();

			// Set up sorting order
			$this->SetUpSortOrder();

			// Get basic search criteria
			if ($gsSearchError == "")
				$sSrchBasic = $this->BasicSearchWhere();
		}

		// Restore display records
		if ($this->getRecordsPerPage() <> "") {
			$this->DisplayRecs = $this->getRecordsPerPage(); // Restore from Session
		} else {
			$this->DisplayRecs = 20; // Load default
		}

		// Load Sorting Order
		$this->LoadSortOrder();

		// Load search default if no existing search criteria
		if (!$this->CheckSearchParms()) {

			// Load basic search from default
			$this->BasicSearch->LoadDefault();
			if ($this->BasicSearch->Keyword != "")
				$sSrchBasic = $this->BasicSearchWhere();
		}

		// Build search criteria
		ew_AddFilter($this->SearchWhere, $sSrchAdvanced);
		ew_AddFilter($this->SearchWhere, $sSrchBasic);

		// Call Recordset_Searching event
		$this->Recordset_Searching($this->SearchWhere);

		// Save search criteria
		if ($this->Command == "search" && !$this->RestoreSearch) {
			$this->setSearchWhere($this->SearchWhere); // Save to Session
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} else {
			$this->SearchWhere = $this->getSearchWhere();
		}

		// Build filter
		$sFilter = "";
		if (!$Security->CanList())
			$sFilter = "(0=1)"; // Filter all records
		ew_AddFilter($sFilter, $this->DbDetailFilter);
		ew_AddFilter($sFilter, $this->SearchWhere);

		// Set up filter in session
		$this->setSessionWhere($sFilter);
		$this->CurrentFilter = "";

		// Load record count first
		if (!$this->IsAddOrEdit()) {
			$bSelectLimit = $this->UseSelectLimit;
			if ($bSelectLimit) {
				$this->TotalRecs = $this->SelectRecordCount();
			} else {
				if ($this->Recordset = $this->LoadRecordset())
					$this->TotalRecs = $this->Recordset->RecordCount();
			}
		}

		// Search options
		$this->SetupSearchOptions();
	}

	//  Exit inline mode
	function ClearInlineMode() {
		$this->LastAction = $this->CurrentAction; // Save last action
		$this->CurrentAction = ""; // Clear action
		$_SESSION[EW_SESSION_INLINE_MODE] = ""; // Clear inline mode
	}

	// Switch to Grid Add mode
	function GridAddMode() {
		$_SESSION[EW_SESSION_INLINE_MODE] = "gridadd"; // Enabled grid add
	}

	// Switch to Grid Edit mode
	function GridEditMode() {
		$_SESSION[EW_SESSION_INLINE_MODE] = "gridedit"; // Enable grid edit
	}

	// Perform update to grid
	function GridUpdate() {
		global $Language, $objForm, $gsFormError;
		$bGridUpdate = TRUE;

		// Get old recordset
		$this->CurrentFilter = $this->BuildKeyFilter();
		if ($this->CurrentFilter == "")
			$this->CurrentFilter = "0=1";
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		if ($rs = $conn->Execute($sSql)) {
			$rsold = $rs->GetRows();
			$rs->Close();
		}

		// Call Grid Updating event
		if (!$this->Grid_Updating($rsold)) {
			if ($this->getFailureMessage() == "")
				$this->setFailureMessage($Language->Phrase("GridEditCancelled")); // Set grid edit cancelled message
			return FALSE;
		}

		// Begin transaction
		$conn->BeginTrans();
		$sKey = "";

		// Update row index and get row key
		$objForm->Index = -1;
		$rowcnt = strval($objForm->GetValue($this->FormKeyCountName));
		if ($rowcnt == "" || !is_numeric($rowcnt))
			$rowcnt = 0;

		// Update all rows based on key
		for ($rowindex = 1; $rowindex <= $rowcnt; $rowindex++) {
			$objForm->Index = $rowindex;
			$rowkey = strval($objForm->GetValue($this->FormKeyName));
			$rowaction = strval($objForm->GetValue($this->FormActionName));

			// Load all values and keys
			if ($rowaction <> "insertdelete") { // Skip insert then deleted rows
				$this->LoadFormValues(); // Get form values
				if ($rowaction == "" || $rowaction == "edit" || $rowaction == "delete") {
					$bGridUpdate = $this->SetupKeyValues($rowkey); // Set up key values
				} else {
					$bGridUpdate = TRUE;
				}

				// Skip empty row
				if ($rowaction == "insert" && $this->EmptyRow()) {

					// No action required
				// Validate form and insert/update/delete record

				} elseif ($bGridUpdate) {
					if ($rowaction == "delete") {
						$this->CurrentFilter = $this->KeyFilter();
						$bGridUpdate = $this->DeleteRows(); // Delete this row
					} else if (!$this->ValidateForm()) {
						$bGridUpdate = FALSE; // Form error, reset action
						$this->setFailureMessage($gsFormError);
					} else {
						if ($rowaction == "insert") {
							$bGridUpdate = $this->AddRow(); // Insert this row
						} else {
							if ($rowkey <> "") {
								$this->SendEmail = FALSE; // Do not send email on update success
								$bGridUpdate = $this->EditRow(); // Update this row
							}
						} // End update
					}
				}
				if ($bGridUpdate) {
					if ($sKey <> "") $sKey .= ", ";
					$sKey .= $rowkey;
				} else {
					break;
				}
			}
		}
		if ($bGridUpdate) {
			$conn->CommitTrans(); // Commit transaction

			// Get new recordset
			if ($rs = $conn->Execute($sSql)) {
				$rsnew = $rs->GetRows();
				$rs->Close();
			}

			// Call Grid_Updated event
			$this->Grid_Updated($rsold, $rsnew);
			if ($this->getSuccessMessage() == "")
				$this->setSuccessMessage($Language->Phrase("UpdateSuccess")); // Set up update success message
			$this->ClearInlineMode(); // Clear inline edit mode
		} else {
			$conn->RollbackTrans(); // Rollback transaction
			if ($this->getFailureMessage() == "")
				$this->setFailureMessage($Language->Phrase("UpdateFailed")); // Set update failed message
		}
		return $bGridUpdate;
	}

	// Build filter for all keys
	function BuildKeyFilter() {
		global $objForm;
		$sWrkFilter = "";

		// Update row index and get row key
		$rowindex = 1;
		$objForm->Index = $rowindex;
		$sThisKey = strval($objForm->GetValue($this->FormKeyName));
		while ($sThisKey <> "") {
			if ($this->SetupKeyValues($sThisKey)) {
				$sFilter = $this->KeyFilter();
				if ($sWrkFilter <> "") $sWrkFilter .= " OR ";
				$sWrkFilter .= $sFilter;
			} else {
				$sWrkFilter = "0=1";
				break;
			}

			// Update row index and get row key
			$rowindex++; // Next row
			$objForm->Index = $rowindex;
			$sThisKey = strval($objForm->GetValue($this->FormKeyName));
		}
		return $sWrkFilter;
	}

	// Set up key values
	function SetupKeyValues($key) {
		$arrKeyFlds = explode($GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"], $key);
		if (count($arrKeyFlds) >= 1) {
			$this->OcoCodi->setFormValue($arrKeyFlds[0]);
			if (!is_numeric($this->OcoCodi->FormValue))
				return FALSE;
		}
		return TRUE;
	}

	// Perform Grid Add
	function GridInsert() {
		global $Language, $objForm, $gsFormError;
		$rowindex = 1;
		$bGridInsert = FALSE;
		$conn = &$this->Connection();

		// Call Grid Inserting event
		if (!$this->Grid_Inserting()) {
			if ($this->getFailureMessage() == "") {
				$this->setFailureMessage($Language->Phrase("GridAddCancelled")); // Set grid add cancelled message
			}
			return FALSE;
		}

		// Begin transaction
		$conn->BeginTrans();

		// Init key filter
		$sWrkFilter = "";
		$addcnt = 0;
		$sKey = "";

		// Get row count
		$objForm->Index = -1;
		$rowcnt = strval($objForm->GetValue($this->FormKeyCountName));
		if ($rowcnt == "" || !is_numeric($rowcnt))
			$rowcnt = 0;

		// Insert all rows
		for ($rowindex = 1; $rowindex <= $rowcnt; $rowindex++) {

			// Load current row values
			$objForm->Index = $rowindex;
			$rowaction = strval($objForm->GetValue($this->FormActionName));
			if ($rowaction <> "" && $rowaction <> "insert")
				continue; // Skip
			$this->LoadFormValues(); // Get form values
			if (!$this->EmptyRow()) {
				$addcnt++;
				$this->SendEmail = FALSE; // Do not send email on insert success

				// Validate form
				if (!$this->ValidateForm()) {
					$bGridInsert = FALSE; // Form error, reset action
					$this->setFailureMessage($gsFormError);
				} else {
					$bGridInsert = $this->AddRow($this->OldRecordset); // Insert this row
				}
				if ($bGridInsert) {
					if ($sKey <> "") $sKey .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
					$sKey .= $this->OcoCodi->CurrentValue;

					// Add filter for this record
					$sFilter = $this->KeyFilter();
					if ($sWrkFilter <> "") $sWrkFilter .= " OR ";
					$sWrkFilter .= $sFilter;
				} else {
					break;
				}
			}
		}
		if ($addcnt == 0) { // No record inserted
			$this->setFailureMessage($Language->Phrase("NoAddRecord"));
			$bGridInsert = FALSE;
		}
		if ($bGridInsert) {
			$conn->CommitTrans(); // Commit transaction

			// Get new recordset
			$this->CurrentFilter = $sWrkFilter;
			$sSql = $this->SQL();
			if ($rs = $conn->Execute($sSql)) {
				$rsnew = $rs->GetRows();
				$rs->Close();
			}

			// Call Grid_Inserted event
			$this->Grid_Inserted($rsnew);
			if ($this->getSuccessMessage() == "")
				$this->setSuccessMessage($Language->Phrase("InsertSuccess")); // Set up insert success message
			$this->ClearInlineMode(); // Clear grid add mode
		} else {
			$conn->RollbackTrans(); // Rollback transaction
			if ($this->getFailureMessage() == "") {
				$this->setFailureMessage($Language->Phrase("InsertFailed")); // Set insert failed message
			}
		}
		return $bGridInsert;
	}

	// Check if empty row
	function EmptyRow() {
		global $objForm;
		if ($objForm->HasValue("x_PrvCodi") && $objForm->HasValue("o_PrvCodi") && $this->PrvCodi->CurrentValue <> $this->PrvCodi->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_OcoFCom") && $objForm->HasValue("o_OcoFCom") && $this->OcoFCom->CurrentValue <> $this->OcoFCom->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_OcoEsta") && $objForm->HasValue("o_OcoEsta") && $this->OcoEsta->CurrentValue <> $this->OcoEsta->OldValue)
			return FALSE;
		return TRUE;
	}

	// Validate grid form
	function ValidateGridForm() {
		global $objForm;

		// Get row count
		$objForm->Index = -1;
		$rowcnt = strval($objForm->GetValue($this->FormKeyCountName));
		if ($rowcnt == "" || !is_numeric($rowcnt))
			$rowcnt = 0;

		// Validate all records
		for ($rowindex = 1; $rowindex <= $rowcnt; $rowindex++) {

			// Load current row values
			$objForm->Index = $rowindex;
			$rowaction = strval($objForm->GetValue($this->FormActionName));
			if ($rowaction <> "delete" && $rowaction <> "insertdelete") {
				$this->LoadFormValues(); // Get form values
				if ($rowaction == "insert" && $this->EmptyRow()) {

					// Ignore
				} else if (!$this->ValidateForm()) {
					return FALSE;
				}
			}
		}
		return TRUE;
	}

	// Get all form values of the grid
	function GetGridFormValues() {
		global $objForm;

		// Get row count
		$objForm->Index = -1;
		$rowcnt = strval($objForm->GetValue($this->FormKeyCountName));
		if ($rowcnt == "" || !is_numeric($rowcnt))
			$rowcnt = 0;
		$rows = array();

		// Loop through all records
		for ($rowindex = 1; $rowindex <= $rowcnt; $rowindex++) {

			// Load current row values
			$objForm->Index = $rowindex;
			$rowaction = strval($objForm->GetValue($this->FormActionName));
			if ($rowaction <> "delete" && $rowaction <> "insertdelete") {
				$this->LoadFormValues(); // Get form values
				if ($rowaction == "insert" && $this->EmptyRow()) {

					// Ignore
				} else {
					$rows[] = $this->GetFieldValues("FormValue"); // Return row as array
				}
			}
		}
		return $rows; // Return as array of array
	}

	// Restore form values for current row
	function RestoreCurrentRowFormValues($idx) {
		global $objForm;

		// Get row based on current index
		$objForm->Index = $idx;
		$this->LoadFormValues(); // Load form values
	}

	// Get list of filters
	function GetFilterList() {

		// Initialize
		$sFilterList = "";
		$sFilterList = ew_Concat($sFilterList, $this->OcoCodi->AdvancedSearch->ToJSON(), ","); // Field OcoCodi
		$sFilterList = ew_Concat($sFilterList, $this->PrvCodi->AdvancedSearch->ToJSON(), ","); // Field PrvCodi
		$sFilterList = ew_Concat($sFilterList, $this->OcoFCom->AdvancedSearch->ToJSON(), ","); // Field OcoFCom
		$sFilterList = ew_Concat($sFilterList, $this->OcoEsta->AdvancedSearch->ToJSON(), ","); // Field OcoEsta
		if ($this->BasicSearch->Keyword <> "") {
			$sWrk = "\"" . EW_TABLE_BASIC_SEARCH . "\":\"" . ew_JsEncode2($this->BasicSearch->Keyword) . "\",\"" . EW_TABLE_BASIC_SEARCH_TYPE . "\":\"" . ew_JsEncode2($this->BasicSearch->Type) . "\"";
			$sFilterList = ew_Concat($sFilterList, $sWrk, ",");
		}

		// Return filter list in json
		return ($sFilterList <> "") ? "{" . $sFilterList . "}" : "null";
	}

	// Restore list of filters
	function RestoreFilterList() {

		// Return if not reset filter
		if (@$_POST["cmd"] <> "resetfilter")
			return FALSE;
		$filter = json_decode(ew_StripSlashes(@$_POST["filter"]), TRUE);
		$this->Command = "search";

		// Field OcoCodi
		$this->OcoCodi->AdvancedSearch->SearchValue = @$filter["x_OcoCodi"];
		$this->OcoCodi->AdvancedSearch->SearchOperator = @$filter["z_OcoCodi"];
		$this->OcoCodi->AdvancedSearch->SearchCondition = @$filter["v_OcoCodi"];
		$this->OcoCodi->AdvancedSearch->SearchValue2 = @$filter["y_OcoCodi"];
		$this->OcoCodi->AdvancedSearch->SearchOperator2 = @$filter["w_OcoCodi"];
		$this->OcoCodi->AdvancedSearch->Save();

		// Field PrvCodi
		$this->PrvCodi->AdvancedSearch->SearchValue = @$filter["x_PrvCodi"];
		$this->PrvCodi->AdvancedSearch->SearchOperator = @$filter["z_PrvCodi"];
		$this->PrvCodi->AdvancedSearch->SearchCondition = @$filter["v_PrvCodi"];
		$this->PrvCodi->AdvancedSearch->SearchValue2 = @$filter["y_PrvCodi"];
		$this->PrvCodi->AdvancedSearch->SearchOperator2 = @$filter["w_PrvCodi"];
		$this->PrvCodi->AdvancedSearch->Save();

		// Field OcoFCom
		$this->OcoFCom->AdvancedSearch->SearchValue = @$filter["x_OcoFCom"];
		$this->OcoFCom->AdvancedSearch->SearchOperator = @$filter["z_OcoFCom"];
		$this->OcoFCom->AdvancedSearch->SearchCondition = @$filter["v_OcoFCom"];
		$this->OcoFCom->AdvancedSearch->SearchValue2 = @$filter["y_OcoFCom"];
		$this->OcoFCom->AdvancedSearch->SearchOperator2 = @$filter["w_OcoFCom"];
		$this->OcoFCom->AdvancedSearch->Save();

		// Field OcoEsta
		$this->OcoEsta->AdvancedSearch->SearchValue = @$filter["x_OcoEsta"];
		$this->OcoEsta->AdvancedSearch->SearchOperator = @$filter["z_OcoEsta"];
		$this->OcoEsta->AdvancedSearch->SearchCondition = @$filter["v_OcoEsta"];
		$this->OcoEsta->AdvancedSearch->SearchValue2 = @$filter["y_OcoEsta"];
		$this->OcoEsta->AdvancedSearch->SearchOperator2 = @$filter["w_OcoEsta"];
		$this->OcoEsta->AdvancedSearch->Save();
		$this->BasicSearch->setKeyword(@$filter[EW_TABLE_BASIC_SEARCH]);
		$this->BasicSearch->setType(@$filter[EW_TABLE_BASIC_SEARCH_TYPE]);
	}

	// Return basic search SQL
	function BasicSearchSQL($arKeywords, $type) {
		$sWhere = "";
		$this->BuildBasicSearchSQL($sWhere, $this->OcoEsta, $arKeywords, $type);
		return $sWhere;
	}

	// Build basic search SQL
	function BuildBasicSearchSql(&$Where, &$Fld, $arKeywords, $type) {
		$sDefCond = ($type == "OR") ? "OR" : "AND";
		$arSQL = array(); // Array for SQL parts
		$arCond = array(); // Array for search conditions
		$cnt = count($arKeywords);
		$j = 0; // Number of SQL parts
		for ($i = 0; $i < $cnt; $i++) {
			$Keyword = $arKeywords[$i];
			$Keyword = trim($Keyword);
			if (EW_BASIC_SEARCH_IGNORE_PATTERN <> "") {
				$Keyword = preg_replace(EW_BASIC_SEARCH_IGNORE_PATTERN, "\\", $Keyword);
				$ar = explode("\\", $Keyword);
			} else {
				$ar = array($Keyword);
			}
			foreach ($ar as $Keyword) {
				if ($Keyword <> "") {
					$sWrk = "";
					if ($Keyword == "OR" && $type == "") {
						if ($j > 0)
							$arCond[$j-1] = "OR";
					} elseif ($Keyword == EW_NULL_VALUE) {
						$sWrk = $Fld->FldExpression . " IS NULL";
					} elseif ($Keyword == EW_NOT_NULL_VALUE) {
						$sWrk = $Fld->FldExpression . " IS NOT NULL";
					} elseif ($Fld->FldIsVirtual && $Fld->FldVirtualSearch) {
						$sWrk = $Fld->FldVirtualExpression . ew_Like(ew_QuotedValue("%" . $Keyword . "%", EW_DATATYPE_STRING, $this->DBID), $this->DBID);
					} elseif ($Fld->FldDataType != EW_DATATYPE_NUMBER || is_numeric($Keyword)) {
						$sWrk = $Fld->FldBasicSearchExpression . ew_Like(ew_QuotedValue("%" . $Keyword . "%", EW_DATATYPE_STRING, $this->DBID), $this->DBID);
					}
					if ($sWrk <> "") {
						$arSQL[$j] = $sWrk;
						$arCond[$j] = $sDefCond;
						$j += 1;
					}
				}
			}
		}
		$cnt = count($arSQL);
		$bQuoted = FALSE;
		$sSql = "";
		if ($cnt > 0) {
			for ($i = 0; $i < $cnt-1; $i++) {
				if ($arCond[$i] == "OR") {
					if (!$bQuoted) $sSql .= "(";
					$bQuoted = TRUE;
				}
				$sSql .= $arSQL[$i];
				if ($bQuoted && $arCond[$i] <> "OR") {
					$sSql .= ")";
					$bQuoted = FALSE;
				}
				$sSql .= " " . $arCond[$i] . " ";
			}
			$sSql .= $arSQL[$cnt-1];
			if ($bQuoted)
				$sSql .= ")";
		}
		if ($sSql <> "") {
			if ($Where <> "") $Where .= " OR ";
			$Where .=  "(" . $sSql . ")";
		}
	}

	// Return basic search WHERE clause based on search keyword and type
	function BasicSearchWhere($Default = FALSE) {
		global $Security;
		$sSearchStr = "";
		if (!$Security->CanSearch()) return "";
		$sSearchKeyword = ($Default) ? $this->BasicSearch->KeywordDefault : $this->BasicSearch->Keyword;
		$sSearchType = ($Default) ? $this->BasicSearch->TypeDefault : $this->BasicSearch->Type;
		if ($sSearchKeyword <> "") {
			$sSearch = trim($sSearchKeyword);
			if ($sSearchType <> "=") {
				$ar = array();

				// Match quoted keywords (i.e.: "...")
				if (preg_match_all('/"([^"]*)"/i', $sSearch, $matches, PREG_SET_ORDER)) {
					foreach ($matches as $match) {
						$p = strpos($sSearch, $match[0]);
						$str = substr($sSearch, 0, $p);
						$sSearch = substr($sSearch, $p + strlen($match[0]));
						if (strlen(trim($str)) > 0)
							$ar = array_merge($ar, explode(" ", trim($str)));
						$ar[] = $match[1]; // Save quoted keyword
					}
				}

				// Match individual keywords
				if (strlen(trim($sSearch)) > 0)
					$ar = array_merge($ar, explode(" ", trim($sSearch)));

				// Search keyword in any fields
				if (($sSearchType == "OR" || $sSearchType == "AND") && $this->BasicSearch->BasicSearchAnyFields) {
					foreach ($ar as $sKeyword) {
						if ($sKeyword <> "") {
							if ($sSearchStr <> "") $sSearchStr .= " " . $sSearchType . " ";
							$sSearchStr .= "(" . $this->BasicSearchSQL(array($sKeyword), $sSearchType) . ")";
						}
					}
				} else {
					$sSearchStr = $this->BasicSearchSQL($ar, $sSearchType);
				}
			} else {
				$sSearchStr = $this->BasicSearchSQL(array($sSearch), $sSearchType);
			}
			if (!$Default) $this->Command = "search";
		}
		if (!$Default && $this->Command == "search") {
			$this->BasicSearch->setKeyword($sSearchKeyword);
			$this->BasicSearch->setType($sSearchType);
		}
		return $sSearchStr;
	}

	// Check if search parm exists
	function CheckSearchParms() {

		// Check basic search
		if ($this->BasicSearch->IssetSession())
			return TRUE;
		return FALSE;
	}

	// Clear all search parameters
	function ResetSearchParms() {

		// Clear search WHERE clause
		$this->SearchWhere = "";
		$this->setSearchWhere($this->SearchWhere);

		// Clear basic search parameters
		$this->ResetBasicSearchParms();
	}

	// Load advanced search default values
	function LoadAdvancedSearchDefault() {
		return FALSE;
	}

	// Clear all basic search parameters
	function ResetBasicSearchParms() {
		$this->BasicSearch->UnsetSession();
	}

	// Restore all search parameters
	function RestoreSearchParms() {
		$this->RestoreSearch = TRUE;

		// Restore basic search values
		$this->BasicSearch->Load();
	}

	// Set up sort parameters
	function SetUpSortOrder() {

		// Check for Ctrl pressed
		$bCtrl = (@$_GET["ctrl"] <> "");

		// Check for "order" parameter
		if (@$_GET["order"] <> "") {
			$this->CurrentOrder = ew_StripSlashes(@$_GET["order"]);
			$this->CurrentOrderType = @$_GET["ordertype"];
			$this->UpdateSort($this->OcoCodi, $bCtrl); // OcoCodi
			$this->UpdateSort($this->PrvCodi, $bCtrl); // PrvCodi
			$this->UpdateSort($this->OcoFCom, $bCtrl); // OcoFCom
			$this->UpdateSort($this->OcoEsta, $bCtrl); // OcoEsta
			$this->setStartRecordNumber(1); // Reset start position
		}
	}

	// Load sort order parameters
	function LoadSortOrder() {
		$sOrderBy = $this->getSessionOrderBy(); // Get ORDER BY from Session
		if ($sOrderBy == "") {
			if ($this->getSqlOrderBy() <> "") {
				$sOrderBy = $this->getSqlOrderBy();
				$this->setSessionOrderBy($sOrderBy);
			}
		}
	}

	// Reset command
	// - cmd=reset (Reset search parameters)
	// - cmd=resetall (Reset search and master/detail parameters)
	// - cmd=resetsort (Reset sort parameters)
	function ResetCmd() {

		// Check if reset command
		if (substr($this->Command,0,5) == "reset") {

			// Reset search criteria
			if ($this->Command == "reset" || $this->Command == "resetall")
				$this->ResetSearchParms();

			// Reset sorting order
			if ($this->Command == "resetsort") {
				$sOrderBy = "";
				$this->setSessionOrderBy($sOrderBy);
				$this->OcoCodi->setSort("");
				$this->PrvCodi->setSort("");
				$this->OcoFCom->setSort("");
				$this->OcoEsta->setSort("");
			}

			// Reset start position
			$this->StartRec = 1;
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Set up list options
	function SetupListOptions() {
		global $Security, $Language;

		// "griddelete"
		if ($this->AllowAddDeleteRow) {
			$item = &$this->ListOptions->Add("griddelete");
			$item->CssStyle = "white-space: nowrap;";
			$item->OnLeft = FALSE;
			$item->Visible = FALSE; // Default hidden
		}

		// Add group option item
		$item = &$this->ListOptions->Add($this->ListOptions->GroupOptionName);
		$item->Body = "";
		$item->OnLeft = FALSE;
		$item->Visible = FALSE;

		// "view"
		$item = &$this->ListOptions->Add("view");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->CanView();
		$item->OnLeft = FALSE;

		// "edit"
		$item = &$this->ListOptions->Add("edit");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->CanEdit();
		$item->OnLeft = FALSE;

		// "copy"
		$item = &$this->ListOptions->Add("copy");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->CanAdd();
		$item->OnLeft = FALSE;

		// "delete"
		$item = &$this->ListOptions->Add("delete");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->CanDelete();
		$item->OnLeft = FALSE;

		// List actions
		$item = &$this->ListOptions->Add("listactions");
		$item->CssStyle = "white-space: nowrap;";
		$item->OnLeft = FALSE;
		$item->Visible = FALSE;
		$item->ShowInButtonGroup = FALSE;
		$item->ShowInDropDown = FALSE;

		// "checkbox"
		$item = &$this->ListOptions->Add("checkbox");
		$item->Visible = FALSE;
		$item->OnLeft = FALSE;
		$item->Header = "<input type=\"checkbox\" name=\"key\" id=\"key\" onclick=\"ew_SelectAllKey(this);\">";
		$item->ShowInDropDown = FALSE;
		$item->ShowInButtonGroup = FALSE;

		// Drop down button for ListOptions
		$this->ListOptions->UseImageAndText = TRUE;
		$this->ListOptions->UseDropDownButton = FALSE;
		$this->ListOptions->DropDownButtonPhrase = $Language->Phrase("ButtonListOptions");
		$this->ListOptions->UseButtonGroup = FALSE;
		if ($this->ListOptions->UseButtonGroup && ew_IsMobile())
			$this->ListOptions->UseDropDownButton = TRUE;
		$this->ListOptions->ButtonClass = "btn-sm"; // Class for button group

		// Call ListOptions_Load event
		$this->ListOptions_Load();
		$this->SetupListOptionsExt();
		$item = &$this->ListOptions->GetItem($this->ListOptions->GroupOptionName);
		$item->Visible = $this->ListOptions->GroupOptionVisible();
	}

	// Render list options
	function RenderListOptions() {
		global $Security, $Language, $objForm;
		$this->ListOptions->LoadDefault();

		// Set up row action and key
		if (is_numeric($this->RowIndex) && $this->CurrentMode <> "view") {
			$objForm->Index = $this->RowIndex;
			$ActionName = str_replace("k_", "k" . $this->RowIndex . "_", $this->FormActionName);
			$OldKeyName = str_replace("k_", "k" . $this->RowIndex . "_", $this->FormOldKeyName);
			$KeyName = str_replace("k_", "k" . $this->RowIndex . "_", $this->FormKeyName);
			$BlankRowName = str_replace("k_", "k" . $this->RowIndex . "_", $this->FormBlankRowName);
			if ($this->RowAction <> "")
				$this->MultiSelectKey .= "<input type=\"hidden\" name=\"" . $ActionName . "\" id=\"" . $ActionName . "\" value=\"" . $this->RowAction . "\">";
			if ($this->RowAction == "delete") {
				$rowkey = $objForm->GetValue($this->FormKeyName);
				$this->SetupKeyValues($rowkey);
			}
			if ($this->RowAction == "insert" && $this->CurrentAction == "F" && $this->EmptyRow())
				$this->MultiSelectKey .= "<input type=\"hidden\" name=\"" . $BlankRowName . "\" id=\"" . $BlankRowName . "\" value=\"1\">";
		}

		// "delete"
		if ($this->AllowAddDeleteRow) {
			if ($this->CurrentAction == "gridadd" || $this->CurrentAction == "gridedit") {
				$option = &$this->ListOptions;
				$option->UseButtonGroup = TRUE; // Use button group for grid delete button
				$option->UseImageAndText = TRUE; // Use image and text for grid delete button
				$oListOpt = &$option->Items["griddelete"];
				if (!$Security->CanDelete() && is_numeric($this->RowIndex) && ($this->RowAction == "" || $this->RowAction == "edit")) { // Do not allow delete existing record
					$oListOpt->Body = "&nbsp;";
				} else {
					$oListOpt->Body = "<a class=\"ewGridLink ewGridDelete\" title=\"" . ew_HtmlTitle($Language->Phrase("DeleteLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("DeleteLink")) . "\" onclick=\"return ew_DeleteGridRow(this, " . $this->RowIndex . ");\">" . $Language->Phrase("DeleteLink") . "</a>";
				}
			}
		}

		// "view"
		$oListOpt = &$this->ListOptions->Items["view"];
		if ($Security->CanView())
			$oListOpt->Body = "<a class=\"ewRowLink ewView\" title=\"" . ew_HtmlTitle($Language->Phrase("ViewLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("ViewLink")) . "\" href=\"" . ew_HtmlEncode($this->ViewUrl) . "\">" . $Language->Phrase("ViewLink") . "</a>";
		else
			$oListOpt->Body = "";

		// "edit"
		$oListOpt = &$this->ListOptions->Items["edit"];
		if ($Security->CanEdit()) {
			$oListOpt->Body = "<a class=\"ewRowLink ewEdit\" title=\"" . ew_HtmlTitle($Language->Phrase("EditLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("EditLink")) . "\" href=\"" . ew_HtmlEncode($this->EditUrl) . "\">" . $Language->Phrase("EditLink") . "</a>";
		} else {
			$oListOpt->Body = "";
		}

		// "copy"
		$oListOpt = &$this->ListOptions->Items["copy"];
		if ($Security->CanAdd()) {
			$oListOpt->Body = "<a class=\"ewRowLink ewCopy\" title=\"" . ew_HtmlTitle($Language->Phrase("CopyLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("CopyLink")) . "\" href=\"" . ew_HtmlEncode($this->CopyUrl) . "\">" . $Language->Phrase("CopyLink") . "</a>";
		} else {
			$oListOpt->Body = "";
		}

		// "delete"
		$oListOpt = &$this->ListOptions->Items["delete"];
		if ($Security->CanDelete())
			$oListOpt->Body = "<a class=\"ewRowLink ewDelete\"" . "" . " title=\"" . ew_HtmlTitle($Language->Phrase("DeleteLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("DeleteLink")) . "\" href=\"" . ew_HtmlEncode($this->DeleteUrl) . "\">" . $Language->Phrase("DeleteLink") . "</a>";
		else
			$oListOpt->Body = "";

		// Set up list action buttons
		$oListOpt = &$this->ListOptions->GetItem("listactions");
		if ($oListOpt) {
			$body = "";
			$links = array();
			foreach ($this->ListActions->Items as $listaction) {
				if ($listaction->Select == EW_ACTION_SINGLE && $listaction->Allow) {
					$action = $listaction->Action;
					$caption = $listaction->Caption;
					$icon = ($listaction->Icon <> "") ? "<span class=\"" . ew_HtmlEncode(str_replace(" ewIcon", "", $listaction->Icon)) . "\" data-caption=\"" . ew_HtmlTitle($caption) . "\"></span> " : "";
					$links[] = "<li><a class=\"ewAction ewListAction\" data-action=\"" . ew_HtmlEncode($action) . "\" data-caption=\"" . ew_HtmlTitle($caption) . "\" href=\"\" onclick=\"ew_SubmitAction(event,jQuery.extend({key:" . $this->KeyToJson() . "}," . $listaction->ToJson(TRUE) . "));return false;\">" . $icon . $listaction->Caption . "</a></li>";
					if (count($links) == 1) // Single button
						$body = "<a class=\"ewAction ewListAction\" data-action=\"" . ew_HtmlEncode($action) . "\" title=\"" . ew_HtmlTitle($caption) . "\" data-caption=\"" . ew_HtmlTitle($caption) . "\" href=\"\" onclick=\"ew_SubmitAction(event,jQuery.extend({key:" . $this->KeyToJson() . "}," . $listaction->ToJson(TRUE) . "));return false;\">" . $Language->Phrase("ListActionButton") . "</a>";
				}
			}
			if (count($links) > 1) { // More than one buttons, use dropdown
				$body = "<button class=\"dropdown-toggle btn btn-default btn-sm ewActions\" title=\"" . ew_HtmlTitle($Language->Phrase("ListActionButton")) . "\" data-toggle=\"dropdown\">" . $Language->Phrase("ListActionButton") . "<b class=\"caret\"></b></button>";
				$content = "";
				foreach ($links as $link)
					$content .= "<li>" . $link . "</li>";
				$body .= "<ul class=\"dropdown-menu" . ($oListOpt->OnLeft ? "" : " dropdown-menu-right") . "\">". $content . "</ul>";
				$body = "<div class=\"btn-group\">" . $body . "</div>";
			}
			if (count($links) > 0) {
				$oListOpt->Body = $body;
				$oListOpt->Visible = TRUE;
			}
		}

		// "checkbox"
		$oListOpt = &$this->ListOptions->Items["checkbox"];
		$oListOpt->Body = "<input type=\"checkbox\" name=\"key_m[]\" value=\"" . ew_HtmlEncode($this->OcoCodi->CurrentValue) . "\" onclick='ew_ClickMultiCheckbox(event);'>";
		if ($this->CurrentAction == "gridedit" && is_numeric($this->RowIndex)) {
			$this->MultiSelectKey .= "<input type=\"hidden\" name=\"" . $KeyName . "\" id=\"" . $KeyName . "\" value=\"" . $this->OcoCodi->CurrentValue . "\">";
		}
		$this->RenderListOptionsExt();

		// Call ListOptions_Rendered event
		$this->ListOptions_Rendered();
	}

	// Set up other options
	function SetupOtherOptions() {
		global $Language, $Security;
		$options = &$this->OtherOptions;
		$option = $options["addedit"];

		// Add
		$item = &$option->Add("add");
		$item->Body = "<a class=\"ewAddEdit ewAdd\" title=\"" . ew_HtmlTitle($Language->Phrase("AddLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("AddLink")) . "\" href=\"" . ew_HtmlEncode($this->AddUrl) . "\">" . $Language->Phrase("AddLink") . "</a>";
		$item->Visible = ($this->AddUrl <> "" && $Security->CanAdd());
		$item = &$option->Add("gridadd");
		$item->Body = "<a class=\"ewAddEdit ewGridAdd\" title=\"" . ew_HtmlTitle($Language->Phrase("GridAddLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("GridAddLink")) . "\" href=\"" . ew_HtmlEncode($this->GridAddUrl) . "\">" . $Language->Phrase("GridAddLink") . "</a>";
		$item->Visible = ($this->GridAddUrl <> "" && $Security->CanAdd());

		// Add grid edit
		$option = $options["addedit"];
		$item = &$option->Add("gridedit");
		$item->Body = "<a class=\"ewAddEdit ewGridEdit\" title=\"" . ew_HtmlTitle($Language->Phrase("GridEditLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("GridEditLink")) . "\" href=\"" . ew_HtmlEncode($this->GridEditUrl) . "\">" . $Language->Phrase("GridEditLink") . "</a>";
		$item->Visible = ($this->GridEditUrl <> "" && $Security->CanEdit());
		$option = $options["action"];

		// Set up options default
		foreach ($options as &$option) {
			$option->UseImageAndText = TRUE;
			$option->UseDropDownButton = FALSE;
			$option->UseButtonGroup = TRUE;
			$option->ButtonClass = "btn-sm"; // Class for button group
			$item = &$option->Add($option->GroupOptionName);
			$item->Body = "";
			$item->Visible = FALSE;
		}
		$options["addedit"]->DropDownButtonPhrase = $Language->Phrase("ButtonAddEdit");
		$options["detail"]->DropDownButtonPhrase = $Language->Phrase("ButtonDetails");
		$options["action"]->DropDownButtonPhrase = $Language->Phrase("ButtonActions");

		// Filter button
		$item = &$this->FilterOptions->Add("savecurrentfilter");
		$item->Body = "<a class=\"ewSaveFilter\" data-form=\"fOComlistsrch\" href=\"#\">" . $Language->Phrase("SaveCurrentFilter") . "</a>";
		$item->Visible = TRUE;
		$item = &$this->FilterOptions->Add("deletefilter");
		$item->Body = "<a class=\"ewDeleteFilter\" data-form=\"fOComlistsrch\" href=\"#\">" . $Language->Phrase("DeleteFilter") . "</a>";
		$item->Visible = TRUE;
		$this->FilterOptions->UseDropDownButton = TRUE;
		$this->FilterOptions->UseButtonGroup = !$this->FilterOptions->UseDropDownButton;
		$this->FilterOptions->DropDownButtonPhrase = $Language->Phrase("Filters");

		// Add group option item
		$item = &$this->FilterOptions->Add($this->FilterOptions->GroupOptionName);
		$item->Body = "";
		$item->Visible = FALSE;
	}

	// Render other options
	function RenderOtherOptions() {
		global $Language, $Security;
		$options = &$this->OtherOptions;
		if ($this->CurrentAction <> "gridadd" && $this->CurrentAction <> "gridedit") { // Not grid add/edit mode
			$option = &$options["action"];

			// Set up list action buttons
			foreach ($this->ListActions->Items as $listaction) {
				if ($listaction->Select == EW_ACTION_MULTIPLE) {
					$item = &$option->Add("custom_" . $listaction->Action);
					$caption = $listaction->Caption;
					$icon = ($listaction->Icon <> "") ? "<span class=\"" . ew_HtmlEncode($listaction->Icon) . "\" data-caption=\"" . ew_HtmlEncode($caption) . "\"></span> " : $caption;
					$item->Body = "<a class=\"ewAction ewListAction\" title=\"" . ew_HtmlEncode($caption) . "\" data-caption=\"" . ew_HtmlEncode($caption) . "\" href=\"\" onclick=\"ew_SubmitAction(event,jQuery.extend({f:document.fOComlist}," . $listaction->ToJson(TRUE) . "));return false;\">" . $icon . "</a>";
					$item->Visible = $listaction->Allow;
				}
			}

			// Hide grid edit and other options
			if ($this->TotalRecs <= 0) {
				$option = &$options["addedit"];
				$item = &$option->GetItem("gridedit");
				if ($item) $item->Visible = FALSE;
				$option = &$options["action"];
				$option->HideAllOptions();
			}
		} else { // Grid add/edit mode

			// Hide all options first
			foreach ($options as &$option)
				$option->HideAllOptions();
			if ($this->CurrentAction == "gridadd") {
				if ($this->AllowAddDeleteRow) {

					// Add add blank row
					$option = &$options["addedit"];
					$option->UseDropDownButton = FALSE;
					$option->UseImageAndText = TRUE;
					$item = &$option->Add("addblankrow");
					$item->Body = "<a class=\"ewAddEdit ewAddBlankRow\" title=\"" . ew_HtmlTitle($Language->Phrase("AddBlankRow")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("AddBlankRow")) . "\" href=\"javascript:void(0);\" onclick=\"ew_AddGridRow(this);\">" . $Language->Phrase("AddBlankRow") . "</a>";
					$item->Visible = $Security->CanAdd();
				}
				$option = &$options["action"];
				$option->UseDropDownButton = FALSE;
				$option->UseImageAndText = TRUE;

				// Add grid insert
				$item = &$option->Add("gridinsert");
				$item->Body = "<a class=\"ewAction ewGridInsert\" title=\"" . ew_HtmlTitle($Language->Phrase("GridInsertLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("GridInsertLink")) . "\" href=\"\" onclick=\"return ewForms(this).Submit('" . $this->AddMasterUrl($this->PageName()) . "');\">" . $Language->Phrase("GridInsertLink") . "</a>";

				// Add grid cancel
				$item = &$option->Add("gridcancel");
				$cancelurl = $this->AddMasterUrl($this->PageUrl() . "a=cancel");
				$item->Body = "<a class=\"ewAction ewGridCancel\" title=\"" . ew_HtmlTitle($Language->Phrase("GridCancelLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("GridCancelLink")) . "\" href=\"" . $cancelurl . "\">" . $Language->Phrase("GridCancelLink") . "</a>";
			}
			if ($this->CurrentAction == "gridedit") {
				if ($this->AllowAddDeleteRow) {

					// Add add blank row
					$option = &$options["addedit"];
					$option->UseDropDownButton = FALSE;
					$option->UseImageAndText = TRUE;
					$item = &$option->Add("addblankrow");
					$item->Body = "<a class=\"ewAddEdit ewAddBlankRow\" title=\"" . ew_HtmlTitle($Language->Phrase("AddBlankRow")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("AddBlankRow")) . "\" href=\"javascript:void(0);\" onclick=\"ew_AddGridRow(this);\">" . $Language->Phrase("AddBlankRow") . "</a>";
					$item->Visible = $Security->CanAdd();
				}
				$option = &$options["action"];
				$option->UseDropDownButton = FALSE;
				$option->UseImageAndText = TRUE;
					$item = &$option->Add("gridsave");
					$item->Body = "<a class=\"ewAction ewGridSave\" title=\"" . ew_HtmlTitle($Language->Phrase("GridSaveLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("GridSaveLink")) . "\" href=\"\" onclick=\"return ewForms(this).Submit('" . $this->AddMasterUrl($this->PageName()) . "');\">" . $Language->Phrase("GridSaveLink") . "</a>";
					$item = &$option->Add("gridcancel");
					$cancelurl = $this->AddMasterUrl($this->PageUrl() . "a=cancel");
					$item->Body = "<a class=\"ewAction ewGridCancel\" title=\"" . ew_HtmlTitle($Language->Phrase("GridCancelLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("GridCancelLink")) . "\" href=\"" . $cancelurl . "\">" . $Language->Phrase("GridCancelLink") . "</a>";
			}
		}
	}

	// Process list action
	function ProcessListAction() {
		global $Language, $Security;
		$userlist = "";
		$user = "";
		$sFilter = $this->GetKeyFilter();
		$UserAction = @$_POST["useraction"];
		if ($sFilter <> "" && $UserAction <> "") {

			// Check permission first
			$ActionCaption = $UserAction;
			if (array_key_exists($UserAction, $this->ListActions->Items)) {
				$ActionCaption = $this->ListActions->Items[$UserAction]->Caption;
				if (!$this->ListActions->Items[$UserAction]->Allow) {
					$errmsg = str_replace('%s', $ActionCaption, $Language->Phrase("CustomActionNotAllowed"));
					if (@$_POST["ajax"] == $UserAction) // Ajax
						echo "<p class=\"text-danger\">" . $errmsg . "</p>";
					else
						$this->setFailureMessage($errmsg);
					return FALSE;
				}
			}
			$this->CurrentFilter = $sFilter;
			$sSql = $this->SQL();
			$conn = &$this->Connection();
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			$rs = $conn->Execute($sSql);
			$conn->raiseErrorFn = '';
			$this->CurrentAction = $UserAction;

			// Call row action event
			if ($rs && !$rs->EOF) {
				$conn->BeginTrans();
				$this->SelectedCount = $rs->RecordCount();
				$this->SelectedIndex = 0;
				while (!$rs->EOF) {
					$this->SelectedIndex++;
					$row = $rs->fields;
					$Processed = $this->Row_CustomAction($UserAction, $row);
					if (!$Processed) break;
					$rs->MoveNext();
				}
				if ($Processed) {
					$conn->CommitTrans(); // Commit the changes
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage(str_replace('%s', $ActionCaption, $Language->Phrase("CustomActionCompleted"))); // Set up success message
				} else {
					$conn->RollbackTrans(); // Rollback changes

					// Set up error message
					if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

						// Use the message, do nothing
					} elseif ($this->CancelMessage <> "") {
						$this->setFailureMessage($this->CancelMessage);
						$this->CancelMessage = "";
					} else {
						$this->setFailureMessage(str_replace('%s', $ActionCaption, $Language->Phrase("CustomActionFailed")));
					}
				}
			}
			if ($rs)
				$rs->Close();
			if (@$_POST["ajax"] == $UserAction) { // Ajax
				if ($this->getSuccessMessage() <> "") {
					echo "<p class=\"text-success\">" . $this->getSuccessMessage() . "</p>";
					$this->ClearSuccessMessage(); // Clear message
				}
				if ($this->getFailureMessage() <> "") {
					echo "<p class=\"text-danger\">" . $this->getFailureMessage() . "</p>";
					$this->ClearFailureMessage(); // Clear message
				}
				return TRUE;
			}
		}
		return FALSE; // Not ajax request
	}

	// Set up search options
	function SetupSearchOptions() {
		global $Language;
		$this->SearchOptions = new cListOptions();
		$this->SearchOptions->Tag = "div";
		$this->SearchOptions->TagClassName = "ewSearchOption";

		// Search button
		$item = &$this->SearchOptions->Add("searchtoggle");
		$SearchToggleClass = ($this->SearchWhere <> "") ? " active" : " active";
		$item->Body = "<button type=\"button\" class=\"btn btn-default ewSearchToggle" . $SearchToggleClass . "\" title=\"" . $Language->Phrase("SearchPanel") . "\" data-caption=\"" . $Language->Phrase("SearchPanel") . "\" data-toggle=\"button\" data-form=\"fOComlistsrch\">" . $Language->Phrase("SearchBtn") . "</button>";
		$item->Visible = TRUE;

		// Show all button
		$item = &$this->SearchOptions->Add("showall");
		$item->Body = "<a class=\"btn btn-default ewShowAll\" title=\"" . $Language->Phrase("ShowAll") . "\" data-caption=\"" . $Language->Phrase("ShowAll") . "\" href=\"" . $this->PageUrl() . "cmd=reset\">" . $Language->Phrase("ShowAllBtn") . "</a>";
		$item->Visible = ($this->SearchWhere <> $this->DefaultSearchWhere && $this->SearchWhere <> "0=101");

		// Button group for search
		$this->SearchOptions->UseDropDownButton = FALSE;
		$this->SearchOptions->UseImageAndText = TRUE;
		$this->SearchOptions->UseButtonGroup = TRUE;
		$this->SearchOptions->DropDownButtonPhrase = $Language->Phrase("ButtonSearch");

		// Add group option item
		$item = &$this->SearchOptions->Add($this->SearchOptions->GroupOptionName);
		$item->Body = "";
		$item->Visible = FALSE;

		// Hide search options
		if ($this->Export <> "" || $this->CurrentAction <> "")
			$this->SearchOptions->HideAllOptions();
		global $Security;
		if (!$Security->CanSearch()) {
			$this->SearchOptions->HideAllOptions();
			$this->FilterOptions->HideAllOptions();
		}
	}

	function SetupListOptionsExt() {
		global $Security, $Language;
	}

	function RenderListOptionsExt() {
		global $Security, $Language;
	}

	// Set up starting record parameters
	function SetUpStartRec() {
		if ($this->DisplayRecs == 0)
			return;
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET[EW_TABLE_START_REC] <> "") { // Check for "start" parameter
				$this->StartRec = $_GET[EW_TABLE_START_REC];
				$this->setStartRecordNumber($this->StartRec);
			} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
				$PageNo = $_GET[EW_TABLE_PAGE_NO];
				if (is_numeric($PageNo)) {
					$this->StartRec = ($PageNo-1)*$this->DisplayRecs+1;
					if ($this->StartRec <= 0) {
						$this->StartRec = 1;
					} elseif ($this->StartRec >= intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1) {
						$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1;
					}
					$this->setStartRecordNumber($this->StartRec);
				}
			}
		}
		$this->StartRec = $this->getStartRecordNumber();

		// Check if correct start record counter
		if (!is_numeric($this->StartRec) || $this->StartRec == "") { // Avoid invalid start record counter
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} elseif (intval($this->StartRec) > intval($this->TotalRecs)) { // Avoid starting record > total records
			$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to last page first record
			$this->setStartRecordNumber($this->StartRec);
		} elseif (($this->StartRec-1) % $this->DisplayRecs <> 0) {
			$this->StartRec = intval(($this->StartRec-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to page boundary
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Load default values
	function LoadDefaultValues() {
		$this->OcoCodi->CurrentValue = NULL;
		$this->OcoCodi->OldValue = $this->OcoCodi->CurrentValue;
		$this->PrvCodi->CurrentValue = NULL;
		$this->PrvCodi->OldValue = $this->PrvCodi->CurrentValue;
		$this->OcoFCom->CurrentValue = NULL;
		$this->OcoFCom->OldValue = $this->OcoFCom->CurrentValue;
		$this->OcoEsta->CurrentValue = NULL;
		$this->OcoEsta->OldValue = $this->OcoEsta->CurrentValue;
	}

	// Load basic search values
	function LoadBasicSearchValues() {
		$this->BasicSearch->Keyword = @$_GET[EW_TABLE_BASIC_SEARCH];
		if ($this->BasicSearch->Keyword <> "") $this->Command = "search";
		$this->BasicSearch->Type = @$_GET[EW_TABLE_BASIC_SEARCH_TYPE];
	}

	// Load form values
	function LoadFormValues() {

		// Load from form
		global $objForm;
		if (!$this->OcoCodi->FldIsDetailKey && $this->CurrentAction <> "gridadd" && $this->CurrentAction <> "add")
			$this->OcoCodi->setFormValue($objForm->GetValue("x_OcoCodi"));
		if (!$this->PrvCodi->FldIsDetailKey) {
			$this->PrvCodi->setFormValue($objForm->GetValue("x_PrvCodi"));
		}
		$this->PrvCodi->setOldValue($objForm->GetValue("o_PrvCodi"));
		if (!$this->OcoFCom->FldIsDetailKey) {
			$this->OcoFCom->setFormValue($objForm->GetValue("x_OcoFCom"));
			$this->OcoFCom->CurrentValue = ew_UnFormatDateTime($this->OcoFCom->CurrentValue, 7);
		}
		$this->OcoFCom->setOldValue($objForm->GetValue("o_OcoFCom"));
		if (!$this->OcoEsta->FldIsDetailKey) {
			$this->OcoEsta->setFormValue($objForm->GetValue("x_OcoEsta"));
		}
		$this->OcoEsta->setOldValue($objForm->GetValue("o_OcoEsta"));
	}

	// Restore form values
	function RestoreFormValues() {
		global $objForm;
		if ($this->CurrentAction <> "gridadd" && $this->CurrentAction <> "add")
			$this->OcoCodi->CurrentValue = $this->OcoCodi->FormValue;
		$this->PrvCodi->CurrentValue = $this->PrvCodi->FormValue;
		$this->OcoFCom->CurrentValue = $this->OcoFCom->FormValue;
		$this->OcoFCom->CurrentValue = ew_UnFormatDateTime($this->OcoFCom->CurrentValue, 7);
		$this->OcoEsta->CurrentValue = $this->OcoEsta->FormValue;
	}

	// Load recordset
	function LoadRecordset($offset = -1, $rowcnt = -1) {

		// Load List page SQL
		$sSql = $this->SelectSQL();
		$conn = &$this->Connection();

		// Load recordset
		$dbtype = ew_GetConnectionType($this->DBID);
		if ($this->UseSelectLimit) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			if ($dbtype == "MSSQL") {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset, array("_hasOrderBy" => trim($this->getOrderBy()) || trim($this->getSessionOrderBy())));
			} else {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset);
			}
			$conn->raiseErrorFn = '';
		} else {
			$rs = ew_LoadRecordset($sSql, $conn);
		}

		// Call Recordset Selected event
		$this->Recordset_Selected($rs);
		return $rs;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->OcoCodi->setDbValue($rs->fields('OcoCodi'));
		$this->PrvCodi->setDbValue($rs->fields('PrvCodi'));
		$this->OcoLPed->Upload->DbValue = $rs->fields('OcoLPed');
		if (is_array($this->OcoLPed->Upload->DbValue) || is_object($this->OcoLPed->Upload->DbValue)) // Byte array
			$this->OcoLPed->Upload->DbValue = ew_BytesToStr($this->OcoLPed->Upload->DbValue);
		$this->OcoFCom->setDbValue($rs->fields('OcoFCom'));
		$this->OcoProf->Upload->DbValue = $rs->fields('OcoProf');
		if (is_array($this->OcoProf->Upload->DbValue) || is_object($this->OcoProf->Upload->DbValue)) // Byte array
			$this->OcoProf->Upload->DbValue = ew_BytesToStr($this->OcoProf->Upload->DbValue);
		$this->OcoEsta->setDbValue($rs->fields('OcoEsta'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->OcoCodi->DbValue = $row['OcoCodi'];
		$this->PrvCodi->DbValue = $row['PrvCodi'];
		$this->OcoLPed->Upload->DbValue = $row['OcoLPed'];
		$this->OcoFCom->DbValue = $row['OcoFCom'];
		$this->OcoProf->Upload->DbValue = $row['OcoProf'];
		$this->OcoEsta->DbValue = $row['OcoEsta'];
	}

	// Load old record
	function LoadOldRecord() {

		// Load key values from Session
		$bValidKey = TRUE;
		if (strval($this->getKey("OcoCodi")) <> "")
			$this->OcoCodi->CurrentValue = $this->getKey("OcoCodi"); // OcoCodi
		else
			$bValidKey = FALSE;

		// Load old recordset
		if ($bValidKey) {
			$this->CurrentFilter = $this->KeyFilter();
			$sSql = $this->SQL();
			$conn = &$this->Connection();
			$this->OldRecordset = ew_LoadRecordset($sSql, $conn);
			$this->LoadRowValues($this->OldRecordset); // Load row values
		} else {
			$this->OldRecordset = NULL;
		}
		return $bValidKey;
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		$this->ViewUrl = $this->GetViewUrl();
		$this->EditUrl = $this->GetEditUrl();
		$this->InlineEditUrl = $this->GetInlineEditUrl();
		$this->CopyUrl = $this->GetCopyUrl();
		$this->InlineCopyUrl = $this->GetInlineCopyUrl();
		$this->DeleteUrl = $this->GetDeleteUrl();

		// Call Row_Rendering event
		$this->Row_Rendering();

		// Common render codes for all row types
		// OcoCodi
		// PrvCodi
		// OcoLPed
		// OcoFCom
		// OcoProf
		// OcoEsta

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// OcoCodi
		$this->OcoCodi->ViewValue = $this->OcoCodi->CurrentValue;
		$this->OcoCodi->ViewCustomAttributes = "";

		// PrvCodi
		if (strval($this->PrvCodi->CurrentValue) <> "") {
			$sFilterWrk = "\"PrvCodi\"" . ew_SearchString("=", $this->PrvCodi->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT \"PrvCodi\", \"PrvRSoc\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"Prov\"";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->PrvCodi, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->PrvCodi->ViewValue = $this->PrvCodi->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->PrvCodi->ViewValue = $this->PrvCodi->CurrentValue;
			}
		} else {
			$this->PrvCodi->ViewValue = NULL;
		}
		$this->PrvCodi->ViewCustomAttributes = "";

		// OcoFCom
		$this->OcoFCom->ViewValue = $this->OcoFCom->CurrentValue;
		$this->OcoFCom->ViewValue = ew_FormatDateTime($this->OcoFCom->ViewValue, 7);
		$this->OcoFCom->ViewCustomAttributes = "";

		// OcoEsta
		$this->OcoEsta->ViewValue = $this->OcoEsta->CurrentValue;
		$this->OcoEsta->ViewCustomAttributes = "";

			// OcoCodi
			$this->OcoCodi->LinkCustomAttributes = "";
			$this->OcoCodi->HrefValue = "";
			$this->OcoCodi->TooltipValue = "";

			// PrvCodi
			$this->PrvCodi->LinkCustomAttributes = "";
			$this->PrvCodi->HrefValue = "";
			$this->PrvCodi->TooltipValue = "";

			// OcoFCom
			$this->OcoFCom->LinkCustomAttributes = "";
			$this->OcoFCom->HrefValue = "";
			$this->OcoFCom->TooltipValue = "";

			// OcoEsta
			$this->OcoEsta->LinkCustomAttributes = "";
			$this->OcoEsta->HrefValue = "";
			$this->OcoEsta->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_ADD) { // Add row

			// OcoCodi
			// PrvCodi

			$this->PrvCodi->EditAttrs["class"] = "form-control";
			$this->PrvCodi->EditCustomAttributes = "";
			if (trim(strval($this->PrvCodi->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "\"PrvCodi\"" . ew_SearchString("=", $this->PrvCodi->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT \"PrvCodi\", \"PrvRSoc\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\", '' AS \"SelectFilterFld\", '' AS \"SelectFilterFld2\", '' AS \"SelectFilterFld3\", '' AS \"SelectFilterFld4\" FROM \"public\".\"Prov\"";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->PrvCodi, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->PrvCodi->EditValue = $arwrk;

			// OcoFCom
			$this->OcoFCom->EditAttrs["class"] = "form-control";
			$this->OcoFCom->EditCustomAttributes = "";
			$this->OcoFCom->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->OcoFCom->CurrentValue, 7));
			$this->OcoFCom->PlaceHolder = ew_RemoveHtml($this->OcoFCom->FldCaption());

			// OcoEsta
			$this->OcoEsta->EditAttrs["class"] = "form-control";
			$this->OcoEsta->EditCustomAttributes = "";
			$this->OcoEsta->EditValue = ew_HtmlEncode($this->OcoEsta->CurrentValue);
			$this->OcoEsta->PlaceHolder = ew_RemoveHtml($this->OcoEsta->FldCaption());

			// Edit refer script
			// OcoCodi

			$this->OcoCodi->HrefValue = "";

			// PrvCodi
			$this->PrvCodi->HrefValue = "";

			// OcoFCom
			$this->OcoFCom->HrefValue = "";

			// OcoEsta
			$this->OcoEsta->HrefValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_EDIT) { // Edit row

			// OcoCodi
			$this->OcoCodi->EditAttrs["class"] = "form-control";
			$this->OcoCodi->EditCustomAttributes = "";
			$this->OcoCodi->EditValue = $this->OcoCodi->CurrentValue;
			$this->OcoCodi->ViewCustomAttributes = "";

			// PrvCodi
			$this->PrvCodi->EditAttrs["class"] = "form-control";
			$this->PrvCodi->EditCustomAttributes = "";
			if (trim(strval($this->PrvCodi->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "\"PrvCodi\"" . ew_SearchString("=", $this->PrvCodi->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT \"PrvCodi\", \"PrvRSoc\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\", '' AS \"SelectFilterFld\", '' AS \"SelectFilterFld2\", '' AS \"SelectFilterFld3\", '' AS \"SelectFilterFld4\" FROM \"public\".\"Prov\"";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->PrvCodi, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->PrvCodi->EditValue = $arwrk;

			// OcoFCom
			$this->OcoFCom->EditAttrs["class"] = "form-control";
			$this->OcoFCom->EditCustomAttributes = "";
			$this->OcoFCom->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->OcoFCom->CurrentValue, 7));
			$this->OcoFCom->PlaceHolder = ew_RemoveHtml($this->OcoFCom->FldCaption());

			// OcoEsta
			$this->OcoEsta->EditAttrs["class"] = "form-control";
			$this->OcoEsta->EditCustomAttributes = "";
			$this->OcoEsta->EditValue = ew_HtmlEncode($this->OcoEsta->CurrentValue);
			$this->OcoEsta->PlaceHolder = ew_RemoveHtml($this->OcoEsta->FldCaption());

			// Edit refer script
			// OcoCodi

			$this->OcoCodi->HrefValue = "";

			// PrvCodi
			$this->PrvCodi->HrefValue = "";

			// OcoFCom
			$this->OcoFCom->HrefValue = "";

			// OcoEsta
			$this->OcoEsta->HrefValue = "";
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate form
	function ValidateForm() {
		global $Language, $gsFormError;

		// Initialize form error message
		$gsFormError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return ($gsFormError == "");
		if (!$this->PrvCodi->FldIsDetailKey && !is_null($this->PrvCodi->FormValue) && $this->PrvCodi->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->PrvCodi->FldCaption(), $this->PrvCodi->ReqErrMsg));
		}
		if (!$this->OcoFCom->FldIsDetailKey && !is_null($this->OcoFCom->FormValue) && $this->OcoFCom->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->OcoFCom->FldCaption(), $this->OcoFCom->ReqErrMsg));
		}
		if (!ew_CheckEuroDate($this->OcoFCom->FormValue)) {
			ew_AddMessage($gsFormError, $this->OcoFCom->FldErrMsg());
		}
		if (!$this->OcoEsta->FldIsDetailKey && !is_null($this->OcoEsta->FormValue) && $this->OcoEsta->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->OcoEsta->FldCaption(), $this->OcoEsta->ReqErrMsg));
		}

		// Return validate result
		$ValidateForm = ($gsFormError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateForm = $ValidateForm && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsFormError, $sFormCustomError);
		}
		return $ValidateForm;
	}

	//
	// Delete records based on current filter
	//
	function DeleteRows() {
		global $Language, $Security;
		if (!$Security->CanDelete()) {
			$this->setFailureMessage($Language->Phrase("NoDeletePermission")); // No delete permission
			return FALSE;
		}
		$DeleteRows = TRUE;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE) {
			return FALSE;
		} elseif ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
			$rs->Close();
			return FALSE;

		//} else {
		//	$this->LoadRowValues($rs); // Load row values

		}
		$rows = ($rs) ? $rs->GetRows() : array();

		// Clone old rows
		$rsold = $rows;
		if ($rs)
			$rs->Close();

		// Call row deleting event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$DeleteRows = $this->Row_Deleting($row);
				if (!$DeleteRows) break;
			}
		}
		if ($DeleteRows) {
			$sKey = "";
			foreach ($rsold as $row) {
				$sThisKey = "";
				if ($sThisKey <> "") $sThisKey .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
				$sThisKey .= $row['OcoCodi'];
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				$DeleteRows = $this->Delete($row); // Delete
				$conn->raiseErrorFn = '';
				if ($DeleteRows === FALSE)
					break;
				if ($sKey <> "") $sKey .= ", ";
				$sKey .= $sThisKey;
			}
		} else {

			// Set up error message
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("DeleteCancelled"));
			}
		}
		if ($DeleteRows) {
		} else {
		}

		// Call Row Deleted event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$this->Row_Deleted($row);
			}
		}
		return $DeleteRows;
	}

	// Update record based on key values
	function EditRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$conn = &$this->Connection();
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE)
			return FALSE;
		if ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record message
			$EditRow = FALSE; // Update Failed
		} else {

			// Save old values
			$rsold = &$rs->fields;
			$this->LoadDbValues($rsold);
			$rsnew = array();

			// PrvCodi
			$this->PrvCodi->SetDbValueDef($rsnew, $this->PrvCodi->CurrentValue, 0, $this->PrvCodi->ReadOnly);

			// OcoFCom
			$this->OcoFCom->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->OcoFCom->CurrentValue, 7), ew_CurrentDate(), $this->OcoFCom->ReadOnly);

			// OcoEsta
			$this->OcoEsta->SetDbValueDef($rsnew, $this->OcoEsta->CurrentValue, "", $this->OcoEsta->ReadOnly);

			// Call Row Updating event
			$bUpdateRow = $this->Row_Updating($rsold, $rsnew);
			if ($bUpdateRow) {
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				if (count($rsnew) > 0)
					$EditRow = $this->Update($rsnew, "", $rsold);
				else
					$EditRow = TRUE; // No field to update
				$conn->raiseErrorFn = '';
				if ($EditRow) {
				}
			} else {
				if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

					// Use the message, do nothing
				} elseif ($this->CancelMessage <> "") {
					$this->setFailureMessage($this->CancelMessage);
					$this->CancelMessage = "";
				} else {
					$this->setFailureMessage($Language->Phrase("UpdateCancelled"));
				}
				$EditRow = FALSE;
			}
		}

		// Call Row_Updated event
		if ($EditRow)
			$this->Row_Updated($rsold, $rsnew);
		$rs->Close();
		return $EditRow;
	}

	// Add record
	function AddRow($rsold = NULL) {
		global $Language, $Security;
		$conn = &$this->Connection();

		// Load db values from rsold
		if ($rsold) {
			$this->LoadDbValues($rsold);
		}
		$rsnew = array();

		// PrvCodi
		$this->PrvCodi->SetDbValueDef($rsnew, $this->PrvCodi->CurrentValue, 0, FALSE);

		// OcoFCom
		$this->OcoFCom->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->OcoFCom->CurrentValue, 7), ew_CurrentDate(), FALSE);

		// OcoEsta
		$this->OcoEsta->SetDbValueDef($rsnew, $this->OcoEsta->CurrentValue, "", FALSE);

		// Call Row Inserting event
		$rs = ($rsold == NULL) ? NULL : $rsold->fields;
		$bInsertRow = $this->Row_Inserting($rs, $rsnew);
		if ($bInsertRow) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			$AddRow = $this->Insert($rsnew);
			$conn->raiseErrorFn = '';
			if ($AddRow) {

				// Get insert id if necessary
				$this->OcoCodi->setDbValue($conn->GetOne("SELECT currval('\"OCom_OcoCodi_seq\"'::regclass)"));
				$rsnew['OcoCodi'] = $this->OcoCodi->DbValue;
			}
		} else {
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("InsertCancelled"));
			}
			$AddRow = FALSE;
		}
		if ($AddRow) {

			// Call Row Inserted event
			$rs = ($rsold == NULL) ? NULL : $rsold->fields;
			$this->Row_Inserted($rs, $rsnew);
		}
		return $AddRow;
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$url = preg_replace('/\?cmd=reset(all){0,1}$/i', '', $url); // Remove cmd=reset / cmd=resetall
		$Breadcrumb->Add("list", $this->TableVar, $url, "", $this->TableVar, TRUE);
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}

	// ListOptions Load event
	function ListOptions_Load() {

		// Example:
		//$opt = &$this->ListOptions->Add("new");
		//$opt->Header = "xxx";
		//$opt->OnLeft = TRUE; // Link on left
		//$opt->MoveTo(0); // Move to first column

	}

	// ListOptions Rendered event
	function ListOptions_Rendered() {

		// Example: 
		//$this->ListOptions->Items["new"]->Body = "xxx";

	}

	// Row Custom Action event
	function Row_CustomAction($action, $row) {

		// Return FALSE to abort
		return TRUE;
	}

	// Page Exporting event
	// $this->ExportDoc = export document object
	function Page_Exporting() {

		//$this->ExportDoc->Text = "my header"; // Export header
		//return FALSE; // Return FALSE to skip default export and use Row_Export event

		return TRUE; // Return TRUE to use default export and skip Row_Export event
	}

	// Row Export event
	// $this->ExportDoc = export document object
	function Row_Export($rs) {

	    //$this->ExportDoc->Text .= "my content"; // Build HTML with field value: $rs["MyField"] or $this->MyField->ViewValue
	}

	// Page Exported event
	// $this->ExportDoc = export document object
	function Page_Exported() {

		//$this->ExportDoc->Text .= "my footer"; // Export footer
		//echo $this->ExportDoc->Text;

	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($OCom_list)) $OCom_list = new cOCom_list();

// Page init
$OCom_list->Page_Init();

// Page main
$OCom_list->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$OCom_list->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "list";
var CurrentForm = fOComlist = new ew_Form("fOComlist", "list");
fOComlist.FormKeyCountName = '<?php echo $OCom_list->FormKeyCountName ?>';

// Validate form
fOComlist.Validate = function() {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.GetForm(), $fobj = $(fobj);
	if ($fobj.find("#a_confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.FormKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = $fobj.find("#a_list").val() == "gridinsert";
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
		var checkrow = (gridinsert) ? !this.EmptyRow(infix) : true;
		if (checkrow) {
			addcnt++;
			elm = this.GetElements("x" + infix + "_PrvCodi");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $OCom->PrvCodi->FldCaption(), $OCom->PrvCodi->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_OcoFCom");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $OCom->OcoFCom->FldCaption(), $OCom->OcoFCom->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_OcoFCom");
			if (elm && !ew_CheckEuroDate(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($OCom->OcoFCom->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_OcoEsta");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $OCom->OcoEsta->FldCaption(), $OCom->OcoEsta->ReqErrMsg)) ?>");

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
		} // End Grid Add checking
	}
	if (gridinsert && addcnt == 0) { // No row added
		ew_Alert(ewLanguage.Phrase("NoAddRecord"));
		return false;
	}
	return true;
}

// Check empty row
fOComlist.EmptyRow = function(infix) {
	var fobj = this.Form;
	if (ew_ValueChanged(fobj, infix, "PrvCodi", false)) return false;
	if (ew_ValueChanged(fobj, infix, "OcoFCom", false)) return false;
	if (ew_ValueChanged(fobj, infix, "OcoEsta", false)) return false;
	return true;
}

// Form_CustomValidate event
fOComlist.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fOComlist.ValidateRequired = true;
<?php } else { ?>
fOComlist.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
fOComlist.Lists["x_PrvCodi"] = {"LinkField":"x_PrvCodi","Ajax":true,"AutoFill":false,"DisplayFields":["x_PrvRSoc","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};

// Form object for search
var CurrentSearchForm = fOComlistsrch = new ew_Form("fOComlistsrch");
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php if ($OCom_list->TotalRecs > 0 && $OCom_list->ExportOptions->Visible()) { ?>
<?php $OCom_list->ExportOptions->Render("body") ?>
<?php } ?>
<?php if ($OCom_list->SearchOptions->Visible()) { ?>
<?php $OCom_list->SearchOptions->Render("body") ?>
<?php } ?>
<?php if ($OCom_list->FilterOptions->Visible()) { ?>
<?php $OCom_list->FilterOptions->Render("body") ?>
<?php } ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php
if ($OCom->CurrentAction == "gridadd") {
	$OCom->CurrentFilter = "0=1";
	$OCom_list->StartRec = 1;
	$OCom_list->DisplayRecs = $OCom->GridAddRowCount;
	$OCom_list->TotalRecs = $OCom_list->DisplayRecs;
	$OCom_list->StopRec = $OCom_list->DisplayRecs;
} else {
	$bSelectLimit = $OCom_list->UseSelectLimit;
	if ($bSelectLimit) {
		if ($OCom_list->TotalRecs <= 0)
			$OCom_list->TotalRecs = $OCom->SelectRecordCount();
	} else {
		if (!$OCom_list->Recordset && ($OCom_list->Recordset = $OCom_list->LoadRecordset()))
			$OCom_list->TotalRecs = $OCom_list->Recordset->RecordCount();
	}
	$OCom_list->StartRec = 1;
	if ($OCom_list->DisplayRecs <= 0 || ($OCom->Export <> "" && $OCom->ExportAll)) // Display all records
		$OCom_list->DisplayRecs = $OCom_list->TotalRecs;
	if (!($OCom->Export <> "" && $OCom->ExportAll))
		$OCom_list->SetUpStartRec(); // Set up start record position
	if ($bSelectLimit)
		$OCom_list->Recordset = $OCom_list->LoadRecordset($OCom_list->StartRec-1, $OCom_list->DisplayRecs);

	// Set no record found message
	if ($OCom->CurrentAction == "" && $OCom_list->TotalRecs == 0) {
		if (!$Security->CanList())
			$OCom_list->setWarningMessage($Language->Phrase("NoPermission"));
		if ($OCom_list->SearchWhere == "0=101")
			$OCom_list->setWarningMessage($Language->Phrase("EnterSearchCriteria"));
		else
			$OCom_list->setWarningMessage($Language->Phrase("NoRecord"));
	}
}
$OCom_list->RenderOtherOptions();
?>
<?php if ($Security->CanSearch()) { ?>
<?php if ($OCom->Export == "" && $OCom->CurrentAction == "") { ?>
<form name="fOComlistsrch" id="fOComlistsrch" class="form-inline ewForm" action="<?php echo ew_CurrentPage() ?>">
<?php $SearchPanelClass = ($OCom_list->SearchWhere <> "") ? " in" : " in"; ?>
<div id="fOComlistsrch_SearchPanel" class="ewSearchPanel collapse<?php echo $SearchPanelClass ?>">
<input type="hidden" name="cmd" value="search">
<input type="hidden" name="t" value="OCom">
	<div class="ewBasicSearch">
<div id="xsr_1" class="ewRow">
	<div class="ewQuickSearch input-group">
	<input type="text" name="<?php echo EW_TABLE_BASIC_SEARCH ?>" id="<?php echo EW_TABLE_BASIC_SEARCH ?>" class="form-control" value="<?php echo ew_HtmlEncode($OCom_list->BasicSearch->getKeyword()) ?>" placeholder="<?php echo ew_HtmlEncode($Language->Phrase("Search")) ?>">
	<input type="hidden" name="<?php echo EW_TABLE_BASIC_SEARCH_TYPE ?>" id="<?php echo EW_TABLE_BASIC_SEARCH_TYPE ?>" value="<?php echo ew_HtmlEncode($OCom_list->BasicSearch->getType()) ?>">
	<div class="input-group-btn">
		<button type="button" data-toggle="dropdown" class="btn btn-default"><span id="searchtype"><?php echo $OCom_list->BasicSearch->getTypeNameShort() ?></span><span class="caret"></span></button>
		<ul class="dropdown-menu pull-right" role="menu">
			<li<?php if ($OCom_list->BasicSearch->getType() == "") echo " class=\"active\""; ?>><a href="javascript:void(0);" onclick="ew_SetSearchType(this)"><?php echo $Language->Phrase("QuickSearchAuto") ?></a></li>
			<li<?php if ($OCom_list->BasicSearch->getType() == "=") echo " class=\"active\""; ?>><a href="javascript:void(0);" onclick="ew_SetSearchType(this,'=')"><?php echo $Language->Phrase("QuickSearchExact") ?></a></li>
			<li<?php if ($OCom_list->BasicSearch->getType() == "AND") echo " class=\"active\""; ?>><a href="javascript:void(0);" onclick="ew_SetSearchType(this,'AND')"><?php echo $Language->Phrase("QuickSearchAll") ?></a></li>
			<li<?php if ($OCom_list->BasicSearch->getType() == "OR") echo " class=\"active\""; ?>><a href="javascript:void(0);" onclick="ew_SetSearchType(this,'OR')"><?php echo $Language->Phrase("QuickSearchAny") ?></a></li>
		</ul>
	<button class="btn btn-primary ewButton" name="btnsubmit" id="btnsubmit" type="submit"><?php echo $Language->Phrase("QuickSearchBtn") ?></button>
	</div>
	</div>
</div>
	</div>
</div>
</form>
<?php } ?>
<?php } ?>
<?php $OCom_list->ShowPageHeader(); ?>
<?php
$OCom_list->ShowMessage();
?>
<?php if ($OCom_list->TotalRecs > 0 || $OCom->CurrentAction <> "") { ?>
<div class="panel panel-default ewGrid">
<form name="fOComlist" id="fOComlist" class="form-inline ewForm ewListForm" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($OCom_list->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $OCom_list->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="OCom">
<div id="gmp_OCom" class="<?php if (ew_IsResponsiveLayout()) { echo "table-responsive "; } ?>ewGridMiddlePanel">
<?php if ($OCom_list->TotalRecs > 0) { ?>
<table id="tbl_OComlist" class="table ewTable">
<?php echo $OCom->TableCustomInnerHtml ?>
<thead><!-- Table header -->
	<tr class="ewTableHeader">
<?php

// Header row
$OCom_list->RowType = EW_ROWTYPE_HEADER;

// Render list options
$OCom_list->RenderListOptions();

// Render list options (header, left)
$OCom_list->ListOptions->Render("header", "left");
?>
<?php if ($OCom->OcoCodi->Visible) { // OcoCodi ?>
	<?php if ($OCom->SortUrl($OCom->OcoCodi) == "") { ?>
		<th data-name="OcoCodi"><div id="elh_OCom_OcoCodi" class="OCom_OcoCodi"><div class="ewTableHeaderCaption"><?php echo $OCom->OcoCodi->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="OcoCodi"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $OCom->SortUrl($OCom->OcoCodi) ?>',2);"><div id="elh_OCom_OcoCodi" class="OCom_OcoCodi">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $OCom->OcoCodi->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($OCom->OcoCodi->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($OCom->OcoCodi->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($OCom->PrvCodi->Visible) { // PrvCodi ?>
	<?php if ($OCom->SortUrl($OCom->PrvCodi) == "") { ?>
		<th data-name="PrvCodi"><div id="elh_OCom_PrvCodi" class="OCom_PrvCodi"><div class="ewTableHeaderCaption"><?php echo $OCom->PrvCodi->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="PrvCodi"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $OCom->SortUrl($OCom->PrvCodi) ?>',2);"><div id="elh_OCom_PrvCodi" class="OCom_PrvCodi">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $OCom->PrvCodi->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($OCom->PrvCodi->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($OCom->PrvCodi->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($OCom->OcoFCom->Visible) { // OcoFCom ?>
	<?php if ($OCom->SortUrl($OCom->OcoFCom) == "") { ?>
		<th data-name="OcoFCom"><div id="elh_OCom_OcoFCom" class="OCom_OcoFCom"><div class="ewTableHeaderCaption"><?php echo $OCom->OcoFCom->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="OcoFCom"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $OCom->SortUrl($OCom->OcoFCom) ?>',2);"><div id="elh_OCom_OcoFCom" class="OCom_OcoFCom">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $OCom->OcoFCom->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($OCom->OcoFCom->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($OCom->OcoFCom->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($OCom->OcoEsta->Visible) { // OcoEsta ?>
	<?php if ($OCom->SortUrl($OCom->OcoEsta) == "") { ?>
		<th data-name="OcoEsta"><div id="elh_OCom_OcoEsta" class="OCom_OcoEsta"><div class="ewTableHeaderCaption"><?php echo $OCom->OcoEsta->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="OcoEsta"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $OCom->SortUrl($OCom->OcoEsta) ?>',2);"><div id="elh_OCom_OcoEsta" class="OCom_OcoEsta">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $OCom->OcoEsta->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($OCom->OcoEsta->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($OCom->OcoEsta->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php

// Render list options (header, right)
$OCom_list->ListOptions->Render("header", "right");
?>
	</tr>
</thead>
<tbody>
<?php
if ($OCom->ExportAll && $OCom->Export <> "") {
	$OCom_list->StopRec = $OCom_list->TotalRecs;
} else {

	// Set the last record to display
	if ($OCom_list->TotalRecs > $OCom_list->StartRec + $OCom_list->DisplayRecs - 1)
		$OCom_list->StopRec = $OCom_list->StartRec + $OCom_list->DisplayRecs - 1;
	else
		$OCom_list->StopRec = $OCom_list->TotalRecs;
}

// Restore number of post back records
if ($objForm) {
	$objForm->Index = -1;
	if ($objForm->HasValue($OCom_list->FormKeyCountName) && ($OCom->CurrentAction == "gridadd" || $OCom->CurrentAction == "gridedit" || $OCom->CurrentAction == "F")) {
		$OCom_list->KeyCount = $objForm->GetValue($OCom_list->FormKeyCountName);
		$OCom_list->StopRec = $OCom_list->StartRec + $OCom_list->KeyCount - 1;
	}
}
$OCom_list->RecCnt = $OCom_list->StartRec - 1;
if ($OCom_list->Recordset && !$OCom_list->Recordset->EOF) {
	$OCom_list->Recordset->MoveFirst();
	$bSelectLimit = $OCom_list->UseSelectLimit;
	if (!$bSelectLimit && $OCom_list->StartRec > 1)
		$OCom_list->Recordset->Move($OCom_list->StartRec - 1);
} elseif (!$OCom->AllowAddDeleteRow && $OCom_list->StopRec == 0) {
	$OCom_list->StopRec = $OCom->GridAddRowCount;
}

// Initialize aggregate
$OCom->RowType = EW_ROWTYPE_AGGREGATEINIT;
$OCom->ResetAttrs();
$OCom_list->RenderRow();
if ($OCom->CurrentAction == "gridadd")
	$OCom_list->RowIndex = 0;
if ($OCom->CurrentAction == "gridedit")
	$OCom_list->RowIndex = 0;
while ($OCom_list->RecCnt < $OCom_list->StopRec) {
	$OCom_list->RecCnt++;
	if (intval($OCom_list->RecCnt) >= intval($OCom_list->StartRec)) {
		$OCom_list->RowCnt++;
		if ($OCom->CurrentAction == "gridadd" || $OCom->CurrentAction == "gridedit" || $OCom->CurrentAction == "F") {
			$OCom_list->RowIndex++;
			$objForm->Index = $OCom_list->RowIndex;
			if ($objForm->HasValue($OCom_list->FormActionName))
				$OCom_list->RowAction = strval($objForm->GetValue($OCom_list->FormActionName));
			elseif ($OCom->CurrentAction == "gridadd")
				$OCom_list->RowAction = "insert";
			else
				$OCom_list->RowAction = "";
		}

		// Set up key count
		$OCom_list->KeyCount = $OCom_list->RowIndex;

		// Init row class and style
		$OCom->ResetAttrs();
		$OCom->CssClass = "";
		if ($OCom->CurrentAction == "gridadd") {
			$OCom_list->LoadDefaultValues(); // Load default values
		} else {
			$OCom_list->LoadRowValues($OCom_list->Recordset); // Load row values
		}
		$OCom->RowType = EW_ROWTYPE_VIEW; // Render view
		if ($OCom->CurrentAction == "gridadd") // Grid add
			$OCom->RowType = EW_ROWTYPE_ADD; // Render add
		if ($OCom->CurrentAction == "gridadd" && $OCom->EventCancelled && !$objForm->HasValue("k_blankrow")) // Insert failed
			$OCom_list->RestoreCurrentRowFormValues($OCom_list->RowIndex); // Restore form values
		if ($OCom->CurrentAction == "gridedit") { // Grid edit
			if ($OCom->EventCancelled) {
				$OCom_list->RestoreCurrentRowFormValues($OCom_list->RowIndex); // Restore form values
			}
			if ($OCom_list->RowAction == "insert")
				$OCom->RowType = EW_ROWTYPE_ADD; // Render add
			else
				$OCom->RowType = EW_ROWTYPE_EDIT; // Render edit
		}
		if ($OCom->CurrentAction == "gridedit" && ($OCom->RowType == EW_ROWTYPE_EDIT || $OCom->RowType == EW_ROWTYPE_ADD) && $OCom->EventCancelled) // Update failed
			$OCom_list->RestoreCurrentRowFormValues($OCom_list->RowIndex); // Restore form values
		if ($OCom->RowType == EW_ROWTYPE_EDIT) // Edit row
			$OCom_list->EditRowCnt++;

		// Set up row id / data-rowindex
		$OCom->RowAttrs = array_merge($OCom->RowAttrs, array('data-rowindex'=>$OCom_list->RowCnt, 'id'=>'r' . $OCom_list->RowCnt . '_OCom', 'data-rowtype'=>$OCom->RowType));

		// Render row
		$OCom_list->RenderRow();

		// Render list options
		$OCom_list->RenderListOptions();

		// Skip delete row / empty row for confirm page
		if ($OCom_list->RowAction <> "delete" && $OCom_list->RowAction <> "insertdelete" && !($OCom_list->RowAction == "insert" && $OCom->CurrentAction == "F" && $OCom_list->EmptyRow())) {
?>
	<tr<?php echo $OCom->RowAttributes() ?>>
<?php

// Render list options (body, left)
$OCom_list->ListOptions->Render("body", "left", $OCom_list->RowCnt);
?>
	<?php if ($OCom->OcoCodi->Visible) { // OcoCodi ?>
		<td data-name="OcoCodi"<?php echo $OCom->OcoCodi->CellAttributes() ?>>
<?php if ($OCom->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<input type="hidden" data-table="OCom" data-field="x_OcoCodi" name="o<?php echo $OCom_list->RowIndex ?>_OcoCodi" id="o<?php echo $OCom_list->RowIndex ?>_OcoCodi" value="<?php echo ew_HtmlEncode($OCom->OcoCodi->OldValue) ?>">
<?php } ?>
<?php if ($OCom->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $OCom_list->RowCnt ?>_OCom_OcoCodi" class="form-group OCom_OcoCodi">
<span<?php echo $OCom->OcoCodi->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $OCom->OcoCodi->EditValue ?></p></span>
</span>
<input type="hidden" data-table="OCom" data-field="x_OcoCodi" name="x<?php echo $OCom_list->RowIndex ?>_OcoCodi" id="x<?php echo $OCom_list->RowIndex ?>_OcoCodi" value="<?php echo ew_HtmlEncode($OCom->OcoCodi->CurrentValue) ?>">
<?php } ?>
<?php if ($OCom->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $OCom_list->RowCnt ?>_OCom_OcoCodi" class="OCom_OcoCodi">
<span<?php echo $OCom->OcoCodi->ViewAttributes() ?>>
<?php echo $OCom->OcoCodi->ListViewValue() ?></span>
</span>
<?php } ?>
<a id="<?php echo $OCom_list->PageObjName . "_row_" . $OCom_list->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($OCom->PrvCodi->Visible) { // PrvCodi ?>
		<td data-name="PrvCodi"<?php echo $OCom->PrvCodi->CellAttributes() ?>>
<?php if ($OCom->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $OCom_list->RowCnt ?>_OCom_PrvCodi" class="form-group OCom_PrvCodi">
<select data-table="OCom" data-field="x_PrvCodi" data-value-separator="<?php echo ew_HtmlEncode(is_array($OCom->PrvCodi->DisplayValueSeparator) ? json_encode($OCom->PrvCodi->DisplayValueSeparator) : $OCom->PrvCodi->DisplayValueSeparator) ?>" id="x<?php echo $OCom_list->RowIndex ?>_PrvCodi" name="x<?php echo $OCom_list->RowIndex ?>_PrvCodi"<?php echo $OCom->PrvCodi->EditAttributes() ?>>
<?php
if (is_array($OCom->PrvCodi->EditValue)) {
	$arwrk = $OCom->PrvCodi->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($OCom->PrvCodi->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $OCom->PrvCodi->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($OCom->PrvCodi->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($OCom->PrvCodi->CurrentValue) ?>" selected><?php echo $OCom->PrvCodi->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $OCom->PrvCodi->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT \"PrvCodi\", \"PrvRSoc\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"Prov\"";
$sWhereWrk = "";
$OCom->PrvCodi->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$OCom->PrvCodi->LookupFilters += array("f0" => "\"PrvCodi\" = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$OCom->Lookup_Selecting($OCom->PrvCodi, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $OCom->PrvCodi->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $OCom_list->RowIndex ?>_PrvCodi" id="s_x<?php echo $OCom_list->RowIndex ?>_PrvCodi" value="<?php echo $OCom->PrvCodi->LookupFilterQuery() ?>">
</span>
<input type="hidden" data-table="OCom" data-field="x_PrvCodi" name="o<?php echo $OCom_list->RowIndex ?>_PrvCodi" id="o<?php echo $OCom_list->RowIndex ?>_PrvCodi" value="<?php echo ew_HtmlEncode($OCom->PrvCodi->OldValue) ?>">
<?php } ?>
<?php if ($OCom->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $OCom_list->RowCnt ?>_OCom_PrvCodi" class="form-group OCom_PrvCodi">
<select data-table="OCom" data-field="x_PrvCodi" data-value-separator="<?php echo ew_HtmlEncode(is_array($OCom->PrvCodi->DisplayValueSeparator) ? json_encode($OCom->PrvCodi->DisplayValueSeparator) : $OCom->PrvCodi->DisplayValueSeparator) ?>" id="x<?php echo $OCom_list->RowIndex ?>_PrvCodi" name="x<?php echo $OCom_list->RowIndex ?>_PrvCodi"<?php echo $OCom->PrvCodi->EditAttributes() ?>>
<?php
if (is_array($OCom->PrvCodi->EditValue)) {
	$arwrk = $OCom->PrvCodi->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($OCom->PrvCodi->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $OCom->PrvCodi->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($OCom->PrvCodi->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($OCom->PrvCodi->CurrentValue) ?>" selected><?php echo $OCom->PrvCodi->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $OCom->PrvCodi->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT \"PrvCodi\", \"PrvRSoc\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"Prov\"";
$sWhereWrk = "";
$OCom->PrvCodi->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$OCom->PrvCodi->LookupFilters += array("f0" => "\"PrvCodi\" = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$OCom->Lookup_Selecting($OCom->PrvCodi, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $OCom->PrvCodi->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $OCom_list->RowIndex ?>_PrvCodi" id="s_x<?php echo $OCom_list->RowIndex ?>_PrvCodi" value="<?php echo $OCom->PrvCodi->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php if ($OCom->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $OCom_list->RowCnt ?>_OCom_PrvCodi" class="OCom_PrvCodi">
<span<?php echo $OCom->PrvCodi->ViewAttributes() ?>>
<?php echo $OCom->PrvCodi->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($OCom->OcoFCom->Visible) { // OcoFCom ?>
		<td data-name="OcoFCom"<?php echo $OCom->OcoFCom->CellAttributes() ?>>
<?php if ($OCom->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $OCom_list->RowCnt ?>_OCom_OcoFCom" class="form-group OCom_OcoFCom">
<input type="text" data-table="OCom" data-field="x_OcoFCom" data-format="7" name="x<?php echo $OCom_list->RowIndex ?>_OcoFCom" id="x<?php echo $OCom_list->RowIndex ?>_OcoFCom" placeholder="<?php echo ew_HtmlEncode($OCom->OcoFCom->getPlaceHolder()) ?>" value="<?php echo $OCom->OcoFCom->EditValue ?>"<?php echo $OCom->OcoFCom->EditAttributes() ?>>
</span>
<input type="hidden" data-table="OCom" data-field="x_OcoFCom" name="o<?php echo $OCom_list->RowIndex ?>_OcoFCom" id="o<?php echo $OCom_list->RowIndex ?>_OcoFCom" value="<?php echo ew_HtmlEncode($OCom->OcoFCom->OldValue) ?>">
<?php } ?>
<?php if ($OCom->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $OCom_list->RowCnt ?>_OCom_OcoFCom" class="form-group OCom_OcoFCom">
<input type="text" data-table="OCom" data-field="x_OcoFCom" data-format="7" name="x<?php echo $OCom_list->RowIndex ?>_OcoFCom" id="x<?php echo $OCom_list->RowIndex ?>_OcoFCom" placeholder="<?php echo ew_HtmlEncode($OCom->OcoFCom->getPlaceHolder()) ?>" value="<?php echo $OCom->OcoFCom->EditValue ?>"<?php echo $OCom->OcoFCom->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($OCom->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $OCom_list->RowCnt ?>_OCom_OcoFCom" class="OCom_OcoFCom">
<span<?php echo $OCom->OcoFCom->ViewAttributes() ?>>
<?php echo $OCom->OcoFCom->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($OCom->OcoEsta->Visible) { // OcoEsta ?>
		<td data-name="OcoEsta"<?php echo $OCom->OcoEsta->CellAttributes() ?>>
<?php if ($OCom->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $OCom_list->RowCnt ?>_OCom_OcoEsta" class="form-group OCom_OcoEsta">
<input type="text" data-table="OCom" data-field="x_OcoEsta" name="x<?php echo $OCom_list->RowIndex ?>_OcoEsta" id="x<?php echo $OCom_list->RowIndex ?>_OcoEsta" size="30" maxlength="1" placeholder="<?php echo ew_HtmlEncode($OCom->OcoEsta->getPlaceHolder()) ?>" value="<?php echo $OCom->OcoEsta->EditValue ?>"<?php echo $OCom->OcoEsta->EditAttributes() ?>>
</span>
<input type="hidden" data-table="OCom" data-field="x_OcoEsta" name="o<?php echo $OCom_list->RowIndex ?>_OcoEsta" id="o<?php echo $OCom_list->RowIndex ?>_OcoEsta" value="<?php echo ew_HtmlEncode($OCom->OcoEsta->OldValue) ?>">
<?php } ?>
<?php if ($OCom->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $OCom_list->RowCnt ?>_OCom_OcoEsta" class="form-group OCom_OcoEsta">
<input type="text" data-table="OCom" data-field="x_OcoEsta" name="x<?php echo $OCom_list->RowIndex ?>_OcoEsta" id="x<?php echo $OCom_list->RowIndex ?>_OcoEsta" size="30" maxlength="1" placeholder="<?php echo ew_HtmlEncode($OCom->OcoEsta->getPlaceHolder()) ?>" value="<?php echo $OCom->OcoEsta->EditValue ?>"<?php echo $OCom->OcoEsta->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($OCom->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $OCom_list->RowCnt ?>_OCom_OcoEsta" class="OCom_OcoEsta">
<span<?php echo $OCom->OcoEsta->ViewAttributes() ?>>
<?php echo $OCom->OcoEsta->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
<?php

// Render list options (body, right)
$OCom_list->ListOptions->Render("body", "right", $OCom_list->RowCnt);
?>
	</tr>
<?php if ($OCom->RowType == EW_ROWTYPE_ADD || $OCom->RowType == EW_ROWTYPE_EDIT) { ?>
<script type="text/javascript">
fOComlist.UpdateOpts(<?php echo $OCom_list->RowIndex ?>);
</script>
<?php } ?>
<?php
	}
	} // End delete row checking
	if ($OCom->CurrentAction <> "gridadd")
		if (!$OCom_list->Recordset->EOF) $OCom_list->Recordset->MoveNext();
}
?>
<?php
	if ($OCom->CurrentAction == "gridadd" || $OCom->CurrentAction == "gridedit") {
		$OCom_list->RowIndex = '$rowindex$';
		$OCom_list->LoadDefaultValues();

		// Set row properties
		$OCom->ResetAttrs();
		$OCom->RowAttrs = array_merge($OCom->RowAttrs, array('data-rowindex'=>$OCom_list->RowIndex, 'id'=>'r0_OCom', 'data-rowtype'=>EW_ROWTYPE_ADD));
		ew_AppendClass($OCom->RowAttrs["class"], "ewTemplate");
		$OCom->RowType = EW_ROWTYPE_ADD;

		// Render row
		$OCom_list->RenderRow();

		// Render list options
		$OCom_list->RenderListOptions();
		$OCom_list->StartRowCnt = 0;
?>
	<tr<?php echo $OCom->RowAttributes() ?>>
<?php

// Render list options (body, left)
$OCom_list->ListOptions->Render("body", "left", $OCom_list->RowIndex);
?>
	<?php if ($OCom->OcoCodi->Visible) { // OcoCodi ?>
		<td data-name="OcoCodi">
<input type="hidden" data-table="OCom" data-field="x_OcoCodi" name="o<?php echo $OCom_list->RowIndex ?>_OcoCodi" id="o<?php echo $OCom_list->RowIndex ?>_OcoCodi" value="<?php echo ew_HtmlEncode($OCom->OcoCodi->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($OCom->PrvCodi->Visible) { // PrvCodi ?>
		<td data-name="PrvCodi">
<span id="el$rowindex$_OCom_PrvCodi" class="form-group OCom_PrvCodi">
<select data-table="OCom" data-field="x_PrvCodi" data-value-separator="<?php echo ew_HtmlEncode(is_array($OCom->PrvCodi->DisplayValueSeparator) ? json_encode($OCom->PrvCodi->DisplayValueSeparator) : $OCom->PrvCodi->DisplayValueSeparator) ?>" id="x<?php echo $OCom_list->RowIndex ?>_PrvCodi" name="x<?php echo $OCom_list->RowIndex ?>_PrvCodi"<?php echo $OCom->PrvCodi->EditAttributes() ?>>
<?php
if (is_array($OCom->PrvCodi->EditValue)) {
	$arwrk = $OCom->PrvCodi->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($OCom->PrvCodi->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $OCom->PrvCodi->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($OCom->PrvCodi->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($OCom->PrvCodi->CurrentValue) ?>" selected><?php echo $OCom->PrvCodi->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $OCom->PrvCodi->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT \"PrvCodi\", \"PrvRSoc\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"Prov\"";
$sWhereWrk = "";
$OCom->PrvCodi->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$OCom->PrvCodi->LookupFilters += array("f0" => "\"PrvCodi\" = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$OCom->Lookup_Selecting($OCom->PrvCodi, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $OCom->PrvCodi->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $OCom_list->RowIndex ?>_PrvCodi" id="s_x<?php echo $OCom_list->RowIndex ?>_PrvCodi" value="<?php echo $OCom->PrvCodi->LookupFilterQuery() ?>">
</span>
<input type="hidden" data-table="OCom" data-field="x_PrvCodi" name="o<?php echo $OCom_list->RowIndex ?>_PrvCodi" id="o<?php echo $OCom_list->RowIndex ?>_PrvCodi" value="<?php echo ew_HtmlEncode($OCom->PrvCodi->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($OCom->OcoFCom->Visible) { // OcoFCom ?>
		<td data-name="OcoFCom">
<span id="el$rowindex$_OCom_OcoFCom" class="form-group OCom_OcoFCom">
<input type="text" data-table="OCom" data-field="x_OcoFCom" data-format="7" name="x<?php echo $OCom_list->RowIndex ?>_OcoFCom" id="x<?php echo $OCom_list->RowIndex ?>_OcoFCom" placeholder="<?php echo ew_HtmlEncode($OCom->OcoFCom->getPlaceHolder()) ?>" value="<?php echo $OCom->OcoFCom->EditValue ?>"<?php echo $OCom->OcoFCom->EditAttributes() ?>>
</span>
<input type="hidden" data-table="OCom" data-field="x_OcoFCom" name="o<?php echo $OCom_list->RowIndex ?>_OcoFCom" id="o<?php echo $OCom_list->RowIndex ?>_OcoFCom" value="<?php echo ew_HtmlEncode($OCom->OcoFCom->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($OCom->OcoEsta->Visible) { // OcoEsta ?>
		<td data-name="OcoEsta">
<span id="el$rowindex$_OCom_OcoEsta" class="form-group OCom_OcoEsta">
<input type="text" data-table="OCom" data-field="x_OcoEsta" name="x<?php echo $OCom_list->RowIndex ?>_OcoEsta" id="x<?php echo $OCom_list->RowIndex ?>_OcoEsta" size="30" maxlength="1" placeholder="<?php echo ew_HtmlEncode($OCom->OcoEsta->getPlaceHolder()) ?>" value="<?php echo $OCom->OcoEsta->EditValue ?>"<?php echo $OCom->OcoEsta->EditAttributes() ?>>
</span>
<input type="hidden" data-table="OCom" data-field="x_OcoEsta" name="o<?php echo $OCom_list->RowIndex ?>_OcoEsta" id="o<?php echo $OCom_list->RowIndex ?>_OcoEsta" value="<?php echo ew_HtmlEncode($OCom->OcoEsta->OldValue) ?>">
</td>
	<?php } ?>
<?php

// Render list options (body, right)
$OCom_list->ListOptions->Render("body", "right", $OCom_list->RowCnt);
?>
<script type="text/javascript">
fOComlist.UpdateOpts(<?php echo $OCom_list->RowIndex ?>);
</script>
	</tr>
<?php
}
?>
</tbody>
</table>
<?php } ?>
<?php if ($OCom->CurrentAction == "gridadd") { ?>
<input type="hidden" name="a_list" id="a_list" value="gridinsert">
<input type="hidden" name="<?php echo $OCom_list->FormKeyCountName ?>" id="<?php echo $OCom_list->FormKeyCountName ?>" value="<?php echo $OCom_list->KeyCount ?>">
<?php echo $OCom_list->MultiSelectKey ?>
<?php } ?>
<?php if ($OCom->CurrentAction == "gridedit") { ?>
<input type="hidden" name="a_list" id="a_list" value="gridupdate">
<input type="hidden" name="<?php echo $OCom_list->FormKeyCountName ?>" id="<?php echo $OCom_list->FormKeyCountName ?>" value="<?php echo $OCom_list->KeyCount ?>">
<?php echo $OCom_list->MultiSelectKey ?>
<?php } ?>
<?php if ($OCom->CurrentAction == "") { ?>
<input type="hidden" name="a_list" id="a_list" value="">
<?php } ?>
</div>
</form>
<?php

// Close recordset
if ($OCom_list->Recordset)
	$OCom_list->Recordset->Close();
?>
<div class="panel-footer ewGridLowerPanel">
<?php if ($OCom->CurrentAction <> "gridadd" && $OCom->CurrentAction <> "gridedit") { ?>
<form name="ewPagerForm" class="ewForm form-inline ewPagerForm" action="<?php echo ew_CurrentPage() ?>">
<?php if (!isset($OCom_list->Pager)) $OCom_list->Pager = new cPrevNextPager($OCom_list->StartRec, $OCom_list->DisplayRecs, $OCom_list->TotalRecs) ?>
<?php if ($OCom_list->Pager->RecordCount > 0) { ?>
<div class="ewPager">
<span><?php echo $Language->Phrase("Page") ?>&nbsp;</span>
<div class="ewPrevNext"><div class="input-group">
<div class="input-group-btn">
<!--first page button-->
	<?php if ($OCom_list->Pager->FirstButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerFirst") ?>" href="<?php echo $OCom_list->PageUrl() ?>start=<?php echo $OCom_list->Pager->FirstButton->Start ?>"><span class="icon-first ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerFirst") ?>"><span class="icon-first ewIcon"></span></a>
	<?php } ?>
<!--previous page button-->
	<?php if ($OCom_list->Pager->PrevButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerPrevious") ?>" href="<?php echo $OCom_list->PageUrl() ?>start=<?php echo $OCom_list->Pager->PrevButton->Start ?>"><span class="icon-prev ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerPrevious") ?>"><span class="icon-prev ewIcon"></span></a>
	<?php } ?>
</div>
<!--current page number-->
	<input class="form-control input-sm" type="text" name="<?php echo EW_TABLE_PAGE_NO ?>" value="<?php echo $OCom_list->Pager->CurrentPage ?>">
<div class="input-group-btn">
<!--next page button-->
	<?php if ($OCom_list->Pager->NextButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerNext") ?>" href="<?php echo $OCom_list->PageUrl() ?>start=<?php echo $OCom_list->Pager->NextButton->Start ?>"><span class="icon-next ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerNext") ?>"><span class="icon-next ewIcon"></span></a>
	<?php } ?>
<!--last page button-->
	<?php if ($OCom_list->Pager->LastButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerLast") ?>" href="<?php echo $OCom_list->PageUrl() ?>start=<?php echo $OCom_list->Pager->LastButton->Start ?>"><span class="icon-last ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerLast") ?>"><span class="icon-last ewIcon"></span></a>
	<?php } ?>
</div>
</div>
</div>
<span>&nbsp;<?php echo $Language->Phrase("of") ?>&nbsp;<?php echo $OCom_list->Pager->PageCount ?></span>
</div>
<div class="ewPager ewRec">
	<span><?php echo $Language->Phrase("Record") ?>&nbsp;<?php echo $OCom_list->Pager->FromIndex ?>&nbsp;<?php echo $Language->Phrase("To") ?>&nbsp;<?php echo $OCom_list->Pager->ToIndex ?>&nbsp;<?php echo $Language->Phrase("Of") ?>&nbsp;<?php echo $OCom_list->Pager->RecordCount ?></span>
</div>
<?php } ?>
</form>
<?php } ?>
<div class="ewListOtherOptions">
<?php
	foreach ($OCom_list->OtherOptions as &$option)
		$option->Render("body", "bottom");
?>
</div>
<div class="clearfix"></div>
</div>
</div>
<?php } ?>
<?php if ($OCom_list->TotalRecs == 0 && $OCom->CurrentAction == "") { // Show other options ?>
<div class="ewListOtherOptions">
<?php
	foreach ($OCom_list->OtherOptions as &$option) {
		$option->ButtonClass = "";
		$option->Render("body", "");
	}
?>
</div>
<div class="clearfix"></div>
<?php } ?>
<script type="text/javascript">
fOComlistsrch.Init();
fOComlistsrch.FilterList = <?php echo $OCom_list->GetFilterList() ?>;
fOComlist.Init();
</script>
<?php
$OCom_list->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$OCom_list->Page_Terminate();
?>
