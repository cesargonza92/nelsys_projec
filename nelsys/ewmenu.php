<!-- Begin Main Menu -->
<?php $RootMenu = new cMenu(EW_MENUBAR_ID) ?>
<?php

// Generate all menu items
$RootMenu->IsRoot = TRUE;
$RootMenu->AddMenuItem(27, "mi_Clie", $Language->MenuPhrase("27", "MenuText"), "Clielist.php", -1, "", AllowListMenu('{04439FF7-B43F-460F-8514-F71C8FF9E679}Clie'), FALSE);
$RootMenu->AddMenuItem(24, "mci_Persona", $Language->MenuPhrase("24", "MenuText"), "", -1, "", TRUE, FALSE, TRUE);
$RootMenu->AddMenuItem(1, "mi_Usua", $Language->MenuPhrase("1", "MenuText"), "Usualist.php", 24, "", AllowListMenu('{04439FF7-B43F-460F-8514-F71C8FF9E679}Usua'), FALSE);
$RootMenu->AddMenuItem(13, "mi_Prov", $Language->MenuPhrase("13", "MenuText"), "Provlist.php", 24, "", AllowListMenu('{04439FF7-B43F-460F-8514-F71C8FF9E679}Prov'), FALSE);
$RootMenu->AddMenuItem(15, "mi_Empl", $Language->MenuPhrase("15", "MenuText"), "Empllist.php", 24, "", AllowListMenu('{04439FF7-B43F-460F-8514-F71C8FF9E679}Empl'), FALSE);
$RootMenu->AddMenuItem(25, "mci_Operaciones", $Language->MenuPhrase("25", "MenuText"), "", -1, "", TRUE, FALSE, TRUE);
$RootMenu->AddMenuItem(8, "mi_OSto", $Language->MenuPhrase("8", "MenuText"), "OStolist.php", 25, "", AllowListMenu('{04439FF7-B43F-460F-8514-F71C8FF9E679}OSto'), FALSE);
$RootMenu->AddMenuItem(20, "mi_OCom", $Language->MenuPhrase("20", "MenuText"), "OComlist.php", 25, "", AllowListMenu('{04439FF7-B43F-460F-8514-F71C8FF9E679}OCom'), FALSE);
$RootMenu->AddMenuItem(2, "mi_OVen", $Language->MenuPhrase("2", "MenuText"), "OVenlist.php", 25, "", AllowListMenu('{04439FF7-B43F-460F-8514-F71C8FF9E679}OVen'), FALSE);
$RootMenu->AddMenuItem(23, "mi_CCre", $Language->MenuPhrase("23", "MenuText"), "CCrelist.php", 2, "", AllowListMenu('{04439FF7-B43F-460F-8514-F71C8FF9E679}CCre'), FALSE);
$RootMenu->AddMenuItem(10, "mi_CCon", $Language->MenuPhrase("10", "MenuText"), "CConlist.php", 2, "", AllowListMenu('{04439FF7-B43F-460F-8514-F71C8FF9E679}CCon'), FALSE);
$RootMenu->AddMenuItem(21, "mi_VCre", $Language->MenuPhrase("21", "MenuText"), "VCrelist.php", 2, "", AllowListMenu('{04439FF7-B43F-460F-8514-F71C8FF9E679}VCre'), FALSE);
$RootMenu->AddMenuItem(7, "mi_VCon", $Language->MenuPhrase("7", "MenuText"), "VConlist.php", 2, "", AllowListMenu('{04439FF7-B43F-460F-8514-F71C8FF9E679}VCon'), FALSE);
$RootMenu->AddMenuItem(12, "mi_FCab", $Language->MenuPhrase("12", "MenuText"), "FCablist.php", 2, "", AllowListMenu('{04439FF7-B43F-460F-8514-F71C8FF9E679}FCab'), FALSE);
$RootMenu->AddMenuItem(5, "mi_FDet", $Language->MenuPhrase("5", "MenuText"), "FDetlist.php", 2, "", AllowListMenu('{04439FF7-B43F-460F-8514-F71C8FF9E679}FDet'), FALSE);
$RootMenu->AddMenuItem(11, "mi_DCre", $Language->MenuPhrase("11", "MenuText"), "DCrelist.php", 2, "", AllowListMenu('{04439FF7-B43F-460F-8514-F71C8FF9E679}DCre'), FALSE);
$RootMenu->AddMenuItem(26, "mci_Producto", $Language->MenuPhrase("26", "MenuText"), "", -1, "", TRUE, FALSE, TRUE);
$RootMenu->AddMenuItem(18, "mi_Prod", $Language->MenuPhrase("18", "MenuText"), "Prodlist.php", 26, "", AllowListMenu('{04439FF7-B43F-460F-8514-F71C8FF9E679}Prod'), FALSE);
$RootMenu->AddMenuItem(6, "mi_CPro", $Language->MenuPhrase("6", "MenuText"), "CProlist.php", 26, "", AllowListMenu('{04439FF7-B43F-460F-8514-F71C8FF9E679}CPro'), FALSE);
$RootMenu->AddMenuItem(9, "mi_Mpro", $Language->MenuPhrase("9", "MenuText"), "Mprolist.php", 26, "", AllowListMenu('{04439FF7-B43F-460F-8514-F71C8FF9E679}Mpro'), FALSE);
$RootMenu->AddMenuItem(17, "mi_TProd", $Language->MenuPhrase("17", "MenuText"), "TProdlist.php", 26, "", AllowListMenu('{04439FF7-B43F-460F-8514-F71C8FF9E679}TProd'), FALSE);
$RootMenu->AddMenuItem(-2, "mi_changepwd", $Language->Phrase("ChangePwd"), "changepwd.php", -1, "", IsLoggedIn() && !IsSysAdmin());
$RootMenu->AddMenuItem(-1, "mi_logout", $Language->Phrase("Logout"), "logout.php", -1, "", IsLoggedIn());
$RootMenu->AddMenuItem(-1, "mi_login", $Language->Phrase("Login"), "login.php", -1, "", !IsLoggedIn() && substr(@$_SERVER["URL"], -1 * strlen("login.php")) <> "login.php");
$RootMenu->Render();
?>
<!-- End Main Menu -->
