<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "CConinfo.php" ?>
<?php include_once "Usuainfo.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$CCon_edit = NULL; // Initialize page object first

class cCCon_edit extends cCCon {

	// Page ID
	var $PageID = 'edit';

	// Project ID
	var $ProjectID = "{04439FF7-B43F-460F-8514-F71C8FF9E679}";

	// Table name
	var $TableName = 'CCon';

	// Page object name
	var $PageObjName = 'CCon_edit';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (CCon)
		if (!isset($GLOBALS["CCon"]) || get_class($GLOBALS["CCon"]) == "cCCon") {
			$GLOBALS["CCon"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["CCon"];
		}

		// Table object (Usua)
		if (!isset($GLOBALS['Usua'])) $GLOBALS['Usua'] = new cUsua();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'edit', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'CCon', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (Usua)
		if (!isset($UserTable)) {
			$UserTable = new cUsua();
			$UserTableConn = Conn($UserTable->DBID);
		}
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanEdit()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage($Language->Phrase("NoPermission")); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("CConlist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}

		// Create form object
		$objForm = new cFormObj();
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action
		$this->CcoCodi->Visible = !$this->IsAdd() && !$this->IsCopy() && !$this->IsGridAdd();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Process auto fill
		if (@$_POST["ajax"] == "autofill") {
			$results = $this->GetAutoFill(@$_POST["name"], @$_POST["q"]);
			if ($results) {

				// Clean output buffer
				if (!EW_DEBUG_ENABLED && ob_get_length())
					ob_end_clean();
				echo $results;
				$this->Page_Terminate();
				exit();
			}
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $CCon;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($CCon);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $FormClassName = "form-horizontal ewForm ewEditForm";
	var $DbMasterFilter;
	var $DbDetailFilter;

	// 
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError;

		// Load key from QueryString
		if (@$_GET["CcoCodi"] <> "") {
			$this->CcoCodi->setQueryStringValue($_GET["CcoCodi"]);
		}

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Process form if post back
		if (@$_POST["a_edit"] <> "") {
			$this->CurrentAction = $_POST["a_edit"]; // Get action code
			$this->LoadFormValues(); // Get form values
		} else {
			$this->CurrentAction = "I"; // Default action is display
		}

		// Check if valid key
		if ($this->CcoCodi->CurrentValue == "")
			$this->Page_Terminate("CConlist.php"); // Invalid key, return to list

		// Validate form if post back
		if (@$_POST["a_edit"] <> "") {
			if (!$this->ValidateForm()) {
				$this->CurrentAction = ""; // Form error, reset action
				$this->setFailureMessage($gsFormError);
				$this->EventCancelled = TRUE; // Event cancelled
				$this->RestoreFormValues();
			}
		}
		switch ($this->CurrentAction) {
			case "I": // Get a record to display
				if (!$this->LoadRow()) { // Load record based on key
					if ($this->getFailureMessage() == "") $this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
					$this->Page_Terminate("CConlist.php"); // No matching record, return to list
				}
				break;
			Case "U": // Update
				$sReturnUrl = $this->getReturnUrl();
				$this->SendEmail = TRUE; // Send email on update success
				if ($this->EditRow()) { // Update record based on key
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage($Language->Phrase("UpdateSuccess")); // Update success
					$this->Page_Terminate($sReturnUrl); // Return to caller
				} elseif ($this->getFailureMessage() == $Language->Phrase("NoRecord")) {
					$this->Page_Terminate($sReturnUrl); // Return to caller
				} else {
					$this->EventCancelled = TRUE; // Event cancelled
					$this->RestoreFormValues(); // Restore form values if update failed
				}
		}

		// Render the record
		if ($this->CurrentAction == "F") { // Confirm page
			$this->RowType = EW_ROWTYPE_VIEW; // Render as View
		} else {
			$this->RowType = EW_ROWTYPE_EDIT; // Render as Edit
		}
		$this->ResetAttrs();
		$this->RenderRow();
	}

	// Set up starting record parameters
	function SetUpStartRec() {
		if ($this->DisplayRecs == 0)
			return;
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET[EW_TABLE_START_REC] <> "") { // Check for "start" parameter
				$this->StartRec = $_GET[EW_TABLE_START_REC];
				$this->setStartRecordNumber($this->StartRec);
			} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
				$PageNo = $_GET[EW_TABLE_PAGE_NO];
				if (is_numeric($PageNo)) {
					$this->StartRec = ($PageNo-1)*$this->DisplayRecs+1;
					if ($this->StartRec <= 0) {
						$this->StartRec = 1;
					} elseif ($this->StartRec >= intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1) {
						$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1;
					}
					$this->setStartRecordNumber($this->StartRec);
				}
			}
		}
		$this->StartRec = $this->getStartRecordNumber();

		// Check if correct start record counter
		if (!is_numeric($this->StartRec) || $this->StartRec == "") { // Avoid invalid start record counter
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} elseif (intval($this->StartRec) > intval($this->TotalRecs)) { // Avoid starting record > total records
			$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to last page first record
			$this->setStartRecordNumber($this->StartRec);
		} elseif (($this->StartRec-1) % $this->DisplayRecs <> 0) {
			$this->StartRec = intval(($this->StartRec-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to page boundary
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Get upload files
	function GetUploadFiles() {
		global $objForm, $Language;

		// Get upload data
	}

	// Load form values
	function LoadFormValues() {

		// Load from form
		global $objForm;
		if (!$this->CcoCodi->FldIsDetailKey)
			$this->CcoCodi->setFormValue($objForm->GetValue("x_CcoCodi"));
		if (!$this->VcoCodi->FldIsDetailKey) {
			$this->VcoCodi->setFormValue($objForm->GetValue("x_VcoCodi"));
		}
		if (!$this->CcoFPag->FldIsDetailKey) {
			$this->CcoFPag->setFormValue($objForm->GetValue("x_CcoFPag"));
			$this->CcoFPag->CurrentValue = ew_UnFormatDateTime($this->CcoFPag->CurrentValue, 7);
		}
		if (!$this->CcoMont->FldIsDetailKey) {
			$this->CcoMont->setFormValue($objForm->GetValue("x_CcoMont"));
		}
		if (!$this->CcoEsta->FldIsDetailKey) {
			$this->CcoEsta->setFormValue($objForm->GetValue("x_CcoEsta"));
		}
		if (!$this->CcoAnho->FldIsDetailKey) {
			$this->CcoAnho->setFormValue($objForm->GetValue("x_CcoAnho"));
		}
		if (!$this->CcoMes->FldIsDetailKey) {
			$this->CcoMes->setFormValue($objForm->GetValue("x_CcoMes"));
		}
		if (!$this->CcoConc->FldIsDetailKey) {
			$this->CcoConc->setFormValue($objForm->GetValue("x_CcoConc"));
		}
		if (!$this->CcoUsua->FldIsDetailKey) {
			$this->CcoUsua->setFormValue($objForm->GetValue("x_CcoUsua"));
		}
		if (!$this->CcoFCre->FldIsDetailKey) {
			$this->CcoFCre->setFormValue($objForm->GetValue("x_CcoFCre"));
			$this->CcoFCre->CurrentValue = ew_UnFormatDateTime($this->CcoFCre->CurrentValue, 7);
		}
	}

	// Restore form values
	function RestoreFormValues() {
		global $objForm;
		$this->LoadRow();
		$this->CcoCodi->CurrentValue = $this->CcoCodi->FormValue;
		$this->VcoCodi->CurrentValue = $this->VcoCodi->FormValue;
		$this->CcoFPag->CurrentValue = $this->CcoFPag->FormValue;
		$this->CcoFPag->CurrentValue = ew_UnFormatDateTime($this->CcoFPag->CurrentValue, 7);
		$this->CcoMont->CurrentValue = $this->CcoMont->FormValue;
		$this->CcoEsta->CurrentValue = $this->CcoEsta->FormValue;
		$this->CcoAnho->CurrentValue = $this->CcoAnho->FormValue;
		$this->CcoMes->CurrentValue = $this->CcoMes->FormValue;
		$this->CcoConc->CurrentValue = $this->CcoConc->FormValue;
		$this->CcoUsua->CurrentValue = $this->CcoUsua->FormValue;
		$this->CcoFCre->CurrentValue = $this->CcoFCre->FormValue;
		$this->CcoFCre->CurrentValue = ew_UnFormatDateTime($this->CcoFCre->CurrentValue, 7);
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->CcoCodi->setDbValue($rs->fields('CcoCodi'));
		$this->VcoCodi->setDbValue($rs->fields('VcoCodi'));
		$this->CcoFPag->setDbValue($rs->fields('CcoFPag'));
		$this->CcoMont->setDbValue($rs->fields('CcoMont'));
		$this->CcoEsta->setDbValue($rs->fields('CcoEsta'));
		$this->CcoAnho->setDbValue($rs->fields('CcoAnho'));
		$this->CcoMes->setDbValue($rs->fields('CcoMes'));
		$this->CcoConc->setDbValue($rs->fields('CcoConc'));
		$this->CcoUsua->setDbValue($rs->fields('CcoUsua'));
		$this->CcoFCre->setDbValue($rs->fields('CcoFCre'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->CcoCodi->DbValue = $row['CcoCodi'];
		$this->VcoCodi->DbValue = $row['VcoCodi'];
		$this->CcoFPag->DbValue = $row['CcoFPag'];
		$this->CcoMont->DbValue = $row['CcoMont'];
		$this->CcoEsta->DbValue = $row['CcoEsta'];
		$this->CcoAnho->DbValue = $row['CcoAnho'];
		$this->CcoMes->DbValue = $row['CcoMes'];
		$this->CcoConc->DbValue = $row['CcoConc'];
		$this->CcoUsua->DbValue = $row['CcoUsua'];
		$this->CcoFCre->DbValue = $row['CcoFCre'];
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		// Convert decimal values if posted back

		if ($this->CcoMont->FormValue == $this->CcoMont->CurrentValue && is_numeric(ew_StrToFloat($this->CcoMont->CurrentValue)))
			$this->CcoMont->CurrentValue = ew_StrToFloat($this->CcoMont->CurrentValue);

		// Call Row_Rendering event
		$this->Row_Rendering();

		// Common render codes for all row types
		// CcoCodi
		// VcoCodi
		// CcoFPag
		// CcoMont
		// CcoEsta
		// CcoAnho
		// CcoMes
		// CcoConc
		// CcoUsua
		// CcoFCre

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// CcoCodi
		$this->CcoCodi->ViewValue = $this->CcoCodi->CurrentValue;
		$this->CcoCodi->ViewCustomAttributes = "";

		// VcoCodi
		if (strval($this->VcoCodi->CurrentValue) <> "") {
			$sFilterWrk = "\"VcoCodi\"" . ew_SearchString("=", $this->VcoCodi->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT \"VcoCodi\", \"VcoCodi\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"VCon\"";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->VcoCodi, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->VcoCodi->ViewValue = $this->VcoCodi->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->VcoCodi->ViewValue = $this->VcoCodi->CurrentValue;
			}
		} else {
			$this->VcoCodi->ViewValue = NULL;
		}
		$this->VcoCodi->ViewCustomAttributes = "";

		// CcoFPag
		$this->CcoFPag->ViewValue = $this->CcoFPag->CurrentValue;
		$this->CcoFPag->ViewValue = ew_FormatDateTime($this->CcoFPag->ViewValue, 7);
		$this->CcoFPag->ViewCustomAttributes = "";

		// CcoMont
		$this->CcoMont->ViewValue = $this->CcoMont->CurrentValue;
		$this->CcoMont->ViewCustomAttributes = "";

		// CcoEsta
		$this->CcoEsta->ViewValue = $this->CcoEsta->CurrentValue;
		$this->CcoEsta->ViewCustomAttributes = "";

		// CcoAnho
		$this->CcoAnho->ViewValue = $this->CcoAnho->CurrentValue;
		$this->CcoAnho->ViewCustomAttributes = "";

		// CcoMes
		$this->CcoMes->ViewValue = $this->CcoMes->CurrentValue;
		$this->CcoMes->ViewCustomAttributes = "";

		// CcoConc
		$this->CcoConc->ViewValue = $this->CcoConc->CurrentValue;
		$this->CcoConc->ViewCustomAttributes = "";

		// CcoUsua
		$this->CcoUsua->ViewValue = $this->CcoUsua->CurrentValue;
		$this->CcoUsua->ViewCustomAttributes = "";

		// CcoFCre
		$this->CcoFCre->ViewValue = $this->CcoFCre->CurrentValue;
		$this->CcoFCre->ViewValue = ew_FormatDateTime($this->CcoFCre->ViewValue, 7);
		$this->CcoFCre->ViewCustomAttributes = "";

			// CcoCodi
			$this->CcoCodi->LinkCustomAttributes = "";
			$this->CcoCodi->HrefValue = "";
			$this->CcoCodi->TooltipValue = "";

			// VcoCodi
			$this->VcoCodi->LinkCustomAttributes = "";
			$this->VcoCodi->HrefValue = "";
			$this->VcoCodi->TooltipValue = "";

			// CcoFPag
			$this->CcoFPag->LinkCustomAttributes = "";
			$this->CcoFPag->HrefValue = "";
			$this->CcoFPag->TooltipValue = "";

			// CcoMont
			$this->CcoMont->LinkCustomAttributes = "";
			$this->CcoMont->HrefValue = "";
			$this->CcoMont->TooltipValue = "";

			// CcoEsta
			$this->CcoEsta->LinkCustomAttributes = "";
			$this->CcoEsta->HrefValue = "";
			$this->CcoEsta->TooltipValue = "";

			// CcoAnho
			$this->CcoAnho->LinkCustomAttributes = "";
			$this->CcoAnho->HrefValue = "";
			$this->CcoAnho->TooltipValue = "";

			// CcoMes
			$this->CcoMes->LinkCustomAttributes = "";
			$this->CcoMes->HrefValue = "";
			$this->CcoMes->TooltipValue = "";

			// CcoConc
			$this->CcoConc->LinkCustomAttributes = "";
			$this->CcoConc->HrefValue = "";
			$this->CcoConc->TooltipValue = "";

			// CcoUsua
			$this->CcoUsua->LinkCustomAttributes = "";
			$this->CcoUsua->HrefValue = "";
			$this->CcoUsua->TooltipValue = "";

			// CcoFCre
			$this->CcoFCre->LinkCustomAttributes = "";
			$this->CcoFCre->HrefValue = "";
			$this->CcoFCre->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_EDIT) { // Edit row

			// CcoCodi
			$this->CcoCodi->EditAttrs["class"] = "form-control";
			$this->CcoCodi->EditCustomAttributes = "";
			$this->CcoCodi->EditValue = $this->CcoCodi->CurrentValue;
			$this->CcoCodi->ViewCustomAttributes = "";

			// VcoCodi
			$this->VcoCodi->EditAttrs["class"] = "form-control";
			$this->VcoCodi->EditCustomAttributes = "";
			if (trim(strval($this->VcoCodi->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "\"VcoCodi\"" . ew_SearchString("=", $this->VcoCodi->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT \"VcoCodi\", \"VcoCodi\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\", '' AS \"SelectFilterFld\", '' AS \"SelectFilterFld2\", '' AS \"SelectFilterFld3\", '' AS \"SelectFilterFld4\" FROM \"public\".\"VCon\"";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->VcoCodi, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->VcoCodi->EditValue = $arwrk;

			// CcoFPag
			$this->CcoFPag->EditAttrs["class"] = "form-control";
			$this->CcoFPag->EditCustomAttributes = "";
			$this->CcoFPag->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->CcoFPag->CurrentValue, 7));
			$this->CcoFPag->PlaceHolder = ew_RemoveHtml($this->CcoFPag->FldCaption());

			// CcoMont
			$this->CcoMont->EditAttrs["class"] = "form-control";
			$this->CcoMont->EditCustomAttributes = "";
			$this->CcoMont->EditValue = ew_HtmlEncode($this->CcoMont->CurrentValue);
			$this->CcoMont->PlaceHolder = ew_RemoveHtml($this->CcoMont->FldCaption());
			if (strval($this->CcoMont->EditValue) <> "" && is_numeric($this->CcoMont->EditValue)) $this->CcoMont->EditValue = ew_FormatNumber($this->CcoMont->EditValue, -2, -1, -2, 0);

			// CcoEsta
			$this->CcoEsta->EditAttrs["class"] = "form-control";
			$this->CcoEsta->EditCustomAttributes = "";
			$this->CcoEsta->EditValue = ew_HtmlEncode($this->CcoEsta->CurrentValue);
			$this->CcoEsta->PlaceHolder = ew_RemoveHtml($this->CcoEsta->FldCaption());

			// CcoAnho
			$this->CcoAnho->EditAttrs["class"] = "form-control";
			$this->CcoAnho->EditCustomAttributes = "";
			$this->CcoAnho->EditValue = ew_HtmlEncode($this->CcoAnho->CurrentValue);
			$this->CcoAnho->PlaceHolder = ew_RemoveHtml($this->CcoAnho->FldCaption());

			// CcoMes
			$this->CcoMes->EditAttrs["class"] = "form-control";
			$this->CcoMes->EditCustomAttributes = "";
			$this->CcoMes->EditValue = ew_HtmlEncode($this->CcoMes->CurrentValue);
			$this->CcoMes->PlaceHolder = ew_RemoveHtml($this->CcoMes->FldCaption());

			// CcoConc
			$this->CcoConc->EditAttrs["class"] = "form-control";
			$this->CcoConc->EditCustomAttributes = "";
			$this->CcoConc->EditValue = ew_HtmlEncode($this->CcoConc->CurrentValue);
			$this->CcoConc->PlaceHolder = ew_RemoveHtml($this->CcoConc->FldCaption());

			// CcoUsua
			$this->CcoUsua->EditAttrs["class"] = "form-control";
			$this->CcoUsua->EditCustomAttributes = "";
			$this->CcoUsua->EditValue = ew_HtmlEncode($this->CcoUsua->CurrentValue);
			$this->CcoUsua->PlaceHolder = ew_RemoveHtml($this->CcoUsua->FldCaption());

			// CcoFCre
			$this->CcoFCre->EditAttrs["class"] = "form-control";
			$this->CcoFCre->EditCustomAttributes = "";
			$this->CcoFCre->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->CcoFCre->CurrentValue, 7));
			$this->CcoFCre->PlaceHolder = ew_RemoveHtml($this->CcoFCre->FldCaption());

			// Edit refer script
			// CcoCodi

			$this->CcoCodi->HrefValue = "";

			// VcoCodi
			$this->VcoCodi->HrefValue = "";

			// CcoFPag
			$this->CcoFPag->HrefValue = "";

			// CcoMont
			$this->CcoMont->HrefValue = "";

			// CcoEsta
			$this->CcoEsta->HrefValue = "";

			// CcoAnho
			$this->CcoAnho->HrefValue = "";

			// CcoMes
			$this->CcoMes->HrefValue = "";

			// CcoConc
			$this->CcoConc->HrefValue = "";

			// CcoUsua
			$this->CcoUsua->HrefValue = "";

			// CcoFCre
			$this->CcoFCre->HrefValue = "";
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate form
	function ValidateForm() {
		global $Language, $gsFormError;

		// Initialize form error message
		$gsFormError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return ($gsFormError == "");
		if (!$this->VcoCodi->FldIsDetailKey && !is_null($this->VcoCodi->FormValue) && $this->VcoCodi->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->VcoCodi->FldCaption(), $this->VcoCodi->ReqErrMsg));
		}
		if (!$this->CcoFPag->FldIsDetailKey && !is_null($this->CcoFPag->FormValue) && $this->CcoFPag->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->CcoFPag->FldCaption(), $this->CcoFPag->ReqErrMsg));
		}
		if (!ew_CheckEuroDate($this->CcoFPag->FormValue)) {
			ew_AddMessage($gsFormError, $this->CcoFPag->FldErrMsg());
		}
		if (!$this->CcoMont->FldIsDetailKey && !is_null($this->CcoMont->FormValue) && $this->CcoMont->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->CcoMont->FldCaption(), $this->CcoMont->ReqErrMsg));
		}
		if (!ew_CheckNumber($this->CcoMont->FormValue)) {
			ew_AddMessage($gsFormError, $this->CcoMont->FldErrMsg());
		}
		if (!$this->CcoEsta->FldIsDetailKey && !is_null($this->CcoEsta->FormValue) && $this->CcoEsta->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->CcoEsta->FldCaption(), $this->CcoEsta->ReqErrMsg));
		}
		if (!$this->CcoAnho->FldIsDetailKey && !is_null($this->CcoAnho->FormValue) && $this->CcoAnho->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->CcoAnho->FldCaption(), $this->CcoAnho->ReqErrMsg));
		}
		if (!$this->CcoMes->FldIsDetailKey && !is_null($this->CcoMes->FormValue) && $this->CcoMes->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->CcoMes->FldCaption(), $this->CcoMes->ReqErrMsg));
		}
		if (!$this->CcoConc->FldIsDetailKey && !is_null($this->CcoConc->FormValue) && $this->CcoConc->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->CcoConc->FldCaption(), $this->CcoConc->ReqErrMsg));
		}
		if (!$this->CcoUsua->FldIsDetailKey && !is_null($this->CcoUsua->FormValue) && $this->CcoUsua->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->CcoUsua->FldCaption(), $this->CcoUsua->ReqErrMsg));
		}
		if (!$this->CcoFCre->FldIsDetailKey && !is_null($this->CcoFCre->FormValue) && $this->CcoFCre->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->CcoFCre->FldCaption(), $this->CcoFCre->ReqErrMsg));
		}
		if (!ew_CheckEuroDate($this->CcoFCre->FormValue)) {
			ew_AddMessage($gsFormError, $this->CcoFCre->FldErrMsg());
		}

		// Return validate result
		$ValidateForm = ($gsFormError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateForm = $ValidateForm && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsFormError, $sFormCustomError);
		}
		return $ValidateForm;
	}

	// Update record based on key values
	function EditRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$conn = &$this->Connection();
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE)
			return FALSE;
		if ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record message
			$EditRow = FALSE; // Update Failed
		} else {

			// Save old values
			$rsold = &$rs->fields;
			$this->LoadDbValues($rsold);
			$rsnew = array();

			// VcoCodi
			$this->VcoCodi->SetDbValueDef($rsnew, $this->VcoCodi->CurrentValue, 0, $this->VcoCodi->ReadOnly);

			// CcoFPag
			$this->CcoFPag->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->CcoFPag->CurrentValue, 7), ew_CurrentDate(), $this->CcoFPag->ReadOnly);

			// CcoMont
			$this->CcoMont->SetDbValueDef($rsnew, $this->CcoMont->CurrentValue, 0, $this->CcoMont->ReadOnly);

			// CcoEsta
			$this->CcoEsta->SetDbValueDef($rsnew, $this->CcoEsta->CurrentValue, "", $this->CcoEsta->ReadOnly);

			// CcoAnho
			$this->CcoAnho->SetDbValueDef($rsnew, $this->CcoAnho->CurrentValue, "", $this->CcoAnho->ReadOnly);

			// CcoMes
			$this->CcoMes->SetDbValueDef($rsnew, $this->CcoMes->CurrentValue, "", $this->CcoMes->ReadOnly);

			// CcoConc
			$this->CcoConc->SetDbValueDef($rsnew, $this->CcoConc->CurrentValue, "", $this->CcoConc->ReadOnly);

			// CcoUsua
			$this->CcoUsua->SetDbValueDef($rsnew, $this->CcoUsua->CurrentValue, "", $this->CcoUsua->ReadOnly);

			// CcoFCre
			$this->CcoFCre->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->CcoFCre->CurrentValue, 7), ew_CurrentDate(), $this->CcoFCre->ReadOnly);

			// Call Row Updating event
			$bUpdateRow = $this->Row_Updating($rsold, $rsnew);
			if ($bUpdateRow) {
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				if (count($rsnew) > 0)
					$EditRow = $this->Update($rsnew, "", $rsold);
				else
					$EditRow = TRUE; // No field to update
				$conn->raiseErrorFn = '';
				if ($EditRow) {
				}
			} else {
				if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

					// Use the message, do nothing
				} elseif ($this->CancelMessage <> "") {
					$this->setFailureMessage($this->CancelMessage);
					$this->CancelMessage = "";
				} else {
					$this->setFailureMessage($Language->Phrase("UpdateCancelled"));
				}
				$EditRow = FALSE;
			}
		}

		// Call Row_Updated event
		if ($EditRow)
			$this->Row_Updated($rsold, $rsnew);
		$rs->Close();
		return $EditRow;
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, "CConlist.php", "", $this->TableVar, TRUE);
		$PageId = "edit";
		$Breadcrumb->Add("edit", $PageId, $url);
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($CCon_edit)) $CCon_edit = new cCCon_edit();

// Page init
$CCon_edit->Page_Init();

// Page main
$CCon_edit->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$CCon_edit->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "edit";
var CurrentForm = fCConedit = new ew_Form("fCConedit", "edit");

// Validate form
fCConedit.Validate = function() {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.GetForm(), $fobj = $(fobj);
	if ($fobj.find("#a_confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.FormKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = $fobj.find("#a_list").val() == "gridinsert";
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
			elm = this.GetElements("x" + infix + "_VcoCodi");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $CCon->VcoCodi->FldCaption(), $CCon->VcoCodi->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_CcoFPag");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $CCon->CcoFPag->FldCaption(), $CCon->CcoFPag->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_CcoFPag");
			if (elm && !ew_CheckEuroDate(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($CCon->CcoFPag->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_CcoMont");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $CCon->CcoMont->FldCaption(), $CCon->CcoMont->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_CcoMont");
			if (elm && !ew_CheckNumber(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($CCon->CcoMont->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_CcoEsta");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $CCon->CcoEsta->FldCaption(), $CCon->CcoEsta->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_CcoAnho");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $CCon->CcoAnho->FldCaption(), $CCon->CcoAnho->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_CcoMes");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $CCon->CcoMes->FldCaption(), $CCon->CcoMes->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_CcoConc");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $CCon->CcoConc->FldCaption(), $CCon->CcoConc->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_CcoUsua");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $CCon->CcoUsua->FldCaption(), $CCon->CcoUsua->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_CcoFCre");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $CCon->CcoFCre->FldCaption(), $CCon->CcoFCre->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_CcoFCre");
			if (elm && !ew_CheckEuroDate(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($CCon->CcoFCre->FldErrMsg()) ?>");

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
	}

	// Process detail forms
	var dfs = $fobj.find("input[name='detailpage']").get();
	for (var i = 0; i < dfs.length; i++) {
		var df = dfs[i], val = df.value;
		if (val && ewForms[val])
			if (!ewForms[val].Validate())
				return false;
	}
	return true;
}

// Form_CustomValidate event
fCConedit.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fCConedit.ValidateRequired = true;
<?php } else { ?>
fCConedit.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
fCConedit.Lists["x_VcoCodi"] = {"LinkField":"x_VcoCodi","Ajax":true,"AutoFill":false,"DisplayFields":["x_VcoCodi","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};

// Form object for search
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php $CCon_edit->ShowPageHeader(); ?>
<?php
$CCon_edit->ShowMessage();
?>
<form name="fCConedit" id="fCConedit" class="<?php echo $CCon_edit->FormClassName ?>" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($CCon_edit->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $CCon_edit->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="CCon">
<input type="hidden" name="a_edit" id="a_edit" value="U">
<?php if ($CCon->CurrentAction == "F") { // Confirm page ?>
<input type="hidden" name="a_confirm" id="a_confirm" value="F">
<?php } ?>
<div>
<?php if ($CCon->CcoCodi->Visible) { // CcoCodi ?>
	<div id="r_CcoCodi" class="form-group">
		<label id="elh_CCon_CcoCodi" class="col-sm-2 control-label ewLabel"><?php echo $CCon->CcoCodi->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $CCon->CcoCodi->CellAttributes() ?>>
<?php if ($CCon->CurrentAction <> "F") { ?>
<span id="el_CCon_CcoCodi">
<span<?php echo $CCon->CcoCodi->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $CCon->CcoCodi->EditValue ?></p></span>
</span>
<input type="hidden" data-table="CCon" data-field="x_CcoCodi" name="x_CcoCodi" id="x_CcoCodi" value="<?php echo ew_HtmlEncode($CCon->CcoCodi->CurrentValue) ?>">
<?php } else { ?>
<span id="el_CCon_CcoCodi">
<span<?php echo $CCon->CcoCodi->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $CCon->CcoCodi->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="CCon" data-field="x_CcoCodi" name="x_CcoCodi" id="x_CcoCodi" value="<?php echo ew_HtmlEncode($CCon->CcoCodi->FormValue) ?>">
<?php } ?>
<?php echo $CCon->CcoCodi->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($CCon->VcoCodi->Visible) { // VcoCodi ?>
	<div id="r_VcoCodi" class="form-group">
		<label id="elh_CCon_VcoCodi" for="x_VcoCodi" class="col-sm-2 control-label ewLabel"><?php echo $CCon->VcoCodi->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $CCon->VcoCodi->CellAttributes() ?>>
<?php if ($CCon->CurrentAction <> "F") { ?>
<span id="el_CCon_VcoCodi">
<select data-table="CCon" data-field="x_VcoCodi" data-value-separator="<?php echo ew_HtmlEncode(is_array($CCon->VcoCodi->DisplayValueSeparator) ? json_encode($CCon->VcoCodi->DisplayValueSeparator) : $CCon->VcoCodi->DisplayValueSeparator) ?>" id="x_VcoCodi" name="x_VcoCodi"<?php echo $CCon->VcoCodi->EditAttributes() ?>>
<?php
if (is_array($CCon->VcoCodi->EditValue)) {
	$arwrk = $CCon->VcoCodi->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($CCon->VcoCodi->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $CCon->VcoCodi->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($CCon->VcoCodi->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($CCon->VcoCodi->CurrentValue) ?>" selected><?php echo $CCon->VcoCodi->CurrentValue ?></option>
<?php
    }
}
?>
</select>
<?php
$sSqlWrk = "SELECT \"VcoCodi\", \"VcoCodi\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"VCon\"";
$sWhereWrk = "";
$CCon->VcoCodi->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$CCon->VcoCodi->LookupFilters += array("f0" => "\"VcoCodi\" = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$CCon->Lookup_Selecting($CCon->VcoCodi, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $CCon->VcoCodi->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x_VcoCodi" id="s_x_VcoCodi" value="<?php echo $CCon->VcoCodi->LookupFilterQuery() ?>">
</span>
<?php } else { ?>
<span id="el_CCon_VcoCodi">
<span<?php echo $CCon->VcoCodi->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $CCon->VcoCodi->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="CCon" data-field="x_VcoCodi" name="x_VcoCodi" id="x_VcoCodi" value="<?php echo ew_HtmlEncode($CCon->VcoCodi->FormValue) ?>">
<?php } ?>
<?php echo $CCon->VcoCodi->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($CCon->CcoFPag->Visible) { // CcoFPag ?>
	<div id="r_CcoFPag" class="form-group">
		<label id="elh_CCon_CcoFPag" for="x_CcoFPag" class="col-sm-2 control-label ewLabel"><?php echo $CCon->CcoFPag->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $CCon->CcoFPag->CellAttributes() ?>>
<?php if ($CCon->CurrentAction <> "F") { ?>
<span id="el_CCon_CcoFPag">
<input type="text" data-table="CCon" data-field="x_CcoFPag" data-format="7" name="x_CcoFPag" id="x_CcoFPag" placeholder="<?php echo ew_HtmlEncode($CCon->CcoFPag->getPlaceHolder()) ?>" value="<?php echo $CCon->CcoFPag->EditValue ?>"<?php echo $CCon->CcoFPag->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_CCon_CcoFPag">
<span<?php echo $CCon->CcoFPag->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $CCon->CcoFPag->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="CCon" data-field="x_CcoFPag" name="x_CcoFPag" id="x_CcoFPag" value="<?php echo ew_HtmlEncode($CCon->CcoFPag->FormValue) ?>">
<?php } ?>
<?php echo $CCon->CcoFPag->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($CCon->CcoMont->Visible) { // CcoMont ?>
	<div id="r_CcoMont" class="form-group">
		<label id="elh_CCon_CcoMont" for="x_CcoMont" class="col-sm-2 control-label ewLabel"><?php echo $CCon->CcoMont->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $CCon->CcoMont->CellAttributes() ?>>
<?php if ($CCon->CurrentAction <> "F") { ?>
<span id="el_CCon_CcoMont">
<input type="text" data-table="CCon" data-field="x_CcoMont" name="x_CcoMont" id="x_CcoMont" size="30" placeholder="<?php echo ew_HtmlEncode($CCon->CcoMont->getPlaceHolder()) ?>" value="<?php echo $CCon->CcoMont->EditValue ?>"<?php echo $CCon->CcoMont->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_CCon_CcoMont">
<span<?php echo $CCon->CcoMont->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $CCon->CcoMont->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="CCon" data-field="x_CcoMont" name="x_CcoMont" id="x_CcoMont" value="<?php echo ew_HtmlEncode($CCon->CcoMont->FormValue) ?>">
<?php } ?>
<?php echo $CCon->CcoMont->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($CCon->CcoEsta->Visible) { // CcoEsta ?>
	<div id="r_CcoEsta" class="form-group">
		<label id="elh_CCon_CcoEsta" for="x_CcoEsta" class="col-sm-2 control-label ewLabel"><?php echo $CCon->CcoEsta->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $CCon->CcoEsta->CellAttributes() ?>>
<?php if ($CCon->CurrentAction <> "F") { ?>
<span id="el_CCon_CcoEsta">
<input type="text" data-table="CCon" data-field="x_CcoEsta" name="x_CcoEsta" id="x_CcoEsta" size="30" maxlength="1" placeholder="<?php echo ew_HtmlEncode($CCon->CcoEsta->getPlaceHolder()) ?>" value="<?php echo $CCon->CcoEsta->EditValue ?>"<?php echo $CCon->CcoEsta->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_CCon_CcoEsta">
<span<?php echo $CCon->CcoEsta->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $CCon->CcoEsta->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="CCon" data-field="x_CcoEsta" name="x_CcoEsta" id="x_CcoEsta" value="<?php echo ew_HtmlEncode($CCon->CcoEsta->FormValue) ?>">
<?php } ?>
<?php echo $CCon->CcoEsta->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($CCon->CcoAnho->Visible) { // CcoAnho ?>
	<div id="r_CcoAnho" class="form-group">
		<label id="elh_CCon_CcoAnho" for="x_CcoAnho" class="col-sm-2 control-label ewLabel"><?php echo $CCon->CcoAnho->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $CCon->CcoAnho->CellAttributes() ?>>
<?php if ($CCon->CurrentAction <> "F") { ?>
<span id="el_CCon_CcoAnho">
<input type="text" data-table="CCon" data-field="x_CcoAnho" name="x_CcoAnho" id="x_CcoAnho" size="30" maxlength="4" placeholder="<?php echo ew_HtmlEncode($CCon->CcoAnho->getPlaceHolder()) ?>" value="<?php echo $CCon->CcoAnho->EditValue ?>"<?php echo $CCon->CcoAnho->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_CCon_CcoAnho">
<span<?php echo $CCon->CcoAnho->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $CCon->CcoAnho->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="CCon" data-field="x_CcoAnho" name="x_CcoAnho" id="x_CcoAnho" value="<?php echo ew_HtmlEncode($CCon->CcoAnho->FormValue) ?>">
<?php } ?>
<?php echo $CCon->CcoAnho->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($CCon->CcoMes->Visible) { // CcoMes ?>
	<div id="r_CcoMes" class="form-group">
		<label id="elh_CCon_CcoMes" for="x_CcoMes" class="col-sm-2 control-label ewLabel"><?php echo $CCon->CcoMes->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $CCon->CcoMes->CellAttributes() ?>>
<?php if ($CCon->CurrentAction <> "F") { ?>
<span id="el_CCon_CcoMes">
<input type="text" data-table="CCon" data-field="x_CcoMes" name="x_CcoMes" id="x_CcoMes" size="30" maxlength="2" placeholder="<?php echo ew_HtmlEncode($CCon->CcoMes->getPlaceHolder()) ?>" value="<?php echo $CCon->CcoMes->EditValue ?>"<?php echo $CCon->CcoMes->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_CCon_CcoMes">
<span<?php echo $CCon->CcoMes->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $CCon->CcoMes->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="CCon" data-field="x_CcoMes" name="x_CcoMes" id="x_CcoMes" value="<?php echo ew_HtmlEncode($CCon->CcoMes->FormValue) ?>">
<?php } ?>
<?php echo $CCon->CcoMes->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($CCon->CcoConc->Visible) { // CcoConc ?>
	<div id="r_CcoConc" class="form-group">
		<label id="elh_CCon_CcoConc" for="x_CcoConc" class="col-sm-2 control-label ewLabel"><?php echo $CCon->CcoConc->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $CCon->CcoConc->CellAttributes() ?>>
<?php if ($CCon->CurrentAction <> "F") { ?>
<span id="el_CCon_CcoConc">
<input type="text" data-table="CCon" data-field="x_CcoConc" name="x_CcoConc" id="x_CcoConc" size="30" placeholder="<?php echo ew_HtmlEncode($CCon->CcoConc->getPlaceHolder()) ?>" value="<?php echo $CCon->CcoConc->EditValue ?>"<?php echo $CCon->CcoConc->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_CCon_CcoConc">
<span<?php echo $CCon->CcoConc->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $CCon->CcoConc->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="CCon" data-field="x_CcoConc" name="x_CcoConc" id="x_CcoConc" value="<?php echo ew_HtmlEncode($CCon->CcoConc->FormValue) ?>">
<?php } ?>
<?php echo $CCon->CcoConc->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($CCon->CcoUsua->Visible) { // CcoUsua ?>
	<div id="r_CcoUsua" class="form-group">
		<label id="elh_CCon_CcoUsua" for="x_CcoUsua" class="col-sm-2 control-label ewLabel"><?php echo $CCon->CcoUsua->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $CCon->CcoUsua->CellAttributes() ?>>
<?php if ($CCon->CurrentAction <> "F") { ?>
<span id="el_CCon_CcoUsua">
<input type="text" data-table="CCon" data-field="x_CcoUsua" name="x_CcoUsua" id="x_CcoUsua" size="30" placeholder="<?php echo ew_HtmlEncode($CCon->CcoUsua->getPlaceHolder()) ?>" value="<?php echo $CCon->CcoUsua->EditValue ?>"<?php echo $CCon->CcoUsua->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_CCon_CcoUsua">
<span<?php echo $CCon->CcoUsua->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $CCon->CcoUsua->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="CCon" data-field="x_CcoUsua" name="x_CcoUsua" id="x_CcoUsua" value="<?php echo ew_HtmlEncode($CCon->CcoUsua->FormValue) ?>">
<?php } ?>
<?php echo $CCon->CcoUsua->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($CCon->CcoFCre->Visible) { // CcoFCre ?>
	<div id="r_CcoFCre" class="form-group">
		<label id="elh_CCon_CcoFCre" for="x_CcoFCre" class="col-sm-2 control-label ewLabel"><?php echo $CCon->CcoFCre->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $CCon->CcoFCre->CellAttributes() ?>>
<?php if ($CCon->CurrentAction <> "F") { ?>
<span id="el_CCon_CcoFCre">
<input type="text" data-table="CCon" data-field="x_CcoFCre" data-format="7" name="x_CcoFCre" id="x_CcoFCre" placeholder="<?php echo ew_HtmlEncode($CCon->CcoFCre->getPlaceHolder()) ?>" value="<?php echo $CCon->CcoFCre->EditValue ?>"<?php echo $CCon->CcoFCre->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_CCon_CcoFCre">
<span<?php echo $CCon->CcoFCre->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $CCon->CcoFCre->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="CCon" data-field="x_CcoFCre" name="x_CcoFCre" id="x_CcoFCre" value="<?php echo ew_HtmlEncode($CCon->CcoFCre->FormValue) ?>">
<?php } ?>
<?php echo $CCon->CcoFCre->CustomMsg ?></div></div>
	</div>
<?php } ?>
</div>
<div class="form-group">
	<div class="col-sm-offset-2 col-sm-10">
<?php if ($CCon->CurrentAction <> "F") { // Confirm page ?>
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit" onclick="this.form.a_edit.value='F';"><?php echo $Language->Phrase("SaveBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="button" data-href="<?php echo $CCon_edit->getReturnUrl() ?>"><?php echo $Language->Phrase("CancelBtn") ?></button>
<?php } else { ?>
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("ConfirmBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="submit" onclick="this.form.a_edit.value='X';"><?php echo $Language->Phrase("CancelBtn") ?></button>
<?php } ?>
	</div>
</div>
</form>
<script type="text/javascript">
fCConedit.Init();
</script>
<?php
$CCon_edit->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$CCon_edit->Page_Terminate();
?>
