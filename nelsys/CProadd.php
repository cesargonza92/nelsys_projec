<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "CProinfo.php" ?>
<?php include_once "Usuainfo.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$CPro_add = NULL; // Initialize page object first

class cCPro_add extends cCPro {

	// Page ID
	var $PageID = 'add';

	// Project ID
	var $ProjectID = "{04439FF7-B43F-460F-8514-F71C8FF9E679}";

	// Table name
	var $TableName = 'CPro';

	// Page object name
	var $PageObjName = 'CPro_add';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (CPro)
		if (!isset($GLOBALS["CPro"]) || get_class($GLOBALS["CPro"]) == "cCPro") {
			$GLOBALS["CPro"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["CPro"];
		}

		// Table object (Usua)
		if (!isset($GLOBALS['Usua'])) $GLOBALS['Usua'] = new cUsua();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'add', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'CPro', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (Usua)
		if (!isset($UserTable)) {
			$UserTable = new cUsua();
			$UserTableConn = Conn($UserTable->DBID);
		}
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanAdd()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage($Language->Phrase("NoPermission")); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("CProlist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}

		// Create form object
		$objForm = new cFormObj();
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Process auto fill
		if (@$_POST["ajax"] == "autofill") {
			$results = $this->GetAutoFill(@$_POST["name"], @$_POST["q"]);
			if ($results) {

				// Clean output buffer
				if (!EW_DEBUG_ENABLED && ob_get_length())
					ob_end_clean();
				echo $results;
				$this->Page_Terminate();
				exit();
			}
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $CPro;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($CPro);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $FormClassName = "form-horizontal ewForm ewAddForm";
	var $DbMasterFilter = "";
	var $DbDetailFilter = "";
	var $StartRec;
	var $Priv = 0;
	var $OldRecordset;
	var $CopyRecord;

	// 
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError;

		// Process form if post back
		if (@$_POST["a_add"] <> "") {
			$this->CurrentAction = $_POST["a_add"]; // Get form action
			$this->CopyRecord = $this->LoadOldRecord(); // Load old recordset
			$this->LoadFormValues(); // Load form values
		} else { // Not post back

			// Load key values from QueryString
			$this->CopyRecord = TRUE;
			if (@$_GET["CprCodi"] != "") {
				$this->CprCodi->setQueryStringValue($_GET["CprCodi"]);
				$this->setKey("CprCodi", $this->CprCodi->CurrentValue); // Set up key
			} else {
				$this->setKey("CprCodi", ""); // Clear key
				$this->CopyRecord = FALSE;
			}
			if ($this->CopyRecord) {
				$this->CurrentAction = "C"; // Copy record
			} else {
				$this->CurrentAction = "I"; // Display blank record
				$this->LoadDefaultValues(); // Load default values
			}
		}

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Validate form if post back
		if (@$_POST["a_add"] <> "") {
			if (!$this->ValidateForm()) {
				$this->CurrentAction = "I"; // Form error, reset action
				$this->EventCancelled = TRUE; // Event cancelled
				$this->RestoreFormValues(); // Restore form values
				$this->setFailureMessage($gsFormError);
			}
		}

		// Perform action based on action code
		switch ($this->CurrentAction) {
			case "I": // Blank record, no action required
				break;
			case "C": // Copy an existing record
				if (!$this->LoadRow()) { // Load record based on key
					if ($this->getFailureMessage() == "") $this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
					$this->Page_Terminate("CProlist.php"); // No matching record, return to list
				}
				break;
			case "A": // Add new record
				$this->SendEmail = TRUE; // Send email on add success
				if ($this->AddRow($this->OldRecordset)) { // Add successful
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage($Language->Phrase("AddSuccess")); // Set up success message
					$sReturnUrl = $this->getReturnUrl();
					if (ew_GetPageName($sReturnUrl) == "CProview.php")
						$sReturnUrl = $this->GetViewUrl(); // View paging, return to view page with keyurl directly
					$this->Page_Terminate($sReturnUrl); // Clean up and return
				} else {
					$this->EventCancelled = TRUE; // Event cancelled
					$this->RestoreFormValues(); // Add failed, restore form values
				}
		}

		// Render row based on row type
		if ($this->CurrentAction == "F") { // Confirm page
			$this->RowType = EW_ROWTYPE_VIEW; // Render view type
		} else {
			$this->RowType = EW_ROWTYPE_ADD; // Render add type
		}

		// Render row
		$this->ResetAttrs();
		$this->RenderRow();
	}

	// Get upload files
	function GetUploadFiles() {
		global $objForm, $Language;

		// Get upload data
	}

	// Load default values
	function LoadDefaultValues() {
		$this->CprNomb->CurrentValue = NULL;
		$this->CprNomb->OldValue = $this->CprNomb->CurrentValue;
		$this->CprDesc->CurrentValue = NULL;
		$this->CprDesc->OldValue = $this->CprDesc->CurrentValue;
		$this->CprUsua->CurrentValue = NULL;
		$this->CprUsua->OldValue = $this->CprUsua->CurrentValue;
	}

	// Load form values
	function LoadFormValues() {

		// Load from form
		global $objForm;
		if (!$this->CprNomb->FldIsDetailKey) {
			$this->CprNomb->setFormValue($objForm->GetValue("x_CprNomb"));
		}
		if (!$this->CprDesc->FldIsDetailKey) {
			$this->CprDesc->setFormValue($objForm->GetValue("x_CprDesc"));
		}
		if (!$this->CprUsua->FldIsDetailKey) {
			$this->CprUsua->setFormValue($objForm->GetValue("x_CprUsua"));
		}
	}

	// Restore form values
	function RestoreFormValues() {
		global $objForm;
		$this->LoadOldRecord();
		$this->CprNomb->CurrentValue = $this->CprNomb->FormValue;
		$this->CprDesc->CurrentValue = $this->CprDesc->FormValue;
		$this->CprUsua->CurrentValue = $this->CprUsua->FormValue;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->CprCodi->setDbValue($rs->fields('CprCodi'));
		$this->CprNomb->setDbValue($rs->fields('CprNomb'));
		$this->CprDesc->setDbValue($rs->fields('CprDesc'));
		$this->CprUsua->setDbValue($rs->fields('CprUsua'));
		$this->CprFCre->setDbValue($rs->fields('CprFCre'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->CprCodi->DbValue = $row['CprCodi'];
		$this->CprNomb->DbValue = $row['CprNomb'];
		$this->CprDesc->DbValue = $row['CprDesc'];
		$this->CprUsua->DbValue = $row['CprUsua'];
		$this->CprFCre->DbValue = $row['CprFCre'];
	}

	// Load old record
	function LoadOldRecord() {

		// Load key values from Session
		$bValidKey = TRUE;
		if (strval($this->getKey("CprCodi")) <> "")
			$this->CprCodi->CurrentValue = $this->getKey("CprCodi"); // CprCodi
		else
			$bValidKey = FALSE;

		// Load old recordset
		if ($bValidKey) {
			$this->CurrentFilter = $this->KeyFilter();
			$sSql = $this->SQL();
			$conn = &$this->Connection();
			$this->OldRecordset = ew_LoadRecordset($sSql, $conn);
			$this->LoadRowValues($this->OldRecordset); // Load row values
		} else {
			$this->OldRecordset = NULL;
		}
		return $bValidKey;
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		// Call Row_Rendering event

		$this->Row_Rendering();

		// Common render codes for all row types
		// CprCodi
		// CprNomb
		// CprDesc
		// CprUsua
		// CprFCre

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// CprCodi
		$this->CprCodi->ViewValue = $this->CprCodi->CurrentValue;
		$this->CprCodi->ViewCustomAttributes = "";

		// CprNomb
		$this->CprNomb->ViewValue = $this->CprNomb->CurrentValue;
		$this->CprNomb->ViewCustomAttributes = "";

		// CprDesc
		$this->CprDesc->ViewValue = $this->CprDesc->CurrentValue;
		$this->CprDesc->ViewCustomAttributes = "";

		// CprUsua
		$this->CprUsua->ViewValue = $this->CprUsua->CurrentValue;
		$this->CprUsua->ViewCustomAttributes = "";

		// CprFCre
		$this->CprFCre->ViewValue = $this->CprFCre->CurrentValue;
		$this->CprFCre->ViewValue = ew_FormatDateTime($this->CprFCre->ViewValue, 7);
		$this->CprFCre->ViewCustomAttributes = "";

			// CprNomb
			$this->CprNomb->LinkCustomAttributes = "";
			$this->CprNomb->HrefValue = "";
			$this->CprNomb->TooltipValue = "";

			// CprDesc
			$this->CprDesc->LinkCustomAttributes = "";
			$this->CprDesc->HrefValue = "";
			$this->CprDesc->TooltipValue = "";

			// CprUsua
			$this->CprUsua->LinkCustomAttributes = "";
			$this->CprUsua->HrefValue = "";
			$this->CprUsua->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_ADD) { // Add row

			// CprNomb
			$this->CprNomb->EditAttrs["class"] = "form-control";
			$this->CprNomb->EditCustomAttributes = "";
			$this->CprNomb->EditValue = ew_HtmlEncode($this->CprNomb->CurrentValue);
			$this->CprNomb->PlaceHolder = ew_RemoveHtml($this->CprNomb->FldCaption());

			// CprDesc
			$this->CprDesc->EditAttrs["class"] = "form-control";
			$this->CprDesc->EditCustomAttributes = "";
			$this->CprDesc->EditValue = ew_HtmlEncode($this->CprDesc->CurrentValue);
			$this->CprDesc->PlaceHolder = ew_RemoveHtml($this->CprDesc->FldCaption());

			// CprUsua
			$this->CprUsua->EditAttrs["class"] = "form-control";
			$this->CprUsua->EditCustomAttributes = "";
			$this->CprUsua->EditValue = ew_HtmlEncode($this->CprUsua->CurrentValue);
			$this->CprUsua->PlaceHolder = ew_RemoveHtml($this->CprUsua->FldCaption());

			// Edit refer script
			// CprNomb

			$this->CprNomb->HrefValue = "";

			// CprDesc
			$this->CprDesc->HrefValue = "";

			// CprUsua
			$this->CprUsua->HrefValue = "";
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate form
	function ValidateForm() {
		global $Language, $gsFormError;

		// Initialize form error message
		$gsFormError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return ($gsFormError == "");
		if (!$this->CprNomb->FldIsDetailKey && !is_null($this->CprNomb->FormValue) && $this->CprNomb->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->CprNomb->FldCaption(), $this->CprNomb->ReqErrMsg));
		}
		if (!$this->CprUsua->FldIsDetailKey && !is_null($this->CprUsua->FormValue) && $this->CprUsua->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->CprUsua->FldCaption(), $this->CprUsua->ReqErrMsg));
		}

		// Return validate result
		$ValidateForm = ($gsFormError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateForm = $ValidateForm && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsFormError, $sFormCustomError);
		}
		return $ValidateForm;
	}

	// Add record
	function AddRow($rsold = NULL) {
		global $Language, $Security;
		$conn = &$this->Connection();

		// Load db values from rsold
		if ($rsold) {
			$this->LoadDbValues($rsold);
		}
		$rsnew = array();

		// CprNomb
		$this->CprNomb->SetDbValueDef($rsnew, $this->CprNomb->CurrentValue, "", FALSE);

		// CprDesc
		$this->CprDesc->SetDbValueDef($rsnew, $this->CprDesc->CurrentValue, NULL, FALSE);

		// CprUsua
		$this->CprUsua->SetDbValueDef($rsnew, $this->CprUsua->CurrentValue, "", FALSE);

		// Call Row Inserting event
		$rs = ($rsold == NULL) ? NULL : $rsold->fields;
		$bInsertRow = $this->Row_Inserting($rs, $rsnew);
		if ($bInsertRow) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			$AddRow = $this->Insert($rsnew);
			$conn->raiseErrorFn = '';
			if ($AddRow) {

				// Get insert id if necessary
				$this->CprCodi->setDbValue($conn->GetOne("SELECT currval('\"CPro_CprCodi_seq\"'::regclass)"));
				$rsnew['CprCodi'] = $this->CprCodi->DbValue;
			}
		} else {
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("InsertCancelled"));
			}
			$AddRow = FALSE;
		}
		if ($AddRow) {

			// Call Row Inserted event
			$rs = ($rsold == NULL) ? NULL : $rsold->fields;
			$this->Row_Inserted($rs, $rsnew);
		}
		return $AddRow;
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, "CProlist.php", "", $this->TableVar, TRUE);
		$PageId = ($this->CurrentAction == "C") ? "Copy" : "Add";
		$Breadcrumb->Add("add", $PageId, $url);
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($CPro_add)) $CPro_add = new cCPro_add();

// Page init
$CPro_add->Page_Init();

// Page main
$CPro_add->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$CPro_add->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "add";
var CurrentForm = fCProadd = new ew_Form("fCProadd", "add");

// Validate form
fCProadd.Validate = function() {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.GetForm(), $fobj = $(fobj);
	if ($fobj.find("#a_confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.FormKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = $fobj.find("#a_list").val() == "gridinsert";
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
			elm = this.GetElements("x" + infix + "_CprNomb");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $CPro->CprNomb->FldCaption(), $CPro->CprNomb->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_CprUsua");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $CPro->CprUsua->FldCaption(), $CPro->CprUsua->ReqErrMsg)) ?>");

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
	}

	// Process detail forms
	var dfs = $fobj.find("input[name='detailpage']").get();
	for (var i = 0; i < dfs.length; i++) {
		var df = dfs[i], val = df.value;
		if (val && ewForms[val])
			if (!ewForms[val].Validate())
				return false;
	}
	return true;
}

// Form_CustomValidate event
fCProadd.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fCProadd.ValidateRequired = true;
<?php } else { ?>
fCProadd.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
// Form object for search

</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php $CPro_add->ShowPageHeader(); ?>
<?php
$CPro_add->ShowMessage();
?>
<form name="fCProadd" id="fCProadd" class="<?php echo $CPro_add->FormClassName ?>" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($CPro_add->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $CPro_add->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="CPro">
<input type="hidden" name="a_add" id="a_add" value="A">
<?php if ($CPro->CurrentAction == "F") { // Confirm page ?>
<input type="hidden" name="a_confirm" id="a_confirm" value="F">
<?php } ?>
<div>
<?php if ($CPro->CprNomb->Visible) { // CprNomb ?>
	<div id="r_CprNomb" class="form-group">
		<label id="elh_CPro_CprNomb" for="x_CprNomb" class="col-sm-2 control-label ewLabel"><?php echo $CPro->CprNomb->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $CPro->CprNomb->CellAttributes() ?>>
<?php if ($CPro->CurrentAction <> "F") { ?>
<span id="el_CPro_CprNomb">
<input type="text" data-table="CPro" data-field="x_CprNomb" name="x_CprNomb" id="x_CprNomb" size="30" placeholder="<?php echo ew_HtmlEncode($CPro->CprNomb->getPlaceHolder()) ?>" value="<?php echo $CPro->CprNomb->EditValue ?>"<?php echo $CPro->CprNomb->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_CPro_CprNomb">
<span<?php echo $CPro->CprNomb->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $CPro->CprNomb->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="CPro" data-field="x_CprNomb" name="x_CprNomb" id="x_CprNomb" value="<?php echo ew_HtmlEncode($CPro->CprNomb->FormValue) ?>">
<?php } ?>
<?php echo $CPro->CprNomb->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($CPro->CprDesc->Visible) { // CprDesc ?>
	<div id="r_CprDesc" class="form-group">
		<label id="elh_CPro_CprDesc" for="x_CprDesc" class="col-sm-2 control-label ewLabel"><?php echo $CPro->CprDesc->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $CPro->CprDesc->CellAttributes() ?>>
<?php if ($CPro->CurrentAction <> "F") { ?>
<span id="el_CPro_CprDesc">
<input type="text" data-table="CPro" data-field="x_CprDesc" name="x_CprDesc" id="x_CprDesc" size="30" placeholder="<?php echo ew_HtmlEncode($CPro->CprDesc->getPlaceHolder()) ?>" value="<?php echo $CPro->CprDesc->EditValue ?>"<?php echo $CPro->CprDesc->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_CPro_CprDesc">
<span<?php echo $CPro->CprDesc->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $CPro->CprDesc->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="CPro" data-field="x_CprDesc" name="x_CprDesc" id="x_CprDesc" value="<?php echo ew_HtmlEncode($CPro->CprDesc->FormValue) ?>">
<?php } ?>
<?php echo $CPro->CprDesc->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($CPro->CprUsua->Visible) { // CprUsua ?>
	<div id="r_CprUsua" class="form-group">
		<label id="elh_CPro_CprUsua" for="x_CprUsua" class="col-sm-2 control-label ewLabel"><?php echo $CPro->CprUsua->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $CPro->CprUsua->CellAttributes() ?>>
<?php if ($CPro->CurrentAction <> "F") { ?>
<span id="el_CPro_CprUsua">
<input type="text" data-table="CPro" data-field="x_CprUsua" name="x_CprUsua" id="x_CprUsua" size="30" placeholder="<?php echo ew_HtmlEncode($CPro->CprUsua->getPlaceHolder()) ?>" value="<?php echo $CPro->CprUsua->EditValue ?>"<?php echo $CPro->CprUsua->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_CPro_CprUsua">
<span<?php echo $CPro->CprUsua->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $CPro->CprUsua->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="CPro" data-field="x_CprUsua" name="x_CprUsua" id="x_CprUsua" value="<?php echo ew_HtmlEncode($CPro->CprUsua->FormValue) ?>">
<?php } ?>
<?php echo $CPro->CprUsua->CustomMsg ?></div></div>
	</div>
<?php } ?>
</div>
<div class="form-group">
	<div class="col-sm-offset-2 col-sm-10">
<?php if ($CPro->CurrentAction <> "F") { // Confirm page ?>
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit" onclick="this.form.a_add.value='F';"><?php echo $Language->Phrase("AddBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="button" data-href="<?php echo $CPro_add->getReturnUrl() ?>"><?php echo $Language->Phrase("CancelBtn") ?></button>
<?php } else { ?>
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("ConfirmBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="submit" onclick="this.form.a_add.value='X';"><?php echo $Language->Phrase("CancelBtn") ?></button>
<?php } ?>
	</div>
</div>
</form>
<script type="text/javascript">
fCProadd.Init();
</script>
<?php
$CPro_add->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$CPro_add->Page_Terminate();
?>
