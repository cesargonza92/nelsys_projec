<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "OVeninfo.php" ?>
<?php include_once "Usuainfo.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$OVen_list = NULL; // Initialize page object first

class cOVen_list extends cOVen {

	// Page ID
	var $PageID = 'list';

	// Project ID
	var $ProjectID = "{04439FF7-B43F-460F-8514-F71C8FF9E679}";

	// Table name
	var $TableName = 'OVen';

	// Page object name
	var $PageObjName = 'OVen_list';

	// Grid form hidden field names
	var $FormName = 'fOVenlist';
	var $FormActionName = 'k_action';
	var $FormKeyName = 'k_key';
	var $FormOldKeyName = 'k_oldkey';
	var $FormBlankRowName = 'k_blankrow';
	var $FormKeyCountName = 'key_count';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Page URLs
	var $AddUrl;
	var $EditUrl;
	var $CopyUrl;
	var $DeleteUrl;
	var $ViewUrl;
	var $ListUrl;

	// Export URLs
	var $ExportPrintUrl;
	var $ExportHtmlUrl;
	var $ExportExcelUrl;
	var $ExportWordUrl;
	var $ExportXmlUrl;
	var $ExportCsvUrl;
	var $ExportPdfUrl;

	// Custom export
	var $ExportExcelCustom = FALSE;
	var $ExportWordCustom = FALSE;
	var $ExportPdfCustom = FALSE;
	var $ExportEmailCustom = FALSE;

	// Update URLs
	var $InlineAddUrl;
	var $InlineCopyUrl;
	var $InlineEditUrl;
	var $GridAddUrl;
	var $GridEditUrl;
	var $MultiDeleteUrl;
	var $MultiUpdateUrl;

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (OVen)
		if (!isset($GLOBALS["OVen"]) || get_class($GLOBALS["OVen"]) == "cOVen") {
			$GLOBALS["OVen"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["OVen"];
		}

		// Initialize URLs
		$this->ExportPrintUrl = $this->PageUrl() . "export=print";
		$this->ExportExcelUrl = $this->PageUrl() . "export=excel";
		$this->ExportWordUrl = $this->PageUrl() . "export=word";
		$this->ExportHtmlUrl = $this->PageUrl() . "export=html";
		$this->ExportXmlUrl = $this->PageUrl() . "export=xml";
		$this->ExportCsvUrl = $this->PageUrl() . "export=csv";
		$this->ExportPdfUrl = $this->PageUrl() . "export=pdf";
		$this->AddUrl = "OVenadd.php";
		$this->InlineAddUrl = $this->PageUrl() . "a=add";
		$this->GridAddUrl = $this->PageUrl() . "a=gridadd";
		$this->GridEditUrl = $this->PageUrl() . "a=gridedit";
		$this->MultiDeleteUrl = "OVendelete.php";
		$this->MultiUpdateUrl = "OVenupdate.php";

		// Table object (Usua)
		if (!isset($GLOBALS['Usua'])) $GLOBALS['Usua'] = new cUsua();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'list', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'OVen', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (Usua)
		if (!isset($UserTable)) {
			$UserTable = new cUsua();
			$UserTableConn = Conn($UserTable->DBID);
		}

		// List options
		$this->ListOptions = new cListOptions();
		$this->ListOptions->TableVar = $this->TableVar;

		// Export options
		$this->ExportOptions = new cListOptions();
		$this->ExportOptions->Tag = "div";
		$this->ExportOptions->TagClassName = "ewExportOption";

		// Other options
		$this->OtherOptions['addedit'] = new cListOptions();
		$this->OtherOptions['addedit']->Tag = "div";
		$this->OtherOptions['addedit']->TagClassName = "ewAddEditOption";
		$this->OtherOptions['detail'] = new cListOptions();
		$this->OtherOptions['detail']->Tag = "div";
		$this->OtherOptions['detail']->TagClassName = "ewDetailOption";
		$this->OtherOptions['action'] = new cListOptions();
		$this->OtherOptions['action']->Tag = "div";
		$this->OtherOptions['action']->TagClassName = "ewActionOption";

		// Filter options
		$this->FilterOptions = new cListOptions();
		$this->FilterOptions->Tag = "div";
		$this->FilterOptions->TagClassName = "ewFilterOption fOVenlistsrch";

		// List actions
		$this->ListActions = new cListActions();
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanList()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage($Language->Phrase("NoPermission")); // Set no permission
			$this->Page_Terminate(ew_GetUrl("index.php"));
		}

		// Create form object
		$objForm = new cFormObj();
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action

		// Get grid add count
		$gridaddcnt = @$_GET[EW_TABLE_GRID_ADD_ROW_COUNT];
		if (is_numeric($gridaddcnt) && $gridaddcnt > 0)
			$this->GridAddRowCount = $gridaddcnt;

		// Set up list options
		$this->SetupListOptions();
		$this->OveCodi->Visible = !$this->IsAdd() && !$this->IsCopy() && !$this->IsGridAdd();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Process auto fill
		if (@$_POST["ajax"] == "autofill") {
			$results = $this->GetAutoFill(@$_POST["name"], @$_POST["q"]);
			if ($results) {

				// Clean output buffer
				if (!EW_DEBUG_ENABLED && ob_get_length())
					ob_end_clean();
				echo $results;
				$this->Page_Terminate();
				exit();
			}
		}

		// Create Token
		$this->CreateToken();

		// Setup other options
		$this->SetupOtherOptions();

		// Set up custom action (compatible with old version)
		foreach ($this->CustomActions as $name => $action)
			$this->ListActions->Add($name, $action);

		// Show checkbox column if multiple action
		foreach ($this->ListActions->Items as $listaction) {
			if ($listaction->Select == EW_ACTION_MULTIPLE && $listaction->Allow) {
				$this->ListOptions->Items["checkbox"]->Visible = TRUE;
				break;
			}
		}
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $OVen;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($OVen);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}

	// Class variables
	var $ListOptions; // List options
	var $ExportOptions; // Export options
	var $SearchOptions; // Search options
	var $OtherOptions = array(); // Other options
	var $FilterOptions; // Filter options
	var $ListActions; // List actions
	var $SelectedCount = 0;
	var $SelectedIndex = 0;
	var $DisplayRecs = 20;
	var $StartRec;
	var $StopRec;
	var $TotalRecs = 0;
	var $RecRange = 10;
	var $Pager;
	var $DefaultSearchWhere = ""; // Default search WHERE clause
	var $SearchWhere = ""; // Search WHERE clause
	var $RecCnt = 0; // Record count
	var $EditRowCnt;
	var $StartRowCnt = 1;
	var $RowCnt = 0;
	var $Attrs = array(); // Row attributes and cell attributes
	var $RowIndex = 0; // Row index
	var $KeyCount = 0; // Key count
	var $RowAction = ""; // Row action
	var $RowOldKey = ""; // Row old key (for copy)
	var $RecPerRow = 0;
	var $MultiColumnClass;
	var $MultiColumnEditClass = "col-sm-12";
	var $MultiColumnCnt = 12;
	var $MultiColumnEditCnt = 12;
	var $GridCnt = 0;
	var $ColCnt = 0;
	var $DbMasterFilter = ""; // Master filter
	var $DbDetailFilter = ""; // Detail filter
	var $MasterRecordExists;	
	var $MultiSelectKey;
	var $Command;
	var $RestoreSearch = FALSE;
	var $DetailPages;
	var $Recordset;
	var $OldRecordset;

	//
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError, $gsSearchError, $Security;

		// Search filters
		$sSrchAdvanced = ""; // Advanced search filter
		$sSrchBasic = ""; // Basic search filter
		$sFilter = "";

		// Get command
		$this->Command = strtolower(@$_GET["cmd"]);
		if ($this->IsPageRequest()) { // Validate request

			// Process list action first
			if ($this->ProcessListAction()) // Ajax request
				$this->Page_Terminate();

			// Handle reset command
			$this->ResetCmd();

			// Set up Breadcrumb
			if ($this->Export == "")
				$this->SetupBreadcrumb();

			// Check QueryString parameters
			if (@$_GET["a"] <> "") {
				$this->CurrentAction = $_GET["a"];

				// Clear inline mode
				if ($this->CurrentAction == "cancel")
					$this->ClearInlineMode();

				// Switch to grid edit mode
				if ($this->CurrentAction == "gridedit")
					$this->GridEditMode();

				// Switch to grid add mode
				if ($this->CurrentAction == "gridadd")
					$this->GridAddMode();
			} else {
				if (@$_POST["a_list"] <> "") {
					$this->CurrentAction = $_POST["a_list"]; // Get action

					// Grid Update
					if (($this->CurrentAction == "gridupdate" || $this->CurrentAction == "gridoverwrite") && @$_SESSION[EW_SESSION_INLINE_MODE] == "gridedit") {
						if ($this->ValidateGridForm()) {
							$bGridUpdate = $this->GridUpdate();
						} else {
							$bGridUpdate = FALSE;
							$this->setFailureMessage($gsFormError);
						}
						if (!$bGridUpdate) {
							$this->EventCancelled = TRUE;
							$this->CurrentAction = "gridedit"; // Stay in Grid Edit mode
						}
					}

					// Grid Insert
					if ($this->CurrentAction == "gridinsert" && @$_SESSION[EW_SESSION_INLINE_MODE] == "gridadd") {
						if ($this->ValidateGridForm()) {
							$bGridInsert = $this->GridInsert();
						} else {
							$bGridInsert = FALSE;
							$this->setFailureMessage($gsFormError);
						}
						if (!$bGridInsert) {
							$this->EventCancelled = TRUE;
							$this->CurrentAction = "gridadd"; // Stay in Grid Add mode
						}
					}
				}
			}

			// Hide list options
			if ($this->Export <> "") {
				$this->ListOptions->HideAllOptions(array("sequence"));
				$this->ListOptions->UseDropDownButton = FALSE; // Disable drop down button
				$this->ListOptions->UseButtonGroup = FALSE; // Disable button group
			} elseif ($this->CurrentAction == "gridadd" || $this->CurrentAction == "gridedit") {
				$this->ListOptions->HideAllOptions();
				$this->ListOptions->UseDropDownButton = FALSE; // Disable drop down button
				$this->ListOptions->UseButtonGroup = FALSE; // Disable button group
			}

			// Hide options
			if ($this->Export <> "" || $this->CurrentAction <> "") {
				$this->ExportOptions->HideAllOptions();
				$this->FilterOptions->HideAllOptions();
			}

			// Hide other options
			if ($this->Export <> "") {
				foreach ($this->OtherOptions as &$option)
					$option->HideAllOptions();
			}

			// Show grid delete link for grid add / grid edit
			if ($this->AllowAddDeleteRow) {
				if ($this->CurrentAction == "gridadd" || $this->CurrentAction == "gridedit") {
					$item = $this->ListOptions->GetItem("griddelete");
					if ($item) $item->Visible = TRUE;
				}
			}

			// Get default search criteria
			ew_AddFilter($this->DefaultSearchWhere, $this->BasicSearchWhere(TRUE));

			// Get basic search values
			$this->LoadBasicSearchValues();

			// Restore filter list
			$this->RestoreFilterList();

			// Restore search parms from Session if not searching / reset / export
			if (($this->Export <> "" || $this->Command <> "search" && $this->Command <> "reset" && $this->Command <> "resetall") && $this->CheckSearchParms())
				$this->RestoreSearchParms();

			// Call Recordset SearchValidated event
			$this->Recordset_SearchValidated();

			// Set up sorting order
			$this->SetUpSortOrder();

			// Get basic search criteria
			if ($gsSearchError == "")
				$sSrchBasic = $this->BasicSearchWhere();
		}

		// Restore display records
		if ($this->getRecordsPerPage() <> "") {
			$this->DisplayRecs = $this->getRecordsPerPage(); // Restore from Session
		} else {
			$this->DisplayRecs = 20; // Load default
		}

		// Load Sorting Order
		$this->LoadSortOrder();

		// Load search default if no existing search criteria
		if (!$this->CheckSearchParms()) {

			// Load basic search from default
			$this->BasicSearch->LoadDefault();
			if ($this->BasicSearch->Keyword != "")
				$sSrchBasic = $this->BasicSearchWhere();
		}

		// Build search criteria
		ew_AddFilter($this->SearchWhere, $sSrchAdvanced);
		ew_AddFilter($this->SearchWhere, $sSrchBasic);

		// Call Recordset_Searching event
		$this->Recordset_Searching($this->SearchWhere);

		// Save search criteria
		if ($this->Command == "search" && !$this->RestoreSearch) {
			$this->setSearchWhere($this->SearchWhere); // Save to Session
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} else {
			$this->SearchWhere = $this->getSearchWhere();
		}

		// Build filter
		$sFilter = "";
		if (!$Security->CanList())
			$sFilter = "(0=1)"; // Filter all records
		ew_AddFilter($sFilter, $this->DbDetailFilter);
		ew_AddFilter($sFilter, $this->SearchWhere);

		// Set up filter in session
		$this->setSessionWhere($sFilter);
		$this->CurrentFilter = "";

		// Load record count first
		if (!$this->IsAddOrEdit()) {
			$bSelectLimit = $this->UseSelectLimit;
			if ($bSelectLimit) {
				$this->TotalRecs = $this->SelectRecordCount();
			} else {
				if ($this->Recordset = $this->LoadRecordset())
					$this->TotalRecs = $this->Recordset->RecordCount();
			}
		}

		// Search options
		$this->SetupSearchOptions();
	}

	//  Exit inline mode
	function ClearInlineMode() {
		$this->OvePIni->FormValue = ""; // Clear form value
		$this->LastAction = $this->CurrentAction; // Save last action
		$this->CurrentAction = ""; // Clear action
		$_SESSION[EW_SESSION_INLINE_MODE] = ""; // Clear inline mode
	}

	// Switch to Grid Add mode
	function GridAddMode() {
		$_SESSION[EW_SESSION_INLINE_MODE] = "gridadd"; // Enabled grid add
	}

	// Switch to Grid Edit mode
	function GridEditMode() {
		$_SESSION[EW_SESSION_INLINE_MODE] = "gridedit"; // Enable grid edit
	}

	// Perform update to grid
	function GridUpdate() {
		global $Language, $objForm, $gsFormError;
		$bGridUpdate = TRUE;

		// Get old recordset
		$this->CurrentFilter = $this->BuildKeyFilter();
		if ($this->CurrentFilter == "")
			$this->CurrentFilter = "0=1";
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		if ($rs = $conn->Execute($sSql)) {
			$rsold = $rs->GetRows();
			$rs->Close();
		}

		// Call Grid Updating event
		if (!$this->Grid_Updating($rsold)) {
			if ($this->getFailureMessage() == "")
				$this->setFailureMessage($Language->Phrase("GridEditCancelled")); // Set grid edit cancelled message
			return FALSE;
		}

		// Begin transaction
		$conn->BeginTrans();
		$sKey = "";

		// Update row index and get row key
		$objForm->Index = -1;
		$rowcnt = strval($objForm->GetValue($this->FormKeyCountName));
		if ($rowcnt == "" || !is_numeric($rowcnt))
			$rowcnt = 0;

		// Update all rows based on key
		for ($rowindex = 1; $rowindex <= $rowcnt; $rowindex++) {
			$objForm->Index = $rowindex;
			$rowkey = strval($objForm->GetValue($this->FormKeyName));
			$rowaction = strval($objForm->GetValue($this->FormActionName));

			// Load all values and keys
			if ($rowaction <> "insertdelete") { // Skip insert then deleted rows
				$this->LoadFormValues(); // Get form values
				if ($rowaction == "" || $rowaction == "edit" || $rowaction == "delete") {
					$bGridUpdate = $this->SetupKeyValues($rowkey); // Set up key values
				} else {
					$bGridUpdate = TRUE;
				}

				// Skip empty row
				if ($rowaction == "insert" && $this->EmptyRow()) {

					// No action required
				// Validate form and insert/update/delete record

				} elseif ($bGridUpdate) {
					if ($rowaction == "delete") {
						$this->CurrentFilter = $this->KeyFilter();
						$bGridUpdate = $this->DeleteRows(); // Delete this row
					} else if (!$this->ValidateForm()) {
						$bGridUpdate = FALSE; // Form error, reset action
						$this->setFailureMessage($gsFormError);
					} else {
						if ($rowaction == "insert") {
							$bGridUpdate = $this->AddRow(); // Insert this row
						} else {
							if ($rowkey <> "") {
								$this->SendEmail = FALSE; // Do not send email on update success
								$bGridUpdate = $this->EditRow(); // Update this row
							}
						} // End update
					}
				}
				if ($bGridUpdate) {
					if ($sKey <> "") $sKey .= ", ";
					$sKey .= $rowkey;
				} else {
					break;
				}
			}
		}
		if ($bGridUpdate) {
			$conn->CommitTrans(); // Commit transaction

			// Get new recordset
			if ($rs = $conn->Execute($sSql)) {
				$rsnew = $rs->GetRows();
				$rs->Close();
			}

			// Call Grid_Updated event
			$this->Grid_Updated($rsold, $rsnew);
			if ($this->getSuccessMessage() == "")
				$this->setSuccessMessage($Language->Phrase("UpdateSuccess")); // Set up update success message
			$this->ClearInlineMode(); // Clear inline edit mode
		} else {
			$conn->RollbackTrans(); // Rollback transaction
			if ($this->getFailureMessage() == "")
				$this->setFailureMessage($Language->Phrase("UpdateFailed")); // Set update failed message
		}
		return $bGridUpdate;
	}

	// Build filter for all keys
	function BuildKeyFilter() {
		global $objForm;
		$sWrkFilter = "";

		// Update row index and get row key
		$rowindex = 1;
		$objForm->Index = $rowindex;
		$sThisKey = strval($objForm->GetValue($this->FormKeyName));
		while ($sThisKey <> "") {
			if ($this->SetupKeyValues($sThisKey)) {
				$sFilter = $this->KeyFilter();
				if ($sWrkFilter <> "") $sWrkFilter .= " OR ";
				$sWrkFilter .= $sFilter;
			} else {
				$sWrkFilter = "0=1";
				break;
			}

			// Update row index and get row key
			$rowindex++; // Next row
			$objForm->Index = $rowindex;
			$sThisKey = strval($objForm->GetValue($this->FormKeyName));
		}
		return $sWrkFilter;
	}

	// Set up key values
	function SetupKeyValues($key) {
		$arrKeyFlds = explode($GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"], $key);
		if (count($arrKeyFlds) >= 1) {
			$this->OveCodi->setFormValue($arrKeyFlds[0]);
			if (!is_numeric($this->OveCodi->FormValue))
				return FALSE;
		}
		return TRUE;
	}

	// Perform Grid Add
	function GridInsert() {
		global $Language, $objForm, $gsFormError;
		$rowindex = 1;
		$bGridInsert = FALSE;
		$conn = &$this->Connection();

		// Call Grid Inserting event
		if (!$this->Grid_Inserting()) {
			if ($this->getFailureMessage() == "") {
				$this->setFailureMessage($Language->Phrase("GridAddCancelled")); // Set grid add cancelled message
			}
			return FALSE;
		}

		// Begin transaction
		$conn->BeginTrans();

		// Init key filter
		$sWrkFilter = "";
		$addcnt = 0;
		$sKey = "";

		// Get row count
		$objForm->Index = -1;
		$rowcnt = strval($objForm->GetValue($this->FormKeyCountName));
		if ($rowcnt == "" || !is_numeric($rowcnt))
			$rowcnt = 0;

		// Insert all rows
		for ($rowindex = 1; $rowindex <= $rowcnt; $rowindex++) {

			// Load current row values
			$objForm->Index = $rowindex;
			$rowaction = strval($objForm->GetValue($this->FormActionName));
			if ($rowaction <> "" && $rowaction <> "insert")
				continue; // Skip
			$this->LoadFormValues(); // Get form values
			if (!$this->EmptyRow()) {
				$addcnt++;
				$this->SendEmail = FALSE; // Do not send email on insert success

				// Validate form
				if (!$this->ValidateForm()) {
					$bGridInsert = FALSE; // Form error, reset action
					$this->setFailureMessage($gsFormError);
				} else {
					$bGridInsert = $this->AddRow($this->OldRecordset); // Insert this row
				}
				if ($bGridInsert) {
					if ($sKey <> "") $sKey .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
					$sKey .= $this->OveCodi->CurrentValue;

					// Add filter for this record
					$sFilter = $this->KeyFilter();
					if ($sWrkFilter <> "") $sWrkFilter .= " OR ";
					$sWrkFilter .= $sFilter;
				} else {
					break;
				}
			}
		}
		if ($addcnt == 0) { // No record inserted
			$this->setFailureMessage($Language->Phrase("NoAddRecord"));
			$bGridInsert = FALSE;
		}
		if ($bGridInsert) {
			$conn->CommitTrans(); // Commit transaction

			// Get new recordset
			$this->CurrentFilter = $sWrkFilter;
			$sSql = $this->SQL();
			if ($rs = $conn->Execute($sSql)) {
				$rsnew = $rs->GetRows();
				$rs->Close();
			}

			// Call Grid_Inserted event
			$this->Grid_Inserted($rsnew);
			if ($this->getSuccessMessage() == "")
				$this->setSuccessMessage($Language->Phrase("InsertSuccess")); // Set up insert success message
			$this->ClearInlineMode(); // Clear grid add mode
		} else {
			$conn->RollbackTrans(); // Rollback transaction
			if ($this->getFailureMessage() == "") {
				$this->setFailureMessage($Language->Phrase("InsertFailed")); // Set insert failed message
			}
		}
		return $bGridInsert;
	}

	// Check if empty row
	function EmptyRow() {
		global $objForm;
		if ($objForm->HasValue("x_ProCodi") && $objForm->HasValue("o_ProCodi") && $this->ProCodi->CurrentValue <> $this->ProCodi->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_OvePIni") && $objForm->HasValue("o_OvePIni") && $this->OvePIni->CurrentValue <> $this->OvePIni->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_OveCPro") && $objForm->HasValue("o_OveCPro") && $this->OveCPro->CurrentValue <> $this->OveCPro->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_OveUsua") && $objForm->HasValue("o_OveUsua") && $this->OveUsua->CurrentValue <> $this->OveUsua->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_OveFCre") && $objForm->HasValue("o_OveFCre") && $this->OveFCre->CurrentValue <> $this->OveFCre->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_OveCVen") && $objForm->HasValue("o_OveCVen") && $this->OveCVen->CurrentValue <> $this->OveCVen->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_CliCodi") && $objForm->HasValue("o_CliCodi") && $this->CliCodi->CurrentValue <> $this->CliCodi->OldValue)
			return FALSE;
		return TRUE;
	}

	// Validate grid form
	function ValidateGridForm() {
		global $objForm;

		// Get row count
		$objForm->Index = -1;
		$rowcnt = strval($objForm->GetValue($this->FormKeyCountName));
		if ($rowcnt == "" || !is_numeric($rowcnt))
			$rowcnt = 0;

		// Validate all records
		for ($rowindex = 1; $rowindex <= $rowcnt; $rowindex++) {

			// Load current row values
			$objForm->Index = $rowindex;
			$rowaction = strval($objForm->GetValue($this->FormActionName));
			if ($rowaction <> "delete" && $rowaction <> "insertdelete") {
				$this->LoadFormValues(); // Get form values
				if ($rowaction == "insert" && $this->EmptyRow()) {

					// Ignore
				} else if (!$this->ValidateForm()) {
					return FALSE;
				}
			}
		}
		return TRUE;
	}

	// Get all form values of the grid
	function GetGridFormValues() {
		global $objForm;

		// Get row count
		$objForm->Index = -1;
		$rowcnt = strval($objForm->GetValue($this->FormKeyCountName));
		if ($rowcnt == "" || !is_numeric($rowcnt))
			$rowcnt = 0;
		$rows = array();

		// Loop through all records
		for ($rowindex = 1; $rowindex <= $rowcnt; $rowindex++) {

			// Load current row values
			$objForm->Index = $rowindex;
			$rowaction = strval($objForm->GetValue($this->FormActionName));
			if ($rowaction <> "delete" && $rowaction <> "insertdelete") {
				$this->LoadFormValues(); // Get form values
				if ($rowaction == "insert" && $this->EmptyRow()) {

					// Ignore
				} else {
					$rows[] = $this->GetFieldValues("FormValue"); // Return row as array
				}
			}
		}
		return $rows; // Return as array of array
	}

	// Restore form values for current row
	function RestoreCurrentRowFormValues($idx) {
		global $objForm;

		// Get row based on current index
		$objForm->Index = $idx;
		$this->LoadFormValues(); // Load form values
	}

	// Get list of filters
	function GetFilterList() {

		// Initialize
		$sFilterList = "";
		$sFilterList = ew_Concat($sFilterList, $this->OveCodi->AdvancedSearch->ToJSON(), ","); // Field OveCodi
		$sFilterList = ew_Concat($sFilterList, $this->ProCodi->AdvancedSearch->ToJSON(), ","); // Field ProCodi
		$sFilterList = ew_Concat($sFilterList, $this->OvePIni->AdvancedSearch->ToJSON(), ","); // Field OvePIni
		$sFilterList = ew_Concat($sFilterList, $this->OveCPro->AdvancedSearch->ToJSON(), ","); // Field OveCPro
		$sFilterList = ew_Concat($sFilterList, $this->OveUsua->AdvancedSearch->ToJSON(), ","); // Field OveUsua
		$sFilterList = ew_Concat($sFilterList, $this->OveFCre->AdvancedSearch->ToJSON(), ","); // Field OveFCre
		$sFilterList = ew_Concat($sFilterList, $this->OveCVen->AdvancedSearch->ToJSON(), ","); // Field OveCVen
		$sFilterList = ew_Concat($sFilterList, $this->CliCodi->AdvancedSearch->ToJSON(), ","); // Field CliCodi
		if ($this->BasicSearch->Keyword <> "") {
			$sWrk = "\"" . EW_TABLE_BASIC_SEARCH . "\":\"" . ew_JsEncode2($this->BasicSearch->Keyword) . "\",\"" . EW_TABLE_BASIC_SEARCH_TYPE . "\":\"" . ew_JsEncode2($this->BasicSearch->Type) . "\"";
			$sFilterList = ew_Concat($sFilterList, $sWrk, ",");
		}

		// Return filter list in json
		return ($sFilterList <> "") ? "{" . $sFilterList . "}" : "null";
	}

	// Restore list of filters
	function RestoreFilterList() {

		// Return if not reset filter
		if (@$_POST["cmd"] <> "resetfilter")
			return FALSE;
		$filter = json_decode(ew_StripSlashes(@$_POST["filter"]), TRUE);
		$this->Command = "search";

		// Field OveCodi
		$this->OveCodi->AdvancedSearch->SearchValue = @$filter["x_OveCodi"];
		$this->OveCodi->AdvancedSearch->SearchOperator = @$filter["z_OveCodi"];
		$this->OveCodi->AdvancedSearch->SearchCondition = @$filter["v_OveCodi"];
		$this->OveCodi->AdvancedSearch->SearchValue2 = @$filter["y_OveCodi"];
		$this->OveCodi->AdvancedSearch->SearchOperator2 = @$filter["w_OveCodi"];
		$this->OveCodi->AdvancedSearch->Save();

		// Field ProCodi
		$this->ProCodi->AdvancedSearch->SearchValue = @$filter["x_ProCodi"];
		$this->ProCodi->AdvancedSearch->SearchOperator = @$filter["z_ProCodi"];
		$this->ProCodi->AdvancedSearch->SearchCondition = @$filter["v_ProCodi"];
		$this->ProCodi->AdvancedSearch->SearchValue2 = @$filter["y_ProCodi"];
		$this->ProCodi->AdvancedSearch->SearchOperator2 = @$filter["w_ProCodi"];
		$this->ProCodi->AdvancedSearch->Save();

		// Field OvePIni
		$this->OvePIni->AdvancedSearch->SearchValue = @$filter["x_OvePIni"];
		$this->OvePIni->AdvancedSearch->SearchOperator = @$filter["z_OvePIni"];
		$this->OvePIni->AdvancedSearch->SearchCondition = @$filter["v_OvePIni"];
		$this->OvePIni->AdvancedSearch->SearchValue2 = @$filter["y_OvePIni"];
		$this->OvePIni->AdvancedSearch->SearchOperator2 = @$filter["w_OvePIni"];
		$this->OvePIni->AdvancedSearch->Save();

		// Field OveCPro
		$this->OveCPro->AdvancedSearch->SearchValue = @$filter["x_OveCPro"];
		$this->OveCPro->AdvancedSearch->SearchOperator = @$filter["z_OveCPro"];
		$this->OveCPro->AdvancedSearch->SearchCondition = @$filter["v_OveCPro"];
		$this->OveCPro->AdvancedSearch->SearchValue2 = @$filter["y_OveCPro"];
		$this->OveCPro->AdvancedSearch->SearchOperator2 = @$filter["w_OveCPro"];
		$this->OveCPro->AdvancedSearch->Save();

		// Field OveUsua
		$this->OveUsua->AdvancedSearch->SearchValue = @$filter["x_OveUsua"];
		$this->OveUsua->AdvancedSearch->SearchOperator = @$filter["z_OveUsua"];
		$this->OveUsua->AdvancedSearch->SearchCondition = @$filter["v_OveUsua"];
		$this->OveUsua->AdvancedSearch->SearchValue2 = @$filter["y_OveUsua"];
		$this->OveUsua->AdvancedSearch->SearchOperator2 = @$filter["w_OveUsua"];
		$this->OveUsua->AdvancedSearch->Save();

		// Field OveFCre
		$this->OveFCre->AdvancedSearch->SearchValue = @$filter["x_OveFCre"];
		$this->OveFCre->AdvancedSearch->SearchOperator = @$filter["z_OveFCre"];
		$this->OveFCre->AdvancedSearch->SearchCondition = @$filter["v_OveFCre"];
		$this->OveFCre->AdvancedSearch->SearchValue2 = @$filter["y_OveFCre"];
		$this->OveFCre->AdvancedSearch->SearchOperator2 = @$filter["w_OveFCre"];
		$this->OveFCre->AdvancedSearch->Save();

		// Field OveCVen
		$this->OveCVen->AdvancedSearch->SearchValue = @$filter["x_OveCVen"];
		$this->OveCVen->AdvancedSearch->SearchOperator = @$filter["z_OveCVen"];
		$this->OveCVen->AdvancedSearch->SearchCondition = @$filter["v_OveCVen"];
		$this->OveCVen->AdvancedSearch->SearchValue2 = @$filter["y_OveCVen"];
		$this->OveCVen->AdvancedSearch->SearchOperator2 = @$filter["w_OveCVen"];
		$this->OveCVen->AdvancedSearch->Save();

		// Field CliCodi
		$this->CliCodi->AdvancedSearch->SearchValue = @$filter["x_CliCodi"];
		$this->CliCodi->AdvancedSearch->SearchOperator = @$filter["z_CliCodi"];
		$this->CliCodi->AdvancedSearch->SearchCondition = @$filter["v_CliCodi"];
		$this->CliCodi->AdvancedSearch->SearchValue2 = @$filter["y_CliCodi"];
		$this->CliCodi->AdvancedSearch->SearchOperator2 = @$filter["w_CliCodi"];
		$this->CliCodi->AdvancedSearch->Save();
		$this->BasicSearch->setKeyword(@$filter[EW_TABLE_BASIC_SEARCH]);
		$this->BasicSearch->setType(@$filter[EW_TABLE_BASIC_SEARCH_TYPE]);
	}

	// Return basic search SQL
	function BasicSearchSQL($arKeywords, $type) {
		$sWhere = "";
		$this->BuildBasicSearchSQL($sWhere, $this->OveUsua, $arKeywords, $type);
		return $sWhere;
	}

	// Build basic search SQL
	function BuildBasicSearchSql(&$Where, &$Fld, $arKeywords, $type) {
		$sDefCond = ($type == "OR") ? "OR" : "AND";
		$arSQL = array(); // Array for SQL parts
		$arCond = array(); // Array for search conditions
		$cnt = count($arKeywords);
		$j = 0; // Number of SQL parts
		for ($i = 0; $i < $cnt; $i++) {
			$Keyword = $arKeywords[$i];
			$Keyword = trim($Keyword);
			if (EW_BASIC_SEARCH_IGNORE_PATTERN <> "") {
				$Keyword = preg_replace(EW_BASIC_SEARCH_IGNORE_PATTERN, "\\", $Keyword);
				$ar = explode("\\", $Keyword);
			} else {
				$ar = array($Keyword);
			}
			foreach ($ar as $Keyword) {
				if ($Keyword <> "") {
					$sWrk = "";
					if ($Keyword == "OR" && $type == "") {
						if ($j > 0)
							$arCond[$j-1] = "OR";
					} elseif ($Keyword == EW_NULL_VALUE) {
						$sWrk = $Fld->FldExpression . " IS NULL";
					} elseif ($Keyword == EW_NOT_NULL_VALUE) {
						$sWrk = $Fld->FldExpression . " IS NOT NULL";
					} elseif ($Fld->FldIsVirtual && $Fld->FldVirtualSearch) {
						$sWrk = $Fld->FldVirtualExpression . ew_Like(ew_QuotedValue("%" . $Keyword . "%", EW_DATATYPE_STRING, $this->DBID), $this->DBID);
					} elseif ($Fld->FldDataType != EW_DATATYPE_NUMBER || is_numeric($Keyword)) {
						$sWrk = $Fld->FldBasicSearchExpression . ew_Like(ew_QuotedValue("%" . $Keyword . "%", EW_DATATYPE_STRING, $this->DBID), $this->DBID);
					}
					if ($sWrk <> "") {
						$arSQL[$j] = $sWrk;
						$arCond[$j] = $sDefCond;
						$j += 1;
					}
				}
			}
		}
		$cnt = count($arSQL);
		$bQuoted = FALSE;
		$sSql = "";
		if ($cnt > 0) {
			for ($i = 0; $i < $cnt-1; $i++) {
				if ($arCond[$i] == "OR") {
					if (!$bQuoted) $sSql .= "(";
					$bQuoted = TRUE;
				}
				$sSql .= $arSQL[$i];
				if ($bQuoted && $arCond[$i] <> "OR") {
					$sSql .= ")";
					$bQuoted = FALSE;
				}
				$sSql .= " " . $arCond[$i] . " ";
			}
			$sSql .= $arSQL[$cnt-1];
			if ($bQuoted)
				$sSql .= ")";
		}
		if ($sSql <> "") {
			if ($Where <> "") $Where .= " OR ";
			$Where .=  "(" . $sSql . ")";
		}
	}

	// Return basic search WHERE clause based on search keyword and type
	function BasicSearchWhere($Default = FALSE) {
		global $Security;
		$sSearchStr = "";
		if (!$Security->CanSearch()) return "";
		$sSearchKeyword = ($Default) ? $this->BasicSearch->KeywordDefault : $this->BasicSearch->Keyword;
		$sSearchType = ($Default) ? $this->BasicSearch->TypeDefault : $this->BasicSearch->Type;
		if ($sSearchKeyword <> "") {
			$sSearch = trim($sSearchKeyword);
			if ($sSearchType <> "=") {
				$ar = array();

				// Match quoted keywords (i.e.: "...")
				if (preg_match_all('/"([^"]*)"/i', $sSearch, $matches, PREG_SET_ORDER)) {
					foreach ($matches as $match) {
						$p = strpos($sSearch, $match[0]);
						$str = substr($sSearch, 0, $p);
						$sSearch = substr($sSearch, $p + strlen($match[0]));
						if (strlen(trim($str)) > 0)
							$ar = array_merge($ar, explode(" ", trim($str)));
						$ar[] = $match[1]; // Save quoted keyword
					}
				}

				// Match individual keywords
				if (strlen(trim($sSearch)) > 0)
					$ar = array_merge($ar, explode(" ", trim($sSearch)));

				// Search keyword in any fields
				if (($sSearchType == "OR" || $sSearchType == "AND") && $this->BasicSearch->BasicSearchAnyFields) {
					foreach ($ar as $sKeyword) {
						if ($sKeyword <> "") {
							if ($sSearchStr <> "") $sSearchStr .= " " . $sSearchType . " ";
							$sSearchStr .= "(" . $this->BasicSearchSQL(array($sKeyword), $sSearchType) . ")";
						}
					}
				} else {
					$sSearchStr = $this->BasicSearchSQL($ar, $sSearchType);
				}
			} else {
				$sSearchStr = $this->BasicSearchSQL(array($sSearch), $sSearchType);
			}
			if (!$Default) $this->Command = "search";
		}
		if (!$Default && $this->Command == "search") {
			$this->BasicSearch->setKeyword($sSearchKeyword);
			$this->BasicSearch->setType($sSearchType);
		}
		return $sSearchStr;
	}

	// Check if search parm exists
	function CheckSearchParms() {

		// Check basic search
		if ($this->BasicSearch->IssetSession())
			return TRUE;
		return FALSE;
	}

	// Clear all search parameters
	function ResetSearchParms() {

		// Clear search WHERE clause
		$this->SearchWhere = "";
		$this->setSearchWhere($this->SearchWhere);

		// Clear basic search parameters
		$this->ResetBasicSearchParms();
	}

	// Load advanced search default values
	function LoadAdvancedSearchDefault() {
		return FALSE;
	}

	// Clear all basic search parameters
	function ResetBasicSearchParms() {
		$this->BasicSearch->UnsetSession();
	}

	// Restore all search parameters
	function RestoreSearchParms() {
		$this->RestoreSearch = TRUE;

		// Restore basic search values
		$this->BasicSearch->Load();
	}

	// Set up sort parameters
	function SetUpSortOrder() {

		// Check for Ctrl pressed
		$bCtrl = (@$_GET["ctrl"] <> "");

		// Check for "order" parameter
		if (@$_GET["order"] <> "") {
			$this->CurrentOrder = ew_StripSlashes(@$_GET["order"]);
			$this->CurrentOrderType = @$_GET["ordertype"];
			$this->UpdateSort($this->OveCodi, $bCtrl); // OveCodi
			$this->UpdateSort($this->ProCodi, $bCtrl); // ProCodi
			$this->UpdateSort($this->OvePIni, $bCtrl); // OvePIni
			$this->UpdateSort($this->OveCPro, $bCtrl); // OveCPro
			$this->UpdateSort($this->OveUsua, $bCtrl); // OveUsua
			$this->UpdateSort($this->OveFCre, $bCtrl); // OveFCre
			$this->UpdateSort($this->OveCVen, $bCtrl); // OveCVen
			$this->UpdateSort($this->CliCodi, $bCtrl); // CliCodi
			$this->setStartRecordNumber(1); // Reset start position
		}
	}

	// Load sort order parameters
	function LoadSortOrder() {
		$sOrderBy = $this->getSessionOrderBy(); // Get ORDER BY from Session
		if ($sOrderBy == "") {
			if ($this->getSqlOrderBy() <> "") {
				$sOrderBy = $this->getSqlOrderBy();
				$this->setSessionOrderBy($sOrderBy);
			}
		}
	}

	// Reset command
	// - cmd=reset (Reset search parameters)
	// - cmd=resetall (Reset search and master/detail parameters)
	// - cmd=resetsort (Reset sort parameters)
	function ResetCmd() {

		// Check if reset command
		if (substr($this->Command,0,5) == "reset") {

			// Reset search criteria
			if ($this->Command == "reset" || $this->Command == "resetall")
				$this->ResetSearchParms();

			// Reset sorting order
			if ($this->Command == "resetsort") {
				$sOrderBy = "";
				$this->setSessionOrderBy($sOrderBy);
				$this->OveCodi->setSort("");
				$this->ProCodi->setSort("");
				$this->OvePIni->setSort("");
				$this->OveCPro->setSort("");
				$this->OveUsua->setSort("");
				$this->OveFCre->setSort("");
				$this->OveCVen->setSort("");
				$this->CliCodi->setSort("");
			}

			// Reset start position
			$this->StartRec = 1;
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Set up list options
	function SetupListOptions() {
		global $Security, $Language;

		// "griddelete"
		if ($this->AllowAddDeleteRow) {
			$item = &$this->ListOptions->Add("griddelete");
			$item->CssStyle = "white-space: nowrap;";
			$item->OnLeft = FALSE;
			$item->Visible = FALSE; // Default hidden
		}

		// Add group option item
		$item = &$this->ListOptions->Add($this->ListOptions->GroupOptionName);
		$item->Body = "";
		$item->OnLeft = FALSE;
		$item->Visible = FALSE;

		// "view"
		$item = &$this->ListOptions->Add("view");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->CanView();
		$item->OnLeft = FALSE;

		// "edit"
		$item = &$this->ListOptions->Add("edit");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->CanEdit();
		$item->OnLeft = FALSE;

		// "copy"
		$item = &$this->ListOptions->Add("copy");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->CanAdd();
		$item->OnLeft = FALSE;

		// "delete"
		$item = &$this->ListOptions->Add("delete");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->CanDelete();
		$item->OnLeft = FALSE;

		// List actions
		$item = &$this->ListOptions->Add("listactions");
		$item->CssStyle = "white-space: nowrap;";
		$item->OnLeft = FALSE;
		$item->Visible = FALSE;
		$item->ShowInButtonGroup = FALSE;
		$item->ShowInDropDown = FALSE;

		// "checkbox"
		$item = &$this->ListOptions->Add("checkbox");
		$item->Visible = FALSE;
		$item->OnLeft = FALSE;
		$item->Header = "<input type=\"checkbox\" name=\"key\" id=\"key\" onclick=\"ew_SelectAllKey(this);\">";
		$item->ShowInDropDown = FALSE;
		$item->ShowInButtonGroup = FALSE;

		// Drop down button for ListOptions
		$this->ListOptions->UseImageAndText = TRUE;
		$this->ListOptions->UseDropDownButton = FALSE;
		$this->ListOptions->DropDownButtonPhrase = $Language->Phrase("ButtonListOptions");
		$this->ListOptions->UseButtonGroup = FALSE;
		if ($this->ListOptions->UseButtonGroup && ew_IsMobile())
			$this->ListOptions->UseDropDownButton = TRUE;
		$this->ListOptions->ButtonClass = "btn-sm"; // Class for button group

		// Call ListOptions_Load event
		$this->ListOptions_Load();
		$this->SetupListOptionsExt();
		$item = &$this->ListOptions->GetItem($this->ListOptions->GroupOptionName);
		$item->Visible = $this->ListOptions->GroupOptionVisible();
	}

	// Render list options
	function RenderListOptions() {
		global $Security, $Language, $objForm;
		$this->ListOptions->LoadDefault();

		// Set up row action and key
		if (is_numeric($this->RowIndex) && $this->CurrentMode <> "view") {
			$objForm->Index = $this->RowIndex;
			$ActionName = str_replace("k_", "k" . $this->RowIndex . "_", $this->FormActionName);
			$OldKeyName = str_replace("k_", "k" . $this->RowIndex . "_", $this->FormOldKeyName);
			$KeyName = str_replace("k_", "k" . $this->RowIndex . "_", $this->FormKeyName);
			$BlankRowName = str_replace("k_", "k" . $this->RowIndex . "_", $this->FormBlankRowName);
			if ($this->RowAction <> "")
				$this->MultiSelectKey .= "<input type=\"hidden\" name=\"" . $ActionName . "\" id=\"" . $ActionName . "\" value=\"" . $this->RowAction . "\">";
			if ($this->RowAction == "delete") {
				$rowkey = $objForm->GetValue($this->FormKeyName);
				$this->SetupKeyValues($rowkey);
			}
			if ($this->RowAction == "insert" && $this->CurrentAction == "F" && $this->EmptyRow())
				$this->MultiSelectKey .= "<input type=\"hidden\" name=\"" . $BlankRowName . "\" id=\"" . $BlankRowName . "\" value=\"1\">";
		}

		// "delete"
		if ($this->AllowAddDeleteRow) {
			if ($this->CurrentAction == "gridadd" || $this->CurrentAction == "gridedit") {
				$option = &$this->ListOptions;
				$option->UseButtonGroup = TRUE; // Use button group for grid delete button
				$option->UseImageAndText = TRUE; // Use image and text for grid delete button
				$oListOpt = &$option->Items["griddelete"];
				if (!$Security->CanDelete() && is_numeric($this->RowIndex) && ($this->RowAction == "" || $this->RowAction == "edit")) { // Do not allow delete existing record
					$oListOpt->Body = "&nbsp;";
				} else {
					$oListOpt->Body = "<a class=\"ewGridLink ewGridDelete\" title=\"" . ew_HtmlTitle($Language->Phrase("DeleteLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("DeleteLink")) . "\" onclick=\"return ew_DeleteGridRow(this, " . $this->RowIndex . ");\">" . $Language->Phrase("DeleteLink") . "</a>";
				}
			}
		}

		// "view"
		$oListOpt = &$this->ListOptions->Items["view"];
		if ($Security->CanView())
			$oListOpt->Body = "<a class=\"ewRowLink ewView\" title=\"" . ew_HtmlTitle($Language->Phrase("ViewLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("ViewLink")) . "\" href=\"" . ew_HtmlEncode($this->ViewUrl) . "\">" . $Language->Phrase("ViewLink") . "</a>";
		else
			$oListOpt->Body = "";

		// "edit"
		$oListOpt = &$this->ListOptions->Items["edit"];
		if ($Security->CanEdit()) {
			$oListOpt->Body = "<a class=\"ewRowLink ewEdit\" title=\"" . ew_HtmlTitle($Language->Phrase("EditLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("EditLink")) . "\" href=\"" . ew_HtmlEncode($this->EditUrl) . "\">" . $Language->Phrase("EditLink") . "</a>";
		} else {
			$oListOpt->Body = "";
		}

		// "copy"
		$oListOpt = &$this->ListOptions->Items["copy"];
		if ($Security->CanAdd()) {
			$oListOpt->Body = "<a class=\"ewRowLink ewCopy\" title=\"" . ew_HtmlTitle($Language->Phrase("CopyLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("CopyLink")) . "\" href=\"" . ew_HtmlEncode($this->CopyUrl) . "\">" . $Language->Phrase("CopyLink") . "</a>";
		} else {
			$oListOpt->Body = "";
		}

		// "delete"
		$oListOpt = &$this->ListOptions->Items["delete"];
		if ($Security->CanDelete())
			$oListOpt->Body = "<a class=\"ewRowLink ewDelete\"" . "" . " title=\"" . ew_HtmlTitle($Language->Phrase("DeleteLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("DeleteLink")) . "\" href=\"" . ew_HtmlEncode($this->DeleteUrl) . "\">" . $Language->Phrase("DeleteLink") . "</a>";
		else
			$oListOpt->Body = "";

		// Set up list action buttons
		$oListOpt = &$this->ListOptions->GetItem("listactions");
		if ($oListOpt) {
			$body = "";
			$links = array();
			foreach ($this->ListActions->Items as $listaction) {
				if ($listaction->Select == EW_ACTION_SINGLE && $listaction->Allow) {
					$action = $listaction->Action;
					$caption = $listaction->Caption;
					$icon = ($listaction->Icon <> "") ? "<span class=\"" . ew_HtmlEncode(str_replace(" ewIcon", "", $listaction->Icon)) . "\" data-caption=\"" . ew_HtmlTitle($caption) . "\"></span> " : "";
					$links[] = "<li><a class=\"ewAction ewListAction\" data-action=\"" . ew_HtmlEncode($action) . "\" data-caption=\"" . ew_HtmlTitle($caption) . "\" href=\"\" onclick=\"ew_SubmitAction(event,jQuery.extend({key:" . $this->KeyToJson() . "}," . $listaction->ToJson(TRUE) . "));return false;\">" . $icon . $listaction->Caption . "</a></li>";
					if (count($links) == 1) // Single button
						$body = "<a class=\"ewAction ewListAction\" data-action=\"" . ew_HtmlEncode($action) . "\" title=\"" . ew_HtmlTitle($caption) . "\" data-caption=\"" . ew_HtmlTitle($caption) . "\" href=\"\" onclick=\"ew_SubmitAction(event,jQuery.extend({key:" . $this->KeyToJson() . "}," . $listaction->ToJson(TRUE) . "));return false;\">" . $Language->Phrase("ListActionButton") . "</a>";
				}
			}
			if (count($links) > 1) { // More than one buttons, use dropdown
				$body = "<button class=\"dropdown-toggle btn btn-default btn-sm ewActions\" title=\"" . ew_HtmlTitle($Language->Phrase("ListActionButton")) . "\" data-toggle=\"dropdown\">" . $Language->Phrase("ListActionButton") . "<b class=\"caret\"></b></button>";
				$content = "";
				foreach ($links as $link)
					$content .= "<li>" . $link . "</li>";
				$body .= "<ul class=\"dropdown-menu" . ($oListOpt->OnLeft ? "" : " dropdown-menu-right") . "\">". $content . "</ul>";
				$body = "<div class=\"btn-group\">" . $body . "</div>";
			}
			if (count($links) > 0) {
				$oListOpt->Body = $body;
				$oListOpt->Visible = TRUE;
			}
		}

		// "checkbox"
		$oListOpt = &$this->ListOptions->Items["checkbox"];
		$oListOpt->Body = "<input type=\"checkbox\" name=\"key_m[]\" value=\"" . ew_HtmlEncode($this->OveCodi->CurrentValue) . "\" onclick='ew_ClickMultiCheckbox(event);'>";
		if ($this->CurrentAction == "gridedit" && is_numeric($this->RowIndex)) {
			$this->MultiSelectKey .= "<input type=\"hidden\" name=\"" . $KeyName . "\" id=\"" . $KeyName . "\" value=\"" . $this->OveCodi->CurrentValue . "\">";
		}
		$this->RenderListOptionsExt();

		// Call ListOptions_Rendered event
		$this->ListOptions_Rendered();
	}

	// Set up other options
	function SetupOtherOptions() {
		global $Language, $Security;
		$options = &$this->OtherOptions;
		$option = $options["addedit"];

		// Add
		$item = &$option->Add("add");
		$item->Body = "<a class=\"ewAddEdit ewAdd\" title=\"" . ew_HtmlTitle($Language->Phrase("AddLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("AddLink")) . "\" href=\"" . ew_HtmlEncode($this->AddUrl) . "\">" . $Language->Phrase("AddLink") . "</a>";
		$item->Visible = ($this->AddUrl <> "" && $Security->CanAdd());
		$item = &$option->Add("gridadd");
		$item->Body = "<a class=\"ewAddEdit ewGridAdd\" title=\"" . ew_HtmlTitle($Language->Phrase("GridAddLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("GridAddLink")) . "\" href=\"" . ew_HtmlEncode($this->GridAddUrl) . "\">" . $Language->Phrase("GridAddLink") . "</a>";
		$item->Visible = ($this->GridAddUrl <> "" && $Security->CanAdd());

		// Add grid edit
		$option = $options["addedit"];
		$item = &$option->Add("gridedit");
		$item->Body = "<a class=\"ewAddEdit ewGridEdit\" title=\"" . ew_HtmlTitle($Language->Phrase("GridEditLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("GridEditLink")) . "\" href=\"" . ew_HtmlEncode($this->GridEditUrl) . "\">" . $Language->Phrase("GridEditLink") . "</a>";
		$item->Visible = ($this->GridEditUrl <> "" && $Security->CanEdit());
		$option = $options["action"];

		// Set up options default
		foreach ($options as &$option) {
			$option->UseImageAndText = TRUE;
			$option->UseDropDownButton = TRUE;
			$option->UseButtonGroup = TRUE;
			$option->ButtonClass = "btn-sm"; // Class for button group
			$item = &$option->Add($option->GroupOptionName);
			$item->Body = "";
			$item->Visible = FALSE;
		}
		$options["addedit"]->DropDownButtonPhrase = $Language->Phrase("ButtonAddEdit");
		$options["detail"]->DropDownButtonPhrase = $Language->Phrase("ButtonDetails");
		$options["action"]->DropDownButtonPhrase = $Language->Phrase("ButtonActions");

		// Filter button
		$item = &$this->FilterOptions->Add("savecurrentfilter");
		$item->Body = "<a class=\"ewSaveFilter\" data-form=\"fOVenlistsrch\" href=\"#\">" . $Language->Phrase("SaveCurrentFilter") . "</a>";
		$item->Visible = TRUE;
		$item = &$this->FilterOptions->Add("deletefilter");
		$item->Body = "<a class=\"ewDeleteFilter\" data-form=\"fOVenlistsrch\" href=\"#\">" . $Language->Phrase("DeleteFilter") . "</a>";
		$item->Visible = TRUE;
		$this->FilterOptions->UseDropDownButton = TRUE;
		$this->FilterOptions->UseButtonGroup = !$this->FilterOptions->UseDropDownButton;
		$this->FilterOptions->DropDownButtonPhrase = $Language->Phrase("Filters");

		// Add group option item
		$item = &$this->FilterOptions->Add($this->FilterOptions->GroupOptionName);
		$item->Body = "";
		$item->Visible = FALSE;
	}

	// Render other options
	function RenderOtherOptions() {
		global $Language, $Security;
		$options = &$this->OtherOptions;
		if ($this->CurrentAction <> "gridadd" && $this->CurrentAction <> "gridedit") { // Not grid add/edit mode
			$option = &$options["action"];

			// Set up list action buttons
			foreach ($this->ListActions->Items as $listaction) {
				if ($listaction->Select == EW_ACTION_MULTIPLE) {
					$item = &$option->Add("custom_" . $listaction->Action);
					$caption = $listaction->Caption;
					$icon = ($listaction->Icon <> "") ? "<span class=\"" . ew_HtmlEncode($listaction->Icon) . "\" data-caption=\"" . ew_HtmlEncode($caption) . "\"></span> " : $caption;
					$item->Body = "<a class=\"ewAction ewListAction\" title=\"" . ew_HtmlEncode($caption) . "\" data-caption=\"" . ew_HtmlEncode($caption) . "\" href=\"\" onclick=\"ew_SubmitAction(event,jQuery.extend({f:document.fOVenlist}," . $listaction->ToJson(TRUE) . "));return false;\">" . $icon . "</a>";
					$item->Visible = $listaction->Allow;
				}
			}

			// Hide grid edit and other options
			if ($this->TotalRecs <= 0) {
				$option = &$options["addedit"];
				$item = &$option->GetItem("gridedit");
				if ($item) $item->Visible = FALSE;
				$option = &$options["action"];
				$option->HideAllOptions();
			}
		} else { // Grid add/edit mode

			// Hide all options first
			foreach ($options as &$option)
				$option->HideAllOptions();
			if ($this->CurrentAction == "gridadd") {
				if ($this->AllowAddDeleteRow) {

					// Add add blank row
					$option = &$options["addedit"];
					$option->UseDropDownButton = FALSE;
					$option->UseImageAndText = TRUE;
					$item = &$option->Add("addblankrow");
					$item->Body = "<a class=\"ewAddEdit ewAddBlankRow\" title=\"" . ew_HtmlTitle($Language->Phrase("AddBlankRow")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("AddBlankRow")) . "\" href=\"javascript:void(0);\" onclick=\"ew_AddGridRow(this);\">" . $Language->Phrase("AddBlankRow") . "</a>";
					$item->Visible = $Security->CanAdd();
				}
				$option = &$options["action"];
				$option->UseDropDownButton = FALSE;
				$option->UseImageAndText = TRUE;

				// Add grid insert
				$item = &$option->Add("gridinsert");
				$item->Body = "<a class=\"ewAction ewGridInsert\" title=\"" . ew_HtmlTitle($Language->Phrase("GridInsertLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("GridInsertLink")) . "\" href=\"\" onclick=\"return ewForms(this).Submit('" . $this->AddMasterUrl($this->PageName()) . "');\">" . $Language->Phrase("GridInsertLink") . "</a>";

				// Add grid cancel
				$item = &$option->Add("gridcancel");
				$cancelurl = $this->AddMasterUrl($this->PageUrl() . "a=cancel");
				$item->Body = "<a class=\"ewAction ewGridCancel\" title=\"" . ew_HtmlTitle($Language->Phrase("GridCancelLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("GridCancelLink")) . "\" href=\"" . $cancelurl . "\">" . $Language->Phrase("GridCancelLink") . "</a>";
			}
			if ($this->CurrentAction == "gridedit") {
				if ($this->AllowAddDeleteRow) {

					// Add add blank row
					$option = &$options["addedit"];
					$option->UseDropDownButton = FALSE;
					$option->UseImageAndText = TRUE;
					$item = &$option->Add("addblankrow");
					$item->Body = "<a class=\"ewAddEdit ewAddBlankRow\" title=\"" . ew_HtmlTitle($Language->Phrase("AddBlankRow")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("AddBlankRow")) . "\" href=\"javascript:void(0);\" onclick=\"ew_AddGridRow(this);\">" . $Language->Phrase("AddBlankRow") . "</a>";
					$item->Visible = $Security->CanAdd();
				}
				$option = &$options["action"];
				$option->UseDropDownButton = FALSE;
				$option->UseImageAndText = TRUE;
					$item = &$option->Add("gridsave");
					$item->Body = "<a class=\"ewAction ewGridSave\" title=\"" . ew_HtmlTitle($Language->Phrase("GridSaveLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("GridSaveLink")) . "\" href=\"\" onclick=\"return ewForms(this).Submit('" . $this->AddMasterUrl($this->PageName()) . "');\">" . $Language->Phrase("GridSaveLink") . "</a>";
					$item = &$option->Add("gridcancel");
					$cancelurl = $this->AddMasterUrl($this->PageUrl() . "a=cancel");
					$item->Body = "<a class=\"ewAction ewGridCancel\" title=\"" . ew_HtmlTitle($Language->Phrase("GridCancelLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("GridCancelLink")) . "\" href=\"" . $cancelurl . "\">" . $Language->Phrase("GridCancelLink") . "</a>";
			}
		}
	}

	// Process list action
	function ProcessListAction() {
		global $Language, $Security;
		$userlist = "";
		$user = "";
		$sFilter = $this->GetKeyFilter();
		$UserAction = @$_POST["useraction"];
		if ($sFilter <> "" && $UserAction <> "") {

			// Check permission first
			$ActionCaption = $UserAction;
			if (array_key_exists($UserAction, $this->ListActions->Items)) {
				$ActionCaption = $this->ListActions->Items[$UserAction]->Caption;
				if (!$this->ListActions->Items[$UserAction]->Allow) {
					$errmsg = str_replace('%s', $ActionCaption, $Language->Phrase("CustomActionNotAllowed"));
					if (@$_POST["ajax"] == $UserAction) // Ajax
						echo "<p class=\"text-danger\">" . $errmsg . "</p>";
					else
						$this->setFailureMessage($errmsg);
					return FALSE;
				}
			}
			$this->CurrentFilter = $sFilter;
			$sSql = $this->SQL();
			$conn = &$this->Connection();
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			$rs = $conn->Execute($sSql);
			$conn->raiseErrorFn = '';
			$this->CurrentAction = $UserAction;

			// Call row action event
			if ($rs && !$rs->EOF) {
				$conn->BeginTrans();
				$this->SelectedCount = $rs->RecordCount();
				$this->SelectedIndex = 0;
				while (!$rs->EOF) {
					$this->SelectedIndex++;
					$row = $rs->fields;
					$Processed = $this->Row_CustomAction($UserAction, $row);
					if (!$Processed) break;
					$rs->MoveNext();
				}
				if ($Processed) {
					$conn->CommitTrans(); // Commit the changes
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage(str_replace('%s', $ActionCaption, $Language->Phrase("CustomActionCompleted"))); // Set up success message
				} else {
					$conn->RollbackTrans(); // Rollback changes

					// Set up error message
					if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

						// Use the message, do nothing
					} elseif ($this->CancelMessage <> "") {
						$this->setFailureMessage($this->CancelMessage);
						$this->CancelMessage = "";
					} else {
						$this->setFailureMessage(str_replace('%s', $ActionCaption, $Language->Phrase("CustomActionFailed")));
					}
				}
			}
			if ($rs)
				$rs->Close();
			if (@$_POST["ajax"] == $UserAction) { // Ajax
				if ($this->getSuccessMessage() <> "") {
					echo "<p class=\"text-success\">" . $this->getSuccessMessage() . "</p>";
					$this->ClearSuccessMessage(); // Clear message
				}
				if ($this->getFailureMessage() <> "") {
					echo "<p class=\"text-danger\">" . $this->getFailureMessage() . "</p>";
					$this->ClearFailureMessage(); // Clear message
				}
				return TRUE;
			}
		}
		return FALSE; // Not ajax request
	}

	// Set up search options
	function SetupSearchOptions() {
		global $Language;
		$this->SearchOptions = new cListOptions();
		$this->SearchOptions->Tag = "div";
		$this->SearchOptions->TagClassName = "ewSearchOption";

		// Search button
		$item = &$this->SearchOptions->Add("searchtoggle");
		$SearchToggleClass = ($this->SearchWhere <> "") ? " active" : " active";
		$item->Body = "<button type=\"button\" class=\"btn btn-default ewSearchToggle" . $SearchToggleClass . "\" title=\"" . $Language->Phrase("SearchPanel") . "\" data-caption=\"" . $Language->Phrase("SearchPanel") . "\" data-toggle=\"button\" data-form=\"fOVenlistsrch\">" . $Language->Phrase("SearchBtn") . "</button>";
		$item->Visible = TRUE;

		// Show all button
		$item = &$this->SearchOptions->Add("showall");
		$item->Body = "<a class=\"btn btn-default ewShowAll\" title=\"" . $Language->Phrase("ShowAll") . "\" data-caption=\"" . $Language->Phrase("ShowAll") . "\" href=\"" . $this->PageUrl() . "cmd=reset\">" . $Language->Phrase("ShowAllBtn") . "</a>";
		$item->Visible = ($this->SearchWhere <> $this->DefaultSearchWhere && $this->SearchWhere <> "0=101");

		// Button group for search
		$this->SearchOptions->UseDropDownButton = FALSE;
		$this->SearchOptions->UseImageAndText = TRUE;
		$this->SearchOptions->UseButtonGroup = TRUE;
		$this->SearchOptions->DropDownButtonPhrase = $Language->Phrase("ButtonSearch");

		// Add group option item
		$item = &$this->SearchOptions->Add($this->SearchOptions->GroupOptionName);
		$item->Body = "";
		$item->Visible = FALSE;

		// Hide search options
		if ($this->Export <> "" || $this->CurrentAction <> "")
			$this->SearchOptions->HideAllOptions();
		global $Security;
		if (!$Security->CanSearch()) {
			$this->SearchOptions->HideAllOptions();
			$this->FilterOptions->HideAllOptions();
		}
	}

	function SetupListOptionsExt() {
		global $Security, $Language;
	}

	function RenderListOptionsExt() {
		global $Security, $Language;
	}

	// Set up starting record parameters
	function SetUpStartRec() {
		if ($this->DisplayRecs == 0)
			return;
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET[EW_TABLE_START_REC] <> "") { // Check for "start" parameter
				$this->StartRec = $_GET[EW_TABLE_START_REC];
				$this->setStartRecordNumber($this->StartRec);
			} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
				$PageNo = $_GET[EW_TABLE_PAGE_NO];
				if (is_numeric($PageNo)) {
					$this->StartRec = ($PageNo-1)*$this->DisplayRecs+1;
					if ($this->StartRec <= 0) {
						$this->StartRec = 1;
					} elseif ($this->StartRec >= intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1) {
						$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1;
					}
					$this->setStartRecordNumber($this->StartRec);
				}
			}
		}
		$this->StartRec = $this->getStartRecordNumber();

		// Check if correct start record counter
		if (!is_numeric($this->StartRec) || $this->StartRec == "") { // Avoid invalid start record counter
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} elseif (intval($this->StartRec) > intval($this->TotalRecs)) { // Avoid starting record > total records
			$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to last page first record
			$this->setStartRecordNumber($this->StartRec);
		} elseif (($this->StartRec-1) % $this->DisplayRecs <> 0) {
			$this->StartRec = intval(($this->StartRec-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to page boundary
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Load default values
	function LoadDefaultValues() {
		$this->OveCodi->CurrentValue = NULL;
		$this->OveCodi->OldValue = $this->OveCodi->CurrentValue;
		$this->ProCodi->CurrentValue = NULL;
		$this->ProCodi->OldValue = $this->ProCodi->CurrentValue;
		$this->OvePIni->CurrentValue = NULL;
		$this->OvePIni->OldValue = $this->OvePIni->CurrentValue;
		$this->OveCPro->CurrentValue = NULL;
		$this->OveCPro->OldValue = $this->OveCPro->CurrentValue;
		$this->OveUsua->CurrentValue = NULL;
		$this->OveUsua->OldValue = $this->OveUsua->CurrentValue;
		$this->OveFCre->CurrentValue = NULL;
		$this->OveFCre->OldValue = $this->OveFCre->CurrentValue;
		$this->OveCVen->CurrentValue = NULL;
		$this->OveCVen->OldValue = $this->OveCVen->CurrentValue;
		$this->CliCodi->CurrentValue = NULL;
		$this->CliCodi->OldValue = $this->CliCodi->CurrentValue;
	}

	// Load basic search values
	function LoadBasicSearchValues() {
		$this->BasicSearch->Keyword = @$_GET[EW_TABLE_BASIC_SEARCH];
		if ($this->BasicSearch->Keyword <> "") $this->Command = "search";
		$this->BasicSearch->Type = @$_GET[EW_TABLE_BASIC_SEARCH_TYPE];
	}

	// Load form values
	function LoadFormValues() {

		// Load from form
		global $objForm;
		if (!$this->OveCodi->FldIsDetailKey && $this->CurrentAction <> "gridadd" && $this->CurrentAction <> "add")
			$this->OveCodi->setFormValue($objForm->GetValue("x_OveCodi"));
		if (!$this->ProCodi->FldIsDetailKey) {
			$this->ProCodi->setFormValue($objForm->GetValue("x_ProCodi"));
		}
		$this->ProCodi->setOldValue($objForm->GetValue("o_ProCodi"));
		if (!$this->OvePIni->FldIsDetailKey) {
			$this->OvePIni->setFormValue($objForm->GetValue("x_OvePIni"));
		}
		$this->OvePIni->setOldValue($objForm->GetValue("o_OvePIni"));
		if (!$this->OveCPro->FldIsDetailKey) {
			$this->OveCPro->setFormValue($objForm->GetValue("x_OveCPro"));
		}
		$this->OveCPro->setOldValue($objForm->GetValue("o_OveCPro"));
		if (!$this->OveUsua->FldIsDetailKey) {
			$this->OveUsua->setFormValue($objForm->GetValue("x_OveUsua"));
		}
		$this->OveUsua->setOldValue($objForm->GetValue("o_OveUsua"));
		if (!$this->OveFCre->FldIsDetailKey) {
			$this->OveFCre->setFormValue($objForm->GetValue("x_OveFCre"));
			$this->OveFCre->CurrentValue = ew_UnFormatDateTime($this->OveFCre->CurrentValue, 7);
		}
		$this->OveFCre->setOldValue($objForm->GetValue("o_OveFCre"));
		if (!$this->OveCVen->FldIsDetailKey) {
			$this->OveCVen->setFormValue($objForm->GetValue("x_OveCVen"));
		}
		$this->OveCVen->setOldValue($objForm->GetValue("o_OveCVen"));
		if (!$this->CliCodi->FldIsDetailKey) {
			$this->CliCodi->setFormValue($objForm->GetValue("x_CliCodi"));
		}
		$this->CliCodi->setOldValue($objForm->GetValue("o_CliCodi"));
	}

	// Restore form values
	function RestoreFormValues() {
		global $objForm;
		if ($this->CurrentAction <> "gridadd" && $this->CurrentAction <> "add")
			$this->OveCodi->CurrentValue = $this->OveCodi->FormValue;
		$this->ProCodi->CurrentValue = $this->ProCodi->FormValue;
		$this->OvePIni->CurrentValue = $this->OvePIni->FormValue;
		$this->OveCPro->CurrentValue = $this->OveCPro->FormValue;
		$this->OveUsua->CurrentValue = $this->OveUsua->FormValue;
		$this->OveFCre->CurrentValue = $this->OveFCre->FormValue;
		$this->OveFCre->CurrentValue = ew_UnFormatDateTime($this->OveFCre->CurrentValue, 7);
		$this->OveCVen->CurrentValue = $this->OveCVen->FormValue;
		$this->CliCodi->CurrentValue = $this->CliCodi->FormValue;
	}

	// Load recordset
	function LoadRecordset($offset = -1, $rowcnt = -1) {

		// Load List page SQL
		$sSql = $this->SelectSQL();
		$conn = &$this->Connection();

		// Load recordset
		$dbtype = ew_GetConnectionType($this->DBID);
		if ($this->UseSelectLimit) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			if ($dbtype == "MSSQL") {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset, array("_hasOrderBy" => trim($this->getOrderBy()) || trim($this->getSessionOrderBy())));
			} else {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset);
			}
			$conn->raiseErrorFn = '';
		} else {
			$rs = ew_LoadRecordset($sSql, $conn);
		}

		// Call Recordset Selected event
		$this->Recordset_Selected($rs);
		return $rs;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->OveCodi->setDbValue($rs->fields('OveCodi'));
		$this->ProCodi->setDbValue($rs->fields('ProCodi'));
		$this->OvePIni->setDbValue($rs->fields('OvePIni'));
		$this->OveCPro->setDbValue($rs->fields('OveCPro'));
		$this->OveUsua->setDbValue($rs->fields('OveUsua'));
		$this->OveFCre->setDbValue($rs->fields('OveFCre'));
		$this->OveCVen->setDbValue($rs->fields('OveCVen'));
		$this->CliCodi->setDbValue($rs->fields('CliCodi'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->OveCodi->DbValue = $row['OveCodi'];
		$this->ProCodi->DbValue = $row['ProCodi'];
		$this->OvePIni->DbValue = $row['OvePIni'];
		$this->OveCPro->DbValue = $row['OveCPro'];
		$this->OveUsua->DbValue = $row['OveUsua'];
		$this->OveFCre->DbValue = $row['OveFCre'];
		$this->OveCVen->DbValue = $row['OveCVen'];
		$this->CliCodi->DbValue = $row['CliCodi'];
	}

	// Load old record
	function LoadOldRecord() {

		// Load key values from Session
		$bValidKey = TRUE;
		if (strval($this->getKey("OveCodi")) <> "")
			$this->OveCodi->CurrentValue = $this->getKey("OveCodi"); // OveCodi
		else
			$bValidKey = FALSE;

		// Load old recordset
		if ($bValidKey) {
			$this->CurrentFilter = $this->KeyFilter();
			$sSql = $this->SQL();
			$conn = &$this->Connection();
			$this->OldRecordset = ew_LoadRecordset($sSql, $conn);
			$this->LoadRowValues($this->OldRecordset); // Load row values
		} else {
			$this->OldRecordset = NULL;
		}
		return $bValidKey;
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		$this->ViewUrl = $this->GetViewUrl();
		$this->EditUrl = $this->GetEditUrl();
		$this->InlineEditUrl = $this->GetInlineEditUrl();
		$this->CopyUrl = $this->GetCopyUrl();
		$this->InlineCopyUrl = $this->GetInlineCopyUrl();
		$this->DeleteUrl = $this->GetDeleteUrl();

		// Convert decimal values if posted back
		if ($this->OvePIni->FormValue == $this->OvePIni->CurrentValue && is_numeric(ew_StrToFloat($this->OvePIni->CurrentValue)))
			$this->OvePIni->CurrentValue = ew_StrToFloat($this->OvePIni->CurrentValue);

		// Call Row_Rendering event
		$this->Row_Rendering();

		// Common render codes for all row types
		// OveCodi
		// ProCodi
		// OvePIni
		// OveCPro
		// OveUsua
		// OveFCre
		// OveCVen
		// CliCodi

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// OveCodi
		$this->OveCodi->ViewValue = $this->OveCodi->CurrentValue;
		$this->OveCodi->ViewCustomAttributes = "";

		// ProCodi
		if (strval($this->ProCodi->CurrentValue) <> "") {
			$sFilterWrk = "\"ProdCodi\"" . ew_SearchString("=", $this->ProCodi->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT \"ProdCodi\", \"CprCodi\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"Prod\"";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->ProCodi, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->ProCodi->ViewValue = $this->ProCodi->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->ProCodi->ViewValue = $this->ProCodi->CurrentValue;
			}
		} else {
			$this->ProCodi->ViewValue = NULL;
		}
		$this->ProCodi->ViewCustomAttributes = "";

		// OvePIni
		$this->OvePIni->ViewValue = $this->OvePIni->CurrentValue;
		$this->OvePIni->ViewCustomAttributes = "";

		// OveCPro
		$this->OveCPro->ViewValue = $this->OveCPro->CurrentValue;
		$this->OveCPro->ViewCustomAttributes = "";

		// OveUsua
		$this->OveUsua->ViewValue = $this->OveUsua->CurrentValue;
		$this->OveUsua->ViewCustomAttributes = "";

		// OveFCre
		$this->OveFCre->ViewValue = $this->OveFCre->CurrentValue;
		$this->OveFCre->ViewValue = ew_FormatDateTime($this->OveFCre->ViewValue, 7);
		$this->OveFCre->ViewCustomAttributes = "";

		// OveCVen
		$this->OveCVen->ViewValue = $this->OveCVen->CurrentValue;
		$this->OveCVen->ViewCustomAttributes = "";

		// CliCodi
		if (strval($this->CliCodi->CurrentValue) <> "") {
			$sFilterWrk = "\"CliCodi\"" . ew_SearchString("=", $this->CliCodi->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT \"CliCodi\", \"CliNomb\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"Clie\"";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->CliCodi, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->CliCodi->ViewValue = $this->CliCodi->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->CliCodi->ViewValue = $this->CliCodi->CurrentValue;
			}
		} else {
			$this->CliCodi->ViewValue = NULL;
		}
		$this->CliCodi->ViewCustomAttributes = "";

			// OveCodi
			$this->OveCodi->LinkCustomAttributes = "";
			$this->OveCodi->HrefValue = "";
			$this->OveCodi->TooltipValue = "";

			// ProCodi
			$this->ProCodi->LinkCustomAttributes = "";
			$this->ProCodi->HrefValue = "";
			$this->ProCodi->TooltipValue = "";

			// OvePIni
			$this->OvePIni->LinkCustomAttributes = "";
			$this->OvePIni->HrefValue = "";
			$this->OvePIni->TooltipValue = "";

			// OveCPro
			$this->OveCPro->LinkCustomAttributes = "";
			$this->OveCPro->HrefValue = "";
			$this->OveCPro->TooltipValue = "";

			// OveUsua
			$this->OveUsua->LinkCustomAttributes = "";
			$this->OveUsua->HrefValue = "";
			$this->OveUsua->TooltipValue = "";

			// OveFCre
			$this->OveFCre->LinkCustomAttributes = "";
			$this->OveFCre->HrefValue = "";
			$this->OveFCre->TooltipValue = "";

			// OveCVen
			$this->OveCVen->LinkCustomAttributes = "";
			$this->OveCVen->HrefValue = "";
			$this->OveCVen->TooltipValue = "";

			// CliCodi
			$this->CliCodi->LinkCustomAttributes = "";
			$this->CliCodi->HrefValue = "";
			$this->CliCodi->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_ADD) { // Add row

			// OveCodi
			// ProCodi

			$this->ProCodi->EditAttrs["class"] = "form-control";
			$this->ProCodi->EditCustomAttributes = "";
			if (trim(strval($this->ProCodi->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "\"ProdCodi\"" . ew_SearchString("=", $this->ProCodi->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT \"ProdCodi\", \"CprCodi\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\", '' AS \"SelectFilterFld\", '' AS \"SelectFilterFld2\", '' AS \"SelectFilterFld3\", '' AS \"SelectFilterFld4\" FROM \"public\".\"Prod\"";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->ProCodi, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->ProCodi->EditValue = $arwrk;

			// OvePIni
			$this->OvePIni->EditAttrs["class"] = "form-control";
			$this->OvePIni->EditCustomAttributes = "";
			$this->OvePIni->EditValue = ew_HtmlEncode($this->OvePIni->CurrentValue);
			$this->OvePIni->PlaceHolder = ew_RemoveHtml($this->OvePIni->FldCaption());
			if (strval($this->OvePIni->EditValue) <> "" && is_numeric($this->OvePIni->EditValue)) {
			$this->OvePIni->EditValue = ew_FormatNumber($this->OvePIni->EditValue, -2, -1, -2, 0);
			$this->OvePIni->OldValue = $this->OvePIni->EditValue;
			}

			// OveCPro
			$this->OveCPro->EditAttrs["class"] = "form-control";
			$this->OveCPro->EditCustomAttributes = "";
			$this->OveCPro->EditValue = ew_HtmlEncode($this->OveCPro->CurrentValue);
			$this->OveCPro->PlaceHolder = ew_RemoveHtml($this->OveCPro->FldCaption());

			// OveUsua
			$this->OveUsua->EditAttrs["class"] = "form-control";
			$this->OveUsua->EditCustomAttributes = "";
			$this->OveUsua->EditValue = ew_HtmlEncode($this->OveUsua->CurrentValue);
			$this->OveUsua->PlaceHolder = ew_RemoveHtml($this->OveUsua->FldCaption());

			// OveFCre
			$this->OveFCre->EditAttrs["class"] = "form-control";
			$this->OveFCre->EditCustomAttributes = "";
			$this->OveFCre->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->OveFCre->CurrentValue, 7));
			$this->OveFCre->PlaceHolder = ew_RemoveHtml($this->OveFCre->FldCaption());

			// OveCVen
			$this->OveCVen->EditAttrs["class"] = "form-control";
			$this->OveCVen->EditCustomAttributes = "";
			$this->OveCVen->EditValue = ew_HtmlEncode($this->OveCVen->CurrentValue);
			$this->OveCVen->PlaceHolder = ew_RemoveHtml($this->OveCVen->FldCaption());

			// CliCodi
			$this->CliCodi->EditAttrs["class"] = "form-control";
			$this->CliCodi->EditCustomAttributes = "";
			if (trim(strval($this->CliCodi->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "\"CliCodi\"" . ew_SearchString("=", $this->CliCodi->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT \"CliCodi\", \"CliNomb\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\", '' AS \"SelectFilterFld\", '' AS \"SelectFilterFld2\", '' AS \"SelectFilterFld3\", '' AS \"SelectFilterFld4\" FROM \"public\".\"Clie\"";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->CliCodi, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->CliCodi->EditValue = $arwrk;

			// Edit refer script
			// OveCodi

			$this->OveCodi->HrefValue = "";

			// ProCodi
			$this->ProCodi->HrefValue = "";

			// OvePIni
			$this->OvePIni->HrefValue = "";

			// OveCPro
			$this->OveCPro->HrefValue = "";

			// OveUsua
			$this->OveUsua->HrefValue = "";

			// OveFCre
			$this->OveFCre->HrefValue = "";

			// OveCVen
			$this->OveCVen->HrefValue = "";

			// CliCodi
			$this->CliCodi->HrefValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_EDIT) { // Edit row

			// OveCodi
			$this->OveCodi->EditAttrs["class"] = "form-control";
			$this->OveCodi->EditCustomAttributes = "";
			$this->OveCodi->EditValue = $this->OveCodi->CurrentValue;
			$this->OveCodi->ViewCustomAttributes = "";

			// ProCodi
			$this->ProCodi->EditAttrs["class"] = "form-control";
			$this->ProCodi->EditCustomAttributes = "";
			if (trim(strval($this->ProCodi->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "\"ProdCodi\"" . ew_SearchString("=", $this->ProCodi->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT \"ProdCodi\", \"CprCodi\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\", '' AS \"SelectFilterFld\", '' AS \"SelectFilterFld2\", '' AS \"SelectFilterFld3\", '' AS \"SelectFilterFld4\" FROM \"public\".\"Prod\"";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->ProCodi, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->ProCodi->EditValue = $arwrk;

			// OvePIni
			$this->OvePIni->EditAttrs["class"] = "form-control";
			$this->OvePIni->EditCustomAttributes = "";
			$this->OvePIni->EditValue = ew_HtmlEncode($this->OvePIni->CurrentValue);
			$this->OvePIni->PlaceHolder = ew_RemoveHtml($this->OvePIni->FldCaption());
			if (strval($this->OvePIni->EditValue) <> "" && is_numeric($this->OvePIni->EditValue)) {
			$this->OvePIni->EditValue = ew_FormatNumber($this->OvePIni->EditValue, -2, -1, -2, 0);
			$this->OvePIni->OldValue = $this->OvePIni->EditValue;
			}

			// OveCPro
			$this->OveCPro->EditAttrs["class"] = "form-control";
			$this->OveCPro->EditCustomAttributes = "";
			$this->OveCPro->EditValue = ew_HtmlEncode($this->OveCPro->CurrentValue);
			$this->OveCPro->PlaceHolder = ew_RemoveHtml($this->OveCPro->FldCaption());

			// OveUsua
			$this->OveUsua->EditAttrs["class"] = "form-control";
			$this->OveUsua->EditCustomAttributes = "";
			$this->OveUsua->EditValue = ew_HtmlEncode($this->OveUsua->CurrentValue);
			$this->OveUsua->PlaceHolder = ew_RemoveHtml($this->OveUsua->FldCaption());

			// OveFCre
			$this->OveFCre->EditAttrs["class"] = "form-control";
			$this->OveFCre->EditCustomAttributes = "";
			$this->OveFCre->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->OveFCre->CurrentValue, 7));
			$this->OveFCre->PlaceHolder = ew_RemoveHtml($this->OveFCre->FldCaption());

			// OveCVen
			$this->OveCVen->EditAttrs["class"] = "form-control";
			$this->OveCVen->EditCustomAttributes = "";
			$this->OveCVen->EditValue = ew_HtmlEncode($this->OveCVen->CurrentValue);
			$this->OveCVen->PlaceHolder = ew_RemoveHtml($this->OveCVen->FldCaption());

			// CliCodi
			$this->CliCodi->EditAttrs["class"] = "form-control";
			$this->CliCodi->EditCustomAttributes = "";
			if (trim(strval($this->CliCodi->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "\"CliCodi\"" . ew_SearchString("=", $this->CliCodi->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT \"CliCodi\", \"CliNomb\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\", '' AS \"SelectFilterFld\", '' AS \"SelectFilterFld2\", '' AS \"SelectFilterFld3\", '' AS \"SelectFilterFld4\" FROM \"public\".\"Clie\"";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->CliCodi, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->CliCodi->EditValue = $arwrk;

			// Edit refer script
			// OveCodi

			$this->OveCodi->HrefValue = "";

			// ProCodi
			$this->ProCodi->HrefValue = "";

			// OvePIni
			$this->OvePIni->HrefValue = "";

			// OveCPro
			$this->OveCPro->HrefValue = "";

			// OveUsua
			$this->OveUsua->HrefValue = "";

			// OveFCre
			$this->OveFCre->HrefValue = "";

			// OveCVen
			$this->OveCVen->HrefValue = "";

			// CliCodi
			$this->CliCodi->HrefValue = "";
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate form
	function ValidateForm() {
		global $Language, $gsFormError;

		// Initialize form error message
		$gsFormError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return ($gsFormError == "");
		if (!$this->ProCodi->FldIsDetailKey && !is_null($this->ProCodi->FormValue) && $this->ProCodi->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->ProCodi->FldCaption(), $this->ProCodi->ReqErrMsg));
		}
		if (!$this->OvePIni->FldIsDetailKey && !is_null($this->OvePIni->FormValue) && $this->OvePIni->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->OvePIni->FldCaption(), $this->OvePIni->ReqErrMsg));
		}
		if (!ew_CheckNumber($this->OvePIni->FormValue)) {
			ew_AddMessage($gsFormError, $this->OvePIni->FldErrMsg());
		}
		if (!$this->OveCPro->FldIsDetailKey && !is_null($this->OveCPro->FormValue) && $this->OveCPro->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->OveCPro->FldCaption(), $this->OveCPro->ReqErrMsg));
		}
		if (!ew_CheckInteger($this->OveCPro->FormValue)) {
			ew_AddMessage($gsFormError, $this->OveCPro->FldErrMsg());
		}
		if (!$this->OveUsua->FldIsDetailKey && !is_null($this->OveUsua->FormValue) && $this->OveUsua->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->OveUsua->FldCaption(), $this->OveUsua->ReqErrMsg));
		}
		if (!$this->OveFCre->FldIsDetailKey && !is_null($this->OveFCre->FormValue) && $this->OveFCre->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->OveFCre->FldCaption(), $this->OveFCre->ReqErrMsg));
		}
		if (!ew_CheckEuroDate($this->OveFCre->FormValue)) {
			ew_AddMessage($gsFormError, $this->OveFCre->FldErrMsg());
		}
		if (!$this->OveCVen->FldIsDetailKey && !is_null($this->OveCVen->FormValue) && $this->OveCVen->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->OveCVen->FldCaption(), $this->OveCVen->ReqErrMsg));
		}
		if (!ew_CheckInteger($this->OveCVen->FormValue)) {
			ew_AddMessage($gsFormError, $this->OveCVen->FldErrMsg());
		}

		// Return validate result
		$ValidateForm = ($gsFormError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateForm = $ValidateForm && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsFormError, $sFormCustomError);
		}
		return $ValidateForm;
	}

	//
	// Delete records based on current filter
	//
	function DeleteRows() {
		global $Language, $Security;
		if (!$Security->CanDelete()) {
			$this->setFailureMessage($Language->Phrase("NoDeletePermission")); // No delete permission
			return FALSE;
		}
		$DeleteRows = TRUE;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE) {
			return FALSE;
		} elseif ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
			$rs->Close();
			return FALSE;

		//} else {
		//	$this->LoadRowValues($rs); // Load row values

		}
		$rows = ($rs) ? $rs->GetRows() : array();

		// Clone old rows
		$rsold = $rows;
		if ($rs)
			$rs->Close();

		// Call row deleting event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$DeleteRows = $this->Row_Deleting($row);
				if (!$DeleteRows) break;
			}
		}
		if ($DeleteRows) {
			$sKey = "";
			foreach ($rsold as $row) {
				$sThisKey = "";
				if ($sThisKey <> "") $sThisKey .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
				$sThisKey .= $row['OveCodi'];
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				$DeleteRows = $this->Delete($row); // Delete
				$conn->raiseErrorFn = '';
				if ($DeleteRows === FALSE)
					break;
				if ($sKey <> "") $sKey .= ", ";
				$sKey .= $sThisKey;
			}
		} else {

			// Set up error message
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("DeleteCancelled"));
			}
		}
		if ($DeleteRows) {
		} else {
		}

		// Call Row Deleted event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$this->Row_Deleted($row);
			}
		}
		return $DeleteRows;
	}

	// Update record based on key values
	function EditRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$conn = &$this->Connection();
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE)
			return FALSE;
		if ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record message
			$EditRow = FALSE; // Update Failed
		} else {

			// Save old values
			$rsold = &$rs->fields;
			$this->LoadDbValues($rsold);
			$rsnew = array();

			// ProCodi
			$this->ProCodi->SetDbValueDef($rsnew, $this->ProCodi->CurrentValue, 0, $this->ProCodi->ReadOnly);

			// OvePIni
			$this->OvePIni->SetDbValueDef($rsnew, $this->OvePIni->CurrentValue, 0, $this->OvePIni->ReadOnly);

			// OveCPro
			$this->OveCPro->SetDbValueDef($rsnew, $this->OveCPro->CurrentValue, 0, $this->OveCPro->ReadOnly);

			// OveUsua
			$this->OveUsua->SetDbValueDef($rsnew, $this->OveUsua->CurrentValue, "", $this->OveUsua->ReadOnly);

			// OveFCre
			$this->OveFCre->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->OveFCre->CurrentValue, 7), ew_CurrentDate(), $this->OveFCre->ReadOnly);

			// OveCVen
			$this->OveCVen->SetDbValueDef($rsnew, $this->OveCVen->CurrentValue, 0, $this->OveCVen->ReadOnly);

			// CliCodi
			$this->CliCodi->SetDbValueDef($rsnew, $this->CliCodi->CurrentValue, NULL, $this->CliCodi->ReadOnly);

			// Call Row Updating event
			$bUpdateRow = $this->Row_Updating($rsold, $rsnew);
			if ($bUpdateRow) {
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				if (count($rsnew) > 0)
					$EditRow = $this->Update($rsnew, "", $rsold);
				else
					$EditRow = TRUE; // No field to update
				$conn->raiseErrorFn = '';
				if ($EditRow) {
				}
			} else {
				if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

					// Use the message, do nothing
				} elseif ($this->CancelMessage <> "") {
					$this->setFailureMessage($this->CancelMessage);
					$this->CancelMessage = "";
				} else {
					$this->setFailureMessage($Language->Phrase("UpdateCancelled"));
				}
				$EditRow = FALSE;
			}
		}

		// Call Row_Updated event
		if ($EditRow)
			$this->Row_Updated($rsold, $rsnew);
		$rs->Close();
		return $EditRow;
	}

	// Add record
	function AddRow($rsold = NULL) {
		global $Language, $Security;
		$conn = &$this->Connection();

		// Load db values from rsold
		if ($rsold) {
			$this->LoadDbValues($rsold);
		}
		$rsnew = array();

		// ProCodi
		$this->ProCodi->SetDbValueDef($rsnew, $this->ProCodi->CurrentValue, 0, FALSE);

		// OvePIni
		$this->OvePIni->SetDbValueDef($rsnew, $this->OvePIni->CurrentValue, 0, FALSE);

		// OveCPro
		$this->OveCPro->SetDbValueDef($rsnew, $this->OveCPro->CurrentValue, 0, FALSE);

		// OveUsua
		$this->OveUsua->SetDbValueDef($rsnew, $this->OveUsua->CurrentValue, "", FALSE);

		// OveFCre
		$this->OveFCre->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->OveFCre->CurrentValue, 7), ew_CurrentDate(), FALSE);

		// OveCVen
		$this->OveCVen->SetDbValueDef($rsnew, $this->OveCVen->CurrentValue, 0, FALSE);

		// CliCodi
		$this->CliCodi->SetDbValueDef($rsnew, $this->CliCodi->CurrentValue, NULL, FALSE);

		// Call Row Inserting event
		$rs = ($rsold == NULL) ? NULL : $rsold->fields;
		$bInsertRow = $this->Row_Inserting($rs, $rsnew);
		if ($bInsertRow) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			$AddRow = $this->Insert($rsnew);
			$conn->raiseErrorFn = '';
			if ($AddRow) {

				// Get insert id if necessary
				$this->OveCodi->setDbValue($conn->GetOne("SELECT currval('\"OVen_OveCodi_seq1\"'::regclass)"));
				$rsnew['OveCodi'] = $this->OveCodi->DbValue;
			}
		} else {
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("InsertCancelled"));
			}
			$AddRow = FALSE;
		}
		if ($AddRow) {

			// Call Row Inserted event
			$rs = ($rsold == NULL) ? NULL : $rsold->fields;
			$this->Row_Inserted($rs, $rsnew);
		}
		return $AddRow;
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$url = preg_replace('/\?cmd=reset(all){0,1}$/i', '', $url); // Remove cmd=reset / cmd=resetall
		$Breadcrumb->Add("list", $this->TableVar, $url, "", $this->TableVar, TRUE);
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}

	// ListOptions Load event
	function ListOptions_Load() {

		// Example:
		//$opt = &$this->ListOptions->Add("new");
		//$opt->Header = "xxx";
		//$opt->OnLeft = TRUE; // Link on left
		//$opt->MoveTo(0); // Move to first column

	}

	// ListOptions Rendered event
	function ListOptions_Rendered() {

		// Example: 
		//$this->ListOptions->Items["new"]->Body = "xxx";

	}

	// Row Custom Action event
	function Row_CustomAction($action, $row) {

		// Return FALSE to abort
		return TRUE;
	}

	// Page Exporting event
	// $this->ExportDoc = export document object
	function Page_Exporting() {

		//$this->ExportDoc->Text = "my header"; // Export header
		//return FALSE; // Return FALSE to skip default export and use Row_Export event

		return TRUE; // Return TRUE to use default export and skip Row_Export event
	}

	// Row Export event
	// $this->ExportDoc = export document object
	function Row_Export($rs) {

	    //$this->ExportDoc->Text .= "my content"; // Build HTML with field value: $rs["MyField"] or $this->MyField->ViewValue
	}

	// Page Exported event
	// $this->ExportDoc = export document object
	function Page_Exported() {

		//$this->ExportDoc->Text .= "my footer"; // Export footer
		//echo $this->ExportDoc->Text;

	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($OVen_list)) $OVen_list = new cOVen_list();

// Page init
$OVen_list->Page_Init();

// Page main
$OVen_list->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$OVen_list->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "list";
var CurrentForm = fOVenlist = new ew_Form("fOVenlist", "list");
fOVenlist.FormKeyCountName = '<?php echo $OVen_list->FormKeyCountName ?>';

// Validate form
fOVenlist.Validate = function() {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.GetForm(), $fobj = $(fobj);
	if ($fobj.find("#a_confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.FormKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = $fobj.find("#a_list").val() == "gridinsert";
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
		var checkrow = (gridinsert) ? !this.EmptyRow(infix) : true;
		if (checkrow) {
			addcnt++;
			elm = this.GetElements("x" + infix + "_ProCodi");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $OVen->ProCodi->FldCaption(), $OVen->ProCodi->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_OvePIni");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $OVen->OvePIni->FldCaption(), $OVen->OvePIni->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_OvePIni");
			if (elm && !ew_CheckNumber(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($OVen->OvePIni->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_OveCPro");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $OVen->OveCPro->FldCaption(), $OVen->OveCPro->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_OveCPro");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($OVen->OveCPro->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_OveUsua");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $OVen->OveUsua->FldCaption(), $OVen->OveUsua->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_OveFCre");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $OVen->OveFCre->FldCaption(), $OVen->OveFCre->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_OveFCre");
			if (elm && !ew_CheckEuroDate(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($OVen->OveFCre->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_OveCVen");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $OVen->OveCVen->FldCaption(), $OVen->OveCVen->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_OveCVen");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($OVen->OveCVen->FldErrMsg()) ?>");

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
		} // End Grid Add checking
	}
	if (gridinsert && addcnt == 0) { // No row added
		ew_Alert(ewLanguage.Phrase("NoAddRecord"));
		return false;
	}
	return true;
}

// Check empty row
fOVenlist.EmptyRow = function(infix) {
	var fobj = this.Form;
	if (ew_ValueChanged(fobj, infix, "ProCodi", false)) return false;
	if (ew_ValueChanged(fobj, infix, "OvePIni", false)) return false;
	if (ew_ValueChanged(fobj, infix, "OveCPro", false)) return false;
	if (ew_ValueChanged(fobj, infix, "OveUsua", false)) return false;
	if (ew_ValueChanged(fobj, infix, "OveFCre", false)) return false;
	if (ew_ValueChanged(fobj, infix, "OveCVen", false)) return false;
	if (ew_ValueChanged(fobj, infix, "CliCodi", false)) return false;
	return true;
}

// Form_CustomValidate event
fOVenlist.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fOVenlist.ValidateRequired = true;
<?php } else { ?>
fOVenlist.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
fOVenlist.Lists["x_ProCodi"] = {"LinkField":"x_ProdCodi","Ajax":true,"AutoFill":false,"DisplayFields":["x_CprCodi","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fOVenlist.Lists["x_CliCodi"] = {"LinkField":"x_CliCodi","Ajax":true,"AutoFill":false,"DisplayFields":["x_CliNomb","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};

// Form object for search
var CurrentSearchForm = fOVenlistsrch = new ew_Form("fOVenlistsrch");
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php if ($OVen_list->TotalRecs > 0 && $OVen_list->ExportOptions->Visible()) { ?>
<?php $OVen_list->ExportOptions->Render("body") ?>
<?php } ?>
<?php if ($OVen_list->SearchOptions->Visible()) { ?>
<?php $OVen_list->SearchOptions->Render("body") ?>
<?php } ?>
<?php if ($OVen_list->FilterOptions->Visible()) { ?>
<?php $OVen_list->FilterOptions->Render("body") ?>
<?php } ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php
if ($OVen->CurrentAction == "gridadd") {
	$OVen->CurrentFilter = "0=1";
	$OVen_list->StartRec = 1;
	$OVen_list->DisplayRecs = $OVen->GridAddRowCount;
	$OVen_list->TotalRecs = $OVen_list->DisplayRecs;
	$OVen_list->StopRec = $OVen_list->DisplayRecs;
} else {
	$bSelectLimit = $OVen_list->UseSelectLimit;
	if ($bSelectLimit) {
		if ($OVen_list->TotalRecs <= 0)
			$OVen_list->TotalRecs = $OVen->SelectRecordCount();
	} else {
		if (!$OVen_list->Recordset && ($OVen_list->Recordset = $OVen_list->LoadRecordset()))
			$OVen_list->TotalRecs = $OVen_list->Recordset->RecordCount();
	}
	$OVen_list->StartRec = 1;
	if ($OVen_list->DisplayRecs <= 0 || ($OVen->Export <> "" && $OVen->ExportAll)) // Display all records
		$OVen_list->DisplayRecs = $OVen_list->TotalRecs;
	if (!($OVen->Export <> "" && $OVen->ExportAll))
		$OVen_list->SetUpStartRec(); // Set up start record position
	if ($bSelectLimit)
		$OVen_list->Recordset = $OVen_list->LoadRecordset($OVen_list->StartRec-1, $OVen_list->DisplayRecs);

	// Set no record found message
	if ($OVen->CurrentAction == "" && $OVen_list->TotalRecs == 0) {
		if (!$Security->CanList())
			$OVen_list->setWarningMessage($Language->Phrase("NoPermission"));
		if ($OVen_list->SearchWhere == "0=101")
			$OVen_list->setWarningMessage($Language->Phrase("EnterSearchCriteria"));
		else
			$OVen_list->setWarningMessage($Language->Phrase("NoRecord"));
	}
}
$OVen_list->RenderOtherOptions();
?>
<?php if ($Security->CanSearch()) { ?>
<?php if ($OVen->Export == "" && $OVen->CurrentAction == "") { ?>
<form name="fOVenlistsrch" id="fOVenlistsrch" class="form-inline ewForm" action="<?php echo ew_CurrentPage() ?>">
<?php $SearchPanelClass = ($OVen_list->SearchWhere <> "") ? " in" : " in"; ?>
<div id="fOVenlistsrch_SearchPanel" class="ewSearchPanel collapse<?php echo $SearchPanelClass ?>">
<input type="hidden" name="cmd" value="search">
<input type="hidden" name="t" value="OVen">
	<div class="ewBasicSearch">
<div id="xsr_1" class="ewRow">
	<div class="ewQuickSearch input-group">
	<input type="text" name="<?php echo EW_TABLE_BASIC_SEARCH ?>" id="<?php echo EW_TABLE_BASIC_SEARCH ?>" class="form-control" value="<?php echo ew_HtmlEncode($OVen_list->BasicSearch->getKeyword()) ?>" placeholder="<?php echo ew_HtmlEncode($Language->Phrase("Search")) ?>">
	<input type="hidden" name="<?php echo EW_TABLE_BASIC_SEARCH_TYPE ?>" id="<?php echo EW_TABLE_BASIC_SEARCH_TYPE ?>" value="<?php echo ew_HtmlEncode($OVen_list->BasicSearch->getType()) ?>">
	<div class="input-group-btn">
		<button type="button" data-toggle="dropdown" class="btn btn-default"><span id="searchtype"><?php echo $OVen_list->BasicSearch->getTypeNameShort() ?></span><span class="caret"></span></button>
		<ul class="dropdown-menu pull-right" role="menu">
			<li<?php if ($OVen_list->BasicSearch->getType() == "") echo " class=\"active\""; ?>><a href="javascript:void(0);" onclick="ew_SetSearchType(this)"><?php echo $Language->Phrase("QuickSearchAuto") ?></a></li>
			<li<?php if ($OVen_list->BasicSearch->getType() == "=") echo " class=\"active\""; ?>><a href="javascript:void(0);" onclick="ew_SetSearchType(this,'=')"><?php echo $Language->Phrase("QuickSearchExact") ?></a></li>
			<li<?php if ($OVen_list->BasicSearch->getType() == "AND") echo " class=\"active\""; ?>><a href="javascript:void(0);" onclick="ew_SetSearchType(this,'AND')"><?php echo $Language->Phrase("QuickSearchAll") ?></a></li>
			<li<?php if ($OVen_list->BasicSearch->getType() == "OR") echo " class=\"active\""; ?>><a href="javascript:void(0);" onclick="ew_SetSearchType(this,'OR')"><?php echo $Language->Phrase("QuickSearchAny") ?></a></li>
		</ul>
	<button class="btn btn-primary ewButton" name="btnsubmit" id="btnsubmit" type="submit"><?php echo $Language->Phrase("QuickSearchBtn") ?></button>
	</div>
	</div>
</div>
	</div>
</div>
</form>
<?php } ?>
<?php } ?>
<?php $OVen_list->ShowPageHeader(); ?>
<?php
$OVen_list->ShowMessage();
?>
<?php if ($OVen_list->TotalRecs > 0 || $OVen->CurrentAction <> "") { ?>
<div class="panel panel-default ewGrid">
<form name="fOVenlist" id="fOVenlist" class="form-inline ewForm ewListForm" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($OVen_list->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $OVen_list->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="OVen">
<div id="gmp_OVen" class="<?php if (ew_IsResponsiveLayout()) { echo "table-responsive "; } ?>ewGridMiddlePanel">
<?php if ($OVen_list->TotalRecs > 0) { ?>
<table id="tbl_OVenlist" class="table ewTable">
<?php echo $OVen->TableCustomInnerHtml ?>
<thead><!-- Table header -->
	<tr class="ewTableHeader">
<?php

// Header row
$OVen_list->RowType = EW_ROWTYPE_HEADER;

// Render list options
$OVen_list->RenderListOptions();

// Render list options (header, left)
$OVen_list->ListOptions->Render("header", "left");
?>
<?php if ($OVen->OveCodi->Visible) { // OveCodi ?>
	<?php if ($OVen->SortUrl($OVen->OveCodi) == "") { ?>
		<th data-name="OveCodi"><div id="elh_OVen_OveCodi" class="OVen_OveCodi"><div class="ewTableHeaderCaption"><?php echo $OVen->OveCodi->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="OveCodi"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $OVen->SortUrl($OVen->OveCodi) ?>',2);"><div id="elh_OVen_OveCodi" class="OVen_OveCodi">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $OVen->OveCodi->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($OVen->OveCodi->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($OVen->OveCodi->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($OVen->ProCodi->Visible) { // ProCodi ?>
	<?php if ($OVen->SortUrl($OVen->ProCodi) == "") { ?>
		<th data-name="ProCodi"><div id="elh_OVen_ProCodi" class="OVen_ProCodi"><div class="ewTableHeaderCaption"><?php echo $OVen->ProCodi->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="ProCodi"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $OVen->SortUrl($OVen->ProCodi) ?>',2);"><div id="elh_OVen_ProCodi" class="OVen_ProCodi">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $OVen->ProCodi->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($OVen->ProCodi->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($OVen->ProCodi->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($OVen->OvePIni->Visible) { // OvePIni ?>
	<?php if ($OVen->SortUrl($OVen->OvePIni) == "") { ?>
		<th data-name="OvePIni"><div id="elh_OVen_OvePIni" class="OVen_OvePIni"><div class="ewTableHeaderCaption"><?php echo $OVen->OvePIni->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="OvePIni"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $OVen->SortUrl($OVen->OvePIni) ?>',2);"><div id="elh_OVen_OvePIni" class="OVen_OvePIni">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $OVen->OvePIni->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($OVen->OvePIni->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($OVen->OvePIni->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($OVen->OveCPro->Visible) { // OveCPro ?>
	<?php if ($OVen->SortUrl($OVen->OveCPro) == "") { ?>
		<th data-name="OveCPro"><div id="elh_OVen_OveCPro" class="OVen_OveCPro"><div class="ewTableHeaderCaption"><?php echo $OVen->OveCPro->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="OveCPro"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $OVen->SortUrl($OVen->OveCPro) ?>',2);"><div id="elh_OVen_OveCPro" class="OVen_OveCPro">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $OVen->OveCPro->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($OVen->OveCPro->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($OVen->OveCPro->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($OVen->OveUsua->Visible) { // OveUsua ?>
	<?php if ($OVen->SortUrl($OVen->OveUsua) == "") { ?>
		<th data-name="OveUsua"><div id="elh_OVen_OveUsua" class="OVen_OveUsua"><div class="ewTableHeaderCaption"><?php echo $OVen->OveUsua->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="OveUsua"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $OVen->SortUrl($OVen->OveUsua) ?>',2);"><div id="elh_OVen_OveUsua" class="OVen_OveUsua">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $OVen->OveUsua->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($OVen->OveUsua->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($OVen->OveUsua->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($OVen->OveFCre->Visible) { // OveFCre ?>
	<?php if ($OVen->SortUrl($OVen->OveFCre) == "") { ?>
		<th data-name="OveFCre"><div id="elh_OVen_OveFCre" class="OVen_OveFCre"><div class="ewTableHeaderCaption"><?php echo $OVen->OveFCre->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="OveFCre"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $OVen->SortUrl($OVen->OveFCre) ?>',2);"><div id="elh_OVen_OveFCre" class="OVen_OveFCre">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $OVen->OveFCre->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($OVen->OveFCre->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($OVen->OveFCre->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($OVen->OveCVen->Visible) { // OveCVen ?>
	<?php if ($OVen->SortUrl($OVen->OveCVen) == "") { ?>
		<th data-name="OveCVen"><div id="elh_OVen_OveCVen" class="OVen_OveCVen"><div class="ewTableHeaderCaption"><?php echo $OVen->OveCVen->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="OveCVen"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $OVen->SortUrl($OVen->OveCVen) ?>',2);"><div id="elh_OVen_OveCVen" class="OVen_OveCVen">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $OVen->OveCVen->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($OVen->OveCVen->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($OVen->OveCVen->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($OVen->CliCodi->Visible) { // CliCodi ?>
	<?php if ($OVen->SortUrl($OVen->CliCodi) == "") { ?>
		<th data-name="CliCodi"><div id="elh_OVen_CliCodi" class="OVen_CliCodi"><div class="ewTableHeaderCaption"><?php echo $OVen->CliCodi->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="CliCodi"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $OVen->SortUrl($OVen->CliCodi) ?>',2);"><div id="elh_OVen_CliCodi" class="OVen_CliCodi">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $OVen->CliCodi->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($OVen->CliCodi->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($OVen->CliCodi->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php

// Render list options (header, right)
$OVen_list->ListOptions->Render("header", "right");
?>
	</tr>
</thead>
<tbody>
<?php
if ($OVen->ExportAll && $OVen->Export <> "") {
	$OVen_list->StopRec = $OVen_list->TotalRecs;
} else {

	// Set the last record to display
	if ($OVen_list->TotalRecs > $OVen_list->StartRec + $OVen_list->DisplayRecs - 1)
		$OVen_list->StopRec = $OVen_list->StartRec + $OVen_list->DisplayRecs - 1;
	else
		$OVen_list->StopRec = $OVen_list->TotalRecs;
}

// Restore number of post back records
if ($objForm) {
	$objForm->Index = -1;
	if ($objForm->HasValue($OVen_list->FormKeyCountName) && ($OVen->CurrentAction == "gridadd" || $OVen->CurrentAction == "gridedit" || $OVen->CurrentAction == "F")) {
		$OVen_list->KeyCount = $objForm->GetValue($OVen_list->FormKeyCountName);
		$OVen_list->StopRec = $OVen_list->StartRec + $OVen_list->KeyCount - 1;
	}
}
$OVen_list->RecCnt = $OVen_list->StartRec - 1;
if ($OVen_list->Recordset && !$OVen_list->Recordset->EOF) {
	$OVen_list->Recordset->MoveFirst();
	$bSelectLimit = $OVen_list->UseSelectLimit;
	if (!$bSelectLimit && $OVen_list->StartRec > 1)
		$OVen_list->Recordset->Move($OVen_list->StartRec - 1);
} elseif (!$OVen->AllowAddDeleteRow && $OVen_list->StopRec == 0) {
	$OVen_list->StopRec = $OVen->GridAddRowCount;
}

// Initialize aggregate
$OVen->RowType = EW_ROWTYPE_AGGREGATEINIT;
$OVen->ResetAttrs();
$OVen_list->RenderRow();
if ($OVen->CurrentAction == "gridadd")
	$OVen_list->RowIndex = 0;
if ($OVen->CurrentAction == "gridedit")
	$OVen_list->RowIndex = 0;
while ($OVen_list->RecCnt < $OVen_list->StopRec) {
	$OVen_list->RecCnt++;
	if (intval($OVen_list->RecCnt) >= intval($OVen_list->StartRec)) {
		$OVen_list->RowCnt++;
		if ($OVen->CurrentAction == "gridadd" || $OVen->CurrentAction == "gridedit" || $OVen->CurrentAction == "F") {
			$OVen_list->RowIndex++;
			$objForm->Index = $OVen_list->RowIndex;
			if ($objForm->HasValue($OVen_list->FormActionName))
				$OVen_list->RowAction = strval($objForm->GetValue($OVen_list->FormActionName));
			elseif ($OVen->CurrentAction == "gridadd")
				$OVen_list->RowAction = "insert";
			else
				$OVen_list->RowAction = "";
		}

		// Set up key count
		$OVen_list->KeyCount = $OVen_list->RowIndex;

		// Init row class and style
		$OVen->ResetAttrs();
		$OVen->CssClass = "";
		if ($OVen->CurrentAction == "gridadd") {
			$OVen_list->LoadDefaultValues(); // Load default values
		} else {
			$OVen_list->LoadRowValues($OVen_list->Recordset); // Load row values
		}
		$OVen->RowType = EW_ROWTYPE_VIEW; // Render view
		if ($OVen->CurrentAction == "gridadd") // Grid add
			$OVen->RowType = EW_ROWTYPE_ADD; // Render add
		if ($OVen->CurrentAction == "gridadd" && $OVen->EventCancelled && !$objForm->HasValue("k_blankrow")) // Insert failed
			$OVen_list->RestoreCurrentRowFormValues($OVen_list->RowIndex); // Restore form values
		if ($OVen->CurrentAction == "gridedit") { // Grid edit
			if ($OVen->EventCancelled) {
				$OVen_list->RestoreCurrentRowFormValues($OVen_list->RowIndex); // Restore form values
			}
			if ($OVen_list->RowAction == "insert")
				$OVen->RowType = EW_ROWTYPE_ADD; // Render add
			else
				$OVen->RowType = EW_ROWTYPE_EDIT; // Render edit
		}
		if ($OVen->CurrentAction == "gridedit" && ($OVen->RowType == EW_ROWTYPE_EDIT || $OVen->RowType == EW_ROWTYPE_ADD) && $OVen->EventCancelled) // Update failed
			$OVen_list->RestoreCurrentRowFormValues($OVen_list->RowIndex); // Restore form values
		if ($OVen->RowType == EW_ROWTYPE_EDIT) // Edit row
			$OVen_list->EditRowCnt++;

		// Set up row id / data-rowindex
		$OVen->RowAttrs = array_merge($OVen->RowAttrs, array('data-rowindex'=>$OVen_list->RowCnt, 'id'=>'r' . $OVen_list->RowCnt . '_OVen', 'data-rowtype'=>$OVen->RowType));

		// Render row
		$OVen_list->RenderRow();

		// Render list options
		$OVen_list->RenderListOptions();

		// Skip delete row / empty row for confirm page
		if ($OVen_list->RowAction <> "delete" && $OVen_list->RowAction <> "insertdelete" && !($OVen_list->RowAction == "insert" && $OVen->CurrentAction == "F" && $OVen_list->EmptyRow())) {
?>
	<tr<?php echo $OVen->RowAttributes() ?>>
<?php

// Render list options (body, left)
$OVen_list->ListOptions->Render("body", "left", $OVen_list->RowCnt);
?>
	<?php if ($OVen->OveCodi->Visible) { // OveCodi ?>
		<td data-name="OveCodi"<?php echo $OVen->OveCodi->CellAttributes() ?>>
<?php if ($OVen->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<input type="hidden" data-table="OVen" data-field="x_OveCodi" name="o<?php echo $OVen_list->RowIndex ?>_OveCodi" id="o<?php echo $OVen_list->RowIndex ?>_OveCodi" value="<?php echo ew_HtmlEncode($OVen->OveCodi->OldValue) ?>">
<?php } ?>
<?php if ($OVen->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $OVen_list->RowCnt ?>_OVen_OveCodi" class="form-group OVen_OveCodi">
<span<?php echo $OVen->OveCodi->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $OVen->OveCodi->EditValue ?></p></span>
</span>
<input type="hidden" data-table="OVen" data-field="x_OveCodi" name="x<?php echo $OVen_list->RowIndex ?>_OveCodi" id="x<?php echo $OVen_list->RowIndex ?>_OveCodi" value="<?php echo ew_HtmlEncode($OVen->OveCodi->CurrentValue) ?>">
<?php } ?>
<?php if ($OVen->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $OVen_list->RowCnt ?>_OVen_OveCodi" class="OVen_OveCodi">
<span<?php echo $OVen->OveCodi->ViewAttributes() ?>>
<?php echo $OVen->OveCodi->ListViewValue() ?></span>
</span>
<?php } ?>
<a id="<?php echo $OVen_list->PageObjName . "_row_" . $OVen_list->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($OVen->ProCodi->Visible) { // ProCodi ?>
		<td data-name="ProCodi"<?php echo $OVen->ProCodi->CellAttributes() ?>>
<?php if ($OVen->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $OVen_list->RowCnt ?>_OVen_ProCodi" class="form-group OVen_ProCodi">
<select data-table="OVen" data-field="x_ProCodi" data-value-separator="<?php echo ew_HtmlEncode(is_array($OVen->ProCodi->DisplayValueSeparator) ? json_encode($OVen->ProCodi->DisplayValueSeparator) : $OVen->ProCodi->DisplayValueSeparator) ?>" id="x<?php echo $OVen_list->RowIndex ?>_ProCodi" name="x<?php echo $OVen_list->RowIndex ?>_ProCodi"<?php echo $OVen->ProCodi->EditAttributes() ?>>
<?php
if (is_array($OVen->ProCodi->EditValue)) {
	$arwrk = $OVen->ProCodi->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($OVen->ProCodi->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $OVen->ProCodi->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($OVen->ProCodi->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($OVen->ProCodi->CurrentValue) ?>" selected><?php echo $OVen->ProCodi->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $OVen->ProCodi->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT \"ProdCodi\", \"CprCodi\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"Prod\"";
$sWhereWrk = "";
$OVen->ProCodi->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$OVen->ProCodi->LookupFilters += array("f0" => "\"ProdCodi\" = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$OVen->Lookup_Selecting($OVen->ProCodi, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $OVen->ProCodi->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $OVen_list->RowIndex ?>_ProCodi" id="s_x<?php echo $OVen_list->RowIndex ?>_ProCodi" value="<?php echo $OVen->ProCodi->LookupFilterQuery() ?>">
</span>
<input type="hidden" data-table="OVen" data-field="x_ProCodi" name="o<?php echo $OVen_list->RowIndex ?>_ProCodi" id="o<?php echo $OVen_list->RowIndex ?>_ProCodi" value="<?php echo ew_HtmlEncode($OVen->ProCodi->OldValue) ?>">
<?php } ?>
<?php if ($OVen->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $OVen_list->RowCnt ?>_OVen_ProCodi" class="form-group OVen_ProCodi">
<select data-table="OVen" data-field="x_ProCodi" data-value-separator="<?php echo ew_HtmlEncode(is_array($OVen->ProCodi->DisplayValueSeparator) ? json_encode($OVen->ProCodi->DisplayValueSeparator) : $OVen->ProCodi->DisplayValueSeparator) ?>" id="x<?php echo $OVen_list->RowIndex ?>_ProCodi" name="x<?php echo $OVen_list->RowIndex ?>_ProCodi"<?php echo $OVen->ProCodi->EditAttributes() ?>>
<?php
if (is_array($OVen->ProCodi->EditValue)) {
	$arwrk = $OVen->ProCodi->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($OVen->ProCodi->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $OVen->ProCodi->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($OVen->ProCodi->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($OVen->ProCodi->CurrentValue) ?>" selected><?php echo $OVen->ProCodi->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $OVen->ProCodi->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT \"ProdCodi\", \"CprCodi\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"Prod\"";
$sWhereWrk = "";
$OVen->ProCodi->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$OVen->ProCodi->LookupFilters += array("f0" => "\"ProdCodi\" = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$OVen->Lookup_Selecting($OVen->ProCodi, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $OVen->ProCodi->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $OVen_list->RowIndex ?>_ProCodi" id="s_x<?php echo $OVen_list->RowIndex ?>_ProCodi" value="<?php echo $OVen->ProCodi->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php if ($OVen->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $OVen_list->RowCnt ?>_OVen_ProCodi" class="OVen_ProCodi">
<span<?php echo $OVen->ProCodi->ViewAttributes() ?>>
<?php echo $OVen->ProCodi->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($OVen->OvePIni->Visible) { // OvePIni ?>
		<td data-name="OvePIni"<?php echo $OVen->OvePIni->CellAttributes() ?>>
<?php if ($OVen->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $OVen_list->RowCnt ?>_OVen_OvePIni" class="form-group OVen_OvePIni">
<input type="text" data-table="OVen" data-field="x_OvePIni" name="x<?php echo $OVen_list->RowIndex ?>_OvePIni" id="x<?php echo $OVen_list->RowIndex ?>_OvePIni" size="30" placeholder="<?php echo ew_HtmlEncode($OVen->OvePIni->getPlaceHolder()) ?>" value="<?php echo $OVen->OvePIni->EditValue ?>"<?php echo $OVen->OvePIni->EditAttributes() ?>>
</span>
<input type="hidden" data-table="OVen" data-field="x_OvePIni" name="o<?php echo $OVen_list->RowIndex ?>_OvePIni" id="o<?php echo $OVen_list->RowIndex ?>_OvePIni" value="<?php echo ew_HtmlEncode($OVen->OvePIni->OldValue) ?>">
<?php } ?>
<?php if ($OVen->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $OVen_list->RowCnt ?>_OVen_OvePIni" class="form-group OVen_OvePIni">
<input type="text" data-table="OVen" data-field="x_OvePIni" name="x<?php echo $OVen_list->RowIndex ?>_OvePIni" id="x<?php echo $OVen_list->RowIndex ?>_OvePIni" size="30" placeholder="<?php echo ew_HtmlEncode($OVen->OvePIni->getPlaceHolder()) ?>" value="<?php echo $OVen->OvePIni->EditValue ?>"<?php echo $OVen->OvePIni->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($OVen->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $OVen_list->RowCnt ?>_OVen_OvePIni" class="OVen_OvePIni">
<span<?php echo $OVen->OvePIni->ViewAttributes() ?>>
<?php echo $OVen->OvePIni->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($OVen->OveCPro->Visible) { // OveCPro ?>
		<td data-name="OveCPro"<?php echo $OVen->OveCPro->CellAttributes() ?>>
<?php if ($OVen->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $OVen_list->RowCnt ?>_OVen_OveCPro" class="form-group OVen_OveCPro">
<input type="text" data-table="OVen" data-field="x_OveCPro" name="x<?php echo $OVen_list->RowIndex ?>_OveCPro" id="x<?php echo $OVen_list->RowIndex ?>_OveCPro" size="30" placeholder="<?php echo ew_HtmlEncode($OVen->OveCPro->getPlaceHolder()) ?>" value="<?php echo $OVen->OveCPro->EditValue ?>"<?php echo $OVen->OveCPro->EditAttributes() ?>>
</span>
<input type="hidden" data-table="OVen" data-field="x_OveCPro" name="o<?php echo $OVen_list->RowIndex ?>_OveCPro" id="o<?php echo $OVen_list->RowIndex ?>_OveCPro" value="<?php echo ew_HtmlEncode($OVen->OveCPro->OldValue) ?>">
<?php } ?>
<?php if ($OVen->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $OVen_list->RowCnt ?>_OVen_OveCPro" class="form-group OVen_OveCPro">
<input type="text" data-table="OVen" data-field="x_OveCPro" name="x<?php echo $OVen_list->RowIndex ?>_OveCPro" id="x<?php echo $OVen_list->RowIndex ?>_OveCPro" size="30" placeholder="<?php echo ew_HtmlEncode($OVen->OveCPro->getPlaceHolder()) ?>" value="<?php echo $OVen->OveCPro->EditValue ?>"<?php echo $OVen->OveCPro->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($OVen->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $OVen_list->RowCnt ?>_OVen_OveCPro" class="OVen_OveCPro">
<span<?php echo $OVen->OveCPro->ViewAttributes() ?>>
<?php echo $OVen->OveCPro->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($OVen->OveUsua->Visible) { // OveUsua ?>
		<td data-name="OveUsua"<?php echo $OVen->OveUsua->CellAttributes() ?>>
<?php if ($OVen->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $OVen_list->RowCnt ?>_OVen_OveUsua" class="form-group OVen_OveUsua">
<input type="text" data-table="OVen" data-field="x_OveUsua" name="x<?php echo $OVen_list->RowIndex ?>_OveUsua" id="x<?php echo $OVen_list->RowIndex ?>_OveUsua" size="30" placeholder="<?php echo ew_HtmlEncode($OVen->OveUsua->getPlaceHolder()) ?>" value="<?php echo $OVen->OveUsua->EditValue ?>"<?php echo $OVen->OveUsua->EditAttributes() ?>>
</span>
<input type="hidden" data-table="OVen" data-field="x_OveUsua" name="o<?php echo $OVen_list->RowIndex ?>_OveUsua" id="o<?php echo $OVen_list->RowIndex ?>_OveUsua" value="<?php echo ew_HtmlEncode($OVen->OveUsua->OldValue) ?>">
<?php } ?>
<?php if ($OVen->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $OVen_list->RowCnt ?>_OVen_OveUsua" class="form-group OVen_OveUsua">
<input type="text" data-table="OVen" data-field="x_OveUsua" name="x<?php echo $OVen_list->RowIndex ?>_OveUsua" id="x<?php echo $OVen_list->RowIndex ?>_OveUsua" size="30" placeholder="<?php echo ew_HtmlEncode($OVen->OveUsua->getPlaceHolder()) ?>" value="<?php echo $OVen->OveUsua->EditValue ?>"<?php echo $OVen->OveUsua->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($OVen->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $OVen_list->RowCnt ?>_OVen_OveUsua" class="OVen_OveUsua">
<span<?php echo $OVen->OveUsua->ViewAttributes() ?>>
<?php echo $OVen->OveUsua->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($OVen->OveFCre->Visible) { // OveFCre ?>
		<td data-name="OveFCre"<?php echo $OVen->OveFCre->CellAttributes() ?>>
<?php if ($OVen->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $OVen_list->RowCnt ?>_OVen_OveFCre" class="form-group OVen_OveFCre">
<input type="text" data-table="OVen" data-field="x_OveFCre" data-format="7" name="x<?php echo $OVen_list->RowIndex ?>_OveFCre" id="x<?php echo $OVen_list->RowIndex ?>_OveFCre" placeholder="<?php echo ew_HtmlEncode($OVen->OveFCre->getPlaceHolder()) ?>" value="<?php echo $OVen->OveFCre->EditValue ?>"<?php echo $OVen->OveFCre->EditAttributes() ?>>
</span>
<input type="hidden" data-table="OVen" data-field="x_OveFCre" name="o<?php echo $OVen_list->RowIndex ?>_OveFCre" id="o<?php echo $OVen_list->RowIndex ?>_OveFCre" value="<?php echo ew_HtmlEncode($OVen->OveFCre->OldValue) ?>">
<?php } ?>
<?php if ($OVen->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $OVen_list->RowCnt ?>_OVen_OveFCre" class="form-group OVen_OveFCre">
<input type="text" data-table="OVen" data-field="x_OveFCre" data-format="7" name="x<?php echo $OVen_list->RowIndex ?>_OveFCre" id="x<?php echo $OVen_list->RowIndex ?>_OveFCre" placeholder="<?php echo ew_HtmlEncode($OVen->OveFCre->getPlaceHolder()) ?>" value="<?php echo $OVen->OveFCre->EditValue ?>"<?php echo $OVen->OveFCre->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($OVen->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $OVen_list->RowCnt ?>_OVen_OveFCre" class="OVen_OveFCre">
<span<?php echo $OVen->OveFCre->ViewAttributes() ?>>
<?php echo $OVen->OveFCre->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($OVen->OveCVen->Visible) { // OveCVen ?>
		<td data-name="OveCVen"<?php echo $OVen->OveCVen->CellAttributes() ?>>
<?php if ($OVen->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $OVen_list->RowCnt ?>_OVen_OveCVen" class="form-group OVen_OveCVen">
<input type="text" data-table="OVen" data-field="x_OveCVen" name="x<?php echo $OVen_list->RowIndex ?>_OveCVen" id="x<?php echo $OVen_list->RowIndex ?>_OveCVen" size="30" placeholder="<?php echo ew_HtmlEncode($OVen->OveCVen->getPlaceHolder()) ?>" value="<?php echo $OVen->OveCVen->EditValue ?>"<?php echo $OVen->OveCVen->EditAttributes() ?>>
</span>
<input type="hidden" data-table="OVen" data-field="x_OveCVen" name="o<?php echo $OVen_list->RowIndex ?>_OveCVen" id="o<?php echo $OVen_list->RowIndex ?>_OveCVen" value="<?php echo ew_HtmlEncode($OVen->OveCVen->OldValue) ?>">
<?php } ?>
<?php if ($OVen->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $OVen_list->RowCnt ?>_OVen_OveCVen" class="form-group OVen_OveCVen">
<input type="text" data-table="OVen" data-field="x_OveCVen" name="x<?php echo $OVen_list->RowIndex ?>_OveCVen" id="x<?php echo $OVen_list->RowIndex ?>_OveCVen" size="30" placeholder="<?php echo ew_HtmlEncode($OVen->OveCVen->getPlaceHolder()) ?>" value="<?php echo $OVen->OveCVen->EditValue ?>"<?php echo $OVen->OveCVen->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($OVen->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $OVen_list->RowCnt ?>_OVen_OveCVen" class="OVen_OveCVen">
<span<?php echo $OVen->OveCVen->ViewAttributes() ?>>
<?php echo $OVen->OveCVen->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($OVen->CliCodi->Visible) { // CliCodi ?>
		<td data-name="CliCodi"<?php echo $OVen->CliCodi->CellAttributes() ?>>
<?php if ($OVen->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $OVen_list->RowCnt ?>_OVen_CliCodi" class="form-group OVen_CliCodi">
<select data-table="OVen" data-field="x_CliCodi" data-value-separator="<?php echo ew_HtmlEncode(is_array($OVen->CliCodi->DisplayValueSeparator) ? json_encode($OVen->CliCodi->DisplayValueSeparator) : $OVen->CliCodi->DisplayValueSeparator) ?>" id="x<?php echo $OVen_list->RowIndex ?>_CliCodi" name="x<?php echo $OVen_list->RowIndex ?>_CliCodi"<?php echo $OVen->CliCodi->EditAttributes() ?>>
<?php
if (is_array($OVen->CliCodi->EditValue)) {
	$arwrk = $OVen->CliCodi->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($OVen->CliCodi->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $OVen->CliCodi->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($OVen->CliCodi->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($OVen->CliCodi->CurrentValue) ?>" selected><?php echo $OVen->CliCodi->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $OVen->CliCodi->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT \"CliCodi\", \"CliNomb\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"Clie\"";
$sWhereWrk = "";
$OVen->CliCodi->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$OVen->CliCodi->LookupFilters += array("f0" => "\"CliCodi\" = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$OVen->Lookup_Selecting($OVen->CliCodi, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $OVen->CliCodi->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $OVen_list->RowIndex ?>_CliCodi" id="s_x<?php echo $OVen_list->RowIndex ?>_CliCodi" value="<?php echo $OVen->CliCodi->LookupFilterQuery() ?>">
</span>
<input type="hidden" data-table="OVen" data-field="x_CliCodi" name="o<?php echo $OVen_list->RowIndex ?>_CliCodi" id="o<?php echo $OVen_list->RowIndex ?>_CliCodi" value="<?php echo ew_HtmlEncode($OVen->CliCodi->OldValue) ?>">
<?php } ?>
<?php if ($OVen->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $OVen_list->RowCnt ?>_OVen_CliCodi" class="form-group OVen_CliCodi">
<select data-table="OVen" data-field="x_CliCodi" data-value-separator="<?php echo ew_HtmlEncode(is_array($OVen->CliCodi->DisplayValueSeparator) ? json_encode($OVen->CliCodi->DisplayValueSeparator) : $OVen->CliCodi->DisplayValueSeparator) ?>" id="x<?php echo $OVen_list->RowIndex ?>_CliCodi" name="x<?php echo $OVen_list->RowIndex ?>_CliCodi"<?php echo $OVen->CliCodi->EditAttributes() ?>>
<?php
if (is_array($OVen->CliCodi->EditValue)) {
	$arwrk = $OVen->CliCodi->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($OVen->CliCodi->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $OVen->CliCodi->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($OVen->CliCodi->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($OVen->CliCodi->CurrentValue) ?>" selected><?php echo $OVen->CliCodi->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $OVen->CliCodi->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT \"CliCodi\", \"CliNomb\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"Clie\"";
$sWhereWrk = "";
$OVen->CliCodi->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$OVen->CliCodi->LookupFilters += array("f0" => "\"CliCodi\" = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$OVen->Lookup_Selecting($OVen->CliCodi, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $OVen->CliCodi->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $OVen_list->RowIndex ?>_CliCodi" id="s_x<?php echo $OVen_list->RowIndex ?>_CliCodi" value="<?php echo $OVen->CliCodi->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php if ($OVen->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $OVen_list->RowCnt ?>_OVen_CliCodi" class="OVen_CliCodi">
<span<?php echo $OVen->CliCodi->ViewAttributes() ?>>
<?php echo $OVen->CliCodi->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
<?php

// Render list options (body, right)
$OVen_list->ListOptions->Render("body", "right", $OVen_list->RowCnt);
?>
	</tr>
<?php if ($OVen->RowType == EW_ROWTYPE_ADD || $OVen->RowType == EW_ROWTYPE_EDIT) { ?>
<script type="text/javascript">
fOVenlist.UpdateOpts(<?php echo $OVen_list->RowIndex ?>);
</script>
<?php } ?>
<?php
	}
	} // End delete row checking
	if ($OVen->CurrentAction <> "gridadd")
		if (!$OVen_list->Recordset->EOF) $OVen_list->Recordset->MoveNext();
}
?>
<?php
	if ($OVen->CurrentAction == "gridadd" || $OVen->CurrentAction == "gridedit") {
		$OVen_list->RowIndex = '$rowindex$';
		$OVen_list->LoadDefaultValues();

		// Set row properties
		$OVen->ResetAttrs();
		$OVen->RowAttrs = array_merge($OVen->RowAttrs, array('data-rowindex'=>$OVen_list->RowIndex, 'id'=>'r0_OVen', 'data-rowtype'=>EW_ROWTYPE_ADD));
		ew_AppendClass($OVen->RowAttrs["class"], "ewTemplate");
		$OVen->RowType = EW_ROWTYPE_ADD;

		// Render row
		$OVen_list->RenderRow();

		// Render list options
		$OVen_list->RenderListOptions();
		$OVen_list->StartRowCnt = 0;
?>
	<tr<?php echo $OVen->RowAttributes() ?>>
<?php

// Render list options (body, left)
$OVen_list->ListOptions->Render("body", "left", $OVen_list->RowIndex);
?>
	<?php if ($OVen->OveCodi->Visible) { // OveCodi ?>
		<td data-name="OveCodi">
<input type="hidden" data-table="OVen" data-field="x_OveCodi" name="o<?php echo $OVen_list->RowIndex ?>_OveCodi" id="o<?php echo $OVen_list->RowIndex ?>_OveCodi" value="<?php echo ew_HtmlEncode($OVen->OveCodi->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($OVen->ProCodi->Visible) { // ProCodi ?>
		<td data-name="ProCodi">
<span id="el$rowindex$_OVen_ProCodi" class="form-group OVen_ProCodi">
<select data-table="OVen" data-field="x_ProCodi" data-value-separator="<?php echo ew_HtmlEncode(is_array($OVen->ProCodi->DisplayValueSeparator) ? json_encode($OVen->ProCodi->DisplayValueSeparator) : $OVen->ProCodi->DisplayValueSeparator) ?>" id="x<?php echo $OVen_list->RowIndex ?>_ProCodi" name="x<?php echo $OVen_list->RowIndex ?>_ProCodi"<?php echo $OVen->ProCodi->EditAttributes() ?>>
<?php
if (is_array($OVen->ProCodi->EditValue)) {
	$arwrk = $OVen->ProCodi->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($OVen->ProCodi->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $OVen->ProCodi->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($OVen->ProCodi->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($OVen->ProCodi->CurrentValue) ?>" selected><?php echo $OVen->ProCodi->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $OVen->ProCodi->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT \"ProdCodi\", \"CprCodi\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"Prod\"";
$sWhereWrk = "";
$OVen->ProCodi->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$OVen->ProCodi->LookupFilters += array("f0" => "\"ProdCodi\" = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$OVen->Lookup_Selecting($OVen->ProCodi, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $OVen->ProCodi->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $OVen_list->RowIndex ?>_ProCodi" id="s_x<?php echo $OVen_list->RowIndex ?>_ProCodi" value="<?php echo $OVen->ProCodi->LookupFilterQuery() ?>">
</span>
<input type="hidden" data-table="OVen" data-field="x_ProCodi" name="o<?php echo $OVen_list->RowIndex ?>_ProCodi" id="o<?php echo $OVen_list->RowIndex ?>_ProCodi" value="<?php echo ew_HtmlEncode($OVen->ProCodi->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($OVen->OvePIni->Visible) { // OvePIni ?>
		<td data-name="OvePIni">
<span id="el$rowindex$_OVen_OvePIni" class="form-group OVen_OvePIni">
<input type="text" data-table="OVen" data-field="x_OvePIni" name="x<?php echo $OVen_list->RowIndex ?>_OvePIni" id="x<?php echo $OVen_list->RowIndex ?>_OvePIni" size="30" placeholder="<?php echo ew_HtmlEncode($OVen->OvePIni->getPlaceHolder()) ?>" value="<?php echo $OVen->OvePIni->EditValue ?>"<?php echo $OVen->OvePIni->EditAttributes() ?>>
</span>
<input type="hidden" data-table="OVen" data-field="x_OvePIni" name="o<?php echo $OVen_list->RowIndex ?>_OvePIni" id="o<?php echo $OVen_list->RowIndex ?>_OvePIni" value="<?php echo ew_HtmlEncode($OVen->OvePIni->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($OVen->OveCPro->Visible) { // OveCPro ?>
		<td data-name="OveCPro">
<span id="el$rowindex$_OVen_OveCPro" class="form-group OVen_OveCPro">
<input type="text" data-table="OVen" data-field="x_OveCPro" name="x<?php echo $OVen_list->RowIndex ?>_OveCPro" id="x<?php echo $OVen_list->RowIndex ?>_OveCPro" size="30" placeholder="<?php echo ew_HtmlEncode($OVen->OveCPro->getPlaceHolder()) ?>" value="<?php echo $OVen->OveCPro->EditValue ?>"<?php echo $OVen->OveCPro->EditAttributes() ?>>
</span>
<input type="hidden" data-table="OVen" data-field="x_OveCPro" name="o<?php echo $OVen_list->RowIndex ?>_OveCPro" id="o<?php echo $OVen_list->RowIndex ?>_OveCPro" value="<?php echo ew_HtmlEncode($OVen->OveCPro->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($OVen->OveUsua->Visible) { // OveUsua ?>
		<td data-name="OveUsua">
<span id="el$rowindex$_OVen_OveUsua" class="form-group OVen_OveUsua">
<input type="text" data-table="OVen" data-field="x_OveUsua" name="x<?php echo $OVen_list->RowIndex ?>_OveUsua" id="x<?php echo $OVen_list->RowIndex ?>_OveUsua" size="30" placeholder="<?php echo ew_HtmlEncode($OVen->OveUsua->getPlaceHolder()) ?>" value="<?php echo $OVen->OveUsua->EditValue ?>"<?php echo $OVen->OveUsua->EditAttributes() ?>>
</span>
<input type="hidden" data-table="OVen" data-field="x_OveUsua" name="o<?php echo $OVen_list->RowIndex ?>_OveUsua" id="o<?php echo $OVen_list->RowIndex ?>_OveUsua" value="<?php echo ew_HtmlEncode($OVen->OveUsua->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($OVen->OveFCre->Visible) { // OveFCre ?>
		<td data-name="OveFCre">
<span id="el$rowindex$_OVen_OveFCre" class="form-group OVen_OveFCre">
<input type="text" data-table="OVen" data-field="x_OveFCre" data-format="7" name="x<?php echo $OVen_list->RowIndex ?>_OveFCre" id="x<?php echo $OVen_list->RowIndex ?>_OveFCre" placeholder="<?php echo ew_HtmlEncode($OVen->OveFCre->getPlaceHolder()) ?>" value="<?php echo $OVen->OveFCre->EditValue ?>"<?php echo $OVen->OveFCre->EditAttributes() ?>>
</span>
<input type="hidden" data-table="OVen" data-field="x_OveFCre" name="o<?php echo $OVen_list->RowIndex ?>_OveFCre" id="o<?php echo $OVen_list->RowIndex ?>_OveFCre" value="<?php echo ew_HtmlEncode($OVen->OveFCre->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($OVen->OveCVen->Visible) { // OveCVen ?>
		<td data-name="OveCVen">
<span id="el$rowindex$_OVen_OveCVen" class="form-group OVen_OveCVen">
<input type="text" data-table="OVen" data-field="x_OveCVen" name="x<?php echo $OVen_list->RowIndex ?>_OveCVen" id="x<?php echo $OVen_list->RowIndex ?>_OveCVen" size="30" placeholder="<?php echo ew_HtmlEncode($OVen->OveCVen->getPlaceHolder()) ?>" value="<?php echo $OVen->OveCVen->EditValue ?>"<?php echo $OVen->OveCVen->EditAttributes() ?>>
</span>
<input type="hidden" data-table="OVen" data-field="x_OveCVen" name="o<?php echo $OVen_list->RowIndex ?>_OveCVen" id="o<?php echo $OVen_list->RowIndex ?>_OveCVen" value="<?php echo ew_HtmlEncode($OVen->OveCVen->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($OVen->CliCodi->Visible) { // CliCodi ?>
		<td data-name="CliCodi">
<span id="el$rowindex$_OVen_CliCodi" class="form-group OVen_CliCodi">
<select data-table="OVen" data-field="x_CliCodi" data-value-separator="<?php echo ew_HtmlEncode(is_array($OVen->CliCodi->DisplayValueSeparator) ? json_encode($OVen->CliCodi->DisplayValueSeparator) : $OVen->CliCodi->DisplayValueSeparator) ?>" id="x<?php echo $OVen_list->RowIndex ?>_CliCodi" name="x<?php echo $OVen_list->RowIndex ?>_CliCodi"<?php echo $OVen->CliCodi->EditAttributes() ?>>
<?php
if (is_array($OVen->CliCodi->EditValue)) {
	$arwrk = $OVen->CliCodi->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($OVen->CliCodi->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $OVen->CliCodi->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($OVen->CliCodi->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($OVen->CliCodi->CurrentValue) ?>" selected><?php echo $OVen->CliCodi->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $OVen->CliCodi->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT \"CliCodi\", \"CliNomb\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"Clie\"";
$sWhereWrk = "";
$OVen->CliCodi->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$OVen->CliCodi->LookupFilters += array("f0" => "\"CliCodi\" = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$OVen->Lookup_Selecting($OVen->CliCodi, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $OVen->CliCodi->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $OVen_list->RowIndex ?>_CliCodi" id="s_x<?php echo $OVen_list->RowIndex ?>_CliCodi" value="<?php echo $OVen->CliCodi->LookupFilterQuery() ?>">
</span>
<input type="hidden" data-table="OVen" data-field="x_CliCodi" name="o<?php echo $OVen_list->RowIndex ?>_CliCodi" id="o<?php echo $OVen_list->RowIndex ?>_CliCodi" value="<?php echo ew_HtmlEncode($OVen->CliCodi->OldValue) ?>">
</td>
	<?php } ?>
<?php

// Render list options (body, right)
$OVen_list->ListOptions->Render("body", "right", $OVen_list->RowCnt);
?>
<script type="text/javascript">
fOVenlist.UpdateOpts(<?php echo $OVen_list->RowIndex ?>);
</script>
	</tr>
<?php
}
?>
</tbody>
</table>
<?php } ?>
<?php if ($OVen->CurrentAction == "gridadd") { ?>
<input type="hidden" name="a_list" id="a_list" value="gridinsert">
<input type="hidden" name="<?php echo $OVen_list->FormKeyCountName ?>" id="<?php echo $OVen_list->FormKeyCountName ?>" value="<?php echo $OVen_list->KeyCount ?>">
<?php echo $OVen_list->MultiSelectKey ?>
<?php } ?>
<?php if ($OVen->CurrentAction == "gridedit") { ?>
<input type="hidden" name="a_list" id="a_list" value="gridupdate">
<input type="hidden" name="<?php echo $OVen_list->FormKeyCountName ?>" id="<?php echo $OVen_list->FormKeyCountName ?>" value="<?php echo $OVen_list->KeyCount ?>">
<?php echo $OVen_list->MultiSelectKey ?>
<?php } ?>
<?php if ($OVen->CurrentAction == "") { ?>
<input type="hidden" name="a_list" id="a_list" value="">
<?php } ?>
</div>
</form>
<?php

// Close recordset
if ($OVen_list->Recordset)
	$OVen_list->Recordset->Close();
?>
<div class="panel-footer ewGridLowerPanel">
<?php if ($OVen->CurrentAction <> "gridadd" && $OVen->CurrentAction <> "gridedit") { ?>
<form name="ewPagerForm" class="ewForm form-inline ewPagerForm" action="<?php echo ew_CurrentPage() ?>">
<?php if (!isset($OVen_list->Pager)) $OVen_list->Pager = new cPrevNextPager($OVen_list->StartRec, $OVen_list->DisplayRecs, $OVen_list->TotalRecs) ?>
<?php if ($OVen_list->Pager->RecordCount > 0) { ?>
<div class="ewPager">
<span><?php echo $Language->Phrase("Page") ?>&nbsp;</span>
<div class="ewPrevNext"><div class="input-group">
<div class="input-group-btn">
<!--first page button-->
	<?php if ($OVen_list->Pager->FirstButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerFirst") ?>" href="<?php echo $OVen_list->PageUrl() ?>start=<?php echo $OVen_list->Pager->FirstButton->Start ?>"><span class="icon-first ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerFirst") ?>"><span class="icon-first ewIcon"></span></a>
	<?php } ?>
<!--previous page button-->
	<?php if ($OVen_list->Pager->PrevButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerPrevious") ?>" href="<?php echo $OVen_list->PageUrl() ?>start=<?php echo $OVen_list->Pager->PrevButton->Start ?>"><span class="icon-prev ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerPrevious") ?>"><span class="icon-prev ewIcon"></span></a>
	<?php } ?>
</div>
<!--current page number-->
	<input class="form-control input-sm" type="text" name="<?php echo EW_TABLE_PAGE_NO ?>" value="<?php echo $OVen_list->Pager->CurrentPage ?>">
<div class="input-group-btn">
<!--next page button-->
	<?php if ($OVen_list->Pager->NextButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerNext") ?>" href="<?php echo $OVen_list->PageUrl() ?>start=<?php echo $OVen_list->Pager->NextButton->Start ?>"><span class="icon-next ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerNext") ?>"><span class="icon-next ewIcon"></span></a>
	<?php } ?>
<!--last page button-->
	<?php if ($OVen_list->Pager->LastButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerLast") ?>" href="<?php echo $OVen_list->PageUrl() ?>start=<?php echo $OVen_list->Pager->LastButton->Start ?>"><span class="icon-last ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerLast") ?>"><span class="icon-last ewIcon"></span></a>
	<?php } ?>
</div>
</div>
</div>
<span>&nbsp;<?php echo $Language->Phrase("of") ?>&nbsp;<?php echo $OVen_list->Pager->PageCount ?></span>
</div>
<div class="ewPager ewRec">
	<span><?php echo $Language->Phrase("Record") ?>&nbsp;<?php echo $OVen_list->Pager->FromIndex ?>&nbsp;<?php echo $Language->Phrase("To") ?>&nbsp;<?php echo $OVen_list->Pager->ToIndex ?>&nbsp;<?php echo $Language->Phrase("Of") ?>&nbsp;<?php echo $OVen_list->Pager->RecordCount ?></span>
</div>
<?php } ?>
</form>
<?php } ?>
<div class="ewListOtherOptions">
<?php
	foreach ($OVen_list->OtherOptions as &$option)
		$option->Render("body", "bottom");
?>
</div>
<div class="clearfix"></div>
</div>
</div>
<?php } ?>
<?php if ($OVen_list->TotalRecs == 0 && $OVen->CurrentAction == "") { // Show other options ?>
<div class="ewListOtherOptions">
<?php
	foreach ($OVen_list->OtherOptions as &$option) {
		$option->ButtonClass = "";
		$option->Render("body", "");
	}
?>
</div>
<div class="clearfix"></div>
<?php } ?>
<script type="text/javascript">
fOVenlistsrch.Init();
fOVenlistsrch.FilterList = <?php echo $OVen_list->GetFilterList() ?>;
fOVenlist.Init();
</script>
<?php
$OVen_list->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$OVen_list->Page_Terminate();
?>
