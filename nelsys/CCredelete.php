<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "CCreinfo.php" ?>
<?php include_once "Usuainfo.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$CCre_delete = NULL; // Initialize page object first

class cCCre_delete extends cCCre {

	// Page ID
	var $PageID = 'delete';

	// Project ID
	var $ProjectID = "{04439FF7-B43F-460F-8514-F71C8FF9E679}";

	// Table name
	var $TableName = 'CCre';

	// Page object name
	var $PageObjName = 'CCre_delete';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (CCre)
		if (!isset($GLOBALS["CCre"]) || get_class($GLOBALS["CCre"]) == "cCCre") {
			$GLOBALS["CCre"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["CCre"];
		}

		// Table object (Usua)
		if (!isset($GLOBALS['Usua'])) $GLOBALS['Usua'] = new cUsua();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'delete', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'CCre', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (Usua)
		if (!isset($UserTable)) {
			$UserTable = new cUsua();
			$UserTableConn = Conn($UserTable->DBID);
		}
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanDelete()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage($Language->Phrase("NoPermission")); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("CCrelist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action
		$this->CcrCodi->Visible = !$this->IsAdd() && !$this->IsCopy() && !$this->IsGridAdd();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $CCre;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($CCre);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $DbMasterFilter = "";
	var $DbDetailFilter = "";
	var $StartRec;
	var $TotalRecs = 0;
	var $RecCnt;
	var $RecKeys = array();
	var $Recordset;
	var $StartRowCnt = 1;
	var $RowCnt = 0;

	//
	// Page main
	//
	function Page_Main() {
		global $Language;

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Load key parameters
		$this->RecKeys = $this->GetRecordKeys(); // Load record keys
		$sFilter = $this->GetKeyFilter();
		if ($sFilter == "")
			$this->Page_Terminate("CCrelist.php"); // Prevent SQL injection, return to list

		// Set up filter (SQL WHHERE clause) and get return SQL
		// SQL constructor in CCre class, CCreinfo.php

		$this->CurrentFilter = $sFilter;

		// Get action
		if (@$_POST["a_delete"] <> "") {
			$this->CurrentAction = $_POST["a_delete"];
		} else {
			$this->CurrentAction = "I"; // Display record
		}
		switch ($this->CurrentAction) {
			case "D": // Delete
				$this->SendEmail = TRUE; // Send email on delete success
				if ($this->DeleteRows()) { // Delete rows
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage($Language->Phrase("DeleteSuccess")); // Set up success message
					$this->Page_Terminate($this->getReturnUrl()); // Return to caller
				}
		}
	}

	// Load recordset
	function LoadRecordset($offset = -1, $rowcnt = -1) {

		// Load List page SQL
		$sSql = $this->SelectSQL();
		$conn = &$this->Connection();

		// Load recordset
		$dbtype = ew_GetConnectionType($this->DBID);
		if ($this->UseSelectLimit) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			if ($dbtype == "MSSQL") {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset, array("_hasOrderBy" => trim($this->getOrderBy()) || trim($this->getSessionOrderBy())));
			} else {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset);
			}
			$conn->raiseErrorFn = '';
		} else {
			$rs = ew_LoadRecordset($sSql, $conn);
		}

		// Call Recordset Selected event
		$this->Recordset_Selected($rs);
		return $rs;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->CcrCodi->setDbValue($rs->fields('CcrCodi'));
		$this->VcrCodi->setDbValue($rs->fields('VcrCodi'));
		$this->CcrNCuo->setDbValue($rs->fields('CcrNCuo'));
		$this->CcrFPag->setDbValue($rs->fields('CcrFPag'));
		$this->CcrMont->setDbValue($rs->fields('CcrMont'));
		$this->CcrMora->setDbValue($rs->fields('CcrMora'));
		$this->CcrDAtr->setDbValue($rs->fields('CcrDAtr'));
		$this->CcrEsta->setDbValue($rs->fields('CcrEsta'));
		$this->CcrAnho->setDbValue($rs->fields('CcrAnho'));
		$this->CcrMes->setDbValue($rs->fields('CcrMes'));
		$this->CcrConc->setDbValue($rs->fields('CcrConc'));
		$this->CcrUsua->setDbValue($rs->fields('CcrUsua'));
		$this->CcrFCre->setDbValue($rs->fields('CcrFCre'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->CcrCodi->DbValue = $row['CcrCodi'];
		$this->VcrCodi->DbValue = $row['VcrCodi'];
		$this->CcrNCuo->DbValue = $row['CcrNCuo'];
		$this->CcrFPag->DbValue = $row['CcrFPag'];
		$this->CcrMont->DbValue = $row['CcrMont'];
		$this->CcrMora->DbValue = $row['CcrMora'];
		$this->CcrDAtr->DbValue = $row['CcrDAtr'];
		$this->CcrEsta->DbValue = $row['CcrEsta'];
		$this->CcrAnho->DbValue = $row['CcrAnho'];
		$this->CcrMes->DbValue = $row['CcrMes'];
		$this->CcrConc->DbValue = $row['CcrConc'];
		$this->CcrUsua->DbValue = $row['CcrUsua'];
		$this->CcrFCre->DbValue = $row['CcrFCre'];
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		// Convert decimal values if posted back

		if ($this->CcrMont->FormValue == $this->CcrMont->CurrentValue && is_numeric(ew_StrToFloat($this->CcrMont->CurrentValue)))
			$this->CcrMont->CurrentValue = ew_StrToFloat($this->CcrMont->CurrentValue);

		// Convert decimal values if posted back
		if ($this->CcrMora->FormValue == $this->CcrMora->CurrentValue && is_numeric(ew_StrToFloat($this->CcrMora->CurrentValue)))
			$this->CcrMora->CurrentValue = ew_StrToFloat($this->CcrMora->CurrentValue);

		// Call Row_Rendering event
		$this->Row_Rendering();

		// Common render codes for all row types
		// CcrCodi
		// VcrCodi
		// CcrNCuo
		// CcrFPag
		// CcrMont
		// CcrMora
		// CcrDAtr
		// CcrEsta
		// CcrAnho
		// CcrMes
		// CcrConc
		// CcrUsua
		// CcrFCre

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// CcrCodi
		$this->CcrCodi->ViewValue = $this->CcrCodi->CurrentValue;
		$this->CcrCodi->ViewCustomAttributes = "";

		// VcrCodi
		if (strval($this->VcrCodi->CurrentValue) <> "") {
			$sFilterWrk = "\"VcrCodi\"" . ew_SearchString("=", $this->VcrCodi->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT \"VcrCodi\", \"VcrCodi\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"VCre\"";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->VcrCodi, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->VcrCodi->ViewValue = $this->VcrCodi->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->VcrCodi->ViewValue = $this->VcrCodi->CurrentValue;
			}
		} else {
			$this->VcrCodi->ViewValue = NULL;
		}
		$this->VcrCodi->ViewCustomAttributes = "";

		// CcrNCuo
		$this->CcrNCuo->ViewValue = $this->CcrNCuo->CurrentValue;
		$this->CcrNCuo->ViewCustomAttributes = "";

		// CcrFPag
		$this->CcrFPag->ViewValue = $this->CcrFPag->CurrentValue;
		$this->CcrFPag->ViewValue = ew_FormatDateTime($this->CcrFPag->ViewValue, 7);
		$this->CcrFPag->ViewCustomAttributes = "";

		// CcrMont
		$this->CcrMont->ViewValue = $this->CcrMont->CurrentValue;
		$this->CcrMont->ViewCustomAttributes = "";

		// CcrMora
		$this->CcrMora->ViewValue = $this->CcrMora->CurrentValue;
		$this->CcrMora->ViewCustomAttributes = "";

		// CcrDAtr
		$this->CcrDAtr->ViewValue = $this->CcrDAtr->CurrentValue;
		$this->CcrDAtr->ViewCustomAttributes = "";

		// CcrEsta
		$this->CcrEsta->ViewValue = $this->CcrEsta->CurrentValue;
		$this->CcrEsta->ViewCustomAttributes = "";

		// CcrAnho
		$this->CcrAnho->ViewValue = $this->CcrAnho->CurrentValue;
		$this->CcrAnho->ViewCustomAttributes = "";

		// CcrMes
		$this->CcrMes->ViewValue = $this->CcrMes->CurrentValue;
		$this->CcrMes->ViewCustomAttributes = "";

		// CcrConc
		$this->CcrConc->ViewValue = $this->CcrConc->CurrentValue;
		$this->CcrConc->ViewCustomAttributes = "";

		// CcrUsua
		$this->CcrUsua->ViewValue = $this->CcrUsua->CurrentValue;
		$this->CcrUsua->ViewCustomAttributes = "";

		// CcrFCre
		$this->CcrFCre->ViewValue = $this->CcrFCre->CurrentValue;
		$this->CcrFCre->ViewValue = ew_FormatDateTime($this->CcrFCre->ViewValue, 7);
		$this->CcrFCre->ViewCustomAttributes = "";

			// CcrCodi
			$this->CcrCodi->LinkCustomAttributes = "";
			$this->CcrCodi->HrefValue = "";
			$this->CcrCodi->TooltipValue = "";

			// VcrCodi
			$this->VcrCodi->LinkCustomAttributes = "";
			$this->VcrCodi->HrefValue = "";
			$this->VcrCodi->TooltipValue = "";

			// CcrNCuo
			$this->CcrNCuo->LinkCustomAttributes = "";
			$this->CcrNCuo->HrefValue = "";
			$this->CcrNCuo->TooltipValue = "";

			// CcrFPag
			$this->CcrFPag->LinkCustomAttributes = "";
			$this->CcrFPag->HrefValue = "";
			$this->CcrFPag->TooltipValue = "";

			// CcrMont
			$this->CcrMont->LinkCustomAttributes = "";
			$this->CcrMont->HrefValue = "";
			$this->CcrMont->TooltipValue = "";

			// CcrMora
			$this->CcrMora->LinkCustomAttributes = "";
			$this->CcrMora->HrefValue = "";
			$this->CcrMora->TooltipValue = "";

			// CcrDAtr
			$this->CcrDAtr->LinkCustomAttributes = "";
			$this->CcrDAtr->HrefValue = "";
			$this->CcrDAtr->TooltipValue = "";

			// CcrEsta
			$this->CcrEsta->LinkCustomAttributes = "";
			$this->CcrEsta->HrefValue = "";
			$this->CcrEsta->TooltipValue = "";

			// CcrAnho
			$this->CcrAnho->LinkCustomAttributes = "";
			$this->CcrAnho->HrefValue = "";
			$this->CcrAnho->TooltipValue = "";

			// CcrMes
			$this->CcrMes->LinkCustomAttributes = "";
			$this->CcrMes->HrefValue = "";
			$this->CcrMes->TooltipValue = "";

			// CcrConc
			$this->CcrConc->LinkCustomAttributes = "";
			$this->CcrConc->HrefValue = "";
			$this->CcrConc->TooltipValue = "";

			// CcrUsua
			$this->CcrUsua->LinkCustomAttributes = "";
			$this->CcrUsua->HrefValue = "";
			$this->CcrUsua->TooltipValue = "";

			// CcrFCre
			$this->CcrFCre->LinkCustomAttributes = "";
			$this->CcrFCre->HrefValue = "";
			$this->CcrFCre->TooltipValue = "";
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	//
	// Delete records based on current filter
	//
	function DeleteRows() {
		global $Language, $Security;
		if (!$Security->CanDelete()) {
			$this->setFailureMessage($Language->Phrase("NoDeletePermission")); // No delete permission
			return FALSE;
		}
		$DeleteRows = TRUE;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE) {
			return FALSE;
		} elseif ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
			$rs->Close();
			return FALSE;

		//} else {
		//	$this->LoadRowValues($rs); // Load row values

		}
		$rows = ($rs) ? $rs->GetRows() : array();
		$conn->BeginTrans();

		// Clone old rows
		$rsold = $rows;
		if ($rs)
			$rs->Close();

		// Call row deleting event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$DeleteRows = $this->Row_Deleting($row);
				if (!$DeleteRows) break;
			}
		}
		if ($DeleteRows) {
			$sKey = "";
			foreach ($rsold as $row) {
				$sThisKey = "";
				if ($sThisKey <> "") $sThisKey .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
				$sThisKey .= $row['CcrCodi'];
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				$DeleteRows = $this->Delete($row); // Delete
				$conn->raiseErrorFn = '';
				if ($DeleteRows === FALSE)
					break;
				if ($sKey <> "") $sKey .= ", ";
				$sKey .= $sThisKey;
			}
		} else {

			// Set up error message
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("DeleteCancelled"));
			}
		}
		if ($DeleteRows) {
			$conn->CommitTrans(); // Commit the changes
		} else {
			$conn->RollbackTrans(); // Rollback changes
		}

		// Call Row Deleted event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$this->Row_Deleted($row);
			}
		}
		return $DeleteRows;
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, "CCrelist.php", "", $this->TableVar, TRUE);
		$PageId = "delete";
		$Breadcrumb->Add("delete", $PageId, $url);
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($CCre_delete)) $CCre_delete = new cCCre_delete();

// Page init
$CCre_delete->Page_Init();

// Page main
$CCre_delete->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$CCre_delete->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "delete";
var CurrentForm = fCCredelete = new ew_Form("fCCredelete", "delete");

// Form_CustomValidate event
fCCredelete.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fCCredelete.ValidateRequired = true;
<?php } else { ?>
fCCredelete.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
fCCredelete.Lists["x_VcrCodi"] = {"LinkField":"x_VcrCodi","Ajax":true,"AutoFill":false,"DisplayFields":["x_VcrCodi","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};

// Form object for search
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<?php

// Load records for display
if ($CCre_delete->Recordset = $CCre_delete->LoadRecordset())
	$CCre_deleteTotalRecs = $CCre_delete->Recordset->RecordCount(); // Get record count
if ($CCre_deleteTotalRecs <= 0) { // No record found, exit
	if ($CCre_delete->Recordset)
		$CCre_delete->Recordset->Close();
	$CCre_delete->Page_Terminate("CCrelist.php"); // Return to list
}
?>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php $CCre_delete->ShowPageHeader(); ?>
<?php
$CCre_delete->ShowMessage();
?>
<form name="fCCredelete" id="fCCredelete" class="form-inline ewForm ewDeleteForm" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($CCre_delete->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $CCre_delete->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="CCre">
<input type="hidden" name="a_delete" id="a_delete" value="D">
<?php foreach ($CCre_delete->RecKeys as $key) { ?>
<?php $keyvalue = is_array($key) ? implode($EW_COMPOSITE_KEY_SEPARATOR, $key) : $key; ?>
<input type="hidden" name="key_m[]" value="<?php echo ew_HtmlEncode($keyvalue) ?>">
<?php } ?>
<div class="ewGrid">
<div class="<?php if (ew_IsResponsiveLayout()) { echo "table-responsive "; } ?>ewGridMiddlePanel">
<table class="table ewTable">
<?php echo $CCre->TableCustomInnerHtml ?>
	<thead>
	<tr class="ewTableHeader">
<?php if ($CCre->CcrCodi->Visible) { // CcrCodi ?>
		<th><span id="elh_CCre_CcrCodi" class="CCre_CcrCodi"><?php echo $CCre->CcrCodi->FldCaption() ?></span></th>
<?php } ?>
<?php if ($CCre->VcrCodi->Visible) { // VcrCodi ?>
		<th><span id="elh_CCre_VcrCodi" class="CCre_VcrCodi"><?php echo $CCre->VcrCodi->FldCaption() ?></span></th>
<?php } ?>
<?php if ($CCre->CcrNCuo->Visible) { // CcrNCuo ?>
		<th><span id="elh_CCre_CcrNCuo" class="CCre_CcrNCuo"><?php echo $CCre->CcrNCuo->FldCaption() ?></span></th>
<?php } ?>
<?php if ($CCre->CcrFPag->Visible) { // CcrFPag ?>
		<th><span id="elh_CCre_CcrFPag" class="CCre_CcrFPag"><?php echo $CCre->CcrFPag->FldCaption() ?></span></th>
<?php } ?>
<?php if ($CCre->CcrMont->Visible) { // CcrMont ?>
		<th><span id="elh_CCre_CcrMont" class="CCre_CcrMont"><?php echo $CCre->CcrMont->FldCaption() ?></span></th>
<?php } ?>
<?php if ($CCre->CcrMora->Visible) { // CcrMora ?>
		<th><span id="elh_CCre_CcrMora" class="CCre_CcrMora"><?php echo $CCre->CcrMora->FldCaption() ?></span></th>
<?php } ?>
<?php if ($CCre->CcrDAtr->Visible) { // CcrDAtr ?>
		<th><span id="elh_CCre_CcrDAtr" class="CCre_CcrDAtr"><?php echo $CCre->CcrDAtr->FldCaption() ?></span></th>
<?php } ?>
<?php if ($CCre->CcrEsta->Visible) { // CcrEsta ?>
		<th><span id="elh_CCre_CcrEsta" class="CCre_CcrEsta"><?php echo $CCre->CcrEsta->FldCaption() ?></span></th>
<?php } ?>
<?php if ($CCre->CcrAnho->Visible) { // CcrAnho ?>
		<th><span id="elh_CCre_CcrAnho" class="CCre_CcrAnho"><?php echo $CCre->CcrAnho->FldCaption() ?></span></th>
<?php } ?>
<?php if ($CCre->CcrMes->Visible) { // CcrMes ?>
		<th><span id="elh_CCre_CcrMes" class="CCre_CcrMes"><?php echo $CCre->CcrMes->FldCaption() ?></span></th>
<?php } ?>
<?php if ($CCre->CcrConc->Visible) { // CcrConc ?>
		<th><span id="elh_CCre_CcrConc" class="CCre_CcrConc"><?php echo $CCre->CcrConc->FldCaption() ?></span></th>
<?php } ?>
<?php if ($CCre->CcrUsua->Visible) { // CcrUsua ?>
		<th><span id="elh_CCre_CcrUsua" class="CCre_CcrUsua"><?php echo $CCre->CcrUsua->FldCaption() ?></span></th>
<?php } ?>
<?php if ($CCre->CcrFCre->Visible) { // CcrFCre ?>
		<th><span id="elh_CCre_CcrFCre" class="CCre_CcrFCre"><?php echo $CCre->CcrFCre->FldCaption() ?></span></th>
<?php } ?>
	</tr>
	</thead>
	<tbody>
<?php
$CCre_delete->RecCnt = 0;
$i = 0;
while (!$CCre_delete->Recordset->EOF) {
	$CCre_delete->RecCnt++;
	$CCre_delete->RowCnt++;

	// Set row properties
	$CCre->ResetAttrs();
	$CCre->RowType = EW_ROWTYPE_VIEW; // View

	// Get the field contents
	$CCre_delete->LoadRowValues($CCre_delete->Recordset);

	// Render row
	$CCre_delete->RenderRow();
?>
	<tr<?php echo $CCre->RowAttributes() ?>>
<?php if ($CCre->CcrCodi->Visible) { // CcrCodi ?>
		<td<?php echo $CCre->CcrCodi->CellAttributes() ?>>
<span id="el<?php echo $CCre_delete->RowCnt ?>_CCre_CcrCodi" class="CCre_CcrCodi">
<span<?php echo $CCre->CcrCodi->ViewAttributes() ?>>
<?php echo $CCre->CcrCodi->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($CCre->VcrCodi->Visible) { // VcrCodi ?>
		<td<?php echo $CCre->VcrCodi->CellAttributes() ?>>
<span id="el<?php echo $CCre_delete->RowCnt ?>_CCre_VcrCodi" class="CCre_VcrCodi">
<span<?php echo $CCre->VcrCodi->ViewAttributes() ?>>
<?php echo $CCre->VcrCodi->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($CCre->CcrNCuo->Visible) { // CcrNCuo ?>
		<td<?php echo $CCre->CcrNCuo->CellAttributes() ?>>
<span id="el<?php echo $CCre_delete->RowCnt ?>_CCre_CcrNCuo" class="CCre_CcrNCuo">
<span<?php echo $CCre->CcrNCuo->ViewAttributes() ?>>
<?php echo $CCre->CcrNCuo->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($CCre->CcrFPag->Visible) { // CcrFPag ?>
		<td<?php echo $CCre->CcrFPag->CellAttributes() ?>>
<span id="el<?php echo $CCre_delete->RowCnt ?>_CCre_CcrFPag" class="CCre_CcrFPag">
<span<?php echo $CCre->CcrFPag->ViewAttributes() ?>>
<?php echo $CCre->CcrFPag->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($CCre->CcrMont->Visible) { // CcrMont ?>
		<td<?php echo $CCre->CcrMont->CellAttributes() ?>>
<span id="el<?php echo $CCre_delete->RowCnt ?>_CCre_CcrMont" class="CCre_CcrMont">
<span<?php echo $CCre->CcrMont->ViewAttributes() ?>>
<?php echo $CCre->CcrMont->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($CCre->CcrMora->Visible) { // CcrMora ?>
		<td<?php echo $CCre->CcrMora->CellAttributes() ?>>
<span id="el<?php echo $CCre_delete->RowCnt ?>_CCre_CcrMora" class="CCre_CcrMora">
<span<?php echo $CCre->CcrMora->ViewAttributes() ?>>
<?php echo $CCre->CcrMora->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($CCre->CcrDAtr->Visible) { // CcrDAtr ?>
		<td<?php echo $CCre->CcrDAtr->CellAttributes() ?>>
<span id="el<?php echo $CCre_delete->RowCnt ?>_CCre_CcrDAtr" class="CCre_CcrDAtr">
<span<?php echo $CCre->CcrDAtr->ViewAttributes() ?>>
<?php echo $CCre->CcrDAtr->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($CCre->CcrEsta->Visible) { // CcrEsta ?>
		<td<?php echo $CCre->CcrEsta->CellAttributes() ?>>
<span id="el<?php echo $CCre_delete->RowCnt ?>_CCre_CcrEsta" class="CCre_CcrEsta">
<span<?php echo $CCre->CcrEsta->ViewAttributes() ?>>
<?php echo $CCre->CcrEsta->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($CCre->CcrAnho->Visible) { // CcrAnho ?>
		<td<?php echo $CCre->CcrAnho->CellAttributes() ?>>
<span id="el<?php echo $CCre_delete->RowCnt ?>_CCre_CcrAnho" class="CCre_CcrAnho">
<span<?php echo $CCre->CcrAnho->ViewAttributes() ?>>
<?php echo $CCre->CcrAnho->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($CCre->CcrMes->Visible) { // CcrMes ?>
		<td<?php echo $CCre->CcrMes->CellAttributes() ?>>
<span id="el<?php echo $CCre_delete->RowCnt ?>_CCre_CcrMes" class="CCre_CcrMes">
<span<?php echo $CCre->CcrMes->ViewAttributes() ?>>
<?php echo $CCre->CcrMes->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($CCre->CcrConc->Visible) { // CcrConc ?>
		<td<?php echo $CCre->CcrConc->CellAttributes() ?>>
<span id="el<?php echo $CCre_delete->RowCnt ?>_CCre_CcrConc" class="CCre_CcrConc">
<span<?php echo $CCre->CcrConc->ViewAttributes() ?>>
<?php echo $CCre->CcrConc->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($CCre->CcrUsua->Visible) { // CcrUsua ?>
		<td<?php echo $CCre->CcrUsua->CellAttributes() ?>>
<span id="el<?php echo $CCre_delete->RowCnt ?>_CCre_CcrUsua" class="CCre_CcrUsua">
<span<?php echo $CCre->CcrUsua->ViewAttributes() ?>>
<?php echo $CCre->CcrUsua->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($CCre->CcrFCre->Visible) { // CcrFCre ?>
		<td<?php echo $CCre->CcrFCre->CellAttributes() ?>>
<span id="el<?php echo $CCre_delete->RowCnt ?>_CCre_CcrFCre" class="CCre_CcrFCre">
<span<?php echo $CCre->CcrFCre->ViewAttributes() ?>>
<?php echo $CCre->CcrFCre->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
	</tr>
<?php
	$CCre_delete->Recordset->MoveNext();
}
$CCre_delete->Recordset->Close();
?>
</tbody>
</table>
</div>
</div>
<div>
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("DeleteBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="button" data-href="<?php echo $CCre_delete->getReturnUrl() ?>"><?php echo $Language->Phrase("CancelBtn") ?></button>
</div>
</form>
<script type="text/javascript">
fCCredelete.Init();
</script>
<?php
$CCre_delete->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$CCre_delete->Page_Terminate();
?>
