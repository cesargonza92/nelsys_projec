<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "OVeninfo.php" ?>
<?php include_once "Usuainfo.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$OVen_edit = NULL; // Initialize page object first

class cOVen_edit extends cOVen {

	// Page ID
	var $PageID = 'edit';

	// Project ID
	var $ProjectID = "{04439FF7-B43F-460F-8514-F71C8FF9E679}";

	// Table name
	var $TableName = 'OVen';

	// Page object name
	var $PageObjName = 'OVen_edit';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (OVen)
		if (!isset($GLOBALS["OVen"]) || get_class($GLOBALS["OVen"]) == "cOVen") {
			$GLOBALS["OVen"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["OVen"];
		}

		// Table object (Usua)
		if (!isset($GLOBALS['Usua'])) $GLOBALS['Usua'] = new cUsua();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'edit', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'OVen', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (Usua)
		if (!isset($UserTable)) {
			$UserTable = new cUsua();
			$UserTableConn = Conn($UserTable->DBID);
		}
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanEdit()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage($Language->Phrase("NoPermission")); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("OVenlist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}

		// Create form object
		$objForm = new cFormObj();
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action
		$this->OveCodi->Visible = !$this->IsAdd() && !$this->IsCopy() && !$this->IsGridAdd();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Process auto fill
		if (@$_POST["ajax"] == "autofill") {
			$results = $this->GetAutoFill(@$_POST["name"], @$_POST["q"]);
			if ($results) {

				// Clean output buffer
				if (!EW_DEBUG_ENABLED && ob_get_length())
					ob_end_clean();
				echo $results;
				$this->Page_Terminate();
				exit();
			}
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $OVen;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($OVen);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $FormClassName = "form-horizontal ewForm ewEditForm";
	var $DbMasterFilter;
	var $DbDetailFilter;

	// 
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError;

		// Load key from QueryString
		if (@$_GET["OveCodi"] <> "") {
			$this->OveCodi->setQueryStringValue($_GET["OveCodi"]);
		}

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Process form if post back
		if (@$_POST["a_edit"] <> "") {
			$this->CurrentAction = $_POST["a_edit"]; // Get action code
			$this->LoadFormValues(); // Get form values
		} else {
			$this->CurrentAction = "I"; // Default action is display
		}

		// Check if valid key
		if ($this->OveCodi->CurrentValue == "")
			$this->Page_Terminate("OVenlist.php"); // Invalid key, return to list

		// Validate form if post back
		if (@$_POST["a_edit"] <> "") {
			if (!$this->ValidateForm()) {
				$this->CurrentAction = ""; // Form error, reset action
				$this->setFailureMessage($gsFormError);
				$this->EventCancelled = TRUE; // Event cancelled
				$this->RestoreFormValues();
			}
		}
		switch ($this->CurrentAction) {
			case "I": // Get a record to display
				if (!$this->LoadRow()) { // Load record based on key
					if ($this->getFailureMessage() == "") $this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
					$this->Page_Terminate("OVenlist.php"); // No matching record, return to list
				}
				break;
			Case "U": // Update
				$sReturnUrl = $this->getReturnUrl();
				$this->SendEmail = TRUE; // Send email on update success
				if ($this->EditRow()) { // Update record based on key
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage($Language->Phrase("UpdateSuccess")); // Update success
					$this->Page_Terminate($sReturnUrl); // Return to caller
				} elseif ($this->getFailureMessage() == $Language->Phrase("NoRecord")) {
					$this->Page_Terminate($sReturnUrl); // Return to caller
				} else {
					$this->EventCancelled = TRUE; // Event cancelled
					$this->RestoreFormValues(); // Restore form values if update failed
				}
		}

		// Render the record
		if ($this->CurrentAction == "F") { // Confirm page
			$this->RowType = EW_ROWTYPE_VIEW; // Render as View
		} else {
			$this->RowType = EW_ROWTYPE_EDIT; // Render as Edit
		}
		$this->ResetAttrs();
		$this->RenderRow();
	}

	// Set up starting record parameters
	function SetUpStartRec() {
		if ($this->DisplayRecs == 0)
			return;
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET[EW_TABLE_START_REC] <> "") { // Check for "start" parameter
				$this->StartRec = $_GET[EW_TABLE_START_REC];
				$this->setStartRecordNumber($this->StartRec);
			} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
				$PageNo = $_GET[EW_TABLE_PAGE_NO];
				if (is_numeric($PageNo)) {
					$this->StartRec = ($PageNo-1)*$this->DisplayRecs+1;
					if ($this->StartRec <= 0) {
						$this->StartRec = 1;
					} elseif ($this->StartRec >= intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1) {
						$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1;
					}
					$this->setStartRecordNumber($this->StartRec);
				}
			}
		}
		$this->StartRec = $this->getStartRecordNumber();

		// Check if correct start record counter
		if (!is_numeric($this->StartRec) || $this->StartRec == "") { // Avoid invalid start record counter
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} elseif (intval($this->StartRec) > intval($this->TotalRecs)) { // Avoid starting record > total records
			$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to last page first record
			$this->setStartRecordNumber($this->StartRec);
		} elseif (($this->StartRec-1) % $this->DisplayRecs <> 0) {
			$this->StartRec = intval(($this->StartRec-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to page boundary
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Get upload files
	function GetUploadFiles() {
		global $objForm, $Language;

		// Get upload data
	}

	// Load form values
	function LoadFormValues() {

		// Load from form
		global $objForm;
		if (!$this->OveCodi->FldIsDetailKey)
			$this->OveCodi->setFormValue($objForm->GetValue("x_OveCodi"));
		if (!$this->ProCodi->FldIsDetailKey) {
			$this->ProCodi->setFormValue($objForm->GetValue("x_ProCodi"));
		}
		if (!$this->OvePIni->FldIsDetailKey) {
			$this->OvePIni->setFormValue($objForm->GetValue("x_OvePIni"));
		}
		if (!$this->OveCPro->FldIsDetailKey) {
			$this->OveCPro->setFormValue($objForm->GetValue("x_OveCPro"));
		}
		if (!$this->OveUsua->FldIsDetailKey) {
			$this->OveUsua->setFormValue($objForm->GetValue("x_OveUsua"));
		}
		if (!$this->OveFCre->FldIsDetailKey) {
			$this->OveFCre->setFormValue($objForm->GetValue("x_OveFCre"));
			$this->OveFCre->CurrentValue = ew_UnFormatDateTime($this->OveFCre->CurrentValue, 7);
		}
		if (!$this->OveCVen->FldIsDetailKey) {
			$this->OveCVen->setFormValue($objForm->GetValue("x_OveCVen"));
		}
		if (!$this->CliCodi->FldIsDetailKey) {
			$this->CliCodi->setFormValue($objForm->GetValue("x_CliCodi"));
		}
	}

	// Restore form values
	function RestoreFormValues() {
		global $objForm;
		$this->LoadRow();
		$this->OveCodi->CurrentValue = $this->OveCodi->FormValue;
		$this->ProCodi->CurrentValue = $this->ProCodi->FormValue;
		$this->OvePIni->CurrentValue = $this->OvePIni->FormValue;
		$this->OveCPro->CurrentValue = $this->OveCPro->FormValue;
		$this->OveUsua->CurrentValue = $this->OveUsua->FormValue;
		$this->OveFCre->CurrentValue = $this->OveFCre->FormValue;
		$this->OveFCre->CurrentValue = ew_UnFormatDateTime($this->OveFCre->CurrentValue, 7);
		$this->OveCVen->CurrentValue = $this->OveCVen->FormValue;
		$this->CliCodi->CurrentValue = $this->CliCodi->FormValue;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->OveCodi->setDbValue($rs->fields('OveCodi'));
		$this->ProCodi->setDbValue($rs->fields('ProCodi'));
		$this->OvePIni->setDbValue($rs->fields('OvePIni'));
		$this->OveCPro->setDbValue($rs->fields('OveCPro'));
		$this->OveUsua->setDbValue($rs->fields('OveUsua'));
		$this->OveFCre->setDbValue($rs->fields('OveFCre'));
		$this->OveCVen->setDbValue($rs->fields('OveCVen'));
		$this->CliCodi->setDbValue($rs->fields('CliCodi'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->OveCodi->DbValue = $row['OveCodi'];
		$this->ProCodi->DbValue = $row['ProCodi'];
		$this->OvePIni->DbValue = $row['OvePIni'];
		$this->OveCPro->DbValue = $row['OveCPro'];
		$this->OveUsua->DbValue = $row['OveUsua'];
		$this->OveFCre->DbValue = $row['OveFCre'];
		$this->OveCVen->DbValue = $row['OveCVen'];
		$this->CliCodi->DbValue = $row['CliCodi'];
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		// Convert decimal values if posted back

		if ($this->OvePIni->FormValue == $this->OvePIni->CurrentValue && is_numeric(ew_StrToFloat($this->OvePIni->CurrentValue)))
			$this->OvePIni->CurrentValue = ew_StrToFloat($this->OvePIni->CurrentValue);

		// Call Row_Rendering event
		$this->Row_Rendering();

		// Common render codes for all row types
		// OveCodi
		// ProCodi
		// OvePIni
		// OveCPro
		// OveUsua
		// OveFCre
		// OveCVen
		// CliCodi

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// OveCodi
		$this->OveCodi->ViewValue = $this->OveCodi->CurrentValue;
		$this->OveCodi->ViewCustomAttributes = "";

		// ProCodi
		if (strval($this->ProCodi->CurrentValue) <> "") {
			$sFilterWrk = "\"ProdCodi\"" . ew_SearchString("=", $this->ProCodi->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT \"ProdCodi\", \"CprCodi\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"Prod\"";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->ProCodi, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->ProCodi->ViewValue = $this->ProCodi->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->ProCodi->ViewValue = $this->ProCodi->CurrentValue;
			}
		} else {
			$this->ProCodi->ViewValue = NULL;
		}
		$this->ProCodi->ViewCustomAttributes = "";

		// OvePIni
		$this->OvePIni->ViewValue = $this->OvePIni->CurrentValue;
		$this->OvePIni->ViewCustomAttributes = "";

		// OveCPro
		$this->OveCPro->ViewValue = $this->OveCPro->CurrentValue;
		$this->OveCPro->ViewCustomAttributes = "";

		// OveUsua
		$this->OveUsua->ViewValue = $this->OveUsua->CurrentValue;
		$this->OveUsua->ViewCustomAttributes = "";

		// OveFCre
		$this->OveFCre->ViewValue = $this->OveFCre->CurrentValue;
		$this->OveFCre->ViewValue = ew_FormatDateTime($this->OveFCre->ViewValue, 7);
		$this->OveFCre->ViewCustomAttributes = "";

		// OveCVen
		$this->OveCVen->ViewValue = $this->OveCVen->CurrentValue;
		$this->OveCVen->ViewCustomAttributes = "";

		// CliCodi
		if (strval($this->CliCodi->CurrentValue) <> "") {
			$sFilterWrk = "\"CliCodi\"" . ew_SearchString("=", $this->CliCodi->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT \"CliCodi\", \"CliNomb\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"Clie\"";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->CliCodi, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->CliCodi->ViewValue = $this->CliCodi->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->CliCodi->ViewValue = $this->CliCodi->CurrentValue;
			}
		} else {
			$this->CliCodi->ViewValue = NULL;
		}
		$this->CliCodi->ViewCustomAttributes = "";

			// OveCodi
			$this->OveCodi->LinkCustomAttributes = "";
			$this->OveCodi->HrefValue = "";
			$this->OveCodi->TooltipValue = "";

			// ProCodi
			$this->ProCodi->LinkCustomAttributes = "";
			$this->ProCodi->HrefValue = "";
			$this->ProCodi->TooltipValue = "";

			// OvePIni
			$this->OvePIni->LinkCustomAttributes = "";
			$this->OvePIni->HrefValue = "";
			$this->OvePIni->TooltipValue = "";

			// OveCPro
			$this->OveCPro->LinkCustomAttributes = "";
			$this->OveCPro->HrefValue = "";
			$this->OveCPro->TooltipValue = "";

			// OveUsua
			$this->OveUsua->LinkCustomAttributes = "";
			$this->OveUsua->HrefValue = "";
			$this->OveUsua->TooltipValue = "";

			// OveFCre
			$this->OveFCre->LinkCustomAttributes = "";
			$this->OveFCre->HrefValue = "";
			$this->OveFCre->TooltipValue = "";

			// OveCVen
			$this->OveCVen->LinkCustomAttributes = "";
			$this->OveCVen->HrefValue = "";
			$this->OveCVen->TooltipValue = "";

			// CliCodi
			$this->CliCodi->LinkCustomAttributes = "";
			$this->CliCodi->HrefValue = "";
			$this->CliCodi->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_EDIT) { // Edit row

			// OveCodi
			$this->OveCodi->EditAttrs["class"] = "form-control";
			$this->OveCodi->EditCustomAttributes = "";
			$this->OveCodi->EditValue = $this->OveCodi->CurrentValue;
			$this->OveCodi->ViewCustomAttributes = "";

			// ProCodi
			$this->ProCodi->EditAttrs["class"] = "form-control";
			$this->ProCodi->EditCustomAttributes = "";
			if (trim(strval($this->ProCodi->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "\"ProdCodi\"" . ew_SearchString("=", $this->ProCodi->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT \"ProdCodi\", \"CprCodi\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\", '' AS \"SelectFilterFld\", '' AS \"SelectFilterFld2\", '' AS \"SelectFilterFld3\", '' AS \"SelectFilterFld4\" FROM \"public\".\"Prod\"";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->ProCodi, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->ProCodi->EditValue = $arwrk;

			// OvePIni
			$this->OvePIni->EditAttrs["class"] = "form-control";
			$this->OvePIni->EditCustomAttributes = "";
			$this->OvePIni->EditValue = ew_HtmlEncode($this->OvePIni->CurrentValue);
			$this->OvePIni->PlaceHolder = ew_RemoveHtml($this->OvePIni->FldCaption());
			if (strval($this->OvePIni->EditValue) <> "" && is_numeric($this->OvePIni->EditValue)) $this->OvePIni->EditValue = ew_FormatNumber($this->OvePIni->EditValue, -2, -1, -2, 0);

			// OveCPro
			$this->OveCPro->EditAttrs["class"] = "form-control";
			$this->OveCPro->EditCustomAttributes = "";
			$this->OveCPro->EditValue = ew_HtmlEncode($this->OveCPro->CurrentValue);
			$this->OveCPro->PlaceHolder = ew_RemoveHtml($this->OveCPro->FldCaption());

			// OveUsua
			$this->OveUsua->EditAttrs["class"] = "form-control";
			$this->OveUsua->EditCustomAttributes = "";
			$this->OveUsua->EditValue = ew_HtmlEncode($this->OveUsua->CurrentValue);
			$this->OveUsua->PlaceHolder = ew_RemoveHtml($this->OveUsua->FldCaption());

			// OveFCre
			$this->OveFCre->EditAttrs["class"] = "form-control";
			$this->OveFCre->EditCustomAttributes = "";
			$this->OveFCre->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->OveFCre->CurrentValue, 7));
			$this->OveFCre->PlaceHolder = ew_RemoveHtml($this->OveFCre->FldCaption());

			// OveCVen
			$this->OveCVen->EditAttrs["class"] = "form-control";
			$this->OveCVen->EditCustomAttributes = "";
			$this->OveCVen->EditValue = ew_HtmlEncode($this->OveCVen->CurrentValue);
			$this->OveCVen->PlaceHolder = ew_RemoveHtml($this->OveCVen->FldCaption());

			// CliCodi
			$this->CliCodi->EditAttrs["class"] = "form-control";
			$this->CliCodi->EditCustomAttributes = "";
			if (trim(strval($this->CliCodi->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "\"CliCodi\"" . ew_SearchString("=", $this->CliCodi->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT \"CliCodi\", \"CliNomb\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\", '' AS \"SelectFilterFld\", '' AS \"SelectFilterFld2\", '' AS \"SelectFilterFld3\", '' AS \"SelectFilterFld4\" FROM \"public\".\"Clie\"";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->CliCodi, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->CliCodi->EditValue = $arwrk;

			// Edit refer script
			// OveCodi

			$this->OveCodi->HrefValue = "";

			// ProCodi
			$this->ProCodi->HrefValue = "";

			// OvePIni
			$this->OvePIni->HrefValue = "";

			// OveCPro
			$this->OveCPro->HrefValue = "";

			// OveUsua
			$this->OveUsua->HrefValue = "";

			// OveFCre
			$this->OveFCre->HrefValue = "";

			// OveCVen
			$this->OveCVen->HrefValue = "";

			// CliCodi
			$this->CliCodi->HrefValue = "";
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate form
	function ValidateForm() {
		global $Language, $gsFormError;

		// Initialize form error message
		$gsFormError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return ($gsFormError == "");
		if (!$this->ProCodi->FldIsDetailKey && !is_null($this->ProCodi->FormValue) && $this->ProCodi->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->ProCodi->FldCaption(), $this->ProCodi->ReqErrMsg));
		}
		if (!$this->OvePIni->FldIsDetailKey && !is_null($this->OvePIni->FormValue) && $this->OvePIni->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->OvePIni->FldCaption(), $this->OvePIni->ReqErrMsg));
		}
		if (!ew_CheckNumber($this->OvePIni->FormValue)) {
			ew_AddMessage($gsFormError, $this->OvePIni->FldErrMsg());
		}
		if (!$this->OveCPro->FldIsDetailKey && !is_null($this->OveCPro->FormValue) && $this->OveCPro->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->OveCPro->FldCaption(), $this->OveCPro->ReqErrMsg));
		}
		if (!ew_CheckInteger($this->OveCPro->FormValue)) {
			ew_AddMessage($gsFormError, $this->OveCPro->FldErrMsg());
		}
		if (!$this->OveUsua->FldIsDetailKey && !is_null($this->OveUsua->FormValue) && $this->OveUsua->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->OveUsua->FldCaption(), $this->OveUsua->ReqErrMsg));
		}
		if (!$this->OveFCre->FldIsDetailKey && !is_null($this->OveFCre->FormValue) && $this->OveFCre->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->OveFCre->FldCaption(), $this->OveFCre->ReqErrMsg));
		}
		if (!ew_CheckEuroDate($this->OveFCre->FormValue)) {
			ew_AddMessage($gsFormError, $this->OveFCre->FldErrMsg());
		}
		if (!$this->OveCVen->FldIsDetailKey && !is_null($this->OveCVen->FormValue) && $this->OveCVen->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->OveCVen->FldCaption(), $this->OveCVen->ReqErrMsg));
		}
		if (!ew_CheckInteger($this->OveCVen->FormValue)) {
			ew_AddMessage($gsFormError, $this->OveCVen->FldErrMsg());
		}

		// Return validate result
		$ValidateForm = ($gsFormError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateForm = $ValidateForm && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsFormError, $sFormCustomError);
		}
		return $ValidateForm;
	}

	// Update record based on key values
	function EditRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$conn = &$this->Connection();
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE)
			return FALSE;
		if ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record message
			$EditRow = FALSE; // Update Failed
		} else {

			// Save old values
			$rsold = &$rs->fields;
			$this->LoadDbValues($rsold);
			$rsnew = array();

			// ProCodi
			$this->ProCodi->SetDbValueDef($rsnew, $this->ProCodi->CurrentValue, 0, $this->ProCodi->ReadOnly);

			// OvePIni
			$this->OvePIni->SetDbValueDef($rsnew, $this->OvePIni->CurrentValue, 0, $this->OvePIni->ReadOnly);

			// OveCPro
			$this->OveCPro->SetDbValueDef($rsnew, $this->OveCPro->CurrentValue, 0, $this->OveCPro->ReadOnly);

			// OveUsua
			$this->OveUsua->SetDbValueDef($rsnew, $this->OveUsua->CurrentValue, "", $this->OveUsua->ReadOnly);

			// OveFCre
			$this->OveFCre->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->OveFCre->CurrentValue, 7), ew_CurrentDate(), $this->OveFCre->ReadOnly);

			// OveCVen
			$this->OveCVen->SetDbValueDef($rsnew, $this->OveCVen->CurrentValue, 0, $this->OveCVen->ReadOnly);

			// CliCodi
			$this->CliCodi->SetDbValueDef($rsnew, $this->CliCodi->CurrentValue, NULL, $this->CliCodi->ReadOnly);

			// Call Row Updating event
			$bUpdateRow = $this->Row_Updating($rsold, $rsnew);
			if ($bUpdateRow) {
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				if (count($rsnew) > 0)
					$EditRow = $this->Update($rsnew, "", $rsold);
				else
					$EditRow = TRUE; // No field to update
				$conn->raiseErrorFn = '';
				if ($EditRow) {
				}
			} else {
				if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

					// Use the message, do nothing
				} elseif ($this->CancelMessage <> "") {
					$this->setFailureMessage($this->CancelMessage);
					$this->CancelMessage = "";
				} else {
					$this->setFailureMessage($Language->Phrase("UpdateCancelled"));
				}
				$EditRow = FALSE;
			}
		}

		// Call Row_Updated event
		if ($EditRow)
			$this->Row_Updated($rsold, $rsnew);
		$rs->Close();
		return $EditRow;
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, "OVenlist.php", "", $this->TableVar, TRUE);
		$PageId = "edit";
		$Breadcrumb->Add("edit", $PageId, $url);
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($OVen_edit)) $OVen_edit = new cOVen_edit();

// Page init
$OVen_edit->Page_Init();

// Page main
$OVen_edit->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$OVen_edit->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "edit";
var CurrentForm = fOVenedit = new ew_Form("fOVenedit", "edit");

// Validate form
fOVenedit.Validate = function() {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.GetForm(), $fobj = $(fobj);
	if ($fobj.find("#a_confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.FormKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = $fobj.find("#a_list").val() == "gridinsert";
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
			elm = this.GetElements("x" + infix + "_ProCodi");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $OVen->ProCodi->FldCaption(), $OVen->ProCodi->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_OvePIni");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $OVen->OvePIni->FldCaption(), $OVen->OvePIni->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_OvePIni");
			if (elm && !ew_CheckNumber(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($OVen->OvePIni->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_OveCPro");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $OVen->OveCPro->FldCaption(), $OVen->OveCPro->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_OveCPro");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($OVen->OveCPro->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_OveUsua");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $OVen->OveUsua->FldCaption(), $OVen->OveUsua->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_OveFCre");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $OVen->OveFCre->FldCaption(), $OVen->OveFCre->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_OveFCre");
			if (elm && !ew_CheckEuroDate(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($OVen->OveFCre->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_OveCVen");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $OVen->OveCVen->FldCaption(), $OVen->OveCVen->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_OveCVen");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($OVen->OveCVen->FldErrMsg()) ?>");

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
	}

	// Process detail forms
	var dfs = $fobj.find("input[name='detailpage']").get();
	for (var i = 0; i < dfs.length; i++) {
		var df = dfs[i], val = df.value;
		if (val && ewForms[val])
			if (!ewForms[val].Validate())
				return false;
	}
	return true;
}

// Form_CustomValidate event
fOVenedit.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fOVenedit.ValidateRequired = true;
<?php } else { ?>
fOVenedit.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
fOVenedit.Lists["x_ProCodi"] = {"LinkField":"x_ProdCodi","Ajax":true,"AutoFill":false,"DisplayFields":["x_CprCodi","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fOVenedit.Lists["x_CliCodi"] = {"LinkField":"x_CliCodi","Ajax":true,"AutoFill":false,"DisplayFields":["x_CliNomb","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};

// Form object for search
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php $OVen_edit->ShowPageHeader(); ?>
<?php
$OVen_edit->ShowMessage();
?>
<form name="fOVenedit" id="fOVenedit" class="<?php echo $OVen_edit->FormClassName ?>" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($OVen_edit->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $OVen_edit->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="OVen">
<input type="hidden" name="a_edit" id="a_edit" value="U">
<?php if ($OVen->CurrentAction == "F") { // Confirm page ?>
<input type="hidden" name="a_confirm" id="a_confirm" value="F">
<?php } ?>
<div>
<?php if ($OVen->OveCodi->Visible) { // OveCodi ?>
	<div id="r_OveCodi" class="form-group">
		<label id="elh_OVen_OveCodi" class="col-sm-2 control-label ewLabel"><?php echo $OVen->OveCodi->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $OVen->OveCodi->CellAttributes() ?>>
<?php if ($OVen->CurrentAction <> "F") { ?>
<span id="el_OVen_OveCodi">
<span<?php echo $OVen->OveCodi->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $OVen->OveCodi->EditValue ?></p></span>
</span>
<input type="hidden" data-table="OVen" data-field="x_OveCodi" name="x_OveCodi" id="x_OveCodi" value="<?php echo ew_HtmlEncode($OVen->OveCodi->CurrentValue) ?>">
<?php } else { ?>
<span id="el_OVen_OveCodi">
<span<?php echo $OVen->OveCodi->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $OVen->OveCodi->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="OVen" data-field="x_OveCodi" name="x_OveCodi" id="x_OveCodi" value="<?php echo ew_HtmlEncode($OVen->OveCodi->FormValue) ?>">
<?php } ?>
<?php echo $OVen->OveCodi->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($OVen->ProCodi->Visible) { // ProCodi ?>
	<div id="r_ProCodi" class="form-group">
		<label id="elh_OVen_ProCodi" for="x_ProCodi" class="col-sm-2 control-label ewLabel"><?php echo $OVen->ProCodi->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $OVen->ProCodi->CellAttributes() ?>>
<?php if ($OVen->CurrentAction <> "F") { ?>
<span id="el_OVen_ProCodi">
<select data-table="OVen" data-field="x_ProCodi" data-value-separator="<?php echo ew_HtmlEncode(is_array($OVen->ProCodi->DisplayValueSeparator) ? json_encode($OVen->ProCodi->DisplayValueSeparator) : $OVen->ProCodi->DisplayValueSeparator) ?>" id="x_ProCodi" name="x_ProCodi"<?php echo $OVen->ProCodi->EditAttributes() ?>>
<?php
if (is_array($OVen->ProCodi->EditValue)) {
	$arwrk = $OVen->ProCodi->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($OVen->ProCodi->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $OVen->ProCodi->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($OVen->ProCodi->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($OVen->ProCodi->CurrentValue) ?>" selected><?php echo $OVen->ProCodi->CurrentValue ?></option>
<?php
    }
}
?>
</select>
<?php
$sSqlWrk = "SELECT \"ProdCodi\", \"CprCodi\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"Prod\"";
$sWhereWrk = "";
$OVen->ProCodi->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$OVen->ProCodi->LookupFilters += array("f0" => "\"ProdCodi\" = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$OVen->Lookup_Selecting($OVen->ProCodi, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $OVen->ProCodi->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x_ProCodi" id="s_x_ProCodi" value="<?php echo $OVen->ProCodi->LookupFilterQuery() ?>">
</span>
<?php } else { ?>
<span id="el_OVen_ProCodi">
<span<?php echo $OVen->ProCodi->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $OVen->ProCodi->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="OVen" data-field="x_ProCodi" name="x_ProCodi" id="x_ProCodi" value="<?php echo ew_HtmlEncode($OVen->ProCodi->FormValue) ?>">
<?php } ?>
<?php echo $OVen->ProCodi->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($OVen->OvePIni->Visible) { // OvePIni ?>
	<div id="r_OvePIni" class="form-group">
		<label id="elh_OVen_OvePIni" for="x_OvePIni" class="col-sm-2 control-label ewLabel"><?php echo $OVen->OvePIni->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $OVen->OvePIni->CellAttributes() ?>>
<?php if ($OVen->CurrentAction <> "F") { ?>
<span id="el_OVen_OvePIni">
<input type="text" data-table="OVen" data-field="x_OvePIni" name="x_OvePIni" id="x_OvePIni" size="30" placeholder="<?php echo ew_HtmlEncode($OVen->OvePIni->getPlaceHolder()) ?>" value="<?php echo $OVen->OvePIni->EditValue ?>"<?php echo $OVen->OvePIni->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_OVen_OvePIni">
<span<?php echo $OVen->OvePIni->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $OVen->OvePIni->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="OVen" data-field="x_OvePIni" name="x_OvePIni" id="x_OvePIni" value="<?php echo ew_HtmlEncode($OVen->OvePIni->FormValue) ?>">
<?php } ?>
<?php echo $OVen->OvePIni->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($OVen->OveCPro->Visible) { // OveCPro ?>
	<div id="r_OveCPro" class="form-group">
		<label id="elh_OVen_OveCPro" for="x_OveCPro" class="col-sm-2 control-label ewLabel"><?php echo $OVen->OveCPro->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $OVen->OveCPro->CellAttributes() ?>>
<?php if ($OVen->CurrentAction <> "F") { ?>
<span id="el_OVen_OveCPro">
<input type="text" data-table="OVen" data-field="x_OveCPro" name="x_OveCPro" id="x_OveCPro" size="30" placeholder="<?php echo ew_HtmlEncode($OVen->OveCPro->getPlaceHolder()) ?>" value="<?php echo $OVen->OveCPro->EditValue ?>"<?php echo $OVen->OveCPro->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_OVen_OveCPro">
<span<?php echo $OVen->OveCPro->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $OVen->OveCPro->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="OVen" data-field="x_OveCPro" name="x_OveCPro" id="x_OveCPro" value="<?php echo ew_HtmlEncode($OVen->OveCPro->FormValue) ?>">
<?php } ?>
<?php echo $OVen->OveCPro->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($OVen->OveUsua->Visible) { // OveUsua ?>
	<div id="r_OveUsua" class="form-group">
		<label id="elh_OVen_OveUsua" for="x_OveUsua" class="col-sm-2 control-label ewLabel"><?php echo $OVen->OveUsua->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $OVen->OveUsua->CellAttributes() ?>>
<?php if ($OVen->CurrentAction <> "F") { ?>
<span id="el_OVen_OveUsua">
<input type="text" data-table="OVen" data-field="x_OveUsua" name="x_OveUsua" id="x_OveUsua" size="30" placeholder="<?php echo ew_HtmlEncode($OVen->OveUsua->getPlaceHolder()) ?>" value="<?php echo $OVen->OveUsua->EditValue ?>"<?php echo $OVen->OveUsua->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_OVen_OveUsua">
<span<?php echo $OVen->OveUsua->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $OVen->OveUsua->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="OVen" data-field="x_OveUsua" name="x_OveUsua" id="x_OveUsua" value="<?php echo ew_HtmlEncode($OVen->OveUsua->FormValue) ?>">
<?php } ?>
<?php echo $OVen->OveUsua->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($OVen->OveFCre->Visible) { // OveFCre ?>
	<div id="r_OveFCre" class="form-group">
		<label id="elh_OVen_OveFCre" for="x_OveFCre" class="col-sm-2 control-label ewLabel"><?php echo $OVen->OveFCre->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $OVen->OveFCre->CellAttributes() ?>>
<?php if ($OVen->CurrentAction <> "F") { ?>
<span id="el_OVen_OveFCre">
<input type="text" data-table="OVen" data-field="x_OveFCre" data-format="7" name="x_OveFCre" id="x_OveFCre" placeholder="<?php echo ew_HtmlEncode($OVen->OveFCre->getPlaceHolder()) ?>" value="<?php echo $OVen->OveFCre->EditValue ?>"<?php echo $OVen->OveFCre->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_OVen_OveFCre">
<span<?php echo $OVen->OveFCre->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $OVen->OveFCre->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="OVen" data-field="x_OveFCre" name="x_OveFCre" id="x_OveFCre" value="<?php echo ew_HtmlEncode($OVen->OveFCre->FormValue) ?>">
<?php } ?>
<?php echo $OVen->OveFCre->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($OVen->OveCVen->Visible) { // OveCVen ?>
	<div id="r_OveCVen" class="form-group">
		<label id="elh_OVen_OveCVen" for="x_OveCVen" class="col-sm-2 control-label ewLabel"><?php echo $OVen->OveCVen->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $OVen->OveCVen->CellAttributes() ?>>
<?php if ($OVen->CurrentAction <> "F") { ?>
<span id="el_OVen_OveCVen">
<input type="text" data-table="OVen" data-field="x_OveCVen" name="x_OveCVen" id="x_OveCVen" size="30" placeholder="<?php echo ew_HtmlEncode($OVen->OveCVen->getPlaceHolder()) ?>" value="<?php echo $OVen->OveCVen->EditValue ?>"<?php echo $OVen->OveCVen->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_OVen_OveCVen">
<span<?php echo $OVen->OveCVen->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $OVen->OveCVen->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="OVen" data-field="x_OveCVen" name="x_OveCVen" id="x_OveCVen" value="<?php echo ew_HtmlEncode($OVen->OveCVen->FormValue) ?>">
<?php } ?>
<?php echo $OVen->OveCVen->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($OVen->CliCodi->Visible) { // CliCodi ?>
	<div id="r_CliCodi" class="form-group">
		<label id="elh_OVen_CliCodi" for="x_CliCodi" class="col-sm-2 control-label ewLabel"><?php echo $OVen->CliCodi->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $OVen->CliCodi->CellAttributes() ?>>
<?php if ($OVen->CurrentAction <> "F") { ?>
<span id="el_OVen_CliCodi">
<select data-table="OVen" data-field="x_CliCodi" data-value-separator="<?php echo ew_HtmlEncode(is_array($OVen->CliCodi->DisplayValueSeparator) ? json_encode($OVen->CliCodi->DisplayValueSeparator) : $OVen->CliCodi->DisplayValueSeparator) ?>" id="x_CliCodi" name="x_CliCodi"<?php echo $OVen->CliCodi->EditAttributes() ?>>
<?php
if (is_array($OVen->CliCodi->EditValue)) {
	$arwrk = $OVen->CliCodi->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($OVen->CliCodi->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $OVen->CliCodi->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($OVen->CliCodi->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($OVen->CliCodi->CurrentValue) ?>" selected><?php echo $OVen->CliCodi->CurrentValue ?></option>
<?php
    }
}
?>
</select>
<?php
$sSqlWrk = "SELECT \"CliCodi\", \"CliNomb\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"Clie\"";
$sWhereWrk = "";
$OVen->CliCodi->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$OVen->CliCodi->LookupFilters += array("f0" => "\"CliCodi\" = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$OVen->Lookup_Selecting($OVen->CliCodi, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $OVen->CliCodi->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x_CliCodi" id="s_x_CliCodi" value="<?php echo $OVen->CliCodi->LookupFilterQuery() ?>">
</span>
<?php } else { ?>
<span id="el_OVen_CliCodi">
<span<?php echo $OVen->CliCodi->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $OVen->CliCodi->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="OVen" data-field="x_CliCodi" name="x_CliCodi" id="x_CliCodi" value="<?php echo ew_HtmlEncode($OVen->CliCodi->FormValue) ?>">
<?php } ?>
<?php echo $OVen->CliCodi->CustomMsg ?></div></div>
	</div>
<?php } ?>
</div>
<div class="form-group">
	<div class="col-sm-offset-2 col-sm-10">
<?php if ($OVen->CurrentAction <> "F") { // Confirm page ?>
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit" onclick="this.form.a_edit.value='F';"><?php echo $Language->Phrase("SaveBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="button" data-href="<?php echo $OVen_edit->getReturnUrl() ?>"><?php echo $Language->Phrase("CancelBtn") ?></button>
<?php } else { ?>
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("ConfirmBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="submit" onclick="this.form.a_edit.value='X';"><?php echo $Language->Phrase("CancelBtn") ?></button>
<?php } ?>
	</div>
</div>
</form>
<script type="text/javascript">
fOVenedit.Init();
</script>
<?php
$OVen_edit->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$OVen_edit->Page_Terminate();
?>
