<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "CConinfo.php" ?>
<?php include_once "Usuainfo.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$CCon_list = NULL; // Initialize page object first

class cCCon_list extends cCCon {

	// Page ID
	var $PageID = 'list';

	// Project ID
	var $ProjectID = "{04439FF7-B43F-460F-8514-F71C8FF9E679}";

	// Table name
	var $TableName = 'CCon';

	// Page object name
	var $PageObjName = 'CCon_list';

	// Grid form hidden field names
	var $FormName = 'fCConlist';
	var $FormActionName = 'k_action';
	var $FormKeyName = 'k_key';
	var $FormOldKeyName = 'k_oldkey';
	var $FormBlankRowName = 'k_blankrow';
	var $FormKeyCountName = 'key_count';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Page URLs
	var $AddUrl;
	var $EditUrl;
	var $CopyUrl;
	var $DeleteUrl;
	var $ViewUrl;
	var $ListUrl;

	// Export URLs
	var $ExportPrintUrl;
	var $ExportHtmlUrl;
	var $ExportExcelUrl;
	var $ExportWordUrl;
	var $ExportXmlUrl;
	var $ExportCsvUrl;
	var $ExportPdfUrl;

	// Custom export
	var $ExportExcelCustom = FALSE;
	var $ExportWordCustom = FALSE;
	var $ExportPdfCustom = FALSE;
	var $ExportEmailCustom = FALSE;

	// Update URLs
	var $InlineAddUrl;
	var $InlineCopyUrl;
	var $InlineEditUrl;
	var $GridAddUrl;
	var $GridEditUrl;
	var $MultiDeleteUrl;
	var $MultiUpdateUrl;

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (CCon)
		if (!isset($GLOBALS["CCon"]) || get_class($GLOBALS["CCon"]) == "cCCon") {
			$GLOBALS["CCon"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["CCon"];
		}

		// Initialize URLs
		$this->ExportPrintUrl = $this->PageUrl() . "export=print";
		$this->ExportExcelUrl = $this->PageUrl() . "export=excel";
		$this->ExportWordUrl = $this->PageUrl() . "export=word";
		$this->ExportHtmlUrl = $this->PageUrl() . "export=html";
		$this->ExportXmlUrl = $this->PageUrl() . "export=xml";
		$this->ExportCsvUrl = $this->PageUrl() . "export=csv";
		$this->ExportPdfUrl = $this->PageUrl() . "export=pdf";
		$this->AddUrl = "CConadd.php";
		$this->InlineAddUrl = $this->PageUrl() . "a=add";
		$this->GridAddUrl = $this->PageUrl() . "a=gridadd";
		$this->GridEditUrl = $this->PageUrl() . "a=gridedit";
		$this->MultiDeleteUrl = "CCondelete.php";
		$this->MultiUpdateUrl = "CConupdate.php";

		// Table object (Usua)
		if (!isset($GLOBALS['Usua'])) $GLOBALS['Usua'] = new cUsua();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'list', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'CCon', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (Usua)
		if (!isset($UserTable)) {
			$UserTable = new cUsua();
			$UserTableConn = Conn($UserTable->DBID);
		}

		// List options
		$this->ListOptions = new cListOptions();
		$this->ListOptions->TableVar = $this->TableVar;

		// Export options
		$this->ExportOptions = new cListOptions();
		$this->ExportOptions->Tag = "div";
		$this->ExportOptions->TagClassName = "ewExportOption";

		// Other options
		$this->OtherOptions['addedit'] = new cListOptions();
		$this->OtherOptions['addedit']->Tag = "div";
		$this->OtherOptions['addedit']->TagClassName = "ewAddEditOption";
		$this->OtherOptions['detail'] = new cListOptions();
		$this->OtherOptions['detail']->Tag = "div";
		$this->OtherOptions['detail']->TagClassName = "ewDetailOption";
		$this->OtherOptions['action'] = new cListOptions();
		$this->OtherOptions['action']->Tag = "div";
		$this->OtherOptions['action']->TagClassName = "ewActionOption";

		// Filter options
		$this->FilterOptions = new cListOptions();
		$this->FilterOptions->Tag = "div";
		$this->FilterOptions->TagClassName = "ewFilterOption fCConlistsrch";

		// List actions
		$this->ListActions = new cListActions();
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanList()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage($Language->Phrase("NoPermission")); // Set no permission
			$this->Page_Terminate(ew_GetUrl("index.php"));
		}

		// Create form object
		$objForm = new cFormObj();
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action

		// Get grid add count
		$gridaddcnt = @$_GET[EW_TABLE_GRID_ADD_ROW_COUNT];
		if (is_numeric($gridaddcnt) && $gridaddcnt > 0)
			$this->GridAddRowCount = $gridaddcnt;

		// Set up list options
		$this->SetupListOptions();
		$this->CcoCodi->Visible = !$this->IsAdd() && !$this->IsCopy() && !$this->IsGridAdd();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Process auto fill
		if (@$_POST["ajax"] == "autofill") {
			$results = $this->GetAutoFill(@$_POST["name"], @$_POST["q"]);
			if ($results) {

				// Clean output buffer
				if (!EW_DEBUG_ENABLED && ob_get_length())
					ob_end_clean();
				echo $results;
				$this->Page_Terminate();
				exit();
			}
		}

		// Create Token
		$this->CreateToken();

		// Setup other options
		$this->SetupOtherOptions();

		// Set up custom action (compatible with old version)
		foreach ($this->CustomActions as $name => $action)
			$this->ListActions->Add($name, $action);

		// Show checkbox column if multiple action
		foreach ($this->ListActions->Items as $listaction) {
			if ($listaction->Select == EW_ACTION_MULTIPLE && $listaction->Allow) {
				$this->ListOptions->Items["checkbox"]->Visible = TRUE;
				break;
			}
		}
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $CCon;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($CCon);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}

	// Class variables
	var $ListOptions; // List options
	var $ExportOptions; // Export options
	var $SearchOptions; // Search options
	var $OtherOptions = array(); // Other options
	var $FilterOptions; // Filter options
	var $ListActions; // List actions
	var $SelectedCount = 0;
	var $SelectedIndex = 0;
	var $DisplayRecs = 20;
	var $StartRec;
	var $StopRec;
	var $TotalRecs = 0;
	var $RecRange = 10;
	var $Pager;
	var $DefaultSearchWhere = ""; // Default search WHERE clause
	var $SearchWhere = ""; // Search WHERE clause
	var $RecCnt = 0; // Record count
	var $EditRowCnt;
	var $StartRowCnt = 1;
	var $RowCnt = 0;
	var $Attrs = array(); // Row attributes and cell attributes
	var $RowIndex = 0; // Row index
	var $KeyCount = 0; // Key count
	var $RowAction = ""; // Row action
	var $RowOldKey = ""; // Row old key (for copy)
	var $RecPerRow = 0;
	var $MultiColumnClass;
	var $MultiColumnEditClass = "col-sm-12";
	var $MultiColumnCnt = 12;
	var $MultiColumnEditCnt = 12;
	var $GridCnt = 0;
	var $ColCnt = 0;
	var $DbMasterFilter = ""; // Master filter
	var $DbDetailFilter = ""; // Detail filter
	var $MasterRecordExists;	
	var $MultiSelectKey;
	var $Command;
	var $RestoreSearch = FALSE;
	var $DetailPages;
	var $Recordset;
	var $OldRecordset;

	//
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError, $gsSearchError, $Security;

		// Search filters
		$sSrchAdvanced = ""; // Advanced search filter
		$sSrchBasic = ""; // Basic search filter
		$sFilter = "";

		// Get command
		$this->Command = strtolower(@$_GET["cmd"]);
		if ($this->IsPageRequest()) { // Validate request

			// Process list action first
			if ($this->ProcessListAction()) // Ajax request
				$this->Page_Terminate();

			// Handle reset command
			$this->ResetCmd();

			// Set up Breadcrumb
			if ($this->Export == "")
				$this->SetupBreadcrumb();

			// Check QueryString parameters
			if (@$_GET["a"] <> "") {
				$this->CurrentAction = $_GET["a"];

				// Clear inline mode
				if ($this->CurrentAction == "cancel")
					$this->ClearInlineMode();

				// Switch to grid edit mode
				if ($this->CurrentAction == "gridedit")
					$this->GridEditMode();

				// Switch to grid add mode
				if ($this->CurrentAction == "gridadd")
					$this->GridAddMode();
			} else {
				if (@$_POST["a_list"] <> "") {
					$this->CurrentAction = $_POST["a_list"]; // Get action

					// Grid Update
					if (($this->CurrentAction == "gridupdate" || $this->CurrentAction == "gridoverwrite") && @$_SESSION[EW_SESSION_INLINE_MODE] == "gridedit") {
						if ($this->ValidateGridForm()) {
							$bGridUpdate = $this->GridUpdate();
						} else {
							$bGridUpdate = FALSE;
							$this->setFailureMessage($gsFormError);
						}
						if (!$bGridUpdate) {
							$this->EventCancelled = TRUE;
							$this->CurrentAction = "gridedit"; // Stay in Grid Edit mode
						}
					}

					// Grid Insert
					if ($this->CurrentAction == "gridinsert" && @$_SESSION[EW_SESSION_INLINE_MODE] == "gridadd") {
						if ($this->ValidateGridForm()) {
							$bGridInsert = $this->GridInsert();
						} else {
							$bGridInsert = FALSE;
							$this->setFailureMessage($gsFormError);
						}
						if (!$bGridInsert) {
							$this->EventCancelled = TRUE;
							$this->CurrentAction = "gridadd"; // Stay in Grid Add mode
						}
					}
				}
			}

			// Hide list options
			if ($this->Export <> "") {
				$this->ListOptions->HideAllOptions(array("sequence"));
				$this->ListOptions->UseDropDownButton = FALSE; // Disable drop down button
				$this->ListOptions->UseButtonGroup = FALSE; // Disable button group
			} elseif ($this->CurrentAction == "gridadd" || $this->CurrentAction == "gridedit") {
				$this->ListOptions->HideAllOptions();
				$this->ListOptions->UseDropDownButton = FALSE; // Disable drop down button
				$this->ListOptions->UseButtonGroup = FALSE; // Disable button group
			}

			// Hide options
			if ($this->Export <> "" || $this->CurrentAction <> "") {
				$this->ExportOptions->HideAllOptions();
				$this->FilterOptions->HideAllOptions();
			}

			// Hide other options
			if ($this->Export <> "") {
				foreach ($this->OtherOptions as &$option)
					$option->HideAllOptions();
			}

			// Show grid delete link for grid add / grid edit
			if ($this->AllowAddDeleteRow) {
				if ($this->CurrentAction == "gridadd" || $this->CurrentAction == "gridedit") {
					$item = $this->ListOptions->GetItem("griddelete");
					if ($item) $item->Visible = TRUE;
				}
			}

			// Get default search criteria
			ew_AddFilter($this->DefaultSearchWhere, $this->BasicSearchWhere(TRUE));

			// Get basic search values
			$this->LoadBasicSearchValues();

			// Restore filter list
			$this->RestoreFilterList();

			// Restore search parms from Session if not searching / reset / export
			if (($this->Export <> "" || $this->Command <> "search" && $this->Command <> "reset" && $this->Command <> "resetall") && $this->CheckSearchParms())
				$this->RestoreSearchParms();

			// Call Recordset SearchValidated event
			$this->Recordset_SearchValidated();

			// Set up sorting order
			$this->SetUpSortOrder();

			// Get basic search criteria
			if ($gsSearchError == "")
				$sSrchBasic = $this->BasicSearchWhere();
		}

		// Restore display records
		if ($this->getRecordsPerPage() <> "") {
			$this->DisplayRecs = $this->getRecordsPerPage(); // Restore from Session
		} else {
			$this->DisplayRecs = 20; // Load default
		}

		// Load Sorting Order
		$this->LoadSortOrder();

		// Load search default if no existing search criteria
		if (!$this->CheckSearchParms()) {

			// Load basic search from default
			$this->BasicSearch->LoadDefault();
			if ($this->BasicSearch->Keyword != "")
				$sSrchBasic = $this->BasicSearchWhere();
		}

		// Build search criteria
		ew_AddFilter($this->SearchWhere, $sSrchAdvanced);
		ew_AddFilter($this->SearchWhere, $sSrchBasic);

		// Call Recordset_Searching event
		$this->Recordset_Searching($this->SearchWhere);

		// Save search criteria
		if ($this->Command == "search" && !$this->RestoreSearch) {
			$this->setSearchWhere($this->SearchWhere); // Save to Session
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} else {
			$this->SearchWhere = $this->getSearchWhere();
		}

		// Build filter
		$sFilter = "";
		if (!$Security->CanList())
			$sFilter = "(0=1)"; // Filter all records
		ew_AddFilter($sFilter, $this->DbDetailFilter);
		ew_AddFilter($sFilter, $this->SearchWhere);

		// Set up filter in session
		$this->setSessionWhere($sFilter);
		$this->CurrentFilter = "";

		// Load record count first
		if (!$this->IsAddOrEdit()) {
			$bSelectLimit = $this->UseSelectLimit;
			if ($bSelectLimit) {
				$this->TotalRecs = $this->SelectRecordCount();
			} else {
				if ($this->Recordset = $this->LoadRecordset())
					$this->TotalRecs = $this->Recordset->RecordCount();
			}
		}

		// Search options
		$this->SetupSearchOptions();
	}

	//  Exit inline mode
	function ClearInlineMode() {
		$this->CcoMont->FormValue = ""; // Clear form value
		$this->LastAction = $this->CurrentAction; // Save last action
		$this->CurrentAction = ""; // Clear action
		$_SESSION[EW_SESSION_INLINE_MODE] = ""; // Clear inline mode
	}

	// Switch to Grid Add mode
	function GridAddMode() {
		$_SESSION[EW_SESSION_INLINE_MODE] = "gridadd"; // Enabled grid add
	}

	// Switch to Grid Edit mode
	function GridEditMode() {
		$_SESSION[EW_SESSION_INLINE_MODE] = "gridedit"; // Enable grid edit
	}

	// Perform update to grid
	function GridUpdate() {
		global $Language, $objForm, $gsFormError;
		$bGridUpdate = TRUE;

		// Get old recordset
		$this->CurrentFilter = $this->BuildKeyFilter();
		if ($this->CurrentFilter == "")
			$this->CurrentFilter = "0=1";
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		if ($rs = $conn->Execute($sSql)) {
			$rsold = $rs->GetRows();
			$rs->Close();
		}

		// Call Grid Updating event
		if (!$this->Grid_Updating($rsold)) {
			if ($this->getFailureMessage() == "")
				$this->setFailureMessage($Language->Phrase("GridEditCancelled")); // Set grid edit cancelled message
			return FALSE;
		}

		// Begin transaction
		$conn->BeginTrans();
		$sKey = "";

		// Update row index and get row key
		$objForm->Index = -1;
		$rowcnt = strval($objForm->GetValue($this->FormKeyCountName));
		if ($rowcnt == "" || !is_numeric($rowcnt))
			$rowcnt = 0;

		// Update all rows based on key
		for ($rowindex = 1; $rowindex <= $rowcnt; $rowindex++) {
			$objForm->Index = $rowindex;
			$rowkey = strval($objForm->GetValue($this->FormKeyName));
			$rowaction = strval($objForm->GetValue($this->FormActionName));

			// Load all values and keys
			if ($rowaction <> "insertdelete") { // Skip insert then deleted rows
				$this->LoadFormValues(); // Get form values
				if ($rowaction == "" || $rowaction == "edit" || $rowaction == "delete") {
					$bGridUpdate = $this->SetupKeyValues($rowkey); // Set up key values
				} else {
					$bGridUpdate = TRUE;
				}

				// Skip empty row
				if ($rowaction == "insert" && $this->EmptyRow()) {

					// No action required
				// Validate form and insert/update/delete record

				} elseif ($bGridUpdate) {
					if ($rowaction == "delete") {
						$this->CurrentFilter = $this->KeyFilter();
						$bGridUpdate = $this->DeleteRows(); // Delete this row
					} else if (!$this->ValidateForm()) {
						$bGridUpdate = FALSE; // Form error, reset action
						$this->setFailureMessage($gsFormError);
					} else {
						if ($rowaction == "insert") {
							$bGridUpdate = $this->AddRow(); // Insert this row
						} else {
							if ($rowkey <> "") {
								$this->SendEmail = FALSE; // Do not send email on update success
								$bGridUpdate = $this->EditRow(); // Update this row
							}
						} // End update
					}
				}
				if ($bGridUpdate) {
					if ($sKey <> "") $sKey .= ", ";
					$sKey .= $rowkey;
				} else {
					break;
				}
			}
		}
		if ($bGridUpdate) {
			$conn->CommitTrans(); // Commit transaction

			// Get new recordset
			if ($rs = $conn->Execute($sSql)) {
				$rsnew = $rs->GetRows();
				$rs->Close();
			}

			// Call Grid_Updated event
			$this->Grid_Updated($rsold, $rsnew);
			if ($this->getSuccessMessage() == "")
				$this->setSuccessMessage($Language->Phrase("UpdateSuccess")); // Set up update success message
			$this->ClearInlineMode(); // Clear inline edit mode
		} else {
			$conn->RollbackTrans(); // Rollback transaction
			if ($this->getFailureMessage() == "")
				$this->setFailureMessage($Language->Phrase("UpdateFailed")); // Set update failed message
		}
		return $bGridUpdate;
	}

	// Build filter for all keys
	function BuildKeyFilter() {
		global $objForm;
		$sWrkFilter = "";

		// Update row index and get row key
		$rowindex = 1;
		$objForm->Index = $rowindex;
		$sThisKey = strval($objForm->GetValue($this->FormKeyName));
		while ($sThisKey <> "") {
			if ($this->SetupKeyValues($sThisKey)) {
				$sFilter = $this->KeyFilter();
				if ($sWrkFilter <> "") $sWrkFilter .= " OR ";
				$sWrkFilter .= $sFilter;
			} else {
				$sWrkFilter = "0=1";
				break;
			}

			// Update row index and get row key
			$rowindex++; // Next row
			$objForm->Index = $rowindex;
			$sThisKey = strval($objForm->GetValue($this->FormKeyName));
		}
		return $sWrkFilter;
	}

	// Set up key values
	function SetupKeyValues($key) {
		$arrKeyFlds = explode($GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"], $key);
		if (count($arrKeyFlds) >= 1) {
			$this->CcoCodi->setFormValue($arrKeyFlds[0]);
			if (!is_numeric($this->CcoCodi->FormValue))
				return FALSE;
		}
		return TRUE;
	}

	// Perform Grid Add
	function GridInsert() {
		global $Language, $objForm, $gsFormError;
		$rowindex = 1;
		$bGridInsert = FALSE;
		$conn = &$this->Connection();

		// Call Grid Inserting event
		if (!$this->Grid_Inserting()) {
			if ($this->getFailureMessage() == "") {
				$this->setFailureMessage($Language->Phrase("GridAddCancelled")); // Set grid add cancelled message
			}
			return FALSE;
		}

		// Begin transaction
		$conn->BeginTrans();

		// Init key filter
		$sWrkFilter = "";
		$addcnt = 0;
		$sKey = "";

		// Get row count
		$objForm->Index = -1;
		$rowcnt = strval($objForm->GetValue($this->FormKeyCountName));
		if ($rowcnt == "" || !is_numeric($rowcnt))
			$rowcnt = 0;

		// Insert all rows
		for ($rowindex = 1; $rowindex <= $rowcnt; $rowindex++) {

			// Load current row values
			$objForm->Index = $rowindex;
			$rowaction = strval($objForm->GetValue($this->FormActionName));
			if ($rowaction <> "" && $rowaction <> "insert")
				continue; // Skip
			$this->LoadFormValues(); // Get form values
			if (!$this->EmptyRow()) {
				$addcnt++;
				$this->SendEmail = FALSE; // Do not send email on insert success

				// Validate form
				if (!$this->ValidateForm()) {
					$bGridInsert = FALSE; // Form error, reset action
					$this->setFailureMessage($gsFormError);
				} else {
					$bGridInsert = $this->AddRow($this->OldRecordset); // Insert this row
				}
				if ($bGridInsert) {
					if ($sKey <> "") $sKey .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
					$sKey .= $this->CcoCodi->CurrentValue;

					// Add filter for this record
					$sFilter = $this->KeyFilter();
					if ($sWrkFilter <> "") $sWrkFilter .= " OR ";
					$sWrkFilter .= $sFilter;
				} else {
					break;
				}
			}
		}
		if ($addcnt == 0) { // No record inserted
			$this->setFailureMessage($Language->Phrase("NoAddRecord"));
			$bGridInsert = FALSE;
		}
		if ($bGridInsert) {
			$conn->CommitTrans(); // Commit transaction

			// Get new recordset
			$this->CurrentFilter = $sWrkFilter;
			$sSql = $this->SQL();
			if ($rs = $conn->Execute($sSql)) {
				$rsnew = $rs->GetRows();
				$rs->Close();
			}

			// Call Grid_Inserted event
			$this->Grid_Inserted($rsnew);
			if ($this->getSuccessMessage() == "")
				$this->setSuccessMessage($Language->Phrase("InsertSuccess")); // Set up insert success message
			$this->ClearInlineMode(); // Clear grid add mode
		} else {
			$conn->RollbackTrans(); // Rollback transaction
			if ($this->getFailureMessage() == "") {
				$this->setFailureMessage($Language->Phrase("InsertFailed")); // Set insert failed message
			}
		}
		return $bGridInsert;
	}

	// Check if empty row
	function EmptyRow() {
		global $objForm;
		if ($objForm->HasValue("x_VcoCodi") && $objForm->HasValue("o_VcoCodi") && $this->VcoCodi->CurrentValue <> $this->VcoCodi->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_CcoFPag") && $objForm->HasValue("o_CcoFPag") && $this->CcoFPag->CurrentValue <> $this->CcoFPag->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_CcoMont") && $objForm->HasValue("o_CcoMont") && $this->CcoMont->CurrentValue <> $this->CcoMont->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_CcoEsta") && $objForm->HasValue("o_CcoEsta") && $this->CcoEsta->CurrentValue <> $this->CcoEsta->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_CcoAnho") && $objForm->HasValue("o_CcoAnho") && $this->CcoAnho->CurrentValue <> $this->CcoAnho->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_CcoMes") && $objForm->HasValue("o_CcoMes") && $this->CcoMes->CurrentValue <> $this->CcoMes->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_CcoConc") && $objForm->HasValue("o_CcoConc") && $this->CcoConc->CurrentValue <> $this->CcoConc->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_CcoUsua") && $objForm->HasValue("o_CcoUsua") && $this->CcoUsua->CurrentValue <> $this->CcoUsua->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_CcoFCre") && $objForm->HasValue("o_CcoFCre") && $this->CcoFCre->CurrentValue <> $this->CcoFCre->OldValue)
			return FALSE;
		return TRUE;
	}

	// Validate grid form
	function ValidateGridForm() {
		global $objForm;

		// Get row count
		$objForm->Index = -1;
		$rowcnt = strval($objForm->GetValue($this->FormKeyCountName));
		if ($rowcnt == "" || !is_numeric($rowcnt))
			$rowcnt = 0;

		// Validate all records
		for ($rowindex = 1; $rowindex <= $rowcnt; $rowindex++) {

			// Load current row values
			$objForm->Index = $rowindex;
			$rowaction = strval($objForm->GetValue($this->FormActionName));
			if ($rowaction <> "delete" && $rowaction <> "insertdelete") {
				$this->LoadFormValues(); // Get form values
				if ($rowaction == "insert" && $this->EmptyRow()) {

					// Ignore
				} else if (!$this->ValidateForm()) {
					return FALSE;
				}
			}
		}
		return TRUE;
	}

	// Get all form values of the grid
	function GetGridFormValues() {
		global $objForm;

		// Get row count
		$objForm->Index = -1;
		$rowcnt = strval($objForm->GetValue($this->FormKeyCountName));
		if ($rowcnt == "" || !is_numeric($rowcnt))
			$rowcnt = 0;
		$rows = array();

		// Loop through all records
		for ($rowindex = 1; $rowindex <= $rowcnt; $rowindex++) {

			// Load current row values
			$objForm->Index = $rowindex;
			$rowaction = strval($objForm->GetValue($this->FormActionName));
			if ($rowaction <> "delete" && $rowaction <> "insertdelete") {
				$this->LoadFormValues(); // Get form values
				if ($rowaction == "insert" && $this->EmptyRow()) {

					// Ignore
				} else {
					$rows[] = $this->GetFieldValues("FormValue"); // Return row as array
				}
			}
		}
		return $rows; // Return as array of array
	}

	// Restore form values for current row
	function RestoreCurrentRowFormValues($idx) {
		global $objForm;

		// Get row based on current index
		$objForm->Index = $idx;
		$this->LoadFormValues(); // Load form values
	}

	// Get list of filters
	function GetFilterList() {

		// Initialize
		$sFilterList = "";
		$sFilterList = ew_Concat($sFilterList, $this->CcoCodi->AdvancedSearch->ToJSON(), ","); // Field CcoCodi
		$sFilterList = ew_Concat($sFilterList, $this->VcoCodi->AdvancedSearch->ToJSON(), ","); // Field VcoCodi
		$sFilterList = ew_Concat($sFilterList, $this->CcoFPag->AdvancedSearch->ToJSON(), ","); // Field CcoFPag
		$sFilterList = ew_Concat($sFilterList, $this->CcoMont->AdvancedSearch->ToJSON(), ","); // Field CcoMont
		$sFilterList = ew_Concat($sFilterList, $this->CcoEsta->AdvancedSearch->ToJSON(), ","); // Field CcoEsta
		$sFilterList = ew_Concat($sFilterList, $this->CcoAnho->AdvancedSearch->ToJSON(), ","); // Field CcoAnho
		$sFilterList = ew_Concat($sFilterList, $this->CcoMes->AdvancedSearch->ToJSON(), ","); // Field CcoMes
		$sFilterList = ew_Concat($sFilterList, $this->CcoConc->AdvancedSearch->ToJSON(), ","); // Field CcoConc
		$sFilterList = ew_Concat($sFilterList, $this->CcoUsua->AdvancedSearch->ToJSON(), ","); // Field CcoUsua
		$sFilterList = ew_Concat($sFilterList, $this->CcoFCre->AdvancedSearch->ToJSON(), ","); // Field CcoFCre
		if ($this->BasicSearch->Keyword <> "") {
			$sWrk = "\"" . EW_TABLE_BASIC_SEARCH . "\":\"" . ew_JsEncode2($this->BasicSearch->Keyword) . "\",\"" . EW_TABLE_BASIC_SEARCH_TYPE . "\":\"" . ew_JsEncode2($this->BasicSearch->Type) . "\"";
			$sFilterList = ew_Concat($sFilterList, $sWrk, ",");
		}

		// Return filter list in json
		return ($sFilterList <> "") ? "{" . $sFilterList . "}" : "null";
	}

	// Restore list of filters
	function RestoreFilterList() {

		// Return if not reset filter
		if (@$_POST["cmd"] <> "resetfilter")
			return FALSE;
		$filter = json_decode(ew_StripSlashes(@$_POST["filter"]), TRUE);
		$this->Command = "search";

		// Field CcoCodi
		$this->CcoCodi->AdvancedSearch->SearchValue = @$filter["x_CcoCodi"];
		$this->CcoCodi->AdvancedSearch->SearchOperator = @$filter["z_CcoCodi"];
		$this->CcoCodi->AdvancedSearch->SearchCondition = @$filter["v_CcoCodi"];
		$this->CcoCodi->AdvancedSearch->SearchValue2 = @$filter["y_CcoCodi"];
		$this->CcoCodi->AdvancedSearch->SearchOperator2 = @$filter["w_CcoCodi"];
		$this->CcoCodi->AdvancedSearch->Save();

		// Field VcoCodi
		$this->VcoCodi->AdvancedSearch->SearchValue = @$filter["x_VcoCodi"];
		$this->VcoCodi->AdvancedSearch->SearchOperator = @$filter["z_VcoCodi"];
		$this->VcoCodi->AdvancedSearch->SearchCondition = @$filter["v_VcoCodi"];
		$this->VcoCodi->AdvancedSearch->SearchValue2 = @$filter["y_VcoCodi"];
		$this->VcoCodi->AdvancedSearch->SearchOperator2 = @$filter["w_VcoCodi"];
		$this->VcoCodi->AdvancedSearch->Save();

		// Field CcoFPag
		$this->CcoFPag->AdvancedSearch->SearchValue = @$filter["x_CcoFPag"];
		$this->CcoFPag->AdvancedSearch->SearchOperator = @$filter["z_CcoFPag"];
		$this->CcoFPag->AdvancedSearch->SearchCondition = @$filter["v_CcoFPag"];
		$this->CcoFPag->AdvancedSearch->SearchValue2 = @$filter["y_CcoFPag"];
		$this->CcoFPag->AdvancedSearch->SearchOperator2 = @$filter["w_CcoFPag"];
		$this->CcoFPag->AdvancedSearch->Save();

		// Field CcoMont
		$this->CcoMont->AdvancedSearch->SearchValue = @$filter["x_CcoMont"];
		$this->CcoMont->AdvancedSearch->SearchOperator = @$filter["z_CcoMont"];
		$this->CcoMont->AdvancedSearch->SearchCondition = @$filter["v_CcoMont"];
		$this->CcoMont->AdvancedSearch->SearchValue2 = @$filter["y_CcoMont"];
		$this->CcoMont->AdvancedSearch->SearchOperator2 = @$filter["w_CcoMont"];
		$this->CcoMont->AdvancedSearch->Save();

		// Field CcoEsta
		$this->CcoEsta->AdvancedSearch->SearchValue = @$filter["x_CcoEsta"];
		$this->CcoEsta->AdvancedSearch->SearchOperator = @$filter["z_CcoEsta"];
		$this->CcoEsta->AdvancedSearch->SearchCondition = @$filter["v_CcoEsta"];
		$this->CcoEsta->AdvancedSearch->SearchValue2 = @$filter["y_CcoEsta"];
		$this->CcoEsta->AdvancedSearch->SearchOperator2 = @$filter["w_CcoEsta"];
		$this->CcoEsta->AdvancedSearch->Save();

		// Field CcoAnho
		$this->CcoAnho->AdvancedSearch->SearchValue = @$filter["x_CcoAnho"];
		$this->CcoAnho->AdvancedSearch->SearchOperator = @$filter["z_CcoAnho"];
		$this->CcoAnho->AdvancedSearch->SearchCondition = @$filter["v_CcoAnho"];
		$this->CcoAnho->AdvancedSearch->SearchValue2 = @$filter["y_CcoAnho"];
		$this->CcoAnho->AdvancedSearch->SearchOperator2 = @$filter["w_CcoAnho"];
		$this->CcoAnho->AdvancedSearch->Save();

		// Field CcoMes
		$this->CcoMes->AdvancedSearch->SearchValue = @$filter["x_CcoMes"];
		$this->CcoMes->AdvancedSearch->SearchOperator = @$filter["z_CcoMes"];
		$this->CcoMes->AdvancedSearch->SearchCondition = @$filter["v_CcoMes"];
		$this->CcoMes->AdvancedSearch->SearchValue2 = @$filter["y_CcoMes"];
		$this->CcoMes->AdvancedSearch->SearchOperator2 = @$filter["w_CcoMes"];
		$this->CcoMes->AdvancedSearch->Save();

		// Field CcoConc
		$this->CcoConc->AdvancedSearch->SearchValue = @$filter["x_CcoConc"];
		$this->CcoConc->AdvancedSearch->SearchOperator = @$filter["z_CcoConc"];
		$this->CcoConc->AdvancedSearch->SearchCondition = @$filter["v_CcoConc"];
		$this->CcoConc->AdvancedSearch->SearchValue2 = @$filter["y_CcoConc"];
		$this->CcoConc->AdvancedSearch->SearchOperator2 = @$filter["w_CcoConc"];
		$this->CcoConc->AdvancedSearch->Save();

		// Field CcoUsua
		$this->CcoUsua->AdvancedSearch->SearchValue = @$filter["x_CcoUsua"];
		$this->CcoUsua->AdvancedSearch->SearchOperator = @$filter["z_CcoUsua"];
		$this->CcoUsua->AdvancedSearch->SearchCondition = @$filter["v_CcoUsua"];
		$this->CcoUsua->AdvancedSearch->SearchValue2 = @$filter["y_CcoUsua"];
		$this->CcoUsua->AdvancedSearch->SearchOperator2 = @$filter["w_CcoUsua"];
		$this->CcoUsua->AdvancedSearch->Save();

		// Field CcoFCre
		$this->CcoFCre->AdvancedSearch->SearchValue = @$filter["x_CcoFCre"];
		$this->CcoFCre->AdvancedSearch->SearchOperator = @$filter["z_CcoFCre"];
		$this->CcoFCre->AdvancedSearch->SearchCondition = @$filter["v_CcoFCre"];
		$this->CcoFCre->AdvancedSearch->SearchValue2 = @$filter["y_CcoFCre"];
		$this->CcoFCre->AdvancedSearch->SearchOperator2 = @$filter["w_CcoFCre"];
		$this->CcoFCre->AdvancedSearch->Save();
		$this->BasicSearch->setKeyword(@$filter[EW_TABLE_BASIC_SEARCH]);
		$this->BasicSearch->setType(@$filter[EW_TABLE_BASIC_SEARCH_TYPE]);
	}

	// Return basic search SQL
	function BasicSearchSQL($arKeywords, $type) {
		$sWhere = "";
		$this->BuildBasicSearchSQL($sWhere, $this->CcoEsta, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->CcoAnho, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->CcoMes, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->CcoConc, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->CcoUsua, $arKeywords, $type);
		return $sWhere;
	}

	// Build basic search SQL
	function BuildBasicSearchSql(&$Where, &$Fld, $arKeywords, $type) {
		$sDefCond = ($type == "OR") ? "OR" : "AND";
		$arSQL = array(); // Array for SQL parts
		$arCond = array(); // Array for search conditions
		$cnt = count($arKeywords);
		$j = 0; // Number of SQL parts
		for ($i = 0; $i < $cnt; $i++) {
			$Keyword = $arKeywords[$i];
			$Keyword = trim($Keyword);
			if (EW_BASIC_SEARCH_IGNORE_PATTERN <> "") {
				$Keyword = preg_replace(EW_BASIC_SEARCH_IGNORE_PATTERN, "\\", $Keyword);
				$ar = explode("\\", $Keyword);
			} else {
				$ar = array($Keyword);
			}
			foreach ($ar as $Keyword) {
				if ($Keyword <> "") {
					$sWrk = "";
					if ($Keyword == "OR" && $type == "") {
						if ($j > 0)
							$arCond[$j-1] = "OR";
					} elseif ($Keyword == EW_NULL_VALUE) {
						$sWrk = $Fld->FldExpression . " IS NULL";
					} elseif ($Keyword == EW_NOT_NULL_VALUE) {
						$sWrk = $Fld->FldExpression . " IS NOT NULL";
					} elseif ($Fld->FldIsVirtual && $Fld->FldVirtualSearch) {
						$sWrk = $Fld->FldVirtualExpression . ew_Like(ew_QuotedValue("%" . $Keyword . "%", EW_DATATYPE_STRING, $this->DBID), $this->DBID);
					} elseif ($Fld->FldDataType != EW_DATATYPE_NUMBER || is_numeric($Keyword)) {
						$sWrk = $Fld->FldBasicSearchExpression . ew_Like(ew_QuotedValue("%" . $Keyword . "%", EW_DATATYPE_STRING, $this->DBID), $this->DBID);
					}
					if ($sWrk <> "") {
						$arSQL[$j] = $sWrk;
						$arCond[$j] = $sDefCond;
						$j += 1;
					}
				}
			}
		}
		$cnt = count($arSQL);
		$bQuoted = FALSE;
		$sSql = "";
		if ($cnt > 0) {
			for ($i = 0; $i < $cnt-1; $i++) {
				if ($arCond[$i] == "OR") {
					if (!$bQuoted) $sSql .= "(";
					$bQuoted = TRUE;
				}
				$sSql .= $arSQL[$i];
				if ($bQuoted && $arCond[$i] <> "OR") {
					$sSql .= ")";
					$bQuoted = FALSE;
				}
				$sSql .= " " . $arCond[$i] . " ";
			}
			$sSql .= $arSQL[$cnt-1];
			if ($bQuoted)
				$sSql .= ")";
		}
		if ($sSql <> "") {
			if ($Where <> "") $Where .= " OR ";
			$Where .=  "(" . $sSql . ")";
		}
	}

	// Return basic search WHERE clause based on search keyword and type
	function BasicSearchWhere($Default = FALSE) {
		global $Security;
		$sSearchStr = "";
		if (!$Security->CanSearch()) return "";
		$sSearchKeyword = ($Default) ? $this->BasicSearch->KeywordDefault : $this->BasicSearch->Keyword;
		$sSearchType = ($Default) ? $this->BasicSearch->TypeDefault : $this->BasicSearch->Type;
		if ($sSearchKeyword <> "") {
			$sSearch = trim($sSearchKeyword);
			if ($sSearchType <> "=") {
				$ar = array();

				// Match quoted keywords (i.e.: "...")
				if (preg_match_all('/"([^"]*)"/i', $sSearch, $matches, PREG_SET_ORDER)) {
					foreach ($matches as $match) {
						$p = strpos($sSearch, $match[0]);
						$str = substr($sSearch, 0, $p);
						$sSearch = substr($sSearch, $p + strlen($match[0]));
						if (strlen(trim($str)) > 0)
							$ar = array_merge($ar, explode(" ", trim($str)));
						$ar[] = $match[1]; // Save quoted keyword
					}
				}

				// Match individual keywords
				if (strlen(trim($sSearch)) > 0)
					$ar = array_merge($ar, explode(" ", trim($sSearch)));

				// Search keyword in any fields
				if (($sSearchType == "OR" || $sSearchType == "AND") && $this->BasicSearch->BasicSearchAnyFields) {
					foreach ($ar as $sKeyword) {
						if ($sKeyword <> "") {
							if ($sSearchStr <> "") $sSearchStr .= " " . $sSearchType . " ";
							$sSearchStr .= "(" . $this->BasicSearchSQL(array($sKeyword), $sSearchType) . ")";
						}
					}
				} else {
					$sSearchStr = $this->BasicSearchSQL($ar, $sSearchType);
				}
			} else {
				$sSearchStr = $this->BasicSearchSQL(array($sSearch), $sSearchType);
			}
			if (!$Default) $this->Command = "search";
		}
		if (!$Default && $this->Command == "search") {
			$this->BasicSearch->setKeyword($sSearchKeyword);
			$this->BasicSearch->setType($sSearchType);
		}
		return $sSearchStr;
	}

	// Check if search parm exists
	function CheckSearchParms() {

		// Check basic search
		if ($this->BasicSearch->IssetSession())
			return TRUE;
		return FALSE;
	}

	// Clear all search parameters
	function ResetSearchParms() {

		// Clear search WHERE clause
		$this->SearchWhere = "";
		$this->setSearchWhere($this->SearchWhere);

		// Clear basic search parameters
		$this->ResetBasicSearchParms();
	}

	// Load advanced search default values
	function LoadAdvancedSearchDefault() {
		return FALSE;
	}

	// Clear all basic search parameters
	function ResetBasicSearchParms() {
		$this->BasicSearch->UnsetSession();
	}

	// Restore all search parameters
	function RestoreSearchParms() {
		$this->RestoreSearch = TRUE;

		// Restore basic search values
		$this->BasicSearch->Load();
	}

	// Set up sort parameters
	function SetUpSortOrder() {

		// Check for Ctrl pressed
		$bCtrl = (@$_GET["ctrl"] <> "");

		// Check for "order" parameter
		if (@$_GET["order"] <> "") {
			$this->CurrentOrder = ew_StripSlashes(@$_GET["order"]);
			$this->CurrentOrderType = @$_GET["ordertype"];
			$this->UpdateSort($this->CcoCodi, $bCtrl); // CcoCodi
			$this->UpdateSort($this->VcoCodi, $bCtrl); // VcoCodi
			$this->UpdateSort($this->CcoFPag, $bCtrl); // CcoFPag
			$this->UpdateSort($this->CcoMont, $bCtrl); // CcoMont
			$this->UpdateSort($this->CcoEsta, $bCtrl); // CcoEsta
			$this->UpdateSort($this->CcoAnho, $bCtrl); // CcoAnho
			$this->UpdateSort($this->CcoMes, $bCtrl); // CcoMes
			$this->UpdateSort($this->CcoConc, $bCtrl); // CcoConc
			$this->UpdateSort($this->CcoUsua, $bCtrl); // CcoUsua
			$this->UpdateSort($this->CcoFCre, $bCtrl); // CcoFCre
			$this->setStartRecordNumber(1); // Reset start position
		}
	}

	// Load sort order parameters
	function LoadSortOrder() {
		$sOrderBy = $this->getSessionOrderBy(); // Get ORDER BY from Session
		if ($sOrderBy == "") {
			if ($this->getSqlOrderBy() <> "") {
				$sOrderBy = $this->getSqlOrderBy();
				$this->setSessionOrderBy($sOrderBy);
			}
		}
	}

	// Reset command
	// - cmd=reset (Reset search parameters)
	// - cmd=resetall (Reset search and master/detail parameters)
	// - cmd=resetsort (Reset sort parameters)
	function ResetCmd() {

		// Check if reset command
		if (substr($this->Command,0,5) == "reset") {

			// Reset search criteria
			if ($this->Command == "reset" || $this->Command == "resetall")
				$this->ResetSearchParms();

			// Reset sorting order
			if ($this->Command == "resetsort") {
				$sOrderBy = "";
				$this->setSessionOrderBy($sOrderBy);
				$this->CcoCodi->setSort("");
				$this->VcoCodi->setSort("");
				$this->CcoFPag->setSort("");
				$this->CcoMont->setSort("");
				$this->CcoEsta->setSort("");
				$this->CcoAnho->setSort("");
				$this->CcoMes->setSort("");
				$this->CcoConc->setSort("");
				$this->CcoUsua->setSort("");
				$this->CcoFCre->setSort("");
			}

			// Reset start position
			$this->StartRec = 1;
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Set up list options
	function SetupListOptions() {
		global $Security, $Language;

		// "griddelete"
		if ($this->AllowAddDeleteRow) {
			$item = &$this->ListOptions->Add("griddelete");
			$item->CssStyle = "white-space: nowrap;";
			$item->OnLeft = FALSE;
			$item->Visible = FALSE; // Default hidden
		}

		// Add group option item
		$item = &$this->ListOptions->Add($this->ListOptions->GroupOptionName);
		$item->Body = "";
		$item->OnLeft = FALSE;
		$item->Visible = FALSE;

		// "view"
		$item = &$this->ListOptions->Add("view");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->CanView();
		$item->OnLeft = FALSE;

		// "edit"
		$item = &$this->ListOptions->Add("edit");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->CanEdit();
		$item->OnLeft = FALSE;

		// "copy"
		$item = &$this->ListOptions->Add("copy");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->CanAdd();
		$item->OnLeft = FALSE;

		// "delete"
		$item = &$this->ListOptions->Add("delete");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->CanDelete();
		$item->OnLeft = FALSE;

		// List actions
		$item = &$this->ListOptions->Add("listactions");
		$item->CssStyle = "white-space: nowrap;";
		$item->OnLeft = FALSE;
		$item->Visible = FALSE;
		$item->ShowInButtonGroup = FALSE;
		$item->ShowInDropDown = FALSE;

		// "checkbox"
		$item = &$this->ListOptions->Add("checkbox");
		$item->Visible = FALSE;
		$item->OnLeft = FALSE;
		$item->Header = "<input type=\"checkbox\" name=\"key\" id=\"key\" onclick=\"ew_SelectAllKey(this);\">";
		$item->ShowInDropDown = FALSE;
		$item->ShowInButtonGroup = FALSE;

		// Drop down button for ListOptions
		$this->ListOptions->UseImageAndText = TRUE;
		$this->ListOptions->UseDropDownButton = FALSE;
		$this->ListOptions->DropDownButtonPhrase = $Language->Phrase("ButtonListOptions");
		$this->ListOptions->UseButtonGroup = FALSE;
		if ($this->ListOptions->UseButtonGroup && ew_IsMobile())
			$this->ListOptions->UseDropDownButton = TRUE;
		$this->ListOptions->ButtonClass = "btn-sm"; // Class for button group

		// Call ListOptions_Load event
		$this->ListOptions_Load();
		$this->SetupListOptionsExt();
		$item = &$this->ListOptions->GetItem($this->ListOptions->GroupOptionName);
		$item->Visible = $this->ListOptions->GroupOptionVisible();
	}

	// Render list options
	function RenderListOptions() {
		global $Security, $Language, $objForm;
		$this->ListOptions->LoadDefault();

		// Set up row action and key
		if (is_numeric($this->RowIndex) && $this->CurrentMode <> "view") {
			$objForm->Index = $this->RowIndex;
			$ActionName = str_replace("k_", "k" . $this->RowIndex . "_", $this->FormActionName);
			$OldKeyName = str_replace("k_", "k" . $this->RowIndex . "_", $this->FormOldKeyName);
			$KeyName = str_replace("k_", "k" . $this->RowIndex . "_", $this->FormKeyName);
			$BlankRowName = str_replace("k_", "k" . $this->RowIndex . "_", $this->FormBlankRowName);
			if ($this->RowAction <> "")
				$this->MultiSelectKey .= "<input type=\"hidden\" name=\"" . $ActionName . "\" id=\"" . $ActionName . "\" value=\"" . $this->RowAction . "\">";
			if ($this->RowAction == "delete") {
				$rowkey = $objForm->GetValue($this->FormKeyName);
				$this->SetupKeyValues($rowkey);
			}
			if ($this->RowAction == "insert" && $this->CurrentAction == "F" && $this->EmptyRow())
				$this->MultiSelectKey .= "<input type=\"hidden\" name=\"" . $BlankRowName . "\" id=\"" . $BlankRowName . "\" value=\"1\">";
		}

		// "delete"
		if ($this->AllowAddDeleteRow) {
			if ($this->CurrentAction == "gridadd" || $this->CurrentAction == "gridedit") {
				$option = &$this->ListOptions;
				$option->UseButtonGroup = TRUE; // Use button group for grid delete button
				$option->UseImageAndText = TRUE; // Use image and text for grid delete button
				$oListOpt = &$option->Items["griddelete"];
				if (!$Security->CanDelete() && is_numeric($this->RowIndex) && ($this->RowAction == "" || $this->RowAction == "edit")) { // Do not allow delete existing record
					$oListOpt->Body = "&nbsp;";
				} else {
					$oListOpt->Body = "<a class=\"ewGridLink ewGridDelete\" title=\"" . ew_HtmlTitle($Language->Phrase("DeleteLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("DeleteLink")) . "\" onclick=\"return ew_DeleteGridRow(this, " . $this->RowIndex . ");\">" . $Language->Phrase("DeleteLink") . "</a>";
				}
			}
		}

		// "view"
		$oListOpt = &$this->ListOptions->Items["view"];
		if ($Security->CanView())
			$oListOpt->Body = "<a class=\"ewRowLink ewView\" title=\"" . ew_HtmlTitle($Language->Phrase("ViewLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("ViewLink")) . "\" href=\"" . ew_HtmlEncode($this->ViewUrl) . "\">" . $Language->Phrase("ViewLink") . "</a>";
		else
			$oListOpt->Body = "";

		// "edit"
		$oListOpt = &$this->ListOptions->Items["edit"];
		if ($Security->CanEdit()) {
			$oListOpt->Body = "<a class=\"ewRowLink ewEdit\" title=\"" . ew_HtmlTitle($Language->Phrase("EditLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("EditLink")) . "\" href=\"" . ew_HtmlEncode($this->EditUrl) . "\">" . $Language->Phrase("EditLink") . "</a>";
		} else {
			$oListOpt->Body = "";
		}

		// "copy"
		$oListOpt = &$this->ListOptions->Items["copy"];
		if ($Security->CanAdd()) {
			$oListOpt->Body = "<a class=\"ewRowLink ewCopy\" title=\"" . ew_HtmlTitle($Language->Phrase("CopyLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("CopyLink")) . "\" href=\"" . ew_HtmlEncode($this->CopyUrl) . "\">" . $Language->Phrase("CopyLink") . "</a>";
		} else {
			$oListOpt->Body = "";
		}

		// "delete"
		$oListOpt = &$this->ListOptions->Items["delete"];
		if ($Security->CanDelete())
			$oListOpt->Body = "<a class=\"ewRowLink ewDelete\"" . "" . " title=\"" . ew_HtmlTitle($Language->Phrase("DeleteLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("DeleteLink")) . "\" href=\"" . ew_HtmlEncode($this->DeleteUrl) . "\">" . $Language->Phrase("DeleteLink") . "</a>";
		else
			$oListOpt->Body = "";

		// Set up list action buttons
		$oListOpt = &$this->ListOptions->GetItem("listactions");
		if ($oListOpt) {
			$body = "";
			$links = array();
			foreach ($this->ListActions->Items as $listaction) {
				if ($listaction->Select == EW_ACTION_SINGLE && $listaction->Allow) {
					$action = $listaction->Action;
					$caption = $listaction->Caption;
					$icon = ($listaction->Icon <> "") ? "<span class=\"" . ew_HtmlEncode(str_replace(" ewIcon", "", $listaction->Icon)) . "\" data-caption=\"" . ew_HtmlTitle($caption) . "\"></span> " : "";
					$links[] = "<li><a class=\"ewAction ewListAction\" data-action=\"" . ew_HtmlEncode($action) . "\" data-caption=\"" . ew_HtmlTitle($caption) . "\" href=\"\" onclick=\"ew_SubmitAction(event,jQuery.extend({key:" . $this->KeyToJson() . "}," . $listaction->ToJson(TRUE) . "));return false;\">" . $icon . $listaction->Caption . "</a></li>";
					if (count($links) == 1) // Single button
						$body = "<a class=\"ewAction ewListAction\" data-action=\"" . ew_HtmlEncode($action) . "\" title=\"" . ew_HtmlTitle($caption) . "\" data-caption=\"" . ew_HtmlTitle($caption) . "\" href=\"\" onclick=\"ew_SubmitAction(event,jQuery.extend({key:" . $this->KeyToJson() . "}," . $listaction->ToJson(TRUE) . "));return false;\">" . $Language->Phrase("ListActionButton") . "</a>";
				}
			}
			if (count($links) > 1) { // More than one buttons, use dropdown
				$body = "<button class=\"dropdown-toggle btn btn-default btn-sm ewActions\" title=\"" . ew_HtmlTitle($Language->Phrase("ListActionButton")) . "\" data-toggle=\"dropdown\">" . $Language->Phrase("ListActionButton") . "<b class=\"caret\"></b></button>";
				$content = "";
				foreach ($links as $link)
					$content .= "<li>" . $link . "</li>";
				$body .= "<ul class=\"dropdown-menu" . ($oListOpt->OnLeft ? "" : " dropdown-menu-right") . "\">". $content . "</ul>";
				$body = "<div class=\"btn-group\">" . $body . "</div>";
			}
			if (count($links) > 0) {
				$oListOpt->Body = $body;
				$oListOpt->Visible = TRUE;
			}
		}

		// "checkbox"
		$oListOpt = &$this->ListOptions->Items["checkbox"];
		$oListOpt->Body = "<input type=\"checkbox\" name=\"key_m[]\" value=\"" . ew_HtmlEncode($this->CcoCodi->CurrentValue) . "\" onclick='ew_ClickMultiCheckbox(event);'>";
		if ($this->CurrentAction == "gridedit" && is_numeric($this->RowIndex)) {
			$this->MultiSelectKey .= "<input type=\"hidden\" name=\"" . $KeyName . "\" id=\"" . $KeyName . "\" value=\"" . $this->CcoCodi->CurrentValue . "\">";
		}
		$this->RenderListOptionsExt();

		// Call ListOptions_Rendered event
		$this->ListOptions_Rendered();
	}

	// Set up other options
	function SetupOtherOptions() {
		global $Language, $Security;
		$options = &$this->OtherOptions;
		$option = $options["addedit"];

		// Add
		$item = &$option->Add("add");
		$item->Body = "<a class=\"ewAddEdit ewAdd\" title=\"" . ew_HtmlTitle($Language->Phrase("AddLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("AddLink")) . "\" href=\"" . ew_HtmlEncode($this->AddUrl) . "\">" . $Language->Phrase("AddLink") . "</a>";
		$item->Visible = ($this->AddUrl <> "" && $Security->CanAdd());
		$item = &$option->Add("gridadd");
		$item->Body = "<a class=\"ewAddEdit ewGridAdd\" title=\"" . ew_HtmlTitle($Language->Phrase("GridAddLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("GridAddLink")) . "\" href=\"" . ew_HtmlEncode($this->GridAddUrl) . "\">" . $Language->Phrase("GridAddLink") . "</a>";
		$item->Visible = ($this->GridAddUrl <> "" && $Security->CanAdd());

		// Add grid edit
		$option = $options["addedit"];
		$item = &$option->Add("gridedit");
		$item->Body = "<a class=\"ewAddEdit ewGridEdit\" title=\"" . ew_HtmlTitle($Language->Phrase("GridEditLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("GridEditLink")) . "\" href=\"" . ew_HtmlEncode($this->GridEditUrl) . "\">" . $Language->Phrase("GridEditLink") . "</a>";
		$item->Visible = ($this->GridEditUrl <> "" && $Security->CanEdit());
		$option = $options["action"];

		// Set up options default
		foreach ($options as &$option) {
			$option->UseImageAndText = TRUE;
			$option->UseDropDownButton = FALSE;
			$option->UseButtonGroup = TRUE;
			$option->ButtonClass = "btn-sm"; // Class for button group
			$item = &$option->Add($option->GroupOptionName);
			$item->Body = "";
			$item->Visible = FALSE;
		}
		$options["addedit"]->DropDownButtonPhrase = $Language->Phrase("ButtonAddEdit");
		$options["detail"]->DropDownButtonPhrase = $Language->Phrase("ButtonDetails");
		$options["action"]->DropDownButtonPhrase = $Language->Phrase("ButtonActions");

		// Filter button
		$item = &$this->FilterOptions->Add("savecurrentfilter");
		$item->Body = "<a class=\"ewSaveFilter\" data-form=\"fCConlistsrch\" href=\"#\">" . $Language->Phrase("SaveCurrentFilter") . "</a>";
		$item->Visible = TRUE;
		$item = &$this->FilterOptions->Add("deletefilter");
		$item->Body = "<a class=\"ewDeleteFilter\" data-form=\"fCConlistsrch\" href=\"#\">" . $Language->Phrase("DeleteFilter") . "</a>";
		$item->Visible = TRUE;
		$this->FilterOptions->UseDropDownButton = TRUE;
		$this->FilterOptions->UseButtonGroup = !$this->FilterOptions->UseDropDownButton;
		$this->FilterOptions->DropDownButtonPhrase = $Language->Phrase("Filters");

		// Add group option item
		$item = &$this->FilterOptions->Add($this->FilterOptions->GroupOptionName);
		$item->Body = "";
		$item->Visible = FALSE;
	}

	// Render other options
	function RenderOtherOptions() {
		global $Language, $Security;
		$options = &$this->OtherOptions;
		if ($this->CurrentAction <> "gridadd" && $this->CurrentAction <> "gridedit") { // Not grid add/edit mode
			$option = &$options["action"];

			// Set up list action buttons
			foreach ($this->ListActions->Items as $listaction) {
				if ($listaction->Select == EW_ACTION_MULTIPLE) {
					$item = &$option->Add("custom_" . $listaction->Action);
					$caption = $listaction->Caption;
					$icon = ($listaction->Icon <> "") ? "<span class=\"" . ew_HtmlEncode($listaction->Icon) . "\" data-caption=\"" . ew_HtmlEncode($caption) . "\"></span> " : $caption;
					$item->Body = "<a class=\"ewAction ewListAction\" title=\"" . ew_HtmlEncode($caption) . "\" data-caption=\"" . ew_HtmlEncode($caption) . "\" href=\"\" onclick=\"ew_SubmitAction(event,jQuery.extend({f:document.fCConlist}," . $listaction->ToJson(TRUE) . "));return false;\">" . $icon . "</a>";
					$item->Visible = $listaction->Allow;
				}
			}

			// Hide grid edit and other options
			if ($this->TotalRecs <= 0) {
				$option = &$options["addedit"];
				$item = &$option->GetItem("gridedit");
				if ($item) $item->Visible = FALSE;
				$option = &$options["action"];
				$option->HideAllOptions();
			}
		} else { // Grid add/edit mode

			// Hide all options first
			foreach ($options as &$option)
				$option->HideAllOptions();
			if ($this->CurrentAction == "gridadd") {
				if ($this->AllowAddDeleteRow) {

					// Add add blank row
					$option = &$options["addedit"];
					$option->UseDropDownButton = FALSE;
					$option->UseImageAndText = TRUE;
					$item = &$option->Add("addblankrow");
					$item->Body = "<a class=\"ewAddEdit ewAddBlankRow\" title=\"" . ew_HtmlTitle($Language->Phrase("AddBlankRow")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("AddBlankRow")) . "\" href=\"javascript:void(0);\" onclick=\"ew_AddGridRow(this);\">" . $Language->Phrase("AddBlankRow") . "</a>";
					$item->Visible = $Security->CanAdd();
				}
				$option = &$options["action"];
				$option->UseDropDownButton = FALSE;
				$option->UseImageAndText = TRUE;

				// Add grid insert
				$item = &$option->Add("gridinsert");
				$item->Body = "<a class=\"ewAction ewGridInsert\" title=\"" . ew_HtmlTitle($Language->Phrase("GridInsertLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("GridInsertLink")) . "\" href=\"\" onclick=\"return ewForms(this).Submit('" . $this->AddMasterUrl($this->PageName()) . "');\">" . $Language->Phrase("GridInsertLink") . "</a>";

				// Add grid cancel
				$item = &$option->Add("gridcancel");
				$cancelurl = $this->AddMasterUrl($this->PageUrl() . "a=cancel");
				$item->Body = "<a class=\"ewAction ewGridCancel\" title=\"" . ew_HtmlTitle($Language->Phrase("GridCancelLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("GridCancelLink")) . "\" href=\"" . $cancelurl . "\">" . $Language->Phrase("GridCancelLink") . "</a>";
			}
			if ($this->CurrentAction == "gridedit") {
				if ($this->AllowAddDeleteRow) {

					// Add add blank row
					$option = &$options["addedit"];
					$option->UseDropDownButton = FALSE;
					$option->UseImageAndText = TRUE;
					$item = &$option->Add("addblankrow");
					$item->Body = "<a class=\"ewAddEdit ewAddBlankRow\" title=\"" . ew_HtmlTitle($Language->Phrase("AddBlankRow")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("AddBlankRow")) . "\" href=\"javascript:void(0);\" onclick=\"ew_AddGridRow(this);\">" . $Language->Phrase("AddBlankRow") . "</a>";
					$item->Visible = $Security->CanAdd();
				}
				$option = &$options["action"];
				$option->UseDropDownButton = FALSE;
				$option->UseImageAndText = TRUE;
					$item = &$option->Add("gridsave");
					$item->Body = "<a class=\"ewAction ewGridSave\" title=\"" . ew_HtmlTitle($Language->Phrase("GridSaveLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("GridSaveLink")) . "\" href=\"\" onclick=\"return ewForms(this).Submit('" . $this->AddMasterUrl($this->PageName()) . "');\">" . $Language->Phrase("GridSaveLink") . "</a>";
					$item = &$option->Add("gridcancel");
					$cancelurl = $this->AddMasterUrl($this->PageUrl() . "a=cancel");
					$item->Body = "<a class=\"ewAction ewGridCancel\" title=\"" . ew_HtmlTitle($Language->Phrase("GridCancelLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("GridCancelLink")) . "\" href=\"" . $cancelurl . "\">" . $Language->Phrase("GridCancelLink") . "</a>";
			}
		}
	}

	// Process list action
	function ProcessListAction() {
		global $Language, $Security;
		$userlist = "";
		$user = "";
		$sFilter = $this->GetKeyFilter();
		$UserAction = @$_POST["useraction"];
		if ($sFilter <> "" && $UserAction <> "") {

			// Check permission first
			$ActionCaption = $UserAction;
			if (array_key_exists($UserAction, $this->ListActions->Items)) {
				$ActionCaption = $this->ListActions->Items[$UserAction]->Caption;
				if (!$this->ListActions->Items[$UserAction]->Allow) {
					$errmsg = str_replace('%s', $ActionCaption, $Language->Phrase("CustomActionNotAllowed"));
					if (@$_POST["ajax"] == $UserAction) // Ajax
						echo "<p class=\"text-danger\">" . $errmsg . "</p>";
					else
						$this->setFailureMessage($errmsg);
					return FALSE;
				}
			}
			$this->CurrentFilter = $sFilter;
			$sSql = $this->SQL();
			$conn = &$this->Connection();
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			$rs = $conn->Execute($sSql);
			$conn->raiseErrorFn = '';
			$this->CurrentAction = $UserAction;

			// Call row action event
			if ($rs && !$rs->EOF) {
				$conn->BeginTrans();
				$this->SelectedCount = $rs->RecordCount();
				$this->SelectedIndex = 0;
				while (!$rs->EOF) {
					$this->SelectedIndex++;
					$row = $rs->fields;
					$Processed = $this->Row_CustomAction($UserAction, $row);
					if (!$Processed) break;
					$rs->MoveNext();
				}
				if ($Processed) {
					$conn->CommitTrans(); // Commit the changes
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage(str_replace('%s', $ActionCaption, $Language->Phrase("CustomActionCompleted"))); // Set up success message
				} else {
					$conn->RollbackTrans(); // Rollback changes

					// Set up error message
					if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

						// Use the message, do nothing
					} elseif ($this->CancelMessage <> "") {
						$this->setFailureMessage($this->CancelMessage);
						$this->CancelMessage = "";
					} else {
						$this->setFailureMessage(str_replace('%s', $ActionCaption, $Language->Phrase("CustomActionFailed")));
					}
				}
			}
			if ($rs)
				$rs->Close();
			if (@$_POST["ajax"] == $UserAction) { // Ajax
				if ($this->getSuccessMessage() <> "") {
					echo "<p class=\"text-success\">" . $this->getSuccessMessage() . "</p>";
					$this->ClearSuccessMessage(); // Clear message
				}
				if ($this->getFailureMessage() <> "") {
					echo "<p class=\"text-danger\">" . $this->getFailureMessage() . "</p>";
					$this->ClearFailureMessage(); // Clear message
				}
				return TRUE;
			}
		}
		return FALSE; // Not ajax request
	}

	// Set up search options
	function SetupSearchOptions() {
		global $Language;
		$this->SearchOptions = new cListOptions();
		$this->SearchOptions->Tag = "div";
		$this->SearchOptions->TagClassName = "ewSearchOption";

		// Search button
		$item = &$this->SearchOptions->Add("searchtoggle");
		$SearchToggleClass = ($this->SearchWhere <> "") ? " active" : " active";
		$item->Body = "<button type=\"button\" class=\"btn btn-default ewSearchToggle" . $SearchToggleClass . "\" title=\"" . $Language->Phrase("SearchPanel") . "\" data-caption=\"" . $Language->Phrase("SearchPanel") . "\" data-toggle=\"button\" data-form=\"fCConlistsrch\">" . $Language->Phrase("SearchBtn") . "</button>";
		$item->Visible = TRUE;

		// Show all button
		$item = &$this->SearchOptions->Add("showall");
		$item->Body = "<a class=\"btn btn-default ewShowAll\" title=\"" . $Language->Phrase("ShowAll") . "\" data-caption=\"" . $Language->Phrase("ShowAll") . "\" href=\"" . $this->PageUrl() . "cmd=reset\">" . $Language->Phrase("ShowAllBtn") . "</a>";
		$item->Visible = ($this->SearchWhere <> $this->DefaultSearchWhere && $this->SearchWhere <> "0=101");

		// Button group for search
		$this->SearchOptions->UseDropDownButton = FALSE;
		$this->SearchOptions->UseImageAndText = TRUE;
		$this->SearchOptions->UseButtonGroup = TRUE;
		$this->SearchOptions->DropDownButtonPhrase = $Language->Phrase("ButtonSearch");

		// Add group option item
		$item = &$this->SearchOptions->Add($this->SearchOptions->GroupOptionName);
		$item->Body = "";
		$item->Visible = FALSE;

		// Hide search options
		if ($this->Export <> "" || $this->CurrentAction <> "")
			$this->SearchOptions->HideAllOptions();
		global $Security;
		if (!$Security->CanSearch()) {
			$this->SearchOptions->HideAllOptions();
			$this->FilterOptions->HideAllOptions();
		}
	}

	function SetupListOptionsExt() {
		global $Security, $Language;
	}

	function RenderListOptionsExt() {
		global $Security, $Language;
	}

	// Set up starting record parameters
	function SetUpStartRec() {
		if ($this->DisplayRecs == 0)
			return;
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET[EW_TABLE_START_REC] <> "") { // Check for "start" parameter
				$this->StartRec = $_GET[EW_TABLE_START_REC];
				$this->setStartRecordNumber($this->StartRec);
			} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
				$PageNo = $_GET[EW_TABLE_PAGE_NO];
				if (is_numeric($PageNo)) {
					$this->StartRec = ($PageNo-1)*$this->DisplayRecs+1;
					if ($this->StartRec <= 0) {
						$this->StartRec = 1;
					} elseif ($this->StartRec >= intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1) {
						$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1;
					}
					$this->setStartRecordNumber($this->StartRec);
				}
			}
		}
		$this->StartRec = $this->getStartRecordNumber();

		// Check if correct start record counter
		if (!is_numeric($this->StartRec) || $this->StartRec == "") { // Avoid invalid start record counter
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} elseif (intval($this->StartRec) > intval($this->TotalRecs)) { // Avoid starting record > total records
			$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to last page first record
			$this->setStartRecordNumber($this->StartRec);
		} elseif (($this->StartRec-1) % $this->DisplayRecs <> 0) {
			$this->StartRec = intval(($this->StartRec-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to page boundary
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Load default values
	function LoadDefaultValues() {
		$this->CcoCodi->CurrentValue = NULL;
		$this->CcoCodi->OldValue = $this->CcoCodi->CurrentValue;
		$this->VcoCodi->CurrentValue = NULL;
		$this->VcoCodi->OldValue = $this->VcoCodi->CurrentValue;
		$this->CcoFPag->CurrentValue = NULL;
		$this->CcoFPag->OldValue = $this->CcoFPag->CurrentValue;
		$this->CcoMont->CurrentValue = NULL;
		$this->CcoMont->OldValue = $this->CcoMont->CurrentValue;
		$this->CcoEsta->CurrentValue = NULL;
		$this->CcoEsta->OldValue = $this->CcoEsta->CurrentValue;
		$this->CcoAnho->CurrentValue = NULL;
		$this->CcoAnho->OldValue = $this->CcoAnho->CurrentValue;
		$this->CcoMes->CurrentValue = NULL;
		$this->CcoMes->OldValue = $this->CcoMes->CurrentValue;
		$this->CcoConc->CurrentValue = NULL;
		$this->CcoConc->OldValue = $this->CcoConc->CurrentValue;
		$this->CcoUsua->CurrentValue = NULL;
		$this->CcoUsua->OldValue = $this->CcoUsua->CurrentValue;
		$this->CcoFCre->CurrentValue = NULL;
		$this->CcoFCre->OldValue = $this->CcoFCre->CurrentValue;
	}

	// Load basic search values
	function LoadBasicSearchValues() {
		$this->BasicSearch->Keyword = @$_GET[EW_TABLE_BASIC_SEARCH];
		if ($this->BasicSearch->Keyword <> "") $this->Command = "search";
		$this->BasicSearch->Type = @$_GET[EW_TABLE_BASIC_SEARCH_TYPE];
	}

	// Load form values
	function LoadFormValues() {

		// Load from form
		global $objForm;
		if (!$this->CcoCodi->FldIsDetailKey && $this->CurrentAction <> "gridadd" && $this->CurrentAction <> "add")
			$this->CcoCodi->setFormValue($objForm->GetValue("x_CcoCodi"));
		if (!$this->VcoCodi->FldIsDetailKey) {
			$this->VcoCodi->setFormValue($objForm->GetValue("x_VcoCodi"));
		}
		$this->VcoCodi->setOldValue($objForm->GetValue("o_VcoCodi"));
		if (!$this->CcoFPag->FldIsDetailKey) {
			$this->CcoFPag->setFormValue($objForm->GetValue("x_CcoFPag"));
			$this->CcoFPag->CurrentValue = ew_UnFormatDateTime($this->CcoFPag->CurrentValue, 7);
		}
		$this->CcoFPag->setOldValue($objForm->GetValue("o_CcoFPag"));
		if (!$this->CcoMont->FldIsDetailKey) {
			$this->CcoMont->setFormValue($objForm->GetValue("x_CcoMont"));
		}
		$this->CcoMont->setOldValue($objForm->GetValue("o_CcoMont"));
		if (!$this->CcoEsta->FldIsDetailKey) {
			$this->CcoEsta->setFormValue($objForm->GetValue("x_CcoEsta"));
		}
		$this->CcoEsta->setOldValue($objForm->GetValue("o_CcoEsta"));
		if (!$this->CcoAnho->FldIsDetailKey) {
			$this->CcoAnho->setFormValue($objForm->GetValue("x_CcoAnho"));
		}
		$this->CcoAnho->setOldValue($objForm->GetValue("o_CcoAnho"));
		if (!$this->CcoMes->FldIsDetailKey) {
			$this->CcoMes->setFormValue($objForm->GetValue("x_CcoMes"));
		}
		$this->CcoMes->setOldValue($objForm->GetValue("o_CcoMes"));
		if (!$this->CcoConc->FldIsDetailKey) {
			$this->CcoConc->setFormValue($objForm->GetValue("x_CcoConc"));
		}
		$this->CcoConc->setOldValue($objForm->GetValue("o_CcoConc"));
		if (!$this->CcoUsua->FldIsDetailKey) {
			$this->CcoUsua->setFormValue($objForm->GetValue("x_CcoUsua"));
		}
		$this->CcoUsua->setOldValue($objForm->GetValue("o_CcoUsua"));
		if (!$this->CcoFCre->FldIsDetailKey) {
			$this->CcoFCre->setFormValue($objForm->GetValue("x_CcoFCre"));
			$this->CcoFCre->CurrentValue = ew_UnFormatDateTime($this->CcoFCre->CurrentValue, 7);
		}
		$this->CcoFCre->setOldValue($objForm->GetValue("o_CcoFCre"));
	}

	// Restore form values
	function RestoreFormValues() {
		global $objForm;
		if ($this->CurrentAction <> "gridadd" && $this->CurrentAction <> "add")
			$this->CcoCodi->CurrentValue = $this->CcoCodi->FormValue;
		$this->VcoCodi->CurrentValue = $this->VcoCodi->FormValue;
		$this->CcoFPag->CurrentValue = $this->CcoFPag->FormValue;
		$this->CcoFPag->CurrentValue = ew_UnFormatDateTime($this->CcoFPag->CurrentValue, 7);
		$this->CcoMont->CurrentValue = $this->CcoMont->FormValue;
		$this->CcoEsta->CurrentValue = $this->CcoEsta->FormValue;
		$this->CcoAnho->CurrentValue = $this->CcoAnho->FormValue;
		$this->CcoMes->CurrentValue = $this->CcoMes->FormValue;
		$this->CcoConc->CurrentValue = $this->CcoConc->FormValue;
		$this->CcoUsua->CurrentValue = $this->CcoUsua->FormValue;
		$this->CcoFCre->CurrentValue = $this->CcoFCre->FormValue;
		$this->CcoFCre->CurrentValue = ew_UnFormatDateTime($this->CcoFCre->CurrentValue, 7);
	}

	// Load recordset
	function LoadRecordset($offset = -1, $rowcnt = -1) {

		// Load List page SQL
		$sSql = $this->SelectSQL();
		$conn = &$this->Connection();

		// Load recordset
		$dbtype = ew_GetConnectionType($this->DBID);
		if ($this->UseSelectLimit) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			if ($dbtype == "MSSQL") {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset, array("_hasOrderBy" => trim($this->getOrderBy()) || trim($this->getSessionOrderBy())));
			} else {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset);
			}
			$conn->raiseErrorFn = '';
		} else {
			$rs = ew_LoadRecordset($sSql, $conn);
		}

		// Call Recordset Selected event
		$this->Recordset_Selected($rs);
		return $rs;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->CcoCodi->setDbValue($rs->fields('CcoCodi'));
		$this->VcoCodi->setDbValue($rs->fields('VcoCodi'));
		$this->CcoFPag->setDbValue($rs->fields('CcoFPag'));
		$this->CcoMont->setDbValue($rs->fields('CcoMont'));
		$this->CcoEsta->setDbValue($rs->fields('CcoEsta'));
		$this->CcoAnho->setDbValue($rs->fields('CcoAnho'));
		$this->CcoMes->setDbValue($rs->fields('CcoMes'));
		$this->CcoConc->setDbValue($rs->fields('CcoConc'));
		$this->CcoUsua->setDbValue($rs->fields('CcoUsua'));
		$this->CcoFCre->setDbValue($rs->fields('CcoFCre'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->CcoCodi->DbValue = $row['CcoCodi'];
		$this->VcoCodi->DbValue = $row['VcoCodi'];
		$this->CcoFPag->DbValue = $row['CcoFPag'];
		$this->CcoMont->DbValue = $row['CcoMont'];
		$this->CcoEsta->DbValue = $row['CcoEsta'];
		$this->CcoAnho->DbValue = $row['CcoAnho'];
		$this->CcoMes->DbValue = $row['CcoMes'];
		$this->CcoConc->DbValue = $row['CcoConc'];
		$this->CcoUsua->DbValue = $row['CcoUsua'];
		$this->CcoFCre->DbValue = $row['CcoFCre'];
	}

	// Load old record
	function LoadOldRecord() {

		// Load key values from Session
		$bValidKey = TRUE;
		if (strval($this->getKey("CcoCodi")) <> "")
			$this->CcoCodi->CurrentValue = $this->getKey("CcoCodi"); // CcoCodi
		else
			$bValidKey = FALSE;

		// Load old recordset
		if ($bValidKey) {
			$this->CurrentFilter = $this->KeyFilter();
			$sSql = $this->SQL();
			$conn = &$this->Connection();
			$this->OldRecordset = ew_LoadRecordset($sSql, $conn);
			$this->LoadRowValues($this->OldRecordset); // Load row values
		} else {
			$this->OldRecordset = NULL;
		}
		return $bValidKey;
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		$this->ViewUrl = $this->GetViewUrl();
		$this->EditUrl = $this->GetEditUrl();
		$this->InlineEditUrl = $this->GetInlineEditUrl();
		$this->CopyUrl = $this->GetCopyUrl();
		$this->InlineCopyUrl = $this->GetInlineCopyUrl();
		$this->DeleteUrl = $this->GetDeleteUrl();

		// Convert decimal values if posted back
		if ($this->CcoMont->FormValue == $this->CcoMont->CurrentValue && is_numeric(ew_StrToFloat($this->CcoMont->CurrentValue)))
			$this->CcoMont->CurrentValue = ew_StrToFloat($this->CcoMont->CurrentValue);

		// Call Row_Rendering event
		$this->Row_Rendering();

		// Common render codes for all row types
		// CcoCodi
		// VcoCodi
		// CcoFPag
		// CcoMont
		// CcoEsta
		// CcoAnho
		// CcoMes
		// CcoConc
		// CcoUsua
		// CcoFCre

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// CcoCodi
		$this->CcoCodi->ViewValue = $this->CcoCodi->CurrentValue;
		$this->CcoCodi->ViewCustomAttributes = "";

		// VcoCodi
		if (strval($this->VcoCodi->CurrentValue) <> "") {
			$sFilterWrk = "\"VcoCodi\"" . ew_SearchString("=", $this->VcoCodi->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT \"VcoCodi\", \"VcoCodi\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"VCon\"";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->VcoCodi, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->VcoCodi->ViewValue = $this->VcoCodi->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->VcoCodi->ViewValue = $this->VcoCodi->CurrentValue;
			}
		} else {
			$this->VcoCodi->ViewValue = NULL;
		}
		$this->VcoCodi->ViewCustomAttributes = "";

		// CcoFPag
		$this->CcoFPag->ViewValue = $this->CcoFPag->CurrentValue;
		$this->CcoFPag->ViewValue = ew_FormatDateTime($this->CcoFPag->ViewValue, 7);
		$this->CcoFPag->ViewCustomAttributes = "";

		// CcoMont
		$this->CcoMont->ViewValue = $this->CcoMont->CurrentValue;
		$this->CcoMont->ViewCustomAttributes = "";

		// CcoEsta
		$this->CcoEsta->ViewValue = $this->CcoEsta->CurrentValue;
		$this->CcoEsta->ViewCustomAttributes = "";

		// CcoAnho
		$this->CcoAnho->ViewValue = $this->CcoAnho->CurrentValue;
		$this->CcoAnho->ViewCustomAttributes = "";

		// CcoMes
		$this->CcoMes->ViewValue = $this->CcoMes->CurrentValue;
		$this->CcoMes->ViewCustomAttributes = "";

		// CcoConc
		$this->CcoConc->ViewValue = $this->CcoConc->CurrentValue;
		$this->CcoConc->ViewCustomAttributes = "";

		// CcoUsua
		$this->CcoUsua->ViewValue = $this->CcoUsua->CurrentValue;
		$this->CcoUsua->ViewCustomAttributes = "";

		// CcoFCre
		$this->CcoFCre->ViewValue = $this->CcoFCre->CurrentValue;
		$this->CcoFCre->ViewValue = ew_FormatDateTime($this->CcoFCre->ViewValue, 7);
		$this->CcoFCre->ViewCustomAttributes = "";

			// CcoCodi
			$this->CcoCodi->LinkCustomAttributes = "";
			$this->CcoCodi->HrefValue = "";
			$this->CcoCodi->TooltipValue = "";

			// VcoCodi
			$this->VcoCodi->LinkCustomAttributes = "";
			$this->VcoCodi->HrefValue = "";
			$this->VcoCodi->TooltipValue = "";

			// CcoFPag
			$this->CcoFPag->LinkCustomAttributes = "";
			$this->CcoFPag->HrefValue = "";
			$this->CcoFPag->TooltipValue = "";

			// CcoMont
			$this->CcoMont->LinkCustomAttributes = "";
			$this->CcoMont->HrefValue = "";
			$this->CcoMont->TooltipValue = "";

			// CcoEsta
			$this->CcoEsta->LinkCustomAttributes = "";
			$this->CcoEsta->HrefValue = "";
			$this->CcoEsta->TooltipValue = "";

			// CcoAnho
			$this->CcoAnho->LinkCustomAttributes = "";
			$this->CcoAnho->HrefValue = "";
			$this->CcoAnho->TooltipValue = "";

			// CcoMes
			$this->CcoMes->LinkCustomAttributes = "";
			$this->CcoMes->HrefValue = "";
			$this->CcoMes->TooltipValue = "";

			// CcoConc
			$this->CcoConc->LinkCustomAttributes = "";
			$this->CcoConc->HrefValue = "";
			$this->CcoConc->TooltipValue = "";

			// CcoUsua
			$this->CcoUsua->LinkCustomAttributes = "";
			$this->CcoUsua->HrefValue = "";
			$this->CcoUsua->TooltipValue = "";

			// CcoFCre
			$this->CcoFCre->LinkCustomAttributes = "";
			$this->CcoFCre->HrefValue = "";
			$this->CcoFCre->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_ADD) { // Add row

			// CcoCodi
			// VcoCodi

			$this->VcoCodi->EditAttrs["class"] = "form-control";
			$this->VcoCodi->EditCustomAttributes = "";
			if (trim(strval($this->VcoCodi->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "\"VcoCodi\"" . ew_SearchString("=", $this->VcoCodi->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT \"VcoCodi\", \"VcoCodi\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\", '' AS \"SelectFilterFld\", '' AS \"SelectFilterFld2\", '' AS \"SelectFilterFld3\", '' AS \"SelectFilterFld4\" FROM \"public\".\"VCon\"";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->VcoCodi, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->VcoCodi->EditValue = $arwrk;

			// CcoFPag
			$this->CcoFPag->EditAttrs["class"] = "form-control";
			$this->CcoFPag->EditCustomAttributes = "";
			$this->CcoFPag->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->CcoFPag->CurrentValue, 7));
			$this->CcoFPag->PlaceHolder = ew_RemoveHtml($this->CcoFPag->FldCaption());

			// CcoMont
			$this->CcoMont->EditAttrs["class"] = "form-control";
			$this->CcoMont->EditCustomAttributes = "";
			$this->CcoMont->EditValue = ew_HtmlEncode($this->CcoMont->CurrentValue);
			$this->CcoMont->PlaceHolder = ew_RemoveHtml($this->CcoMont->FldCaption());
			if (strval($this->CcoMont->EditValue) <> "" && is_numeric($this->CcoMont->EditValue)) {
			$this->CcoMont->EditValue = ew_FormatNumber($this->CcoMont->EditValue, -2, -1, -2, 0);
			$this->CcoMont->OldValue = $this->CcoMont->EditValue;
			}

			// CcoEsta
			$this->CcoEsta->EditAttrs["class"] = "form-control";
			$this->CcoEsta->EditCustomAttributes = "";
			$this->CcoEsta->EditValue = ew_HtmlEncode($this->CcoEsta->CurrentValue);
			$this->CcoEsta->PlaceHolder = ew_RemoveHtml($this->CcoEsta->FldCaption());

			// CcoAnho
			$this->CcoAnho->EditAttrs["class"] = "form-control";
			$this->CcoAnho->EditCustomAttributes = "";
			$this->CcoAnho->EditValue = ew_HtmlEncode($this->CcoAnho->CurrentValue);
			$this->CcoAnho->PlaceHolder = ew_RemoveHtml($this->CcoAnho->FldCaption());

			// CcoMes
			$this->CcoMes->EditAttrs["class"] = "form-control";
			$this->CcoMes->EditCustomAttributes = "";
			$this->CcoMes->EditValue = ew_HtmlEncode($this->CcoMes->CurrentValue);
			$this->CcoMes->PlaceHolder = ew_RemoveHtml($this->CcoMes->FldCaption());

			// CcoConc
			$this->CcoConc->EditAttrs["class"] = "form-control";
			$this->CcoConc->EditCustomAttributes = "";
			$this->CcoConc->EditValue = ew_HtmlEncode($this->CcoConc->CurrentValue);
			$this->CcoConc->PlaceHolder = ew_RemoveHtml($this->CcoConc->FldCaption());

			// CcoUsua
			$this->CcoUsua->EditAttrs["class"] = "form-control";
			$this->CcoUsua->EditCustomAttributes = "";
			$this->CcoUsua->EditValue = ew_HtmlEncode($this->CcoUsua->CurrentValue);
			$this->CcoUsua->PlaceHolder = ew_RemoveHtml($this->CcoUsua->FldCaption());

			// CcoFCre
			$this->CcoFCre->EditAttrs["class"] = "form-control";
			$this->CcoFCre->EditCustomAttributes = "";
			$this->CcoFCre->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->CcoFCre->CurrentValue, 7));
			$this->CcoFCre->PlaceHolder = ew_RemoveHtml($this->CcoFCre->FldCaption());

			// Edit refer script
			// CcoCodi

			$this->CcoCodi->HrefValue = "";

			// VcoCodi
			$this->VcoCodi->HrefValue = "";

			// CcoFPag
			$this->CcoFPag->HrefValue = "";

			// CcoMont
			$this->CcoMont->HrefValue = "";

			// CcoEsta
			$this->CcoEsta->HrefValue = "";

			// CcoAnho
			$this->CcoAnho->HrefValue = "";

			// CcoMes
			$this->CcoMes->HrefValue = "";

			// CcoConc
			$this->CcoConc->HrefValue = "";

			// CcoUsua
			$this->CcoUsua->HrefValue = "";

			// CcoFCre
			$this->CcoFCre->HrefValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_EDIT) { // Edit row

			// CcoCodi
			$this->CcoCodi->EditAttrs["class"] = "form-control";
			$this->CcoCodi->EditCustomAttributes = "";
			$this->CcoCodi->EditValue = $this->CcoCodi->CurrentValue;
			$this->CcoCodi->ViewCustomAttributes = "";

			// VcoCodi
			$this->VcoCodi->EditAttrs["class"] = "form-control";
			$this->VcoCodi->EditCustomAttributes = "";
			if (trim(strval($this->VcoCodi->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "\"VcoCodi\"" . ew_SearchString("=", $this->VcoCodi->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT \"VcoCodi\", \"VcoCodi\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\", '' AS \"SelectFilterFld\", '' AS \"SelectFilterFld2\", '' AS \"SelectFilterFld3\", '' AS \"SelectFilterFld4\" FROM \"public\".\"VCon\"";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->VcoCodi, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->VcoCodi->EditValue = $arwrk;

			// CcoFPag
			$this->CcoFPag->EditAttrs["class"] = "form-control";
			$this->CcoFPag->EditCustomAttributes = "";
			$this->CcoFPag->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->CcoFPag->CurrentValue, 7));
			$this->CcoFPag->PlaceHolder = ew_RemoveHtml($this->CcoFPag->FldCaption());

			// CcoMont
			$this->CcoMont->EditAttrs["class"] = "form-control";
			$this->CcoMont->EditCustomAttributes = "";
			$this->CcoMont->EditValue = ew_HtmlEncode($this->CcoMont->CurrentValue);
			$this->CcoMont->PlaceHolder = ew_RemoveHtml($this->CcoMont->FldCaption());
			if (strval($this->CcoMont->EditValue) <> "" && is_numeric($this->CcoMont->EditValue)) {
			$this->CcoMont->EditValue = ew_FormatNumber($this->CcoMont->EditValue, -2, -1, -2, 0);
			$this->CcoMont->OldValue = $this->CcoMont->EditValue;
			}

			// CcoEsta
			$this->CcoEsta->EditAttrs["class"] = "form-control";
			$this->CcoEsta->EditCustomAttributes = "";
			$this->CcoEsta->EditValue = ew_HtmlEncode($this->CcoEsta->CurrentValue);
			$this->CcoEsta->PlaceHolder = ew_RemoveHtml($this->CcoEsta->FldCaption());

			// CcoAnho
			$this->CcoAnho->EditAttrs["class"] = "form-control";
			$this->CcoAnho->EditCustomAttributes = "";
			$this->CcoAnho->EditValue = ew_HtmlEncode($this->CcoAnho->CurrentValue);
			$this->CcoAnho->PlaceHolder = ew_RemoveHtml($this->CcoAnho->FldCaption());

			// CcoMes
			$this->CcoMes->EditAttrs["class"] = "form-control";
			$this->CcoMes->EditCustomAttributes = "";
			$this->CcoMes->EditValue = ew_HtmlEncode($this->CcoMes->CurrentValue);
			$this->CcoMes->PlaceHolder = ew_RemoveHtml($this->CcoMes->FldCaption());

			// CcoConc
			$this->CcoConc->EditAttrs["class"] = "form-control";
			$this->CcoConc->EditCustomAttributes = "";
			$this->CcoConc->EditValue = ew_HtmlEncode($this->CcoConc->CurrentValue);
			$this->CcoConc->PlaceHolder = ew_RemoveHtml($this->CcoConc->FldCaption());

			// CcoUsua
			$this->CcoUsua->EditAttrs["class"] = "form-control";
			$this->CcoUsua->EditCustomAttributes = "";
			$this->CcoUsua->EditValue = ew_HtmlEncode($this->CcoUsua->CurrentValue);
			$this->CcoUsua->PlaceHolder = ew_RemoveHtml($this->CcoUsua->FldCaption());

			// CcoFCre
			$this->CcoFCre->EditAttrs["class"] = "form-control";
			$this->CcoFCre->EditCustomAttributes = "";
			$this->CcoFCre->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->CcoFCre->CurrentValue, 7));
			$this->CcoFCre->PlaceHolder = ew_RemoveHtml($this->CcoFCre->FldCaption());

			// Edit refer script
			// CcoCodi

			$this->CcoCodi->HrefValue = "";

			// VcoCodi
			$this->VcoCodi->HrefValue = "";

			// CcoFPag
			$this->CcoFPag->HrefValue = "";

			// CcoMont
			$this->CcoMont->HrefValue = "";

			// CcoEsta
			$this->CcoEsta->HrefValue = "";

			// CcoAnho
			$this->CcoAnho->HrefValue = "";

			// CcoMes
			$this->CcoMes->HrefValue = "";

			// CcoConc
			$this->CcoConc->HrefValue = "";

			// CcoUsua
			$this->CcoUsua->HrefValue = "";

			// CcoFCre
			$this->CcoFCre->HrefValue = "";
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate form
	function ValidateForm() {
		global $Language, $gsFormError;

		// Initialize form error message
		$gsFormError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return ($gsFormError == "");
		if (!$this->VcoCodi->FldIsDetailKey && !is_null($this->VcoCodi->FormValue) && $this->VcoCodi->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->VcoCodi->FldCaption(), $this->VcoCodi->ReqErrMsg));
		}
		if (!$this->CcoFPag->FldIsDetailKey && !is_null($this->CcoFPag->FormValue) && $this->CcoFPag->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->CcoFPag->FldCaption(), $this->CcoFPag->ReqErrMsg));
		}
		if (!ew_CheckEuroDate($this->CcoFPag->FormValue)) {
			ew_AddMessage($gsFormError, $this->CcoFPag->FldErrMsg());
		}
		if (!$this->CcoMont->FldIsDetailKey && !is_null($this->CcoMont->FormValue) && $this->CcoMont->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->CcoMont->FldCaption(), $this->CcoMont->ReqErrMsg));
		}
		if (!ew_CheckNumber($this->CcoMont->FormValue)) {
			ew_AddMessage($gsFormError, $this->CcoMont->FldErrMsg());
		}
		if (!$this->CcoEsta->FldIsDetailKey && !is_null($this->CcoEsta->FormValue) && $this->CcoEsta->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->CcoEsta->FldCaption(), $this->CcoEsta->ReqErrMsg));
		}
		if (!$this->CcoAnho->FldIsDetailKey && !is_null($this->CcoAnho->FormValue) && $this->CcoAnho->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->CcoAnho->FldCaption(), $this->CcoAnho->ReqErrMsg));
		}
		if (!$this->CcoMes->FldIsDetailKey && !is_null($this->CcoMes->FormValue) && $this->CcoMes->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->CcoMes->FldCaption(), $this->CcoMes->ReqErrMsg));
		}
		if (!$this->CcoConc->FldIsDetailKey && !is_null($this->CcoConc->FormValue) && $this->CcoConc->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->CcoConc->FldCaption(), $this->CcoConc->ReqErrMsg));
		}
		if (!$this->CcoUsua->FldIsDetailKey && !is_null($this->CcoUsua->FormValue) && $this->CcoUsua->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->CcoUsua->FldCaption(), $this->CcoUsua->ReqErrMsg));
		}
		if (!$this->CcoFCre->FldIsDetailKey && !is_null($this->CcoFCre->FormValue) && $this->CcoFCre->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->CcoFCre->FldCaption(), $this->CcoFCre->ReqErrMsg));
		}
		if (!ew_CheckEuroDate($this->CcoFCre->FormValue)) {
			ew_AddMessage($gsFormError, $this->CcoFCre->FldErrMsg());
		}

		// Return validate result
		$ValidateForm = ($gsFormError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateForm = $ValidateForm && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsFormError, $sFormCustomError);
		}
		return $ValidateForm;
	}

	//
	// Delete records based on current filter
	//
	function DeleteRows() {
		global $Language, $Security;
		if (!$Security->CanDelete()) {
			$this->setFailureMessage($Language->Phrase("NoDeletePermission")); // No delete permission
			return FALSE;
		}
		$DeleteRows = TRUE;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE) {
			return FALSE;
		} elseif ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
			$rs->Close();
			return FALSE;

		//} else {
		//	$this->LoadRowValues($rs); // Load row values

		}
		$rows = ($rs) ? $rs->GetRows() : array();

		// Clone old rows
		$rsold = $rows;
		if ($rs)
			$rs->Close();

		// Call row deleting event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$DeleteRows = $this->Row_Deleting($row);
				if (!$DeleteRows) break;
			}
		}
		if ($DeleteRows) {
			$sKey = "";
			foreach ($rsold as $row) {
				$sThisKey = "";
				if ($sThisKey <> "") $sThisKey .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
				$sThisKey .= $row['CcoCodi'];
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				$DeleteRows = $this->Delete($row); // Delete
				$conn->raiseErrorFn = '';
				if ($DeleteRows === FALSE)
					break;
				if ($sKey <> "") $sKey .= ", ";
				$sKey .= $sThisKey;
			}
		} else {

			// Set up error message
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("DeleteCancelled"));
			}
		}
		if ($DeleteRows) {
		} else {
		}

		// Call Row Deleted event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$this->Row_Deleted($row);
			}
		}
		return $DeleteRows;
	}

	// Update record based on key values
	function EditRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$conn = &$this->Connection();
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE)
			return FALSE;
		if ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record message
			$EditRow = FALSE; // Update Failed
		} else {

			// Save old values
			$rsold = &$rs->fields;
			$this->LoadDbValues($rsold);
			$rsnew = array();

			// VcoCodi
			$this->VcoCodi->SetDbValueDef($rsnew, $this->VcoCodi->CurrentValue, 0, $this->VcoCodi->ReadOnly);

			// CcoFPag
			$this->CcoFPag->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->CcoFPag->CurrentValue, 7), ew_CurrentDate(), $this->CcoFPag->ReadOnly);

			// CcoMont
			$this->CcoMont->SetDbValueDef($rsnew, $this->CcoMont->CurrentValue, 0, $this->CcoMont->ReadOnly);

			// CcoEsta
			$this->CcoEsta->SetDbValueDef($rsnew, $this->CcoEsta->CurrentValue, "", $this->CcoEsta->ReadOnly);

			// CcoAnho
			$this->CcoAnho->SetDbValueDef($rsnew, $this->CcoAnho->CurrentValue, "", $this->CcoAnho->ReadOnly);

			// CcoMes
			$this->CcoMes->SetDbValueDef($rsnew, $this->CcoMes->CurrentValue, "", $this->CcoMes->ReadOnly);

			// CcoConc
			$this->CcoConc->SetDbValueDef($rsnew, $this->CcoConc->CurrentValue, "", $this->CcoConc->ReadOnly);

			// CcoUsua
			$this->CcoUsua->SetDbValueDef($rsnew, $this->CcoUsua->CurrentValue, "", $this->CcoUsua->ReadOnly);

			// CcoFCre
			$this->CcoFCre->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->CcoFCre->CurrentValue, 7), ew_CurrentDate(), $this->CcoFCre->ReadOnly);

			// Call Row Updating event
			$bUpdateRow = $this->Row_Updating($rsold, $rsnew);
			if ($bUpdateRow) {
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				if (count($rsnew) > 0)
					$EditRow = $this->Update($rsnew, "", $rsold);
				else
					$EditRow = TRUE; // No field to update
				$conn->raiseErrorFn = '';
				if ($EditRow) {
				}
			} else {
				if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

					// Use the message, do nothing
				} elseif ($this->CancelMessage <> "") {
					$this->setFailureMessage($this->CancelMessage);
					$this->CancelMessage = "";
				} else {
					$this->setFailureMessage($Language->Phrase("UpdateCancelled"));
				}
				$EditRow = FALSE;
			}
		}

		// Call Row_Updated event
		if ($EditRow)
			$this->Row_Updated($rsold, $rsnew);
		$rs->Close();
		return $EditRow;
	}

	// Add record
	function AddRow($rsold = NULL) {
		global $Language, $Security;
		$conn = &$this->Connection();

		// Load db values from rsold
		if ($rsold) {
			$this->LoadDbValues($rsold);
		}
		$rsnew = array();

		// VcoCodi
		$this->VcoCodi->SetDbValueDef($rsnew, $this->VcoCodi->CurrentValue, 0, FALSE);

		// CcoFPag
		$this->CcoFPag->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->CcoFPag->CurrentValue, 7), ew_CurrentDate(), FALSE);

		// CcoMont
		$this->CcoMont->SetDbValueDef($rsnew, $this->CcoMont->CurrentValue, 0, FALSE);

		// CcoEsta
		$this->CcoEsta->SetDbValueDef($rsnew, $this->CcoEsta->CurrentValue, "", FALSE);

		// CcoAnho
		$this->CcoAnho->SetDbValueDef($rsnew, $this->CcoAnho->CurrentValue, "", FALSE);

		// CcoMes
		$this->CcoMes->SetDbValueDef($rsnew, $this->CcoMes->CurrentValue, "", FALSE);

		// CcoConc
		$this->CcoConc->SetDbValueDef($rsnew, $this->CcoConc->CurrentValue, "", FALSE);

		// CcoUsua
		$this->CcoUsua->SetDbValueDef($rsnew, $this->CcoUsua->CurrentValue, "", FALSE);

		// CcoFCre
		$this->CcoFCre->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->CcoFCre->CurrentValue, 7), ew_CurrentDate(), FALSE);

		// Call Row Inserting event
		$rs = ($rsold == NULL) ? NULL : $rsold->fields;
		$bInsertRow = $this->Row_Inserting($rs, $rsnew);
		if ($bInsertRow) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			$AddRow = $this->Insert($rsnew);
			$conn->raiseErrorFn = '';
			if ($AddRow) {

				// Get insert id if necessary
				$this->CcoCodi->setDbValue($conn->GetOne("SELECT currval('\"CCon_CcrCodi_seq\"'::regclass)"));
				$rsnew['CcoCodi'] = $this->CcoCodi->DbValue;
			}
		} else {
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("InsertCancelled"));
			}
			$AddRow = FALSE;
		}
		if ($AddRow) {

			// Call Row Inserted event
			$rs = ($rsold == NULL) ? NULL : $rsold->fields;
			$this->Row_Inserted($rs, $rsnew);
		}
		return $AddRow;
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$url = preg_replace('/\?cmd=reset(all){0,1}$/i', '', $url); // Remove cmd=reset / cmd=resetall
		$Breadcrumb->Add("list", $this->TableVar, $url, "", $this->TableVar, TRUE);
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}

	// ListOptions Load event
	function ListOptions_Load() {

		// Example:
		//$opt = &$this->ListOptions->Add("new");
		//$opt->Header = "xxx";
		//$opt->OnLeft = TRUE; // Link on left
		//$opt->MoveTo(0); // Move to first column

	}

	// ListOptions Rendered event
	function ListOptions_Rendered() {

		// Example: 
		//$this->ListOptions->Items["new"]->Body = "xxx";

	}

	// Row Custom Action event
	function Row_CustomAction($action, $row) {

		// Return FALSE to abort
		return TRUE;
	}

	// Page Exporting event
	// $this->ExportDoc = export document object
	function Page_Exporting() {

		//$this->ExportDoc->Text = "my header"; // Export header
		//return FALSE; // Return FALSE to skip default export and use Row_Export event

		return TRUE; // Return TRUE to use default export and skip Row_Export event
	}

	// Row Export event
	// $this->ExportDoc = export document object
	function Row_Export($rs) {

	    //$this->ExportDoc->Text .= "my content"; // Build HTML with field value: $rs["MyField"] or $this->MyField->ViewValue
	}

	// Page Exported event
	// $this->ExportDoc = export document object
	function Page_Exported() {

		//$this->ExportDoc->Text .= "my footer"; // Export footer
		//echo $this->ExportDoc->Text;

	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($CCon_list)) $CCon_list = new cCCon_list();

// Page init
$CCon_list->Page_Init();

// Page main
$CCon_list->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$CCon_list->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "list";
var CurrentForm = fCConlist = new ew_Form("fCConlist", "list");
fCConlist.FormKeyCountName = '<?php echo $CCon_list->FormKeyCountName ?>';

// Validate form
fCConlist.Validate = function() {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.GetForm(), $fobj = $(fobj);
	if ($fobj.find("#a_confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.FormKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = $fobj.find("#a_list").val() == "gridinsert";
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
		var checkrow = (gridinsert) ? !this.EmptyRow(infix) : true;
		if (checkrow) {
			addcnt++;
			elm = this.GetElements("x" + infix + "_VcoCodi");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $CCon->VcoCodi->FldCaption(), $CCon->VcoCodi->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_CcoFPag");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $CCon->CcoFPag->FldCaption(), $CCon->CcoFPag->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_CcoFPag");
			if (elm && !ew_CheckEuroDate(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($CCon->CcoFPag->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_CcoMont");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $CCon->CcoMont->FldCaption(), $CCon->CcoMont->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_CcoMont");
			if (elm && !ew_CheckNumber(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($CCon->CcoMont->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_CcoEsta");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $CCon->CcoEsta->FldCaption(), $CCon->CcoEsta->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_CcoAnho");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $CCon->CcoAnho->FldCaption(), $CCon->CcoAnho->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_CcoMes");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $CCon->CcoMes->FldCaption(), $CCon->CcoMes->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_CcoConc");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $CCon->CcoConc->FldCaption(), $CCon->CcoConc->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_CcoUsua");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $CCon->CcoUsua->FldCaption(), $CCon->CcoUsua->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_CcoFCre");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $CCon->CcoFCre->FldCaption(), $CCon->CcoFCre->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_CcoFCre");
			if (elm && !ew_CheckEuroDate(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($CCon->CcoFCre->FldErrMsg()) ?>");

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
		} // End Grid Add checking
	}
	if (gridinsert && addcnt == 0) { // No row added
		ew_Alert(ewLanguage.Phrase("NoAddRecord"));
		return false;
	}
	return true;
}

// Check empty row
fCConlist.EmptyRow = function(infix) {
	var fobj = this.Form;
	if (ew_ValueChanged(fobj, infix, "VcoCodi", false)) return false;
	if (ew_ValueChanged(fobj, infix, "CcoFPag", false)) return false;
	if (ew_ValueChanged(fobj, infix, "CcoMont", false)) return false;
	if (ew_ValueChanged(fobj, infix, "CcoEsta", false)) return false;
	if (ew_ValueChanged(fobj, infix, "CcoAnho", false)) return false;
	if (ew_ValueChanged(fobj, infix, "CcoMes", false)) return false;
	if (ew_ValueChanged(fobj, infix, "CcoConc", false)) return false;
	if (ew_ValueChanged(fobj, infix, "CcoUsua", false)) return false;
	if (ew_ValueChanged(fobj, infix, "CcoFCre", false)) return false;
	return true;
}

// Form_CustomValidate event
fCConlist.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fCConlist.ValidateRequired = true;
<?php } else { ?>
fCConlist.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
fCConlist.Lists["x_VcoCodi"] = {"LinkField":"x_VcoCodi","Ajax":true,"AutoFill":false,"DisplayFields":["x_VcoCodi","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};

// Form object for search
var CurrentSearchForm = fCConlistsrch = new ew_Form("fCConlistsrch");
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php if ($CCon_list->TotalRecs > 0 && $CCon_list->ExportOptions->Visible()) { ?>
<?php $CCon_list->ExportOptions->Render("body") ?>
<?php } ?>
<?php if ($CCon_list->SearchOptions->Visible()) { ?>
<?php $CCon_list->SearchOptions->Render("body") ?>
<?php } ?>
<?php if ($CCon_list->FilterOptions->Visible()) { ?>
<?php $CCon_list->FilterOptions->Render("body") ?>
<?php } ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php
if ($CCon->CurrentAction == "gridadd") {
	$CCon->CurrentFilter = "0=1";
	$CCon_list->StartRec = 1;
	$CCon_list->DisplayRecs = $CCon->GridAddRowCount;
	$CCon_list->TotalRecs = $CCon_list->DisplayRecs;
	$CCon_list->StopRec = $CCon_list->DisplayRecs;
} else {
	$bSelectLimit = $CCon_list->UseSelectLimit;
	if ($bSelectLimit) {
		if ($CCon_list->TotalRecs <= 0)
			$CCon_list->TotalRecs = $CCon->SelectRecordCount();
	} else {
		if (!$CCon_list->Recordset && ($CCon_list->Recordset = $CCon_list->LoadRecordset()))
			$CCon_list->TotalRecs = $CCon_list->Recordset->RecordCount();
	}
	$CCon_list->StartRec = 1;
	if ($CCon_list->DisplayRecs <= 0 || ($CCon->Export <> "" && $CCon->ExportAll)) // Display all records
		$CCon_list->DisplayRecs = $CCon_list->TotalRecs;
	if (!($CCon->Export <> "" && $CCon->ExportAll))
		$CCon_list->SetUpStartRec(); // Set up start record position
	if ($bSelectLimit)
		$CCon_list->Recordset = $CCon_list->LoadRecordset($CCon_list->StartRec-1, $CCon_list->DisplayRecs);

	// Set no record found message
	if ($CCon->CurrentAction == "" && $CCon_list->TotalRecs == 0) {
		if (!$Security->CanList())
			$CCon_list->setWarningMessage($Language->Phrase("NoPermission"));
		if ($CCon_list->SearchWhere == "0=101")
			$CCon_list->setWarningMessage($Language->Phrase("EnterSearchCriteria"));
		else
			$CCon_list->setWarningMessage($Language->Phrase("NoRecord"));
	}
}
$CCon_list->RenderOtherOptions();
?>
<?php if ($Security->CanSearch()) { ?>
<?php if ($CCon->Export == "" && $CCon->CurrentAction == "") { ?>
<form name="fCConlistsrch" id="fCConlistsrch" class="form-inline ewForm" action="<?php echo ew_CurrentPage() ?>">
<?php $SearchPanelClass = ($CCon_list->SearchWhere <> "") ? " in" : " in"; ?>
<div id="fCConlistsrch_SearchPanel" class="ewSearchPanel collapse<?php echo $SearchPanelClass ?>">
<input type="hidden" name="cmd" value="search">
<input type="hidden" name="t" value="CCon">
	<div class="ewBasicSearch">
<div id="xsr_1" class="ewRow">
	<div class="ewQuickSearch input-group">
	<input type="text" name="<?php echo EW_TABLE_BASIC_SEARCH ?>" id="<?php echo EW_TABLE_BASIC_SEARCH ?>" class="form-control" value="<?php echo ew_HtmlEncode($CCon_list->BasicSearch->getKeyword()) ?>" placeholder="<?php echo ew_HtmlEncode($Language->Phrase("Search")) ?>">
	<input type="hidden" name="<?php echo EW_TABLE_BASIC_SEARCH_TYPE ?>" id="<?php echo EW_TABLE_BASIC_SEARCH_TYPE ?>" value="<?php echo ew_HtmlEncode($CCon_list->BasicSearch->getType()) ?>">
	<div class="input-group-btn">
		<button type="button" data-toggle="dropdown" class="btn btn-default"><span id="searchtype"><?php echo $CCon_list->BasicSearch->getTypeNameShort() ?></span><span class="caret"></span></button>
		<ul class="dropdown-menu pull-right" role="menu">
			<li<?php if ($CCon_list->BasicSearch->getType() == "") echo " class=\"active\""; ?>><a href="javascript:void(0);" onclick="ew_SetSearchType(this)"><?php echo $Language->Phrase("QuickSearchAuto") ?></a></li>
			<li<?php if ($CCon_list->BasicSearch->getType() == "=") echo " class=\"active\""; ?>><a href="javascript:void(0);" onclick="ew_SetSearchType(this,'=')"><?php echo $Language->Phrase("QuickSearchExact") ?></a></li>
			<li<?php if ($CCon_list->BasicSearch->getType() == "AND") echo " class=\"active\""; ?>><a href="javascript:void(0);" onclick="ew_SetSearchType(this,'AND')"><?php echo $Language->Phrase("QuickSearchAll") ?></a></li>
			<li<?php if ($CCon_list->BasicSearch->getType() == "OR") echo " class=\"active\""; ?>><a href="javascript:void(0);" onclick="ew_SetSearchType(this,'OR')"><?php echo $Language->Phrase("QuickSearchAny") ?></a></li>
		</ul>
	<button class="btn btn-primary ewButton" name="btnsubmit" id="btnsubmit" type="submit"><?php echo $Language->Phrase("QuickSearchBtn") ?></button>
	</div>
	</div>
</div>
	</div>
</div>
</form>
<?php } ?>
<?php } ?>
<?php $CCon_list->ShowPageHeader(); ?>
<?php
$CCon_list->ShowMessage();
?>
<?php if ($CCon_list->TotalRecs > 0 || $CCon->CurrentAction <> "") { ?>
<div class="panel panel-default ewGrid">
<form name="fCConlist" id="fCConlist" class="form-inline ewForm ewListForm" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($CCon_list->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $CCon_list->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="CCon">
<div id="gmp_CCon" class="<?php if (ew_IsResponsiveLayout()) { echo "table-responsive "; } ?>ewGridMiddlePanel">
<?php if ($CCon_list->TotalRecs > 0) { ?>
<table id="tbl_CConlist" class="table ewTable">
<?php echo $CCon->TableCustomInnerHtml ?>
<thead><!-- Table header -->
	<tr class="ewTableHeader">
<?php

// Header row
$CCon_list->RowType = EW_ROWTYPE_HEADER;

// Render list options
$CCon_list->RenderListOptions();

// Render list options (header, left)
$CCon_list->ListOptions->Render("header", "left");
?>
<?php if ($CCon->CcoCodi->Visible) { // CcoCodi ?>
	<?php if ($CCon->SortUrl($CCon->CcoCodi) == "") { ?>
		<th data-name="CcoCodi"><div id="elh_CCon_CcoCodi" class="CCon_CcoCodi"><div class="ewTableHeaderCaption"><?php echo $CCon->CcoCodi->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="CcoCodi"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $CCon->SortUrl($CCon->CcoCodi) ?>',2);"><div id="elh_CCon_CcoCodi" class="CCon_CcoCodi">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $CCon->CcoCodi->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($CCon->CcoCodi->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($CCon->CcoCodi->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($CCon->VcoCodi->Visible) { // VcoCodi ?>
	<?php if ($CCon->SortUrl($CCon->VcoCodi) == "") { ?>
		<th data-name="VcoCodi"><div id="elh_CCon_VcoCodi" class="CCon_VcoCodi"><div class="ewTableHeaderCaption"><?php echo $CCon->VcoCodi->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="VcoCodi"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $CCon->SortUrl($CCon->VcoCodi) ?>',2);"><div id="elh_CCon_VcoCodi" class="CCon_VcoCodi">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $CCon->VcoCodi->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($CCon->VcoCodi->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($CCon->VcoCodi->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($CCon->CcoFPag->Visible) { // CcoFPag ?>
	<?php if ($CCon->SortUrl($CCon->CcoFPag) == "") { ?>
		<th data-name="CcoFPag"><div id="elh_CCon_CcoFPag" class="CCon_CcoFPag"><div class="ewTableHeaderCaption"><?php echo $CCon->CcoFPag->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="CcoFPag"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $CCon->SortUrl($CCon->CcoFPag) ?>',2);"><div id="elh_CCon_CcoFPag" class="CCon_CcoFPag">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $CCon->CcoFPag->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($CCon->CcoFPag->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($CCon->CcoFPag->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($CCon->CcoMont->Visible) { // CcoMont ?>
	<?php if ($CCon->SortUrl($CCon->CcoMont) == "") { ?>
		<th data-name="CcoMont"><div id="elh_CCon_CcoMont" class="CCon_CcoMont"><div class="ewTableHeaderCaption"><?php echo $CCon->CcoMont->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="CcoMont"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $CCon->SortUrl($CCon->CcoMont) ?>',2);"><div id="elh_CCon_CcoMont" class="CCon_CcoMont">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $CCon->CcoMont->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($CCon->CcoMont->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($CCon->CcoMont->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($CCon->CcoEsta->Visible) { // CcoEsta ?>
	<?php if ($CCon->SortUrl($CCon->CcoEsta) == "") { ?>
		<th data-name="CcoEsta"><div id="elh_CCon_CcoEsta" class="CCon_CcoEsta"><div class="ewTableHeaderCaption"><?php echo $CCon->CcoEsta->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="CcoEsta"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $CCon->SortUrl($CCon->CcoEsta) ?>',2);"><div id="elh_CCon_CcoEsta" class="CCon_CcoEsta">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $CCon->CcoEsta->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($CCon->CcoEsta->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($CCon->CcoEsta->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($CCon->CcoAnho->Visible) { // CcoAnho ?>
	<?php if ($CCon->SortUrl($CCon->CcoAnho) == "") { ?>
		<th data-name="CcoAnho"><div id="elh_CCon_CcoAnho" class="CCon_CcoAnho"><div class="ewTableHeaderCaption"><?php echo $CCon->CcoAnho->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="CcoAnho"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $CCon->SortUrl($CCon->CcoAnho) ?>',2);"><div id="elh_CCon_CcoAnho" class="CCon_CcoAnho">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $CCon->CcoAnho->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($CCon->CcoAnho->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($CCon->CcoAnho->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($CCon->CcoMes->Visible) { // CcoMes ?>
	<?php if ($CCon->SortUrl($CCon->CcoMes) == "") { ?>
		<th data-name="CcoMes"><div id="elh_CCon_CcoMes" class="CCon_CcoMes"><div class="ewTableHeaderCaption"><?php echo $CCon->CcoMes->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="CcoMes"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $CCon->SortUrl($CCon->CcoMes) ?>',2);"><div id="elh_CCon_CcoMes" class="CCon_CcoMes">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $CCon->CcoMes->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($CCon->CcoMes->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($CCon->CcoMes->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($CCon->CcoConc->Visible) { // CcoConc ?>
	<?php if ($CCon->SortUrl($CCon->CcoConc) == "") { ?>
		<th data-name="CcoConc"><div id="elh_CCon_CcoConc" class="CCon_CcoConc"><div class="ewTableHeaderCaption"><?php echo $CCon->CcoConc->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="CcoConc"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $CCon->SortUrl($CCon->CcoConc) ?>',2);"><div id="elh_CCon_CcoConc" class="CCon_CcoConc">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $CCon->CcoConc->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($CCon->CcoConc->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($CCon->CcoConc->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($CCon->CcoUsua->Visible) { // CcoUsua ?>
	<?php if ($CCon->SortUrl($CCon->CcoUsua) == "") { ?>
		<th data-name="CcoUsua"><div id="elh_CCon_CcoUsua" class="CCon_CcoUsua"><div class="ewTableHeaderCaption"><?php echo $CCon->CcoUsua->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="CcoUsua"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $CCon->SortUrl($CCon->CcoUsua) ?>',2);"><div id="elh_CCon_CcoUsua" class="CCon_CcoUsua">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $CCon->CcoUsua->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($CCon->CcoUsua->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($CCon->CcoUsua->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($CCon->CcoFCre->Visible) { // CcoFCre ?>
	<?php if ($CCon->SortUrl($CCon->CcoFCre) == "") { ?>
		<th data-name="CcoFCre"><div id="elh_CCon_CcoFCre" class="CCon_CcoFCre"><div class="ewTableHeaderCaption"><?php echo $CCon->CcoFCre->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="CcoFCre"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $CCon->SortUrl($CCon->CcoFCre) ?>',2);"><div id="elh_CCon_CcoFCre" class="CCon_CcoFCre">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $CCon->CcoFCre->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($CCon->CcoFCre->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($CCon->CcoFCre->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php

// Render list options (header, right)
$CCon_list->ListOptions->Render("header", "right");
?>
	</tr>
</thead>
<tbody>
<?php
if ($CCon->ExportAll && $CCon->Export <> "") {
	$CCon_list->StopRec = $CCon_list->TotalRecs;
} else {

	// Set the last record to display
	if ($CCon_list->TotalRecs > $CCon_list->StartRec + $CCon_list->DisplayRecs - 1)
		$CCon_list->StopRec = $CCon_list->StartRec + $CCon_list->DisplayRecs - 1;
	else
		$CCon_list->StopRec = $CCon_list->TotalRecs;
}

// Restore number of post back records
if ($objForm) {
	$objForm->Index = -1;
	if ($objForm->HasValue($CCon_list->FormKeyCountName) && ($CCon->CurrentAction == "gridadd" || $CCon->CurrentAction == "gridedit" || $CCon->CurrentAction == "F")) {
		$CCon_list->KeyCount = $objForm->GetValue($CCon_list->FormKeyCountName);
		$CCon_list->StopRec = $CCon_list->StartRec + $CCon_list->KeyCount - 1;
	}
}
$CCon_list->RecCnt = $CCon_list->StartRec - 1;
if ($CCon_list->Recordset && !$CCon_list->Recordset->EOF) {
	$CCon_list->Recordset->MoveFirst();
	$bSelectLimit = $CCon_list->UseSelectLimit;
	if (!$bSelectLimit && $CCon_list->StartRec > 1)
		$CCon_list->Recordset->Move($CCon_list->StartRec - 1);
} elseif (!$CCon->AllowAddDeleteRow && $CCon_list->StopRec == 0) {
	$CCon_list->StopRec = $CCon->GridAddRowCount;
}

// Initialize aggregate
$CCon->RowType = EW_ROWTYPE_AGGREGATEINIT;
$CCon->ResetAttrs();
$CCon_list->RenderRow();
if ($CCon->CurrentAction == "gridadd")
	$CCon_list->RowIndex = 0;
if ($CCon->CurrentAction == "gridedit")
	$CCon_list->RowIndex = 0;
while ($CCon_list->RecCnt < $CCon_list->StopRec) {
	$CCon_list->RecCnt++;
	if (intval($CCon_list->RecCnt) >= intval($CCon_list->StartRec)) {
		$CCon_list->RowCnt++;
		if ($CCon->CurrentAction == "gridadd" || $CCon->CurrentAction == "gridedit" || $CCon->CurrentAction == "F") {
			$CCon_list->RowIndex++;
			$objForm->Index = $CCon_list->RowIndex;
			if ($objForm->HasValue($CCon_list->FormActionName))
				$CCon_list->RowAction = strval($objForm->GetValue($CCon_list->FormActionName));
			elseif ($CCon->CurrentAction == "gridadd")
				$CCon_list->RowAction = "insert";
			else
				$CCon_list->RowAction = "";
		}

		// Set up key count
		$CCon_list->KeyCount = $CCon_list->RowIndex;

		// Init row class and style
		$CCon->ResetAttrs();
		$CCon->CssClass = "";
		if ($CCon->CurrentAction == "gridadd") {
			$CCon_list->LoadDefaultValues(); // Load default values
		} else {
			$CCon_list->LoadRowValues($CCon_list->Recordset); // Load row values
		}
		$CCon->RowType = EW_ROWTYPE_VIEW; // Render view
		if ($CCon->CurrentAction == "gridadd") // Grid add
			$CCon->RowType = EW_ROWTYPE_ADD; // Render add
		if ($CCon->CurrentAction == "gridadd" && $CCon->EventCancelled && !$objForm->HasValue("k_blankrow")) // Insert failed
			$CCon_list->RestoreCurrentRowFormValues($CCon_list->RowIndex); // Restore form values
		if ($CCon->CurrentAction == "gridedit") { // Grid edit
			if ($CCon->EventCancelled) {
				$CCon_list->RestoreCurrentRowFormValues($CCon_list->RowIndex); // Restore form values
			}
			if ($CCon_list->RowAction == "insert")
				$CCon->RowType = EW_ROWTYPE_ADD; // Render add
			else
				$CCon->RowType = EW_ROWTYPE_EDIT; // Render edit
		}
		if ($CCon->CurrentAction == "gridedit" && ($CCon->RowType == EW_ROWTYPE_EDIT || $CCon->RowType == EW_ROWTYPE_ADD) && $CCon->EventCancelled) // Update failed
			$CCon_list->RestoreCurrentRowFormValues($CCon_list->RowIndex); // Restore form values
		if ($CCon->RowType == EW_ROWTYPE_EDIT) // Edit row
			$CCon_list->EditRowCnt++;

		// Set up row id / data-rowindex
		$CCon->RowAttrs = array_merge($CCon->RowAttrs, array('data-rowindex'=>$CCon_list->RowCnt, 'id'=>'r' . $CCon_list->RowCnt . '_CCon', 'data-rowtype'=>$CCon->RowType));

		// Render row
		$CCon_list->RenderRow();

		// Render list options
		$CCon_list->RenderListOptions();

		// Skip delete row / empty row for confirm page
		if ($CCon_list->RowAction <> "delete" && $CCon_list->RowAction <> "insertdelete" && !($CCon_list->RowAction == "insert" && $CCon->CurrentAction == "F" && $CCon_list->EmptyRow())) {
?>
	<tr<?php echo $CCon->RowAttributes() ?>>
<?php

// Render list options (body, left)
$CCon_list->ListOptions->Render("body", "left", $CCon_list->RowCnt);
?>
	<?php if ($CCon->CcoCodi->Visible) { // CcoCodi ?>
		<td data-name="CcoCodi"<?php echo $CCon->CcoCodi->CellAttributes() ?>>
<?php if ($CCon->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<input type="hidden" data-table="CCon" data-field="x_CcoCodi" name="o<?php echo $CCon_list->RowIndex ?>_CcoCodi" id="o<?php echo $CCon_list->RowIndex ?>_CcoCodi" value="<?php echo ew_HtmlEncode($CCon->CcoCodi->OldValue) ?>">
<?php } ?>
<?php if ($CCon->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $CCon_list->RowCnt ?>_CCon_CcoCodi" class="form-group CCon_CcoCodi">
<span<?php echo $CCon->CcoCodi->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $CCon->CcoCodi->EditValue ?></p></span>
</span>
<input type="hidden" data-table="CCon" data-field="x_CcoCodi" name="x<?php echo $CCon_list->RowIndex ?>_CcoCodi" id="x<?php echo $CCon_list->RowIndex ?>_CcoCodi" value="<?php echo ew_HtmlEncode($CCon->CcoCodi->CurrentValue) ?>">
<?php } ?>
<?php if ($CCon->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $CCon_list->RowCnt ?>_CCon_CcoCodi" class="CCon_CcoCodi">
<span<?php echo $CCon->CcoCodi->ViewAttributes() ?>>
<?php echo $CCon->CcoCodi->ListViewValue() ?></span>
</span>
<?php } ?>
<a id="<?php echo $CCon_list->PageObjName . "_row_" . $CCon_list->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($CCon->VcoCodi->Visible) { // VcoCodi ?>
		<td data-name="VcoCodi"<?php echo $CCon->VcoCodi->CellAttributes() ?>>
<?php if ($CCon->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $CCon_list->RowCnt ?>_CCon_VcoCodi" class="form-group CCon_VcoCodi">
<select data-table="CCon" data-field="x_VcoCodi" data-value-separator="<?php echo ew_HtmlEncode(is_array($CCon->VcoCodi->DisplayValueSeparator) ? json_encode($CCon->VcoCodi->DisplayValueSeparator) : $CCon->VcoCodi->DisplayValueSeparator) ?>" id="x<?php echo $CCon_list->RowIndex ?>_VcoCodi" name="x<?php echo $CCon_list->RowIndex ?>_VcoCodi"<?php echo $CCon->VcoCodi->EditAttributes() ?>>
<?php
if (is_array($CCon->VcoCodi->EditValue)) {
	$arwrk = $CCon->VcoCodi->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($CCon->VcoCodi->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $CCon->VcoCodi->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($CCon->VcoCodi->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($CCon->VcoCodi->CurrentValue) ?>" selected><?php echo $CCon->VcoCodi->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $CCon->VcoCodi->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT \"VcoCodi\", \"VcoCodi\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"VCon\"";
$sWhereWrk = "";
$CCon->VcoCodi->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$CCon->VcoCodi->LookupFilters += array("f0" => "\"VcoCodi\" = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$CCon->Lookup_Selecting($CCon->VcoCodi, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $CCon->VcoCodi->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $CCon_list->RowIndex ?>_VcoCodi" id="s_x<?php echo $CCon_list->RowIndex ?>_VcoCodi" value="<?php echo $CCon->VcoCodi->LookupFilterQuery() ?>">
</span>
<input type="hidden" data-table="CCon" data-field="x_VcoCodi" name="o<?php echo $CCon_list->RowIndex ?>_VcoCodi" id="o<?php echo $CCon_list->RowIndex ?>_VcoCodi" value="<?php echo ew_HtmlEncode($CCon->VcoCodi->OldValue) ?>">
<?php } ?>
<?php if ($CCon->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $CCon_list->RowCnt ?>_CCon_VcoCodi" class="form-group CCon_VcoCodi">
<select data-table="CCon" data-field="x_VcoCodi" data-value-separator="<?php echo ew_HtmlEncode(is_array($CCon->VcoCodi->DisplayValueSeparator) ? json_encode($CCon->VcoCodi->DisplayValueSeparator) : $CCon->VcoCodi->DisplayValueSeparator) ?>" id="x<?php echo $CCon_list->RowIndex ?>_VcoCodi" name="x<?php echo $CCon_list->RowIndex ?>_VcoCodi"<?php echo $CCon->VcoCodi->EditAttributes() ?>>
<?php
if (is_array($CCon->VcoCodi->EditValue)) {
	$arwrk = $CCon->VcoCodi->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($CCon->VcoCodi->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $CCon->VcoCodi->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($CCon->VcoCodi->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($CCon->VcoCodi->CurrentValue) ?>" selected><?php echo $CCon->VcoCodi->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $CCon->VcoCodi->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT \"VcoCodi\", \"VcoCodi\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"VCon\"";
$sWhereWrk = "";
$CCon->VcoCodi->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$CCon->VcoCodi->LookupFilters += array("f0" => "\"VcoCodi\" = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$CCon->Lookup_Selecting($CCon->VcoCodi, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $CCon->VcoCodi->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $CCon_list->RowIndex ?>_VcoCodi" id="s_x<?php echo $CCon_list->RowIndex ?>_VcoCodi" value="<?php echo $CCon->VcoCodi->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php if ($CCon->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $CCon_list->RowCnt ?>_CCon_VcoCodi" class="CCon_VcoCodi">
<span<?php echo $CCon->VcoCodi->ViewAttributes() ?>>
<?php echo $CCon->VcoCodi->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($CCon->CcoFPag->Visible) { // CcoFPag ?>
		<td data-name="CcoFPag"<?php echo $CCon->CcoFPag->CellAttributes() ?>>
<?php if ($CCon->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $CCon_list->RowCnt ?>_CCon_CcoFPag" class="form-group CCon_CcoFPag">
<input type="text" data-table="CCon" data-field="x_CcoFPag" data-format="7" name="x<?php echo $CCon_list->RowIndex ?>_CcoFPag" id="x<?php echo $CCon_list->RowIndex ?>_CcoFPag" placeholder="<?php echo ew_HtmlEncode($CCon->CcoFPag->getPlaceHolder()) ?>" value="<?php echo $CCon->CcoFPag->EditValue ?>"<?php echo $CCon->CcoFPag->EditAttributes() ?>>
</span>
<input type="hidden" data-table="CCon" data-field="x_CcoFPag" name="o<?php echo $CCon_list->RowIndex ?>_CcoFPag" id="o<?php echo $CCon_list->RowIndex ?>_CcoFPag" value="<?php echo ew_HtmlEncode($CCon->CcoFPag->OldValue) ?>">
<?php } ?>
<?php if ($CCon->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $CCon_list->RowCnt ?>_CCon_CcoFPag" class="form-group CCon_CcoFPag">
<input type="text" data-table="CCon" data-field="x_CcoFPag" data-format="7" name="x<?php echo $CCon_list->RowIndex ?>_CcoFPag" id="x<?php echo $CCon_list->RowIndex ?>_CcoFPag" placeholder="<?php echo ew_HtmlEncode($CCon->CcoFPag->getPlaceHolder()) ?>" value="<?php echo $CCon->CcoFPag->EditValue ?>"<?php echo $CCon->CcoFPag->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($CCon->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $CCon_list->RowCnt ?>_CCon_CcoFPag" class="CCon_CcoFPag">
<span<?php echo $CCon->CcoFPag->ViewAttributes() ?>>
<?php echo $CCon->CcoFPag->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($CCon->CcoMont->Visible) { // CcoMont ?>
		<td data-name="CcoMont"<?php echo $CCon->CcoMont->CellAttributes() ?>>
<?php if ($CCon->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $CCon_list->RowCnt ?>_CCon_CcoMont" class="form-group CCon_CcoMont">
<input type="text" data-table="CCon" data-field="x_CcoMont" name="x<?php echo $CCon_list->RowIndex ?>_CcoMont" id="x<?php echo $CCon_list->RowIndex ?>_CcoMont" size="30" placeholder="<?php echo ew_HtmlEncode($CCon->CcoMont->getPlaceHolder()) ?>" value="<?php echo $CCon->CcoMont->EditValue ?>"<?php echo $CCon->CcoMont->EditAttributes() ?>>
</span>
<input type="hidden" data-table="CCon" data-field="x_CcoMont" name="o<?php echo $CCon_list->RowIndex ?>_CcoMont" id="o<?php echo $CCon_list->RowIndex ?>_CcoMont" value="<?php echo ew_HtmlEncode($CCon->CcoMont->OldValue) ?>">
<?php } ?>
<?php if ($CCon->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $CCon_list->RowCnt ?>_CCon_CcoMont" class="form-group CCon_CcoMont">
<input type="text" data-table="CCon" data-field="x_CcoMont" name="x<?php echo $CCon_list->RowIndex ?>_CcoMont" id="x<?php echo $CCon_list->RowIndex ?>_CcoMont" size="30" placeholder="<?php echo ew_HtmlEncode($CCon->CcoMont->getPlaceHolder()) ?>" value="<?php echo $CCon->CcoMont->EditValue ?>"<?php echo $CCon->CcoMont->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($CCon->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $CCon_list->RowCnt ?>_CCon_CcoMont" class="CCon_CcoMont">
<span<?php echo $CCon->CcoMont->ViewAttributes() ?>>
<?php echo $CCon->CcoMont->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($CCon->CcoEsta->Visible) { // CcoEsta ?>
		<td data-name="CcoEsta"<?php echo $CCon->CcoEsta->CellAttributes() ?>>
<?php if ($CCon->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $CCon_list->RowCnt ?>_CCon_CcoEsta" class="form-group CCon_CcoEsta">
<input type="text" data-table="CCon" data-field="x_CcoEsta" name="x<?php echo $CCon_list->RowIndex ?>_CcoEsta" id="x<?php echo $CCon_list->RowIndex ?>_CcoEsta" size="30" maxlength="1" placeholder="<?php echo ew_HtmlEncode($CCon->CcoEsta->getPlaceHolder()) ?>" value="<?php echo $CCon->CcoEsta->EditValue ?>"<?php echo $CCon->CcoEsta->EditAttributes() ?>>
</span>
<input type="hidden" data-table="CCon" data-field="x_CcoEsta" name="o<?php echo $CCon_list->RowIndex ?>_CcoEsta" id="o<?php echo $CCon_list->RowIndex ?>_CcoEsta" value="<?php echo ew_HtmlEncode($CCon->CcoEsta->OldValue) ?>">
<?php } ?>
<?php if ($CCon->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $CCon_list->RowCnt ?>_CCon_CcoEsta" class="form-group CCon_CcoEsta">
<input type="text" data-table="CCon" data-field="x_CcoEsta" name="x<?php echo $CCon_list->RowIndex ?>_CcoEsta" id="x<?php echo $CCon_list->RowIndex ?>_CcoEsta" size="30" maxlength="1" placeholder="<?php echo ew_HtmlEncode($CCon->CcoEsta->getPlaceHolder()) ?>" value="<?php echo $CCon->CcoEsta->EditValue ?>"<?php echo $CCon->CcoEsta->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($CCon->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $CCon_list->RowCnt ?>_CCon_CcoEsta" class="CCon_CcoEsta">
<span<?php echo $CCon->CcoEsta->ViewAttributes() ?>>
<?php echo $CCon->CcoEsta->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($CCon->CcoAnho->Visible) { // CcoAnho ?>
		<td data-name="CcoAnho"<?php echo $CCon->CcoAnho->CellAttributes() ?>>
<?php if ($CCon->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $CCon_list->RowCnt ?>_CCon_CcoAnho" class="form-group CCon_CcoAnho">
<input type="text" data-table="CCon" data-field="x_CcoAnho" name="x<?php echo $CCon_list->RowIndex ?>_CcoAnho" id="x<?php echo $CCon_list->RowIndex ?>_CcoAnho" size="30" maxlength="4" placeholder="<?php echo ew_HtmlEncode($CCon->CcoAnho->getPlaceHolder()) ?>" value="<?php echo $CCon->CcoAnho->EditValue ?>"<?php echo $CCon->CcoAnho->EditAttributes() ?>>
</span>
<input type="hidden" data-table="CCon" data-field="x_CcoAnho" name="o<?php echo $CCon_list->RowIndex ?>_CcoAnho" id="o<?php echo $CCon_list->RowIndex ?>_CcoAnho" value="<?php echo ew_HtmlEncode($CCon->CcoAnho->OldValue) ?>">
<?php } ?>
<?php if ($CCon->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $CCon_list->RowCnt ?>_CCon_CcoAnho" class="form-group CCon_CcoAnho">
<input type="text" data-table="CCon" data-field="x_CcoAnho" name="x<?php echo $CCon_list->RowIndex ?>_CcoAnho" id="x<?php echo $CCon_list->RowIndex ?>_CcoAnho" size="30" maxlength="4" placeholder="<?php echo ew_HtmlEncode($CCon->CcoAnho->getPlaceHolder()) ?>" value="<?php echo $CCon->CcoAnho->EditValue ?>"<?php echo $CCon->CcoAnho->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($CCon->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $CCon_list->RowCnt ?>_CCon_CcoAnho" class="CCon_CcoAnho">
<span<?php echo $CCon->CcoAnho->ViewAttributes() ?>>
<?php echo $CCon->CcoAnho->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($CCon->CcoMes->Visible) { // CcoMes ?>
		<td data-name="CcoMes"<?php echo $CCon->CcoMes->CellAttributes() ?>>
<?php if ($CCon->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $CCon_list->RowCnt ?>_CCon_CcoMes" class="form-group CCon_CcoMes">
<input type="text" data-table="CCon" data-field="x_CcoMes" name="x<?php echo $CCon_list->RowIndex ?>_CcoMes" id="x<?php echo $CCon_list->RowIndex ?>_CcoMes" size="30" maxlength="2" placeholder="<?php echo ew_HtmlEncode($CCon->CcoMes->getPlaceHolder()) ?>" value="<?php echo $CCon->CcoMes->EditValue ?>"<?php echo $CCon->CcoMes->EditAttributes() ?>>
</span>
<input type="hidden" data-table="CCon" data-field="x_CcoMes" name="o<?php echo $CCon_list->RowIndex ?>_CcoMes" id="o<?php echo $CCon_list->RowIndex ?>_CcoMes" value="<?php echo ew_HtmlEncode($CCon->CcoMes->OldValue) ?>">
<?php } ?>
<?php if ($CCon->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $CCon_list->RowCnt ?>_CCon_CcoMes" class="form-group CCon_CcoMes">
<input type="text" data-table="CCon" data-field="x_CcoMes" name="x<?php echo $CCon_list->RowIndex ?>_CcoMes" id="x<?php echo $CCon_list->RowIndex ?>_CcoMes" size="30" maxlength="2" placeholder="<?php echo ew_HtmlEncode($CCon->CcoMes->getPlaceHolder()) ?>" value="<?php echo $CCon->CcoMes->EditValue ?>"<?php echo $CCon->CcoMes->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($CCon->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $CCon_list->RowCnt ?>_CCon_CcoMes" class="CCon_CcoMes">
<span<?php echo $CCon->CcoMes->ViewAttributes() ?>>
<?php echo $CCon->CcoMes->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($CCon->CcoConc->Visible) { // CcoConc ?>
		<td data-name="CcoConc"<?php echo $CCon->CcoConc->CellAttributes() ?>>
<?php if ($CCon->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $CCon_list->RowCnt ?>_CCon_CcoConc" class="form-group CCon_CcoConc">
<input type="text" data-table="CCon" data-field="x_CcoConc" name="x<?php echo $CCon_list->RowIndex ?>_CcoConc" id="x<?php echo $CCon_list->RowIndex ?>_CcoConc" size="30" placeholder="<?php echo ew_HtmlEncode($CCon->CcoConc->getPlaceHolder()) ?>" value="<?php echo $CCon->CcoConc->EditValue ?>"<?php echo $CCon->CcoConc->EditAttributes() ?>>
</span>
<input type="hidden" data-table="CCon" data-field="x_CcoConc" name="o<?php echo $CCon_list->RowIndex ?>_CcoConc" id="o<?php echo $CCon_list->RowIndex ?>_CcoConc" value="<?php echo ew_HtmlEncode($CCon->CcoConc->OldValue) ?>">
<?php } ?>
<?php if ($CCon->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $CCon_list->RowCnt ?>_CCon_CcoConc" class="form-group CCon_CcoConc">
<input type="text" data-table="CCon" data-field="x_CcoConc" name="x<?php echo $CCon_list->RowIndex ?>_CcoConc" id="x<?php echo $CCon_list->RowIndex ?>_CcoConc" size="30" placeholder="<?php echo ew_HtmlEncode($CCon->CcoConc->getPlaceHolder()) ?>" value="<?php echo $CCon->CcoConc->EditValue ?>"<?php echo $CCon->CcoConc->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($CCon->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $CCon_list->RowCnt ?>_CCon_CcoConc" class="CCon_CcoConc">
<span<?php echo $CCon->CcoConc->ViewAttributes() ?>>
<?php echo $CCon->CcoConc->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($CCon->CcoUsua->Visible) { // CcoUsua ?>
		<td data-name="CcoUsua"<?php echo $CCon->CcoUsua->CellAttributes() ?>>
<?php if ($CCon->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $CCon_list->RowCnt ?>_CCon_CcoUsua" class="form-group CCon_CcoUsua">
<input type="text" data-table="CCon" data-field="x_CcoUsua" name="x<?php echo $CCon_list->RowIndex ?>_CcoUsua" id="x<?php echo $CCon_list->RowIndex ?>_CcoUsua" size="30" placeholder="<?php echo ew_HtmlEncode($CCon->CcoUsua->getPlaceHolder()) ?>" value="<?php echo $CCon->CcoUsua->EditValue ?>"<?php echo $CCon->CcoUsua->EditAttributes() ?>>
</span>
<input type="hidden" data-table="CCon" data-field="x_CcoUsua" name="o<?php echo $CCon_list->RowIndex ?>_CcoUsua" id="o<?php echo $CCon_list->RowIndex ?>_CcoUsua" value="<?php echo ew_HtmlEncode($CCon->CcoUsua->OldValue) ?>">
<?php } ?>
<?php if ($CCon->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $CCon_list->RowCnt ?>_CCon_CcoUsua" class="form-group CCon_CcoUsua">
<input type="text" data-table="CCon" data-field="x_CcoUsua" name="x<?php echo $CCon_list->RowIndex ?>_CcoUsua" id="x<?php echo $CCon_list->RowIndex ?>_CcoUsua" size="30" placeholder="<?php echo ew_HtmlEncode($CCon->CcoUsua->getPlaceHolder()) ?>" value="<?php echo $CCon->CcoUsua->EditValue ?>"<?php echo $CCon->CcoUsua->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($CCon->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $CCon_list->RowCnt ?>_CCon_CcoUsua" class="CCon_CcoUsua">
<span<?php echo $CCon->CcoUsua->ViewAttributes() ?>>
<?php echo $CCon->CcoUsua->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($CCon->CcoFCre->Visible) { // CcoFCre ?>
		<td data-name="CcoFCre"<?php echo $CCon->CcoFCre->CellAttributes() ?>>
<?php if ($CCon->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $CCon_list->RowCnt ?>_CCon_CcoFCre" class="form-group CCon_CcoFCre">
<input type="text" data-table="CCon" data-field="x_CcoFCre" data-format="7" name="x<?php echo $CCon_list->RowIndex ?>_CcoFCre" id="x<?php echo $CCon_list->RowIndex ?>_CcoFCre" placeholder="<?php echo ew_HtmlEncode($CCon->CcoFCre->getPlaceHolder()) ?>" value="<?php echo $CCon->CcoFCre->EditValue ?>"<?php echo $CCon->CcoFCre->EditAttributes() ?>>
</span>
<input type="hidden" data-table="CCon" data-field="x_CcoFCre" name="o<?php echo $CCon_list->RowIndex ?>_CcoFCre" id="o<?php echo $CCon_list->RowIndex ?>_CcoFCre" value="<?php echo ew_HtmlEncode($CCon->CcoFCre->OldValue) ?>">
<?php } ?>
<?php if ($CCon->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $CCon_list->RowCnt ?>_CCon_CcoFCre" class="form-group CCon_CcoFCre">
<input type="text" data-table="CCon" data-field="x_CcoFCre" data-format="7" name="x<?php echo $CCon_list->RowIndex ?>_CcoFCre" id="x<?php echo $CCon_list->RowIndex ?>_CcoFCre" placeholder="<?php echo ew_HtmlEncode($CCon->CcoFCre->getPlaceHolder()) ?>" value="<?php echo $CCon->CcoFCre->EditValue ?>"<?php echo $CCon->CcoFCre->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($CCon->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $CCon_list->RowCnt ?>_CCon_CcoFCre" class="CCon_CcoFCre">
<span<?php echo $CCon->CcoFCre->ViewAttributes() ?>>
<?php echo $CCon->CcoFCre->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
<?php

// Render list options (body, right)
$CCon_list->ListOptions->Render("body", "right", $CCon_list->RowCnt);
?>
	</tr>
<?php if ($CCon->RowType == EW_ROWTYPE_ADD || $CCon->RowType == EW_ROWTYPE_EDIT) { ?>
<script type="text/javascript">
fCConlist.UpdateOpts(<?php echo $CCon_list->RowIndex ?>);
</script>
<?php } ?>
<?php
	}
	} // End delete row checking
	if ($CCon->CurrentAction <> "gridadd")
		if (!$CCon_list->Recordset->EOF) $CCon_list->Recordset->MoveNext();
}
?>
<?php
	if ($CCon->CurrentAction == "gridadd" || $CCon->CurrentAction == "gridedit") {
		$CCon_list->RowIndex = '$rowindex$';
		$CCon_list->LoadDefaultValues();

		// Set row properties
		$CCon->ResetAttrs();
		$CCon->RowAttrs = array_merge($CCon->RowAttrs, array('data-rowindex'=>$CCon_list->RowIndex, 'id'=>'r0_CCon', 'data-rowtype'=>EW_ROWTYPE_ADD));
		ew_AppendClass($CCon->RowAttrs["class"], "ewTemplate");
		$CCon->RowType = EW_ROWTYPE_ADD;

		// Render row
		$CCon_list->RenderRow();

		// Render list options
		$CCon_list->RenderListOptions();
		$CCon_list->StartRowCnt = 0;
?>
	<tr<?php echo $CCon->RowAttributes() ?>>
<?php

// Render list options (body, left)
$CCon_list->ListOptions->Render("body", "left", $CCon_list->RowIndex);
?>
	<?php if ($CCon->CcoCodi->Visible) { // CcoCodi ?>
		<td data-name="CcoCodi">
<input type="hidden" data-table="CCon" data-field="x_CcoCodi" name="o<?php echo $CCon_list->RowIndex ?>_CcoCodi" id="o<?php echo $CCon_list->RowIndex ?>_CcoCodi" value="<?php echo ew_HtmlEncode($CCon->CcoCodi->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($CCon->VcoCodi->Visible) { // VcoCodi ?>
		<td data-name="VcoCodi">
<span id="el$rowindex$_CCon_VcoCodi" class="form-group CCon_VcoCodi">
<select data-table="CCon" data-field="x_VcoCodi" data-value-separator="<?php echo ew_HtmlEncode(is_array($CCon->VcoCodi->DisplayValueSeparator) ? json_encode($CCon->VcoCodi->DisplayValueSeparator) : $CCon->VcoCodi->DisplayValueSeparator) ?>" id="x<?php echo $CCon_list->RowIndex ?>_VcoCodi" name="x<?php echo $CCon_list->RowIndex ?>_VcoCodi"<?php echo $CCon->VcoCodi->EditAttributes() ?>>
<?php
if (is_array($CCon->VcoCodi->EditValue)) {
	$arwrk = $CCon->VcoCodi->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($CCon->VcoCodi->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $CCon->VcoCodi->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($CCon->VcoCodi->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($CCon->VcoCodi->CurrentValue) ?>" selected><?php echo $CCon->VcoCodi->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $CCon->VcoCodi->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT \"VcoCodi\", \"VcoCodi\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"VCon\"";
$sWhereWrk = "";
$CCon->VcoCodi->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$CCon->VcoCodi->LookupFilters += array("f0" => "\"VcoCodi\" = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$CCon->Lookup_Selecting($CCon->VcoCodi, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $CCon->VcoCodi->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $CCon_list->RowIndex ?>_VcoCodi" id="s_x<?php echo $CCon_list->RowIndex ?>_VcoCodi" value="<?php echo $CCon->VcoCodi->LookupFilterQuery() ?>">
</span>
<input type="hidden" data-table="CCon" data-field="x_VcoCodi" name="o<?php echo $CCon_list->RowIndex ?>_VcoCodi" id="o<?php echo $CCon_list->RowIndex ?>_VcoCodi" value="<?php echo ew_HtmlEncode($CCon->VcoCodi->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($CCon->CcoFPag->Visible) { // CcoFPag ?>
		<td data-name="CcoFPag">
<span id="el$rowindex$_CCon_CcoFPag" class="form-group CCon_CcoFPag">
<input type="text" data-table="CCon" data-field="x_CcoFPag" data-format="7" name="x<?php echo $CCon_list->RowIndex ?>_CcoFPag" id="x<?php echo $CCon_list->RowIndex ?>_CcoFPag" placeholder="<?php echo ew_HtmlEncode($CCon->CcoFPag->getPlaceHolder()) ?>" value="<?php echo $CCon->CcoFPag->EditValue ?>"<?php echo $CCon->CcoFPag->EditAttributes() ?>>
</span>
<input type="hidden" data-table="CCon" data-field="x_CcoFPag" name="o<?php echo $CCon_list->RowIndex ?>_CcoFPag" id="o<?php echo $CCon_list->RowIndex ?>_CcoFPag" value="<?php echo ew_HtmlEncode($CCon->CcoFPag->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($CCon->CcoMont->Visible) { // CcoMont ?>
		<td data-name="CcoMont">
<span id="el$rowindex$_CCon_CcoMont" class="form-group CCon_CcoMont">
<input type="text" data-table="CCon" data-field="x_CcoMont" name="x<?php echo $CCon_list->RowIndex ?>_CcoMont" id="x<?php echo $CCon_list->RowIndex ?>_CcoMont" size="30" placeholder="<?php echo ew_HtmlEncode($CCon->CcoMont->getPlaceHolder()) ?>" value="<?php echo $CCon->CcoMont->EditValue ?>"<?php echo $CCon->CcoMont->EditAttributes() ?>>
</span>
<input type="hidden" data-table="CCon" data-field="x_CcoMont" name="o<?php echo $CCon_list->RowIndex ?>_CcoMont" id="o<?php echo $CCon_list->RowIndex ?>_CcoMont" value="<?php echo ew_HtmlEncode($CCon->CcoMont->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($CCon->CcoEsta->Visible) { // CcoEsta ?>
		<td data-name="CcoEsta">
<span id="el$rowindex$_CCon_CcoEsta" class="form-group CCon_CcoEsta">
<input type="text" data-table="CCon" data-field="x_CcoEsta" name="x<?php echo $CCon_list->RowIndex ?>_CcoEsta" id="x<?php echo $CCon_list->RowIndex ?>_CcoEsta" size="30" maxlength="1" placeholder="<?php echo ew_HtmlEncode($CCon->CcoEsta->getPlaceHolder()) ?>" value="<?php echo $CCon->CcoEsta->EditValue ?>"<?php echo $CCon->CcoEsta->EditAttributes() ?>>
</span>
<input type="hidden" data-table="CCon" data-field="x_CcoEsta" name="o<?php echo $CCon_list->RowIndex ?>_CcoEsta" id="o<?php echo $CCon_list->RowIndex ?>_CcoEsta" value="<?php echo ew_HtmlEncode($CCon->CcoEsta->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($CCon->CcoAnho->Visible) { // CcoAnho ?>
		<td data-name="CcoAnho">
<span id="el$rowindex$_CCon_CcoAnho" class="form-group CCon_CcoAnho">
<input type="text" data-table="CCon" data-field="x_CcoAnho" name="x<?php echo $CCon_list->RowIndex ?>_CcoAnho" id="x<?php echo $CCon_list->RowIndex ?>_CcoAnho" size="30" maxlength="4" placeholder="<?php echo ew_HtmlEncode($CCon->CcoAnho->getPlaceHolder()) ?>" value="<?php echo $CCon->CcoAnho->EditValue ?>"<?php echo $CCon->CcoAnho->EditAttributes() ?>>
</span>
<input type="hidden" data-table="CCon" data-field="x_CcoAnho" name="o<?php echo $CCon_list->RowIndex ?>_CcoAnho" id="o<?php echo $CCon_list->RowIndex ?>_CcoAnho" value="<?php echo ew_HtmlEncode($CCon->CcoAnho->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($CCon->CcoMes->Visible) { // CcoMes ?>
		<td data-name="CcoMes">
<span id="el$rowindex$_CCon_CcoMes" class="form-group CCon_CcoMes">
<input type="text" data-table="CCon" data-field="x_CcoMes" name="x<?php echo $CCon_list->RowIndex ?>_CcoMes" id="x<?php echo $CCon_list->RowIndex ?>_CcoMes" size="30" maxlength="2" placeholder="<?php echo ew_HtmlEncode($CCon->CcoMes->getPlaceHolder()) ?>" value="<?php echo $CCon->CcoMes->EditValue ?>"<?php echo $CCon->CcoMes->EditAttributes() ?>>
</span>
<input type="hidden" data-table="CCon" data-field="x_CcoMes" name="o<?php echo $CCon_list->RowIndex ?>_CcoMes" id="o<?php echo $CCon_list->RowIndex ?>_CcoMes" value="<?php echo ew_HtmlEncode($CCon->CcoMes->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($CCon->CcoConc->Visible) { // CcoConc ?>
		<td data-name="CcoConc">
<span id="el$rowindex$_CCon_CcoConc" class="form-group CCon_CcoConc">
<input type="text" data-table="CCon" data-field="x_CcoConc" name="x<?php echo $CCon_list->RowIndex ?>_CcoConc" id="x<?php echo $CCon_list->RowIndex ?>_CcoConc" size="30" placeholder="<?php echo ew_HtmlEncode($CCon->CcoConc->getPlaceHolder()) ?>" value="<?php echo $CCon->CcoConc->EditValue ?>"<?php echo $CCon->CcoConc->EditAttributes() ?>>
</span>
<input type="hidden" data-table="CCon" data-field="x_CcoConc" name="o<?php echo $CCon_list->RowIndex ?>_CcoConc" id="o<?php echo $CCon_list->RowIndex ?>_CcoConc" value="<?php echo ew_HtmlEncode($CCon->CcoConc->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($CCon->CcoUsua->Visible) { // CcoUsua ?>
		<td data-name="CcoUsua">
<span id="el$rowindex$_CCon_CcoUsua" class="form-group CCon_CcoUsua">
<input type="text" data-table="CCon" data-field="x_CcoUsua" name="x<?php echo $CCon_list->RowIndex ?>_CcoUsua" id="x<?php echo $CCon_list->RowIndex ?>_CcoUsua" size="30" placeholder="<?php echo ew_HtmlEncode($CCon->CcoUsua->getPlaceHolder()) ?>" value="<?php echo $CCon->CcoUsua->EditValue ?>"<?php echo $CCon->CcoUsua->EditAttributes() ?>>
</span>
<input type="hidden" data-table="CCon" data-field="x_CcoUsua" name="o<?php echo $CCon_list->RowIndex ?>_CcoUsua" id="o<?php echo $CCon_list->RowIndex ?>_CcoUsua" value="<?php echo ew_HtmlEncode($CCon->CcoUsua->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($CCon->CcoFCre->Visible) { // CcoFCre ?>
		<td data-name="CcoFCre">
<span id="el$rowindex$_CCon_CcoFCre" class="form-group CCon_CcoFCre">
<input type="text" data-table="CCon" data-field="x_CcoFCre" data-format="7" name="x<?php echo $CCon_list->RowIndex ?>_CcoFCre" id="x<?php echo $CCon_list->RowIndex ?>_CcoFCre" placeholder="<?php echo ew_HtmlEncode($CCon->CcoFCre->getPlaceHolder()) ?>" value="<?php echo $CCon->CcoFCre->EditValue ?>"<?php echo $CCon->CcoFCre->EditAttributes() ?>>
</span>
<input type="hidden" data-table="CCon" data-field="x_CcoFCre" name="o<?php echo $CCon_list->RowIndex ?>_CcoFCre" id="o<?php echo $CCon_list->RowIndex ?>_CcoFCre" value="<?php echo ew_HtmlEncode($CCon->CcoFCre->OldValue) ?>">
</td>
	<?php } ?>
<?php

// Render list options (body, right)
$CCon_list->ListOptions->Render("body", "right", $CCon_list->RowCnt);
?>
<script type="text/javascript">
fCConlist.UpdateOpts(<?php echo $CCon_list->RowIndex ?>);
</script>
	</tr>
<?php
}
?>
</tbody>
</table>
<?php } ?>
<?php if ($CCon->CurrentAction == "gridadd") { ?>
<input type="hidden" name="a_list" id="a_list" value="gridinsert">
<input type="hidden" name="<?php echo $CCon_list->FormKeyCountName ?>" id="<?php echo $CCon_list->FormKeyCountName ?>" value="<?php echo $CCon_list->KeyCount ?>">
<?php echo $CCon_list->MultiSelectKey ?>
<?php } ?>
<?php if ($CCon->CurrentAction == "gridedit") { ?>
<input type="hidden" name="a_list" id="a_list" value="gridupdate">
<input type="hidden" name="<?php echo $CCon_list->FormKeyCountName ?>" id="<?php echo $CCon_list->FormKeyCountName ?>" value="<?php echo $CCon_list->KeyCount ?>">
<?php echo $CCon_list->MultiSelectKey ?>
<?php } ?>
<?php if ($CCon->CurrentAction == "") { ?>
<input type="hidden" name="a_list" id="a_list" value="">
<?php } ?>
</div>
</form>
<?php

// Close recordset
if ($CCon_list->Recordset)
	$CCon_list->Recordset->Close();
?>
<div class="panel-footer ewGridLowerPanel">
<?php if ($CCon->CurrentAction <> "gridadd" && $CCon->CurrentAction <> "gridedit") { ?>
<form name="ewPagerForm" class="ewForm form-inline ewPagerForm" action="<?php echo ew_CurrentPage() ?>">
<?php if (!isset($CCon_list->Pager)) $CCon_list->Pager = new cPrevNextPager($CCon_list->StartRec, $CCon_list->DisplayRecs, $CCon_list->TotalRecs) ?>
<?php if ($CCon_list->Pager->RecordCount > 0) { ?>
<div class="ewPager">
<span><?php echo $Language->Phrase("Page") ?>&nbsp;</span>
<div class="ewPrevNext"><div class="input-group">
<div class="input-group-btn">
<!--first page button-->
	<?php if ($CCon_list->Pager->FirstButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerFirst") ?>" href="<?php echo $CCon_list->PageUrl() ?>start=<?php echo $CCon_list->Pager->FirstButton->Start ?>"><span class="icon-first ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerFirst") ?>"><span class="icon-first ewIcon"></span></a>
	<?php } ?>
<!--previous page button-->
	<?php if ($CCon_list->Pager->PrevButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerPrevious") ?>" href="<?php echo $CCon_list->PageUrl() ?>start=<?php echo $CCon_list->Pager->PrevButton->Start ?>"><span class="icon-prev ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerPrevious") ?>"><span class="icon-prev ewIcon"></span></a>
	<?php } ?>
</div>
<!--current page number-->
	<input class="form-control input-sm" type="text" name="<?php echo EW_TABLE_PAGE_NO ?>" value="<?php echo $CCon_list->Pager->CurrentPage ?>">
<div class="input-group-btn">
<!--next page button-->
	<?php if ($CCon_list->Pager->NextButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerNext") ?>" href="<?php echo $CCon_list->PageUrl() ?>start=<?php echo $CCon_list->Pager->NextButton->Start ?>"><span class="icon-next ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerNext") ?>"><span class="icon-next ewIcon"></span></a>
	<?php } ?>
<!--last page button-->
	<?php if ($CCon_list->Pager->LastButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerLast") ?>" href="<?php echo $CCon_list->PageUrl() ?>start=<?php echo $CCon_list->Pager->LastButton->Start ?>"><span class="icon-last ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerLast") ?>"><span class="icon-last ewIcon"></span></a>
	<?php } ?>
</div>
</div>
</div>
<span>&nbsp;<?php echo $Language->Phrase("of") ?>&nbsp;<?php echo $CCon_list->Pager->PageCount ?></span>
</div>
<div class="ewPager ewRec">
	<span><?php echo $Language->Phrase("Record") ?>&nbsp;<?php echo $CCon_list->Pager->FromIndex ?>&nbsp;<?php echo $Language->Phrase("To") ?>&nbsp;<?php echo $CCon_list->Pager->ToIndex ?>&nbsp;<?php echo $Language->Phrase("Of") ?>&nbsp;<?php echo $CCon_list->Pager->RecordCount ?></span>
</div>
<?php } ?>
</form>
<?php } ?>
<div class="ewListOtherOptions">
<?php
	foreach ($CCon_list->OtherOptions as &$option)
		$option->Render("body", "bottom");
?>
</div>
<div class="clearfix"></div>
</div>
</div>
<?php } ?>
<?php if ($CCon_list->TotalRecs == 0 && $CCon->CurrentAction == "") { // Show other options ?>
<div class="ewListOtherOptions">
<?php
	foreach ($CCon_list->OtherOptions as &$option) {
		$option->ButtonClass = "";
		$option->Render("body", "");
	}
?>
</div>
<div class="clearfix"></div>
<?php } ?>
<script type="text/javascript">
fCConlistsrch.Init();
fCConlistsrch.FilterList = <?php echo $CCon_list->GetFilterList() ?>;
fCConlist.Init();
</script>
<?php
$CCon_list->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$CCon_list->Page_Terminate();
?>
