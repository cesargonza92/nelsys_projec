<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "DCreinfo.php" ?>
<?php include_once "Usuainfo.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$DCre_add = NULL; // Initialize page object first

class cDCre_add extends cDCre {

	// Page ID
	var $PageID = 'add';

	// Project ID
	var $ProjectID = "{04439FF7-B43F-460F-8514-F71C8FF9E679}";

	// Table name
	var $TableName = 'DCre';

	// Page object name
	var $PageObjName = 'DCre_add';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (DCre)
		if (!isset($GLOBALS["DCre"]) || get_class($GLOBALS["DCre"]) == "cDCre") {
			$GLOBALS["DCre"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["DCre"];
		}

		// Table object (Usua)
		if (!isset($GLOBALS['Usua'])) $GLOBALS['Usua'] = new cUsua();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'add', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'DCre', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (Usua)
		if (!isset($UserTable)) {
			$UserTable = new cUsua();
			$UserTableConn = Conn($UserTable->DBID);
		}
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanAdd()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage($Language->Phrase("NoPermission")); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("DCrelist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}

		// Create form object
		$objForm = new cFormObj();
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Process auto fill
		if (@$_POST["ajax"] == "autofill") {
			$results = $this->GetAutoFill(@$_POST["name"], @$_POST["q"]);
			if ($results) {

				// Clean output buffer
				if (!EW_DEBUG_ENABLED && ob_get_length())
					ob_end_clean();
				echo $results;
				$this->Page_Terminate();
				exit();
			}
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $DCre;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($DCre);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $FormClassName = "form-horizontal ewForm ewAddForm";
	var $DbMasterFilter = "";
	var $DbDetailFilter = "";
	var $StartRec;
	var $Priv = 0;
	var $OldRecordset;
	var $CopyRecord;

	// 
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError;

		// Process form if post back
		if (@$_POST["a_add"] <> "") {
			$this->CurrentAction = $_POST["a_add"]; // Get form action
			$this->CopyRecord = $this->LoadOldRecord(); // Load old recordset
			$this->LoadFormValues(); // Load form values
		} else { // Not post back

			// Load key values from QueryString
			$this->CopyRecord = TRUE;
			if (@$_GET["DcrCodi"] != "") {
				$this->DcrCodi->setQueryStringValue($_GET["DcrCodi"]);
				$this->setKey("DcrCodi", $this->DcrCodi->CurrentValue); // Set up key
			} else {
				$this->setKey("DcrCodi", ""); // Clear key
				$this->CopyRecord = FALSE;
			}
			if ($this->CopyRecord) {
				$this->CurrentAction = "C"; // Copy record
			} else {
				$this->CurrentAction = "I"; // Display blank record
				$this->LoadDefaultValues(); // Load default values
			}
		}

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Validate form if post back
		if (@$_POST["a_add"] <> "") {
			if (!$this->ValidateForm()) {
				$this->CurrentAction = "I"; // Form error, reset action
				$this->EventCancelled = TRUE; // Event cancelled
				$this->RestoreFormValues(); // Restore form values
				$this->setFailureMessage($gsFormError);
			}
		}

		// Perform action based on action code
		switch ($this->CurrentAction) {
			case "I": // Blank record, no action required
				break;
			case "C": // Copy an existing record
				if (!$this->LoadRow()) { // Load record based on key
					if ($this->getFailureMessage() == "") $this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
					$this->Page_Terminate("DCrelist.php"); // No matching record, return to list
				}
				break;
			case "A": // Add new record
				$this->SendEmail = TRUE; // Send email on add success
				if ($this->AddRow($this->OldRecordset)) { // Add successful
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage($Language->Phrase("AddSuccess")); // Set up success message
					$sReturnUrl = $this->getReturnUrl();
					if (ew_GetPageName($sReturnUrl) == "DCreview.php")
						$sReturnUrl = $this->GetViewUrl(); // View paging, return to view page with keyurl directly
					$this->Page_Terminate($sReturnUrl); // Clean up and return
				} else {
					$this->EventCancelled = TRUE; // Event cancelled
					$this->RestoreFormValues(); // Add failed, restore form values
				}
		}

		// Render row based on row type
		if ($this->CurrentAction == "F") { // Confirm page
			$this->RowType = EW_ROWTYPE_VIEW; // Render view type
		} else {
			$this->RowType = EW_ROWTYPE_ADD; // Render add type
		}

		// Render row
		$this->ResetAttrs();
		$this->RenderRow();
	}

	// Get upload files
	function GetUploadFiles() {
		global $objForm, $Language;

		// Get upload data
	}

	// Load default values
	function LoadDefaultValues() {
		$this->DcrCodi->CurrentValue = NULL;
		$this->DcrCodi->OldValue = $this->DcrCodi->CurrentValue;
		$this->VcrCodi->CurrentValue = NULL;
		$this->VcrCodi->OldValue = $this->VcrCodi->CurrentValue;
		$this->DcrFech->CurrentValue = NULL;
		$this->DcrFech->OldValue = $this->DcrFech->CurrentValue;
		$this->DcrMont->CurrentValue = NULL;
		$this->DcrMont->OldValue = $this->DcrMont->CurrentValue;
		$this->DcrSCuo->CurrentValue = NULL;
		$this->DcrSCuo->OldValue = $this->DcrSCuo->CurrentValue;
		$this->DcrNCuo->CurrentValue = NULL;
		$this->DcrNCuo->OldValue = $this->DcrNCuo->CurrentValue;
		$this->DcrEsta->CurrentValue = NULL;
		$this->DcrEsta->OldValue = $this->DcrEsta->CurrentValue;
		$this->DcrUsu->CurrentValue = NULL;
		$this->DcrUsu->OldValue = $this->DcrUsu->CurrentValue;
	}

	// Load form values
	function LoadFormValues() {

		// Load from form
		global $objForm;
		if (!$this->DcrCodi->FldIsDetailKey) {
			$this->DcrCodi->setFormValue($objForm->GetValue("x_DcrCodi"));
		}
		if (!$this->VcrCodi->FldIsDetailKey) {
			$this->VcrCodi->setFormValue($objForm->GetValue("x_VcrCodi"));
		}
		if (!$this->DcrFech->FldIsDetailKey) {
			$this->DcrFech->setFormValue($objForm->GetValue("x_DcrFech"));
			$this->DcrFech->CurrentValue = ew_UnFormatDateTime($this->DcrFech->CurrentValue, 7);
		}
		if (!$this->DcrMont->FldIsDetailKey) {
			$this->DcrMont->setFormValue($objForm->GetValue("x_DcrMont"));
		}
		if (!$this->DcrSCuo->FldIsDetailKey) {
			$this->DcrSCuo->setFormValue($objForm->GetValue("x_DcrSCuo"));
		}
		if (!$this->DcrNCuo->FldIsDetailKey) {
			$this->DcrNCuo->setFormValue($objForm->GetValue("x_DcrNCuo"));
		}
		if (!$this->DcrEsta->FldIsDetailKey) {
			$this->DcrEsta->setFormValue($objForm->GetValue("x_DcrEsta"));
		}
		if (!$this->DcrUsu->FldIsDetailKey) {
			$this->DcrUsu->setFormValue($objForm->GetValue("x_DcrUsu"));
		}
	}

	// Restore form values
	function RestoreFormValues() {
		global $objForm;
		$this->LoadOldRecord();
		$this->DcrCodi->CurrentValue = $this->DcrCodi->FormValue;
		$this->VcrCodi->CurrentValue = $this->VcrCodi->FormValue;
		$this->DcrFech->CurrentValue = $this->DcrFech->FormValue;
		$this->DcrFech->CurrentValue = ew_UnFormatDateTime($this->DcrFech->CurrentValue, 7);
		$this->DcrMont->CurrentValue = $this->DcrMont->FormValue;
		$this->DcrSCuo->CurrentValue = $this->DcrSCuo->FormValue;
		$this->DcrNCuo->CurrentValue = $this->DcrNCuo->FormValue;
		$this->DcrEsta->CurrentValue = $this->DcrEsta->FormValue;
		$this->DcrUsu->CurrentValue = $this->DcrUsu->FormValue;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->DcrCodi->setDbValue($rs->fields('DcrCodi'));
		$this->VcrCodi->setDbValue($rs->fields('VcrCodi'));
		$this->DcrFech->setDbValue($rs->fields('DcrFech'));
		$this->DcrMont->setDbValue($rs->fields('DcrMont'));
		$this->DcrSCuo->setDbValue($rs->fields('DcrSCuo'));
		$this->DcrNCuo->setDbValue($rs->fields('DcrNCuo'));
		$this->DcrEsta->setDbValue($rs->fields('DcrEsta'));
		$this->DcrUsu->setDbValue($rs->fields('DcrUsu'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->DcrCodi->DbValue = $row['DcrCodi'];
		$this->VcrCodi->DbValue = $row['VcrCodi'];
		$this->DcrFech->DbValue = $row['DcrFech'];
		$this->DcrMont->DbValue = $row['DcrMont'];
		$this->DcrSCuo->DbValue = $row['DcrSCuo'];
		$this->DcrNCuo->DbValue = $row['DcrNCuo'];
		$this->DcrEsta->DbValue = $row['DcrEsta'];
		$this->DcrUsu->DbValue = $row['DcrUsu'];
	}

	// Load old record
	function LoadOldRecord() {

		// Load key values from Session
		$bValidKey = TRUE;
		if (strval($this->getKey("DcrCodi")) <> "")
			$this->DcrCodi->CurrentValue = $this->getKey("DcrCodi"); // DcrCodi
		else
			$bValidKey = FALSE;

		// Load old recordset
		if ($bValidKey) {
			$this->CurrentFilter = $this->KeyFilter();
			$sSql = $this->SQL();
			$conn = &$this->Connection();
			$this->OldRecordset = ew_LoadRecordset($sSql, $conn);
			$this->LoadRowValues($this->OldRecordset); // Load row values
		} else {
			$this->OldRecordset = NULL;
		}
		return $bValidKey;
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		// Convert decimal values if posted back

		if ($this->DcrMont->FormValue == $this->DcrMont->CurrentValue && is_numeric(ew_StrToFloat($this->DcrMont->CurrentValue)))
			$this->DcrMont->CurrentValue = ew_StrToFloat($this->DcrMont->CurrentValue);

		// Convert decimal values if posted back
		if ($this->DcrSCuo->FormValue == $this->DcrSCuo->CurrentValue && is_numeric(ew_StrToFloat($this->DcrSCuo->CurrentValue)))
			$this->DcrSCuo->CurrentValue = ew_StrToFloat($this->DcrSCuo->CurrentValue);

		// Call Row_Rendering event
		$this->Row_Rendering();

		// Common render codes for all row types
		// DcrCodi
		// VcrCodi
		// DcrFech
		// DcrMont
		// DcrSCuo
		// DcrNCuo
		// DcrEsta
		// DcrUsu

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// DcrCodi
		$this->DcrCodi->ViewValue = $this->DcrCodi->CurrentValue;
		$this->DcrCodi->ViewCustomAttributes = "";

		// VcrCodi
		if (strval($this->VcrCodi->CurrentValue) <> "") {
			$sFilterWrk = "\"VcrCodi\"" . ew_SearchString("=", $this->VcrCodi->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT \"VcrCodi\", \"VcrCodi\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"VCre\"";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->VcrCodi, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->VcrCodi->ViewValue = $this->VcrCodi->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->VcrCodi->ViewValue = $this->VcrCodi->CurrentValue;
			}
		} else {
			$this->VcrCodi->ViewValue = NULL;
		}
		$this->VcrCodi->ViewCustomAttributes = "";

		// DcrFech
		$this->DcrFech->ViewValue = $this->DcrFech->CurrentValue;
		$this->DcrFech->ViewValue = ew_FormatDateTime($this->DcrFech->ViewValue, 7);
		$this->DcrFech->ViewCustomAttributes = "";

		// DcrMont
		$this->DcrMont->ViewValue = $this->DcrMont->CurrentValue;
		$this->DcrMont->ViewCustomAttributes = "";

		// DcrSCuo
		$this->DcrSCuo->ViewValue = $this->DcrSCuo->CurrentValue;
		$this->DcrSCuo->ViewCustomAttributes = "";

		// DcrNCuo
		$this->DcrNCuo->ViewValue = $this->DcrNCuo->CurrentValue;
		$this->DcrNCuo->ViewCustomAttributes = "";

		// DcrEsta
		$this->DcrEsta->ViewValue = $this->DcrEsta->CurrentValue;
		$this->DcrEsta->ViewCustomAttributes = "";

		// DcrUsu
		$this->DcrUsu->ViewValue = $this->DcrUsu->CurrentValue;
		$this->DcrUsu->ViewCustomAttributes = "";

			// DcrCodi
			$this->DcrCodi->LinkCustomAttributes = "";
			$this->DcrCodi->HrefValue = "";
			$this->DcrCodi->TooltipValue = "";

			// VcrCodi
			$this->VcrCodi->LinkCustomAttributes = "";
			$this->VcrCodi->HrefValue = "";
			$this->VcrCodi->TooltipValue = "";

			// DcrFech
			$this->DcrFech->LinkCustomAttributes = "";
			$this->DcrFech->HrefValue = "";
			$this->DcrFech->TooltipValue = "";

			// DcrMont
			$this->DcrMont->LinkCustomAttributes = "";
			$this->DcrMont->HrefValue = "";
			$this->DcrMont->TooltipValue = "";

			// DcrSCuo
			$this->DcrSCuo->LinkCustomAttributes = "";
			$this->DcrSCuo->HrefValue = "";
			$this->DcrSCuo->TooltipValue = "";

			// DcrNCuo
			$this->DcrNCuo->LinkCustomAttributes = "";
			$this->DcrNCuo->HrefValue = "";
			$this->DcrNCuo->TooltipValue = "";

			// DcrEsta
			$this->DcrEsta->LinkCustomAttributes = "";
			$this->DcrEsta->HrefValue = "";
			$this->DcrEsta->TooltipValue = "";

			// DcrUsu
			$this->DcrUsu->LinkCustomAttributes = "";
			$this->DcrUsu->HrefValue = "";
			$this->DcrUsu->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_ADD) { // Add row

			// DcrCodi
			$this->DcrCodi->EditAttrs["class"] = "form-control";
			$this->DcrCodi->EditCustomAttributes = "";
			$this->DcrCodi->EditValue = ew_HtmlEncode($this->DcrCodi->CurrentValue);
			$this->DcrCodi->PlaceHolder = ew_RemoveHtml($this->DcrCodi->FldCaption());

			// VcrCodi
			$this->VcrCodi->EditAttrs["class"] = "form-control";
			$this->VcrCodi->EditCustomAttributes = "";
			if (trim(strval($this->VcrCodi->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "\"VcrCodi\"" . ew_SearchString("=", $this->VcrCodi->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT \"VcrCodi\", \"VcrCodi\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\", '' AS \"SelectFilterFld\", '' AS \"SelectFilterFld2\", '' AS \"SelectFilterFld3\", '' AS \"SelectFilterFld4\" FROM \"public\".\"VCre\"";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->VcrCodi, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->VcrCodi->EditValue = $arwrk;

			// DcrFech
			$this->DcrFech->EditAttrs["class"] = "form-control";
			$this->DcrFech->EditCustomAttributes = "";
			$this->DcrFech->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->DcrFech->CurrentValue, 7));
			$this->DcrFech->PlaceHolder = ew_RemoveHtml($this->DcrFech->FldCaption());

			// DcrMont
			$this->DcrMont->EditAttrs["class"] = "form-control";
			$this->DcrMont->EditCustomAttributes = "";
			$this->DcrMont->EditValue = ew_HtmlEncode($this->DcrMont->CurrentValue);
			$this->DcrMont->PlaceHolder = ew_RemoveHtml($this->DcrMont->FldCaption());
			if (strval($this->DcrMont->EditValue) <> "" && is_numeric($this->DcrMont->EditValue)) $this->DcrMont->EditValue = ew_FormatNumber($this->DcrMont->EditValue, -2, -1, -2, 0);

			// DcrSCuo
			$this->DcrSCuo->EditAttrs["class"] = "form-control";
			$this->DcrSCuo->EditCustomAttributes = "";
			$this->DcrSCuo->EditValue = ew_HtmlEncode($this->DcrSCuo->CurrentValue);
			$this->DcrSCuo->PlaceHolder = ew_RemoveHtml($this->DcrSCuo->FldCaption());
			if (strval($this->DcrSCuo->EditValue) <> "" && is_numeric($this->DcrSCuo->EditValue)) $this->DcrSCuo->EditValue = ew_FormatNumber($this->DcrSCuo->EditValue, -2, -1, -2, 0);

			// DcrNCuo
			$this->DcrNCuo->EditAttrs["class"] = "form-control";
			$this->DcrNCuo->EditCustomAttributes = "";
			$this->DcrNCuo->EditValue = ew_HtmlEncode($this->DcrNCuo->CurrentValue);
			$this->DcrNCuo->PlaceHolder = ew_RemoveHtml($this->DcrNCuo->FldCaption());

			// DcrEsta
			$this->DcrEsta->EditAttrs["class"] = "form-control";
			$this->DcrEsta->EditCustomAttributes = "";
			$this->DcrEsta->EditValue = ew_HtmlEncode($this->DcrEsta->CurrentValue);
			$this->DcrEsta->PlaceHolder = ew_RemoveHtml($this->DcrEsta->FldCaption());

			// DcrUsu
			$this->DcrUsu->EditAttrs["class"] = "form-control";
			$this->DcrUsu->EditCustomAttributes = "";
			$this->DcrUsu->EditValue = ew_HtmlEncode($this->DcrUsu->CurrentValue);
			$this->DcrUsu->PlaceHolder = ew_RemoveHtml($this->DcrUsu->FldCaption());

			// Edit refer script
			// DcrCodi

			$this->DcrCodi->HrefValue = "";

			// VcrCodi
			$this->VcrCodi->HrefValue = "";

			// DcrFech
			$this->DcrFech->HrefValue = "";

			// DcrMont
			$this->DcrMont->HrefValue = "";

			// DcrSCuo
			$this->DcrSCuo->HrefValue = "";

			// DcrNCuo
			$this->DcrNCuo->HrefValue = "";

			// DcrEsta
			$this->DcrEsta->HrefValue = "";

			// DcrUsu
			$this->DcrUsu->HrefValue = "";
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate form
	function ValidateForm() {
		global $Language, $gsFormError;

		// Initialize form error message
		$gsFormError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return ($gsFormError == "");
		if (!$this->DcrCodi->FldIsDetailKey && !is_null($this->DcrCodi->FormValue) && $this->DcrCodi->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->DcrCodi->FldCaption(), $this->DcrCodi->ReqErrMsg));
		}
		if (!ew_CheckInteger($this->DcrCodi->FormValue)) {
			ew_AddMessage($gsFormError, $this->DcrCodi->FldErrMsg());
		}
		if (!$this->VcrCodi->FldIsDetailKey && !is_null($this->VcrCodi->FormValue) && $this->VcrCodi->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->VcrCodi->FldCaption(), $this->VcrCodi->ReqErrMsg));
		}
		if (!$this->DcrFech->FldIsDetailKey && !is_null($this->DcrFech->FormValue) && $this->DcrFech->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->DcrFech->FldCaption(), $this->DcrFech->ReqErrMsg));
		}
		if (!ew_CheckEuroDate($this->DcrFech->FormValue)) {
			ew_AddMessage($gsFormError, $this->DcrFech->FldErrMsg());
		}
		if (!$this->DcrMont->FldIsDetailKey && !is_null($this->DcrMont->FormValue) && $this->DcrMont->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->DcrMont->FldCaption(), $this->DcrMont->ReqErrMsg));
		}
		if (!ew_CheckNumber($this->DcrMont->FormValue)) {
			ew_AddMessage($gsFormError, $this->DcrMont->FldErrMsg());
		}
		if (!$this->DcrSCuo->FldIsDetailKey && !is_null($this->DcrSCuo->FormValue) && $this->DcrSCuo->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->DcrSCuo->FldCaption(), $this->DcrSCuo->ReqErrMsg));
		}
		if (!ew_CheckNumber($this->DcrSCuo->FormValue)) {
			ew_AddMessage($gsFormError, $this->DcrSCuo->FldErrMsg());
		}
		if (!$this->DcrNCuo->FldIsDetailKey && !is_null($this->DcrNCuo->FormValue) && $this->DcrNCuo->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->DcrNCuo->FldCaption(), $this->DcrNCuo->ReqErrMsg));
		}
		if (!ew_CheckInteger($this->DcrNCuo->FormValue)) {
			ew_AddMessage($gsFormError, $this->DcrNCuo->FldErrMsg());
		}
		if (!$this->DcrEsta->FldIsDetailKey && !is_null($this->DcrEsta->FormValue) && $this->DcrEsta->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->DcrEsta->FldCaption(), $this->DcrEsta->ReqErrMsg));
		}
		if (!$this->DcrUsu->FldIsDetailKey && !is_null($this->DcrUsu->FormValue) && $this->DcrUsu->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->DcrUsu->FldCaption(), $this->DcrUsu->ReqErrMsg));
		}

		// Return validate result
		$ValidateForm = ($gsFormError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateForm = $ValidateForm && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsFormError, $sFormCustomError);
		}
		return $ValidateForm;
	}

	// Add record
	function AddRow($rsold = NULL) {
		global $Language, $Security;
		if ($this->DcrCodi->CurrentValue <> "") { // Check field with unique index
			$sFilter = "(DcrCodi = " . ew_AdjustSql($this->DcrCodi->CurrentValue, $this->DBID) . ")";
			$rsChk = $this->LoadRs($sFilter);
			if ($rsChk && !$rsChk->EOF) {
				$sIdxErrMsg = str_replace("%f", $this->DcrCodi->FldCaption(), $Language->Phrase("DupIndex"));
				$sIdxErrMsg = str_replace("%v", $this->DcrCodi->CurrentValue, $sIdxErrMsg);
				$this->setFailureMessage($sIdxErrMsg);
				$rsChk->Close();
				return FALSE;
			}
		}
		$conn = &$this->Connection();

		// Load db values from rsold
		if ($rsold) {
			$this->LoadDbValues($rsold);
		}
		$rsnew = array();

		// DcrCodi
		$this->DcrCodi->SetDbValueDef($rsnew, $this->DcrCodi->CurrentValue, 0, FALSE);

		// VcrCodi
		$this->VcrCodi->SetDbValueDef($rsnew, $this->VcrCodi->CurrentValue, 0, FALSE);

		// DcrFech
		$this->DcrFech->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->DcrFech->CurrentValue, 7), ew_CurrentDate(), FALSE);

		// DcrMont
		$this->DcrMont->SetDbValueDef($rsnew, $this->DcrMont->CurrentValue, 0, FALSE);

		// DcrSCuo
		$this->DcrSCuo->SetDbValueDef($rsnew, $this->DcrSCuo->CurrentValue, 0, FALSE);

		// DcrNCuo
		$this->DcrNCuo->SetDbValueDef($rsnew, $this->DcrNCuo->CurrentValue, 0, FALSE);

		// DcrEsta
		$this->DcrEsta->SetDbValueDef($rsnew, $this->DcrEsta->CurrentValue, "", FALSE);

		// DcrUsu
		$this->DcrUsu->SetDbValueDef($rsnew, $this->DcrUsu->CurrentValue, "", FALSE);

		// Call Row Inserting event
		$rs = ($rsold == NULL) ? NULL : $rsold->fields;
		$bInsertRow = $this->Row_Inserting($rs, $rsnew);

		// Check if key value entered
		if ($bInsertRow && $this->ValidateKey && strval($rsnew['DcrCodi']) == "") {
			$this->setFailureMessage($Language->Phrase("InvalidKeyValue"));
			$bInsertRow = FALSE;
		}

		// Check for duplicate key
		if ($bInsertRow && $this->ValidateKey) {
			$sFilter = $this->KeyFilter();
			$rsChk = $this->LoadRs($sFilter);
			if ($rsChk && !$rsChk->EOF) {
				$sKeyErrMsg = str_replace("%f", $sFilter, $Language->Phrase("DupKey"));
				$this->setFailureMessage($sKeyErrMsg);
				$rsChk->Close();
				$bInsertRow = FALSE;
			}
		}
		if ($bInsertRow) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			$AddRow = $this->Insert($rsnew);
			$conn->raiseErrorFn = '';
			if ($AddRow) {
			}
		} else {
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("InsertCancelled"));
			}
			$AddRow = FALSE;
		}
		if ($AddRow) {

			// Call Row Inserted event
			$rs = ($rsold == NULL) ? NULL : $rsold->fields;
			$this->Row_Inserted($rs, $rsnew);
		}
		return $AddRow;
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, "DCrelist.php", "", $this->TableVar, TRUE);
		$PageId = ($this->CurrentAction == "C") ? "Copy" : "Add";
		$Breadcrumb->Add("add", $PageId, $url);
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($DCre_add)) $DCre_add = new cDCre_add();

// Page init
$DCre_add->Page_Init();

// Page main
$DCre_add->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$DCre_add->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "add";
var CurrentForm = fDCreadd = new ew_Form("fDCreadd", "add");

// Validate form
fDCreadd.Validate = function() {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.GetForm(), $fobj = $(fobj);
	if ($fobj.find("#a_confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.FormKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = $fobj.find("#a_list").val() == "gridinsert";
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
			elm = this.GetElements("x" + infix + "_DcrCodi");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $DCre->DcrCodi->FldCaption(), $DCre->DcrCodi->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_DcrCodi");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($DCre->DcrCodi->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_VcrCodi");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $DCre->VcrCodi->FldCaption(), $DCre->VcrCodi->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_DcrFech");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $DCre->DcrFech->FldCaption(), $DCre->DcrFech->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_DcrFech");
			if (elm && !ew_CheckEuroDate(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($DCre->DcrFech->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_DcrMont");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $DCre->DcrMont->FldCaption(), $DCre->DcrMont->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_DcrMont");
			if (elm && !ew_CheckNumber(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($DCre->DcrMont->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_DcrSCuo");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $DCre->DcrSCuo->FldCaption(), $DCre->DcrSCuo->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_DcrSCuo");
			if (elm && !ew_CheckNumber(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($DCre->DcrSCuo->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_DcrNCuo");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $DCre->DcrNCuo->FldCaption(), $DCre->DcrNCuo->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_DcrNCuo");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($DCre->DcrNCuo->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_DcrEsta");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $DCre->DcrEsta->FldCaption(), $DCre->DcrEsta->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_DcrUsu");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $DCre->DcrUsu->FldCaption(), $DCre->DcrUsu->ReqErrMsg)) ?>");

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
	}

	// Process detail forms
	var dfs = $fobj.find("input[name='detailpage']").get();
	for (var i = 0; i < dfs.length; i++) {
		var df = dfs[i], val = df.value;
		if (val && ewForms[val])
			if (!ewForms[val].Validate())
				return false;
	}
	return true;
}

// Form_CustomValidate event
fDCreadd.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fDCreadd.ValidateRequired = true;
<?php } else { ?>
fDCreadd.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
fDCreadd.Lists["x_VcrCodi"] = {"LinkField":"x_VcrCodi","Ajax":true,"AutoFill":false,"DisplayFields":["x_VcrCodi","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};

// Form object for search
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php $DCre_add->ShowPageHeader(); ?>
<?php
$DCre_add->ShowMessage();
?>
<form name="fDCreadd" id="fDCreadd" class="<?php echo $DCre_add->FormClassName ?>" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($DCre_add->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $DCre_add->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="DCre">
<input type="hidden" name="a_add" id="a_add" value="A">
<?php if ($DCre->CurrentAction == "F") { // Confirm page ?>
<input type="hidden" name="a_confirm" id="a_confirm" value="F">
<?php } ?>
<div>
<?php if ($DCre->DcrCodi->Visible) { // DcrCodi ?>
	<div id="r_DcrCodi" class="form-group">
		<label id="elh_DCre_DcrCodi" for="x_DcrCodi" class="col-sm-2 control-label ewLabel"><?php echo $DCre->DcrCodi->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $DCre->DcrCodi->CellAttributes() ?>>
<?php if ($DCre->CurrentAction <> "F") { ?>
<span id="el_DCre_DcrCodi">
<input type="text" data-table="DCre" data-field="x_DcrCodi" name="x_DcrCodi" id="x_DcrCodi" size="30" placeholder="<?php echo ew_HtmlEncode($DCre->DcrCodi->getPlaceHolder()) ?>" value="<?php echo $DCre->DcrCodi->EditValue ?>"<?php echo $DCre->DcrCodi->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_DCre_DcrCodi">
<span<?php echo $DCre->DcrCodi->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $DCre->DcrCodi->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="DCre" data-field="x_DcrCodi" name="x_DcrCodi" id="x_DcrCodi" value="<?php echo ew_HtmlEncode($DCre->DcrCodi->FormValue) ?>">
<?php } ?>
<?php echo $DCre->DcrCodi->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($DCre->VcrCodi->Visible) { // VcrCodi ?>
	<div id="r_VcrCodi" class="form-group">
		<label id="elh_DCre_VcrCodi" for="x_VcrCodi" class="col-sm-2 control-label ewLabel"><?php echo $DCre->VcrCodi->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $DCre->VcrCodi->CellAttributes() ?>>
<?php if ($DCre->CurrentAction <> "F") { ?>
<span id="el_DCre_VcrCodi">
<select data-table="DCre" data-field="x_VcrCodi" data-value-separator="<?php echo ew_HtmlEncode(is_array($DCre->VcrCodi->DisplayValueSeparator) ? json_encode($DCre->VcrCodi->DisplayValueSeparator) : $DCre->VcrCodi->DisplayValueSeparator) ?>" id="x_VcrCodi" name="x_VcrCodi"<?php echo $DCre->VcrCodi->EditAttributes() ?>>
<?php
if (is_array($DCre->VcrCodi->EditValue)) {
	$arwrk = $DCre->VcrCodi->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($DCre->VcrCodi->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $DCre->VcrCodi->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($DCre->VcrCodi->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($DCre->VcrCodi->CurrentValue) ?>" selected><?php echo $DCre->VcrCodi->CurrentValue ?></option>
<?php
    }
}
?>
</select>
<?php
$sSqlWrk = "SELECT \"VcrCodi\", \"VcrCodi\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"VCre\"";
$sWhereWrk = "";
$DCre->VcrCodi->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$DCre->VcrCodi->LookupFilters += array("f0" => "\"VcrCodi\" = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$DCre->Lookup_Selecting($DCre->VcrCodi, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $DCre->VcrCodi->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x_VcrCodi" id="s_x_VcrCodi" value="<?php echo $DCre->VcrCodi->LookupFilterQuery() ?>">
</span>
<?php } else { ?>
<span id="el_DCre_VcrCodi">
<span<?php echo $DCre->VcrCodi->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $DCre->VcrCodi->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="DCre" data-field="x_VcrCodi" name="x_VcrCodi" id="x_VcrCodi" value="<?php echo ew_HtmlEncode($DCre->VcrCodi->FormValue) ?>">
<?php } ?>
<?php echo $DCre->VcrCodi->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($DCre->DcrFech->Visible) { // DcrFech ?>
	<div id="r_DcrFech" class="form-group">
		<label id="elh_DCre_DcrFech" for="x_DcrFech" class="col-sm-2 control-label ewLabel"><?php echo $DCre->DcrFech->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $DCre->DcrFech->CellAttributes() ?>>
<?php if ($DCre->CurrentAction <> "F") { ?>
<span id="el_DCre_DcrFech">
<input type="text" data-table="DCre" data-field="x_DcrFech" data-format="7" name="x_DcrFech" id="x_DcrFech" placeholder="<?php echo ew_HtmlEncode($DCre->DcrFech->getPlaceHolder()) ?>" value="<?php echo $DCre->DcrFech->EditValue ?>"<?php echo $DCre->DcrFech->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_DCre_DcrFech">
<span<?php echo $DCre->DcrFech->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $DCre->DcrFech->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="DCre" data-field="x_DcrFech" name="x_DcrFech" id="x_DcrFech" value="<?php echo ew_HtmlEncode($DCre->DcrFech->FormValue) ?>">
<?php } ?>
<?php echo $DCre->DcrFech->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($DCre->DcrMont->Visible) { // DcrMont ?>
	<div id="r_DcrMont" class="form-group">
		<label id="elh_DCre_DcrMont" for="x_DcrMont" class="col-sm-2 control-label ewLabel"><?php echo $DCre->DcrMont->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $DCre->DcrMont->CellAttributes() ?>>
<?php if ($DCre->CurrentAction <> "F") { ?>
<span id="el_DCre_DcrMont">
<input type="text" data-table="DCre" data-field="x_DcrMont" name="x_DcrMont" id="x_DcrMont" size="30" placeholder="<?php echo ew_HtmlEncode($DCre->DcrMont->getPlaceHolder()) ?>" value="<?php echo $DCre->DcrMont->EditValue ?>"<?php echo $DCre->DcrMont->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_DCre_DcrMont">
<span<?php echo $DCre->DcrMont->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $DCre->DcrMont->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="DCre" data-field="x_DcrMont" name="x_DcrMont" id="x_DcrMont" value="<?php echo ew_HtmlEncode($DCre->DcrMont->FormValue) ?>">
<?php } ?>
<?php echo $DCre->DcrMont->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($DCre->DcrSCuo->Visible) { // DcrSCuo ?>
	<div id="r_DcrSCuo" class="form-group">
		<label id="elh_DCre_DcrSCuo" for="x_DcrSCuo" class="col-sm-2 control-label ewLabel"><?php echo $DCre->DcrSCuo->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $DCre->DcrSCuo->CellAttributes() ?>>
<?php if ($DCre->CurrentAction <> "F") { ?>
<span id="el_DCre_DcrSCuo">
<input type="text" data-table="DCre" data-field="x_DcrSCuo" name="x_DcrSCuo" id="x_DcrSCuo" size="30" placeholder="<?php echo ew_HtmlEncode($DCre->DcrSCuo->getPlaceHolder()) ?>" value="<?php echo $DCre->DcrSCuo->EditValue ?>"<?php echo $DCre->DcrSCuo->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_DCre_DcrSCuo">
<span<?php echo $DCre->DcrSCuo->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $DCre->DcrSCuo->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="DCre" data-field="x_DcrSCuo" name="x_DcrSCuo" id="x_DcrSCuo" value="<?php echo ew_HtmlEncode($DCre->DcrSCuo->FormValue) ?>">
<?php } ?>
<?php echo $DCre->DcrSCuo->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($DCre->DcrNCuo->Visible) { // DcrNCuo ?>
	<div id="r_DcrNCuo" class="form-group">
		<label id="elh_DCre_DcrNCuo" for="x_DcrNCuo" class="col-sm-2 control-label ewLabel"><?php echo $DCre->DcrNCuo->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $DCre->DcrNCuo->CellAttributes() ?>>
<?php if ($DCre->CurrentAction <> "F") { ?>
<span id="el_DCre_DcrNCuo">
<input type="text" data-table="DCre" data-field="x_DcrNCuo" name="x_DcrNCuo" id="x_DcrNCuo" size="30" placeholder="<?php echo ew_HtmlEncode($DCre->DcrNCuo->getPlaceHolder()) ?>" value="<?php echo $DCre->DcrNCuo->EditValue ?>"<?php echo $DCre->DcrNCuo->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_DCre_DcrNCuo">
<span<?php echo $DCre->DcrNCuo->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $DCre->DcrNCuo->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="DCre" data-field="x_DcrNCuo" name="x_DcrNCuo" id="x_DcrNCuo" value="<?php echo ew_HtmlEncode($DCre->DcrNCuo->FormValue) ?>">
<?php } ?>
<?php echo $DCre->DcrNCuo->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($DCre->DcrEsta->Visible) { // DcrEsta ?>
	<div id="r_DcrEsta" class="form-group">
		<label id="elh_DCre_DcrEsta" for="x_DcrEsta" class="col-sm-2 control-label ewLabel"><?php echo $DCre->DcrEsta->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $DCre->DcrEsta->CellAttributes() ?>>
<?php if ($DCre->CurrentAction <> "F") { ?>
<span id="el_DCre_DcrEsta">
<input type="text" data-table="DCre" data-field="x_DcrEsta" name="x_DcrEsta" id="x_DcrEsta" size="30" maxlength="1" placeholder="<?php echo ew_HtmlEncode($DCre->DcrEsta->getPlaceHolder()) ?>" value="<?php echo $DCre->DcrEsta->EditValue ?>"<?php echo $DCre->DcrEsta->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_DCre_DcrEsta">
<span<?php echo $DCre->DcrEsta->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $DCre->DcrEsta->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="DCre" data-field="x_DcrEsta" name="x_DcrEsta" id="x_DcrEsta" value="<?php echo ew_HtmlEncode($DCre->DcrEsta->FormValue) ?>">
<?php } ?>
<?php echo $DCre->DcrEsta->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($DCre->DcrUsu->Visible) { // DcrUsu ?>
	<div id="r_DcrUsu" class="form-group">
		<label id="elh_DCre_DcrUsu" for="x_DcrUsu" class="col-sm-2 control-label ewLabel"><?php echo $DCre->DcrUsu->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $DCre->DcrUsu->CellAttributes() ?>>
<?php if ($DCre->CurrentAction <> "F") { ?>
<span id="el_DCre_DcrUsu">
<input type="text" data-table="DCre" data-field="x_DcrUsu" name="x_DcrUsu" id="x_DcrUsu" size="30" placeholder="<?php echo ew_HtmlEncode($DCre->DcrUsu->getPlaceHolder()) ?>" value="<?php echo $DCre->DcrUsu->EditValue ?>"<?php echo $DCre->DcrUsu->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el_DCre_DcrUsu">
<span<?php echo $DCre->DcrUsu->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $DCre->DcrUsu->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="DCre" data-field="x_DcrUsu" name="x_DcrUsu" id="x_DcrUsu" value="<?php echo ew_HtmlEncode($DCre->DcrUsu->FormValue) ?>">
<?php } ?>
<?php echo $DCre->DcrUsu->CustomMsg ?></div></div>
	</div>
<?php } ?>
</div>
<div class="form-group">
	<div class="col-sm-offset-2 col-sm-10">
<?php if ($DCre->CurrentAction <> "F") { // Confirm page ?>
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit" onclick="this.form.a_add.value='F';"><?php echo $Language->Phrase("AddBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="button" data-href="<?php echo $DCre_add->getReturnUrl() ?>"><?php echo $Language->Phrase("CancelBtn") ?></button>
<?php } else { ?>
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("ConfirmBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="submit" onclick="this.form.a_add.value='X';"><?php echo $Language->Phrase("CancelBtn") ?></button>
<?php } ?>
	</div>
</div>
</form>
<script type="text/javascript">
fDCreadd.Init();
</script>
<?php
$DCre_add->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$DCre_add->Page_Terminate();
?>
