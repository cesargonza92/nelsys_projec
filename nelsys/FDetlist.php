<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "FDetinfo.php" ?>
<?php include_once "Usuainfo.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$FDet_list = NULL; // Initialize page object first

class cFDet_list extends cFDet {

	// Page ID
	var $PageID = 'list';

	// Project ID
	var $ProjectID = "{04439FF7-B43F-460F-8514-F71C8FF9E679}";

	// Table name
	var $TableName = 'FDet';

	// Page object name
	var $PageObjName = 'FDet_list';

	// Grid form hidden field names
	var $FormName = 'fFDetlist';
	var $FormActionName = 'k_action';
	var $FormKeyName = 'k_key';
	var $FormOldKeyName = 'k_oldkey';
	var $FormBlankRowName = 'k_blankrow';
	var $FormKeyCountName = 'key_count';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Page URLs
	var $AddUrl;
	var $EditUrl;
	var $CopyUrl;
	var $DeleteUrl;
	var $ViewUrl;
	var $ListUrl;

	// Export URLs
	var $ExportPrintUrl;
	var $ExportHtmlUrl;
	var $ExportExcelUrl;
	var $ExportWordUrl;
	var $ExportXmlUrl;
	var $ExportCsvUrl;
	var $ExportPdfUrl;

	// Custom export
	var $ExportExcelCustom = FALSE;
	var $ExportWordCustom = FALSE;
	var $ExportPdfCustom = FALSE;
	var $ExportEmailCustom = FALSE;

	// Update URLs
	var $InlineAddUrl;
	var $InlineCopyUrl;
	var $InlineEditUrl;
	var $GridAddUrl;
	var $GridEditUrl;
	var $MultiDeleteUrl;
	var $MultiUpdateUrl;

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (FDet)
		if (!isset($GLOBALS["FDet"]) || get_class($GLOBALS["FDet"]) == "cFDet") {
			$GLOBALS["FDet"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["FDet"];
		}

		// Initialize URLs
		$this->ExportPrintUrl = $this->PageUrl() . "export=print";
		$this->ExportExcelUrl = $this->PageUrl() . "export=excel";
		$this->ExportWordUrl = $this->PageUrl() . "export=word";
		$this->ExportHtmlUrl = $this->PageUrl() . "export=html";
		$this->ExportXmlUrl = $this->PageUrl() . "export=xml";
		$this->ExportCsvUrl = $this->PageUrl() . "export=csv";
		$this->ExportPdfUrl = $this->PageUrl() . "export=pdf";
		$this->AddUrl = "FDetadd.php";
		$this->InlineAddUrl = $this->PageUrl() . "a=add";
		$this->GridAddUrl = $this->PageUrl() . "a=gridadd";
		$this->GridEditUrl = $this->PageUrl() . "a=gridedit";
		$this->MultiDeleteUrl = "FDetdelete.php";
		$this->MultiUpdateUrl = "FDetupdate.php";

		// Table object (Usua)
		if (!isset($GLOBALS['Usua'])) $GLOBALS['Usua'] = new cUsua();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'list', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'FDet', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (Usua)
		if (!isset($UserTable)) {
			$UserTable = new cUsua();
			$UserTableConn = Conn($UserTable->DBID);
		}

		// List options
		$this->ListOptions = new cListOptions();
		$this->ListOptions->TableVar = $this->TableVar;

		// Export options
		$this->ExportOptions = new cListOptions();
		$this->ExportOptions->Tag = "div";
		$this->ExportOptions->TagClassName = "ewExportOption";

		// Other options
		$this->OtherOptions['addedit'] = new cListOptions();
		$this->OtherOptions['addedit']->Tag = "div";
		$this->OtherOptions['addedit']->TagClassName = "ewAddEditOption";
		$this->OtherOptions['detail'] = new cListOptions();
		$this->OtherOptions['detail']->Tag = "div";
		$this->OtherOptions['detail']->TagClassName = "ewDetailOption";
		$this->OtherOptions['action'] = new cListOptions();
		$this->OtherOptions['action']->Tag = "div";
		$this->OtherOptions['action']->TagClassName = "ewActionOption";

		// Filter options
		$this->FilterOptions = new cListOptions();
		$this->FilterOptions->Tag = "div";
		$this->FilterOptions->TagClassName = "ewFilterOption fFDetlistsrch";

		// List actions
		$this->ListActions = new cListActions();
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanList()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage($Language->Phrase("NoPermission")); // Set no permission
			$this->Page_Terminate(ew_GetUrl("index.php"));
		}

		// Create form object
		$objForm = new cFormObj();
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action

		// Get grid add count
		$gridaddcnt = @$_GET[EW_TABLE_GRID_ADD_ROW_COUNT];
		if (is_numeric($gridaddcnt) && $gridaddcnt > 0)
			$this->GridAddRowCount = $gridaddcnt;

		// Set up list options
		$this->SetupListOptions();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Process auto fill
		if (@$_POST["ajax"] == "autofill") {
			$results = $this->GetAutoFill(@$_POST["name"], @$_POST["q"]);
			if ($results) {

				// Clean output buffer
				if (!EW_DEBUG_ENABLED && ob_get_length())
					ob_end_clean();
				echo $results;
				$this->Page_Terminate();
				exit();
			}
		}

		// Create Token
		$this->CreateToken();

		// Setup other options
		$this->SetupOtherOptions();

		// Set up custom action (compatible with old version)
		foreach ($this->CustomActions as $name => $action)
			$this->ListActions->Add($name, $action);

		// Show checkbox column if multiple action
		foreach ($this->ListActions->Items as $listaction) {
			if ($listaction->Select == EW_ACTION_MULTIPLE && $listaction->Allow) {
				$this->ListOptions->Items["checkbox"]->Visible = TRUE;
				break;
			}
		}
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $FDet;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($FDet);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}

	// Class variables
	var $ListOptions; // List options
	var $ExportOptions; // Export options
	var $SearchOptions; // Search options
	var $OtherOptions = array(); // Other options
	var $FilterOptions; // Filter options
	var $ListActions; // List actions
	var $SelectedCount = 0;
	var $SelectedIndex = 0;
	var $DisplayRecs = 20;
	var $StartRec;
	var $StopRec;
	var $TotalRecs = 0;
	var $RecRange = 10;
	var $Pager;
	var $DefaultSearchWhere = ""; // Default search WHERE clause
	var $SearchWhere = ""; // Search WHERE clause
	var $RecCnt = 0; // Record count
	var $EditRowCnt;
	var $StartRowCnt = 1;
	var $RowCnt = 0;
	var $Attrs = array(); // Row attributes and cell attributes
	var $RowIndex = 0; // Row index
	var $KeyCount = 0; // Key count
	var $RowAction = ""; // Row action
	var $RowOldKey = ""; // Row old key (for copy)
	var $RecPerRow = 0;
	var $MultiColumnClass;
	var $MultiColumnEditClass = "col-sm-12";
	var $MultiColumnCnt = 12;
	var $MultiColumnEditCnt = 12;
	var $GridCnt = 0;
	var $ColCnt = 0;
	var $DbMasterFilter = ""; // Master filter
	var $DbDetailFilter = ""; // Detail filter
	var $MasterRecordExists;	
	var $MultiSelectKey;
	var $Command;
	var $RestoreSearch = FALSE;
	var $DetailPages;
	var $Recordset;
	var $OldRecordset;

	//
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError, $gsSearchError, $Security;

		// Search filters
		$sSrchAdvanced = ""; // Advanced search filter
		$sSrchBasic = ""; // Basic search filter
		$sFilter = "";

		// Get command
		$this->Command = strtolower(@$_GET["cmd"]);
		if ($this->IsPageRequest()) { // Validate request

			// Process list action first
			if ($this->ProcessListAction()) // Ajax request
				$this->Page_Terminate();

			// Handle reset command
			$this->ResetCmd();

			// Set up Breadcrumb
			if ($this->Export == "")
				$this->SetupBreadcrumb();

			// Check QueryString parameters
			if (@$_GET["a"] <> "") {
				$this->CurrentAction = $_GET["a"];

				// Clear inline mode
				if ($this->CurrentAction == "cancel")
					$this->ClearInlineMode();

				// Switch to grid edit mode
				if ($this->CurrentAction == "gridedit")
					$this->GridEditMode();

				// Switch to grid add mode
				if ($this->CurrentAction == "gridadd")
					$this->GridAddMode();
			} else {
				if (@$_POST["a_list"] <> "") {
					$this->CurrentAction = $_POST["a_list"]; // Get action

					// Grid Update
					if (($this->CurrentAction == "gridupdate" || $this->CurrentAction == "gridoverwrite") && @$_SESSION[EW_SESSION_INLINE_MODE] == "gridedit") {
						if ($this->ValidateGridForm()) {
							$bGridUpdate = $this->GridUpdate();
						} else {
							$bGridUpdate = FALSE;
							$this->setFailureMessage($gsFormError);
						}
						if (!$bGridUpdate) {
							$this->EventCancelled = TRUE;
							$this->CurrentAction = "gridedit"; // Stay in Grid Edit mode
						}
					}

					// Grid Insert
					if ($this->CurrentAction == "gridinsert" && @$_SESSION[EW_SESSION_INLINE_MODE] == "gridadd") {
						if ($this->ValidateGridForm()) {
							$bGridInsert = $this->GridInsert();
						} else {
							$bGridInsert = FALSE;
							$this->setFailureMessage($gsFormError);
						}
						if (!$bGridInsert) {
							$this->EventCancelled = TRUE;
							$this->CurrentAction = "gridadd"; // Stay in Grid Add mode
						}
					}
				}
			}

			// Hide list options
			if ($this->Export <> "") {
				$this->ListOptions->HideAllOptions(array("sequence"));
				$this->ListOptions->UseDropDownButton = FALSE; // Disable drop down button
				$this->ListOptions->UseButtonGroup = FALSE; // Disable button group
			} elseif ($this->CurrentAction == "gridadd" || $this->CurrentAction == "gridedit") {
				$this->ListOptions->HideAllOptions();
				$this->ListOptions->UseDropDownButton = FALSE; // Disable drop down button
				$this->ListOptions->UseButtonGroup = FALSE; // Disable button group
			}

			// Hide options
			if ($this->Export <> "" || $this->CurrentAction <> "") {
				$this->ExportOptions->HideAllOptions();
				$this->FilterOptions->HideAllOptions();
			}

			// Hide other options
			if ($this->Export <> "") {
				foreach ($this->OtherOptions as &$option)
					$option->HideAllOptions();
			}

			// Show grid delete link for grid add / grid edit
			if ($this->AllowAddDeleteRow) {
				if ($this->CurrentAction == "gridadd" || $this->CurrentAction == "gridedit") {
					$item = $this->ListOptions->GetItem("griddelete");
					if ($item) $item->Visible = TRUE;
				}
			}

			// Get default search criteria
			ew_AddFilter($this->DefaultSearchWhere, $this->BasicSearchWhere(TRUE));

			// Get basic search values
			$this->LoadBasicSearchValues();

			// Restore filter list
			$this->RestoreFilterList();

			// Restore search parms from Session if not searching / reset / export
			if (($this->Export <> "" || $this->Command <> "search" && $this->Command <> "reset" && $this->Command <> "resetall") && $this->CheckSearchParms())
				$this->RestoreSearchParms();

			// Call Recordset SearchValidated event
			$this->Recordset_SearchValidated();

			// Set up sorting order
			$this->SetUpSortOrder();

			// Get basic search criteria
			if ($gsSearchError == "")
				$sSrchBasic = $this->BasicSearchWhere();
		}

		// Restore display records
		if ($this->getRecordsPerPage() <> "") {
			$this->DisplayRecs = $this->getRecordsPerPage(); // Restore from Session
		} else {
			$this->DisplayRecs = 20; // Load default
		}

		// Load Sorting Order
		$this->LoadSortOrder();

		// Load search default if no existing search criteria
		if (!$this->CheckSearchParms()) {

			// Load basic search from default
			$this->BasicSearch->LoadDefault();
			if ($this->BasicSearch->Keyword != "")
				$sSrchBasic = $this->BasicSearchWhere();
		}

		// Build search criteria
		ew_AddFilter($this->SearchWhere, $sSrchAdvanced);
		ew_AddFilter($this->SearchWhere, $sSrchBasic);

		// Call Recordset_Searching event
		$this->Recordset_Searching($this->SearchWhere);

		// Save search criteria
		if ($this->Command == "search" && !$this->RestoreSearch) {
			$this->setSearchWhere($this->SearchWhere); // Save to Session
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} else {
			$this->SearchWhere = $this->getSearchWhere();
		}

		// Build filter
		$sFilter = "";
		if (!$Security->CanList())
			$sFilter = "(0=1)"; // Filter all records
		ew_AddFilter($sFilter, $this->DbDetailFilter);
		ew_AddFilter($sFilter, $this->SearchWhere);

		// Set up filter in session
		$this->setSessionWhere($sFilter);
		$this->CurrentFilter = "";

		// Load record count first
		if (!$this->IsAddOrEdit()) {
			$bSelectLimit = $this->UseSelectLimit;
			if ($bSelectLimit) {
				$this->TotalRecs = $this->SelectRecordCount();
			} else {
				if ($this->Recordset = $this->LoadRecordset())
					$this->TotalRecs = $this->Recordset->RecordCount();
			}
		}

		// Search options
		$this->SetupSearchOptions();
	}

	//  Exit inline mode
	function ClearInlineMode() {
		$this->FdePrec->FormValue = ""; // Clear form value
		$this->LastAction = $this->CurrentAction; // Save last action
		$this->CurrentAction = ""; // Clear action
		$_SESSION[EW_SESSION_INLINE_MODE] = ""; // Clear inline mode
	}

	// Switch to Grid Add mode
	function GridAddMode() {
		$_SESSION[EW_SESSION_INLINE_MODE] = "gridadd"; // Enabled grid add
	}

	// Switch to Grid Edit mode
	function GridEditMode() {
		$_SESSION[EW_SESSION_INLINE_MODE] = "gridedit"; // Enable grid edit
	}

	// Perform update to grid
	function GridUpdate() {
		global $Language, $objForm, $gsFormError;
		$bGridUpdate = TRUE;

		// Get old recordset
		$this->CurrentFilter = $this->BuildKeyFilter();
		if ($this->CurrentFilter == "")
			$this->CurrentFilter = "0=1";
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		if ($rs = $conn->Execute($sSql)) {
			$rsold = $rs->GetRows();
			$rs->Close();
		}

		// Call Grid Updating event
		if (!$this->Grid_Updating($rsold)) {
			if ($this->getFailureMessage() == "")
				$this->setFailureMessage($Language->Phrase("GridEditCancelled")); // Set grid edit cancelled message
			return FALSE;
		}

		// Begin transaction
		$conn->BeginTrans();
		$sKey = "";

		// Update row index and get row key
		$objForm->Index = -1;
		$rowcnt = strval($objForm->GetValue($this->FormKeyCountName));
		if ($rowcnt == "" || !is_numeric($rowcnt))
			$rowcnt = 0;

		// Update all rows based on key
		for ($rowindex = 1; $rowindex <= $rowcnt; $rowindex++) {
			$objForm->Index = $rowindex;
			$rowkey = strval($objForm->GetValue($this->FormKeyName));
			$rowaction = strval($objForm->GetValue($this->FormActionName));

			// Load all values and keys
			if ($rowaction <> "insertdelete") { // Skip insert then deleted rows
				$this->LoadFormValues(); // Get form values
				if ($rowaction == "" || $rowaction == "edit" || $rowaction == "delete") {
					$bGridUpdate = $this->SetupKeyValues($rowkey); // Set up key values
				} else {
					$bGridUpdate = TRUE;
				}

				// Skip empty row
				if ($rowaction == "insert" && $this->EmptyRow()) {

					// No action required
				// Validate form and insert/update/delete record

				} elseif ($bGridUpdate) {
					if ($rowaction == "delete") {
						$this->CurrentFilter = $this->KeyFilter();
						$bGridUpdate = $this->DeleteRows(); // Delete this row
					} else if (!$this->ValidateForm()) {
						$bGridUpdate = FALSE; // Form error, reset action
						$this->setFailureMessage($gsFormError);
					} else {
						if ($rowaction == "insert") {
							$bGridUpdate = $this->AddRow(); // Insert this row
						} else {
							if ($rowkey <> "") {
								$this->SendEmail = FALSE; // Do not send email on update success
								$bGridUpdate = $this->EditRow(); // Update this row
							}
						} // End update
					}
				}
				if ($bGridUpdate) {
					if ($sKey <> "") $sKey .= ", ";
					$sKey .= $rowkey;
				} else {
					break;
				}
			}
		}
		if ($bGridUpdate) {
			$conn->CommitTrans(); // Commit transaction

			// Get new recordset
			if ($rs = $conn->Execute($sSql)) {
				$rsnew = $rs->GetRows();
				$rs->Close();
			}

			// Call Grid_Updated event
			$this->Grid_Updated($rsold, $rsnew);
			if ($this->getSuccessMessage() == "")
				$this->setSuccessMessage($Language->Phrase("UpdateSuccess")); // Set up update success message
			$this->ClearInlineMode(); // Clear inline edit mode
		} else {
			$conn->RollbackTrans(); // Rollback transaction
			if ($this->getFailureMessage() == "")
				$this->setFailureMessage($Language->Phrase("UpdateFailed")); // Set update failed message
		}
		return $bGridUpdate;
	}

	// Build filter for all keys
	function BuildKeyFilter() {
		global $objForm;
		$sWrkFilter = "";

		// Update row index and get row key
		$rowindex = 1;
		$objForm->Index = $rowindex;
		$sThisKey = strval($objForm->GetValue($this->FormKeyName));
		while ($sThisKey <> "") {
			if ($this->SetupKeyValues($sThisKey)) {
				$sFilter = $this->KeyFilter();
				if ($sWrkFilter <> "") $sWrkFilter .= " OR ";
				$sWrkFilter .= $sFilter;
			} else {
				$sWrkFilter = "0=1";
				break;
			}

			// Update row index and get row key
			$rowindex++; // Next row
			$objForm->Index = $rowindex;
			$sThisKey = strval($objForm->GetValue($this->FormKeyName));
		}
		return $sWrkFilter;
	}

	// Set up key values
	function SetupKeyValues($key) {
		$arrKeyFlds = explode($GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"], $key);
		if (count($arrKeyFlds) >= 2) {
			$this->FdeCodi->setFormValue($arrKeyFlds[0]);
			if (!is_numeric($this->FdeCodi->FormValue))
				return FALSE;
			$this->FcaCodi->setFormValue($arrKeyFlds[1]);
			if (!is_numeric($this->FcaCodi->FormValue))
				return FALSE;
		}
		return TRUE;
	}

	// Perform Grid Add
	function GridInsert() {
		global $Language, $objForm, $gsFormError;
		$rowindex = 1;
		$bGridInsert = FALSE;
		$conn = &$this->Connection();

		// Call Grid Inserting event
		if (!$this->Grid_Inserting()) {
			if ($this->getFailureMessage() == "") {
				$this->setFailureMessage($Language->Phrase("GridAddCancelled")); // Set grid add cancelled message
			}
			return FALSE;
		}

		// Begin transaction
		$conn->BeginTrans();

		// Init key filter
		$sWrkFilter = "";
		$addcnt = 0;
		$sKey = "";

		// Get row count
		$objForm->Index = -1;
		$rowcnt = strval($objForm->GetValue($this->FormKeyCountName));
		if ($rowcnt == "" || !is_numeric($rowcnt))
			$rowcnt = 0;

		// Insert all rows
		for ($rowindex = 1; $rowindex <= $rowcnt; $rowindex++) {

			// Load current row values
			$objForm->Index = $rowindex;
			$rowaction = strval($objForm->GetValue($this->FormActionName));
			if ($rowaction <> "" && $rowaction <> "insert")
				continue; // Skip
			$this->LoadFormValues(); // Get form values
			if (!$this->EmptyRow()) {
				$addcnt++;
				$this->SendEmail = FALSE; // Do not send email on insert success

				// Validate form
				if (!$this->ValidateForm()) {
					$bGridInsert = FALSE; // Form error, reset action
					$this->setFailureMessage($gsFormError);
				} else {
					$bGridInsert = $this->AddRow($this->OldRecordset); // Insert this row
				}
				if ($bGridInsert) {
					if ($sKey <> "") $sKey .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
					$sKey .= $this->FdeCodi->CurrentValue;
					if ($sKey <> "") $sKey .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
					$sKey .= $this->FcaCodi->CurrentValue;

					// Add filter for this record
					$sFilter = $this->KeyFilter();
					if ($sWrkFilter <> "") $sWrkFilter .= " OR ";
					$sWrkFilter .= $sFilter;
				} else {
					break;
				}
			}
		}
		if ($addcnt == 0) { // No record inserted
			$this->setFailureMessage($Language->Phrase("NoAddRecord"));
			$bGridInsert = FALSE;
		}
		if ($bGridInsert) {
			$conn->CommitTrans(); // Commit transaction

			// Get new recordset
			$this->CurrentFilter = $sWrkFilter;
			$sSql = $this->SQL();
			if ($rs = $conn->Execute($sSql)) {
				$rsnew = $rs->GetRows();
				$rs->Close();
			}

			// Call Grid_Inserted event
			$this->Grid_Inserted($rsnew);
			if ($this->getSuccessMessage() == "")
				$this->setSuccessMessage($Language->Phrase("InsertSuccess")); // Set up insert success message
			$this->ClearInlineMode(); // Clear grid add mode
		} else {
			$conn->RollbackTrans(); // Rollback transaction
			if ($this->getFailureMessage() == "") {
				$this->setFailureMessage($Language->Phrase("InsertFailed")); // Set insert failed message
			}
		}
		return $bGridInsert;
	}

	// Check if empty row
	function EmptyRow() {
		global $objForm;
		if ($objForm->HasValue("x_FdeCodi") && $objForm->HasValue("o_FdeCodi") && $this->FdeCodi->CurrentValue <> $this->FdeCodi->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_FcaCodi") && $objForm->HasValue("o_FcaCodi") && $this->FcaCodi->CurrentValue <> $this->FcaCodi->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_FdeCant") && $objForm->HasValue("o_FdeCant") && $this->FdeCant->CurrentValue <> $this->FdeCant->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_FdePrec") && $objForm->HasValue("o_FdePrec") && $this->FdePrec->CurrentValue <> $this->FdePrec->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_FdeDesc") && $objForm->HasValue("o_FdeDesc") && $this->FdeDesc->CurrentValue <> $this->FdeDesc->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_FdeUsua") && $objForm->HasValue("o_FdeUsua") && $this->FdeUsua->CurrentValue <> $this->FdeUsua->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_FdeFCre") && $objForm->HasValue("o_FdeFCre") && $this->FdeFCre->CurrentValue <> $this->FdeFCre->OldValue)
			return FALSE;
		return TRUE;
	}

	// Validate grid form
	function ValidateGridForm() {
		global $objForm;

		// Get row count
		$objForm->Index = -1;
		$rowcnt = strval($objForm->GetValue($this->FormKeyCountName));
		if ($rowcnt == "" || !is_numeric($rowcnt))
			$rowcnt = 0;

		// Validate all records
		for ($rowindex = 1; $rowindex <= $rowcnt; $rowindex++) {

			// Load current row values
			$objForm->Index = $rowindex;
			$rowaction = strval($objForm->GetValue($this->FormActionName));
			if ($rowaction <> "delete" && $rowaction <> "insertdelete") {
				$this->LoadFormValues(); // Get form values
				if ($rowaction == "insert" && $this->EmptyRow()) {

					// Ignore
				} else if (!$this->ValidateForm()) {
					return FALSE;
				}
			}
		}
		return TRUE;
	}

	// Get all form values of the grid
	function GetGridFormValues() {
		global $objForm;

		// Get row count
		$objForm->Index = -1;
		$rowcnt = strval($objForm->GetValue($this->FormKeyCountName));
		if ($rowcnt == "" || !is_numeric($rowcnt))
			$rowcnt = 0;
		$rows = array();

		// Loop through all records
		for ($rowindex = 1; $rowindex <= $rowcnt; $rowindex++) {

			// Load current row values
			$objForm->Index = $rowindex;
			$rowaction = strval($objForm->GetValue($this->FormActionName));
			if ($rowaction <> "delete" && $rowaction <> "insertdelete") {
				$this->LoadFormValues(); // Get form values
				if ($rowaction == "insert" && $this->EmptyRow()) {

					// Ignore
				} else {
					$rows[] = $this->GetFieldValues("FormValue"); // Return row as array
				}
			}
		}
		return $rows; // Return as array of array
	}

	// Restore form values for current row
	function RestoreCurrentRowFormValues($idx) {
		global $objForm;

		// Get row based on current index
		$objForm->Index = $idx;
		$this->LoadFormValues(); // Load form values
	}

	// Get list of filters
	function GetFilterList() {

		// Initialize
		$sFilterList = "";
		$sFilterList = ew_Concat($sFilterList, $this->FdeCodi->AdvancedSearch->ToJSON(), ","); // Field FdeCodi
		$sFilterList = ew_Concat($sFilterList, $this->FcaCodi->AdvancedSearch->ToJSON(), ","); // Field FcaCodi
		$sFilterList = ew_Concat($sFilterList, $this->FdeCant->AdvancedSearch->ToJSON(), ","); // Field FdeCant
		$sFilterList = ew_Concat($sFilterList, $this->FdePrec->AdvancedSearch->ToJSON(), ","); // Field FdePrec
		$sFilterList = ew_Concat($sFilterList, $this->FdeDesc->AdvancedSearch->ToJSON(), ","); // Field FdeDesc
		$sFilterList = ew_Concat($sFilterList, $this->FdeUsua->AdvancedSearch->ToJSON(), ","); // Field FdeUsua
		$sFilterList = ew_Concat($sFilterList, $this->FdeFCre->AdvancedSearch->ToJSON(), ","); // Field FdeFCre
		if ($this->BasicSearch->Keyword <> "") {
			$sWrk = "\"" . EW_TABLE_BASIC_SEARCH . "\":\"" . ew_JsEncode2($this->BasicSearch->Keyword) . "\",\"" . EW_TABLE_BASIC_SEARCH_TYPE . "\":\"" . ew_JsEncode2($this->BasicSearch->Type) . "\"";
			$sFilterList = ew_Concat($sFilterList, $sWrk, ",");
		}

		// Return filter list in json
		return ($sFilterList <> "") ? "{" . $sFilterList . "}" : "null";
	}

	// Restore list of filters
	function RestoreFilterList() {

		// Return if not reset filter
		if (@$_POST["cmd"] <> "resetfilter")
			return FALSE;
		$filter = json_decode(ew_StripSlashes(@$_POST["filter"]), TRUE);
		$this->Command = "search";

		// Field FdeCodi
		$this->FdeCodi->AdvancedSearch->SearchValue = @$filter["x_FdeCodi"];
		$this->FdeCodi->AdvancedSearch->SearchOperator = @$filter["z_FdeCodi"];
		$this->FdeCodi->AdvancedSearch->SearchCondition = @$filter["v_FdeCodi"];
		$this->FdeCodi->AdvancedSearch->SearchValue2 = @$filter["y_FdeCodi"];
		$this->FdeCodi->AdvancedSearch->SearchOperator2 = @$filter["w_FdeCodi"];
		$this->FdeCodi->AdvancedSearch->Save();

		// Field FcaCodi
		$this->FcaCodi->AdvancedSearch->SearchValue = @$filter["x_FcaCodi"];
		$this->FcaCodi->AdvancedSearch->SearchOperator = @$filter["z_FcaCodi"];
		$this->FcaCodi->AdvancedSearch->SearchCondition = @$filter["v_FcaCodi"];
		$this->FcaCodi->AdvancedSearch->SearchValue2 = @$filter["y_FcaCodi"];
		$this->FcaCodi->AdvancedSearch->SearchOperator2 = @$filter["w_FcaCodi"];
		$this->FcaCodi->AdvancedSearch->Save();

		// Field FdeCant
		$this->FdeCant->AdvancedSearch->SearchValue = @$filter["x_FdeCant"];
		$this->FdeCant->AdvancedSearch->SearchOperator = @$filter["z_FdeCant"];
		$this->FdeCant->AdvancedSearch->SearchCondition = @$filter["v_FdeCant"];
		$this->FdeCant->AdvancedSearch->SearchValue2 = @$filter["y_FdeCant"];
		$this->FdeCant->AdvancedSearch->SearchOperator2 = @$filter["w_FdeCant"];
		$this->FdeCant->AdvancedSearch->Save();

		// Field FdePrec
		$this->FdePrec->AdvancedSearch->SearchValue = @$filter["x_FdePrec"];
		$this->FdePrec->AdvancedSearch->SearchOperator = @$filter["z_FdePrec"];
		$this->FdePrec->AdvancedSearch->SearchCondition = @$filter["v_FdePrec"];
		$this->FdePrec->AdvancedSearch->SearchValue2 = @$filter["y_FdePrec"];
		$this->FdePrec->AdvancedSearch->SearchOperator2 = @$filter["w_FdePrec"];
		$this->FdePrec->AdvancedSearch->Save();

		// Field FdeDesc
		$this->FdeDesc->AdvancedSearch->SearchValue = @$filter["x_FdeDesc"];
		$this->FdeDesc->AdvancedSearch->SearchOperator = @$filter["z_FdeDesc"];
		$this->FdeDesc->AdvancedSearch->SearchCondition = @$filter["v_FdeDesc"];
		$this->FdeDesc->AdvancedSearch->SearchValue2 = @$filter["y_FdeDesc"];
		$this->FdeDesc->AdvancedSearch->SearchOperator2 = @$filter["w_FdeDesc"];
		$this->FdeDesc->AdvancedSearch->Save();

		// Field FdeUsua
		$this->FdeUsua->AdvancedSearch->SearchValue = @$filter["x_FdeUsua"];
		$this->FdeUsua->AdvancedSearch->SearchOperator = @$filter["z_FdeUsua"];
		$this->FdeUsua->AdvancedSearch->SearchCondition = @$filter["v_FdeUsua"];
		$this->FdeUsua->AdvancedSearch->SearchValue2 = @$filter["y_FdeUsua"];
		$this->FdeUsua->AdvancedSearch->SearchOperator2 = @$filter["w_FdeUsua"];
		$this->FdeUsua->AdvancedSearch->Save();

		// Field FdeFCre
		$this->FdeFCre->AdvancedSearch->SearchValue = @$filter["x_FdeFCre"];
		$this->FdeFCre->AdvancedSearch->SearchOperator = @$filter["z_FdeFCre"];
		$this->FdeFCre->AdvancedSearch->SearchCondition = @$filter["v_FdeFCre"];
		$this->FdeFCre->AdvancedSearch->SearchValue2 = @$filter["y_FdeFCre"];
		$this->FdeFCre->AdvancedSearch->SearchOperator2 = @$filter["w_FdeFCre"];
		$this->FdeFCre->AdvancedSearch->Save();
		$this->BasicSearch->setKeyword(@$filter[EW_TABLE_BASIC_SEARCH]);
		$this->BasicSearch->setType(@$filter[EW_TABLE_BASIC_SEARCH_TYPE]);
	}

	// Return basic search SQL
	function BasicSearchSQL($arKeywords, $type) {
		$sWhere = "";
		$this->BuildBasicSearchSQL($sWhere, $this->FdeDesc, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->FdeUsua, $arKeywords, $type);
		return $sWhere;
	}

	// Build basic search SQL
	function BuildBasicSearchSql(&$Where, &$Fld, $arKeywords, $type) {
		$sDefCond = ($type == "OR") ? "OR" : "AND";
		$arSQL = array(); // Array for SQL parts
		$arCond = array(); // Array for search conditions
		$cnt = count($arKeywords);
		$j = 0; // Number of SQL parts
		for ($i = 0; $i < $cnt; $i++) {
			$Keyword = $arKeywords[$i];
			$Keyword = trim($Keyword);
			if (EW_BASIC_SEARCH_IGNORE_PATTERN <> "") {
				$Keyword = preg_replace(EW_BASIC_SEARCH_IGNORE_PATTERN, "\\", $Keyword);
				$ar = explode("\\", $Keyword);
			} else {
				$ar = array($Keyword);
			}
			foreach ($ar as $Keyword) {
				if ($Keyword <> "") {
					$sWrk = "";
					if ($Keyword == "OR" && $type == "") {
						if ($j > 0)
							$arCond[$j-1] = "OR";
					} elseif ($Keyword == EW_NULL_VALUE) {
						$sWrk = $Fld->FldExpression . " IS NULL";
					} elseif ($Keyword == EW_NOT_NULL_VALUE) {
						$sWrk = $Fld->FldExpression . " IS NOT NULL";
					} elseif ($Fld->FldIsVirtual && $Fld->FldVirtualSearch) {
						$sWrk = $Fld->FldVirtualExpression . ew_Like(ew_QuotedValue("%" . $Keyword . "%", EW_DATATYPE_STRING, $this->DBID), $this->DBID);
					} elseif ($Fld->FldDataType != EW_DATATYPE_NUMBER || is_numeric($Keyword)) {
						$sWrk = $Fld->FldBasicSearchExpression . ew_Like(ew_QuotedValue("%" . $Keyword . "%", EW_DATATYPE_STRING, $this->DBID), $this->DBID);
					}
					if ($sWrk <> "") {
						$arSQL[$j] = $sWrk;
						$arCond[$j] = $sDefCond;
						$j += 1;
					}
				}
			}
		}
		$cnt = count($arSQL);
		$bQuoted = FALSE;
		$sSql = "";
		if ($cnt > 0) {
			for ($i = 0; $i < $cnt-1; $i++) {
				if ($arCond[$i] == "OR") {
					if (!$bQuoted) $sSql .= "(";
					$bQuoted = TRUE;
				}
				$sSql .= $arSQL[$i];
				if ($bQuoted && $arCond[$i] <> "OR") {
					$sSql .= ")";
					$bQuoted = FALSE;
				}
				$sSql .= " " . $arCond[$i] . " ";
			}
			$sSql .= $arSQL[$cnt-1];
			if ($bQuoted)
				$sSql .= ")";
		}
		if ($sSql <> "") {
			if ($Where <> "") $Where .= " OR ";
			$Where .=  "(" . $sSql . ")";
		}
	}

	// Return basic search WHERE clause based on search keyword and type
	function BasicSearchWhere($Default = FALSE) {
		global $Security;
		$sSearchStr = "";
		if (!$Security->CanSearch()) return "";
		$sSearchKeyword = ($Default) ? $this->BasicSearch->KeywordDefault : $this->BasicSearch->Keyword;
		$sSearchType = ($Default) ? $this->BasicSearch->TypeDefault : $this->BasicSearch->Type;
		if ($sSearchKeyword <> "") {
			$sSearch = trim($sSearchKeyword);
			if ($sSearchType <> "=") {
				$ar = array();

				// Match quoted keywords (i.e.: "...")
				if (preg_match_all('/"([^"]*)"/i', $sSearch, $matches, PREG_SET_ORDER)) {
					foreach ($matches as $match) {
						$p = strpos($sSearch, $match[0]);
						$str = substr($sSearch, 0, $p);
						$sSearch = substr($sSearch, $p + strlen($match[0]));
						if (strlen(trim($str)) > 0)
							$ar = array_merge($ar, explode(" ", trim($str)));
						$ar[] = $match[1]; // Save quoted keyword
					}
				}

				// Match individual keywords
				if (strlen(trim($sSearch)) > 0)
					$ar = array_merge($ar, explode(" ", trim($sSearch)));

				// Search keyword in any fields
				if (($sSearchType == "OR" || $sSearchType == "AND") && $this->BasicSearch->BasicSearchAnyFields) {
					foreach ($ar as $sKeyword) {
						if ($sKeyword <> "") {
							if ($sSearchStr <> "") $sSearchStr .= " " . $sSearchType . " ";
							$sSearchStr .= "(" . $this->BasicSearchSQL(array($sKeyword), $sSearchType) . ")";
						}
					}
				} else {
					$sSearchStr = $this->BasicSearchSQL($ar, $sSearchType);
				}
			} else {
				$sSearchStr = $this->BasicSearchSQL(array($sSearch), $sSearchType);
			}
			if (!$Default) $this->Command = "search";
		}
		if (!$Default && $this->Command == "search") {
			$this->BasicSearch->setKeyword($sSearchKeyword);
			$this->BasicSearch->setType($sSearchType);
		}
		return $sSearchStr;
	}

	// Check if search parm exists
	function CheckSearchParms() {

		// Check basic search
		if ($this->BasicSearch->IssetSession())
			return TRUE;
		return FALSE;
	}

	// Clear all search parameters
	function ResetSearchParms() {

		// Clear search WHERE clause
		$this->SearchWhere = "";
		$this->setSearchWhere($this->SearchWhere);

		// Clear basic search parameters
		$this->ResetBasicSearchParms();
	}

	// Load advanced search default values
	function LoadAdvancedSearchDefault() {
		return FALSE;
	}

	// Clear all basic search parameters
	function ResetBasicSearchParms() {
		$this->BasicSearch->UnsetSession();
	}

	// Restore all search parameters
	function RestoreSearchParms() {
		$this->RestoreSearch = TRUE;

		// Restore basic search values
		$this->BasicSearch->Load();
	}

	// Set up sort parameters
	function SetUpSortOrder() {

		// Check for Ctrl pressed
		$bCtrl = (@$_GET["ctrl"] <> "");

		// Check for "order" parameter
		if (@$_GET["order"] <> "") {
			$this->CurrentOrder = ew_StripSlashes(@$_GET["order"]);
			$this->CurrentOrderType = @$_GET["ordertype"];
			$this->UpdateSort($this->FdeCodi, $bCtrl); // FdeCodi
			$this->UpdateSort($this->FcaCodi, $bCtrl); // FcaCodi
			$this->UpdateSort($this->FdeCant, $bCtrl); // FdeCant
			$this->UpdateSort($this->FdePrec, $bCtrl); // FdePrec
			$this->UpdateSort($this->FdeDesc, $bCtrl); // FdeDesc
			$this->UpdateSort($this->FdeUsua, $bCtrl); // FdeUsua
			$this->UpdateSort($this->FdeFCre, $bCtrl); // FdeFCre
			$this->setStartRecordNumber(1); // Reset start position
		}
	}

	// Load sort order parameters
	function LoadSortOrder() {
		$sOrderBy = $this->getSessionOrderBy(); // Get ORDER BY from Session
		if ($sOrderBy == "") {
			if ($this->getSqlOrderBy() <> "") {
				$sOrderBy = $this->getSqlOrderBy();
				$this->setSessionOrderBy($sOrderBy);
			}
		}
	}

	// Reset command
	// - cmd=reset (Reset search parameters)
	// - cmd=resetall (Reset search and master/detail parameters)
	// - cmd=resetsort (Reset sort parameters)
	function ResetCmd() {

		// Check if reset command
		if (substr($this->Command,0,5) == "reset") {

			// Reset search criteria
			if ($this->Command == "reset" || $this->Command == "resetall")
				$this->ResetSearchParms();

			// Reset sorting order
			if ($this->Command == "resetsort") {
				$sOrderBy = "";
				$this->setSessionOrderBy($sOrderBy);
				$this->FdeCodi->setSort("");
				$this->FcaCodi->setSort("");
				$this->FdeCant->setSort("");
				$this->FdePrec->setSort("");
				$this->FdeDesc->setSort("");
				$this->FdeUsua->setSort("");
				$this->FdeFCre->setSort("");
			}

			// Reset start position
			$this->StartRec = 1;
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Set up list options
	function SetupListOptions() {
		global $Security, $Language;

		// "griddelete"
		if ($this->AllowAddDeleteRow) {
			$item = &$this->ListOptions->Add("griddelete");
			$item->CssStyle = "white-space: nowrap;";
			$item->OnLeft = FALSE;
			$item->Visible = FALSE; // Default hidden
		}

		// Add group option item
		$item = &$this->ListOptions->Add($this->ListOptions->GroupOptionName);
		$item->Body = "";
		$item->OnLeft = FALSE;
		$item->Visible = FALSE;

		// "view"
		$item = &$this->ListOptions->Add("view");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->CanView();
		$item->OnLeft = FALSE;

		// "edit"
		$item = &$this->ListOptions->Add("edit");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->CanEdit();
		$item->OnLeft = FALSE;

		// "copy"
		$item = &$this->ListOptions->Add("copy");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->CanAdd();
		$item->OnLeft = FALSE;

		// "delete"
		$item = &$this->ListOptions->Add("delete");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->CanDelete();
		$item->OnLeft = FALSE;

		// List actions
		$item = &$this->ListOptions->Add("listactions");
		$item->CssStyle = "white-space: nowrap;";
		$item->OnLeft = FALSE;
		$item->Visible = FALSE;
		$item->ShowInButtonGroup = FALSE;
		$item->ShowInDropDown = FALSE;

		// "checkbox"
		$item = &$this->ListOptions->Add("checkbox");
		$item->Visible = FALSE;
		$item->OnLeft = FALSE;
		$item->Header = "<input type=\"checkbox\" name=\"key\" id=\"key\" onclick=\"ew_SelectAllKey(this);\">";
		$item->ShowInDropDown = FALSE;
		$item->ShowInButtonGroup = FALSE;

		// Drop down button for ListOptions
		$this->ListOptions->UseImageAndText = TRUE;
		$this->ListOptions->UseDropDownButton = FALSE;
		$this->ListOptions->DropDownButtonPhrase = $Language->Phrase("ButtonListOptions");
		$this->ListOptions->UseButtonGroup = FALSE;
		if ($this->ListOptions->UseButtonGroup && ew_IsMobile())
			$this->ListOptions->UseDropDownButton = TRUE;
		$this->ListOptions->ButtonClass = "btn-sm"; // Class for button group

		// Call ListOptions_Load event
		$this->ListOptions_Load();
		$this->SetupListOptionsExt();
		$item = &$this->ListOptions->GetItem($this->ListOptions->GroupOptionName);
		$item->Visible = $this->ListOptions->GroupOptionVisible();
	}

	// Render list options
	function RenderListOptions() {
		global $Security, $Language, $objForm;
		$this->ListOptions->LoadDefault();

		// Set up row action and key
		if (is_numeric($this->RowIndex) && $this->CurrentMode <> "view") {
			$objForm->Index = $this->RowIndex;
			$ActionName = str_replace("k_", "k" . $this->RowIndex . "_", $this->FormActionName);
			$OldKeyName = str_replace("k_", "k" . $this->RowIndex . "_", $this->FormOldKeyName);
			$KeyName = str_replace("k_", "k" . $this->RowIndex . "_", $this->FormKeyName);
			$BlankRowName = str_replace("k_", "k" . $this->RowIndex . "_", $this->FormBlankRowName);
			if ($this->RowAction <> "")
				$this->MultiSelectKey .= "<input type=\"hidden\" name=\"" . $ActionName . "\" id=\"" . $ActionName . "\" value=\"" . $this->RowAction . "\">";
			if ($this->RowAction == "delete") {
				$rowkey = $objForm->GetValue($this->FormKeyName);
				$this->SetupKeyValues($rowkey);
			}
			if ($this->RowAction == "insert" && $this->CurrentAction == "F" && $this->EmptyRow())
				$this->MultiSelectKey .= "<input type=\"hidden\" name=\"" . $BlankRowName . "\" id=\"" . $BlankRowName . "\" value=\"1\">";
		}

		// "delete"
		if ($this->AllowAddDeleteRow) {
			if ($this->CurrentAction == "gridadd" || $this->CurrentAction == "gridedit") {
				$option = &$this->ListOptions;
				$option->UseButtonGroup = TRUE; // Use button group for grid delete button
				$option->UseImageAndText = TRUE; // Use image and text for grid delete button
				$oListOpt = &$option->Items["griddelete"];
				if (!$Security->CanDelete() && is_numeric($this->RowIndex) && ($this->RowAction == "" || $this->RowAction == "edit")) { // Do not allow delete existing record
					$oListOpt->Body = "&nbsp;";
				} else {
					$oListOpt->Body = "<a class=\"ewGridLink ewGridDelete\" title=\"" . ew_HtmlTitle($Language->Phrase("DeleteLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("DeleteLink")) . "\" onclick=\"return ew_DeleteGridRow(this, " . $this->RowIndex . ");\">" . $Language->Phrase("DeleteLink") . "</a>";
				}
			}
		}

		// "view"
		$oListOpt = &$this->ListOptions->Items["view"];
		if ($Security->CanView())
			$oListOpt->Body = "<a class=\"ewRowLink ewView\" title=\"" . ew_HtmlTitle($Language->Phrase("ViewLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("ViewLink")) . "\" href=\"" . ew_HtmlEncode($this->ViewUrl) . "\">" . $Language->Phrase("ViewLink") . "</a>";
		else
			$oListOpt->Body = "";

		// "edit"
		$oListOpt = &$this->ListOptions->Items["edit"];
		if ($Security->CanEdit()) {
			$oListOpt->Body = "<a class=\"ewRowLink ewEdit\" title=\"" . ew_HtmlTitle($Language->Phrase("EditLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("EditLink")) . "\" href=\"" . ew_HtmlEncode($this->EditUrl) . "\">" . $Language->Phrase("EditLink") . "</a>";
		} else {
			$oListOpt->Body = "";
		}

		// "copy"
		$oListOpt = &$this->ListOptions->Items["copy"];
		if ($Security->CanAdd()) {
			$oListOpt->Body = "<a class=\"ewRowLink ewCopy\" title=\"" . ew_HtmlTitle($Language->Phrase("CopyLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("CopyLink")) . "\" href=\"" . ew_HtmlEncode($this->CopyUrl) . "\">" . $Language->Phrase("CopyLink") . "</a>";
		} else {
			$oListOpt->Body = "";
		}

		// "delete"
		$oListOpt = &$this->ListOptions->Items["delete"];
		if ($Security->CanDelete())
			$oListOpt->Body = "<a class=\"ewRowLink ewDelete\"" . "" . " title=\"" . ew_HtmlTitle($Language->Phrase("DeleteLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("DeleteLink")) . "\" href=\"" . ew_HtmlEncode($this->DeleteUrl) . "\">" . $Language->Phrase("DeleteLink") . "</a>";
		else
			$oListOpt->Body = "";

		// Set up list action buttons
		$oListOpt = &$this->ListOptions->GetItem("listactions");
		if ($oListOpt) {
			$body = "";
			$links = array();
			foreach ($this->ListActions->Items as $listaction) {
				if ($listaction->Select == EW_ACTION_SINGLE && $listaction->Allow) {
					$action = $listaction->Action;
					$caption = $listaction->Caption;
					$icon = ($listaction->Icon <> "") ? "<span class=\"" . ew_HtmlEncode(str_replace(" ewIcon", "", $listaction->Icon)) . "\" data-caption=\"" . ew_HtmlTitle($caption) . "\"></span> " : "";
					$links[] = "<li><a class=\"ewAction ewListAction\" data-action=\"" . ew_HtmlEncode($action) . "\" data-caption=\"" . ew_HtmlTitle($caption) . "\" href=\"\" onclick=\"ew_SubmitAction(event,jQuery.extend({key:" . $this->KeyToJson() . "}," . $listaction->ToJson(TRUE) . "));return false;\">" . $icon . $listaction->Caption . "</a></li>";
					if (count($links) == 1) // Single button
						$body = "<a class=\"ewAction ewListAction\" data-action=\"" . ew_HtmlEncode($action) . "\" title=\"" . ew_HtmlTitle($caption) . "\" data-caption=\"" . ew_HtmlTitle($caption) . "\" href=\"\" onclick=\"ew_SubmitAction(event,jQuery.extend({key:" . $this->KeyToJson() . "}," . $listaction->ToJson(TRUE) . "));return false;\">" . $Language->Phrase("ListActionButton") . "</a>";
				}
			}
			if (count($links) > 1) { // More than one buttons, use dropdown
				$body = "<button class=\"dropdown-toggle btn btn-default btn-sm ewActions\" title=\"" . ew_HtmlTitle($Language->Phrase("ListActionButton")) . "\" data-toggle=\"dropdown\">" . $Language->Phrase("ListActionButton") . "<b class=\"caret\"></b></button>";
				$content = "";
				foreach ($links as $link)
					$content .= "<li>" . $link . "</li>";
				$body .= "<ul class=\"dropdown-menu" . ($oListOpt->OnLeft ? "" : " dropdown-menu-right") . "\">". $content . "</ul>";
				$body = "<div class=\"btn-group\">" . $body . "</div>";
			}
			if (count($links) > 0) {
				$oListOpt->Body = $body;
				$oListOpt->Visible = TRUE;
			}
		}

		// "checkbox"
		$oListOpt = &$this->ListOptions->Items["checkbox"];
		$oListOpt->Body = "<input type=\"checkbox\" name=\"key_m[]\" value=\"" . ew_HtmlEncode($this->FdeCodi->CurrentValue . $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"] . $this->FcaCodi->CurrentValue) . "\" onclick='ew_ClickMultiCheckbox(event);'>";
		if ($this->CurrentAction == "gridedit" && is_numeric($this->RowIndex)) {
			$this->MultiSelectKey .= "<input type=\"hidden\" name=\"" . $KeyName . "\" id=\"" . $KeyName . "\" value=\"" . $this->FdeCodi->CurrentValue . $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"] . $this->FcaCodi->CurrentValue . "\">";
		}
		$this->RenderListOptionsExt();

		// Call ListOptions_Rendered event
		$this->ListOptions_Rendered();
	}

	// Set up other options
	function SetupOtherOptions() {
		global $Language, $Security;
		$options = &$this->OtherOptions;
		$option = $options["addedit"];

		// Add
		$item = &$option->Add("add");
		$item->Body = "<a class=\"ewAddEdit ewAdd\" title=\"" . ew_HtmlTitle($Language->Phrase("AddLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("AddLink")) . "\" href=\"" . ew_HtmlEncode($this->AddUrl) . "\">" . $Language->Phrase("AddLink") . "</a>";
		$item->Visible = ($this->AddUrl <> "" && $Security->CanAdd());
		$item = &$option->Add("gridadd");
		$item->Body = "<a class=\"ewAddEdit ewGridAdd\" title=\"" . ew_HtmlTitle($Language->Phrase("GridAddLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("GridAddLink")) . "\" href=\"" . ew_HtmlEncode($this->GridAddUrl) . "\">" . $Language->Phrase("GridAddLink") . "</a>";
		$item->Visible = ($this->GridAddUrl <> "" && $Security->CanAdd());

		// Add grid edit
		$option = $options["addedit"];
		$item = &$option->Add("gridedit");
		$item->Body = "<a class=\"ewAddEdit ewGridEdit\" title=\"" . ew_HtmlTitle($Language->Phrase("GridEditLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("GridEditLink")) . "\" href=\"" . ew_HtmlEncode($this->GridEditUrl) . "\">" . $Language->Phrase("GridEditLink") . "</a>";
		$item->Visible = ($this->GridEditUrl <> "" && $Security->CanEdit());
		$option = $options["action"];

		// Set up options default
		foreach ($options as &$option) {
			$option->UseImageAndText = TRUE;
			$option->UseDropDownButton = FALSE;
			$option->UseButtonGroup = TRUE;
			$option->ButtonClass = "btn-sm"; // Class for button group
			$item = &$option->Add($option->GroupOptionName);
			$item->Body = "";
			$item->Visible = FALSE;
		}
		$options["addedit"]->DropDownButtonPhrase = $Language->Phrase("ButtonAddEdit");
		$options["detail"]->DropDownButtonPhrase = $Language->Phrase("ButtonDetails");
		$options["action"]->DropDownButtonPhrase = $Language->Phrase("ButtonActions");

		// Filter button
		$item = &$this->FilterOptions->Add("savecurrentfilter");
		$item->Body = "<a class=\"ewSaveFilter\" data-form=\"fFDetlistsrch\" href=\"#\">" . $Language->Phrase("SaveCurrentFilter") . "</a>";
		$item->Visible = TRUE;
		$item = &$this->FilterOptions->Add("deletefilter");
		$item->Body = "<a class=\"ewDeleteFilter\" data-form=\"fFDetlistsrch\" href=\"#\">" . $Language->Phrase("DeleteFilter") . "</a>";
		$item->Visible = TRUE;
		$this->FilterOptions->UseDropDownButton = TRUE;
		$this->FilterOptions->UseButtonGroup = !$this->FilterOptions->UseDropDownButton;
		$this->FilterOptions->DropDownButtonPhrase = $Language->Phrase("Filters");

		// Add group option item
		$item = &$this->FilterOptions->Add($this->FilterOptions->GroupOptionName);
		$item->Body = "";
		$item->Visible = FALSE;
	}

	// Render other options
	function RenderOtherOptions() {
		global $Language, $Security;
		$options = &$this->OtherOptions;
		if ($this->CurrentAction <> "gridadd" && $this->CurrentAction <> "gridedit") { // Not grid add/edit mode
			$option = &$options["action"];

			// Set up list action buttons
			foreach ($this->ListActions->Items as $listaction) {
				if ($listaction->Select == EW_ACTION_MULTIPLE) {
					$item = &$option->Add("custom_" . $listaction->Action);
					$caption = $listaction->Caption;
					$icon = ($listaction->Icon <> "") ? "<span class=\"" . ew_HtmlEncode($listaction->Icon) . "\" data-caption=\"" . ew_HtmlEncode($caption) . "\"></span> " : $caption;
					$item->Body = "<a class=\"ewAction ewListAction\" title=\"" . ew_HtmlEncode($caption) . "\" data-caption=\"" . ew_HtmlEncode($caption) . "\" href=\"\" onclick=\"ew_SubmitAction(event,jQuery.extend({f:document.fFDetlist}," . $listaction->ToJson(TRUE) . "));return false;\">" . $icon . "</a>";
					$item->Visible = $listaction->Allow;
				}
			}

			// Hide grid edit and other options
			if ($this->TotalRecs <= 0) {
				$option = &$options["addedit"];
				$item = &$option->GetItem("gridedit");
				if ($item) $item->Visible = FALSE;
				$option = &$options["action"];
				$option->HideAllOptions();
			}
		} else { // Grid add/edit mode

			// Hide all options first
			foreach ($options as &$option)
				$option->HideAllOptions();
			if ($this->CurrentAction == "gridadd") {
				if ($this->AllowAddDeleteRow) {

					// Add add blank row
					$option = &$options["addedit"];
					$option->UseDropDownButton = FALSE;
					$option->UseImageAndText = TRUE;
					$item = &$option->Add("addblankrow");
					$item->Body = "<a class=\"ewAddEdit ewAddBlankRow\" title=\"" . ew_HtmlTitle($Language->Phrase("AddBlankRow")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("AddBlankRow")) . "\" href=\"javascript:void(0);\" onclick=\"ew_AddGridRow(this);\">" . $Language->Phrase("AddBlankRow") . "</a>";
					$item->Visible = $Security->CanAdd();
				}
				$option = &$options["action"];
				$option->UseDropDownButton = FALSE;
				$option->UseImageAndText = TRUE;

				// Add grid insert
				$item = &$option->Add("gridinsert");
				$item->Body = "<a class=\"ewAction ewGridInsert\" title=\"" . ew_HtmlTitle($Language->Phrase("GridInsertLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("GridInsertLink")) . "\" href=\"\" onclick=\"return ewForms(this).Submit('" . $this->AddMasterUrl($this->PageName()) . "');\">" . $Language->Phrase("GridInsertLink") . "</a>";

				// Add grid cancel
				$item = &$option->Add("gridcancel");
				$cancelurl = $this->AddMasterUrl($this->PageUrl() . "a=cancel");
				$item->Body = "<a class=\"ewAction ewGridCancel\" title=\"" . ew_HtmlTitle($Language->Phrase("GridCancelLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("GridCancelLink")) . "\" href=\"" . $cancelurl . "\">" . $Language->Phrase("GridCancelLink") . "</a>";
			}
			if ($this->CurrentAction == "gridedit") {
				if ($this->AllowAddDeleteRow) {

					// Add add blank row
					$option = &$options["addedit"];
					$option->UseDropDownButton = FALSE;
					$option->UseImageAndText = TRUE;
					$item = &$option->Add("addblankrow");
					$item->Body = "<a class=\"ewAddEdit ewAddBlankRow\" title=\"" . ew_HtmlTitle($Language->Phrase("AddBlankRow")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("AddBlankRow")) . "\" href=\"javascript:void(0);\" onclick=\"ew_AddGridRow(this);\">" . $Language->Phrase("AddBlankRow") . "</a>";
					$item->Visible = $Security->CanAdd();
				}
				$option = &$options["action"];
				$option->UseDropDownButton = FALSE;
				$option->UseImageAndText = TRUE;
					$item = &$option->Add("gridsave");
					$item->Body = "<a class=\"ewAction ewGridSave\" title=\"" . ew_HtmlTitle($Language->Phrase("GridSaveLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("GridSaveLink")) . "\" href=\"\" onclick=\"return ewForms(this).Submit('" . $this->AddMasterUrl($this->PageName()) . "');\">" . $Language->Phrase("GridSaveLink") . "</a>";
					$item = &$option->Add("gridcancel");
					$cancelurl = $this->AddMasterUrl($this->PageUrl() . "a=cancel");
					$item->Body = "<a class=\"ewAction ewGridCancel\" title=\"" . ew_HtmlTitle($Language->Phrase("GridCancelLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("GridCancelLink")) . "\" href=\"" . $cancelurl . "\">" . $Language->Phrase("GridCancelLink") . "</a>";
			}
		}
	}

	// Process list action
	function ProcessListAction() {
		global $Language, $Security;
		$userlist = "";
		$user = "";
		$sFilter = $this->GetKeyFilter();
		$UserAction = @$_POST["useraction"];
		if ($sFilter <> "" && $UserAction <> "") {

			// Check permission first
			$ActionCaption = $UserAction;
			if (array_key_exists($UserAction, $this->ListActions->Items)) {
				$ActionCaption = $this->ListActions->Items[$UserAction]->Caption;
				if (!$this->ListActions->Items[$UserAction]->Allow) {
					$errmsg = str_replace('%s', $ActionCaption, $Language->Phrase("CustomActionNotAllowed"));
					if (@$_POST["ajax"] == $UserAction) // Ajax
						echo "<p class=\"text-danger\">" . $errmsg . "</p>";
					else
						$this->setFailureMessage($errmsg);
					return FALSE;
				}
			}
			$this->CurrentFilter = $sFilter;
			$sSql = $this->SQL();
			$conn = &$this->Connection();
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			$rs = $conn->Execute($sSql);
			$conn->raiseErrorFn = '';
			$this->CurrentAction = $UserAction;

			// Call row action event
			if ($rs && !$rs->EOF) {
				$conn->BeginTrans();
				$this->SelectedCount = $rs->RecordCount();
				$this->SelectedIndex = 0;
				while (!$rs->EOF) {
					$this->SelectedIndex++;
					$row = $rs->fields;
					$Processed = $this->Row_CustomAction($UserAction, $row);
					if (!$Processed) break;
					$rs->MoveNext();
				}
				if ($Processed) {
					$conn->CommitTrans(); // Commit the changes
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage(str_replace('%s', $ActionCaption, $Language->Phrase("CustomActionCompleted"))); // Set up success message
				} else {
					$conn->RollbackTrans(); // Rollback changes

					// Set up error message
					if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

						// Use the message, do nothing
					} elseif ($this->CancelMessage <> "") {
						$this->setFailureMessage($this->CancelMessage);
						$this->CancelMessage = "";
					} else {
						$this->setFailureMessage(str_replace('%s', $ActionCaption, $Language->Phrase("CustomActionFailed")));
					}
				}
			}
			if ($rs)
				$rs->Close();
			if (@$_POST["ajax"] == $UserAction) { // Ajax
				if ($this->getSuccessMessage() <> "") {
					echo "<p class=\"text-success\">" . $this->getSuccessMessage() . "</p>";
					$this->ClearSuccessMessage(); // Clear message
				}
				if ($this->getFailureMessage() <> "") {
					echo "<p class=\"text-danger\">" . $this->getFailureMessage() . "</p>";
					$this->ClearFailureMessage(); // Clear message
				}
				return TRUE;
			}
		}
		return FALSE; // Not ajax request
	}

	// Set up search options
	function SetupSearchOptions() {
		global $Language;
		$this->SearchOptions = new cListOptions();
		$this->SearchOptions->Tag = "div";
		$this->SearchOptions->TagClassName = "ewSearchOption";

		// Search button
		$item = &$this->SearchOptions->Add("searchtoggle");
		$SearchToggleClass = ($this->SearchWhere <> "") ? " active" : " active";
		$item->Body = "<button type=\"button\" class=\"btn btn-default ewSearchToggle" . $SearchToggleClass . "\" title=\"" . $Language->Phrase("SearchPanel") . "\" data-caption=\"" . $Language->Phrase("SearchPanel") . "\" data-toggle=\"button\" data-form=\"fFDetlistsrch\">" . $Language->Phrase("SearchBtn") . "</button>";
		$item->Visible = TRUE;

		// Show all button
		$item = &$this->SearchOptions->Add("showall");
		$item->Body = "<a class=\"btn btn-default ewShowAll\" title=\"" . $Language->Phrase("ShowAll") . "\" data-caption=\"" . $Language->Phrase("ShowAll") . "\" href=\"" . $this->PageUrl() . "cmd=reset\">" . $Language->Phrase("ShowAllBtn") . "</a>";
		$item->Visible = ($this->SearchWhere <> $this->DefaultSearchWhere && $this->SearchWhere <> "0=101");

		// Button group for search
		$this->SearchOptions->UseDropDownButton = FALSE;
		$this->SearchOptions->UseImageAndText = TRUE;
		$this->SearchOptions->UseButtonGroup = TRUE;
		$this->SearchOptions->DropDownButtonPhrase = $Language->Phrase("ButtonSearch");

		// Add group option item
		$item = &$this->SearchOptions->Add($this->SearchOptions->GroupOptionName);
		$item->Body = "";
		$item->Visible = FALSE;

		// Hide search options
		if ($this->Export <> "" || $this->CurrentAction <> "")
			$this->SearchOptions->HideAllOptions();
		global $Security;
		if (!$Security->CanSearch()) {
			$this->SearchOptions->HideAllOptions();
			$this->FilterOptions->HideAllOptions();
		}
	}

	function SetupListOptionsExt() {
		global $Security, $Language;
	}

	function RenderListOptionsExt() {
		global $Security, $Language;
	}

	// Set up starting record parameters
	function SetUpStartRec() {
		if ($this->DisplayRecs == 0)
			return;
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET[EW_TABLE_START_REC] <> "") { // Check for "start" parameter
				$this->StartRec = $_GET[EW_TABLE_START_REC];
				$this->setStartRecordNumber($this->StartRec);
			} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
				$PageNo = $_GET[EW_TABLE_PAGE_NO];
				if (is_numeric($PageNo)) {
					$this->StartRec = ($PageNo-1)*$this->DisplayRecs+1;
					if ($this->StartRec <= 0) {
						$this->StartRec = 1;
					} elseif ($this->StartRec >= intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1) {
						$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1;
					}
					$this->setStartRecordNumber($this->StartRec);
				}
			}
		}
		$this->StartRec = $this->getStartRecordNumber();

		// Check if correct start record counter
		if (!is_numeric($this->StartRec) || $this->StartRec == "") { // Avoid invalid start record counter
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} elseif (intval($this->StartRec) > intval($this->TotalRecs)) { // Avoid starting record > total records
			$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to last page first record
			$this->setStartRecordNumber($this->StartRec);
		} elseif (($this->StartRec-1) % $this->DisplayRecs <> 0) {
			$this->StartRec = intval(($this->StartRec-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to page boundary
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Load default values
	function LoadDefaultValues() {
		$this->FdeCodi->CurrentValue = NULL;
		$this->FdeCodi->OldValue = $this->FdeCodi->CurrentValue;
		$this->FcaCodi->CurrentValue = NULL;
		$this->FcaCodi->OldValue = $this->FcaCodi->CurrentValue;
		$this->FdeCant->CurrentValue = NULL;
		$this->FdeCant->OldValue = $this->FdeCant->CurrentValue;
		$this->FdePrec->CurrentValue = NULL;
		$this->FdePrec->OldValue = $this->FdePrec->CurrentValue;
		$this->FdeDesc->CurrentValue = NULL;
		$this->FdeDesc->OldValue = $this->FdeDesc->CurrentValue;
		$this->FdeUsua->CurrentValue = NULL;
		$this->FdeUsua->OldValue = $this->FdeUsua->CurrentValue;
		$this->FdeFCre->CurrentValue = NULL;
		$this->FdeFCre->OldValue = $this->FdeFCre->CurrentValue;
	}

	// Load basic search values
	function LoadBasicSearchValues() {
		$this->BasicSearch->Keyword = @$_GET[EW_TABLE_BASIC_SEARCH];
		if ($this->BasicSearch->Keyword <> "") $this->Command = "search";
		$this->BasicSearch->Type = @$_GET[EW_TABLE_BASIC_SEARCH_TYPE];
	}

	// Load form values
	function LoadFormValues() {

		// Load from form
		global $objForm;
		if (!$this->FdeCodi->FldIsDetailKey) {
			$this->FdeCodi->setFormValue($objForm->GetValue("x_FdeCodi"));
		}
		$this->FdeCodi->setOldValue($objForm->GetValue("o_FdeCodi"));
		if (!$this->FcaCodi->FldIsDetailKey) {
			$this->FcaCodi->setFormValue($objForm->GetValue("x_FcaCodi"));
		}
		$this->FcaCodi->setOldValue($objForm->GetValue("o_FcaCodi"));
		if (!$this->FdeCant->FldIsDetailKey) {
			$this->FdeCant->setFormValue($objForm->GetValue("x_FdeCant"));
		}
		$this->FdeCant->setOldValue($objForm->GetValue("o_FdeCant"));
		if (!$this->FdePrec->FldIsDetailKey) {
			$this->FdePrec->setFormValue($objForm->GetValue("x_FdePrec"));
		}
		$this->FdePrec->setOldValue($objForm->GetValue("o_FdePrec"));
		if (!$this->FdeDesc->FldIsDetailKey) {
			$this->FdeDesc->setFormValue($objForm->GetValue("x_FdeDesc"));
		}
		$this->FdeDesc->setOldValue($objForm->GetValue("o_FdeDesc"));
		if (!$this->FdeUsua->FldIsDetailKey) {
			$this->FdeUsua->setFormValue($objForm->GetValue("x_FdeUsua"));
		}
		$this->FdeUsua->setOldValue($objForm->GetValue("o_FdeUsua"));
		if (!$this->FdeFCre->FldIsDetailKey) {
			$this->FdeFCre->setFormValue($objForm->GetValue("x_FdeFCre"));
			$this->FdeFCre->CurrentValue = ew_UnFormatDateTime($this->FdeFCre->CurrentValue, 7);
		}
		$this->FdeFCre->setOldValue($objForm->GetValue("o_FdeFCre"));
	}

	// Restore form values
	function RestoreFormValues() {
		global $objForm;
		$this->FdeCodi->CurrentValue = $this->FdeCodi->FormValue;
		$this->FcaCodi->CurrentValue = $this->FcaCodi->FormValue;
		$this->FdeCant->CurrentValue = $this->FdeCant->FormValue;
		$this->FdePrec->CurrentValue = $this->FdePrec->FormValue;
		$this->FdeDesc->CurrentValue = $this->FdeDesc->FormValue;
		$this->FdeUsua->CurrentValue = $this->FdeUsua->FormValue;
		$this->FdeFCre->CurrentValue = $this->FdeFCre->FormValue;
		$this->FdeFCre->CurrentValue = ew_UnFormatDateTime($this->FdeFCre->CurrentValue, 7);
	}

	// Load recordset
	function LoadRecordset($offset = -1, $rowcnt = -1) {

		// Load List page SQL
		$sSql = $this->SelectSQL();
		$conn = &$this->Connection();

		// Load recordset
		$dbtype = ew_GetConnectionType($this->DBID);
		if ($this->UseSelectLimit) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			if ($dbtype == "MSSQL") {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset, array("_hasOrderBy" => trim($this->getOrderBy()) || trim($this->getSessionOrderBy())));
			} else {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset);
			}
			$conn->raiseErrorFn = '';
		} else {
			$rs = ew_LoadRecordset($sSql, $conn);
		}

		// Call Recordset Selected event
		$this->Recordset_Selected($rs);
		return $rs;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->FdeCodi->setDbValue($rs->fields('FdeCodi'));
		$this->FcaCodi->setDbValue($rs->fields('FcaCodi'));
		$this->FdeCant->setDbValue($rs->fields('FdeCant'));
		$this->FdePrec->setDbValue($rs->fields('FdePrec'));
		$this->FdeDesc->setDbValue($rs->fields('FdeDesc'));
		$this->FdeUsua->setDbValue($rs->fields('FdeUsua'));
		$this->FdeFCre->setDbValue($rs->fields('FdeFCre'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->FdeCodi->DbValue = $row['FdeCodi'];
		$this->FcaCodi->DbValue = $row['FcaCodi'];
		$this->FdeCant->DbValue = $row['FdeCant'];
		$this->FdePrec->DbValue = $row['FdePrec'];
		$this->FdeDesc->DbValue = $row['FdeDesc'];
		$this->FdeUsua->DbValue = $row['FdeUsua'];
		$this->FdeFCre->DbValue = $row['FdeFCre'];
	}

	// Load old record
	function LoadOldRecord() {

		// Load key values from Session
		$bValidKey = TRUE;
		if (strval($this->getKey("FdeCodi")) <> "")
			$this->FdeCodi->CurrentValue = $this->getKey("FdeCodi"); // FdeCodi
		else
			$bValidKey = FALSE;
		if (strval($this->getKey("FcaCodi")) <> "")
			$this->FcaCodi->CurrentValue = $this->getKey("FcaCodi"); // FcaCodi
		else
			$bValidKey = FALSE;

		// Load old recordset
		if ($bValidKey) {
			$this->CurrentFilter = $this->KeyFilter();
			$sSql = $this->SQL();
			$conn = &$this->Connection();
			$this->OldRecordset = ew_LoadRecordset($sSql, $conn);
			$this->LoadRowValues($this->OldRecordset); // Load row values
		} else {
			$this->OldRecordset = NULL;
		}
		return $bValidKey;
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		$this->ViewUrl = $this->GetViewUrl();
		$this->EditUrl = $this->GetEditUrl();
		$this->InlineEditUrl = $this->GetInlineEditUrl();
		$this->CopyUrl = $this->GetCopyUrl();
		$this->InlineCopyUrl = $this->GetInlineCopyUrl();
		$this->DeleteUrl = $this->GetDeleteUrl();

		// Convert decimal values if posted back
		if ($this->FdePrec->FormValue == $this->FdePrec->CurrentValue && is_numeric(ew_StrToFloat($this->FdePrec->CurrentValue)))
			$this->FdePrec->CurrentValue = ew_StrToFloat($this->FdePrec->CurrentValue);

		// Call Row_Rendering event
		$this->Row_Rendering();

		// Common render codes for all row types
		// FdeCodi
		// FcaCodi
		// FdeCant
		// FdePrec
		// FdeDesc
		// FdeUsua
		// FdeFCre

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// FdeCodi
		$this->FdeCodi->ViewValue = $this->FdeCodi->CurrentValue;
		$this->FdeCodi->ViewCustomAttributes = "";

		// FcaCodi
		if (strval($this->FcaCodi->CurrentValue) <> "") {
			$sFilterWrk = "\"FcaCodi\"" . ew_SearchString("=", $this->FcaCodi->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT \"FcaCodi\", \"FcaCodi\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"FCab\"";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->FcaCodi, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->FcaCodi->ViewValue = $this->FcaCodi->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->FcaCodi->ViewValue = $this->FcaCodi->CurrentValue;
			}
		} else {
			$this->FcaCodi->ViewValue = NULL;
		}
		$this->FcaCodi->ViewCustomAttributes = "";

		// FdeCant
		$this->FdeCant->ViewValue = $this->FdeCant->CurrentValue;
		$this->FdeCant->ViewCustomAttributes = "";

		// FdePrec
		$this->FdePrec->ViewValue = $this->FdePrec->CurrentValue;
		$this->FdePrec->ViewCustomAttributes = "";

		// FdeDesc
		$this->FdeDesc->ViewValue = $this->FdeDesc->CurrentValue;
		$this->FdeDesc->ViewCustomAttributes = "";

		// FdeUsua
		$this->FdeUsua->ViewValue = $this->FdeUsua->CurrentValue;
		$this->FdeUsua->ViewCustomAttributes = "";

		// FdeFCre
		$this->FdeFCre->ViewValue = $this->FdeFCre->CurrentValue;
		$this->FdeFCre->ViewValue = ew_FormatDateTime($this->FdeFCre->ViewValue, 7);
		$this->FdeFCre->ViewCustomAttributes = "";

			// FdeCodi
			$this->FdeCodi->LinkCustomAttributes = "";
			$this->FdeCodi->HrefValue = "";
			$this->FdeCodi->TooltipValue = "";

			// FcaCodi
			$this->FcaCodi->LinkCustomAttributes = "";
			$this->FcaCodi->HrefValue = "";
			$this->FcaCodi->TooltipValue = "";

			// FdeCant
			$this->FdeCant->LinkCustomAttributes = "";
			$this->FdeCant->HrefValue = "";
			$this->FdeCant->TooltipValue = "";

			// FdePrec
			$this->FdePrec->LinkCustomAttributes = "";
			$this->FdePrec->HrefValue = "";
			$this->FdePrec->TooltipValue = "";

			// FdeDesc
			$this->FdeDesc->LinkCustomAttributes = "";
			$this->FdeDesc->HrefValue = "";
			$this->FdeDesc->TooltipValue = "";

			// FdeUsua
			$this->FdeUsua->LinkCustomAttributes = "";
			$this->FdeUsua->HrefValue = "";
			$this->FdeUsua->TooltipValue = "";

			// FdeFCre
			$this->FdeFCre->LinkCustomAttributes = "";
			$this->FdeFCre->HrefValue = "";
			$this->FdeFCre->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_ADD) { // Add row

			// FdeCodi
			$this->FdeCodi->EditAttrs["class"] = "form-control";
			$this->FdeCodi->EditCustomAttributes = "";
			$this->FdeCodi->EditValue = ew_HtmlEncode($this->FdeCodi->CurrentValue);
			$this->FdeCodi->PlaceHolder = ew_RemoveHtml($this->FdeCodi->FldCaption());

			// FcaCodi
			$this->FcaCodi->EditAttrs["class"] = "form-control";
			$this->FcaCodi->EditCustomAttributes = "";
			if (trim(strval($this->FcaCodi->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "\"FcaCodi\"" . ew_SearchString("=", $this->FcaCodi->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT \"FcaCodi\", \"FcaCodi\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\", '' AS \"SelectFilterFld\", '' AS \"SelectFilterFld2\", '' AS \"SelectFilterFld3\", '' AS \"SelectFilterFld4\" FROM \"public\".\"FCab\"";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->FcaCodi, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->FcaCodi->EditValue = $arwrk;

			// FdeCant
			$this->FdeCant->EditAttrs["class"] = "form-control";
			$this->FdeCant->EditCustomAttributes = "";
			$this->FdeCant->EditValue = ew_HtmlEncode($this->FdeCant->CurrentValue);
			$this->FdeCant->PlaceHolder = ew_RemoveHtml($this->FdeCant->FldCaption());

			// FdePrec
			$this->FdePrec->EditAttrs["class"] = "form-control";
			$this->FdePrec->EditCustomAttributes = "";
			$this->FdePrec->EditValue = ew_HtmlEncode($this->FdePrec->CurrentValue);
			$this->FdePrec->PlaceHolder = ew_RemoveHtml($this->FdePrec->FldCaption());
			if (strval($this->FdePrec->EditValue) <> "" && is_numeric($this->FdePrec->EditValue)) {
			$this->FdePrec->EditValue = ew_FormatNumber($this->FdePrec->EditValue, -2, -1, -2, 0);
			$this->FdePrec->OldValue = $this->FdePrec->EditValue;
			}

			// FdeDesc
			$this->FdeDesc->EditAttrs["class"] = "form-control";
			$this->FdeDesc->EditCustomAttributes = "";
			$this->FdeDesc->EditValue = ew_HtmlEncode($this->FdeDesc->CurrentValue);
			$this->FdeDesc->PlaceHolder = ew_RemoveHtml($this->FdeDesc->FldCaption());

			// FdeUsua
			$this->FdeUsua->EditAttrs["class"] = "form-control";
			$this->FdeUsua->EditCustomAttributes = "";
			$this->FdeUsua->EditValue = ew_HtmlEncode($this->FdeUsua->CurrentValue);
			$this->FdeUsua->PlaceHolder = ew_RemoveHtml($this->FdeUsua->FldCaption());

			// FdeFCre
			$this->FdeFCre->EditAttrs["class"] = "form-control";
			$this->FdeFCre->EditCustomAttributes = "";
			$this->FdeFCre->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->FdeFCre->CurrentValue, 7));
			$this->FdeFCre->PlaceHolder = ew_RemoveHtml($this->FdeFCre->FldCaption());

			// Edit refer script
			// FdeCodi

			$this->FdeCodi->HrefValue = "";

			// FcaCodi
			$this->FcaCodi->HrefValue = "";

			// FdeCant
			$this->FdeCant->HrefValue = "";

			// FdePrec
			$this->FdePrec->HrefValue = "";

			// FdeDesc
			$this->FdeDesc->HrefValue = "";

			// FdeUsua
			$this->FdeUsua->HrefValue = "";

			// FdeFCre
			$this->FdeFCre->HrefValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_EDIT) { // Edit row

			// FdeCodi
			$this->FdeCodi->EditAttrs["class"] = "form-control";
			$this->FdeCodi->EditCustomAttributes = "";
			$this->FdeCodi->EditValue = $this->FdeCodi->CurrentValue;
			$this->FdeCodi->ViewCustomAttributes = "";

			// FcaCodi
			$this->FcaCodi->EditAttrs["class"] = "form-control";
			$this->FcaCodi->EditCustomAttributes = "";
			if (strval($this->FcaCodi->CurrentValue) <> "") {
				$sFilterWrk = "\"FcaCodi\"" . ew_SearchString("=", $this->FcaCodi->CurrentValue, EW_DATATYPE_NUMBER, "");
			$sSqlWrk = "SELECT \"FcaCodi\", \"FcaCodi\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"FCab\"";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->FcaCodi, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
				$rswrk = Conn()->Execute($sSqlWrk);
				if ($rswrk && !$rswrk->EOF) { // Lookup values found
					$arwrk = array();
					$arwrk[1] = $rswrk->fields('DispFld');
					$this->FcaCodi->EditValue = $this->FcaCodi->DisplayValue($arwrk);
					$rswrk->Close();
				} else {
					$this->FcaCodi->EditValue = $this->FcaCodi->CurrentValue;
				}
			} else {
				$this->FcaCodi->EditValue = NULL;
			}
			$this->FcaCodi->ViewCustomAttributes = "";

			// FdeCant
			$this->FdeCant->EditAttrs["class"] = "form-control";
			$this->FdeCant->EditCustomAttributes = "";
			$this->FdeCant->EditValue = ew_HtmlEncode($this->FdeCant->CurrentValue);
			$this->FdeCant->PlaceHolder = ew_RemoveHtml($this->FdeCant->FldCaption());

			// FdePrec
			$this->FdePrec->EditAttrs["class"] = "form-control";
			$this->FdePrec->EditCustomAttributes = "";
			$this->FdePrec->EditValue = ew_HtmlEncode($this->FdePrec->CurrentValue);
			$this->FdePrec->PlaceHolder = ew_RemoveHtml($this->FdePrec->FldCaption());
			if (strval($this->FdePrec->EditValue) <> "" && is_numeric($this->FdePrec->EditValue)) {
			$this->FdePrec->EditValue = ew_FormatNumber($this->FdePrec->EditValue, -2, -1, -2, 0);
			$this->FdePrec->OldValue = $this->FdePrec->EditValue;
			}

			// FdeDesc
			$this->FdeDesc->EditAttrs["class"] = "form-control";
			$this->FdeDesc->EditCustomAttributes = "";
			$this->FdeDesc->EditValue = ew_HtmlEncode($this->FdeDesc->CurrentValue);
			$this->FdeDesc->PlaceHolder = ew_RemoveHtml($this->FdeDesc->FldCaption());

			// FdeUsua
			$this->FdeUsua->EditAttrs["class"] = "form-control";
			$this->FdeUsua->EditCustomAttributes = "";
			$this->FdeUsua->EditValue = ew_HtmlEncode($this->FdeUsua->CurrentValue);
			$this->FdeUsua->PlaceHolder = ew_RemoveHtml($this->FdeUsua->FldCaption());

			// FdeFCre
			$this->FdeFCre->EditAttrs["class"] = "form-control";
			$this->FdeFCre->EditCustomAttributes = "";
			$this->FdeFCre->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->FdeFCre->CurrentValue, 7));
			$this->FdeFCre->PlaceHolder = ew_RemoveHtml($this->FdeFCre->FldCaption());

			// Edit refer script
			// FdeCodi

			$this->FdeCodi->HrefValue = "";

			// FcaCodi
			$this->FcaCodi->HrefValue = "";

			// FdeCant
			$this->FdeCant->HrefValue = "";

			// FdePrec
			$this->FdePrec->HrefValue = "";

			// FdeDesc
			$this->FdeDesc->HrefValue = "";

			// FdeUsua
			$this->FdeUsua->HrefValue = "";

			// FdeFCre
			$this->FdeFCre->HrefValue = "";
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate form
	function ValidateForm() {
		global $Language, $gsFormError;

		// Initialize form error message
		$gsFormError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return ($gsFormError == "");
		if (!$this->FdeCodi->FldIsDetailKey && !is_null($this->FdeCodi->FormValue) && $this->FdeCodi->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->FdeCodi->FldCaption(), $this->FdeCodi->ReqErrMsg));
		}
		if (!ew_CheckInteger($this->FdeCodi->FormValue)) {
			ew_AddMessage($gsFormError, $this->FdeCodi->FldErrMsg());
		}
		if (!$this->FcaCodi->FldIsDetailKey && !is_null($this->FcaCodi->FormValue) && $this->FcaCodi->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->FcaCodi->FldCaption(), $this->FcaCodi->ReqErrMsg));
		}
		if (!$this->FdeCant->FldIsDetailKey && !is_null($this->FdeCant->FormValue) && $this->FdeCant->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->FdeCant->FldCaption(), $this->FdeCant->ReqErrMsg));
		}
		if (!ew_CheckInteger($this->FdeCant->FormValue)) {
			ew_AddMessage($gsFormError, $this->FdeCant->FldErrMsg());
		}
		if (!$this->FdePrec->FldIsDetailKey && !is_null($this->FdePrec->FormValue) && $this->FdePrec->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->FdePrec->FldCaption(), $this->FdePrec->ReqErrMsg));
		}
		if (!ew_CheckNumber($this->FdePrec->FormValue)) {
			ew_AddMessage($gsFormError, $this->FdePrec->FldErrMsg());
		}
		if (!$this->FdeDesc->FldIsDetailKey && !is_null($this->FdeDesc->FormValue) && $this->FdeDesc->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->FdeDesc->FldCaption(), $this->FdeDesc->ReqErrMsg));
		}
		if (!$this->FdeUsua->FldIsDetailKey && !is_null($this->FdeUsua->FormValue) && $this->FdeUsua->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->FdeUsua->FldCaption(), $this->FdeUsua->ReqErrMsg));
		}
		if (!$this->FdeFCre->FldIsDetailKey && !is_null($this->FdeFCre->FormValue) && $this->FdeFCre->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->FdeFCre->FldCaption(), $this->FdeFCre->ReqErrMsg));
		}
		if (!ew_CheckEuroDate($this->FdeFCre->FormValue)) {
			ew_AddMessage($gsFormError, $this->FdeFCre->FldErrMsg());
		}

		// Return validate result
		$ValidateForm = ($gsFormError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateForm = $ValidateForm && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsFormError, $sFormCustomError);
		}
		return $ValidateForm;
	}

	//
	// Delete records based on current filter
	//
	function DeleteRows() {
		global $Language, $Security;
		if (!$Security->CanDelete()) {
			$this->setFailureMessage($Language->Phrase("NoDeletePermission")); // No delete permission
			return FALSE;
		}
		$DeleteRows = TRUE;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE) {
			return FALSE;
		} elseif ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
			$rs->Close();
			return FALSE;

		//} else {
		//	$this->LoadRowValues($rs); // Load row values

		}
		$rows = ($rs) ? $rs->GetRows() : array();

		// Clone old rows
		$rsold = $rows;
		if ($rs)
			$rs->Close();

		// Call row deleting event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$DeleteRows = $this->Row_Deleting($row);
				if (!$DeleteRows) break;
			}
		}
		if ($DeleteRows) {
			$sKey = "";
			foreach ($rsold as $row) {
				$sThisKey = "";
				if ($sThisKey <> "") $sThisKey .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
				$sThisKey .= $row['FdeCodi'];
				if ($sThisKey <> "") $sThisKey .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
				$sThisKey .= $row['FcaCodi'];
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				$DeleteRows = $this->Delete($row); // Delete
				$conn->raiseErrorFn = '';
				if ($DeleteRows === FALSE)
					break;
				if ($sKey <> "") $sKey .= ", ";
				$sKey .= $sThisKey;
			}
		} else {

			// Set up error message
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("DeleteCancelled"));
			}
		}
		if ($DeleteRows) {
		} else {
		}

		// Call Row Deleted event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$this->Row_Deleted($row);
			}
		}
		return $DeleteRows;
	}

	// Update record based on key values
	function EditRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$conn = &$this->Connection();
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE)
			return FALSE;
		if ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record message
			$EditRow = FALSE; // Update Failed
		} else {

			// Save old values
			$rsold = &$rs->fields;
			$this->LoadDbValues($rsold);
			$rsnew = array();

			// FdeCodi
			// FcaCodi
			// FdeCant

			$this->FdeCant->SetDbValueDef($rsnew, $this->FdeCant->CurrentValue, 0, $this->FdeCant->ReadOnly);

			// FdePrec
			$this->FdePrec->SetDbValueDef($rsnew, $this->FdePrec->CurrentValue, 0, $this->FdePrec->ReadOnly);

			// FdeDesc
			$this->FdeDesc->SetDbValueDef($rsnew, $this->FdeDesc->CurrentValue, "", $this->FdeDesc->ReadOnly);

			// FdeUsua
			$this->FdeUsua->SetDbValueDef($rsnew, $this->FdeUsua->CurrentValue, "", $this->FdeUsua->ReadOnly);

			// FdeFCre
			$this->FdeFCre->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->FdeFCre->CurrentValue, 7), ew_CurrentDate(), $this->FdeFCre->ReadOnly);

			// Call Row Updating event
			$bUpdateRow = $this->Row_Updating($rsold, $rsnew);
			if ($bUpdateRow) {
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				if (count($rsnew) > 0)
					$EditRow = $this->Update($rsnew, "", $rsold);
				else
					$EditRow = TRUE; // No field to update
				$conn->raiseErrorFn = '';
				if ($EditRow) {
				}
			} else {
				if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

					// Use the message, do nothing
				} elseif ($this->CancelMessage <> "") {
					$this->setFailureMessage($this->CancelMessage);
					$this->CancelMessage = "";
				} else {
					$this->setFailureMessage($Language->Phrase("UpdateCancelled"));
				}
				$EditRow = FALSE;
			}
		}

		// Call Row_Updated event
		if ($EditRow)
			$this->Row_Updated($rsold, $rsnew);
		$rs->Close();
		return $EditRow;
	}

	// Add record
	function AddRow($rsold = NULL) {
		global $Language, $Security;
		$conn = &$this->Connection();

		// Load db values from rsold
		if ($rsold) {
			$this->LoadDbValues($rsold);
		}
		$rsnew = array();

		// FdeCodi
		$this->FdeCodi->SetDbValueDef($rsnew, $this->FdeCodi->CurrentValue, 0, strval($this->FdeCodi->CurrentValue) == "");

		// FcaCodi
		$this->FcaCodi->SetDbValueDef($rsnew, $this->FcaCodi->CurrentValue, 0, FALSE);

		// FdeCant
		$this->FdeCant->SetDbValueDef($rsnew, $this->FdeCant->CurrentValue, 0, FALSE);

		// FdePrec
		$this->FdePrec->SetDbValueDef($rsnew, $this->FdePrec->CurrentValue, 0, FALSE);

		// FdeDesc
		$this->FdeDesc->SetDbValueDef($rsnew, $this->FdeDesc->CurrentValue, "", FALSE);

		// FdeUsua
		$this->FdeUsua->SetDbValueDef($rsnew, $this->FdeUsua->CurrentValue, "", FALSE);

		// FdeFCre
		$this->FdeFCre->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->FdeFCre->CurrentValue, 7), ew_CurrentDate(), FALSE);

		// Call Row Inserting event
		$rs = ($rsold == NULL) ? NULL : $rsold->fields;
		$bInsertRow = $this->Row_Inserting($rs, $rsnew);

		// Check if key value entered
		if ($bInsertRow && $this->ValidateKey && strval($rsnew['FdeCodi']) == "") {
			$this->setFailureMessage($Language->Phrase("InvalidKeyValue"));
			$bInsertRow = FALSE;
		}

		// Check if key value entered
		if ($bInsertRow && $this->ValidateKey && strval($rsnew['FcaCodi']) == "") {
			$this->setFailureMessage($Language->Phrase("InvalidKeyValue"));
			$bInsertRow = FALSE;
		}

		// Check for duplicate key
		if ($bInsertRow && $this->ValidateKey) {
			$sFilter = $this->KeyFilter();
			$rsChk = $this->LoadRs($sFilter);
			if ($rsChk && !$rsChk->EOF) {
				$sKeyErrMsg = str_replace("%f", $sFilter, $Language->Phrase("DupKey"));
				$this->setFailureMessage($sKeyErrMsg);
				$rsChk->Close();
				$bInsertRow = FALSE;
			}
		}
		if ($bInsertRow) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			$AddRow = $this->Insert($rsnew);
			$conn->raiseErrorFn = '';
			if ($AddRow) {
			}
		} else {
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("InsertCancelled"));
			}
			$AddRow = FALSE;
		}
		if ($AddRow) {

			// Call Row Inserted event
			$rs = ($rsold == NULL) ? NULL : $rsold->fields;
			$this->Row_Inserted($rs, $rsnew);
		}
		return $AddRow;
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$url = preg_replace('/\?cmd=reset(all){0,1}$/i', '', $url); // Remove cmd=reset / cmd=resetall
		$Breadcrumb->Add("list", $this->TableVar, $url, "", $this->TableVar, TRUE);
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}

	// ListOptions Load event
	function ListOptions_Load() {

		// Example:
		//$opt = &$this->ListOptions->Add("new");
		//$opt->Header = "xxx";
		//$opt->OnLeft = TRUE; // Link on left
		//$opt->MoveTo(0); // Move to first column

	}

	// ListOptions Rendered event
	function ListOptions_Rendered() {

		// Example: 
		//$this->ListOptions->Items["new"]->Body = "xxx";

	}

	// Row Custom Action event
	function Row_CustomAction($action, $row) {

		// Return FALSE to abort
		return TRUE;
	}

	// Page Exporting event
	// $this->ExportDoc = export document object
	function Page_Exporting() {

		//$this->ExportDoc->Text = "my header"; // Export header
		//return FALSE; // Return FALSE to skip default export and use Row_Export event

		return TRUE; // Return TRUE to use default export and skip Row_Export event
	}

	// Row Export event
	// $this->ExportDoc = export document object
	function Row_Export($rs) {

	    //$this->ExportDoc->Text .= "my content"; // Build HTML with field value: $rs["MyField"] or $this->MyField->ViewValue
	}

	// Page Exported event
	// $this->ExportDoc = export document object
	function Page_Exported() {

		//$this->ExportDoc->Text .= "my footer"; // Export footer
		//echo $this->ExportDoc->Text;

	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($FDet_list)) $FDet_list = new cFDet_list();

// Page init
$FDet_list->Page_Init();

// Page main
$FDet_list->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$FDet_list->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "list";
var CurrentForm = fFDetlist = new ew_Form("fFDetlist", "list");
fFDetlist.FormKeyCountName = '<?php echo $FDet_list->FormKeyCountName ?>';

// Validate form
fFDetlist.Validate = function() {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.GetForm(), $fobj = $(fobj);
	if ($fobj.find("#a_confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.FormKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = $fobj.find("#a_list").val() == "gridinsert";
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
		var checkrow = (gridinsert) ? !this.EmptyRow(infix) : true;
		if (checkrow) {
			addcnt++;
			elm = this.GetElements("x" + infix + "_FdeCodi");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $FDet->FdeCodi->FldCaption(), $FDet->FdeCodi->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_FdeCodi");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($FDet->FdeCodi->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_FcaCodi");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $FDet->FcaCodi->FldCaption(), $FDet->FcaCodi->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_FdeCant");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $FDet->FdeCant->FldCaption(), $FDet->FdeCant->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_FdeCant");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($FDet->FdeCant->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_FdePrec");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $FDet->FdePrec->FldCaption(), $FDet->FdePrec->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_FdePrec");
			if (elm && !ew_CheckNumber(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($FDet->FdePrec->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_FdeDesc");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $FDet->FdeDesc->FldCaption(), $FDet->FdeDesc->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_FdeUsua");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $FDet->FdeUsua->FldCaption(), $FDet->FdeUsua->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_FdeFCre");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $FDet->FdeFCre->FldCaption(), $FDet->FdeFCre->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_FdeFCre");
			if (elm && !ew_CheckEuroDate(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($FDet->FdeFCre->FldErrMsg()) ?>");

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
		} // End Grid Add checking
	}
	if (gridinsert && addcnt == 0) { // No row added
		ew_Alert(ewLanguage.Phrase("NoAddRecord"));
		return false;
	}
	return true;
}

// Check empty row
fFDetlist.EmptyRow = function(infix) {
	var fobj = this.Form;
	if (ew_ValueChanged(fobj, infix, "FdeCodi", false)) return false;
	if (ew_ValueChanged(fobj, infix, "FcaCodi", false)) return false;
	if (ew_ValueChanged(fobj, infix, "FdeCant", false)) return false;
	if (ew_ValueChanged(fobj, infix, "FdePrec", false)) return false;
	if (ew_ValueChanged(fobj, infix, "FdeDesc", false)) return false;
	if (ew_ValueChanged(fobj, infix, "FdeUsua", false)) return false;
	if (ew_ValueChanged(fobj, infix, "FdeFCre", false)) return false;
	return true;
}

// Form_CustomValidate event
fFDetlist.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fFDetlist.ValidateRequired = true;
<?php } else { ?>
fFDetlist.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
fFDetlist.Lists["x_FcaCodi"] = {"LinkField":"x_FcaCodi","Ajax":true,"AutoFill":false,"DisplayFields":["x_FcaCodi","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};

// Form object for search
var CurrentSearchForm = fFDetlistsrch = new ew_Form("fFDetlistsrch");
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php if ($FDet_list->TotalRecs > 0 && $FDet_list->ExportOptions->Visible()) { ?>
<?php $FDet_list->ExportOptions->Render("body") ?>
<?php } ?>
<?php if ($FDet_list->SearchOptions->Visible()) { ?>
<?php $FDet_list->SearchOptions->Render("body") ?>
<?php } ?>
<?php if ($FDet_list->FilterOptions->Visible()) { ?>
<?php $FDet_list->FilterOptions->Render("body") ?>
<?php } ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php
if ($FDet->CurrentAction == "gridadd") {
	$FDet->CurrentFilter = "0=1";
	$FDet_list->StartRec = 1;
	$FDet_list->DisplayRecs = $FDet->GridAddRowCount;
	$FDet_list->TotalRecs = $FDet_list->DisplayRecs;
	$FDet_list->StopRec = $FDet_list->DisplayRecs;
} else {
	$bSelectLimit = $FDet_list->UseSelectLimit;
	if ($bSelectLimit) {
		if ($FDet_list->TotalRecs <= 0)
			$FDet_list->TotalRecs = $FDet->SelectRecordCount();
	} else {
		if (!$FDet_list->Recordset && ($FDet_list->Recordset = $FDet_list->LoadRecordset()))
			$FDet_list->TotalRecs = $FDet_list->Recordset->RecordCount();
	}
	$FDet_list->StartRec = 1;
	if ($FDet_list->DisplayRecs <= 0 || ($FDet->Export <> "" && $FDet->ExportAll)) // Display all records
		$FDet_list->DisplayRecs = $FDet_list->TotalRecs;
	if (!($FDet->Export <> "" && $FDet->ExportAll))
		$FDet_list->SetUpStartRec(); // Set up start record position
	if ($bSelectLimit)
		$FDet_list->Recordset = $FDet_list->LoadRecordset($FDet_list->StartRec-1, $FDet_list->DisplayRecs);

	// Set no record found message
	if ($FDet->CurrentAction == "" && $FDet_list->TotalRecs == 0) {
		if (!$Security->CanList())
			$FDet_list->setWarningMessage($Language->Phrase("NoPermission"));
		if ($FDet_list->SearchWhere == "0=101")
			$FDet_list->setWarningMessage($Language->Phrase("EnterSearchCriteria"));
		else
			$FDet_list->setWarningMessage($Language->Phrase("NoRecord"));
	}
}
$FDet_list->RenderOtherOptions();
?>
<?php if ($Security->CanSearch()) { ?>
<?php if ($FDet->Export == "" && $FDet->CurrentAction == "") { ?>
<form name="fFDetlistsrch" id="fFDetlistsrch" class="form-inline ewForm" action="<?php echo ew_CurrentPage() ?>">
<?php $SearchPanelClass = ($FDet_list->SearchWhere <> "") ? " in" : " in"; ?>
<div id="fFDetlistsrch_SearchPanel" class="ewSearchPanel collapse<?php echo $SearchPanelClass ?>">
<input type="hidden" name="cmd" value="search">
<input type="hidden" name="t" value="FDet">
	<div class="ewBasicSearch">
<div id="xsr_1" class="ewRow">
	<div class="ewQuickSearch input-group">
	<input type="text" name="<?php echo EW_TABLE_BASIC_SEARCH ?>" id="<?php echo EW_TABLE_BASIC_SEARCH ?>" class="form-control" value="<?php echo ew_HtmlEncode($FDet_list->BasicSearch->getKeyword()) ?>" placeholder="<?php echo ew_HtmlEncode($Language->Phrase("Search")) ?>">
	<input type="hidden" name="<?php echo EW_TABLE_BASIC_SEARCH_TYPE ?>" id="<?php echo EW_TABLE_BASIC_SEARCH_TYPE ?>" value="<?php echo ew_HtmlEncode($FDet_list->BasicSearch->getType()) ?>">
	<div class="input-group-btn">
		<button type="button" data-toggle="dropdown" class="btn btn-default"><span id="searchtype"><?php echo $FDet_list->BasicSearch->getTypeNameShort() ?></span><span class="caret"></span></button>
		<ul class="dropdown-menu pull-right" role="menu">
			<li<?php if ($FDet_list->BasicSearch->getType() == "") echo " class=\"active\""; ?>><a href="javascript:void(0);" onclick="ew_SetSearchType(this)"><?php echo $Language->Phrase("QuickSearchAuto") ?></a></li>
			<li<?php if ($FDet_list->BasicSearch->getType() == "=") echo " class=\"active\""; ?>><a href="javascript:void(0);" onclick="ew_SetSearchType(this,'=')"><?php echo $Language->Phrase("QuickSearchExact") ?></a></li>
			<li<?php if ($FDet_list->BasicSearch->getType() == "AND") echo " class=\"active\""; ?>><a href="javascript:void(0);" onclick="ew_SetSearchType(this,'AND')"><?php echo $Language->Phrase("QuickSearchAll") ?></a></li>
			<li<?php if ($FDet_list->BasicSearch->getType() == "OR") echo " class=\"active\""; ?>><a href="javascript:void(0);" onclick="ew_SetSearchType(this,'OR')"><?php echo $Language->Phrase("QuickSearchAny") ?></a></li>
		</ul>
	<button class="btn btn-primary ewButton" name="btnsubmit" id="btnsubmit" type="submit"><?php echo $Language->Phrase("QuickSearchBtn") ?></button>
	</div>
	</div>
</div>
	</div>
</div>
</form>
<?php } ?>
<?php } ?>
<?php $FDet_list->ShowPageHeader(); ?>
<?php
$FDet_list->ShowMessage();
?>
<?php if ($FDet_list->TotalRecs > 0 || $FDet->CurrentAction <> "") { ?>
<div class="panel panel-default ewGrid">
<form name="fFDetlist" id="fFDetlist" class="form-inline ewForm ewListForm" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($FDet_list->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $FDet_list->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="FDet">
<div id="gmp_FDet" class="<?php if (ew_IsResponsiveLayout()) { echo "table-responsive "; } ?>ewGridMiddlePanel">
<?php if ($FDet_list->TotalRecs > 0) { ?>
<table id="tbl_FDetlist" class="table ewTable">
<?php echo $FDet->TableCustomInnerHtml ?>
<thead><!-- Table header -->
	<tr class="ewTableHeader">
<?php

// Header row
$FDet_list->RowType = EW_ROWTYPE_HEADER;

// Render list options
$FDet_list->RenderListOptions();

// Render list options (header, left)
$FDet_list->ListOptions->Render("header", "left");
?>
<?php if ($FDet->FdeCodi->Visible) { // FdeCodi ?>
	<?php if ($FDet->SortUrl($FDet->FdeCodi) == "") { ?>
		<th data-name="FdeCodi"><div id="elh_FDet_FdeCodi" class="FDet_FdeCodi"><div class="ewTableHeaderCaption"><?php echo $FDet->FdeCodi->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="FdeCodi"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $FDet->SortUrl($FDet->FdeCodi) ?>',2);"><div id="elh_FDet_FdeCodi" class="FDet_FdeCodi">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $FDet->FdeCodi->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($FDet->FdeCodi->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($FDet->FdeCodi->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($FDet->FcaCodi->Visible) { // FcaCodi ?>
	<?php if ($FDet->SortUrl($FDet->FcaCodi) == "") { ?>
		<th data-name="FcaCodi"><div id="elh_FDet_FcaCodi" class="FDet_FcaCodi"><div class="ewTableHeaderCaption"><?php echo $FDet->FcaCodi->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="FcaCodi"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $FDet->SortUrl($FDet->FcaCodi) ?>',2);"><div id="elh_FDet_FcaCodi" class="FDet_FcaCodi">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $FDet->FcaCodi->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($FDet->FcaCodi->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($FDet->FcaCodi->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($FDet->FdeCant->Visible) { // FdeCant ?>
	<?php if ($FDet->SortUrl($FDet->FdeCant) == "") { ?>
		<th data-name="FdeCant"><div id="elh_FDet_FdeCant" class="FDet_FdeCant"><div class="ewTableHeaderCaption"><?php echo $FDet->FdeCant->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="FdeCant"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $FDet->SortUrl($FDet->FdeCant) ?>',2);"><div id="elh_FDet_FdeCant" class="FDet_FdeCant">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $FDet->FdeCant->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($FDet->FdeCant->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($FDet->FdeCant->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($FDet->FdePrec->Visible) { // FdePrec ?>
	<?php if ($FDet->SortUrl($FDet->FdePrec) == "") { ?>
		<th data-name="FdePrec"><div id="elh_FDet_FdePrec" class="FDet_FdePrec"><div class="ewTableHeaderCaption"><?php echo $FDet->FdePrec->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="FdePrec"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $FDet->SortUrl($FDet->FdePrec) ?>',2);"><div id="elh_FDet_FdePrec" class="FDet_FdePrec">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $FDet->FdePrec->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($FDet->FdePrec->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($FDet->FdePrec->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($FDet->FdeDesc->Visible) { // FdeDesc ?>
	<?php if ($FDet->SortUrl($FDet->FdeDesc) == "") { ?>
		<th data-name="FdeDesc"><div id="elh_FDet_FdeDesc" class="FDet_FdeDesc"><div class="ewTableHeaderCaption"><?php echo $FDet->FdeDesc->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="FdeDesc"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $FDet->SortUrl($FDet->FdeDesc) ?>',2);"><div id="elh_FDet_FdeDesc" class="FDet_FdeDesc">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $FDet->FdeDesc->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($FDet->FdeDesc->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($FDet->FdeDesc->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($FDet->FdeUsua->Visible) { // FdeUsua ?>
	<?php if ($FDet->SortUrl($FDet->FdeUsua) == "") { ?>
		<th data-name="FdeUsua"><div id="elh_FDet_FdeUsua" class="FDet_FdeUsua"><div class="ewTableHeaderCaption"><?php echo $FDet->FdeUsua->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="FdeUsua"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $FDet->SortUrl($FDet->FdeUsua) ?>',2);"><div id="elh_FDet_FdeUsua" class="FDet_FdeUsua">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $FDet->FdeUsua->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($FDet->FdeUsua->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($FDet->FdeUsua->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($FDet->FdeFCre->Visible) { // FdeFCre ?>
	<?php if ($FDet->SortUrl($FDet->FdeFCre) == "") { ?>
		<th data-name="FdeFCre"><div id="elh_FDet_FdeFCre" class="FDet_FdeFCre"><div class="ewTableHeaderCaption"><?php echo $FDet->FdeFCre->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="FdeFCre"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $FDet->SortUrl($FDet->FdeFCre) ?>',2);"><div id="elh_FDet_FdeFCre" class="FDet_FdeFCre">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $FDet->FdeFCre->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($FDet->FdeFCre->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($FDet->FdeFCre->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php

// Render list options (header, right)
$FDet_list->ListOptions->Render("header", "right");
?>
	</tr>
</thead>
<tbody>
<?php
if ($FDet->ExportAll && $FDet->Export <> "") {
	$FDet_list->StopRec = $FDet_list->TotalRecs;
} else {

	// Set the last record to display
	if ($FDet_list->TotalRecs > $FDet_list->StartRec + $FDet_list->DisplayRecs - 1)
		$FDet_list->StopRec = $FDet_list->StartRec + $FDet_list->DisplayRecs - 1;
	else
		$FDet_list->StopRec = $FDet_list->TotalRecs;
}

// Restore number of post back records
if ($objForm) {
	$objForm->Index = -1;
	if ($objForm->HasValue($FDet_list->FormKeyCountName) && ($FDet->CurrentAction == "gridadd" || $FDet->CurrentAction == "gridedit" || $FDet->CurrentAction == "F")) {
		$FDet_list->KeyCount = $objForm->GetValue($FDet_list->FormKeyCountName);
		$FDet_list->StopRec = $FDet_list->StartRec + $FDet_list->KeyCount - 1;
	}
}
$FDet_list->RecCnt = $FDet_list->StartRec - 1;
if ($FDet_list->Recordset && !$FDet_list->Recordset->EOF) {
	$FDet_list->Recordset->MoveFirst();
	$bSelectLimit = $FDet_list->UseSelectLimit;
	if (!$bSelectLimit && $FDet_list->StartRec > 1)
		$FDet_list->Recordset->Move($FDet_list->StartRec - 1);
} elseif (!$FDet->AllowAddDeleteRow && $FDet_list->StopRec == 0) {
	$FDet_list->StopRec = $FDet->GridAddRowCount;
}

// Initialize aggregate
$FDet->RowType = EW_ROWTYPE_AGGREGATEINIT;
$FDet->ResetAttrs();
$FDet_list->RenderRow();
if ($FDet->CurrentAction == "gridadd")
	$FDet_list->RowIndex = 0;
if ($FDet->CurrentAction == "gridedit")
	$FDet_list->RowIndex = 0;
while ($FDet_list->RecCnt < $FDet_list->StopRec) {
	$FDet_list->RecCnt++;
	if (intval($FDet_list->RecCnt) >= intval($FDet_list->StartRec)) {
		$FDet_list->RowCnt++;
		if ($FDet->CurrentAction == "gridadd" || $FDet->CurrentAction == "gridedit" || $FDet->CurrentAction == "F") {
			$FDet_list->RowIndex++;
			$objForm->Index = $FDet_list->RowIndex;
			if ($objForm->HasValue($FDet_list->FormActionName))
				$FDet_list->RowAction = strval($objForm->GetValue($FDet_list->FormActionName));
			elseif ($FDet->CurrentAction == "gridadd")
				$FDet_list->RowAction = "insert";
			else
				$FDet_list->RowAction = "";
		}

		// Set up key count
		$FDet_list->KeyCount = $FDet_list->RowIndex;

		// Init row class and style
		$FDet->ResetAttrs();
		$FDet->CssClass = "";
		if ($FDet->CurrentAction == "gridadd") {
			$FDet_list->LoadDefaultValues(); // Load default values
		} else {
			$FDet_list->LoadRowValues($FDet_list->Recordset); // Load row values
		}
		$FDet->RowType = EW_ROWTYPE_VIEW; // Render view
		if ($FDet->CurrentAction == "gridadd") // Grid add
			$FDet->RowType = EW_ROWTYPE_ADD; // Render add
		if ($FDet->CurrentAction == "gridadd" && $FDet->EventCancelled && !$objForm->HasValue("k_blankrow")) // Insert failed
			$FDet_list->RestoreCurrentRowFormValues($FDet_list->RowIndex); // Restore form values
		if ($FDet->CurrentAction == "gridedit") { // Grid edit
			if ($FDet->EventCancelled) {
				$FDet_list->RestoreCurrentRowFormValues($FDet_list->RowIndex); // Restore form values
			}
			if ($FDet_list->RowAction == "insert")
				$FDet->RowType = EW_ROWTYPE_ADD; // Render add
			else
				$FDet->RowType = EW_ROWTYPE_EDIT; // Render edit
		}
		if ($FDet->CurrentAction == "gridedit" && ($FDet->RowType == EW_ROWTYPE_EDIT || $FDet->RowType == EW_ROWTYPE_ADD) && $FDet->EventCancelled) // Update failed
			$FDet_list->RestoreCurrentRowFormValues($FDet_list->RowIndex); // Restore form values
		if ($FDet->RowType == EW_ROWTYPE_EDIT) // Edit row
			$FDet_list->EditRowCnt++;

		// Set up row id / data-rowindex
		$FDet->RowAttrs = array_merge($FDet->RowAttrs, array('data-rowindex'=>$FDet_list->RowCnt, 'id'=>'r' . $FDet_list->RowCnt . '_FDet', 'data-rowtype'=>$FDet->RowType));

		// Render row
		$FDet_list->RenderRow();

		// Render list options
		$FDet_list->RenderListOptions();

		// Skip delete row / empty row for confirm page
		if ($FDet_list->RowAction <> "delete" && $FDet_list->RowAction <> "insertdelete" && !($FDet_list->RowAction == "insert" && $FDet->CurrentAction == "F" && $FDet_list->EmptyRow())) {
?>
	<tr<?php echo $FDet->RowAttributes() ?>>
<?php

// Render list options (body, left)
$FDet_list->ListOptions->Render("body", "left", $FDet_list->RowCnt);
?>
	<?php if ($FDet->FdeCodi->Visible) { // FdeCodi ?>
		<td data-name="FdeCodi"<?php echo $FDet->FdeCodi->CellAttributes() ?>>
<?php if ($FDet->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $FDet_list->RowCnt ?>_FDet_FdeCodi" class="form-group FDet_FdeCodi">
<input type="text" data-table="FDet" data-field="x_FdeCodi" name="x<?php echo $FDet_list->RowIndex ?>_FdeCodi" id="x<?php echo $FDet_list->RowIndex ?>_FdeCodi" size="30" placeholder="<?php echo ew_HtmlEncode($FDet->FdeCodi->getPlaceHolder()) ?>" value="<?php echo $FDet->FdeCodi->EditValue ?>"<?php echo $FDet->FdeCodi->EditAttributes() ?>>
</span>
<input type="hidden" data-table="FDet" data-field="x_FdeCodi" name="o<?php echo $FDet_list->RowIndex ?>_FdeCodi" id="o<?php echo $FDet_list->RowIndex ?>_FdeCodi" value="<?php echo ew_HtmlEncode($FDet->FdeCodi->OldValue) ?>">
<?php } ?>
<?php if ($FDet->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $FDet_list->RowCnt ?>_FDet_FdeCodi" class="form-group FDet_FdeCodi">
<span<?php echo $FDet->FdeCodi->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $FDet->FdeCodi->EditValue ?></p></span>
</span>
<input type="hidden" data-table="FDet" data-field="x_FdeCodi" name="x<?php echo $FDet_list->RowIndex ?>_FdeCodi" id="x<?php echo $FDet_list->RowIndex ?>_FdeCodi" value="<?php echo ew_HtmlEncode($FDet->FdeCodi->CurrentValue) ?>">
<?php } ?>
<?php if ($FDet->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $FDet_list->RowCnt ?>_FDet_FdeCodi" class="FDet_FdeCodi">
<span<?php echo $FDet->FdeCodi->ViewAttributes() ?>>
<?php echo $FDet->FdeCodi->ListViewValue() ?></span>
</span>
<?php } ?>
<a id="<?php echo $FDet_list->PageObjName . "_row_" . $FDet_list->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($FDet->FcaCodi->Visible) { // FcaCodi ?>
		<td data-name="FcaCodi"<?php echo $FDet->FcaCodi->CellAttributes() ?>>
<?php if ($FDet->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $FDet_list->RowCnt ?>_FDet_FcaCodi" class="form-group FDet_FcaCodi">
<select data-table="FDet" data-field="x_FcaCodi" data-value-separator="<?php echo ew_HtmlEncode(is_array($FDet->FcaCodi->DisplayValueSeparator) ? json_encode($FDet->FcaCodi->DisplayValueSeparator) : $FDet->FcaCodi->DisplayValueSeparator) ?>" id="x<?php echo $FDet_list->RowIndex ?>_FcaCodi" name="x<?php echo $FDet_list->RowIndex ?>_FcaCodi"<?php echo $FDet->FcaCodi->EditAttributes() ?>>
<?php
if (is_array($FDet->FcaCodi->EditValue)) {
	$arwrk = $FDet->FcaCodi->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($FDet->FcaCodi->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $FDet->FcaCodi->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($FDet->FcaCodi->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($FDet->FcaCodi->CurrentValue) ?>" selected><?php echo $FDet->FcaCodi->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $FDet->FcaCodi->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT \"FcaCodi\", \"FcaCodi\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"FCab\"";
$sWhereWrk = "";
$FDet->FcaCodi->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$FDet->FcaCodi->LookupFilters += array("f0" => "\"FcaCodi\" = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$FDet->Lookup_Selecting($FDet->FcaCodi, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $FDet->FcaCodi->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $FDet_list->RowIndex ?>_FcaCodi" id="s_x<?php echo $FDet_list->RowIndex ?>_FcaCodi" value="<?php echo $FDet->FcaCodi->LookupFilterQuery() ?>">
</span>
<input type="hidden" data-table="FDet" data-field="x_FcaCodi" name="o<?php echo $FDet_list->RowIndex ?>_FcaCodi" id="o<?php echo $FDet_list->RowIndex ?>_FcaCodi" value="<?php echo ew_HtmlEncode($FDet->FcaCodi->OldValue) ?>">
<?php } ?>
<?php if ($FDet->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $FDet_list->RowCnt ?>_FDet_FcaCodi" class="form-group FDet_FcaCodi">
<span<?php echo $FDet->FcaCodi->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $FDet->FcaCodi->EditValue ?></p></span>
</span>
<input type="hidden" data-table="FDet" data-field="x_FcaCodi" name="x<?php echo $FDet_list->RowIndex ?>_FcaCodi" id="x<?php echo $FDet_list->RowIndex ?>_FcaCodi" value="<?php echo ew_HtmlEncode($FDet->FcaCodi->CurrentValue) ?>">
<?php } ?>
<?php if ($FDet->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $FDet_list->RowCnt ?>_FDet_FcaCodi" class="FDet_FcaCodi">
<span<?php echo $FDet->FcaCodi->ViewAttributes() ?>>
<?php echo $FDet->FcaCodi->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($FDet->FdeCant->Visible) { // FdeCant ?>
		<td data-name="FdeCant"<?php echo $FDet->FdeCant->CellAttributes() ?>>
<?php if ($FDet->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $FDet_list->RowCnt ?>_FDet_FdeCant" class="form-group FDet_FdeCant">
<input type="text" data-table="FDet" data-field="x_FdeCant" name="x<?php echo $FDet_list->RowIndex ?>_FdeCant" id="x<?php echo $FDet_list->RowIndex ?>_FdeCant" size="30" placeholder="<?php echo ew_HtmlEncode($FDet->FdeCant->getPlaceHolder()) ?>" value="<?php echo $FDet->FdeCant->EditValue ?>"<?php echo $FDet->FdeCant->EditAttributes() ?>>
</span>
<input type="hidden" data-table="FDet" data-field="x_FdeCant" name="o<?php echo $FDet_list->RowIndex ?>_FdeCant" id="o<?php echo $FDet_list->RowIndex ?>_FdeCant" value="<?php echo ew_HtmlEncode($FDet->FdeCant->OldValue) ?>">
<?php } ?>
<?php if ($FDet->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $FDet_list->RowCnt ?>_FDet_FdeCant" class="form-group FDet_FdeCant">
<input type="text" data-table="FDet" data-field="x_FdeCant" name="x<?php echo $FDet_list->RowIndex ?>_FdeCant" id="x<?php echo $FDet_list->RowIndex ?>_FdeCant" size="30" placeholder="<?php echo ew_HtmlEncode($FDet->FdeCant->getPlaceHolder()) ?>" value="<?php echo $FDet->FdeCant->EditValue ?>"<?php echo $FDet->FdeCant->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($FDet->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $FDet_list->RowCnt ?>_FDet_FdeCant" class="FDet_FdeCant">
<span<?php echo $FDet->FdeCant->ViewAttributes() ?>>
<?php echo $FDet->FdeCant->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($FDet->FdePrec->Visible) { // FdePrec ?>
		<td data-name="FdePrec"<?php echo $FDet->FdePrec->CellAttributes() ?>>
<?php if ($FDet->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $FDet_list->RowCnt ?>_FDet_FdePrec" class="form-group FDet_FdePrec">
<input type="text" data-table="FDet" data-field="x_FdePrec" name="x<?php echo $FDet_list->RowIndex ?>_FdePrec" id="x<?php echo $FDet_list->RowIndex ?>_FdePrec" size="30" placeholder="<?php echo ew_HtmlEncode($FDet->FdePrec->getPlaceHolder()) ?>" value="<?php echo $FDet->FdePrec->EditValue ?>"<?php echo $FDet->FdePrec->EditAttributes() ?>>
</span>
<input type="hidden" data-table="FDet" data-field="x_FdePrec" name="o<?php echo $FDet_list->RowIndex ?>_FdePrec" id="o<?php echo $FDet_list->RowIndex ?>_FdePrec" value="<?php echo ew_HtmlEncode($FDet->FdePrec->OldValue) ?>">
<?php } ?>
<?php if ($FDet->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $FDet_list->RowCnt ?>_FDet_FdePrec" class="form-group FDet_FdePrec">
<input type="text" data-table="FDet" data-field="x_FdePrec" name="x<?php echo $FDet_list->RowIndex ?>_FdePrec" id="x<?php echo $FDet_list->RowIndex ?>_FdePrec" size="30" placeholder="<?php echo ew_HtmlEncode($FDet->FdePrec->getPlaceHolder()) ?>" value="<?php echo $FDet->FdePrec->EditValue ?>"<?php echo $FDet->FdePrec->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($FDet->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $FDet_list->RowCnt ?>_FDet_FdePrec" class="FDet_FdePrec">
<span<?php echo $FDet->FdePrec->ViewAttributes() ?>>
<?php echo $FDet->FdePrec->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($FDet->FdeDesc->Visible) { // FdeDesc ?>
		<td data-name="FdeDesc"<?php echo $FDet->FdeDesc->CellAttributes() ?>>
<?php if ($FDet->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $FDet_list->RowCnt ?>_FDet_FdeDesc" class="form-group FDet_FdeDesc">
<input type="text" data-table="FDet" data-field="x_FdeDesc" name="x<?php echo $FDet_list->RowIndex ?>_FdeDesc" id="x<?php echo $FDet_list->RowIndex ?>_FdeDesc" size="30" placeholder="<?php echo ew_HtmlEncode($FDet->FdeDesc->getPlaceHolder()) ?>" value="<?php echo $FDet->FdeDesc->EditValue ?>"<?php echo $FDet->FdeDesc->EditAttributes() ?>>
</span>
<input type="hidden" data-table="FDet" data-field="x_FdeDesc" name="o<?php echo $FDet_list->RowIndex ?>_FdeDesc" id="o<?php echo $FDet_list->RowIndex ?>_FdeDesc" value="<?php echo ew_HtmlEncode($FDet->FdeDesc->OldValue) ?>">
<?php } ?>
<?php if ($FDet->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $FDet_list->RowCnt ?>_FDet_FdeDesc" class="form-group FDet_FdeDesc">
<input type="text" data-table="FDet" data-field="x_FdeDesc" name="x<?php echo $FDet_list->RowIndex ?>_FdeDesc" id="x<?php echo $FDet_list->RowIndex ?>_FdeDesc" size="30" placeholder="<?php echo ew_HtmlEncode($FDet->FdeDesc->getPlaceHolder()) ?>" value="<?php echo $FDet->FdeDesc->EditValue ?>"<?php echo $FDet->FdeDesc->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($FDet->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $FDet_list->RowCnt ?>_FDet_FdeDesc" class="FDet_FdeDesc">
<span<?php echo $FDet->FdeDesc->ViewAttributes() ?>>
<?php echo $FDet->FdeDesc->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($FDet->FdeUsua->Visible) { // FdeUsua ?>
		<td data-name="FdeUsua"<?php echo $FDet->FdeUsua->CellAttributes() ?>>
<?php if ($FDet->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $FDet_list->RowCnt ?>_FDet_FdeUsua" class="form-group FDet_FdeUsua">
<input type="text" data-table="FDet" data-field="x_FdeUsua" name="x<?php echo $FDet_list->RowIndex ?>_FdeUsua" id="x<?php echo $FDet_list->RowIndex ?>_FdeUsua" size="30" placeholder="<?php echo ew_HtmlEncode($FDet->FdeUsua->getPlaceHolder()) ?>" value="<?php echo $FDet->FdeUsua->EditValue ?>"<?php echo $FDet->FdeUsua->EditAttributes() ?>>
</span>
<input type="hidden" data-table="FDet" data-field="x_FdeUsua" name="o<?php echo $FDet_list->RowIndex ?>_FdeUsua" id="o<?php echo $FDet_list->RowIndex ?>_FdeUsua" value="<?php echo ew_HtmlEncode($FDet->FdeUsua->OldValue) ?>">
<?php } ?>
<?php if ($FDet->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $FDet_list->RowCnt ?>_FDet_FdeUsua" class="form-group FDet_FdeUsua">
<input type="text" data-table="FDet" data-field="x_FdeUsua" name="x<?php echo $FDet_list->RowIndex ?>_FdeUsua" id="x<?php echo $FDet_list->RowIndex ?>_FdeUsua" size="30" placeholder="<?php echo ew_HtmlEncode($FDet->FdeUsua->getPlaceHolder()) ?>" value="<?php echo $FDet->FdeUsua->EditValue ?>"<?php echo $FDet->FdeUsua->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($FDet->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $FDet_list->RowCnt ?>_FDet_FdeUsua" class="FDet_FdeUsua">
<span<?php echo $FDet->FdeUsua->ViewAttributes() ?>>
<?php echo $FDet->FdeUsua->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($FDet->FdeFCre->Visible) { // FdeFCre ?>
		<td data-name="FdeFCre"<?php echo $FDet->FdeFCre->CellAttributes() ?>>
<?php if ($FDet->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $FDet_list->RowCnt ?>_FDet_FdeFCre" class="form-group FDet_FdeFCre">
<input type="text" data-table="FDet" data-field="x_FdeFCre" data-format="7" name="x<?php echo $FDet_list->RowIndex ?>_FdeFCre" id="x<?php echo $FDet_list->RowIndex ?>_FdeFCre" placeholder="<?php echo ew_HtmlEncode($FDet->FdeFCre->getPlaceHolder()) ?>" value="<?php echo $FDet->FdeFCre->EditValue ?>"<?php echo $FDet->FdeFCre->EditAttributes() ?>>
</span>
<input type="hidden" data-table="FDet" data-field="x_FdeFCre" name="o<?php echo $FDet_list->RowIndex ?>_FdeFCre" id="o<?php echo $FDet_list->RowIndex ?>_FdeFCre" value="<?php echo ew_HtmlEncode($FDet->FdeFCre->OldValue) ?>">
<?php } ?>
<?php if ($FDet->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $FDet_list->RowCnt ?>_FDet_FdeFCre" class="form-group FDet_FdeFCre">
<input type="text" data-table="FDet" data-field="x_FdeFCre" data-format="7" name="x<?php echo $FDet_list->RowIndex ?>_FdeFCre" id="x<?php echo $FDet_list->RowIndex ?>_FdeFCre" placeholder="<?php echo ew_HtmlEncode($FDet->FdeFCre->getPlaceHolder()) ?>" value="<?php echo $FDet->FdeFCre->EditValue ?>"<?php echo $FDet->FdeFCre->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($FDet->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $FDet_list->RowCnt ?>_FDet_FdeFCre" class="FDet_FdeFCre">
<span<?php echo $FDet->FdeFCre->ViewAttributes() ?>>
<?php echo $FDet->FdeFCre->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
<?php

// Render list options (body, right)
$FDet_list->ListOptions->Render("body", "right", $FDet_list->RowCnt);
?>
	</tr>
<?php if ($FDet->RowType == EW_ROWTYPE_ADD || $FDet->RowType == EW_ROWTYPE_EDIT) { ?>
<script type="text/javascript">
fFDetlist.UpdateOpts(<?php echo $FDet_list->RowIndex ?>);
</script>
<?php } ?>
<?php
	}
	} // End delete row checking
	if ($FDet->CurrentAction <> "gridadd")
		if (!$FDet_list->Recordset->EOF) $FDet_list->Recordset->MoveNext();
}
?>
<?php
	if ($FDet->CurrentAction == "gridadd" || $FDet->CurrentAction == "gridedit") {
		$FDet_list->RowIndex = '$rowindex$';
		$FDet_list->LoadDefaultValues();

		// Set row properties
		$FDet->ResetAttrs();
		$FDet->RowAttrs = array_merge($FDet->RowAttrs, array('data-rowindex'=>$FDet_list->RowIndex, 'id'=>'r0_FDet', 'data-rowtype'=>EW_ROWTYPE_ADD));
		ew_AppendClass($FDet->RowAttrs["class"], "ewTemplate");
		$FDet->RowType = EW_ROWTYPE_ADD;

		// Render row
		$FDet_list->RenderRow();

		// Render list options
		$FDet_list->RenderListOptions();
		$FDet_list->StartRowCnt = 0;
?>
	<tr<?php echo $FDet->RowAttributes() ?>>
<?php

// Render list options (body, left)
$FDet_list->ListOptions->Render("body", "left", $FDet_list->RowIndex);
?>
	<?php if ($FDet->FdeCodi->Visible) { // FdeCodi ?>
		<td data-name="FdeCodi">
<span id="el$rowindex$_FDet_FdeCodi" class="form-group FDet_FdeCodi">
<input type="text" data-table="FDet" data-field="x_FdeCodi" name="x<?php echo $FDet_list->RowIndex ?>_FdeCodi" id="x<?php echo $FDet_list->RowIndex ?>_FdeCodi" size="30" placeholder="<?php echo ew_HtmlEncode($FDet->FdeCodi->getPlaceHolder()) ?>" value="<?php echo $FDet->FdeCodi->EditValue ?>"<?php echo $FDet->FdeCodi->EditAttributes() ?>>
</span>
<input type="hidden" data-table="FDet" data-field="x_FdeCodi" name="o<?php echo $FDet_list->RowIndex ?>_FdeCodi" id="o<?php echo $FDet_list->RowIndex ?>_FdeCodi" value="<?php echo ew_HtmlEncode($FDet->FdeCodi->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($FDet->FcaCodi->Visible) { // FcaCodi ?>
		<td data-name="FcaCodi">
<span id="el$rowindex$_FDet_FcaCodi" class="form-group FDet_FcaCodi">
<select data-table="FDet" data-field="x_FcaCodi" data-value-separator="<?php echo ew_HtmlEncode(is_array($FDet->FcaCodi->DisplayValueSeparator) ? json_encode($FDet->FcaCodi->DisplayValueSeparator) : $FDet->FcaCodi->DisplayValueSeparator) ?>" id="x<?php echo $FDet_list->RowIndex ?>_FcaCodi" name="x<?php echo $FDet_list->RowIndex ?>_FcaCodi"<?php echo $FDet->FcaCodi->EditAttributes() ?>>
<?php
if (is_array($FDet->FcaCodi->EditValue)) {
	$arwrk = $FDet->FcaCodi->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($FDet->FcaCodi->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $FDet->FcaCodi->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($FDet->FcaCodi->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($FDet->FcaCodi->CurrentValue) ?>" selected><?php echo $FDet->FcaCodi->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $FDet->FcaCodi->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT \"FcaCodi\", \"FcaCodi\" AS \"DispFld\", '' AS \"Disp2Fld\", '' AS \"Disp3Fld\", '' AS \"Disp4Fld\" FROM \"public\".\"FCab\"";
$sWhereWrk = "";
$FDet->FcaCodi->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$FDet->FcaCodi->LookupFilters += array("f0" => "\"FcaCodi\" = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$FDet->Lookup_Selecting($FDet->FcaCodi, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $FDet->FcaCodi->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $FDet_list->RowIndex ?>_FcaCodi" id="s_x<?php echo $FDet_list->RowIndex ?>_FcaCodi" value="<?php echo $FDet->FcaCodi->LookupFilterQuery() ?>">
</span>
<input type="hidden" data-table="FDet" data-field="x_FcaCodi" name="o<?php echo $FDet_list->RowIndex ?>_FcaCodi" id="o<?php echo $FDet_list->RowIndex ?>_FcaCodi" value="<?php echo ew_HtmlEncode($FDet->FcaCodi->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($FDet->FdeCant->Visible) { // FdeCant ?>
		<td data-name="FdeCant">
<span id="el$rowindex$_FDet_FdeCant" class="form-group FDet_FdeCant">
<input type="text" data-table="FDet" data-field="x_FdeCant" name="x<?php echo $FDet_list->RowIndex ?>_FdeCant" id="x<?php echo $FDet_list->RowIndex ?>_FdeCant" size="30" placeholder="<?php echo ew_HtmlEncode($FDet->FdeCant->getPlaceHolder()) ?>" value="<?php echo $FDet->FdeCant->EditValue ?>"<?php echo $FDet->FdeCant->EditAttributes() ?>>
</span>
<input type="hidden" data-table="FDet" data-field="x_FdeCant" name="o<?php echo $FDet_list->RowIndex ?>_FdeCant" id="o<?php echo $FDet_list->RowIndex ?>_FdeCant" value="<?php echo ew_HtmlEncode($FDet->FdeCant->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($FDet->FdePrec->Visible) { // FdePrec ?>
		<td data-name="FdePrec">
<span id="el$rowindex$_FDet_FdePrec" class="form-group FDet_FdePrec">
<input type="text" data-table="FDet" data-field="x_FdePrec" name="x<?php echo $FDet_list->RowIndex ?>_FdePrec" id="x<?php echo $FDet_list->RowIndex ?>_FdePrec" size="30" placeholder="<?php echo ew_HtmlEncode($FDet->FdePrec->getPlaceHolder()) ?>" value="<?php echo $FDet->FdePrec->EditValue ?>"<?php echo $FDet->FdePrec->EditAttributes() ?>>
</span>
<input type="hidden" data-table="FDet" data-field="x_FdePrec" name="o<?php echo $FDet_list->RowIndex ?>_FdePrec" id="o<?php echo $FDet_list->RowIndex ?>_FdePrec" value="<?php echo ew_HtmlEncode($FDet->FdePrec->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($FDet->FdeDesc->Visible) { // FdeDesc ?>
		<td data-name="FdeDesc">
<span id="el$rowindex$_FDet_FdeDesc" class="form-group FDet_FdeDesc">
<input type="text" data-table="FDet" data-field="x_FdeDesc" name="x<?php echo $FDet_list->RowIndex ?>_FdeDesc" id="x<?php echo $FDet_list->RowIndex ?>_FdeDesc" size="30" placeholder="<?php echo ew_HtmlEncode($FDet->FdeDesc->getPlaceHolder()) ?>" value="<?php echo $FDet->FdeDesc->EditValue ?>"<?php echo $FDet->FdeDesc->EditAttributes() ?>>
</span>
<input type="hidden" data-table="FDet" data-field="x_FdeDesc" name="o<?php echo $FDet_list->RowIndex ?>_FdeDesc" id="o<?php echo $FDet_list->RowIndex ?>_FdeDesc" value="<?php echo ew_HtmlEncode($FDet->FdeDesc->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($FDet->FdeUsua->Visible) { // FdeUsua ?>
		<td data-name="FdeUsua">
<span id="el$rowindex$_FDet_FdeUsua" class="form-group FDet_FdeUsua">
<input type="text" data-table="FDet" data-field="x_FdeUsua" name="x<?php echo $FDet_list->RowIndex ?>_FdeUsua" id="x<?php echo $FDet_list->RowIndex ?>_FdeUsua" size="30" placeholder="<?php echo ew_HtmlEncode($FDet->FdeUsua->getPlaceHolder()) ?>" value="<?php echo $FDet->FdeUsua->EditValue ?>"<?php echo $FDet->FdeUsua->EditAttributes() ?>>
</span>
<input type="hidden" data-table="FDet" data-field="x_FdeUsua" name="o<?php echo $FDet_list->RowIndex ?>_FdeUsua" id="o<?php echo $FDet_list->RowIndex ?>_FdeUsua" value="<?php echo ew_HtmlEncode($FDet->FdeUsua->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($FDet->FdeFCre->Visible) { // FdeFCre ?>
		<td data-name="FdeFCre">
<span id="el$rowindex$_FDet_FdeFCre" class="form-group FDet_FdeFCre">
<input type="text" data-table="FDet" data-field="x_FdeFCre" data-format="7" name="x<?php echo $FDet_list->RowIndex ?>_FdeFCre" id="x<?php echo $FDet_list->RowIndex ?>_FdeFCre" placeholder="<?php echo ew_HtmlEncode($FDet->FdeFCre->getPlaceHolder()) ?>" value="<?php echo $FDet->FdeFCre->EditValue ?>"<?php echo $FDet->FdeFCre->EditAttributes() ?>>
</span>
<input type="hidden" data-table="FDet" data-field="x_FdeFCre" name="o<?php echo $FDet_list->RowIndex ?>_FdeFCre" id="o<?php echo $FDet_list->RowIndex ?>_FdeFCre" value="<?php echo ew_HtmlEncode($FDet->FdeFCre->OldValue) ?>">
</td>
	<?php } ?>
<?php

// Render list options (body, right)
$FDet_list->ListOptions->Render("body", "right", $FDet_list->RowCnt);
?>
<script type="text/javascript">
fFDetlist.UpdateOpts(<?php echo $FDet_list->RowIndex ?>);
</script>
	</tr>
<?php
}
?>
</tbody>
</table>
<?php } ?>
<?php if ($FDet->CurrentAction == "gridadd") { ?>
<input type="hidden" name="a_list" id="a_list" value="gridinsert">
<input type="hidden" name="<?php echo $FDet_list->FormKeyCountName ?>" id="<?php echo $FDet_list->FormKeyCountName ?>" value="<?php echo $FDet_list->KeyCount ?>">
<?php echo $FDet_list->MultiSelectKey ?>
<?php } ?>
<?php if ($FDet->CurrentAction == "gridedit") { ?>
<input type="hidden" name="a_list" id="a_list" value="gridupdate">
<input type="hidden" name="<?php echo $FDet_list->FormKeyCountName ?>" id="<?php echo $FDet_list->FormKeyCountName ?>" value="<?php echo $FDet_list->KeyCount ?>">
<?php echo $FDet_list->MultiSelectKey ?>
<?php } ?>
<?php if ($FDet->CurrentAction == "") { ?>
<input type="hidden" name="a_list" id="a_list" value="">
<?php } ?>
</div>
</form>
<?php

// Close recordset
if ($FDet_list->Recordset)
	$FDet_list->Recordset->Close();
?>
<div class="panel-footer ewGridLowerPanel">
<?php if ($FDet->CurrentAction <> "gridadd" && $FDet->CurrentAction <> "gridedit") { ?>
<form name="ewPagerForm" class="ewForm form-inline ewPagerForm" action="<?php echo ew_CurrentPage() ?>">
<?php if (!isset($FDet_list->Pager)) $FDet_list->Pager = new cPrevNextPager($FDet_list->StartRec, $FDet_list->DisplayRecs, $FDet_list->TotalRecs) ?>
<?php if ($FDet_list->Pager->RecordCount > 0) { ?>
<div class="ewPager">
<span><?php echo $Language->Phrase("Page") ?>&nbsp;</span>
<div class="ewPrevNext"><div class="input-group">
<div class="input-group-btn">
<!--first page button-->
	<?php if ($FDet_list->Pager->FirstButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerFirst") ?>" href="<?php echo $FDet_list->PageUrl() ?>start=<?php echo $FDet_list->Pager->FirstButton->Start ?>"><span class="icon-first ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerFirst") ?>"><span class="icon-first ewIcon"></span></a>
	<?php } ?>
<!--previous page button-->
	<?php if ($FDet_list->Pager->PrevButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerPrevious") ?>" href="<?php echo $FDet_list->PageUrl() ?>start=<?php echo $FDet_list->Pager->PrevButton->Start ?>"><span class="icon-prev ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerPrevious") ?>"><span class="icon-prev ewIcon"></span></a>
	<?php } ?>
</div>
<!--current page number-->
	<input class="form-control input-sm" type="text" name="<?php echo EW_TABLE_PAGE_NO ?>" value="<?php echo $FDet_list->Pager->CurrentPage ?>">
<div class="input-group-btn">
<!--next page button-->
	<?php if ($FDet_list->Pager->NextButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerNext") ?>" href="<?php echo $FDet_list->PageUrl() ?>start=<?php echo $FDet_list->Pager->NextButton->Start ?>"><span class="icon-next ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerNext") ?>"><span class="icon-next ewIcon"></span></a>
	<?php } ?>
<!--last page button-->
	<?php if ($FDet_list->Pager->LastButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerLast") ?>" href="<?php echo $FDet_list->PageUrl() ?>start=<?php echo $FDet_list->Pager->LastButton->Start ?>"><span class="icon-last ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerLast") ?>"><span class="icon-last ewIcon"></span></a>
	<?php } ?>
</div>
</div>
</div>
<span>&nbsp;<?php echo $Language->Phrase("of") ?>&nbsp;<?php echo $FDet_list->Pager->PageCount ?></span>
</div>
<div class="ewPager ewRec">
	<span><?php echo $Language->Phrase("Record") ?>&nbsp;<?php echo $FDet_list->Pager->FromIndex ?>&nbsp;<?php echo $Language->Phrase("To") ?>&nbsp;<?php echo $FDet_list->Pager->ToIndex ?>&nbsp;<?php echo $Language->Phrase("Of") ?>&nbsp;<?php echo $FDet_list->Pager->RecordCount ?></span>
</div>
<?php } ?>
</form>
<?php } ?>
<div class="ewListOtherOptions">
<?php
	foreach ($FDet_list->OtherOptions as &$option)
		$option->Render("body", "bottom");
?>
</div>
<div class="clearfix"></div>
</div>
</div>
<?php } ?>
<?php if ($FDet_list->TotalRecs == 0 && $FDet->CurrentAction == "") { // Show other options ?>
<div class="ewListOtherOptions">
<?php
	foreach ($FDet_list->OtherOptions as &$option) {
		$option->ButtonClass = "";
		$option->Render("body", "");
	}
?>
</div>
<div class="clearfix"></div>
<?php } ?>
<script type="text/javascript">
fFDetlistsrch.Init();
fFDetlistsrch.FilterList = <?php echo $FDet_list->GetFilterList() ?>;
fFDetlist.Init();
</script>
<?php
$FDet_list->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$FDet_list->Page_Terminate();
?>
